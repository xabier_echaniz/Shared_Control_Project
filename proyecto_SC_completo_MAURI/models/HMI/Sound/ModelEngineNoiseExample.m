%% Model Engine Noise
% In this model, the Wavetable Synthesizer block is used to synthesize
% realistic engine noise. Such a system may be found in a vehicle where
% artificial engine noise enhancement is desired. The wavetable sample is
% a real-world engine recorded at an unspecified RPM.
%
% Copyright 2019 The MathWorks, Inc.

%%
%

open_system('ExampleWavetableSynthesizer');

%%
%

sim('ExampleWavetableSynthesizer',10);

%%
% 1. Run the simulation. Listen to the engine sound output from the
% Wavetable Synthesizer.
%
% 2. Tune the RPM source to adjust the perceived RPM of the generated
% engine sound. The RPM source is lowpass smoothed using a Biquad filter,
% so that the engine sound ramps in a realistic fashion. Visualize the RPM
% source before and after smoothing on a Scope.
%

open_system('ExampleWavetableSynthesizer/Scope');

%%
% 3. The tuning factor can be used to increase or decrease the overall
% range of output frequencies. This is used because the wavetable sample
% RPM is unknown and the sound range might require calibration.