% Copyright 2019 The MathWorks, Inc.

[EngineSample,fs] = audioread('EngineSample1.wav');
samplesPerFrame = 2048;
N = 2; % Order
F3dB = 1; % 3-dB Frequency
fs = fs/samplesPerFrame;  % Sampling Frequency (block sample rate)

h = fdesign.lowpass('n,f3db', N, F3dB, fs);

LPF = design(h,'butter','SystemObject',true);