OUTPUT FILE VARIABLE LIST 
-> VISOR DRIVER COMMANDS VARIABLES 
	1. Steering_Wheel 
	2. Throttle 
	3. Brake_Pedal 
-> CHASSIS 1 VARIABLES 
	4. X_CG_SM_chassis_1 
	5. Y_CG_SM_chassis_1 
	6. Z_CG_SM_chassis_1 
	7. Vx_CG_SM_local_chassis_1 
	8. Vy_CG_SM_local_chassis_1 
	9. Vz_CG_SM_local_chassis_1 
	10. Ax_CG_SM_local_chassis_1 
	11. Ay_CG_SM_local_chassis_1 
	12. Az_CG_SM_local_chassis_1 
	13. Roll_Euler_chassis_1 
	14. Pitch_Euler_chassis_1 
	15. Yaw_Euler_chassis_1 
	16. Roll_rate_chassis_1 
	17. Pitch_rate_chassis_1 
	18. Yaw_rate_chassis_1 
	19. Roll_acceleration_chassis_1 
	20. Pitch_acceleration_chassis_1 
	21. Yaw_acceleration_chassis_1 
	22. X_aero_force_chassis_1 
	23. Y_aero_force_chassis_1 
	24. Z_aero_force_chassis_1 
	25. X_aero_torque_chassis_1 
	26. Y_aero_torque_chassis_1 
	27. Z_aero_torque_chassis_1 
	28. Total_speed_chassis_1 
	29. Longitudinal_speed_km_h_chassis_1 
	30. Ignition_button_chassis_1 
	31. Torque_vect_status_chassis_1 
	32. Torque_vect_button_chassis_1 
	33. Normalized_steer_wheel_chassis_1 
	34. Torque_steer_wheel_chassis_1 
	-> KNUCKLE 1 VARIABLES 
		35. Spring_comp_wheel_1 
		36. Spring_comp_rate_wheel_1 
		37. Spring_comp_accel_wheel_1 
		38. Total_susp_force_wheel_1 
		-> WHEEL 1 VARIABLES 
			39. X_wheel_1 
			40. Y_wheel_1 
			41. Z_wheel_1 
			42. Vx_wheel_1 
			43. Vy_wheel_1 
			44. Vz_wheel_1 
			45. Yaw_Euler_wheel_1 
			46. Spin_rate_wheel_1 
			47. Spin_accel_wheel_1 
			48. Fx_wheel_1 
			49. Fy_wheel_1 
			50. Fz_wheel_1 
			51. Mx_wheel_1 
			52. My_wheel_1 
			53. Mz_wheel_1 
		-> TIRE 1 VARIABLES 
			54. Long_slip_wheel_1 
			55. Long_slip_lagged_wheel_1 
			56. Lat_Slip_Angle_wheel_1 
			57. Lat_Slip_Angle_Lagged_wheel_1 
			58. Long_tire_force_wheel_1 
			59. Lat_tire_force_wheel_1 
			60. Vert_tire_force_wheel_1 
			61. Overturning_torque_wheel_1 
			62. Roll_resist_torque_wheel_1 
			63. Aligning_torque_wheel_1 
			64. Friction_coefficient_wheel_1 
	-> KNUCKLE 2 VARIABLES 
		65. Spring_comp_wheel_2 
		66. Spring_comp_rate_wheel_2 
		67. Spring_comp_accel_wheel_2 
		68. Total_susp_force_wheel_2 
		-> WHEEL 2 VARIABLES 
			69. X_wheel_2 
			70. Y_wheel_2 
			71. Z_wheel_2 
			72. Vx_wheel_2 
			73. Vy_wheel_2 
			74. Vz_wheel_2 
			75. Yaw_Euler_wheel_2 
			76. Spin_rate_wheel_2 
			77. Spin_accel_wheel_2 
			78. Fx_wheel_2 
			79. Fy_wheel_2 
			80. Fz_wheel_2 
			81. Mx_wheel_2 
			82. My_wheel_2 
			83. Mz_wheel_2 
		-> TIRE 2 VARIABLES 
			84. Long_slip_wheel_2 
			85. Long_slip_lagged_wheel_2 
			86. Lat_Slip_Angle_wheel_2 
			87. Lat_Slip_Angle_Lagged_wheel_2 
			88. Long_tire_force_wheel_2 
			89. Lat_tire_force_wheel_2 
			90. Vert_tire_force_wheel_2 
			91. Overturning_torque_wheel_2 
			92. Roll_resist_torque_wheel_2 
			93. Aligning_torque_wheel_2 
			94. Friction_coefficient_wheel_2 
	-> KNUCKLE 3 VARIABLES 
		95. Spring_comp_wheel_3 
		96. Spring_comp_rate_wheel_3 
		97. Spring_comp_accel_wheel_3 
		98. Total_susp_force_wheel_3 
		-> WHEEL 3 VARIABLES 
			99. X_wheel_3 
			100. Y_wheel_3 
			101. Z_wheel_3 
			102. Vx_wheel_3 
			103. Vy_wheel_3 
			104. Vz_wheel_3 
			105. Yaw_Euler_wheel_3 
			106. Spin_rate_wheel_3 
			107. Spin_accel_wheel_3 
			108. Fx_wheel_3 
			109. Fy_wheel_3 
			110. Fz_wheel_3 
			111. Mx_wheel_3 
			112. My_wheel_3 
			113. Mz_wheel_3 
		-> TIRE 3 VARIABLES 
			114. Long_slip_wheel_3 
			115. Long_slip_lagged_wheel_3 
			116. Lat_Slip_Angle_wheel_3 
			117. Lat_Slip_Angle_Lagged_wheel_3 
			118. Long_tire_force_wheel_3 
			119. Lat_tire_force_wheel_3 
			120. Vert_tire_force_wheel_3 
			121. Overturning_torque_wheel_3 
			122. Roll_resist_torque_wheel_3 
			123. Aligning_torque_wheel_3 
			124. Friction_coefficient_wheel_3 
	-> KNUCKLE 4 VARIABLES 
		125. Spring_comp_wheel_4 
		126. Spring_comp_rate_wheel_4 
		127. Spring_comp_accel_wheel_4 
		128. Total_susp_force_wheel_4 
		-> WHEEL 4 VARIABLES 
			129. X_wheel_4 
			130. Y_wheel_4 
			131. Z_wheel_4 
			132. Vx_wheel_4 
			133. Vy_wheel_4 
			134. Vz_wheel_4 
			135. Yaw_Euler_wheel_4 
			136. Spin_rate_wheel_4 
			137. Spin_accel_wheel_4 
			138. Fx_wheel_4 
			139. Fy_wheel_4 
			140. Fz_wheel_4 
			141. Mx_wheel_4 
			142. My_wheel_4 
			143. Mz_wheel_4 
		-> TIRE 4 VARIABLES 
			144. Long_slip_wheel_4 
			145. Long_slip_lagged_wheel_4 
			146. Lat_Slip_Angle_wheel_4 
			147. Lat_Slip_Angle_Lagged_wheel_4 
			148. Long_tire_force_wheel_4 
			149. Lat_tire_force_wheel_4 
			150. Vert_tire_force_wheel_4 
			151. Overturning_torque_wheel_4 
			152. Roll_resist_torque_wheel_4 
			153. Aligning_torque_wheel_4 
			154. Friction_coefficient_wheel_4 


 -> UDP NUMBER OF OBJECTS = 50
 -> UDP NUMBER OF CHASSIS = 1
 -> UDP NUMBER OF RIGID AXLES = 0
 -> UDP NUMBER OF WHEELS = 4
 -> UDP NUMBER OF X FORCES = 4
 -> UDP NUMBER OF Y FORCES = 4
 -> UDP NUMBER OF Z FORCES = 4
 -> UDP NUMBER OF STEERING WHEELS = 1
 -> UDP NUMBER OF IGNITION SOUNDS = 0
 -> UDP NUMBER OF ENGINE SOUNDS = 0
 -> UDP NUMBER OF GEAR CHANGE SOUNDS = 0
 -> UDP NUMBER OF WIND SOUNDS = 0
 -> UDP NUMBER OF BRAKE LIGHTS = 1
 -> UDP NUMBER OF REVERSE LIGHTS = 1
 -> UDP NUMBER OF LEFT INDICATORS = 1
 -> UDP NUMBER OF RIGHT INDICATORS = 1
 -> UDP NUMBER OF LOW BEAM LIGHTS = 2
 -> UDP NUMBER OF HIGH BEAM LIGHTS = 2
 -> UDP NUMBER OF ROAD SOUNDS = 4
 -> UDP NUMBER OF CURBS SOUNDS = 4
 -> UDP NUMBER OF GRASS SOUNDS = 4
 -> UDP NUMBER OF SKIDS = 4
 -> UDP NUMBER OF TIRE MARKS = 4
 -> UDP NUMBER OF PARTICLES = 4
