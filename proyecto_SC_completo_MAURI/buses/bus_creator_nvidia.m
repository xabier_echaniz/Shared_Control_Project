min_double = 50; max_double = 100;   %    r = (max_double-min_double).*rand(1,'double') + min_double;
min_uint32 = 0 ; maxuint32  = 10000; %    r = randi([min_uint32,maxuint32], 'uint32');
min_uint8  = 0 ; maxuint8  = 255;   %     r = randi([min_uint8,min_uint8], 'uint8');
min_int16 = -1000 ; max_int16 = 1000;
MAX_POINTS = 30;
MAX_OBSTACLES = 20;

EgoVehicle          = struct; EgoVehicle.id         = uint8(1);
LaneMarkerFragment  = struct; LaneMarkerFragment.id = uint8(2);
Lane                = struct; Lane.id               = uint8(3);
PositionLane        = struct; PositionLane.id       = uint8(4);
Obstacle            = struct; Obstacle.id           = uint8(5);
Obstacle2           = struct; Obstacle2.id           = uint8(5);
singleObstacle      = struct; 

%% EGOVEHICLE
EgoVehicle.pos_x                     = randi([min_int16,max_int16], 'int32');   
EgoVehicle.pos_y                     = randi([min_int16,max_int16], 'int32');                   
EgoVehicle.speed_x                   = randi([min_int16,max_int16], 'int16');                
EgoVehicle.speed_y                   = randi([min_int16,max_int16], 'int16');      
EgoVehicle.acc_x                     = randi([min_int16,max_int16], 'int16');  
EgoVehicle.acc_y                     = randi([min_int16,max_int16], 'int16');  
EgoVehicle.heading                   = randi([min_int16,max_int16], 'int32');             
EgoVehicle.e_pos_x                   = randi([min_int16,max_int16], 'int16');             
EgoVehicle.e_pos_y                   = randi([min_int16,max_int16], 'int16');              
EgoVehicle.e_speed_x                 = randi([min_int16,max_int16], 'int16');             
EgoVehicle.e_speed_y                 = randi([min_int16,max_int16], 'int16');   

%% LANEMARKER
LaneMarkerFragment.id_lane			= randi([min_int16,max_int16], 'int16');  
LaneMarkerFragment.num_points		= uint8(MAX_POINTS);
tempGeometry                		= randi([min_int16,max_int16],2,MAX_POINTS,'int16');
LaneMarkerFragment.geometry         =  reshape(tempGeometry, [1, 2*MAX_POINTS]);
LaneMarkerFragment.distance_min		= randi([min_int16,max_int16], 'int16'); 
LaneMarkerFragment.distance_max		= randi([min_int16,max_int16], 'int16'); 
LaneMarkerFragment.type				= randi([min_uint8,maxuint8], 'uint8');
LaneMarkerFragment.quality			= randi([min_uint8,maxuint8], 'uint8');

%%LANE
Lane.id_lane                        = randi([min_int16,max_int16], 'int16'); 
Lane.id_left_line                   = randi([min_int16,max_int16], 'int16'); 
Lane.id_right_line                  = randi([min_int16,max_int16], 'int16'); 
Lane.id_left_sibling                = randi([min_int16,max_int16], 'int16'); 
Lane.id_right_sibling               = randi([min_int16,max_int16], 'int16'); 
Lane.quality                        = randi([min_uint8,maxuint8], 'uint8');

%%POSITIONLANE
PositionLane.id_lane		= randi([min_uint32,maxuint32], 'int16');
PositionLane.width			= randi([min_int16,max_int16], 'int16'); 
PositionLane.lateral_offset	= randi([min_int16,max_int16], 'int16'); 
PositionLane.angle			= randi([min_int16,max_int16], 'int32'); 
PositionLane.quality		= randi([min_uint8,maxuint8], 'uint8');

%%OBSTACLE
Obstacle.number_obstacles   = uint8(MAX_OBSTACLES);
Obstacle.obstacle_id        = randi([min_int16,max_int16],1,MAX_OBSTACLES,'int16');
Obstacle.type				= randi([min_int16,max_int16],1,MAX_OBSTACLES,'uint8');
Obstacle.pos_x				= randi([min_int16,max_int16],1,MAX_OBSTACLES, 'int16'); 
Obstacle.pos_y				= randi([min_int16,max_int16],1,MAX_OBSTACLES, 'int16'); 
Obstacle.flag_mobile		= randi([min_int16,max_int16],1,MAX_OBSTACLES, 'uint8');
Obstacle.speed_x			= randi([min_int16,max_int16],1,MAX_OBSTACLES, 'int16'); 
Obstacle.speed_y			= randi([min_int16,max_int16],1,MAX_OBSTACLES, 'int16'); 
Obstacle.size_x				= randi([min_int16,max_int16],1,MAX_OBSTACLES, 'int16'); 
Obstacle.size_y				= randi([min_int16,max_int16],1,MAX_OBSTACLES, 'int16'); 
Obstacle.quality			= randi([min_int16,max_int16],1,MAX_OBSTACLES, 'uint8');

singleObstacle.obstacle_id      = randi([min_uint32,maxuint32], 'uint8');
singleObstacle.type				= randi([min_uint32,maxuint32], 'uint8');
singleObstacle.pos_x			= randi([min_int16,max_int16], 'int16'); 
singleObstacle.pos_y			= randi([min_int16,max_int16], 'int16'); 
singleObstacle.flag_mobile		= randi([min_uint8,maxuint8], 'uint8');
singleObstacle.speed_x			= randi([min_int16,max_int16], 'int16'); 
singleObstacle.speed_y			= randi([min_int16,max_int16], 'int16'); 
singleObstacle.size_x			= randi([min_int16,max_int16], 'int16'); 
singleObstacle.size_y			= randi([min_int16,max_int16], 'int16'); 
singleObstacle.quality			= randi([min_uint8,maxuint8], 'uint8');

singleObstacleBus = createBusFromStruct(singleObstacle);
singleObstacleBus.Description = 'singleObstacleBus' ;

Obstacle2.number_obstacles   = uint8(MAX_OBSTACLES);
Obstacle2.singleObstacle(1:MAX_OBSTACLES)      = singleObstacleBus;

EgoVehicleDataBus = createBusFromStruct(EgoVehicle);
LaneMarkerFragmentDataBus = createBusFromStruct(LaneMarkerFragment);
LaneDataBus = createBusFromStruct(Lane);
PositionLaneDataBus = createBusFromStruct(PositionLane);
ObstacleDataBus = createBusFromStruct(Obstacle);
ObstacleDataBus2 = createBusFromStruct(Obstacle2);

BUS_PROTOTYPES                      = struct;
BUS_PROTOTYPES.EgoVehicle           = Simulink.Bus.createMATLABStruct('EgoVehicleDataBus');
BUS_PROTOTYPES.LaneMarkerFragment   = Simulink.Bus.createMATLABStruct('LaneMarkerFragmentDataBus');
BUS_PROTOTYPES.Lane                 = Simulink.Bus.createMATLABStruct('LaneDataBus');
BUS_PROTOTYPES.PositionLane         = Simulink.Bus.createMATLABStruct('PositionLaneDataBus');
BUS_PROTOTYPES.Obstacle             = Simulink.Bus.createMATLABStruct('ObstacleDataBus');
BUS_PROTOTYPES.Obstacle2            = Simulink.Bus.createMATLABStruct('ObstacleDataBus2');