% bus creator for ego-vehicle
EgoVeh       = struct;
EgoVeh.X     = double(0);
EgoVeh.Y     = double(0);
EgoVeh.psi   = double(0);
EgoVeh.vx    = double(0);
EgoVeh.ax    = double(0);
EgoVeh.vy    = double(0);
EgoVeh.ay    = double(0);
EgoVeh.dpsi  = double(0);
EgoVeh.ddpsi = double(0);
EgoVeh.delta = double(0);
EgoVeh.sat   = double(0);
EgoVeh.slip  = double(0);
EgoVeh.Cf    = double(0);
EgoVeh.Cr    = double(0);
EgoVeh.day   = double(0);

EgoVehInfo              = Simulink.Bus.createObject(EgoVeh);
EgoVehBus               = eval(EgoVehInfo.busName);
EgoVehBus.Description   = 'EgoVehBus';