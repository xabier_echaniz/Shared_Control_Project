% bus creator for Task
Task               = struct;
%% head information
Task.interval     = double(0);
Task.time      = double(0);
Task.button       = double(0);
Task.beep         = double(0);


%% Task bus
TaskInfo              = Simulink.Bus.createObject(Task);
TaskBus               = eval(TaskInfo.busName);
TaskBus.Description   = 'TaskBus';