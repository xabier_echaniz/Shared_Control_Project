States              = struct;

%% STATES VARIABLES
States.vx       = double(0);
States.ax       = double(0);
States.vy       = double(0);
States.ay       = double(0);
States.dpsi     = double(0);
States.X        = double(0);
States.Y        = double(0);
States.psi      = double(0);
States.delta    = double(0);

StatesDataBus       = createBusFromStruct(States);

BUS_CORE            = struct;
BUS_CORE.States  = Simulink.Bus.createMATLABStruct('StatesDataBus');