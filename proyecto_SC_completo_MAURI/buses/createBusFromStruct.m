function [ busObject ] = createBusFromStruct( specStruct )
%CREATEBUSFROMSTRUCT Summary of this function goes here
%   Detailed explanation goes here

    busObject = Simulink.Bus;
    busObject.Description = '';
    allFields = fields(specStruct);
%     busObject.Elements(numel(allFields)) = Simulink.BusElement;
    for indF=1:numel(allFields)
        fieldName = allFields{indF};
        field = specStruct.(fieldName);
        busObject.Elements(indF) = Simulink.BusElement;
        busObject.Elements(indF).Name = fieldName;
        busObject.Elements(indF).DataType = class(field);
        busObject.Elements(indF).Dimensions = size(field);
        if isa(field, 'Simulink.Bus')            
            busObject.Elements(indF).DataType = ['Bus: ', field(1).Description];
%         elseif isstruct(field)
%             subBus = createBusFromStruct(field);
%             busObject.Elements(indF).DataType = ['Bus: subBus', num2str(indF)];
        else
            % NOP
        end
    end

end

