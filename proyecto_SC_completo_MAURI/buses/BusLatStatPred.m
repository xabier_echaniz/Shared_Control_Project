% bus creator for ego-vehicle
LatStatPred        = struct;
LatStatPred.X      = double(zeros(31,1));
LatStatPred.Y      = double(zeros(31,1));
LatStatPred.psi    = double(zeros(31,1));
LatStatPred.epsi   = double(zeros(31,1));
LatStatPred.ey     = double(zeros(31,1));
LatStatPred.theta  = double(zeros(31,1));
LatStatPred.w      = double(zeros(31,1));
LatStatPred.torque = double(zeros(31,1));
LatStatPred.vx     = double(zeros(31,1));
LatStatPred.vy     = double(zeros(31,1));
LatStatPred.delta  = double(zeros(31,1));
LatStatPred.dpsi   = double(zeros(31,1));
LatStatPred.Cf     = double(zeros(31,1));
LatStatPred.Cr     = double(zeros(31,1));
LatStatPred.dT     = double(zeros(31,1));

LatStatPredInfo              = Simulink.Bus.createObject(LatStatPred);
LatStatPredBus               = eval(LatStatPredInfo.busName);
LatStatPredBus.Description   = 'LatStatPredBus';