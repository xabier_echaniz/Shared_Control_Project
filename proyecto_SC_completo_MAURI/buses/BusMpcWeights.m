MPC_Weights       = struct;
MPC_Weights.dt    = double(0);
MPC_Weights.t     = double(0);
MPC_Weights.ey    = double(0);
MPC_Weights.epsi  = double(0);
MPC_Weights.vy    = double(0);
MPC_Weights.dpsi  = double(0);
MPC_Weights.w     = double(0);
MPC_Weights.ay    = double(0);

MPC_WeightsInfo              = Simulink.Bus.createObject(MPC_Weights);
MPC_WeightsBus               = eval(MPC_WeightsInfo.busName);
MPC_WeightsBus.Description   = 'MPC_WeightsBus';


