% bus creator for DMS
Dms               = struct;
%% head information
Dms.head_center     = double(0);
Dms.head_right      = double(0);
Dms.head_left       = double(0);
Dms.head_up         = double(0);
Dms.head_down       = double(0);
Dms.head_time       = double(0);
%% eyes information
Dms.eyes_blink      = double(0);
Dms.eyes_blink_rate = double(0);
Dms.eyes_drowsy     = double(0);
Dms.eyes_center     = double(0);
Dms.eyes_right      = double(0);
Dms.eyes_left       = double(0);
Dms.eyes_time       = double(0);
%% distraction
Dms.distraction        = double(0);
Dms.distraction_binary = double(0);

%% Dms bus
DmsInfo              = Simulink.Bus.createObject(Dms);
DmsBus               = eval(DmsInfo.busName);
DmsBus.Description   = 'DmsBus';