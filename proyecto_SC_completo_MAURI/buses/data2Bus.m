function [EgoVehicleBus, updateEV ,LaneFragmentBus, updateLF,LaneBus, updateLA,PositionLaneBus,updateLPOS,ObstacleBus, updateOBS,offset,bytestream8,message_id,bytesTest] = data2Bus(bytestream32,BUS_PROTOTYPES)
% [EgoVehicleBus,LaneFragmentBus,LaneBus,PositionLaneBus,ObstacleBus,offset,bytestream8,message_id,bytesTest] = data2Bus_(bytestream32,BUS_PROTOTYPES)
%[EgoVehicleBus, LaneFragmentBus, LaneBus, PositionLaneBus, ObstacleBus] = data2Bus(bytestream32,BUS_PROTOTYPES);

% Bus_1='EgoVehicle';             EgoVehicle = BUS_PROTOTYPES.EgoVehicle;
% Bus_2='LaneMarkerFragment';     LaneFragment = BUS_PROTOTYPES.LaneMarkerFragment;
% Bus_3='Lane';                   Lane = BUS_PROTOTYPES.Lane;
% Bus_4='PositionLane';           PositionLane = BUS_PROTOTYPES.PositionLane;
% Bus_5='Obstacle';               Obstacle = BUS_PROTOTYPES.Obstacle;

EgoVehicleBus = BUS_PROTOTYPES.EgoVehicle;
LaneFragmentBus = BUS_PROTOTYPES.LaneMarkerFragment;
LaneBus = BUS_PROTOTYPES.Lane;
PositionLaneBus = BUS_PROTOTYPES.PositionLane;
ObstacleBus = BUS_PROTOTYPES.Obstacle;
updateEV = false;
updateLF = false;
updateLA = false;
updateLPOS = false;
updateOBS = false;

persistent EgoVehicle LaneFragment Lane PositionLane Obstacle...
    counter counterEV counterLF counterLA counterLPOS counterOBS;

if isempty(EgoVehicle)
    counter = uint64(0);
    counterEV = uint64(0);
    counterLF = uint64(0);
    counterLA = uint64(0);
    counterLPOS = uint64(0);
    counterOBS = uint64(0);
%     EgoVehicle = EgoVehicleBus;
%     LaneFragment = LaneFragmentBus;
%     Lane = LaneBus;
%     PositionLane = PositionLaneBus;
%     Obstacle = ObstacleBus;
end
%Period definitions in tens of ms (*10ms)
EV_T = 1;
LM_F_T = 20;
L_T = 20;
L_POS = 20;
OBS_T= 10;

% EgoVehicleBusArray = repmat(BUS_PROTOTYPES.EgoVehicle, 1 ,10);
% LaneMarkerFragmentArray = repmat(BUS_PROTOTYPES.LaneMarkerFragment, 1 ,3);
% LaneArray = repmat(BUS_PROTOTYPES.Lane, 1 ,10);
% PositionLaneArray = repmat(BUS_PROTOTYPES.PositionLane, 1 ,10);
% ObstacleArray = repmat(BUS_PROTOTYPES.Obstacle, 1 ,10);

counter = counter + 1;
bytestream8         = typecast((bytestream32),'uint8');
flag_messages_left  = true;
offset              = uint32(1);
offset_temp         = uint32(1);
message_id          = uint8(0);
bytesTest           = zeros(8,1,'uint8');

while (flag_messages_left)
    message_id = bytestream8(offset); 
    switch message_id
        case 1
            [offset_temp,EgoVehicleBus,bytesTest] = deserialize_nvidia(BusType.EGO_VEHICLE,EgoVehicleBus,bytestream8, offset);
            offset = offset + offset_temp;
            counterEV = counter;
            updateEV = true;
%         case 2 
%             [offset_temp, LaneFragmentBus] = deserialize_nvidia(BusType.LANE_MRK_FRAGMENT,LaneFragmentBus,bytestream8, offset);
%             offset = offset + offset_temp;
%             counterLF = counter;
%             updateLF = true;
%         case 3
%             [offset_temp, LaneBus] = deserialize_nvidia(BusType.LANE,LaneBus,bytestream8, offset);
%             offset = offset + offset_temp;
%             counterLA = counter;
%             updateLA = true;
%         case 4
%             [offset_temp, PositionLaneBus] = deserialize_nvidia(BusType.POSITION_LANE,PositionLaneBus,bytestream8, offset);
%             offset = offset + offset_temp;
%             counterLPOS = counter;
%             updateLPOS = true;
        case 5
            [offset_temp, ObstacleBus] = deserialize_nvidia(BusType.OBSTACLE,ObstacleBus,bytestream8, offset);
            offset = offset + offset_temp;
            counterOBS = counter;
            updateOBS = true;
        otherwise
            flag_messages_left = false;
    end
end

% EgoVehicleBus   = EgoVehicle;
% LaneFragmentBus = LaneFragment;
% LaneBus         = Lane;
% PositionLaneBus = PositionLane;
% ObstacleBus     = Obstacle;

% timeoutEV = (counter - counterEV)>2*EV_T;
% timeoutLMF = (counter - counterLF)>2*LM_F_T;
% timeoutLA = (counter - counterLA)>2*L_T;
% timeoutLPOS = (counter - counterLPOS)>2*L_POS;
% timeoutOBST = (counter - counterOBS)>2*OBS_T;
% 
% if timeoutEV
%     EgoVehicleBus = BUS_PROTOTYPES.EgoVehicle;
% end
% if timeoutLMF 
%     LaneFragmentBus = BUS_PROTOTYPES.LaneMarkerFragment;
% end
% if timeoutLA 
%     LaneBus = BUS_PROTOTYPES.Lane;
% end
% if timeoutLPOS
%     PositionLaneBus = BUS_PROTOTYPES.PositionLane;
% end
% if timeoutOBST
%     ObstacleBus = BUS_PROTOTYPES.Obstacle;
% end


% Bytestream32 = dataGeneration2(BUS_PROTOTYPES); BUS_PROTOTYPES
end
