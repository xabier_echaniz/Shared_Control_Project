% bus creator for ego-vehicle
LonStatPred      = struct;
LonStatPred.vx   = double(zeros(31,1));
LonStatPred.ax   = double(zeros(31,1));

LonStatPredInfo              = Simulink.Bus.createObject(LonStatPred);
LonStatPredBus               = eval(LonStatPredInfo.busName);
LonStatPredBus.Description   = 'LonStatPredBus';