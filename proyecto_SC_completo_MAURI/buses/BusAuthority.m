% bus creator for ego-vehicle
Authority       = struct;

Authority.lambda  = double(0);
Authority.kaut    = double(0);
Authority.knorm   = double(0);
Authority.dms     = double(0);
Authority.ey      = double(0);
Authority.ttlc    = double(0);




AuthorityInfo              = Simulink.Bus.createObject(Authority);
AuthorityBus               = eval(AuthorityInfo.busName);
AuthorityBus.Description   = 'AuthorityBus';