SteeringWheel_in   = struct;
SteeringWheel_in.Ttot       = double(0);
SteeringWheel_in.Tmpc       = double(0);
SteeringWheel_in.Tsat       = double(0);
SteeringWheel_in.Tauth      = double(0);
SteeringWheel_in.Tdamp      = double(0);
SteeringWheel_in.Tvib       = double(0);
SteeringWheel_in.friction   = double(0);
SteeringWheel_in.damping    = double(0);
SteeringWheel_in.inertia    = double(0);

SteeringWheelInInfo              = Simulink.Bus.createObject(SteeringWheel_in);
SteeringWheelInBus               = eval(SteeringWheelInInfo.busName);
SteeringWheelInBus.Description   = 'SteeringWheelInBus';

SteeringWheel_out  = struct;
SteeringWheel_out.theta_n  = double(0);
SteeringWheel_out.theta    = double(0);
SteeringWheel_out.w        = double(0);
SteeringWheel_out.torque   = double(0);
SteeringWheel_out.alpha    = double(0);


SteeringWheelOutInfo              = Simulink.Bus.createObject(SteeringWheel_out);
SteeringWheelOutBus               = eval(SteeringWheelOutInfo.busName);
SteeringWheelOutBus.Description   = 'SteeringWheelOutBus';

% SteeringWheel  = struct;
% SteeringWheel.theta_n  = double(0);
% SteeringWheel.theta    = double(0);
% SteeringWheel.w        = double(0);
% SteeringWheel.torque   = double(0);
% 
% SteeringWheelInfo              = Simulink.Bus.createObject(SteeringWheel);
% SteeringWheelBus               = eval(SteeringWheelInfo.busName);
% SteeringWheelBus.Description   = 'SteeringWheelBus';


