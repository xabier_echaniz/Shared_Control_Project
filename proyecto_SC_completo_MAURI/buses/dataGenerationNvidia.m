function [Bytestream32,obstacles] = dataGenerationNvidia(BUS_PROTOTYPES)

%% curvatures along the route
% making points equidistants;
route = load('eMobility_Bezier.mat');
de=10; % desired distance [m]

N=route.R(1):de:route.R(end);
XYe=interparc(length(N),route.X,route.Y,'linear');

n_obstacles = 0; 
X = XYe(:,1)';
Y = XYe(:,2)';
rand_value = randi(length(X),1,n_obstacles);
obstacle_offset_x = randi([-1 1]*10,1,n_obstacles);
obstacle_offset_y = randi([-1 1]*10,1,n_obstacles);
obstacle_x = X(rand_value) + obstacle_offset_x;
obstacle_y = Y(rand_value) + obstacle_offset_y;

obstacles = [obstacle_x;obstacle_y];

if isempty(obstacles)
    obstacles = zeros(2,1);
end

% if isempty(obstacles)
%     obstacles = zeros(2,20);
% else
%     obstacles = [obstacle_x zeros(1,20 - n_onstacles);...
%                  obstacle_y zeros(1,20 - n_onstacles)];
% end


% Bytestream                          = uint8(zeros(1,1472));
% Random value generation
min_double = 50; max_double = 100;   %    r = (max_double-min_double).*rand(1,'double') + min_double;
min_uint32 = 0 ; maxuint32  = 10000; %    r = randi([min_uint32,maxuint32], 'uint32');
min_int16 = -1000; max_int16 = 1000;
min_uint8  = 0 ; maxuint8  = 255;   %     r = randi([min_uint8,min_uint8], 'uint8');
MAX_POINTS = 30;
MAX_OBSTACLES = 20;
MAX_BYTES  = 1472;



EgoVehicle         = BUS_PROTOTYPES.EgoVehicle;
LaneMarkerFragment = BUS_PROTOTYPES.LaneMarkerFragment;
Lane               = BUS_PROTOTYPES.Lane;
PositionLane       = BUS_PROTOTYPES.PositionLane;
Obstacle           = BUS_PROTOTYPES.Obstacle;


EgoVehicle.id         = uint8(1);
LaneMarkerFragment.id = uint8(2);
Lane.id               = uint8(3);
PositionLane.id       = uint8(4);
Obstacle.id           = uint8(5);




% struct EgoVehicle {
% public:
% 	static const msg_id_t id;
% 	double pos_x;
EgoVehicle.pos_x                     = randi([min_int16,max_int16], 'int32'); 
% 	double pos_y;                   
EgoVehicle.pos_y                     = randi([min_int16,max_int16], 'int32'); 
% 	double pos_z;                    
% EgoVehicle.pos_z                     = (max_double-min_double).*rand(1,'double') + min_double;
% 	double speed_x;                  
EgoVehicle.speed_x                   = randi([min_int16,max_int16], 'int16'); 
% 	double speed_y;                  
EgoVehicle.speed_y                   = randi([min_int16,max_int16], 'int16'); 
EgoVehicle.acc_x                     = randi([min_int16,max_int16], 'int16');  
EgoVehicle.acc_y                     = randi([min_int16,max_int16], 'int16');  
% 	double roll;                     
% EgoVehicle.roll                      = (max_double-min_double).*rand(1,'double') + min_double;
% 	double pitch;                   
% EgoVehicle.pitch                     = (max_double-min_double).*rand(1,'double') + min_double;
% 	double heading;                 
EgoVehicle.heading                   = randi([min_int16,max_int16], 'int32'); 
% 	double e_pos_x;                 
EgoVehicle.e_pos_x                   = randi([min_int16,max_int16], 'int16'); 
% 	double e_pos_y;                 
EgoVehicle.e_pos_y                   = randi([min_int16,max_int16], 'int16'); 
% 	double e_pos_z;                 
% EgoVehicle.e_pos_z                   = (max_double-min_double).*rand(1,'double') + min_double;
% 	double e_speed_x;               
EgoVehicle.e_speed_x                 = randi([min_int16,max_int16], 'int16'); 
% 	double e_speed_y;               
EgoVehicle.e_speed_y                 = int16(3.5);%(max_double-min_double).*rand(1,'double') + min_double;
% 	size_t msg_size;                 = (max_double-min_double).*rand(1,'double') + min_double;
%EgoVehicle.msg_size                 
%     }
% struct LaneMarkerFragment {
% public:
% 	static const msg_id_t	id;
% 	lane_marker_id			id_lane;
LaneMarkerFragment.id_lane			= randi([min_uint32,MAX_POINTS], 'int16');
% 	uint32_t				num_points;
% LaneMarkerFragment.num_points		= randi([min_uint32,MAX_POINTS], 'uint32');
LaneMarkerFragment.num_points       = uint8(length(BUS_PROTOTYPES.LaneMarkerFragment.geometry)/2);
% 	std::vector<std::tuple<double, double>>	geometry;
% LaneMarkerFragmentGeometryTemp2      = randi(60,2,LaneMarkerFragment.num_points,'int16');
LaneMarkerFragmentGeometryTemp2      = randi(60,2,MAX_POINTS,'int16');
LaneMarkerFragmentGeometryTemp3      = reshape(LaneMarkerFragmentGeometryTemp2, [1, 2*LaneMarkerFragment.num_points]);
LaneMarkerFragment.geometry			= LaneMarkerFragmentGeometryTemp3;
% 	double					distance_min;
LaneMarkerFragment.distance_min		= randi([min_int16,max_int16], 'int16'); 
% 	double					distance_max;
LaneMarkerFragment.distance_max		= randi([min_int16,max_int16], 'int16'); 
% 	uint32_t				type;
LaneMarkerFragment.type				= uint8(44);%randi([min_uint32,maxuint32], 'uint32');
% 	uint32_t				quality;
LaneMarkerFragment.quality			= uint8(88);%randi([min_uint32,maxuint32], 'uint32');
% 	size_t					msg_size;
%LaneMarkerFragment.msg_size			= (max_double-min_double).*rand(1,'double') + min_double;
%     }

% struct Lane {
% public:
% 	static const msg_id_t id;
% 	lane_id id_lane;
Lane.id_lane                        = randi([min_int16,max_int16], 'int16'); 
% 	lane_id id_left_line;
Lane.id_left_line                   = randi([min_int16,max_int16], 'int16'); 
% 	lane_id id_right_line;
Lane.id_right_line                  = randi([min_int16,max_int16], 'int16'); 
% 	lane_id id_left_sibling;
Lane.id_left_sibling                = randi([min_int16,max_int16], 'int16'); 
% 	lane_id id_right_sibling;
Lane.id_right_sibling               = randi([min_int16,max_int16], 'int16'); 
% 	uint32_t quality;
Lane.quality                        = uint8(99);%randi([min_uint32,maxuint32], 'uint32');
% 	size_t msg_size;
%Lane.msg_size                       = randi([0,10000], 'uint32');
%     }

% struct PositionLane {
% public:
% 	static const msg_id_t id;
% 	lane_id id_lane;
PositionLane.id_lane		= randi([min_uint32,maxuint32], 'int16');
% 	double width;
PositionLane.width			= randi([min_int16,max_int16], 'int16'); 
% 	double lateral_offset;
PositionLane.lateral_offset	= randi([min_int16,max_int16], 'int16'); 
% 	double angle;
PositionLane.angle			= randi([min_int16,max_int16], 'int32'); 
% 	uint32_t quality;
PositionLane.quality		= uint8(77);%randi([min_uint32,maxuint32], 'uint32');
% 	size_t msg_size;
%PositionLane.msg_size
%     }

% struct Obstacle {
% public:
% 	static const msg_id_t id;
Obstacle.number_obstacles   = randi([0,MAX_OBSTACLES], 'uint8'); 
%   obstacle_id id_obstacle;
Obstacle_id_temp            = randi(20,1,MAX_OBSTACLES,'int16');
Obstacle.obstacle_id		= Obstacle_id_temp;
% 	uint32_t type;
Obstacle_type_temp          = randi([1 4],1,MAX_OBSTACLES,'uint8');
Obstacle.type				= Obstacle_type_temp;
% 	double pos_x;
Obstacle_pos_x_temp 		= randi([-1 1]*2000,1,MAX_OBSTACLES,'int16');
Obstacle.pos_x				= Obstacle_pos_x_temp; 
% 	double pos_y;	
Obstacle_pos_y_temp			= randi([-1 1]*2000,1,MAX_OBSTACLES,'int16');
Obstacle.pos_y				= Obstacle_pos_y_temp; 
% 	double pos_z;
% Obstacle.pos_z				= (max_double-min_double).*rand(1,'int16') + min_double;
% 	uint32_t flag_mobile;
Obstacle_flag_mobile_temp	= randi(1,1,MAX_OBSTACLES,'uint8');
Obstacle.flag_mobile		= Obstacle_flag_mobile_temp;
% 	double speed_x;
Obstacle_speed_x_temp		= randi(1,1,MAX_OBSTACLES,'int16');
Obstacle.speed_x			= Obstacle_speed_x_temp; 
% 	double speed_y;
Obstacle_speed_y_temp		= randi(1,1,MAX_OBSTACLES,'int16');
Obstacle.speed_y			= Obstacle_speed_y_temp; 
% 	double speed_z;
% Obstacle.speed_z			= (max_double-min_double).*rand(1,'double') + min_double;
% 	double size_x;
Obstacle_size_x_temp		= randi([100 400],1,MAX_OBSTACLES,'int16');
Obstacle.size_x				= Obstacle_size_x_temp; 
% 	double size_y;
Obstacle_size_y_temp		= randi([100 400],1,MAX_OBSTACLES,'int16');
Obstacle.size_y				= Obstacle_size_y_temp; 
% 	uint32_t quality;
% Obstacle_quality_temp		= randi(20,1,MAX_OBSTACLES,'uint8'); 
Obstacle_quality_temp		= 60*ones(1,MAX_OBSTACLES,'uint8'); 
Obstacle.quality			= Obstacle_quality_temp;%randi([min_uint32,maxuint32], 'uint32');
% 	size_t msg_size;
%Obstacle.msg_size		= randi([min_uint32,maxuint32], 'uint32');
%     }

EgoVehicle = flatten_struct(EgoVehicle);
LaneMarkerFragment = flatten_struct(LaneMarkerFragment);
Lane = flatten_struct(Lane);
PositionLane = flatten_struct(PositionLane);
Obstacle = flatten_struct(Obstacle);

% EgoVehicleVec 			= [EgoVehicle.id EgoVehicle.pos_x   EgoVehicle.pos_y   EgoVehicle.pos_z...
% 						EgoVehicle.speed_x EgoVehicle.speed_y EgoVehicle.roll  EgoVehicle.pitch ...
% 						EgoVehicle.heading EgoVehicle.e_pos_x EgoVehicle.e_pos_y EgoVehicle.e_pos_z...
% 						EgoVehicle.e_speed_x EgoVehicle.e_speed_y];
                    
EgoVehicleVec         = [EgoVehicle.id EgoVehicle.pos_x   EgoVehicle.pos_y ...
						EgoVehicle.speed_x EgoVehicle.speed_y EgoVehicle.acc_x ...
						EgoVehicle.acc_y EgoVehicle.heading EgoVehicle.e_pos_x EgoVehicle.e_pos_y ...
						EgoVehicle.e_speed_x EgoVehicle.e_speed_y];
	
LaneMarkerFragmentVec = [LaneMarkerFragment.id LaneMarkerFragment.id_lane ...
						LaneMarkerFragment.num_points LaneMarkerFragment.geometry  ...
						LaneMarkerFragment.distance_min LaneMarkerFragment.distance_max ...
						LaneMarkerFragment.type LaneMarkerFragment.quality];
						
LaneVec				  = [Lane.id Lane.id_lane Lane.id_left_line Lane.id_right_line...
						Lane.id_left_sibling Lane.id_right_sibling Lane.quality];		
						
PositionLaneVec		  = [PositionLane.id PositionLane.id_lane PositionLane.width...
                        PositionLane.lateral_offset PositionLane.angle	 PositionLane.quality];	
ObstacleVec			  = [Obstacle.id ...
                        Obstacle.number_obstacles];

for k =1:MAX_OBSTACLES
    tempSingleObstacle = [Obstacle.obstacle_id(2*k-1:2*k) ...
                        Obstacle.type(k) ...
                        Obstacle.pos_x(2*k-1:2*k) ...
                        Obstacle.pos_y(2*k-1:2*k) ...
                        Obstacle.flag_mobile(k) ...
                        Obstacle.speed_x(2*k-1:2*k) ...
                        Obstacle.speed_y(2*k-1:2*k) ...
                        Obstacle.size_x(2*k-1:2*k) ...
                        Obstacle.size_y(2*k-1:2*k) ...
                        Obstacle.quality(k)];
    ObstacleVec         =  [ObstacleVec tempSingleObstacle];
end

msg = [EgoVehicleVec LaneMarkerFragmentVec LaneVec PositionLaneVec ObstacleVec];
n_padding = MAX_BYTES-length(msg);
assert (n_padding < MAX_BYTES);
if isempty(msg)
    Padding                 = zeros(1, MAX_BYTES, 'uint8');    
else 
    Padding                 = zeros(1, n_padding, 'uint8');  
end

msg = [ msg Padding];
Bytestream32 = typecast(msg , 'uint32');

end