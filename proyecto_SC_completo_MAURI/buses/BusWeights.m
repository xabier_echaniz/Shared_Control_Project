MPCWeights      = struct;
MPCWeights.WdT  = double(0);
MPCWeights.Wt   = double(0);
MPCWeights.Wc   = double(0);
MPCWeights.Ww   = double(0);
MPCWeights.Wh   = double(0);
MPCWeights.We   = double(0);

MPCWeightsInfo              = Simulink.Bus.createObject(MPCWeights);
MPCWeightsBus               = eval(MPCWeightsInfo.busName);
MPCWeightsBus.Description   = 'MPCWeightsBus';


