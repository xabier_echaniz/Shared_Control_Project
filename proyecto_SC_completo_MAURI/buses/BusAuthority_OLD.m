% bus creator for ego-vehicle
Authority       = struct;
Authority.value = double(0);
Authority.norm  = double(0);
Authority.ey    = double(0);
Authority.dey   = double(0);
Authority.tor   = double(0);
Authority.kaut  = double(0);
Authority.kend  = double(0);
Authority.ksat  = double(0);
Authority.head  = double(0);
Authority.headTime  = double(0);
Authority.beep      = double(0);
Authority.manual    = double(0);


AuthorityInfo              = Simulink.Bus.createObject(Authority);
AuthorityBus               = eval(AuthorityInfo.busName);
AuthorityBus.Description   = 'AuthorityBus';