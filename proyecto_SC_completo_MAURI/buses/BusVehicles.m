Vehicles               = struct;

Vehicles.frontX        = double(0);
Vehicles.frontY        = double(0);
Vehicles.frontYaw      = double(0);
Vehicles.frontSpeed    = double(0);
Vehicles.frontDistance = double(0);

Vehicles.leftX         = double(0);
Vehicles.leftY         = double(0);
Vehicles.leftYaw       = double(0);
Vehicles.leftSpeed     = double(0);
Vehicles.leftDistance  = double(0);

Vehicles.FoV           = double(0);

%Vehicles.leftTTC  = double(0);

VehiclesInfo              = Simulink.Bus.createObject(Vehicles);
VehiclesBus               = eval(VehiclesInfo.busName);
VehiclesBus.Description   = 'VehiclesBus';


