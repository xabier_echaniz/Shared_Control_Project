MPC_Constraints       = struct;
MPC_Constraints.dt    = double(0);
MPC_Constraints.t     = double(0);
MPC_Constraints.ey    = double(0);
MPC_Constraints.vy    = double(0);
MPC_Constraints.theta = double(0);
MPC_Constraints.w     = double(0);
MPC_Constraints.dpsi  = double(0);
MPC_Constraints.ay    = double(0);

MPC_ConstraintsInfo              = Simulink.Bus.createObject(MPC_Constraints);
MPC_ConstraintsBus               = eval(MPC_ConstraintsInfo.busName);
MPC_ConstraintsBus.Description   = 'MPC_ConstraintsBus';


