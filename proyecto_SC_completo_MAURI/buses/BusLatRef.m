% bus creator for ego-vehicle
LatRef       = struct;
LatRef.X     = double(zeros(31,1));
LatRef.Y     = double(zeros(31,1));
LatRef.psi   = double(zeros(31,1));
LatRef.ey    = double(zeros(31,1));
LatRef.epsi  = double(zeros(31,1));
LatRef.k     = double(zeros(31,1));
LatRef.r     = double(zeros(31,1));
LatRef.xr     = double(zeros(30,1));
LatRef.yr     = double(zeros(30,1));
LatRef.psir     = double(zeros(30,1));

LatRefInfo              = Simulink.Bus.createObject(LatRef);
LatRefBus               = eval(LatRefInfo.busName);
LatRefBus.Description   = 'LatRefBus';