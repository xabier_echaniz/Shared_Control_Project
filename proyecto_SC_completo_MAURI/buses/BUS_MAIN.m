BusDynacar();
BusEgoVeh();
BusLatStatPred();
BusLatRef();
BusLatRefOne();
BusLonCtrl();
BusLonStatPred();
BusSteering();
BusPedals();
BusMpc();
BusMpcWeights();
BusMpcConstraints();
BusVehicles();
BusAuthority();
BusDms();
BusTask();

BUS_ALL           = struct;
BUS_ALL.STATES    = Simulink.Bus.createMATLABStruct('EgoVehBus');
BUS_ALL.LATREF    = Simulink.Bus.createMATLABStruct('LatRefBus');
BUS_ALL.VEHICLES  = Simulink.Bus.createMATLABStruct('VehiclesBus');
BUS_ALL.LATPRED   = Simulink.Bus.createMATLABStruct('LatStatPredBus');
BUS_ALL.MPC       = Simulink.Bus.createMATLABStruct('MPCBus');
BUS_ALL.SWIN   = Simulink.Bus.createMATLABStruct('SteeringWheelInBus');
BUS_ALL.SWOUT  = Simulink.Bus.createMATLABStruct('SteeringWheelOutBus');
BUS_ALL.PEDALS    = Simulink.Bus.createMATLABStruct('PedalsBus');
BUS_ALL.AUTHORITY = Simulink.Bus.createMATLABStruct('AuthorityBus');
BUS_ALL.MPC_W     = Simulink.Bus.createMATLABStruct('MPC_WeightsBus');
BUS_ALL.MPC_C     = Simulink.Bus.createMATLABStruct('MPC_ConstraintsBus');
BUS_ALL.DMS       = Simulink.Bus.createMATLABStruct('DmsBus');
% BUS_ALL.TASK       = Simulink.Bus.createMATLABStruct('TaskBus');

BUS_ALL_INFO            = Simulink.Bus.createObject(BUS_ALL);
BUS_ALL_BUS             = eval(BUS_ALL_INFO.busName);
BUS_ALL_BUS.Description = 'BUS_ALL_BUS';

% BUS_ALL_DMS           = struct;
% BUS_ALL_DMS.STATES    = Simulink.Bus.createMATLABStruct('EgoVehBus');
% BUS_ALL_DMS.STEERING  = Simulink.Bus.createMATLABStruct('SteeringWheelBus');
% BUS_ALL_DMS.VEHICLES  = Simulink.Bus.createMATLABStruct('VehiclesBus');
% BUS_ALL_DMS.LATREFONE = Simulink.Bus.createMATLABStruct('LatRefOneBus');
% BUS_ALL_DMS.DMS       = Simulink.Bus.createMATLABStruct('DmsBus');
% 
% BUS_ALL_DMS_INFO            = Simulink.Bus.createObject(BUS_ALL_DMS);
% BUS_ALL_DMS_BUS             = eval(BUS_ALL_DMS_INFO.busName);
% BUS_ALL_DMS_BUS.Description = 'BUS_ALL_DMS_BUS';
