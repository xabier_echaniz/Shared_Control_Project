function [states , obstacles_out] = conversionBuses(egoVehicleBus, obstacleBus, states,BUS_PROTOTYPES)

vx_temp = double(egoVehicleBus.speed_x) ;
ax_temp = double(egoVehicleBus.acc_x );
vy_temp = double(egoVehicleBus.speed_y );
ay_temp = double(egoVehicleBus.acc_y );
%states.dpsi = egoVehicleBus. ;
Lat = double(egoVehicleBus.pos_x)*1e-6;
Lon = double(egoVehicleBus.pos_y)*1e-6;
psi_temp = double(egoVehicleBus.heading );

% X_temp(X_temp <= 5) = X_temp(X_temp <= 5) + 1e+7;
% Y_temp(Y_temp <= 5) = Y_temp(Y_temp <= 5) + 1e+7;

%unit conversion
CM2M = 1/100;         %centimeters to meters
u10deg2deg = 1/100000; %tenths of microdegrees to degrees
vx_temp = CM2M*vx_temp;
ax_temp = CM2M*ax_temp;
vy_temp = CM2M*vy_temp;
ay_temp = CM2M*ay_temp;
% X_temp = CM2M*X_temp;
% Y_temp = CM2M*Y_temp;
psi_temp = u10deg2deg*psi_temp;
rad_psi = deg2rad(psi_temp);

%conversions from east north to xy
%Heading Matrix 2D

H=[cos(rad_psi) -sin(rad_psi);
    sin(rad_psi) cos(rad_psi)];

%Position conversion
% [pos_temp] = H*[X_temp;Y_temp];
% X_temp = pos_temp(1);
% Y_temp = pos_temp(2);

%Speed conversion
[v_temp] = H\[vx_temp;vy_temp];
vx_temp = v_temp(1);
vy_temp = v_temp(2);

%Acceleration conversion
% [a_temp] = H\[ax_temp;ay_temp];
% ax_temp = a_temp(1);
% ay_temp = a_temp(2);

%reorientation heading
rad_psi = -rad_psi+pi/2;
rad_psi = atan2(sin(rad_psi),cos(rad_psi));

%final assginment
states.vx = vx_temp ;
states.ax = ax_temp ;
states.vy = vy_temp ;
states.ay = ay_temp ;
%states.dpsi = egoVehicleBus. ;
states.X = Lat ;
states.Y = Lon ;
states.psi = rad_psi ;

obstacle_x = CM2M*double(obstacleBus.pos_x');
obstacle_y = CM2M*double(obstacleBus.pos_y');
obstacles_out = [obstacle_x obstacle_y]';

end