% bus creator for ego-vehicle
Route      = struct;
Route.vx   = double(0);
Route.X    = double(0);
Route.Y    = double(0);
Route.psi  = double(0);

RouteInfo              = Simulink.Bus.createObject(Route);
RouteBus               = eval(RouteInfo.busName);
RouteBus.Description   = 'RouteBus';