function [dataSize, dataBus, bytesTest] = deserialize2(Bus_N,BusStruct, bytestream, offset)
% argName = Bus_N;
% Bus_1='EgoVehicle';
% Bus_2='LaneMarkerFragment';
% Bus_3='Lane';
% Bus_4='PositionLane';
% Bus_5='Obstacle';
MAX_POINTS = 60;
MAX_OBSTACLES = 20;
bytesTest = zeros(8,1,'uint8');
switch Bus_N
    case BusType.EGO_VEHICLE
% if(strcmp(argName,Bus_1))
        dataBus = BusStruct;
        init_msg = offset;
        init_ind = offset;
        end_ind  = offset;

        dataBus.id 			   = bytestream(offset);

        init_ind = end_ind+1;   
        end_ind = end_ind + sizeoftype(dataBus.pos_x);  
        pos_x                 = typecast( flipud(bytestream(init_ind:end_ind)) , 'int32' );
        dataBus.pos_x      	   = (pos_x(1));%Possible loss of data

        init_ind = end_ind+1;   
        end_ind = end_ind + sizeoftype(dataBus.pos_y);  
        pos_y                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int32' );
        dataBus.pos_y         = pos_y(1);%Possible loss of data

        init_ind = end_ind+1;   
        end_ind = end_ind + sizeoftype(dataBus.speed_x);  
        speed_x                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
        dataBus.speed_x        = speed_x(1);%Possible loss of data

        init_ind = end_ind+1;   
        end_ind = end_ind + sizeoftype(dataBus.speed_y);  
        speed_y                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
        dataBus.speed_y        = speed_y(1);%Possible loss of data

        init_ind = end_ind+1;   
        end_ind = end_ind + sizeoftype(dataBus.acc_x);  
        acc_x                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
        dataBus.acc_x        = acc_x(1);%Possible loss of data

        init_ind = end_ind+1;   
        end_ind = end_ind + sizeoftype(dataBus.acc_y);  
        acc_y                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
        dataBus.acc_y        = acc_y(1);%Possible loss of data

        init_ind = end_ind+1;   
        end_ind = end_ind + sizeoftype(dataBus.heading);  
        heading                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int32' );
        dataBus.heading        = heading(1);%Possible loss of data

        init_ind = end_ind+1;   
        end_ind = end_ind + sizeoftype(dataBus.e_pos_x);  
        e_pos_x                 = typecast( flipud(bytestream(init_ind:end_ind) ) , 'int16' );
        dataBus.e_pos_x        = e_pos_x(1);%Possible loss of data

        init_ind = end_ind+1;   
        end_ind = end_ind + sizeoftype(dataBus.e_pos_y);
        e_pos_y                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
        dataBus.e_pos_y        = e_pos_y(1);%Possible loss of data

        init_ind = end_ind+1;   
        end_ind = end_ind + sizeoftype(dataBus.e_speed_x);  
        e_speed_x                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
        dataBus.e_speed_x         = e_speed_x(1);%Possible loss of data

        init_ind = end_ind+1;   
        end_ind = end_ind + sizeoftype(dataBus.e_speed_y);  
        e_speed_y                = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
        dataBus.e_speed_y        = (e_speed_y(1));%Possible loss of data

        end_msg = end_ind;
        dataSize = 1+end_msg-init_msg;
        
% elseif(strcmp(argName,Bus_2))
    case BusType.LANE_MRK_FRAGMENT
        dataBus = BusStruct;
        init_msg = offset;
        init_ind = offset;
        end_ind  = offset;
        dataBus.id             = bytestream(offset);  

        init_ind = end_ind+1;   
        end_ind = end_ind + sizeoftype(dataBus.id_lane);  
        id_lane                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
        dataBus.id_lane         = id_lane(1); %Possible loss of data

        init_ind = end_ind+1;   
        end_ind = end_ind + sizeoftype(dataBus.num_points);  
        num_points              = typecast( flipud(bytestream(init_ind:end_ind) ), 'uint8');
        dataBus.num_points      = (num_points(1));  %Possible loss of data

        init_ind = end_ind +1;   
        end_ind = end_ind + 2*uint32(dataBus.num_points)*sizeoftype(dataBus.geometry(1,1));  
    %   ------------------------------------
        assert(dataBus.num_points<=MAX_POINTS)
    %   ------------------------------------
        tempGeometry                = typecast(flipud(bytestream(init_ind : end_ind)), 'int16');
        double_sz_padding           = 2*MAX_POINTS-2*dataBus.num_points;
    %   ------------------------------------
        assert(double_sz_padding<=2*MAX_POINTS)
        assert(double_sz_padding>=0)
    %   ------------------------------------
        double_padding              = zeros(1,double_sz_padding);
        geometry                    = [tempGeometry' double_padding]; 
        dataBus.geometry            = geometry(1:length(dataBus.geometry)); 

        init_ind = end_ind +1;   
        end_ind = end_ind + sizeoftype(dataBus.distance_min);  
        distance_min           = typecast(flipud(bytestream(init_ind : end_ind)), 'int16');
        dataBus.distance_min   = distance_min(1);  %Possible loss of data

        init_ind = end_ind +1;   
        end_ind = end_ind + sizeoftype(dataBus.distance_max);
        distance_max           = typecast(flipud(bytestream(init_ind : end_ind)), 'int16');
        dataBus.distance_max   = distance_max(1);  %Possible loss of data

        init_ind = end_ind +1;   
        end_ind = end_ind + sizeoftype(dataBus.type);
        type                   = typecast(flipud(bytestream(init_ind : end_ind)), 'uint8');
        dataBus.type           = type(1);  %Possible loss of data

        init_ind = end_ind +1;   
        end_ind = end_ind + sizeoftype(dataBus.quality);
        quality                = typecast(flipud(bytestream(init_ind : end_ind)), 'uint8');
        dataBus.quality        = quality(1);  %Possible loss of data

        end_msg = end_ind;
        dataSize = 1+end_msg-init_msg;
    
% elseif(strcmp(argName,Bus_3))
    case BusType.LANE
        
        dataBus = BusStruct;
        init_msg = offset;
        init_ind = offset;
        end_ind  = offset;
        dataBus.id             = bytestream(offset);  

        init_ind = end_ind+1;
        end_ind = end_ind + sizeoftype(dataBus.id_lane);
        id_lane                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
        dataBus.id_lane         = id_lane(1); %Possible loss of data

        init_ind = end_ind+1;
        end_ind = end_ind + sizeoftype(dataBus.id_left_line);
        id_left_line                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
        dataBus.id_left_line         = id_left_line(1); %Possible loss of data
        
        init_ind = end_ind+1;
        end_ind = end_ind + sizeoftype(dataBus.id_right_line);
        id_right_line                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
        dataBus.id_right_line         = id_right_line(1); %Possible loss of data

        init_ind = end_ind+1;
        end_ind = end_ind + sizeoftype(dataBus.id_left_sibling);
        id_left_sibling                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
        dataBus.id_left_sibling         = id_left_sibling(1); %Possible loss of data

        init_ind = end_ind+1;
        end_ind = end_ind + sizeoftype(dataBus.id_right_sibling);
        id_right_sibling                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
        dataBus.id_right_sibling         = id_right_sibling(1); %Possible loss of data

        init_ind = end_ind+1;
        end_ind = end_ind + sizeoftype(dataBus.quality);
        quality                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'uint8' );
        dataBus.quality         = quality(1); %Possible loss of data    

        end_msg = end_ind;
        dataSize = 1+end_msg-init_msg;
    
% elseif(strcmp(argName,Bus_4))
    case BusType.POSITION_LANE
        
        dataBus = BusStruct;
        init_msg = offset;
        init_ind = offset;
        end_ind  = offset;
        dataBus.id             = bytestream(offset);  

        init_ind = end_ind+1;
        end_ind = end_ind + sizeoftype(dataBus.id_lane);
        id_lane                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
        dataBus.id_lane         = id_lane(1); %Possible loss of data

        init_ind = end_ind+1;
        end_ind = end_ind + sizeoftype(dataBus.width);
        width                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
        dataBus.width         = width(1); %Possible loss of data
        
        init_ind = end_ind+1;
        end_ind = end_ind + sizeoftype(dataBus.lateral_offset);
        lateral_offset                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
        dataBus.lateral_offset         = lateral_offset(1); %Possible loss of data

        init_ind = end_ind+1;
        end_ind = end_ind + sizeoftype(dataBus.angle);
        angle                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int32' );
        dataBus.angle         = angle(1); %Possible loss of data

        init_ind = end_ind+1;
        end_ind = end_ind + sizeoftype(dataBus.quality);
        quality                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'uint8' );
        dataBus.quality         = quality(1); %Possible loss of data

        end_msg = end_ind;
        dataSize = 1+end_msg-init_msg;
        
    
% elseif(strcmp(argName,Bus_5))
    case BusType.OBSTACLE

        dataBus = BusStruct;
        init_msg = offset;
        init_ind = offset;
        end_ind  = offset;
        dataBus.id             = bytestream(offset);  

        init_ind = end_ind+1;
        end_ind = end_ind + sizeoftype(dataBus.number_obstacles);
        number_obstacles            = typecast( flipud(bytestream(init_ind:end_ind) ), 'uint8' );
        dataBus.number_obstacles    = number_obstacles(1); %Possible loss of data

        assert(dataBus.number_obstacles<=MAX_OBSTACLES)

        for k = 1:dataBus.number_obstacles

            init_ind = end_ind+1;
            end_ind = end_ind + sizeoftype(dataBus.obstacle_id);
            obstacle_id           = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
            dataBus.obstacle_id(k)      = obstacle_id(1); %Possible loss of data

            init_ind = end_ind+1;
            end_ind = end_ind + sizeoftype(dataBus.type);
            type                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'uint8' );
            dataBus.type(k)      = type(1); %Possible loss of data

            init_ind = end_ind+1;
            end_ind = end_ind + sizeoftype(dataBus.pos_x);
            pos_x                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
            dataBus.pos_x(k)      = pos_x(1); %Possible loss of data

            init_ind = end_ind+1;
            end_ind = end_ind + sizeoftype(dataBus.pos_y);
            pos_y                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
            dataBus.pos_y(k)      = pos_y(1); %Possible loss of data

            init_ind = end_ind+1;
            end_ind = end_ind + sizeoftype(dataBus.flag_mobile);
            flag_mobile             = typecast( flipud(bytestream(init_ind:end_ind) ), 'uint8' );
            dataBus.flag_mobile(k)      = flag_mobile(1); %Possible loss of data

            init_ind = end_ind+1;
            end_ind = end_ind + sizeoftype(dataBus.speed_x);
            speed_x                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
            dataBus.speed_x(k)      = speed_x(1); %Possible loss of data

            init_ind = end_ind+1;
            end_ind = end_ind + sizeoftype(dataBus.speed_y);
            speed_y                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
            dataBus.speed_y(k)      = speed_y(1); %Possible loss of data

            init_ind = end_ind+1;
            end_ind = end_ind + sizeoftype(dataBus.size_x);
            size_x                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
            dataBus.size_x(k)      = size_x(1); %Possible loss of data

            init_ind = end_ind+1;
            end_ind = end_ind + sizeoftype(dataBus.size_y);
            size_y                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'int16' );
            dataBus.size_y(k)      = size_y(1); %Possible loss of data

            init_ind = end_ind+1;
            end_ind = end_ind + sizeoftype(dataBus.quality);
            quality                 = typecast( flipud(bytestream(init_ind:end_ind) ), 'uint8' );
            dataBus.quality(k)      = quality(1); %Possible loss of data

        end

        %Putting zeros in everything that its not an obstacle
        assert(dataBus.number_obstacles<=MAX_OBSTACLES)
        ZERO_SIZE = uint8(MAX_OBSTACLES) - dataBus.number_obstacles;
        
        dataBus.obstacle_id(dataBus.number_obstacles+1:MAX_OBSTACLES) = zeros(1,ZERO_SIZE, 'uint16');
        dataBus.type(dataBus.number_obstacles+1:MAX_OBSTACLES) = zeros(1,ZERO_SIZE, 'uint16');
        dataBus.pos_x(dataBus.number_obstacles+1:MAX_OBSTACLES) = zeros(1,ZERO_SIZE, 'uint16');
        dataBus.pos_y(dataBus.number_obstacles+1:MAX_OBSTACLES) = zeros(1,ZERO_SIZE, 'uint16');
        dataBus.flag_mobile(dataBus.number_obstacles+1:MAX_OBSTACLES) = zeros(1,ZERO_SIZE, 'uint16');
        dataBus.speed_x(dataBus.number_obstacles+1:MAX_OBSTACLES) = zeros(1,ZERO_SIZE, 'uint16');
        dataBus.speed_y(dataBus.number_obstacles+1:MAX_OBSTACLES) = zeros(1,ZERO_SIZE, 'uint16');
        dataBus.size_x(dataBus.number_obstacles+1:MAX_OBSTACLES) = zeros(1,ZERO_SIZE, 'uint16');
        dataBus.size_y(dataBus.number_obstacles+1:MAX_OBSTACLES) = zeros(1,ZERO_SIZE, 'uint16');
        dataBus.quality(dataBus.number_obstacles+1:MAX_OBSTACLES) = zeros(1,ZERO_SIZE, 'uint16');

        end_msg = end_ind;
        dataSize = 1+end_msg-init_msg;
        
% else
    otherwise
        dataBus=[];
        dataSize=[];
end