MPC   = struct;
MPC.status   = double(0);
MPC.kkt      = double(0);
MPC.cpuTime  = double(0);
MPC.nIter    = double(0);
MPC.objValue = double(0);
MPC.control  = double(0);
MPC.dT       = double(0);

MPCInfo              = Simulink.Bus.createObject(MPC);
MPCBus               = eval(MPCInfo.busName);
MPCBus.Description   = 'MPCBus';


