% bus creator for ego-vehicle
LatRefOne       = struct;
LatRefOne.X     = double(0);
LatRefOne.Y     = double(0);
LatRefOne.psi   = double(0);
LatRefOne.ey    = double(0);
LatRefOne.epsi  = double(0);
LatRefOne.k     = double(0);

LatRefOneInfo              = Simulink.Bus.createObject(LatRefOne);
LatRefOneBus               = eval(LatRefOneInfo.busName);
LatRefOneBus.Description   = 'LatRefBusOne';