% bus creator for ego-vehicle
LonCtrl                  = struct;
LonCtrl.thr              = double(0);
LonCtrl.brk              = double(0);

LonCtrlInfo              = Simulink.Bus.createObject(LonCtrl);
LonCtrlBus               = eval(LonCtrlInfo.busName);
LonCtrlBus.Description   = 'LonCtrlBus';