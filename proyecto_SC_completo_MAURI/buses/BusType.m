classdef BusType < Simulink.IntEnumType
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    enumeration
        
        EGO_VEHICLE         (1),
        LANE_MRK_FRAGMENT   (2),
        LANE                (3),
        POSITION_LANE       (4),
        OBSTACLE            (5)
    end
    
    
    
%     properties
%         Property1
%     end
%     
%     methods
%         function obj = untitled2(inputArg1,inputArg2)
%             %UNTITLED2 Construct an instance of this class
%             %   Detailed explanation goes here
%             obj.Property1 = inputArg1 + inputArg2;
%         end
%         
%         function outputArg = method1(obj,inputArg)
%             %METHOD1 Summary of this method goes here
%             %   Detailed explanation goes here
%             outputArg = obj.Property1 + inputArg;
%         end
%     end
end

