% clc; clear all; close all;
clc; clear all;
clc; clearvars -except k i amp n; close all;
root = getenv('Dynacar_v_2_1');
cfg = Simulink.fileGenControl('getConfig');

cfg.CacheFolder = fullfile(eval(['pwd']),'slprj/generatedCache');
cfg.CodeGenFolder = fullfile(eval(['pwd']),'slprj/generatedCode');
Simulink.fileGenControl('setConfig', 'config', cfg,'createDir',true);

addpath(genpath([cd,'\models']));
addpath(genpath([cd,'\modules']));
addpath(genpath([cd,'\dynacar']));
addpath(genpath([cd,'\mex']));
addpath(genpath([cd,'\fuzzy']));
addpath(genpath([cd,'\map']));
addpath(genpath([cd,'\..\..\images']));

run([cd,'\buses\BUS_MAIN'])

routeR      = load('right.mat');
routeL      = load('left.mat');
map         = load('lanes.mat');

XInit       = routeR.X(40);
YInit       = routeR.Y(40);
psiInit     = routeR.psi(40);

INSTANCE    = 0;

LAUNCH          = double([cd,'\modules\dynacar\carE_Launch.txt']);
LAUNCH          = [LAUNCH zeros(1,100)];
LAUNCH(101:end) = [];
LAUNCH          = int8(LAUNCH);

INIPOS    = zeros([1 28]);  
INIVEL    = zeros([1 28]);

INIPOS(1) = XInit;   % X position
INIPOS(2) = YInit;   % Y position
INIPOS(3) = 0.1;     % Z position
INIPOS(4) = 0;       % Roll
INIPOS(5) = 0;       % Pitch
INIPOS(6) = psiInit; % Yaw

V_inicial = 5;       % Units [m/s]
A_inicial = 0;       % Units [m/s^2]
R_wheel   = 0.44528; % Units [m]
Heading   = psiInit; % Units [rad]
WA        = 0;       % Units [rad]
W_wheel = (V_inicial*cos(Heading))/R_wheel;
 
INIVEL(1) = V_inicial*cos(Heading);
INIVEL(2) = V_inicial*sin(Heading);
INIVEL(11:16) = W_wheel;

%% Motor parameters
J = 0.1;
b = 0.65;
F = 0;

rate = 0.01;
ts   = 0.001;
modeTest = 1;

STEERING_REAL = Simulink.Variant('Mode_steering == 1');
STEERING_SIM  = Simulink.Variant('Mode_steering == 0');
PEDALS_REAL   = Simulink.Variant('Mode_pedals == 1');
PEDALS_SIM    = Simulink.Variant('Mode_pedals == 0');


EXP_CONCENTRATED = Simulink.Variant('exp_mode == 0');
EXP_DISTRACTED  = Simulink.Variant('exp_mode == 1');


Mode_steering = 0;
Mode_pedals = 0;
exp_mode = 0; % 0 - concentrado, 1 - distraido
k_loha = 12;

% system(fullfile(pwd,'map','exp_harold.lnk'),'-echo');
% path = fullfile(pwd,'map','exp_harold.bat');
% uac = actxserver('Shell.Application');
% uac.ShellExecute(path, "ADM", "", "runas", 0);