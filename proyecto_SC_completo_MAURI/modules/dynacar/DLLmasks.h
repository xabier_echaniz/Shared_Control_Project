#ifndef __DLLMASKS__
#define __DLLMASKS__

#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__)
    #  if defined( BUILDING )
    #    define DLLEXPORT   __declspec(dllexport)
    #  else
    #    define DLLEXPORT   __declspec(dllimport)
    #  endif
#else
    #  define DLLEXPORT
#endif

#include <stdio.h>

DLLEXPORT int Start(const char* launch, double *z0, double *zp0, double *instance);

DLLEXPORT void TimeStep(double *arr, double *instance, double* output, char* buffer);

DLLEXPORT void CleanMemory();

DLLEXPORT struct sistema* getSistPointer();

DLLEXPORT double get_time();

#endif
