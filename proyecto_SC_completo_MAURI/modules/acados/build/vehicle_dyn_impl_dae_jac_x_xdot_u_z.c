/* This file was automatically generated by CasADi.
   The CasADi copyright holders make no ownership claim of its contents. */
#ifdef __cplusplus
extern "C" {
#endif

/* How to prefix internal symbols */
#ifdef CASADI_CODEGEN_PREFIX
  #define CASADI_NAMESPACE_CONCAT(NS, ID) _CASADI_NAMESPACE_CONCAT(NS, ID)
  #define _CASADI_NAMESPACE_CONCAT(NS, ID) NS ## ID
  #define CASADI_PREFIX(ID) CASADI_NAMESPACE_CONCAT(CODEGEN_PREFIX, ID)
#else
  #define CASADI_PREFIX(ID) vehicle_dyn_impl_dae_jac_x_xdot_u_z_ ## ID
#endif

#include <math.h>

#ifndef casadi_real
#define casadi_real double
#endif

#ifndef casadi_int
#define casadi_int int
#endif

/* Add prefix to internal symbols */
#define casadi_f0 CASADI_PREFIX(f0)
#define casadi_s0 CASADI_PREFIX(s0)
#define casadi_s1 CASADI_PREFIX(s1)
#define casadi_s2 CASADI_PREFIX(s2)
#define casadi_s3 CASADI_PREFIX(s3)
#define casadi_s4 CASADI_PREFIX(s4)
#define casadi_s5 CASADI_PREFIX(s5)
#define casadi_s6 CASADI_PREFIX(s6)
#define casadi_s7 CASADI_PREFIX(s7)

/* Symbol visibility in DLLs */
#ifndef CASADI_SYMBOL_EXPORT
  #if defined(_WIN32) || defined(__WIN32__) || defined(__CYGWIN__)
    #if defined(STATIC_LINKED)
      #define CASADI_SYMBOL_EXPORT
    #else
      #define CASADI_SYMBOL_EXPORT __declspec(dllexport)
    #endif
  #elif defined(__GNUC__) && defined(GCC_HASCLASSVISIBILITY)
    #define CASADI_SYMBOL_EXPORT __attribute__ ((visibility ("default")))
  #else
    #define CASADI_SYMBOL_EXPORT
  #endif
#endif

static const casadi_int casadi_s0[15] = {11, 1, 0, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
static const casadi_int casadi_s1[5] = {1, 1, 0, 1, 0};
static const casadi_int casadi_s2[9] = {5, 1, 0, 5, 0, 1, 2, 3, 4};
static const casadi_int casadi_s3[7] = {3, 1, 0, 3, 0, 1, 2};
static const casadi_int casadi_s4[43] = {16, 11, 0, 0, 0, 2, 9, 16, 22, 24, 25, 26, 28, 29, 0, 1, 0, 1, 6, 7, 11, 12, 15, 0, 1, 3, 6, 7, 11, 12, 2, 3, 6, 11, 12, 15, 6, 7, 6, 14, 8, 9, 9};
static const casadi_int casadi_s5[25] = {16, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
static const casadi_int casadi_s6[5] = {16, 1, 0, 1, 10};
static const casadi_int casadi_s7[25] = {16, 5, 0, 5, 8, 10, 15, 17, 3, 5, 9, 11, 15, 5, 12, 15, 9, 13, 3, 5, 11, 14, 15, 4, 15};

/* vehicle_dyn_impl_dae_jac_x_xdot_u_z:(i0[11],i1[11],i2,i3[5],i4[3])->(o0[16x11,29nz],o1[16x11,11nz],o2[16x1,1nz],o3[16x5,17nz]) */
static int casadi_f0(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, int mem) {
  casadi_real a0, a1, a10, a11, a12, a13, a14, a15, a16, a2, a3, a4, a5, a6, a7, a8, a9;
  a0=arg[0]? arg[0][3] : 0;
  a1=arg[0]? arg[0][2] : 0;
  a2=sin(a1);
  a2=(a0*a2);
  a3=arg[0]? arg[0][4] : 0;
  a4=cos(a1);
  a4=(a3*a4);
  a2=(a2+a4);
  a2=(-a2);
  if (res[0]!=0) res[0][0]=a2;
  a2=cos(a1);
  a2=(a0*a2);
  a4=sin(a1);
  a4=(a3*a4);
  a2=(a2-a4);
  if (res[0]!=0) res[0][1]=a2;
  a2=cos(a1);
  if (res[0]!=0) res[0][2]=a2;
  a2=sin(a1);
  if (res[0]!=0) res[0][3]=a2;
  a2=arg[4]? arg[4][2] : 0;
  a4=1.;
  a5=arg[0]? arg[0][7] : 0;
  a5=(a2*a5);
  a5=(a4-a5);
  a6=(a2/a5);
  a7=arg[0]? arg[0][6] : 0;
  a8=cos(a7);
  a9=(a6*a8);
  a9=(-a9);
  if (res[0]!=0) res[0][4]=a9;
  a9=sin(a7);
  if (res[0]!=0) res[0][5]=a9;
  a9=94000.;
  a10=1.3999999999999999e+000;
  a11=arg[0]? arg[0][5] : 0;
  a12=(a10*a11);
  a12=(a3+a12);
  a13=1.0000000000000000e-003;
  a14=(a0+a13);
  a12=(a12/a14);
  a12=(a12/a14);
  a12=(a9*a12);
  if (res[0]!=0) res[0][6]=a12;
  a12=118000.;
  a15=1.6499999999999999e+000;
  a15=(a15*a11);
  a15=(a15-a3);
  a12=(a12*a15);
  a13=(a0+a13);
  a12=(a12/a13);
  a12=(a12/a13);
  a12=(-a12);
  if (res[0]!=0) res[0][7]=a12;
  a12=6.0606060606060606e-004;
  a15=-1650.;
  a16=(a15*a11);
  a16=(a12*a16);
  if (res[0]!=0) res[0][8]=a16;
  a16=sin(a1);
  a16=(-a16);
  if (res[0]!=0) res[0][9]=a16;
  a1=cos(a1);
  if (res[0]!=0) res[0][10]=a1;
  if (res[0]!=0) res[0][11]=a11;
  a11=sin(a7);
  a1=(a6*a11);
  if (res[0]!=0) res[0][12]=a1;
  a1=cos(a7);
  if (res[0]!=0) res[0][13]=a1;
  a1=(a9/a14);
  a1=(-a1);
  if (res[0]!=0) res[0][14]=a1;
  a1=-118000.;
  a1=(a1/a13);
  if (res[0]!=0) res[0][15]=a1;
  if (res[0]!=0) res[0][16]=a4;
  if (res[0]!=0) res[0][17]=a3;
  if (res[0]!=0) res[0][18]=a4;
  a14=(a10/a14);
  a14=(a9*a14);
  a14=(-a14);
  if (res[0]!=0) res[0][19]=a14;
  a14=194700.;
  a14=(a14/a13);
  if (res[0]!=0) res[0][20]=a14;
  a15=(a15*a0);
  a15=(a12*a15);
  if (res[0]!=0) res[0][21]=a15;
  a15=sin(a7);
  a15=(a0*a15);
  a14=cos(a7);
  a14=(a3*a14);
  a15=(a15+a14);
  a15=(a6*a15);
  if (res[0]!=0) res[0][22]=a15;
  a15=cos(a7);
  a15=(a0*a15);
  a7=sin(a7);
  a7=(a3*a7);
  a15=(a15-a7);
  if (res[0]!=0) res[0][23]=a15;
  a0=(a0*a8);
  a3=(a3*a11);
  a0=(a0-a3);
  a6=(a6/a5);
  a6=(a6*a2);
  a0=(a0*a6);
  a0=(-a0);
  if (res[0]!=0) res[0][24]=a0;
  a0=-1.1402508551881414e-001;
  if (res[0]!=0) res[0][25]=a0;
  if (res[0]!=0) res[0][26]=a4;
  a0=arg[3]? arg[3][2] : 0;
  a6=1.0000000000000001e-001;
  a0=(a0/a6);
  a0=(-a0);
  if (res[0]!=0) res[0][27]=a0;
  a0=10.;
  if (res[0]!=0) res[0][28]=a0;
  a6=-1.;
  if (res[1]!=0) res[1][0]=a6;
  if (res[1]!=0) res[1][1]=a6;
  if (res[1]!=0) res[1][2]=a6;
  if (res[1]!=0) res[1][3]=a6;
  if (res[1]!=0) res[1][4]=a6;
  if (res[1]!=0) res[1][5]=a6;
  if (res[1]!=0) res[1][6]=a6;
  if (res[1]!=0) res[1][7]=a6;
  if (res[1]!=0) res[1][8]=a6;
  if (res[1]!=0) res[1][9]=a6;
  if (res[1]!=0) res[1][10]=a6;
  a2=arg[4]? arg[4][0] : 0;
  if (res[2]!=0) res[2][0]=a2;
  a2=arg[3]? arg[3][3] : 0;
  a5=sin(a2);
  a5=(a12*a5);
  a5=(-a5);
  if (res[3]!=0) res[3][0]=a5;
  a5=3.0921459492888067e-004;
  a3=cos(a2);
  a3=(a10*a3);
  a3=(a5*a3);
  if (res[3]!=0) res[3][1]=a3;
  a3=9.0909090909090905e-003;
  if (res[3]!=0) res[3][2]=a3;
  if (res[3]!=0) res[3][3]=a6;
  a3=cos(a2);
  a3=(a12*a3);
  if (res[3]!=0) res[3][4]=a3;
  a3=-5.1020408163265311e-004;
  if (res[3]!=0) res[3][5]=a3;
  if (res[3]!=0) res[3][6]=a6;
  if (res[3]!=0) res[3][7]=a12;
  a3=arg[0]? arg[0][9] : 0;
  a0=(a0*a3);
  a0=(-a0);
  if (res[3]!=0) res[3][8]=a0;
  if (res[3]!=0) res[3][9]=a6;
  a0=1650.;
  a3=arg[4]? arg[4][1] : 0;
  a11=(a0*a3);
  a8=sin(a2);
  a11=(a11*a8);
  a8=arg[3]? arg[3][0] : 0;
  a15=cos(a2);
  a15=(a8*a15);
  a11=(a11+a15);
  a11=(a12*a11);
  a11=(-a11);
  if (res[3]!=0) res[3][10]=a11;
  a11=(a0*a3);
  a15=cos(a2);
  a11=(a11*a15);
  a15=sin(a2);
  a15=(a8*a15);
  a11=(a11-a15);
  a10=(a10*a11);
  a5=(a5*a10);
  if (res[3]!=0) res[3][11]=a5;
  if (res[3]!=0) res[3][12]=a9;
  if (res[3]!=0) res[3][13]=a6;
  a0=(a0*a3);
  a3=cos(a2);
  a0=(a0*a3);
  a2=sin(a2);
  a8=(a8*a2);
  a0=(a0-a8);
  a12=(a12*a0);
  if (res[3]!=0) res[3][14]=a12;
  if (res[3]!=0) res[3][15]=a4;
  if (res[3]!=0) res[3][16]=a6;
  return 0;
}

CASADI_SYMBOL_EXPORT int vehicle_dyn_impl_dae_jac_x_xdot_u_z(const casadi_real** arg, casadi_real** res, casadi_int* iw, casadi_real* w, int mem){
  return casadi_f0(arg, res, iw, w, mem);
}

CASADI_SYMBOL_EXPORT int vehicle_dyn_impl_dae_jac_x_xdot_u_z_alloc_mem(void) {
  return 0;
}

CASADI_SYMBOL_EXPORT int vehicle_dyn_impl_dae_jac_x_xdot_u_z_init_mem(int mem) {
  return 0;
}

CASADI_SYMBOL_EXPORT void vehicle_dyn_impl_dae_jac_x_xdot_u_z_free_mem(int mem) {
}

CASADI_SYMBOL_EXPORT int vehicle_dyn_impl_dae_jac_x_xdot_u_z_checkout(void) {
  return 0;
}

CASADI_SYMBOL_EXPORT void vehicle_dyn_impl_dae_jac_x_xdot_u_z_release(int mem) {
}

CASADI_SYMBOL_EXPORT void vehicle_dyn_impl_dae_jac_x_xdot_u_z_incref(void) {
}

CASADI_SYMBOL_EXPORT void vehicle_dyn_impl_dae_jac_x_xdot_u_z_decref(void) {
}

CASADI_SYMBOL_EXPORT casadi_int vehicle_dyn_impl_dae_jac_x_xdot_u_z_n_in(void) { return 5;}

CASADI_SYMBOL_EXPORT casadi_int vehicle_dyn_impl_dae_jac_x_xdot_u_z_n_out(void) { return 4;}

CASADI_SYMBOL_EXPORT casadi_real vehicle_dyn_impl_dae_jac_x_xdot_u_z_default_in(casadi_int i){
  switch (i) {
    default: return 0;
  }
}

CASADI_SYMBOL_EXPORT const char* vehicle_dyn_impl_dae_jac_x_xdot_u_z_name_in(casadi_int i){
  switch (i) {
    case 0: return "i0";
    case 1: return "i1";
    case 2: return "i2";
    case 3: return "i3";
    case 4: return "i4";
    default: return 0;
  }
}

CASADI_SYMBOL_EXPORT const char* vehicle_dyn_impl_dae_jac_x_xdot_u_z_name_out(casadi_int i){
  switch (i) {
    case 0: return "o0";
    case 1: return "o1";
    case 2: return "o2";
    case 3: return "o3";
    default: return 0;
  }
}

CASADI_SYMBOL_EXPORT const casadi_int* vehicle_dyn_impl_dae_jac_x_xdot_u_z_sparsity_in(casadi_int i) {
  switch (i) {
    case 0: return casadi_s0;
    case 1: return casadi_s0;
    case 2: return casadi_s1;
    case 3: return casadi_s2;
    case 4: return casadi_s3;
    default: return 0;
  }
}

CASADI_SYMBOL_EXPORT const casadi_int* vehicle_dyn_impl_dae_jac_x_xdot_u_z_sparsity_out(casadi_int i) {
  switch (i) {
    case 0: return casadi_s4;
    case 1: return casadi_s5;
    case 2: return casadi_s6;
    case 3: return casadi_s7;
    default: return 0;
  }
}

CASADI_SYMBOL_EXPORT int vehicle_dyn_impl_dae_jac_x_xdot_u_z_work(casadi_int *sz_arg, casadi_int* sz_res, casadi_int *sz_iw, casadi_int *sz_w) {
  if (sz_arg) *sz_arg = 5;
  if (sz_res) *sz_res = 4;
  if (sz_iw) *sz_iw = 0;
  if (sz_w) *sz_w = 0;
  return 0;
}


#ifdef __cplusplus
} /* extern "C" */
#endif
