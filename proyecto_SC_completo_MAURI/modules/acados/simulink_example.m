%% Simulink example
%

%% Run minimal example
%
acados_env_variables_windows;
minimal_example_ocp;

%% Simulink options
simulink_opts = get_acados_simulink_opts;
simulink_opts.outputs.utraj = 1;
simulink_opts.outputs.xtraj = 1;
simulink_opts.inputs.cost_W_0 = 1;
simulink_opts.inputs.cost_W = 1;
simulink_opts.inputs.cost_W_e = 1;
simulink_opts.samplingtime = '-1';

%% Render templated Code for the model contained in ocp object

ocp.generate_c_code(simulink_opts);

%% Compile Sfunctions
cd c_generated_code

make_sfun; % ocp solver
% make_sfun_sim; % integrator

% %% Copy Simulink example blocks into c_generated_code
% source_folder = fullfile(pwd, '..');
% target_folder = pwd;
% copyfile( fullfile(source_folder, 'simulink_model_integrator.slx'), target_folder );
% copyfile( fullfile(source_folder, 'simulink_model_closed_loop.slx'), target_folder );
% 
% 
% %% Open Simulink example blocks
% open_system(fullfile(target_folder, 'simulink_model_integrator'))
% open_system(fullfile(target_folder, 'simulink_model_closed_loop'))
% 
% %%
% disp('Press play in Simulink!');
