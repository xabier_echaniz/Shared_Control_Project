%
% Copyright 2019 Gianluca Frison, Dimitris Kouzoupis, Robin Verschueren,
% Andrea Zanelli, Niels van Duijkeren, Jonathan Frey, Tommaso Sartor,
% Branimir Novoselnik, Rien Quirynen, Rezart Qelibari, Dang Doan,
% Jonas Koenemann, Yutao Chen, Tobias Schöls, Jonas Schlagenhauf, Moritz Diehl
%
% This file is part of acados.
%
% The 2-Clause BSD License 
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.;
%

%% test of native matlab interface
clear all

% model_path = fullfile(pwd,'..','pendulum_on_cart_model');
% addpath(model_path)

check_acados_requirements()

%% discretization
N = 30;
T = 1.5-0.04; % time horizon length
x0 = zeros(11,1);

nlp_solver = 'sqp_rti'; % sqp, sqp_rti
qp_solver = 'partial_condensing_hpipm';
    % full_condensing_hpipm, partial_condensing_hpipm, full_condensing_qpoases
qp_solver_cond_N = 5; % for partial condensing
% integrator type
sim_method = 'irk'; % erk, irk, irk_gnsf

%% model dynamics
model = vehicle_model;
nx = model.nx;
nu = model.nu;
ny = size(model.cost_expr_y, 1);      % used in simulink example
ny_e = size(model.cost_expr_y_e, 1);

%% model to create the solver
ocp_model = acados_ocp_model();
model_name = 'vehicle';

%% acados ocp model
ocp_model.set('name', model_name);
ocp_model.set('T', T);


% symbolics
ocp_model.set('sym_x', model.sym_x);
ocp_model.set('sym_u', model.sym_u);
ocp_model.set('sym_xdot', model.sym_xdot)
ocp_model.set('sym_z', model.sym_z);
ocp_model.set('sym_p', model.sym_p);

% cost
ocp_model.set('cost_expr_ext_cost', model.expr_ext_cost);
ocp_model.set('cost_expr_ext_cost_e', model.expr_ext_cost_e);

% dynamics
if (strcmp(sim_method, 'erk'))
    ocp_model.set('dyn_type', 'explicit');
    ocp_model.set('dyn_expr_f', model.expr_f_expl);
else % irk irk_gnsf
    ocp_model.set('dyn_type', 'implicit');
    ocp_model.set('dyn_expr_f', model.expr_f_impl);
end

% constraints
ocp_model.set('constr_type', 'auto');
ocp_model.set('constr_expr_h', model.expr_h);
ay_max = 1;
dpsi_max = 0.5;
ey_max = 2;
T_max = 3;
dT_max = 3;
ocp_model.set('constr_lh', [-dT_max -dpsi_max -ey_max -T_max -ay_max]); % lower bound on h
ocp_model.set('constr_uh', [dT_max dpsi_max ey_max T_max ay_max]);  % upper bound on h
ocp_model.set('constr_x0', x0);
ocp_model.set('constr_Jsbx', [1 0 0; 0 1 0; 0 0 1]);
ocp_model.set('constr_Jsh', 1);
ocp_model.set('cost_zl', 1e1*ones(4,1));
ocp_model.set('cost_zu', 1e1*ones(4,1));
ocp_model.set('cost_Zl', 1e2*eye(4));
ocp_model.set('cost_Zu', 1e2*eye(4));
% ... see ocp_model.model_struct to see what other fields can be set

%% acados ocp set opts
ocp_opts = acados_ocp_opts();
ocp_opts.set('param_scheme_N', N);
ocp_opts.set('nlp_solver', nlp_solver);
ocp_opts.set('sim_method', sim_method);
ocp_opts.set('qp_solver', qp_solver);
ocp_opts.set('qp_solver_cond_N', qp_solver_cond_N);

N1 = 1;
N2 = N - N1;
time_steps = [( 0.01 * ones(N1,1)); 0.05 * ones(N2,1)];
ocp_opts.set('time_steps', time_steps);

% ... see ocp_opts.opts_struct to see what other fields can be set

%% create ocp solver
ocp = acados_ocp(ocp_model, ocp_opts);

x_traj_init = zeros(nx, N+1);
u_traj_init = zeros(nu, N);

%% call ocp solver
% update initial state
ocp.set('constr_x0', x0);

% set trajectory initialization
ocp.set('init_x', x_traj_init);
ocp.set('init_u', u_traj_init);
ocp.set('init_pi', zeros(nx, N))

% change values for specific shooting node using:
%   ocp.set('field', value, optional: stage_index)
ocp.set('constr_lbx', x0, 0)
% solve
ocp.solve();
% get solution
utraj = ocp.get('u');
xtraj = ocp.get('x');

status = ocp.get('status'); % 0 - success
ocp.print('stat')

%% Plots
% ts = linspace(0, T, N+1);
% figure; hold on;
% States = {'X', 'Y', 'PSI', 'vx'};
% for i=1:length(States)
%     subplot(length(States), 1, i);
%     plot(ts, xtraj(i,:)); grid on;
%     ylabel(States{i});
%     xlabel('t [s]')
% end

% figure
% stairs(ts, [utraj'; utraj(end)])
% ylabel('F [N]')
% xlabel('t [s]')
% grid on

%% go embedded
% to generate templated C code
% ocp.generate_c_code;
