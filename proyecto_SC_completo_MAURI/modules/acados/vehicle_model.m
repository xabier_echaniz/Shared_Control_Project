%
% Copyright 2019 Gianluca Frison, Dimitris Kouzoupis, Robin Verschueren,
% Andrea Zanelli, Niels van Duijkeren, Jonathan Frey, Tommaso Sartor,
% Branimir Novoselnik, Rien Quirynen, Rezart Qelibari, Dang Doan,
% Jonas Koenemann, Yutao Chen, Tobias Schöls, Jonas Schlagenhauf, Moritz Diehl
%
% This file is part of acados.
%
% The 2-Clause BSD License
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
%
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.;
%

function model = vehicle_model()

import casadi.*

%% system dimensions
nx = 11;
nu = 1;

%% system parameters
%% Motor
J    = 0.1;    % Motor Inertia [kg.m^2]
b    = 0.65;    % Motor Damping [N.m^2/s]
ks   = 8.77; %14.5414;  % Steering ratio
%% Vehicle
% W_tot = 1.540 % Vehicle total width [m]
% L_tot = 4.299 % Vehicle total length [m]
L    = 3.05;   % Vehicle wheelbase [m]
lf   = 1.40;    % Vehicle front axle to CG [m]
lr   = L-lf;    % Vehicle rear axle to CG [m]
m    = 1650; % Vehicle mass [Kg] 
Iz   = 3234;    % Vehicle moment of inertia about z [kg.m^2]
Cf   = 94000;   % Tire front cornering stiffness
Cr   = 118000;   % Tire rear cornering stiffness
%% Online Data/Parameter
lambda = SX.sym('lambda'); % Level of haptic authority
ax     = SX.sym('ax');     % Longitudinal acceleration
rho    = SX.sym('rho');    % Road curvature

%% Named symbolic variables

%% Differential States
X     = SX.sym('X');    % X position (absolute) [m]
Y     = SX.sym('Y');    % Y position (absolute) [m]
PSI   = SX.sym('PSI');  % Heading angle [rad]
vx    = SX.sym('vx');   % Longitudinal speed [m/s]
vy    = SX.sym('vy');   % Lateral speed [m/s]
dpsi  = SX.sym('dpsi'); % Yaw rate [rad/s]
epsi  = SX.sym('epsi'); % Angular error [rad]
ey    = SX.sym('ey');   % Lateral error [m]
theta = SX.sym('theta');   % Steering wheel angle [rad]
w     = SX.sym('w');    % Steering wheel angular velocity [rad/s]
T     = SX.sym('T');    % Sterring wheel control torque [Nm]

%% Control Inputs
dT   = SX.sym('dT');   % Sterring wheel control torque rate [Nm/s]

%% Algebraic States
% ay    = SX.sym('ay');    % Lateral vehicle acceleration
Fyf   = SX.sym('Fyf');   % Lateral force front tires
Fyr   = SX.sym('Fyr');   % Lateral force rear tires
beq   = SX.sym('beq');   % Equivalent motor damping
delta = SX.sym('delta'); % Steering angle [rad]
ay    = SX.sym('ay'); % Steering angle [rad]

%% xdot
X_dot     = SX.sym('X_dot');     % X position (absolute) [m]
Y_dot     = SX.sym('Y_dot');     % Y position (absolute) [m]
PSI_dot   = SX.sym('PSI_dot');   % Heading angle [rad]
vx_dot    = SX.sym('vx_dot');    % Longitudinal speed [m/s]
vy_dot    = SX.sym('vy_dot');    % Lateral speed [m/s]
dpsi_dot  = SX.sym('dpsi_dot');  % Yaw rate [rad/s]
epsi_dot  = SX.sym('epsi_dot');  % Angular error [rad]
ey_dot    = SX.sym('ey_dot');    % Lateral error [m]
theta_dot = SX.sym('theta_dot'); % Steering wheel angle [rad]
w_dot     = SX.sym('w_dot');     % Steering wheel angular velocity [rad/s]
T_dot     = SX.sym('T_dot');     % Sterring wheel control torque [Nm]

%% (unnamed) symbolic variables
sym_x    = vertcat(X,Y,PSI,vx,vy,dpsi,epsi,ey,theta,w,T);
sym_z    = vertcat(Fyf, Fyr, beq, delta, ay);
sym_xdot = [X_dot; Y_dot; PSI_dot; vx_dot; vy_dot; dpsi_dot; epsi_dot; ey_dot; theta_dot; w_dot; T_dot];
sym_u    = dT;

%% dynamics
%expr_f_expl = vertcat(v, ...
%                      dtheta, ...
%                      (- l*m*sin(theta)*dtheta.^2 + F + g*m*cos(theta)*sin(theta))/(M + m - m*cos(theta).^2), ...
%                      (- l*m*cos(theta)*sin(theta)*dtheta.^2 + F*cos(theta) + g*m*sin(theta) + M*g*sin(theta))/(l*(M + m - m*cos(theta).^2)));
sin_psi   = sin(PSI);
cos_psi   = cos(PSI);
sin_delta = sin(delta);
cos_delta = cos(delta);
sin_epsi  = sin(epsi);
cos_epsi  = cos(epsi);
% denominator = M + m - m*cos_theta.^2;
expr_f_expl = vertcat(vx*cos(PSI) - vy*sin(PSI)                                , ...
                      vx*sin(PSI) + vy*cos(PSI)                                , ...
                      dpsi                                                     , ...
                      (m*ax*cos(delta) + m*vy*dpsi - Fyf*sin(delta))/m         , ...
                      ay                                                       , ...
                      (lf*(m*ax*sin(delta) + Fyf*cos(delta)) - lr*Fyr)/Iz      , ...
                      dpsi - (rho/(1-rho*ey))*(vx*cos(epsi)- vy*sin(epsi))     , ...
                      vx*sin(epsi) + vy*cos(epsi)                              , ...
                      w                                                        , ...
                      (-beq/J)*w + (T)/(J) + (Fyf/(1100))/J                    , ...
                      lambda*dT                                                , ...
                      (Cf)*(delta - (vy + lf*dpsi)/(vx+1e-3))                  , ...
                      (Cr)*(-vy + lr*dpsi)/(vx+1e-3)                           , ...
                      (b*sqrt((lambda+1)/2))                                   , ...
                      -theta/ks                                                , ...
                      (-m*vx*dpsi + m*ax*sin(delta) + Fyf*cos(delta) + Fyr)/m);
expr_f_impl = expr_f_expl - [sym_xdot;sym_z];

%% constraints
expr_h = [sym_u;dpsi;ey;T; ay];

%% cost
W_x = diag([50, 50, 50, 0, 0, 50, 0, 0, 0, 0, 1e-1]);
W_u = 1e-1;
expr_ext_cost_e = sym_x' * W_x * sym_x;
expr_ext_cost = expr_ext_cost_e + sym_u' * W_u * sym_u;
% nonlinear least sqares
cost_expr_y = vertcat(sym_x, sym_u);
W = blkdiag(W_x, W_u);
model.cost_expr_y_e = sym_x;
model.W_e = zeros(length(W_x),length(W_x));
sym_p = [lambda; ax; rho];
%% populate structure
model.nx = nx;
model.nu = nu;
model.sym_x = sym_x;
model.sym_xdot = sym_xdot;
model.sym_u = sym_u;
model.sym_z = sym_z;
model.sym_p = sym_p;
% model.expr_f_expl = expr_f_expl;
model.expr_f_impl = expr_f_impl;
model.expr_h = expr_h;
model.expr_ext_cost = expr_ext_cost;
model.expr_ext_cost_e = expr_ext_cost_e;

model.cost_expr_y = cost_expr_y;
model.W = W;

end
