# Install script for directory: E:/Documents/2. Tecnalia/1. Software/Codigo/Matlab/Steering/src/01Acquisition/Joystick

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "E:/Documents/2. Tecnalia/1. Software/Codigo/Matlab/Steering/src/mex")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/mex" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/Documents/2. Tecnalia/1. Software/Codigo/Matlab/Steering/src/build/01Acquisition/Joystick/Debug/zJoystick.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/mex" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/Documents/2. Tecnalia/1. Software/Codigo/Matlab/Steering/src/build/01Acquisition/Joystick/Release/zJoystick.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/mex" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/Documents/2. Tecnalia/1. Software/Codigo/Matlab/Steering/src/build/01Acquisition/Joystick/MinSizeRel/zJoystick.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/mex" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/Documents/2. Tecnalia/1. Software/Codigo/Matlab/Steering/src/build/01Acquisition/Joystick/RelWithDebInfo/zJoystick.lib")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/mex" TYPE SHARED_LIBRARY FILES "E:/Documents/2. Tecnalia/1. Software/Codigo/Matlab/Steering/src/build/01Acquisition/Joystick/Debug/zJoystick.mexw64")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/mex" TYPE SHARED_LIBRARY FILES "E:/Documents/2. Tecnalia/1. Software/Codigo/Matlab/Steering/src/build/01Acquisition/Joystick/Release/zJoystick.mexw64")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/mex" TYPE SHARED_LIBRARY FILES "E:/Documents/2. Tecnalia/1. Software/Codigo/Matlab/Steering/src/build/01Acquisition/Joystick/MinSizeRel/zJoystick.mexw64")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/mex" TYPE SHARED_LIBRARY FILES "E:/Documents/2. Tecnalia/1. Software/Codigo/Matlab/Steering/src/build/01Acquisition/Joystick/RelWithDebInfo/zJoystick.mexw64")
  endif()
endif()

