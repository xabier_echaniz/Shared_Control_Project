#define	S_FUNCTION_NAME		zControlLongitudinalFuzzy	// Name of the block that you will create, in order to call the block S-Function you put this name.
#define	S_FUNCTION_LEVEL	2		

// Libraries of C/C++ that you what to add
#include<stdio.h>
#include<stdlib.h>
#include <errno.h>
#include <string.h>
#include "simstruc.h"

// Inclusion of the library Fuzzy
#include "../../TecLibs/src/TRIFuzzy.cpp"
#include <iostream>

using namespace std;

AOrbexDos *controller;
int tri_Started = 0;
/*************************************************
 ***   Initialize the C-Language S-Function    ***
 *************************************************/
static	void	mdlInitializeSizes( SimStruct *S ){

	ssSetNumSFcnParams( S, 0 );						
	ssSetNumContStates( S, 0 );						
	ssSetNumDiscStates( S, 0 );

	// ****************************************************************************
	// Definition of inputs
	// ****************************************************************************
	ssSetNumInputPorts( S, 1 ); // Number of inputs ports, in this case 3 ports.

	ssSetInputPortWidth( S, 0,  1);             // Number of the port, starting with 0, and the size, in this case port 0 size 1.										
	ssSetInputPortDirectFeedThrough( S, 0, 1 ); //Setting of the port where the input will be feed

    // ****************************************************************************
	// Definition of outputs
	// ****************************************************************************
	ssSetNumOutputPorts( S, 2 );	// Number of outputs ports, in this case 2 ports.

	ssSetOutputPortWidth( S, 0, 1);	 // Number of the port, starting with 0, and the size, in this case port 0 size 1.	
    ssSetOutputPortDataType(S, 0, SS_DOUBLE);
    ssSetOutputPortComplexSignal(S, 0, COMPLEX_NO);

    ssSetOutputPortWidth( S, 1, 1);	 // Number of the port, starting with 0, and the size, in this case port 0 size 1.	
    ssSetOutputPortDataType(S, 1, SS_DOUBLE);
    ssSetOutputPortComplexSignal(S, 1, COMPLEX_NO);
    
	ssSetNumSampleTimes( S, 1 );						
    ssSetOptions(S, (SS_OPTION_EXCEPTION_FREE_CODE |
                     SS_OPTION_USE_TLC_WITH_ACCELERATOR | 
		             SS_OPTION_WORKS_WITH_CODE_REUSE));
}
static	void	mdlInitializeSampleTimes( SimStruct *S ){
	ssSetSampleTime( S, 0, INHERITED_SAMPLE_TIME);	
    //ssSetSampleTime( S, 0, 0);	
	ssSetOffsetTime( S, 0, 0.0);						
}


/*************************************************
 ***     Loop in the C-Language S-Function     ***
 *************************************************/					
static	void	mdlOutputs( SimStruct *S, int_T tid ){
    
 	InputRealPtrsType  iSpeed_e = ssGetInputPortRealSignalPtrs( S, 0); // Input  1.
    real_T			  *oAccCV   = ssGetOutputPortRealSignal( S, 0 );   // Output 1.
    real_T			  *oBrkCV   = ssGetOutputPortRealSignal( S, 1 );   // Output 1.
// 	// ***************************************
// 	// Writing inputs
// 	// ***************************************
    
 	double Speed_e = (double)*iSpeed_e[0];
 	double tabInValues[1]={Speed_e};
    
    if(tri_Started == 0){
        
        try{
		   controller= new AOrbexDos("fuzzy/ConfigControlLongitudinalFuzzy.fuzzy");
        }catch(int&) {
            printf("Unable to Fuzzyfy.");
        } 
        tri_Started = 1;
    }
    //controller->Show();
    controller->Evaluate(tabInValues);
 
       
// 	// ***************************************
// 	// Writing outputs
// 	// ***************************************
    if(controller->GetOutValue(0) > 0){
        oAccCV[0] = controller->GetOutValue(0);
        oBrkCV[0] = 0;
    }else{
        oAccCV[0] = 0;
        oBrkCV[0] = (-1)*controller->GetOutValue(0);
    }
}


/*************************************************
 ***     Closing the C-Language S-Function     ***
 *************************************************/
static	void	mdlTerminate( SimStruct *S ){}

#ifdef	MATLAB_MEX_FILE						
	#include	"simulink.c"				
#else								
	#include	"cg_sfun.h"				
#endif

