/* ====================================================================== 
 * Name:        zTemplate.cpp
 * Description: Insert a short description here (1).
 *              Insert a short description here (2).
 *              Insert a short description here (3).
 * Note:        Insert your notes here.
 *              ( 1) For this examples it is mandatory to enable the lines 
 *              35 to 36 if you want to have more than one parameter read.
 * Author:      thename.of.the.author@tecnalia.com/upv.com/usb.com
 * Company:     Tecnalia / UPV / USB , etc.
 * ======================================================================*/
#define	S_FUNCTION_NAME		zCameraUDP // Debe cambiarse el nombre
#define	S_FUNCTION_LEVEL	2		

/* ======================================================================
 * Libraries definition 
 * ======================================================================*/
                                             
#include "src/CameraUDP.cpp"
#include <thread>
#include <atomic>

#define IP(S)   ssGetSFcnParam(S, 0)
#define PORT(S) ssGetSFcnParam(S, 1)

struct CameraUDP_Struct {
	CameraUDP *camera = new CameraUDP();
	std::thread threadLoop; // Thread where routine runs
	bool  stop;             // Flag to stop infinite while
	char  ip[20];           // TCP connection IP
	int_T port;             // TCP connection Port
	bool connected = false;
	int count;
	std::atomic<bool> ready =  false;
	uint8_t * finalData = (uint8_t *)calloc(DATA_SIZE, sizeof(uint8_t));
};

void CameraUDPLoop(CameraUDP_Struct *s, SimStruct *S) {

	std::chrono::milliseconds elapsed_time(10);
	auto init = std::chrono::high_resolution_clock::now();
	auto current = std::chrono::high_resolution_clock::now();
	auto counter = 0;

	real_T  *rawOutput = ssGetOutputPortRealSignal(S, 1);
	
	s->camera->start(s->ip, s->port);

	while (!s->stop) {
		
		if (!s->camera->getStatus()) {
			s->camera->startConnection(s->camera->getSocketID(), s->ip, s->port);
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			s->count++;
		}
		else {
			s->connected = true;
			s->camera->readData();
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}

			//init = std::chrono::high_resolution_clock::now();
			//current = std::chrono::high_resolution_clock::now();
			//counter = 0;
			//
			//	while (current - init <= elapsed_time) {
			//		s->connected = true;
			//		s->camera->readData();
			//		if (s->camera->error == 5) {
			//			memcpy(s->finalData + counter, s->camera->rawData, s->camera->bytesReadCom);
			//			counter += s->camera->bytesReadCom;
			//		}
			//		current = std::chrono::high_resolution_clock::now();
			//	}

			//	memset(s->finalData + counter, 0, DATA_SIZE - counter);
			//	for (int i = 0; i < DATA_SIZE; i++) {
			//		rawOutput[i] = s->finalData[i];
			//	}

		}
	}


int  init = 1;
int  count_ = 0;


/* ====================================================================== 
 * S-Function Configuration 
 * ======================================================================*/
// Numero de puertos de entrada y dimension de los puertos de entrada
#define nInputs  0 //(sizeof nInputsSize / sizeof nInputsSize[0])
#define nOutputs (sizeof nOutputsSize / sizeof nOutputsSize[0])
static int nInputsSize[]  = {0};    // Actually empty, but this is not allowed in C++ (MinGW accepts it, though)
static int nOutputsSize[] = {20}; 	// X,Y,Z for: Acceleration,AngularRate,Orientation,Position,Velocity.
static int nParameters    = 2;				 	// IP and Port for UDP Connection
static int nPWorks        = 1;					// Gps Class

/* ====================================================================== 
 * Configuration parameters method 
 * ======================================================================*/
//#define cMode(S) ssGetSFcnParam(S, 0)
#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE)
	static void mdlCheckParameters(SimStruct *S){}
#endif

/* ====================================================================== 
 * Initialization method
 * ======================================================================*/
#define MDL_INITIALIZE_CONDITIONS 
#if defined(MDL_INITIALIZE_CONDITIONS) && defined(MATLAB_MEX_FILE) 
static void mdlInitializeConditions(SimStruct *S){} 
#endif 

/* ====================================================================== 
 * Start method (Just runs pushing play button)
 * ======================================================================*/
#define MDL_START 
#if defined(MDL_START) 
static void mdlStart(SimStruct *S)
{
/* ====================================================================== 
 * Start your code here.
 * ======================================================================*/	

	CameraUDP_Struct *s = new CameraUDP_Struct();
	ssGetPWork(S)[0] = (void *)s;
	// Read Parameters
	int_T bufLen = mxGetN((IP(S))) * sizeof(mxChar) + 1;  // Read
	mxGetString(IP(S), s->ip, bufLen);                   // IP "String"
	s->port = *mxGetPr(PORT(S));                      // Read Port "int"
	s->stop = false;
	s->threadLoop = std::thread(CameraUDPLoop, s, S);

/* ====================================================================== 
 * Finish your code here.
 * ======================================================================*/ 
}
#endif

/* ======================================================================= 
 * Initialize method (Just runs pushing play button)
 * ======================================================================*/
static	void	mdlInitializeSizes( SimStruct *S ){
	ssSetNumSFcnParams( S, nParameters  );
    #if defined(MATLAB_MEX_FILE)
		if(nParameters != 0){
			if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)){
				mdlCheckParameters(S);
				if (ssGetErrorStatus(S) != NULL){
					ssSetErrorStatus(S, "Parameter to S-function are not \
                                         configure.");
					return;
				}
			}else{
				ssSetErrorStatus(S, "The number of parameters is not  \
                                     consistent.");
				return;
			}
		}
    #endif
	int i;
    for ( i = 0; i < ssGetNumSFcnParams(S); i++ ){
        ssSetSFcnParamTunable( S, i, SS_PRM_SIM_ONLY_TUNABLE );
    }
	
	ssSetNumContStates( S, 0 );						
	ssSetNumDiscStates( S, 0 );

	ssSetNumInputPorts( S, nInputs );	
	for(i=0; i < nInputs ; i = i + 1){
		ssSetInputPortWidth( S, i, nInputsSize[i]); 				
		ssSetInputPortDirectFeedThrough( S, i, 1 );
	}

	ssSetNumOutputPorts( S, nOutputs );	
	for(i=0; i < nOutputs ; i = i + 1){
		ssSetOutputPortWidth( S, i, nOutputsSize[i]);
		ssSetOutputPortDataType(S, i, SS_DOUBLE);
		ssSetOutputPortComplexSignal(S, i, COMPLEX_NO);
	}
    
	ssSetNumSampleTimes( S, 1);						
    ssSetOptions(S, (SS_OPTION_EXCEPTION_FREE_CODE |
                     SS_OPTION_USE_TLC_WITH_ACCELERATOR | 
		             SS_OPTION_WORKS_WITH_CODE_REUSE));    
	ssSetNumPWork(S, nPWorks); 
}

static	void	mdlInitializeSampleTimes( SimStruct *S ){
	ssSetSampleTime( S, 0, 0.01);	  				
	ssSetOffsetTime( S, 0, 0.0);						
}


/* ====================================================================== 
 * Output method.
 * ======================================================================*/			
static	void	mdlOutputs( SimStruct *S, int_T tid ){
	
/* ====================================================================== 
 * Start your code here.
 * ======================================================================*/	
	CameraUDP_Struct *s = (CameraUDP_Struct *)ssGetPWork(S)[0];
    
    real_T  *cameraOutput  = ssGetOutputPortRealSignal(S,0);
    
/* ====================================================================== 
 * Finish your code here.
 * ======================================================================*/
 
/* ====================================================================== 
 * Write the outputs of the S-Function
 * ======================================================================*/
    
    cameraOutput[0]  = s->connected;    
    cameraOutput[1]  = s->count;    
    cameraOutput[2]  = s->camera->bytesReadCom;    
    cameraOutput[3]  = s->camera->error; 
	cameraOutput[4]  = s->camera->getBlink();
	cameraOutput[5]  = s->camera->getDrowsy();
	cameraOutput[6]  = s->camera->getLeft();
	cameraOutput[7]  = s->camera->getCenter();
	cameraOutput[8]  = s->camera->getRight();
	cameraOutput[9]  = s->camera->getUp();
	cameraOutput[10] = s->camera->getDown();
	cameraOutput[11] = s->camera->getEyeLeft();
	cameraOutput[12] = s->camera->getEyeCenter();
	cameraOutput[13] = s->camera->getEyeRight();
	cameraOutput[14] = s->camera->getHeadTime();
	cameraOutput[15] = s->camera->getEyeTime();
}


/* ====================================================================== 
 * Terminate method
 * ======================================================================*/
static	void	mdlTerminate( SimStruct *S ){
/* ====================================================================== 
 * Start your code here.
 * ======================================================================*/	
    
	CameraUDP_Struct *s = (CameraUDP_Struct *)ssGetPWork(S)[0];
	s->stop = true;
	s->threadLoop.join();
	s->camera->stop();
	delete s;
	init = 1;
	count_ = 0;
	
/* ====================================================================== 
 * Finish your code here.
 * ======================================================================*/
}

#ifdef	MATLAB_MEX_FILE						
	#include	"simulink.c"				
#else								
	#include	"cg_sfun.h"				
#endif

