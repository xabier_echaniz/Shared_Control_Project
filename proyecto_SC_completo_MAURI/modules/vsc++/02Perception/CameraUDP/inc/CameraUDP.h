#ifndef CameraUDP_H
#define CameraUDP_H

#include <winsock2.h>
#include <ws2tcpip.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <cmath>
#define _USE_MATH_DEFINES
#include <math.h>
#include "simstruc.h"



#pragma comment(lib, "Ws2_32.lib")

#define DATA_SIZE 42
#define VALID_SYNC 0xE7

class Channel0 {
private:
    long time;
    uint8_t numberOfSatellites;
    uint8_t positionMode;
    uint8_t velocityMode;
    uint8_t orientationMode;
public:
    uint8_t getNumberOfSatellites() { return numberOfSatellites; };
    uint8_t getPositionMode() { return positionMode; };
    uint8_t getVelocityMode() { return velocityMode; };
    uint8_t getOrientationMode() { return orientationMode; };
    void    update(uint8_t *rawData);
};

class CameraUDP
{
private:
   
    uint8_t     sync;               
                                    
    uint16_t    time;    
	uint8_t     blink;
	uint8_t     drowsy;
	int         drowsyI;
	float       left; 		       
    float       center;     
    float       right;      
    float       up;       
	float       down; 
	float       eyeLeft;
	float       eyeCenter;
	float       eyeRight;
    float       eyeTime;
    float       headTime;
                                    
    uint8_t     batchS[8];          
    uint8_t     checksum3;          

    Channel0    channel0;

    SOCKET      socketID;
    char*       ip;
    int         port;
    void        selectSocketVersion(uint8_T version, uint8_T subVersion);
    void        createSocket(int domain, int type, int protocol);
    void        update(uint8_t *rawData);
    float       from24bitsToFloat(uint8_t *bytes, int decimals);
    int32_t     from24bitsToInt32(const uint8_t *b);
    float       radiansToDegrees(float radians);
    double      radiansToDegrees(double radians);
    float       getModule(float x, float y, float z);
	float float_swap(float value);
	int			init = 1;
   
    
    

public:
    CameraUDP();
	uint8_t    *rawData;
	int_T	  getSocketID();
    uint8_t  *getRawData();
    boolean   isDataOk();
    void      updateData();
    void      start(char * ipAddress, int_T port);
	void      startConnection(SOCKET socketID, char * ipAddress, int port);
    void      stop();
	uint8_t   getBlink();
	uint8_t   getDrowsy();
	int       getDrowsyI();
	float	  getLeft();
	float	  getCenter();
	float	  getRight();
	float	  getUp();
	float	  getDown();
	float	  getEyeLeft();
	float	  getEyeRight();
	float	  getEyeCenter();
	float	  getEyeTime();
	float	  getHeadTime();
	bool      connectionOk;
	bool      getStatus();
	void      readData();
	int       bytesReadCom;
	int		  error;


};


#endif