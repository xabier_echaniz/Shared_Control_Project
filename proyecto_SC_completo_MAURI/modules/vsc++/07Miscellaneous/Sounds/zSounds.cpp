/* ====================================================================== 
 * Name:        zTemplate.cpp
 * Description: Insert a short description here (1).
 *              Insert a short description here (2).
 *              Insert a short description here (3).
 * Note:        Insert your notes here.
 *              ( 1) For this examples it is mandatory to enable the lines 
 *              35 to 36 if you want to have more than one parameter read.
 * Author:      thename.of.the.author@tecnalia.com/upv.com/usb.com
 * Company:     Tecnalia / UPV / USB , etc.
 * ======================================================================*/
#define	S_FUNCTION_NAME		zSounds // Debe cambiarse el nombre
#define	S_FUNCTION_LEVEL	2		

/* ======================================================================
 * Libraries definition 
 * ======================================================================*/
#include <iostream> 
#include <windows.h> // WinApi header 
#include "simstruc.h"
#include <thread>
#include <atomic>

using namespace std;
//#define IP(S)   ssGetSFcnParam(S, 0)
//#define PORT(S) ssGetSFcnParam(S, 1)

struct Sound_Struct {
	
	std::thread threadLoop; // Thread where routine runs
	int mode; // mode of sound
	bool  stop;             // Flag to stop infinite while

};

void SoundLoop(Sound_Struct *s, SimStruct *S) {

	std::chrono::milliseconds elapsed_time(10);
	auto init = std::chrono::high_resolution_clock::now();
	auto current = std::chrono::high_resolution_clock::now();
	auto counter = 0;

	//real_T  *rawOutput = ssGetOutputPortRealSignal(S, 1);
	


	while (!s->stop) {

		if (s->mode == 1) {
			for (int i = 0; i < 3; i++) {
				Beep(523, 200);
				std::this_thread::sleep_for(std::chrono::milliseconds(200));
			}
		}
		else {
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}

	}
}


int  init = 1;
int  count_ = 0;


/* ====================================================================== 
 * S-Function Configuration 
 * ======================================================================*/
// Numero de puertos de entrada y dimension de los puertos de entrada
#define nInputs  1 //(sizeof nInputsSize / sizeof nInputsSize[0])
#define nOutputs 1 //(sizeof nOutputsSize / sizeof nOutputsSize[0])
static int nInputsSize[]  = {1};    // Actually empty, but this is not allowed in C++ (MinGW accepts it, though)
static int nOutputsSize[] = {1}; 	// X,Y,Z for: Acceleration,AngularRate,Orientation,Position,Velocity.
static int nParameters    = 0;				 	// IP and Port for UDP Connection
static int nPWorks        = 1;					// Gps Class

/* ====================================================================== 
 * Configuration parameters method 
 * ======================================================================*/
//#define cMode(S) ssGetSFcnParam(S, 0)
#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE)
	static void mdlCheckParameters(SimStruct *S){}
#endif

/* ====================================================================== 
 * Initialization method
 * ======================================================================*/
#define MDL_INITIALIZE_CONDITIONS 
#if defined(MDL_INITIALIZE_CONDITIONS) && defined(MATLAB_MEX_FILE) 
static void mdlInitializeConditions(SimStruct *S){} 
#endif 

/* ====================================================================== 
 * Start method (Just runs pushing play button)
 * ======================================================================*/
#define MDL_START 
#if defined(MDL_START) 
static void mdlStart(SimStruct *S)
{
/* ====================================================================== 
 * Start your code here.
 * ======================================================================*/	

	Sound_Struct *s = new Sound_Struct();
	ssGetPWork(S)[0] = (void *)s;

	s->stop = false;
	s->threadLoop = std::thread(SoundLoop, s, S);

/* ====================================================================== 
 * Finish your code here.
 * ======================================================================*/ 
}
#endif

/* ======================================================================= 
 * Initialize method (Just runs pushing play button)
 * ======================================================================*/
static	void	mdlInitializeSizes( SimStruct *S ){
	ssSetNumSFcnParams( S, nParameters  );
    #if defined(MATLAB_MEX_FILE)
		if(nParameters != 0){
			if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)){
				mdlCheckParameters(S);
				if (ssGetErrorStatus(S) != NULL){
					ssSetErrorStatus(S, "Parameter to S-function are not \
                                         configure.");
					return;
				}
			}else{
				ssSetErrorStatus(S, "The number of parameters is not  \
                                     consistent.");
				return;
			}
		}
    #endif
	int i;
    for ( i = 0; i < ssGetNumSFcnParams(S); i++ ){
        ssSetSFcnParamTunable( S, i, SS_PRM_SIM_ONLY_TUNABLE );
    }
	
	ssSetNumContStates( S, 0 );						
	ssSetNumDiscStates( S, 0 );

	ssSetNumInputPorts( S, nInputs );	
	for(i=0; i < nInputs ; i = i + 1){
		ssSetInputPortWidth( S, i, nInputsSize[i]); 				
		ssSetInputPortDirectFeedThrough( S, i, 1 );
	}

	ssSetNumOutputPorts( S, nOutputs );	
	for(i=0; i < nOutputs ; i = i + 1){
		ssSetOutputPortWidth( S, i, nOutputsSize[i]);
		ssSetOutputPortDataType(S, i, SS_DOUBLE);
		ssSetOutputPortComplexSignal(S, i, COMPLEX_NO);
	}
    
	ssSetNumSampleTimes( S, 1);						
    ssSetOptions(S, (SS_OPTION_EXCEPTION_FREE_CODE |
                     SS_OPTION_USE_TLC_WITH_ACCELERATOR | 
		             SS_OPTION_WORKS_WITH_CODE_REUSE));    
	ssSetNumPWork(S, nPWorks); 
}

static	void	mdlInitializeSampleTimes( SimStruct *S ){
	ssSetSampleTime( S, 0, 0.01);	  				
	ssSetOffsetTime( S, 0, 0.0);						
}


/* ====================================================================== 
 * Output method.
 * ======================================================================*/			
static	void	mdlOutputs( SimStruct *S, int_T tid ){
	
/* ====================================================================== 
 * Start your code here.
 * ======================================================================*/	
	Sound_Struct *s = (Sound_Struct *)ssGetPWork(S)[0];
    
	InputRealPtrsType	mode = ssGetInputPortRealSignalPtrs(S, 0);
	real_T  *out = ssGetOutputPortRealSignal(S, 0);
	s->mode = (int) *mode[0];
	out[0] = s->mode;
/* ====================================================================== 
 * Finish your code here.
 * ======================================================================*/
 
/* ====================================================================== 
 * Write the outputs of the S-Function
 * ======================================================================*/

}


/* ====================================================================== 
 * Terminate method
 * ======================================================================*/
static	void	mdlTerminate( SimStruct *S ){
/* ====================================================================== 
 * Start your code here.
 * ======================================================================*/	
    
	Sound_Struct *s = (Sound_Struct *)ssGetPWork(S)[0];
	s->stop = true;
	s->threadLoop.join();
	delete s;
	init = 1;
	count_ = 0;
	
/* ====================================================================== 
 * Finish your code here.
 * ======================================================================*/
}

#ifdef	MATLAB_MEX_FILE						
	#include	"simulink.c"				
#else								
	#include	"cg_sfun.h"				
#endif

