#include "../inc/CameraUDP.h"

using namespace std;

int num = 0;

CameraUDP::CameraUDP(){

}

int_T CameraUDP::getSocketID() {
	return this->socketID;

}

bool CameraUDP::getStatus() {
	return this->connectionOk;
}


uint8_t*  CameraUDP::getRawData(){
    return this->rawData;
}

float CameraUDP::getModule(float x, float y, float z){
    return sqrt(pow(x,2) + pow(y,2) + pow(z,2));
}
float CameraUDP::float_swap(float value) {
	union v {
		float       f;
		unsigned int    i;
	};

	union v val;
	unsigned int temp;

	val.f = value;
	temp = htonl(val.i);

	return *(float*)&temp;
}
    
uint8_t  CameraUDP::getBlink() {
	return this->blink;
}

uint8_t  CameraUDP::getDrowsy() {
	return this->drowsy;
}

int  CameraUDP::getDrowsyI() {
	return _byteswap_ulong(this->drowsyI);
}

float     CameraUDP::getLeft(){
    return (float) this->float_swap(this->left);
}  
float     CameraUDP::getCenter() {
	return (float)this->float_swap(this->center);
}
float     CameraUDP::getRight() {
	return (float)this->float_swap(this->right);
}
float     CameraUDP::getUp() {
	return (float)this->float_swap(this->up);
}
float     CameraUDP::getDown() {
	return (float)this->float_swap(this->down);
}

float     CameraUDP::getEyeLeft() {
	return (float)this->float_swap(this->eyeLeft);
}
float     CameraUDP::getEyeCenter() {
	return (float)this->float_swap(this->eyeCenter);
}

float CameraUDP::getEyeTime()
{
	return (float)this->float_swap(this->eyeTime);
}

float CameraUDP::getHeadTime()
{
	return (float)this->float_swap(this->headTime);
}

float     CameraUDP::getEyeRight() {
	return (float)this->float_swap(this->eyeRight);
}


float CameraUDP::radiansToDegrees(float radians){
    return radians*180.000000/M_PI;
}

double CameraUDP::radiansToDegrees(double radians){
    return radians*180.000000/M_PI;
}

float CameraUDP::from24bitsToFloat(uint8_t *bytes, int decimals){
    return (float) this->from24bitsToInt32(bytes)/(pow((float)10,decimals));
}       

int32_t CameraUDP::from24bitsToInt32(const uint8_t *b)
{
	union { int32_t x; uint8_t c[4]; } u;
	u.c[1] = b[0];
	u.c[2] = b[1];
	u.c[3] = b[2];
	return u.x >> 8;
}

//boolean CameraUDP::isDataOk(){
//    if(this->getSync() == VALID_SYNC){
//        return true;
//    }
//    return false;
//}

void CameraUDP::updateData(){
    uint8_t* rawData = (uint8_t *) calloc(DATA_SIZE,sizeof(uint8_t));
    // this->readData(rawData);
    if( *rawData == VALID_SYNC){  // Equal to 0xE7 sync byte of NCOM structure - && getNavigationStatus()!= 11 
        //this->update(rawData);                                   // Navigation Status 11 = Invalid Data
         
    }
    else{
        //this->update(rawData);
        //printf("NOT UPDATE\n");
        //printf("%f\n",getHeading());
    }
    free(rawData);
}

//void CameraUDP::update(uint8_t *rawData){
//    memcpy(&this->sync, rawData+0  , 1);
//    memcpy(&this->time, rawData+1  , 2);
//
//    memcpy(&this->navigationStatus  , rawData + 21, 1);
//
//    if(this->getNavigationStatus() != 11){
//    // Batch A
//    this->accelerationX = from24bitsToFloat(rawData + 3,  4);
//    this->accelerationY = from24bitsToFloat(rawData + 6,  4);
//    this->accelerationZ = from24bitsToFloat(rawData + 9,  4);
//    this->angularRateX  = from24bitsToFloat(rawData + 12, 5);
//    this->angularRateY  = from24bitsToFloat(rawData + 15, 5);
//    this->angularRateZ  = from24bitsToFloat(rawData + 18, 5);
//
//    //23memcpy(&this->navigationStatus  , rawData + 21, 1);
//    memcpy(&this->checksum1         , rawData + 22, 1);
//    // Batch B
//    memcpy(&this->latitude          , rawData + 23, 8);
//    memcpy(&this->longitude         , rawData + 31, 8);
//    memcpy(&this->altitude          , rawData + 39, 4);
//    this->northVelocity = from24bitsToFloat(rawData + 43,  4);
//    this->eastVelocity  = from24bitsToFloat(rawData + 46,  4);
//    this->downVelocity  = from24bitsToFloat(rawData + 49,  4);
//    this->heading       = from24bitsToFloat(rawData + 52,  6);
//    this->pitch         = from24bitsToFloat(rawData + 55,  6);
//    this->roll          = from24bitsToFloat(rawData + 58,  6);
//
//     memcpy(&this->checksum2     ,rawData + 61, 1);
//     memcpy(&this->statusChannel ,rawData + 62, 1);
//     memcpy(&this->batchS ,rawData + 63, 8);
//
//     if(this->getStatusChannel() == 0){
//        this->channel0.update(this->getBatchS());
//     }
// }
//}

void CameraUDP::readData(){
    
	if (this->init) {
		this->rawData = (uint8_t *) calloc(DATA_SIZE, sizeof(uint8_t));
		this->init = 0;
	}
	
	struct sockaddr_in srcAddress;
	socklen_t srcAddress_len = sizeof(srcAddress);
	//int bytesRead = -1;
	// Define timeout for read data
	fd_set set;							// File descriptor
	struct timeval timeout;				// Timeout variable
	FD_ZERO(&set);					    // Clear the set
	FD_SET(this->getSocketID(), &set);  // Add file descriptor to the set
	timeout.tv_sec = 4;                 // Seconds
	timeout.tv_usec = 100000;				// Microseconds

	// Size of data to read
	// 13 of command - 13 of keep alive  = 26 bytes
	
	

	int rv = select(this->getSocketID() + 1, &set, NULL, NULL, &timeout);

	if (rv == SOCKET_ERROR)
	{
		this->error = 1;
		// select error...
	}
	else if (rv == 0)
	{
		this->error = 2;
		// timeout, socket does not have anything to read
	}
	else
	{
		// socket has something to read
		this->bytesReadCom = recvfrom(this->socketID, (char *)this->rawData, DATA_SIZE, 0, (sockaddr *)&srcAddress, &srcAddress_len);
		//}

		//if (this->bytesReadCom > 0 && strcmp(inet_ntoa(srcAddress.sin_addr), this->ip) == 0) {

			//printf("Received OK from %s:%d\n", inet_ntoa(srcAddress.sin_addr), ntohs(srcAddress.sin_port));
		//}

		if (this->bytesReadCom == SOCKET_ERROR)
		{
			this->error = 3;
			// read failed...
		}
		else if (this->bytesReadCom == 0)
		{
			this->error = 4;
			this->connectionOk = false;
			// peer disconnected...
		}
		else
		{
			this->error = 5;
			memcpy(&this->blink		, rawData + 0	, 1);
			memcpy(&this->drowsy	, rawData + 1	, 1);
			memcpy(&this->left		, rawData + 2	, 4);
			memcpy(&this->center	, rawData + 6	, 4);
			memcpy(&this->right		, rawData + 10	, 4);
			memcpy(&this->up		, rawData + 14	, 4);
			memcpy(&this->down		, rawData + 18	, 4);
			memcpy(&this->eyeLeft	, rawData + 22	, 4);
			memcpy(&this->eyeCenter	, rawData + 26	, 4);
			memcpy(&this->eyeRight	, rawData + 30	, 4);
			memcpy(&this->eyeTime	, rawData + 34	, 4);
			memcpy(&this->headTime	, rawData + 38	, 4);



			//memset(this->rawData + bytesReadCom, 0, DATA_SIZE - bytesReadCom);
			//send(this->getSocketID(), (char *)(this->throttleMsg), 26, 0);
			// read successful...
		}
	}
	
	


    
    //FD_ZERO(&select_fds);                              //Clear out fd's
    //FD_SET(this->socketID, &select_fds);              //Set the bit that corresponds to socket s       
                                                      
    //timeout.tv_sec = 1;         //Timeout set for 5 sec + 0 micro sec
    //timeout.tv_usec = 0;

    //int selectError = select(32, &select_fds, NULL, NULL, &timeout);

    //if (  selectError == 0 )
    //{
    //   printf("Receiving timed out...\n");

    //}
    //else
    //{   
     
}

void CameraUDP::start(char * ipAddress, int_T port){
    this->selectSocketVersion(2,2);                         // Socket Version 2.2
    this->createSocket(AF_INET, SOCK_DGRAM,0);  // Config for UDP
	this->ip = ipAddress;
    this->startConnection(this->socketID, ipAddress, port);   // Connect to CameraUDP
}

void CameraUDP::stop(){
     closesocket(this->socketID);
     WSACleanup();

}

void CameraUDP::selectSocketVersion(uint8_T version, uint8_T subVersion){
    WSADATA wsa;
    WORD wVersion = MAKEWORD(version,subVersion); // (2,2) means version 2.2
    if (WSAStartup(wVersion,&wsa) != 0){          // Initiates use of the Winsock DLL by a process (0 = OK)
        //printf("WSAStartup error\n");
    }
}

void CameraUDP::createSocket(int domain, int type, int protocol){
    this->socketID = socket(domain,type,protocol);
    if ( this->socketID <= 0 ){
        //printf("Socket creation error\n");
    }   
}

void CameraUDP::startConnection(SOCKET socketID, char * ipAddress, int port){
    struct sockaddr_in socketAddress;
    memset(&socketAddress, 0, sizeof(sockaddr_in)); //Inicializo a 0 todo
    socketAddress.sin_family            = AF_INET;
    socketAddress.sin_port              = htons(port);
	socketAddress.sin_addr.s_addr = INADDR_ANY;//inet_addr(ipAddress);

    if( bind(socketID ,(sockaddr *)&socketAddress, sizeof(socketAddress)) == -1){
        //printf("Error Binding: %d\n", WSAGetLastError());
		this->connectionOk = false;
        return ;
    }
	else {
		this->connectionOk = true;
	}
    // Non-Blocking Socket
    u_long iMode = 1;
	ioctlsocket(socketID,FIONBIO,&iMode);
}

//void Channel0::update(uint8_t *rawData){
//    memcpy(&this->time               , rawData+0 , 4);
//    memcpy(&this->numberOfSatellites , rawData+4 , 1);
//    memcpy(&this->positionMode       , rawData+5 , 1);
//    memcpy(&this->velocityMode       , rawData+6 , 1);
//    memcpy(&this->orientationMode    , rawData+7 , 1);
//    //printf("SAT %d\n",this->numberOfSatellites );
//}



