

find_package(Matlab REQUIRED COMPONENTS SIMULINK MX_LIBRARY)
find_package(tinyxml2 PATHS "${CMAKE_CURRENT_SOURCE_DIR}/ext/tinyxml2/install/lib/cmake/tinyxml2")

set(ADCORE_ROOT ${CMAKE_CURRENT_SOURCE_DIR})

# Socket Lib
set(ADCORE_SOCKET_LIB ws2_32)

# Eigen Lib
set(EIGEN_INCLUDE_DIR ${ADCORE_ROOT}/ext/Eigen)	

# TinyXML Lib
set(tinyxml2_INSTALL_DIR "${ADCORE_ROOT}/ext/tinyxml2/install")
set(TINYXML2_INCLUDE_DIR ${tinyxml2_INSTALL_DIR}/include)
set(TINYXML2_LIB_DIR  	 ${tinyxml2_INSTALL_DIR}/lib)

# SMV2
set(SMV2_INSTALL_DIR "${ADCORE_ROOT}/ext/SimpleMotionV2/install")
set(SMV2_INCLUDE_DIR "${SMV2_INSTALL_DIR}/..")
set(SMV2_LIB_DIR  	 ${SMV2_INSTALL_DIR}/lib)
set(SMV2_DRIVER_DIR  ${SMV2_INCLUDE_DIR}/drivers/ftdi_d2xx/third_party/win_64bit)

set(SDL2_INCLUDE_DIR "${ADCORE_ROOT}/ext/SDL2/include")
set(SDL2_LIB_DIR  	"${ADCORE_ROOT}/ext/SDL2/lib/x64")




set(TECLIBS_ROOT ${ADCORE_ROOT}/TecLibs)
set(TECLIBS_INCLUDE_DIRS ${TECLIBS_ROOT}/inc ${EIGEN_INCLUDE_DIR} ${TINYXML2_INCLUDE_DIR} ${SMV2_INCLUDE_DIR} ${SDL2_INCLUDE_DIR})
set(TECLIBS_LIB_DIRS ${TINYXML2_LIB_DIR} ${SMV2_LIB_DIR} ${SMV2_DRIVER_DIR} ${SDL2_LIB_DIR})
set(TECLIBS_LIBRARIES TecLibs tinyxml2 simplemotionv2 ftd2xx dinput8 dxguid comctl32 SDL2 SDL2main)

