% =========================================================================
% Name:        zTemplate.cpp
% Description: Insert a short description here (1).
%              Insert a short description here (2).
%              Insert a short description here (3).
% Note:        Insert your notes here.
%              ( 1) For this examples it is mandatory to enable the lines 
%              35 to 36 if you want to have more than one parameter read.
% Author:      thename.of.the.author@tecnalia.com/upv.com/usb.com
% Company:     Tecnalia / UPV / USB , etc.
% =========================================================================
clear all; clc;

% Generación del bloque mexw64, siempre se debe realizar la entrega de los
% bloques .mexw64 con un programa de ejemplo.
mex ../zTemplate.cpp 

% Open slx simulink
open('Template.slx');