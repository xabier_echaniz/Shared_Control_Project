/* ====================================================================== 
 * Name:        zTemplate.cpp
 * Description: Insert a short description here (1).
 *              Insert a short description here (2).
 *              Insert a short description here (3).
 * Note:        Insert your notes here.
 *              ( 1) For this examples it is mandatory to enable the lines 
 *              35 to 36 if you want to have more than one parameter read.
 * Author:      thename.of.the.author@tecnalia.com/upv.com/usb.com
 * Company:     Tecnalia / UPV / USB , etc.
 * ======================================================================*/
#define	S_FUNCTION_NAME		zTemplate // Debe cambiarse el nombre
#define	S_FUNCTION_LEVEL	2		

/* ======================================================================
 * Libraries definition 
 * ======================================================================*/
#include "../../TecLibs/inc/Lib_Tecnalia.hpp" //Libreria estandar Automated
                                              // Driving

/* ====================================================================== 
 * S-Function Configuration 
 * ======================================================================*/
// Numero de puertos de entrada y dimension de los puertos de entrada
#define nInputs  (sizeof nInputsSize / sizeof nInputsSize[0])
#define nOutputs (sizeof nOutputsSize / sizeof nOutputsSize[0])
static int nInputsSize[]  = {1, 2, 4};
static int nOutputsSize[] = {3, 5};
static int nParameters    = 0;
static int nPWorks        = 0;

/* ====================================================================== 
 * Configuration parameters method 
 * ======================================================================*/
//#define cMode(S) ssGetSFcnParam(S, 0)
#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE)
	static void mdlCheckParameters(SimStruct *S){}
#endif

/* ====================================================================== 
 * Initialization method
 * ======================================================================*/
#define MDL_INITIALIZE_CONDITIONS 
#if defined(MDL_INITIALIZE_CONDITIONS) && defined(MATLAB_MEX_FILE) 
static void mdlInitializeConditions(SimStruct *S){} 
#endif 

/* ====================================================================== 
 * Start method (Just runs pushing play button)
 * ======================================================================*/
#define MDL_START 
#if defined(MDL_START) 
static void mdlStart(SimStruct *S)
{
/* ====================================================================== 
 * Start your code here.
 * ======================================================================*/	
    // In this method you can initialize classes and read parameters, etc.
    //ssGetPWork(S)[0] = (void *) new CLASS(); //
/* ====================================================================== 
 * Finish your code here.
 * ======================================================================*/ 
}
#endif

/* ======================================================================= 
 * Initialize method (Just runs pushing play button)
 * ======================================================================*/
static	void	mdlInitializeSizes( SimStruct *S ){
	ssSetNumSFcnParams( S, nParameters  );
    #if defined(MATLAB_MEX_FILE)
		if(nParameters != 0){
			if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)){
				mdlCheckParameters(S);
				if (ssGetErrorStatus(S) != NULL){
					ssSetErrorStatus(S, "Parameter to S-function are not \
                                         configure.");
					return;
				}
			}else{
				ssSetErrorStatus(S, "The number of parameters is not  \
                                     consistent.");
				return;
			}
		}
    #endif
	int i;
    for ( i = 0; i < ssGetNumSFcnParams(S); i++ ){
        ssSetSFcnParamTunable( S, i, SS_PRM_SIM_ONLY_TUNABLE );
    }
	
	ssSetNumContStates( S, 0 );						
	ssSetNumDiscStates( S, 0 );

	ssSetNumInputPorts( S, nInputs );	
	for(i=0; i < nInputs ; i = i + 1){
		ssSetInputPortWidth( S, i, nInputsSize[i]); 				
		ssSetInputPortDirectFeedThrough( S, i, 1 );
	}

	ssSetNumOutputPorts( S, nOutputs );	
	for(i=0; i < nOutputs ; i = i + 1){
		ssSetOutputPortWidth( S, i, nOutputsSize[i]);
		ssSetOutputPortDataType(S, i, SS_DOUBLE);
		ssSetOutputPortComplexSignal(S, i, COMPLEX_NO);
	}
    
	ssSetNumSampleTimes( S, 1);						
    ssSetOptions(S, (SS_OPTION_EXCEPTION_FREE_CODE |
                     SS_OPTION_USE_TLC_WITH_ACCELERATOR | 
		             SS_OPTION_WORKS_WITH_CODE_REUSE));    
	ssSetNumPWork(S, nPWorks); 
}

static	void	mdlInitializeSampleTimes( SimStruct *S ){
	ssSetSampleTime( S, 0, INHERITED_SAMPLE_TIME);	  				
	ssSetOffsetTime( S, 0, 0.0);						
}


/* ====================================================================== 
 * Output method.
 * ======================================================================*/			
static	void	mdlOutputs( SimStruct *S, int_T tid ){
	InputRealPtrsType inputs[nInputs];
	real_T *Outputs[nOutputs];
	int i = 0;
	for(i=0; i<nInputs ; i = i + 1){
		inputs[i] = ssGetInputPortRealSignalPtrs(S, i);
	} 
	for(i=0; i<nOutputs ; i = i + 1){
		Outputs[i] = ssGetOutputPortRealSignal(S, i);
	} 
	
	
	
/* ====================================================================== 
 * Start your code here.
 * ======================================================================*/	
	// It is mandatory the current template to program blocks for automated 
	// driving Tecnalia, and keeping the total column width (it is related)
	// with MATLAD coding workspace.
    // Pworks reading template CLASS *class = (CLASS *) ssGetPWork(S)[0];  
    // 
	printf("%lf\n",*inputs[0][0]);
	printf("%lf %lf\n", *inputs[1][0], *inputs[1][1]);
	printf("%lf %lf %lf %lf\n", *inputs[2][0], *inputs[2][1], 
            *inputs[2][2], *inputs[2][3]);
/* ====================================================================== 
 * Finish your code here.
 * ======================================================================*/

 
 
/* ====================================================================== 
 * Write the outputs of the S-Function
 * ======================================================================*/
	Outputs[0][0] = 81.0;
	Outputs[0][1] = 82.0;
	Outputs[0][2] = 83.0;
	
	Outputs[1][0] = 91.0;
	Outputs[1][1] = 92.0;
	Outputs[1][2] = 93.0;
	Outputs[1][3] = 92.0;
	Outputs[1][4] = 93.0;
}


/* ====================================================================== 
 * Terminate method
 * ======================================================================*/
static	void	mdlTerminate( SimStruct *S ){
/* ====================================================================== 
 * Start your code here.
 * ======================================================================*/	

/* ====================================================================== 
 * Finish your code here.
 * ======================================================================*/
}

#ifdef	MATLAB_MEX_FILE						
	#include	"simulink.c"				
#else								
	#include	"cg_sfun.h"				
#endif

