//============================================================================
// Name        : Math_Geometry.cpp
// Author      : RA-MM
// Version     : 0.1
// Description : Math library used for geometric calculation.
//============================================================================

#include "../inc/Math_Square.hpp"

namespace tri  {
namespace ad   {
namespace math {

bool isPointIntoSquare(Eigen::Vector3d pnt, Square sqForm){
    //int             crossProduct1,crossProduct2,crossProduct3,crossProduct4;
    Eigen::Vector3d tempVector1, tempVector2, tempVector3;

    tempVector1   = sqForm.p1 - sqForm.p0;
    tempVector2   = sqForm.p3 - sqForm.p0;
    tempVector3   =       pnt - sqForm.p0;

    double projP0P1 = tempVector3.dot(tempVector1/tempVector1.norm());
    double projP0P3 = tempVector3.dot(tempVector2/tempVector2.norm());

    if( projP0P1 >= 0 && projP0P1 <= tempVector1.norm() && projP0P3 >= 0 && projP0P3 <= tempVector2.norm() )
        return true;
    return false;
}

bool squareIntersect( Square square0 , Square square1){
    Eigen::Vector3d interPnt;

    if(isPointIntoSquare(square1.p0,square0) || isPointIntoSquare(square1.p1,square0) || isPointIntoSquare(square1.p2,square0) || isPointIntoSquare(square1.p3,square0)){
        return true;
    }

    if(isPointIntoSquare(square0.p0,square1) || isPointIntoSquare(square0.p1,square1) || isPointIntoSquare(square0.p2,square1) || isPointIntoSquare(square0.p3,square1)){
        return true;
    }

    return intersectionBetweenSegmentsIn2D(square0.p0, square0.p1, square1.p0, square1.p1, interPnt) ||
           intersectionBetweenSegmentsIn2D(square0.p0, square0.p1, square1.p1, square1.p2, interPnt) ||
           intersectionBetweenSegmentsIn2D(square0.p0, square0.p1, square1.p2, square1.p3, interPnt) ||
           intersectionBetweenSegmentsIn2D(square0.p0, square0.p1, square1.p3, square1.p0, interPnt) ||
           intersectionBetweenSegmentsIn2D(square0.p1, square0.p2, square1.p0, square1.p1, interPnt) ||
           intersectionBetweenSegmentsIn2D(square0.p1, square0.p2, square1.p1, square1.p2, interPnt) ||
           intersectionBetweenSegmentsIn2D(square0.p1, square0.p2, square1.p2, square1.p3, interPnt) ||
           intersectionBetweenSegmentsIn2D(square0.p1, square0.p2, square1.p3, square1.p0, interPnt) ||
           intersectionBetweenSegmentsIn2D(square0.p2, square0.p3, square1.p0, square1.p1, interPnt) ||
           intersectionBetweenSegmentsIn2D(square0.p2, square0.p3, square1.p1, square1.p2, interPnt) ||
           intersectionBetweenSegmentsIn2D(square0.p2, square0.p3, square1.p2, square1.p3, interPnt) ||
           intersectionBetweenSegmentsIn2D(square0.p2, square0.p3, square1.p3, square1.p0, interPnt) ||
           intersectionBetweenSegmentsIn2D(square0.p3, square0.p0, square1.p0, square1.p1, interPnt) ||
           intersectionBetweenSegmentsIn2D(square0.p3, square0.p0, square1.p1, square1.p2, interPnt) ||
           intersectionBetweenSegmentsIn2D(square0.p3, square0.p0, square1.p2, square1.p3, interPnt) ||
           intersectionBetweenSegmentsIn2D(square0.p3, square0.p0, square1.p3, square1.p0, interPnt);
}

} /* end of math */
} /* end of ad       */
} /* end of tri      */
