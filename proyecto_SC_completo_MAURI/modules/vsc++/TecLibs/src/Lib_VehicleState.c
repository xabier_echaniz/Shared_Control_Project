/*********************************************************************************************
 *** Name of the library:        CSV Management                                            ***
 *** Description of the library: This library is implemented to do the management of CSV   ***
 ***                             text files. All the procedures releated to them should be ***
 ***                             with the current library.                                 ***
 *** Autor notes:                TRI means Tecnalia Research and Innovation                ***
 *** Date:                       April 15th, 2016                                          ***
 *** Modifications date:         - - - - - - - - -                                         ***
 *** Version:                    V1.0                                                      ***
 *** Notes about modifications:                                                            ***
 *********************************************************************************************/

/*********************************************************************************************
 *** Libraries to be invoked                                                               ***
 *********************************************************************************************/
#include "../inc/Lib_VehicleState.h"

/*********************************************************************************************
 *** Functions                                                                             ***
 *********************************************************************************************/

 /* Name:              TRI_VehicleConfigInit
   Note:              - - - - - - -
   Input Parameters:
                      VehStateConfig: Structure with the configuration.
   Output Parameters:
                      returns a pointer to the configuration of vehicle state.
 */
 void TRI_VehicleStateConfigInit(TRI_VehicleState_Config *VehStateConfig){
    // Configuration:
    VehStateConfig->PlanningEndPnt.x = 0;
    VehStateConfig->PlanningEndPnt.y = 0;
    VehStateConfig->GPSOffset_Front = 0;
    VehStateConfig->GPSOffset_Back  = 0;
    VehStateConfig->GPSOffset_Left  = 0;
    VehStateConfig->GPSOffset_Right = 0;
    VehStateConfig->Long            = 0;
    VehStateConfig->Width           = 0;
    VehStateConfig->Height          = 0;
    VehStateConfig->Wheelbase       = 0;
    VehStateConfig->Track           = 0;
    VehStateConfig->Lookahead       = 0;
    VehStateConfig->PlanningRightTraffic = 0;
    VehStateConfig->PlanningRoadWidth    = 0;
 }


/* Name:              TRI_VehicleCmdInit
   Note:              - - - - - - -
   Input Parameters:
                      VehStateCmd: Structure with the commands.
   Output Parameters:
                      returns a pointer to the configuration of vehicle state.
 */
 void TRI_VehicleStateCmdInit(TRI_VehicleState_Commands *VehStateCmd){
    VehStateCmd->Run_Stop       = 0;
    VehStateCmd->Emergency_Stop = 0;
    VehStateCmd->PathMode       = 0;
    VehStateCmd->SPSpeed        = 0;
 }

  /* Name:              TRI_VehicleStateInputUpdate
   Note:              - - - - - - -
   Input Parameters:
                      VehState:      Structure with the vehicle state.
                      VehStateInput: the input data.
   Output Parameters:
                      returns a pointer to the vehicle state.
 */
 void TRI_VehicleStateInputInit(TRI_VehicleState_Input *VehStateInput){
    VehStateInput->X = 0;  			// X
    VehStateInput->Y = 0;   			// Y
	VehStateInput->CarSpeed = 0;   	// Speed of the car
    VehStateInput->Ctrl = 0;
    VehStateInput->HeadingAngle = 0;	// Yaw
    VehStateInput->Empty1 = 0;     	// Empty space
    VehStateInput->WheelAngle = 0; 	// Wheel angle
 }


/* Name:              TRI_VehicleStateInputUpdate
   Note:              - - - - - - -
   Input Parameters:
                      VehState:      Structure with the vehicle state.
                      VehStateInput: the input data.
   Output Parameters:
                      returns a pointer to the vehicle state.
 */
 void TRI_VehicleStateOutputInit(TRI_VehicleState_Output *VehStateOutput){
    VehStateOutput->Sts_Run_Stop = 0;  // Estado que indica si el sistema esta en Run (1) o Stop (0)
    VehStateOutput->Run_Stop_Past = 0; // Siempre se lleva registro del bit anterior que se dio con el boton de Run / Stop.
    VehStateOutput->StartPointX = 0;   //
    VehStateOutput->StartPointY = 0;   //
    VehStateOutput->EndPointX = 0;     //
    VehStateOutput->EndPointY = 0;     //
    VehStateOutput->CarSpeed = 0;      // speed in m/s
    VehStateOutput->X = 0;
    VehStateOutput->Y = 0;
    VehStateOutput->HeadingAngleRad = 0;
    VehStateOutput->HeadingAngleDeg = 0;
    VehStateOutput->WheelBase = 0;
    VehStateOutput->MaxSpeed = 0;
 }


/* Name:              TRI_VehicleInit
   Note:              - - - - - - -
   Input Parameters:
                      VehState: Structure with the vehicle state.
   Output Parameters:
                      returns a pointer to the vehicle state.
 */
 void TRI_VehicleStateInit(TRI_VehicleState *VehState){
    // Configuration:
    TRI_VehicleStateConfigInit(&(VehState->C));
    // Commands:
    TRI_VehicleStateCmdInit(&(VehState->Cmd));
    // Inputs:
    TRI_VehicleStateInputInit(&(VehState->I));
    // Output:
    TRI_VehicleStateOutputInit(&(VehState->O));
 }

 /* Name:              TRI_VehicleStateScale
   Note:              - - - - - - -
   Input Parameters:
                      VehState: Structure with the vehicle state.
                      Input:    the input data.
   Output Parameters:
                      returns a pointer to the vehicle state.
 */
 void TRI_VehicleStateScale(TRI_VehicleState *VehState){
	 double Vx;
	 double Vy;
     // Initialization of a temporal variable
     TRI_VehicleState VehicleStateTemp;
     TRI_VehicleStateInit(&VehicleStateTemp);

     // Tranfering information
	 VehicleStateTemp = *VehState;

     // Scale process

     //    Positions
     VehicleStateTemp.O.X = VehicleStateTemp.I.X; //* 111120; No es necesario el factor longitud  ya del bajo nivel vienen esos valores.
     VehicleStateTemp.O.Y = VehicleStateTemp.I.Y; //* 111319; No es necesario el factor latitud,  ya del bajo nivel vienen esos valores.

     //    Speed
     VehicleStateTemp.O.CarSpeed = (((float)VehicleStateTemp.I.CarSpeed) / 100.0f) / 3.6f;

     //    Yaw
     VehicleStateTemp.I.HeadingAngle = VehicleStateTemp.I.HeadingAngle;//(360.0 - VehicleStateTemp.I.HeadingAngle) + 90.0; // El GPS rota en sentido contrario y
     // Comando de inicio y parada: De ocurrir un flanco de subida se activa el comando Run o Stop dependiendo el caso
     if(VehicleStateTemp.O.Run_Stop_Past == 0 && VehicleStateTemp.Cmd.Run_Stop == 1){
        if(VehicleStateTemp.O.Sts_Run_Stop == 0){
            VehicleStateTemp.O.Sts_Run_Stop = 1; // Modo Run
        }else{
            VehicleStateTemp.O.Sts_Run_Stop = 0; // Modo Stop
        }
     }
     VehicleStateTemp.O.Run_Stop_Past = VehicleStateTemp.Cmd.Run_Stop;
     
     VehicleStateTemp.O.Sts_Run_Stop = VehicleStateTemp.Cmd.Run_Stop;
                                                                               // se tienen 90� de error.
     Vx = cos(VehicleStateTemp.I.HeadingAngle*M_PI/180.0);
     Vy = sin(VehicleStateTemp.I.HeadingAngle*M_PI/180.0);
     VehicleStateTemp.O.HeadingAngleRad = (float) atan2(Vy,Vx);
     VehicleStateTemp.O.HeadingAngleDeg = VehicleStateTemp.O.HeadingAngleRad*(180.0f/M_PI);

	 VehState->O = VehicleStateTemp.O;
 }
