/* ************************************************************************************************
   ** Libraries:                                                                                 **
   ************************************************************************************************ */
#include "../inc/Lib_Communications.h"
#include "time.h"


/* ************************************************************************************************
   ** Functions definition:                                                                      **
   ************************************************************************************************ */
// TRI_CommInitNode: Se encarga se inicializar un node de data con toda la informacion a cero.

void TRI_CommDateTime(tri_CommDateTime *datetime){
    // Time variables
    time_t timer;
    struct tm* tm_info;
    time(&timer);
    tm_info = localtime(&timer);
    
    datetime->year  = tm_info->tm_year + 1900;
	datetime->month = tm_info->tm_mon + 1;
	datetime->day   = tm_info->tm_mday;
	datetime->hour  = tm_info->tm_hour;
	datetime->min   = tm_info->tm_min;
	datetime->sec   = tm_info->tm_sec;
}

unsigned long TRI_CommSubTime(tri_CommDateTime datetime){
	time_t timer;
    struct tm* tm_info;
    unsigned long current = 0;
    unsigned long data    = 0;
    time(&timer);
    tm_info = localtime(&timer);
    
    if(datetime.year > (unsigned int)tm_info->tm_year + 1900){
        return 0;
    }else{
        current  = (tm_info->tm_year + 1900 - datetime.year )*365*24*60*60;
        current += (tm_info->tm_mon  + 1)*30*24*60*60;
        current += (tm_info->tm_mday)*24*60*60;
        current += tm_info->tm_hour*60*60;
        current += tm_info->tm_min*60;
        current += tm_info->tm_sec;
        
        data  = (0)*365*24*60*60;
        data += datetime.month*30*24*60*60;
        data += datetime.day*24*60*60;
        data += datetime.hour*60*60;
        data += datetime.min*60;
        data += datetime.sec;
        
        if(current < data){
            return 0;
        }else{
            //printf("Actual: %d/%d/%d %d:%d:%d   %d/%d/%d %d:%d:%d\n",tm_info->tm_year + 1900,tm_info->tm_mon  + 1,tm_info->tm_mday,tm_info->tm_hour,tm_info->tm_min,tm_info->tm_sec,datetime.year,datetime.month,datetime.day,datetime.hour,datetime.min,datetime.sec );
            return current - data;
        }
    }

}

void TRI_CommInitNode(tri_CommStruct *node){
    node->id = 0;
    node->type = 0;
    node->x = 0;
    node->y = 0;
    node->heading = 0;
    TRI_CommDateTime(&(node->datetime));
}

// TRI_CommPrintfNode:
void TRI_CommPrintfNode(tri_CommStruct node){
    printf("CommPrintfNode%d_%d_%d_%d:%d:%d: ", node.datetime.year,node.datetime.month,node.datetime.day,node.datetime.hour,node.datetime.min,node.datetime.sec);
    printf("Id: %d ", node.id);
    printf("Type: %d ", node.type);
    printf("X: %ld ", ((float)node.x)/100);
    printf("Y: %ld ",((float)node.y)/100);
    printf("Heading: %d ",((float)node.heading)/100);
    printf("Heading: %d ",((float)node.speed)/100);
    printf("\n");
}

// TRI_CommCreateNode: Se crea un node informacion con la data introducida.
tri_CommList *TRI_CommCreateNode(tri_CommStruct node){
	// Declaracion de variables.
	tri_CommList *Node;
	
	// Inicializacion de valores y reserva de espacio de memoria.
    Node=(tri_CommList *)malloc(sizeof(tri_CommList));
    if(Node == NULL){
		printf("TRI_CommCreateNode error: There are no space to create the communication node.");
        return NULL;
    }
	
	// Codigo.
	Node->Node.id             = node.id            ;
	Node->Node.type           = node.type          ;
	Node->Node.x              = node.x             ;
    Node->Node.y              = node.y             ;
	Node->Node.heading        = node.heading       ;
    Node->Node.speed          = node.speed         ;
	Node->Node.datetime.year  = node.datetime.year ;
	Node->Node.datetime.month = node.datetime.month;
	Node->Node.datetime.day   = node.datetime.day  ;
	Node->Node.datetime.hour  = node.datetime.hour ;
	Node->Node.datetime.min   = node.datetime.min  ;
	Node->Node.datetime.sec   = node.datetime.sec  ;
	Node->Next                = NULL               ;
	Node->Prev                = NULL               ;
	return Node;
}

// TRI_CommAddNodeList: A�ade un nodo al final 
tri_CommList *TRI_CommAddNodeList(tri_CommList *commList, tri_CommStruct node){

	if(commList == (tri_CommList*)NULL){
        commList = TRI_CommCreateNode(node);
    }
    else{
        tri_CommList *pointerList = commList;
        while (pointerList->Next != NULL){
            pointerList = pointerList->Next;
        }
        pointerList->Next       = TRI_CommCreateNode(node);
		pointerList->Next->Prev = pointerList;
    }
    return commList;
}

// TRI_CommDeleteNodeIdList:  
tri_CommList *TRI_CommUpdateNodeIdList(tri_CommList *commList,  tri_CommStruct node){	
    int detected = 0;
    tri_CommList *pointerList = commList;

	if(commList == (tri_CommList*)NULL){
        return TRI_CommAddNodeList(commList, node);;
    }
    while(pointerList != NULL){
		if(pointerList->Node.id == node.id){
			pointerList->Node = node;
            detected = 1;
		}
		pointerList = pointerList->Next;
    }
    if(detected == 0){
        commList = TRI_CommAddNodeList(commList, node);
    }
	return commList;  
}

// TRI_CommDeleteNodeIdList:  
tri_CommList *TRI_CommDeleteNodeIdList(tri_CommList *commList, unsigned int id){
	int breakloop = 0;
    tri_CommList *pointerList = commList;
	
	if(commList == (tri_CommList*)NULL){
        return commList;
    }
    
    while (pointerList->Next != NULL && breakloop == 0){
		if(pointerList->Node.id == id){
			breakloop = 1;
		}else{
			pointerList = pointerList->Next;
		}
    }
	if(breakloop == 1){
		tri_CommList *pointerListAux = pointerList;
		if(pointerList->Prev == NULL){
			if(pointerList->Next == NULL){
				commList = (tri_CommList*)NULL;
				return commList;
			}else{
				commList = pointerList->Next;
				commList->Prev    = NULL;
				pointerList->Next = NULL;
				return commList;
			}
		}else{
			if(pointerList->Next == NULL){
				pointerListAux = pointerList->Prev;
				pointerListAux->Next = NULL;
				pointerList->Prev = NULL;
				return commList;
			}else{
				pointerListAux          = pointerList->Prev;
				pointerListAux->Next    = pointerList->Next;
				pointerList->Next->Prev = pointerListAux;
				pointerList->Prev       = NULL;
				pointerList->Next       = NULL;
				return commList;
			}
		}
	}else{
		return commList;
	}   
}

// TRI_CommDeleteNodeTimeList:  
tri_CommList *TRI_CommDeleteNodeTimeList(tri_CommList *commList, int timeToClean){
    tri_CommList *pointerList = commList;
    tri_CommList *pointerListAux;

	if(commList == (tri_CommList*)NULL){
        return commList;
    }

    while (pointerList!= NULL){
        time_t timer;
        struct tm* tm_info;
        time(&timer);
    	tm_info = localtime(&timer);
		if(TRI_CommSubTime(pointerList->Node.datetime) >= (unsigned long) timeToClean){
			pointerListAux = pointerList;
            if(pointerList->Prev == NULL){
                if(pointerList->Next == NULL){
                	commList = (tri_CommList*)NULL;
                	return commList;
                }else{
                    commList = pointerList->Next;
                	commList->Prev    = NULL;
                	pointerList->Next = NULL;
                    free(pointerList);
                    pointerList = commList;
                }
            }else{
                if(pointerList->Next == NULL){
                    pointerListAux = pointerList->Prev;
                    pointerListAux->Next = NULL;
                    pointerList->Prev = NULL;
                    pointerList = NULL;
                }else{
                    pointerListAux          = pointerList->Prev;
                    pointerListAux->Next    = pointerList->Next;
                    pointerList->Next->Prev = pointerListAux;
                    pointerList->Prev       = NULL;
                    pointerList->Next       = NULL;
                    free(pointerList);
                    pointerList = pointerListAux->Next;
                }
            }
		}else{
            pointerList = pointerList->Next;
        }
    }
    return commList;
}

void TRI_CommDeleteList(tri_CommList **commList){
    tri_CommList *Current = *commList;
    tri_CommList *Next;
    while (Current != NULL) {
        Next = Current->Next;
        free(Current);
        Current = Next;
    }
    *commList = NULL;
}

unsigned int TRI_CommSizeOfList(tri_CommList *commList){
    tri_CommList *pointerList = commList;
    unsigned int i;
    i = 0;    
    printf("    ");
    while(pointerList != NULL){
        pointerList = pointerList->Next;
        i = i + 1;
    }
    return i;
}