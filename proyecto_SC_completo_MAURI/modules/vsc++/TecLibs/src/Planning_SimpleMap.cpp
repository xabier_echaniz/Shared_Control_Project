//============================================================================
// Name        : Planning_SimpleMap.cpp
// Author      : RL - MM
// Version     : 0.1
// Description :
//============================================================================


#include "../inc/Planning_SimpleMap.hpp"

using tri::ad::math::distance2Points;
using tri::ad::math::distPointSegment;

namespace tri      {
namespace ad       {
namespace planning {

SimpleMap::SimpleMap(){}

SimpleMap::~SimpleMap(){}

void SimpleMap::clear(){
	this->mapNodes.clear();
}

SimpleMapNode SimpleMap::getNodeInfo(const int & idx){
	return this->mapNodes[idx];
}

void SimpleMap::updateNodeSpeed(const int & idx, double speed){
    this->mapNodes[idx].maxSpeed = speed;
}

unsigned int SimpleMap::getSize(){
    return this->mapNodes.size();
}

void SimpleMap::appendNode(SimpleMapNode node){
    this->mapNodes.push_back(node);
}

void SimpleMap::reverseNodes(){
    std::reverse(this->mapNodes.begin(), this->mapNodes.end());
}

void SimpleMap::printFile(std::string directory, std::string name){
    std::ofstream myfile;
    std::string   printingString, addressFile;
    int           i;
    std::ostringstream ss;
    ss.precision(10);
    if(directory != "")
        addressFile = (directory + "//"+ name + tri::ad::time::timeString() + ".csv");
    else
        addressFile = (name + tri::ad::time::timeString() + ".csv");

    myfile.open(addressFile.c_str());

    if (myfile.is_open()){
        myfile << "id;x;y;z;type;k;nLanesRight;nLanesLeft;roadWidth;maxSpeed;yaw;\n";
        for(i = 0; i < (int)this->getSize(); i++){
            printingString = "";
            ss.str(""); ss.clear();
            ss << (this->mapNodes[i].id);
            printingString = printingString + ss.str() + ";";
            ss.str(""); ss.clear();
            ss << (this->mapNodes[i].x) ;
            printingString = printingString + ss.str() + ";";
            ss.str(""); ss.clear();
            ss << (this->mapNodes[i].y);
            printingString = printingString + ss.str() + ";";
            ss.str(""); ss.clear();
            ss << (this->mapNodes[i].z);
            printingString = printingString + ss.str() + ";";
            ss.str(""); ss.clear();
            ss << (this->mapNodes[i].type);
            printingString = printingString + ss.str() + ";";
            ss.str(""); ss.clear();
            ss << (this->mapNodes[i].k);
            printingString = printingString + ss.str() + ";";
            ss.str(""); ss.clear();
            ss << (this->mapNodes[i].nLanesRight);
            printingString = printingString + ss.str() + ";";
            ss.str(""); ss.clear();
            ss << (this->mapNodes[i].nLanesLeft);
            printingString = printingString + ss.str() + ";";
            ss.str(""); ss.clear();
            ss << (this->mapNodes[i].roadWidth);
            printingString = printingString + ss.str() + ";";
            ss.str(""); ss.clear();
            ss << (this->mapNodes[i].maxSpeed);
            printingString = printingString + ss.str() + ";";

            ss.str(""); ss.clear();
            ss << (this->mapNodes[i].yaw);
            printingString = printingString + ss.str() + ";\n";

            myfile << printingString;
        }
    }else{
        std::cout << ERROR0017 << std::endl;
    }
    myfile.close();
}

int SimpleMap::allIdsAreDifferent(){
    int i, j;
    for(i = 0; i < (int)this->getSize() - 1; ){
        for(j = i; j < (int)this->getSize() - 1; j++){
            if(this->getNodeInfo(i).id == this->getNodeInfo(j).id)
                return 0;
        }
    }
    return 1;
}

void SimpleMap::overwriteMapNodes(std::vector<SimpleMapNode> map){
    int i;

    this->clear();

    for(i = 0; i < (int)map.size(); i++){
        this->appendNode(map[i]);
    }
}

void SimpleMap::reIndexing(){
    int i;
    for( i= 0; i < (int)(this->getSize()); i++){
        this->mapNodes[i].id = i + 1;
    }
}

std::vector<SimpleMapNode> SimpleMap::getMapNodes(){
    return this->mapNodes;
}

int  SimpleMap::shortDistPointInLoop(int idxBef, int deltaIdxIn, bool loop, const Eigen::Vector3d & point, double & closestDistance, Eigen::Vector3d & closestPnt){
    Eigen::Vector3d tempPtn1, tempPtn2, tempClosestPtn; // Temporal variables
    double          temporalDst;
    int             i;
    int             idxSgm; // index of the shortest points and segments
    double          dstSgm; // shortest distance to a point and a segment
    bool            tempFlag1, tempFlag2;

    int deltaIdx = std::min(deltaIdxIn, (int)this->getSize()-1);
    int deltaMin, deltaMax;

    // The buffer of point is short
    if( (int)this->getSize() < 2 ){
        closestPnt << 0,0,0;
        return -1;
    }

    // Initializing segments
    idxSgm = -1;
    dstSgm = 999999999;

    // While of the segments (it is considered the last point because it is a loop).
    for(i=0; i < (int)this->getSize(); i++){
        tempPtn1 << this->getNodeInfo(i).x, this->getNodeInfo(i).y, this->getNodeInfo(i).z;
        if(i >= ((int)this->getSize()-1)){ // The last point of the trajectory
            tempPtn2 << this->getNodeInfo( 0 ).x,this->getNodeInfo( 0 ).y,this->getNodeInfo( 0 ).z;
        }else{
            tempPtn2 << this->getNodeInfo(i+1).x,this->getNodeInfo(i+1).y,this->getNodeInfo(i+1).z;
        }

        // closest point to segment
        // This conditions are with ID not INDEX please understand that
        tempFlag1 = fabs(idxBef - i) <= deltaIdx; // the id jump
        tempFlag2 = fabs(idxBef - i - ((int)this->getSize() - 1)) <= deltaIdx;
        //tempFlag2 = ( (idxBef - deltaIdx < 0) && ( ((int)this->getSize()-1) + idxBef - deltaIdx < i ) ) ||
        //( (idxBef + deltaIdx > (int)this->getSize()-1) && ( -((int)this->getSize()-1) + idxBef + deltaIdx > i ) );
        temporalDst = tri::ad::math::distPointSegment(tempPtn1 , tempPtn2, point, &tempClosestPtn );
        if( (tempFlag1 || tempFlag2) ){
            if(std::isnan(temporalDst)){
                //temporalDst = tri::ad::math::distance2Points(tempPtn1, point);
                temporalDst = (tempPtn1- point).norm();
                if(tri::ad::math::distPointLine(tempPtn1 , tempPtn2 - tempPtn1, point, &tempClosestPtn )<0){
                    temporalDst = -temporalDst;
                }
                tempClosestPtn = tempPtn1;
            }
            if( fabs(dstSgm) > fabs(temporalDst) && !(i >= ((int)this->getSize()-1) && !loop) ){// consideration if you are in a loop trajectory
                dstSgm     = temporalDst;
                idxSgm     = i;
                closestPnt = tempClosestPtn;
            }
        }
    }

    // if the segment index is out of range, it is picked the point distance
    closestDistance = dstSgm;
    return idxSgm;
}

std::vector<SimpleMapNode> SimpleMap::extractRoundaboutNodes(const int & idxRnbEntrance, int * idxRnbExit){
    std::vector<SimpleMapNode> roundaboutNodes;
    int i;

    roundaboutNodes.clear();

    i = idxRnbEntrance;

    while( this->getNodeInfo(i).type != RBEXIT){
        roundaboutNodes.push_back(this->getNodeInfo(i));
        i++;
    }
    roundaboutNodes.push_back(this->getNodeInfo(i));
    *idxRnbExit = i;

    return roundaboutNodes;
}


void SimpleMap::generateDataBufferInLoop(int index, double offsetInitial, double bufferDistance, std::vector<SimpleMapNode> &map){
    Eigen::Vector3d tmpPnt1, tmpPnt2;
    double offsetInternal = offsetInitial;
    double distInternal   = 0;
    int    internalIdx    = index;
    SimpleMapNode node;
    map.clear();

    if(offsetInternal  > this->distanceSegment(internalIdx, true)){
        offsetInternal = this->distanceSegment(internalIdx, true);
    }

    distInternal = offsetInternal;
    if(distInternal >= 0.1*bufferDistance){
        distInternal = 0.1*bufferDistance;
    }

    tmpPnt1(0) = (offsetInternal - distInternal)*cos(this->angleSegment(internalIdx, true)) + this->getNodeInfo(internalIdx).x;
    tmpPnt1(1) = (offsetInternal - distInternal)*sin(this->angleSegment(internalIdx, true)) + this->getNodeInfo(internalIdx).y;
    tmpPnt1(2) = 0;
    node   = this->getNodeInfo(internalIdx);
    node.x = tmpPnt1(0); node.y = tmpPnt1(1); node.z = tmpPnt1(2);
    map.push_back(node);
    while(distInternal <= 0.999*bufferDistance){
        internalIdx++;
        if(internalIdx > ((int)this->getSize()-1) ){
            internalIdx = 0;
        }
        node       = this->getNodeInfo(internalIdx);
        tmpPnt2(0) = node.x; tmpPnt2(1) = node.y; tmpPnt2(2) = node.z;
        if(distInternal + (tmpPnt1 - tmpPnt2).norm() >= bufferDistance){
            tmpPnt2 = tmpPnt1 + (bufferDistance - distInternal)*(tmpPnt2 - tmpPnt1)/(tmpPnt2 - tmpPnt1).norm();
        }
        distInternal = distInternal + (tmpPnt1 - tmpPnt2).norm();
        node.x = tmpPnt2(0); node.y = tmpPnt2(1); node.z = tmpPnt2(2);
        map.push_back(node);
        tmpPnt1 = tmpPnt2;
    }
}

double SimpleMap::distanceSegment(int index, bool loop){
    int internalIdx = index;
    Eigen::Vector3d tempPnt1, tempPnt2;
    this->lastNode(&internalIdx, loop);
    tempPnt1 << this->getNodeInfo(internalIdx).x,this->getNodeInfo(internalIdx).y,this->getNodeInfo(internalIdx).z;
    if( index == ((int)this->getSize()-1) ){
        tempPnt2 << this->getNodeInfo(0).x,this->getNodeInfo(0).y,this->getNodeInfo(0).z;
    }else{
        tempPnt2 << this->getNodeInfo(internalIdx+1).x,this->getNodeInfo(internalIdx+1).y,this->getNodeInfo(internalIdx+1).z;
    }
    return (tempPnt2 - tempPnt1).norm();
}

double SimpleMap::angleSegment(int index, bool loop){
    int             internalIdx = index;
    Eigen::Vector3d tempPnt1, tempPnt2;
    this->lastNode(&internalIdx, loop);
    tempPnt1 << this->getNodeInfo(internalIdx).x,this->getNodeInfo(internalIdx).y,this->getNodeInfo(internalIdx).z;
    if( index == ((int)this->getSize()-1) ){
        tempPnt2 << this->getNodeInfo(0).x,this->getNodeInfo(0).y,this->getNodeInfo(0).z;
    }else{
        tempPnt2 << this->getNodeInfo(internalIdx+1).x, this->getNodeInfo(internalIdx+1).y, this->getNodeInfo(internalIdx+1).z;
    }
    return tri::ad::math::angleVectorAroundZ(tempPnt2 - tempPnt1);
}

bool SimpleMap::lastNode(int *index, bool loop){
    if( *index >= (int)this->getSize()-2 &&  !loop ){
        *index = (int)this->getSize()-2;
        return true;
    }else if( *index >= (int)this->getSize()-1 && loop ){
        *index = (int)this->getSize()-1;
        return true;
    }
    return false;
}

void  SimpleMap::yawCalcInLoop(){
    int i = 0;
    Eigen::Vector3d p0, p1;
    if(this->getSize() >= 2){
        for(i = 0; i < (int)this->getSize(); i++){
            p0 << this->getNodeInfo(i+0).x, this->getNodeInfo(i+0).y, this->getNodeInfo(i+0).z;
            if(i >= (int)this->getSize() - 1){
                p1 << this->getNodeInfo(0).x, this->getNodeInfo(0).y, this->getNodeInfo(0).z;
            }else{
                p1 << this->getNodeInfo(i+1).x, this->getNodeInfo(i+1).y, this->getNodeInfo(i+1).z;
            }
            if((p1 - p0).norm() < 0.001){ //i > 0 && (p1 - p0).norm() < 0.01
                if(i >= (int)this->getSize() - 1){
                    this->mapNodes[i].yaw = this->mapNodes[0].yaw;
                }else{
                    this->mapNodes[i].yaw = this->mapNodes[i+1].yaw;
                }

            }else{
                this->mapNodes[i].yaw = tri::ad::math::angleVectorAroundZ(p1-p0);
            }
        }
    }
}


int   SimpleMap::distanceInFront(int idx, bool loop, double distance, Eigen::Vector3d & closestPnt){
    int             sgmIdx        = idx; // index of the segment at the end of the calculation.
    double          dstCalculated = 0;   // variable to accumulate the calculated distance to the end.
    Eigen::Vector3d pntBef(0,0,0);       // point used in the last iteration.
    Eigen::Vector3d vectorDirection, pntTemp;
    double          yaw, x, y;
    while(dstCalculated < distance){ // the 0.999 calue is to ensure that the calculated distance will end in the limit.
        x   = this->getNodeInfo(sgmIdx).x;
        y   = this->getNodeInfo(sgmIdx).y;
        yaw = this->getNodeInfo(sgmIdx).yaw;
        pntBef << getNodeInfo(sgmIdx).x,getNodeInfo(sgmIdx).y,0;

        sgmIdx    = sgmIdx + 1;
        if(sgmIdx > ( (int)this->getSize()-1 ) ){ // if there a no loop condition and the index is the last one
            if(loop){
                sgmIdx    = 0;
            }else{
                // the distance is completed with a infinite line
                sgmIdx          = (int)this->getSize()-1;
                x               = this->getNodeInfo(sgmIdx).x;
                y               = this->getNodeInfo(sgmIdx).y;
                yaw             = this->getNodeInfo(sgmIdx).yaw;
                vectorDirection = Eigen::Vector3d( cos(yaw) , sin(yaw) , 0 );
                pntTemp         = Eigen::Vector3d( x , y , 0 );
                closestPnt      = (distance - dstCalculated)*vectorDirection + pntTemp;
                return sgmIdx;
            }
        }

        vectorDirection = Eigen::Vector3d( cos(yaw) , sin(yaw) , 0 );
        pntTemp << getNodeInfo(sgmIdx).x,getNodeInfo(sgmIdx).y,0;
        if( (pntTemp - pntBef).norm() + dstCalculated >= distance){
            pntTemp = (distance - dstCalculated)*vectorDirection + pntBef;
            dstCalculated = distance;
        }else{
            dstCalculated = (pntTemp - pntBef).norm() + dstCalculated;
        }
        closestPnt = pntTemp;
    }
    if(sgmIdx-1 < 0){
        sgmIdx=(int)this->getSize();
    }
    return sgmIdx-1;
}

//void SimpleMap::createDataArray

} /* end of planning */
} /* end of ad       */
} /* end of tri      */
