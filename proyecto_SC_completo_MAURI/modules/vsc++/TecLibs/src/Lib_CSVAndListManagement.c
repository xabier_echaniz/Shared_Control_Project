/*********************************************************************************************
 *** Name of the library:        CSV Management                                            ***
 *** Description of the library: This library is implemented to do the management of CSV   ***
 ***                             text files. All the procedures releated to them should be ***
 ***                             with the current library.                                 ***
 *** Autor notes:                TRI means Tecnalia Research and Innovation                ***
 *** Date:                       April 13th, 2016                                          ***
 *** Modifications date:         - - - - - - - - -                                         ***
 *** Version:                    V1.0                                                      ***
 *** Notes about modifications:                                                            ***
 *********************************************************************************************/

/*********************************************************************************************
 *** Libraries to be invoked                                                               ***
 *********************************************************************************************/
#include "../inc/Lib_CSVAndListManagement.h"

/*********************************************************************************************
 *** Functions                                                                             ***
 *********************************************************************************************/

/* Name:             TRI_CreateNode
   Note:              - - - - - - -
   Input Parameters:
                      NodeInfo: Information to create on the node.
   Output Parameters:
                      returns a pointer to the node created.
 */
 TRI_CSV_List *TRI_CreateNode(TRI_CSV_NodeInfo NodeInfo){
    TRI_CSV_List *Node;
    Node=(TRI_CSV_List *)malloc(sizeof(TRI_CSV_List));
    if(Node == NULL){
        return NULL;
    }
	Node->Node.Id        = NodeInfo.Id;
	Node->Node.X         = NodeInfo.X;
	Node->Node.Y         = NodeInfo.Y;
	Node->Node.MaxSpeed  = NodeInfo.MaxSpeed;
	Node->Node.Type      = NodeInfo.Type;

	Node->Node.Curvature = NodeInfo.Curvature;
    Node->Node.Angle_In  = NodeInfo.Angle_In;
    Node->Node.Angle_Out = NodeInfo.Angle_Out;

	Node->Node.Di[0]     = NodeInfo.Di[0];
	Node->Node.Di[1]     = NodeInfo.Di[1];
	Node->Node.Di[2]     = NodeInfo.Di[2];
	Node->Node.Di[3]     = NodeInfo.Di[3];
	Node->Node.Di[4]     = NodeInfo.Di[4];
	Node->Node.Di[5]     = NodeInfo.Di[5];
	Node->Node.Do[0]     = NodeInfo.Do[0];
	Node->Node.Do[1]     = NodeInfo.Do[1];
	Node->Node.Do[2]     = NodeInfo.Do[2];
	Node->Node.Do[3]     = NodeInfo.Do[3];
	Node->Node.Do[4]     = NodeInfo.Do[4];
	Node->Node.Do[5]     = NodeInfo.Do[5];

	Node->Next           = NULL;

	return Node;
 }


/* Name:             TRI_DeleteList
   Note:              - - - - - - -
   Input Parameters:
                      LinkedList: Pointer to the list that is going to be deleted.
   Output Parameters:
                      Void.
 */
 void TRI_DeleteList(TRI_CSV_List **LinkedList){
    TRI_CSV_List *Current = *LinkedList;
    TRI_CSV_List *Next;
    while (Current != NULL) {
        Next = Current->Next;
        free(Current);
        Current = Next;
    }
    *LinkedList = NULL;
}


/* Name:             TRI_AddNodeBottomList
   Note:              - - - - - - -
   Input Parameters:
                      LinkedList: Pointer to the list.
                      NodeInfo:   Information to create on the node.
   Output Parameters:
                      A pointer to the new list.
 */
TRI_CSV_List *TRI_AddNodeBottomList(TRI_CSV_List *LinkedList, TRI_CSV_NodeInfo NodeInfo){
    TRI_CSV_List *NewNode = TRI_CreateNode(NodeInfo);
    if(LinkedList == (TRI_CSV_List*)NULL){
        LinkedList = TRI_CreateNode(NodeInfo);
    }
    else{
        TRI_CSV_List *Current = LinkedList;
        while (Current->Next != NULL){
            Current = Current->Next;
        }
        Current->Next = NewNode;
    }
    return LinkedList;
}


/* Name:             TRI_AddNodeTopList
   Note:              - - - - - - -
   Input Parameters:
                      LinkedList: Pointer to the list.
                      NodeInfo:   Information to create on the node.
   Output Parameters:
                      A pointer to the new list.
 */
TRI_CSV_List *TRI_AddNodeTopList(TRI_CSV_List *LinkedList, TRI_CSV_NodeInfo NodeInfo){
    TRI_CSV_List *NewNode = TRI_CreateNode(NodeInfo);
    if(LinkedList == (TRI_CSV_List*)NULL){
        LinkedList = TRI_CreateNode(NodeInfo);
    }
    else{
        NewNode->Next = LinkedList;
        LinkedList = NewNode;
    }
    return LinkedList;
}

/* Name:             TRI_AddNodeOffsetList
   Note:              - - - - - - -
   Input Parameters:
                      LinkedList: Pointer to the list.
                      NodeInfo:   Information to create on the node.
                      Offset:     the offset.
   Output Parameters:
                      A pointer to the new list.
 */
TRI_CSV_List *TRI_AddNodeOffsetList(TRI_CSV_List *LinkedList, TRI_CSV_NodeInfo NodeInfo, int Offset){
    if(LinkedList == (TRI_CSV_List*)NULL || Offset < 0){
        LinkedList = TRI_CreateNode(NodeInfo);
    }
    else{
        if(Offset >= TRI_SizeList(LinkedList) || Offset <= 0){
            LinkedList = TRI_AddNodeBottomList(LinkedList, NodeInfo);
        }else{
            TRI_CSV_List *Current = LinkedList;
            TRI_CSV_List *NewNode = TRI_CreateNode(NodeInfo);
            int i = 1;
            while (i < Offset){
                Current = Current->Next;
                i = i + 1;
            }
            NewNode->Next = Current->Next;
            Current->Next = NewNode;
        }
    }
    return LinkedList;
}

int TRI_MovePointerForward(TRI_CSV_List **LinkedList){
    if((*LinkedList)->Next == NULL){return -1;}
    (*LinkedList) = (*LinkedList)->Next;
    return 0;
}

TRI_CSV_NodeInfo TRI_ExtractNodeInfo(TRI_CSV_NodeInfo Node){
    TRI_CSV_NodeInfo oNode;

    oNode.Id        = Node.Id;
	oNode.X         = Node.X;
	oNode.Y         = Node.Y;
	oNode.MaxSpeed  = Node.MaxSpeed;
	oNode.Type      = Node.Type;

	oNode.Curvature = Node.Curvature;
    oNode.Angle_In  = Node.Angle_In;
    oNode.Angle_Out = Node.Angle_Out;

	oNode.Di[0]     = Node.Di[0];
	oNode.Di[1]     = Node.Di[1];
	oNode.Di[2]     = Node.Di[2];
	oNode.Di[3]     = Node.Di[3];
	oNode.Di[4]     = Node.Di[4];
	oNode.Di[5]     = Node.Di[5];
	oNode.Do[0]     = Node.Do[0];
	oNode.Do[1]     = Node.Do[1];
	oNode.Do[2]     = Node.Do[2];
	oNode.Do[3]     = Node.Do[3];
	oNode.Do[4]     = Node.Do[4];
	oNode.Do[5]     = Node.Do[5];

    return oNode;
}

/* Name:             TRI_SizeList
   Note:              - - - - - - -
   Input Parameters:
                      LinkedList: Pointer to the list.
   Output Parameters:
                      The number of elements of the list.
 */
int TRI_SizeList(TRI_CSV_List *LinkedList){
    if(LinkedList == (TRI_CSV_List*)NULL){
        return 0;
    }
    else{
        int i = 1;
        TRI_CSV_List *Current = LinkedList;
        while (Current->Next != NULL){
            i = i + 1;
            Current = Current->Next;
        }
        return i;
    }
}



TRI_CSV_List *TRI_ListPointerPosition(TRI_CSV_List *LinkedList, int Position){
    TRI_CSV_List *LinkedListAux = LinkedList;
    int i = 1;
    if(Position > TRI_SizeList(LinkedList)){
        return LinkedListAux;
    }else{
        while(i < Position){
            LinkedListAux = LinkedListAux->Next;
            i = i + 1;
        }
        return LinkedListAux;
    }
}

/* Name:              TRI_Read_CSV
   Note:              - - - - - - -
   Input Parameters:
                      LinkedList: List of elements that are part of the CSV
                      FileName:   Name of the file to be open.
   Output Parameters:
                      The function return 0 whether everything is OK or less 0 than if something is wrong
                      -1 means: The .CSV does not exist.
                      -2 means: The .CSV is empty.
                      -3 means: There is a mistake in the header of the CSV.
                      -4 means: Less than 2 coordinates on map.
 */
int TRI_Read_CSV(TRI_CSV_List **LinkedList, char *FileName){
    // Declaracion de variables
    // Caso 1
    FILE *PathFile = fopen(FileName,"r"); // Opening of the .CSV
    // Caso 2
    char   *CSVLine = (char *)NULL;
    size_t  Len;
    // Caso 3
    char *Temp_String = (char *)NULL;
    char LastWord[3]  = "";
    int   AccumulatedError = 0;
    // Caso 4
    int i = 1;
    TRI_CSV_NodeInfo NodeInfo;
    char *Element;
    
    printf("CSV Read: Inicio la lectura de '%s'.\n", FileName);
   //TRI_CleanLinkedData(&(**LinkedData));
    //  Caso 1 de error:
    if(PathFile == NULL){ // Error -1 implica que no se consiguio el archivo
        printf("CSV Read: Error. No se consiguio path.\n");
		return -1;
	}

	// Caso 2 de error: The CSV exist and could be read or could be a mistake at the header
    if(TRI_GetLine(&CSVLine, &Len, PathFile) == -1){
        fclose(PathFile);
        printf("CSV Read: Error. Esta vacio.\n");
        return -2; // El archivo esta vacio
    }

    // Caso 3 de error: Verificacion del header
    // Verifying header
    Temp_String = strtok(CSVLine,";");
    if(strcmp(Temp_String, "X") != 0){ AccumulatedError = AccumulatedError + 1;}

    strcpy(CSVLine,CSVLine+(strlen(Temp_String)+1));
    Temp_String = strtok(CSVLine,";");
    if(strcmp(Temp_String, "Y") != 0){ AccumulatedError = AccumulatedError + 1;}

    strcpy(CSVLine,CSVLine+(strlen(Temp_String)+1));
    Temp_String = strtok(CSVLine,";");
    if(strcmp(Temp_String, "Max Speed") != 0){ AccumulatedError = AccumulatedError + 1;}

    strcpy(CSVLine,CSVLine+(strlen(Temp_String)+1));
    Temp_String = strtok(CSVLine,";");
    if(strcmp(Temp_String, "Type") != 0){ AccumulatedError = AccumulatedError + 1;}

    strcpy(CSVLine,CSVLine+(strlen(Temp_String)+1));
    Temp_String = strtok(CSVLine,";");
    if(strcmp(Temp_String, "Curvature_Merging") != 0){ AccumulatedError = AccumulatedError + 1;}

    strcpy(CSVLine,CSVLine+(strlen(Temp_String)+1));
    Temp_String = strtok(CSVLine,";");
    if(strcmp(Temp_String, "Angle_In") != 0){ AccumulatedError = AccumulatedError + 1;}

    strcpy(CSVLine,CSVLine+(strlen(Temp_String)+1));
    Temp_String = strtok(CSVLine,";");
    if(strcmp(Temp_String, "Angle_Out") != 0){ AccumulatedError = AccumulatedError + 1;}

    strcpy(CSVLine,CSVLine+(strlen(Temp_String)+1));
    Temp_String = strtok(CSVLine,";");
    if(strcmp(Temp_String, "D0") != 0){ AccumulatedError = AccumulatedError + 1;}

    strcpy(CSVLine,CSVLine+(strlen(Temp_String)+1));
    Temp_String = strtok(CSVLine,";");
    if(strcmp(Temp_String, "D1") != 0){ AccumulatedError = AccumulatedError + 1;}

    strcpy(CSVLine,CSVLine+(strlen(Temp_String)+1));
    Temp_String = strtok(CSVLine,";");
    if(strcmp(Temp_String, "D2") != 0){ AccumulatedError = AccumulatedError + 1;}

    strcpy(CSVLine,CSVLine+(strlen(Temp_String)+1));
    Temp_String = strtok(CSVLine,";");
    if(strcmp(Temp_String, "D3") != 0){ AccumulatedError = AccumulatedError + 1;}

    strcpy(CSVLine,CSVLine+(strlen(Temp_String)+1));
    Temp_String = strtok(CSVLine,";");
    if(strcmp(Temp_String, "D4") != 0){ AccumulatedError = AccumulatedError + 1;}

    strcpy(CSVLine,CSVLine+(strlen(Temp_String)+1));
    Temp_String = strtok(CSVLine,";");
    strncpy(LastWord, Temp_String,2);
    if(strcmp(LastWord, "D5") != 0){AccumulatedError = AccumulatedError + 1;}

    if(AccumulatedError > 0){ // Algun elemento del header esta malo
        printf("CSV Read: Error. El header esta malo.\n");
        fclose(PathFile);
        return -3;
    }

    // Caso 4 de error y lectura de los nodos

    TRI_DeleteList(&(*LinkedList));
    printf("CSV Read: Comenzo la lectura de los elementos.\n");
    while(TRI_GetLine(&CSVLine, &Len, PathFile) != -1){

        NodeInfo.Id = i;
        Element = strtok(CSVLine,";");
        if(Element == NULL){
            TRI_DeleteList(&(*LinkedList));
            fclose(PathFile);
            return (-100 - i);
        }
        NodeInfo.X = atof(Element);
        strcpy(CSVLine,CSVLine+(strlen(Element)+1));

        Element = strtok(CSVLine,";");
        if(Element == NULL){
            TRI_DeleteList(&(*LinkedList));
            fclose(PathFile);
            return (-100 - i);
        }
        NodeInfo.Y = atof(Element);
        strcpy(CSVLine,CSVLine+(strlen(Element)+1));

        Element = strtok(CSVLine,";");
        if(Element == NULL){
            TRI_DeleteList(&(*LinkedList));
            fclose(PathFile);
            return (-100 - i);
        }
        NodeInfo.MaxSpeed = atof(Element);
        strcpy(CSVLine,CSVLine+(strlen(Element)+1));

        Element = strtok(CSVLine,";");
        if(Element == NULL){
            TRI_DeleteList(&(*LinkedList));
            fclose(PathFile);
            return (-100 - i);
        }
        NodeInfo.Type = atoi(Element);
        strcpy(CSVLine,CSVLine+(strlen(Element)+1));

        Element = strtok(CSVLine,";");
        if(Element == NULL || (fabs(atof(Element)) > 0.3 && NodeInfo.Type == ROUNDABOUT)){
            TRI_DeleteList(&(*LinkedList));
            fclose(PathFile);
            return (-100 - i);
        }
        NodeInfo.Curvature = atof(Element);
        strcpy(CSVLine,CSVLine+(strlen(Element)+1));

        Element = strtok(CSVLine,";");
        if(Element == NULL){
            TRI_DeleteList(&(*LinkedList));
            fclose(PathFile);
            return (-100 - i);
        }
        NodeInfo.Angle_In = (atof(Element));
        if(NodeInfo.Angle_In > 90){NodeInfo.Angle_In = 90;}
        if(NodeInfo.Angle_In < 0 ){NodeInfo.Angle_In = 0 ;}
        strcpy(CSVLine,CSVLine+(strlen(Element)+1));

        Element = strtok(CSVLine,";");
        if(Element == NULL){
            TRI_DeleteList(&(*LinkedList));
            fclose(PathFile);
            return (-100 - i);
        }
        NodeInfo.Angle_Out = (atof(Element));
        if(NodeInfo.Angle_Out > 90){NodeInfo.Angle_Out = 90;}
        if(NodeInfo.Angle_Out < 0 ){NodeInfo.Angle_Out = 0 ;}
        strcpy(CSVLine,CSVLine+(strlen(Element)+1));

        Element = strtok(CSVLine,";");
        if(Element == NULL){
            TRI_DeleteList(&(*LinkedList));
            fclose(PathFile);
            return (-100 - i);
        }
        NodeInfo.Di[0] = atof(Element);
        strcpy(CSVLine,CSVLine+(strlen(Element)+1));

        Element = strtok(CSVLine,";");
        if(Element == NULL){
            TRI_DeleteList(&(*LinkedList));
            fclose(PathFile);
            return (-100 - i);
        }
        NodeInfo.Di[1] = atof(Element);
        strcpy(CSVLine,CSVLine+(strlen(Element)+1));

        Element = strtok(CSVLine,";");
        if(Element == NULL){
            TRI_DeleteList(&(*LinkedList));
            fclose(PathFile);
            return (-100 - i);
        }
        NodeInfo.Di[2] = atof(Element);
        strcpy(CSVLine,CSVLine+(strlen(Element)+1));

        Element = strtok(CSVLine,";");
        if(Element == NULL){
            TRI_DeleteList(&(*LinkedList));
            fclose(PathFile);
            return (-100 - i);
        }
        NodeInfo.Di[3] = atof(Element);
        strcpy(CSVLine,CSVLine+(strlen(Element)+1));

        Element = strtok(CSVLine,";");
        if(Element == NULL){
            TRI_DeleteList(&(*LinkedList));
            fclose(PathFile);
            return (-100 - i);
        }
        NodeInfo.Di[4] = atof(Element);
        strcpy(CSVLine,CSVLine+(strlen(Element)+1));

        Element = strtok(CSVLine,";");
        if(Element == NULL){
            TRI_DeleteList(&(*LinkedList));
            fclose(PathFile);
            return (-100 - i);
        }
        NodeInfo.Di[5] = atof(Element);
        strcpy(CSVLine,CSVLine+(strlen(Element)+1));

        (*LinkedList) = TRI_AddNodeBottomList((*LinkedList), NodeInfo);
        i = i+1;
    }

    // Caso 5 de error: menos de dos puntos en los nodos
    if(TRI_SizeList(*LinkedList)<2){
        printf("CSV Read: Error. Tiene un solo punto el path.\n");
        TRI_DeleteList(&(*LinkedList));
        fclose(PathFile);
        return -4;
    }

    printf("CSV Read: Finalizo la lectura.\n");
    fclose(PathFile);
    return 0;
 }

/* Name:              TRI_Getdelim
   Note:              The substitute of getdelim() of linux platform in windows.
   Input Parameters:

   Output parameters:
   */
size_t TRI_Getdelim(char **linep, size_t *n, int delim, FILE *fp){
    int ch;
    size_t i = 0;
    if(!linep || !n || !fp){
        errno = EINVAL;
        return -1;
    }
    if(*linep == NULL){
        if(NULL==(*linep = malloc(*n=128))){
            *n = 0;
            errno = ENOMEM;
            return -1;
        }
    }
    while((ch = fgetc(fp)) != EOF){
        if(i + 1 >= *n){
            char *temp = realloc(*linep, *n + 128);
            if(!temp){
                errno = ENOMEM;
                return -1;
            }
            *n += 128;
            *linep = temp;
        }
        (*linep)[i++] = ch;
        if(ch == delim)
            break;
    }
    (*linep)[i] = '\0';
    return !i && ch == EOF ? -1 : i;
}

/* Name:              TRI_GetLine
   Note:              The substitute of getline() of linux platform in windows.
   Input Parameters:

   Output parameters:
   */
size_t TRI_GetLine(char **linep, size_t *n, FILE *fp){
    return TRI_Getdelim(linep, n, '\n', fp);
}


