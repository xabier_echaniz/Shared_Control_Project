/**
 * @ copyright INRIA (c) 2014
 * @ license This source-code is submitted to the CeCILL A license (http://www.cecill.info/)
 * @ Joshue Perez and Vicente Milanes
 * @ joshue.perez_rastelli@inria.fr, vicente.milanes@inria.fr
 * @ file FEMot.cpp
 * @ brief TemplateProject RTMaps Component source file
 * @ details This component implements a template project
 * @ version 1.0
 * @ date 02-07-2014
 * @ bug	No known bugs
 * @ warning No warnings
 */

#include <algorithm> // for max, min

#include "../inc/TRIFuzzy.hpp"
using namespace std;


/*---------------STRUCT LABEL------------------*/
/*
Constructor of Label struct
@deprecated Empty Constructor
*/
Label::Label(){
	type = 0;
	values[0] = INFINITO;
	values[1] = INFINITO;
	values[2] = INFINITO;
	values[3] = INFINITO;
	degree = 0;
	strcpy(name,"		");
};

/*
Constructor of Label struct
@deprecated initializes the label type, values and name
@param Type: Integer. Type of label.
@param Values: Double array. Values of label, depending on type.
@param Name: String. Label name
*/
Label::Label(int Type, double * Values, char * Name){
	int tam = 0;
	strcpy(name, Name);
	type = Type;
	degree = 0;
        values[0] = INFINITO;
	values[1] = INFINITO;
	values[2] = INFINITO;
	values[3] = INFINITO;
	switch(type){
		case(TRI): tam = 3; break;
		case(TRA): tam = 4; break;
		case(GSS): tam = 2; break;
		case(BLL): tam = 3; break;
		case(FPI): tam = 4; break;
		default: throw Error(4);
	}
	Copy_Double_Array(values,Values,tam);
};


/*
Destructor of Label struct
*/
Label::~Label(){};

/*
Duplicates an existent label. Overloads =
@deprecated: Given a label it copies it.
@param: Original: Label. The label we want to duplicate.
@see Copy_Array_Label, Add_Label, Delete label, GetLabel, SetLabel
@throw exception for inalid type
*/
void Label::Equal(Label * original){
    int tam = 0;
	(*original).GetName(name);
	type =  (*original).GetType();
	degree = (*original).GetDegree();
	switch(type){
		case(TRI): tam = 3; break;
		case(TRA): tam = 4; break;
		case(GSS): tam = 2; break;
		case(BLL): tam = 3; break;
		case(FPI): tam = 4; break;
		default: throw Error(4);
	}
    for(int i = 0; i < tam ; i++) {values[i] = (*original).GetValue(i);}
};

/*
Gets type of label
@return Integer that represents the label type
*/
int Label::GetType(){ return type;};

/*
Gets label values
@return Double array. Depending on the type the array�s size changes.
*/
void Label::GetValues(double * labelValues){
	int tam;
	switch(type){
		case(TRI): tam = 3; break;
		case(TRA): tam = 4; break;
		case(GSS): tam = 2; break;
		case(BLL): tam = 3; break;
		case(FPI): tam = 4; break;
	}
	Copy_Double_Array(labelValues,values,tam);
};

/*
Gets label name
@return String. Label name.
*/
void Label::GetName(char * ret){strcpy(ret,name);};

/*
Gets label degree
@return Double. Label degree.
*/
double Label::GetDegree(){return degree;};

/*
Gets a specific label value
@return Double. Label value in the position index of the array.
@throw exception for invalid index
*/
double Label::GetValue(int index){
    switch(type){
		case(TRI): if(index >= 3 || index < 0) throw Error(25,".triangulo."); break;
		case(TRA): if(index >= 4 || index < 0) throw Error(25,".trapecio."); break;
		case(GSS): if(index >= 2 || index < 0) throw Error(25,".gauss."); break;
		case(BLL): if(index >= 3 || index < 0) throw Error(25,".bell."); break;
		case(FPI): if(index >= 4 || index < 0) throw Error(25,".fpi."); break;
	}
	return values[index];
};

/*
Sets label values
@param Values:Double array. Label values to be set.
@param Type: Integer. Label type to be set.
*/
void Label::SetValues(double * Values, int Type){
	int tam;
	type = Type;
	switch(type){
		case(TRI): tam = 3; break;
		case(TRA): tam = 4; break;
		case(GSS): tam = 2; break;
		case(BLL): tam = 3; break;
		case(FPI): tam = 4; break;
		default: throw Error(4);
	}
	Copy_Double_Array(values,Values,tam);
};

/*
Sets label name
@param Name:String. Label name to be set.

*/
void Label::SetName(char * Name){ strcpy(name,Name);};

/*
Sets label degree
@param Degree:Double. Label degree to be set.
*/
void Label::SetDegree(double Degree){degree = Degree;};

/*
Sets a specific value of a label
@param Index:Integer. Index of value to be set.
@param Value: Double. Valuse to be set
@throw exception if is an invalid index
*/
void Label::SetValue(int index, double Value){
	switch(type){
		case(TRI): if(index >= 3 || index < 0) throw Error(25); break;
		case(TRA): if(index >= 4 || index < 0) throw Error(25); break;
		case(GSS): if(index >= 2 || index < 0) throw Error(25); break;
		case(BLL): if(index >= 3 || index < 0) throw Error(25); break;
		case(FPI): if(index >= 4 || index < 0) throw Error(25); break;
	}
	values[index]=Value;
};

/*
Shows a label on console depending on type
*/
void Label::Show(){
	int Nvalues;
	char Type[4];
	switch(type){
		case(TRI): strcpy(Type,"TRI"); Nvalues = 3; break;
		case(TRA): strcpy(Type,"TRA"); Nvalues = 4; break;
		case(GSS): strcpy(Type,"GSS"); Nvalues = 2; break;
		case(BLL): strcpy(Type,"BLL"); Nvalues = 3; break;
		case(FPI): strcpy(Type,"PI"); Nvalues = 4; break;
	}
	cout << name  << " " << Type << " ";
	for(int i = 0; i < Nvalues ; i++ ){
		cout << values[i] << " ";
	}
	cout << "	�" << degree<< endl;
};

/*---------------STRUCT VAR_ENTRY------------------*/
/*
Constructor of Var_Entry struct
@deprecated empty constructor
*/
Var_Entry::Var_Entry(){
	strcpy(name,"		");
	Nlabels = 0;
	range[INF] =-INFINITO;
	range[SUP] =INFINITO;
	//Labels = NULL;
};

/*
Constructor of Var_Entry struct
@deprecated creates an entry inizializating name, range, number of labels and labels.
@param Name: String. Entry name
@param N_labels: Integer. number of labels.
@param Range: Double array. Range of entry
@param labels: Label array. Labels of entry.
*/
Var_Entry::Var_Entry(char * Name, int N_labels, double Range[2], Label * labels){
	strcpy(name, Name);
	range[INF] = Range[INF];
	range[SUP] = Range[SUP];
        SetLabels(labels,N_labels);
};

/*
Destructor of Var_Entry struct
*/
Var_Entry::~Var_Entry(){
	if(Nlabels > 0) {
		delete [] Labels;
	}
};

/*
Duplicates an existent entry.
@deprecated: Given an entry it copies it and returns it.
@param: Original: Var_Entry. The entry we want to duplicate.
@return: Var_Entry. Duplicated entry.
@see Copy_Array_Label, Add_Entry, Delete_Entry, Get_Entry
*/
void Var_Entry::Equal(Var_Entry * original){
	(*original).GetName(name);
	range[INF] = (*original).GetRangeInf();
	range[SUP] = (*original).GetRangeSup();

	if(Nlabels > 0)
		delete [] Labels;

	Nlabels = (*original).GetNlabels();
	if(Nlabels>0){
                Labels = new Label[Nlabels];
        	for(int i = 0;i < Nlabels ; i++){
        		Label aux;
        		(*original).GetLabel(&aux,i);
        		Labels[i].Equal(&aux);
        	}
        }
};

/*
Returns the entry name
@return Entry Name
*/
void Var_Entry::GetName(char * ret){strcpy(ret,name);};

/*
Returns the number of labels of the entry
@return Entry Nlabels
*/
int Var_Entry::GetNlabels(){ return Nlabels;};

/*
Returns the entry range
@return Entry Range
*/
void Var_Entry::GetRange(double * Range){
	Range[INF] = range[INF];
	Range[SUP] = range[SUP];
};

/*
Returns the inferior limit of the entry range
@return Inferior limit of range
*/
double Var_Entry::GetRangeInf(){return range[INF];};

/*
Returns the superior limit of the entry range
@return Superior limit of range
*/
double Var_Entry::GetRangeSup(){return range[SUP];};

/*
Returns the labels of the entry
@return Label array.
*/
void Var_Entry::GetLabels(Label * ret){
	Copy_Array_Label(ret,Labels,Nlabels);
};

/*
Returns a specific label indicated by the index
@return Label in index position
*/
void Var_Entry::GetLabel(Label * ret,int index) {//OJO
	(*ret).Equal(&Labels[index]);

};

/*
Sets the entry name
@param Name: String. Entry name to be set.
*/
void Var_Entry::SetName(char * Name){strcpy(name,Name);};

/*
Sets the entry range
@param rge: Double array. Of 2 elementes. New range of entry.
@throw exception if inferior limit is not smaller than the superior one.
*/
void Var_Entry::SetRange(double rge[2]){
	if (rge[INF] > rge[SUP]) throw Error(1);
	range[INF] = rge[INF];
	range[SUP] = rge[SUP];
};

/*
Sets the inferior limit of the entry�s range
@param inf: Double. New inferior limit
@throw exception if inferior limit is not smaller than the superior one.
*/
void Var_Entry::SetRangeInf(double inf){
	if (inf > range[SUP]) throw Error(1);
	range[INF] = inf;
};

/*
Sets the superior limit of the entry�s range
@param sup: Double. New superior limit
@throw exception if inferior limit is not smaller than the superior one.
*/
void Var_Entry::SetRangeSup(double sup){
	if (range[INF] > sup) throw Error(1);
	range[SUP] = sup;
};

/*
Set the label array of the entry
@param labels: Label array. To be setted
@param size: Integer. New number of labels
*/
void Var_Entry::SetLabels(Label * labels, int size){
	if(Nlabels > 0)delete [] Labels;
	Nlabels = size;
	if(Nlabels > 0){
                Labels = new Label[Nlabels];
        	Copy_Array_Label(Labels,labels,Nlabels);
        }
};

/*
Set a specific label on the index position
@param index: Integer. Index of label to be setted.
@param label: Label. New label to be setted.
*/
void Var_Entry::SetLabel(int index, Label * label){
	if(index < 0 || index >= Nlabels) throw Error(16);
	Labels[index].Equal(label);
};


/*
Adds a new label to the entry
@param label: Label. New label.
*/
void Var_Entry::AddLabel(Label * label){
	Label * aux = new Label[Nlabels+1];
	int i = 0;
	for(i = 0; i < Nlabels ; i++)
		aux[i].Equal(&Labels[i]);
	aux[i].Equal(label);
	SetLabels(aux,Nlabels+1);
	delete [] aux;
};

/*
Deletes a specific label.
@param index: Integer. Index of label to be deleted.
@throw exception if index is not a position in the label array of the entry
*/
void Var_Entry::DeleteLabel(int index){
	if(index < 0 || index >= Nlabels) throw Error(16);
	Label * aux = NULL;
        if((Nlabels-1) >  0){
                aux = new Label[Nlabels-1];
        	for(int i = 0; i < index ; i++)
        		aux[i].Equal(&Labels[i]);
        	for(int i = (index+1); i < Nlabels ; i++)
        		aux[i-1].Equal(&Labels[i]);
        }
	SetLabels(aux,Nlabels-1);
	if((Nlabels) >  0)delete [] aux;
};

/*
Shows an entry on console
*/
void Var_Entry::Show(){
	cout<< "	" << name << " [" <<  range[INF] << "," << range[SUP] << "] {" << endl;
	for(int i = 0; i < Nlabels ; i++){
		cout << "		";
		Labels[i].Show();
	}
	cout << "	}\n"<< endl;
};

/*---------------STRUCT SINGLETON------------------*/
/*
Constructor of struct singleton
@deprecated Inizialites the name and value of a singleton
@param Name: String. Singleton name
@param Value: Singleton value
*/
Singleton::Singleton(char * Name,double  Value){
	strcpy(name, Name);
	value = Value;
};

/*
Duplicates an existent Singleton.
@deprecated: Given a singleton it copies it and returns it.
@param: Original: Singleton. The singleton we want to duplicate.
@return: Copy_Sinbleton. Duplicate_Out, Add_Out_Value, Delete_Out_Value, Get_Singleton.
*/
void Singleton::Equal(Singleton * original){
	strcpy(name, (*original).name);
	value = (*original).value;
};

/*
Returns the singleton name
@return String. Singleton name
*/
void Singleton::GetName(char * ret){strcpy(ret,name);};

/*
Returns the singleton value
@return Double. Singleton value
*/
double Singleton::GetValue(){return value;};

/*
Sets the singleton name
@param Name: String. Singleton�s new name
*/
void Singleton::SetName(char * Name){strcpy(name,Name);};

/*
Sets the singleton value
@param Value: Double. Singleton�s new value
*/
void Singleton::SetValue(double Value){value = Value;};

/*
Shows a singleton in a console
*/
void Singleton::Show(){
	cout << name  << " " << value << endl;
};

/*---------------STRUCT VAR_OUT------------------*/
/*
Constructor of a Var_Out struct
@deprecated Empty Constructor
*/
Var_Out::Var_Out(){
	strcpy(name, "");
	Nvalues = 0;
	//values = NULL;
};

/*
Constructor of a Var_Out struct
@deprecated Inizializates name, number of singletons and singoletos array.
@param Name: String. Out name
@param N_values: Integer. Number of values
@param Values: Singleton Array. Values array
*/
Var_Out::Var_Out(char * Name, int N_values, Singleton * Values){
	Nvalues = 0;
	strcpy(name, Name);
	SetSingletons(Values,N_values);
};

/*
Destructor of Var_Out struct
*/
Var_Out::~Var_Out(){
	if(Nvalues > 0) {
		delete [] values;
	}
};

/*
Duplicates an existent Out.
@deprecated: Given an Var_Out it copies it and returns it.
@param: Original: Var_Out. The out we want to duplicate.
@return: Var_Out. Duplicated Out.
@see Copy_Array_Singleton, Copy_Array_Out, Add_Out, Delete_Out, GetOut
*/
void Var_Out::Equal(Var_Out * original){
	(*original).GetName(name);
	if(Nvalues > 0)	delete [] values;
        Nvalues= (*original).GetNvalues();
        if(Nvalues > 0){
                values = new Singleton[Nvalues];
                for(int i = 0;i < Nvalues ; i++){
                        values[i].value = (*original).values[i].value;
                        strcpy(values[i].name,(*original).values[i].name);
        	}
        }
};

/*
Returns the out name
@return String. Out name
*/
void Var_Out::GetName(char * ret){ strcpy(ret,name);};

/*
Returns the number of values of the out
@return Integer. Out Nvalues
*/
int Var_Out::GetNvalues(){return Nvalues;};

/*
Returns values of the out
@return Singleton Array. Values of out
*/
void Var_Out::GetSingletons(Singleton * ret){
        Copy_Array_Singleton(ret,values,Nvalues);
};

/*
Returns a specific singleton of the out
@param index: Integer. Position of the value
@return Singleton. Value
@throw exception if index is not a valid position
*/
void Var_Out::GetSingleton(Singleton * ret,int index){
	if( index < 0 || index >= Nvalues) throw Error(8);
	(*ret).Equal(&values[index]);
};

/*
Sets out name
@param Name: String. Name to be set
*/
void Var_Out::SetName(char * Name){ strcpy(name,Name);};

/*
Sets the values of the out variable.
@param Values: Singleton Array. New values
@param size: Integer. New Nvalues.
*/
void Var_Out::SetSingletons(Singleton * Values, int size){
	if(Nvalues > 0) {delete [] values;}
	Nvalues = size;
	if(Nvalues > 0){
                values = new Singleton[Nvalues];
        	Copy_Array_Singleton(values,Values,size);
        }
};

/*
Sets a specific value
@param index: Integer. Position in the array of values.
@param Value: Singleton. Value to be set
@throw exception if index is not a valid position
*/
void Var_Out::SetSingleton(int index, Singleton * Value){
	if( index < 0 || index >= Nvalues) throw Error(8);
	values[index].Equal(Value);
};

/*
Adds a new singleton
@deprecated adds the singleton in the specified position, if not specified
adds the singleton in the last position
@param sing: Singleton. To be added
@param index: Integer. Array position. Default value adds the singleton at the end
@throw exception if index is not a valid position
*/
void Var_Out::AddSingleton(Singleton * sing, int index){
	if(index != INFINITO && (index < 0 || index >= Nvalues)) throw Error(8);
	Singleton * Values_Aux = new Singleton[Nvalues+1];
	int pos;
	if(index == INFINITO) pos = Nvalues; //posicion donde agregar, ultima.
	else pos = index;
	for(int i = 0; i < Nvalues+1 ; i++){
		if(i < pos) Values_Aux[i].Equal(&values[i]);
		else if (i > pos) Values_Aux[i].Equal(&values[i-1]);
		else if (i == pos) 	Values_Aux[i].Equal(sing);
	}
	SetSingletons(Values_Aux, Nvalues+1);
	delete [] Values_Aux;
};

void Var_Out::AddSingleton(char * Name, double Value, int index){
        Singleton sing(Name,Value);
		AddSingleton(&sing,index);
};

/*
Deletes a specific singleton
@param index: Integer. Value position
@throw exception if index is and invalid position
*/
void Var_Out::DeleteSingleton(int index){
	if( index < 0 || index >= Nvalues) throw Error(8);
	Singleton * Auxiliar = NULL;
        if((Nvalues -1)>0){
                Auxiliar= new Singleton[Nvalues -1];
        	for(int i = 0;  i < index ; i ++)
                        Auxiliar[i].Equal(&values[i]);
                for(int i = (index+1);  i < Nvalues ; i ++)
                        Auxiliar[i-1].Equal(&values[i]);
        }
        SetSingletons(Auxiliar, Nvalues-1);
	if((Nvalues)>0)delete [] Auxiliar;
};

void Var_Out::Set_SingletonName(int index,char * Name){
        if( index < 0 || index >= Nvalues) throw Error(8);
        strcpy(values[index].name,Name);
};

void Var_Out::Set_SingletonValue(int index,double Value){
        if( index < 0 || index >= Nvalues) throw Error(8);
        values[index].value = Value;
};

void Var_Out::Get_SingletonName(char * ret,int index){
        if( index < 0 || index >= Nvalues) throw Error(8);
        strcpy(ret,values[index].name);
};

double Var_Out::Get_SingletonValue(int index){
        if( index < 0 || index >= Nvalues) throw Error(8);
        return  values[index].value;
};

/*
Shows a var_out on a console
*/
void Var_Out::Show(){
	cout<< name << " {" << endl;
	for(int j = 0; j < Nvalues; j++){
		cout << "	";
		values[j].Show();
	}
	cout << "	}\n" << endl;
};

/*---------------STRUCT RULE------------------*/
/*
Constructor of struct Rule
@deprecated Empty constructor
*/
Rule::Rule(){
        /*cond  = NULL;
	conseq = NULL;
        op_cond = NULL;*/
	Ncond = 0;
	Nconseq = 0;
	degree = 0;
};

/*
Constructor of struct Rule
@deprecated Inizializates the sizes of arrays and allocates memory for the arrays
*/
Rule::Rule(Condition * Cond,char * Op_cond, Consequence * Conseq, int N_cond, int N_conseq){
		Ncond = 0;
		Nconseq = 0;
                SetCondition(Cond,Op_cond,N_cond);
                SetConsequence(Conseq,N_conseq);
		degree = 0;
};

/*
Destructor of struct Rule
*/
Rule::~Rule(){
        if (Ncond > 0)
                delete [] cond;
        if (Ncond > 1)
                delete [] op_cond;
        if (Nconseq > 0){
                delete [] conseq;
        }
};
/*
Duplicates a rule
@param to_cpy: Rule. Rule to be copied.
@return Rule: copied rule.
*/
void Rule:: Equal(Rule *to_cpy){
        if (Ncond > 0)
                delete [] cond;
        if (Ncond > 1)
                delete [] op_cond;
        if (Nconseq > 0){
                delete [] conseq;
        }
	Ncond = (*to_cpy).Ncond;
	Nconseq = (*to_cpy).Nconseq;
	if (Ncond > 0){
                cond = new Condition[Ncond];
        	Copy_Array_Cond(cond,(*to_cpy).cond, Ncond);
        }
	if (Nconseq > 0){
                conseq = new Consequence[Nconseq];
        	Copy_Array_Conseq(conseq,(*to_cpy).conseq, Nconseq);
        }
	if (Ncond > 1){
                op_cond = new char[Ncond-1];
                Copy_Array_Op(op_cond,(*to_cpy).op_cond, Ncond-1);
        }
	degree = (*to_cpy).degree;
};

/*
Returns the rule�s complete condition
*/
void Rule::GetCondition(Condition * ret){Copy_Array_Cond(ret,cond,Ncond);};

/*
Returns the rule�s complete consequence
*/
void Rule::GetConsequence(Consequence * ret){ Copy_Array_Conseq(ret,conseq,Nconseq);};

/*
Returns the operators that are use in the condition
*/
void Rule::GetOp_cond(char * ret){Copy_Array_Op(ret,op_cond, Ncond-1);};

/*
Returns number of conditions of the rule
*/
int Rule::GetNcond(){return Ncond;};

/*
Returns number of consequence of the rule
*/
int Rule::GetNconseq(){return Nconseq;};

/*
Returns the degree of activation of the rule
*/
double Rule::GetDegree(){return degree;};

/*
Returns a specific condition of the rule
*/
void Rule:: GetConditionI(Condition * ret,int index){
	if(index < 0 || index >= Ncond) throw Error(20);
	(*ret).Equal(&cond[index]);
};

/*
Returns a specific consequence of the rule
*/
void Rule:: GetConsequenceI(Consequence * ret,int index){
	if(index < 0 || index >= Nconseq) throw Error(21);
	(*ret).Equal(&conseq[index]);
};

/*
Returns a specific operator of the rule�s condition
*/
char Rule:: GetOp_condI(int index){
	if(index < 0 || index >= Ncond-1) throw Error(20);
	return op_cond[index];
};

/*
Sets the rule�s complete condition and operators of the condition
@param Cond: Condition Array. New complete condition
@param OpCond: Operator Array. Operators of conditions
@param size: Integer. Number of conditions
*/
void Rule:: SetCondition(Condition * Cond, char * OpCond, int N_cond){
	if (Ncond > 0)	delete [] cond;
	if (Ncond > 1) delete [] op_cond;
	Ncond = N_cond;
	if (Ncond > 0){
                cond = new Condition[Ncond];
        	Copy_Array_Cond(cond ,Cond, Ncond);
	        if (Ncond > 1) {
                        op_cond = new char[Ncond-1];
        	        Copy_Array_Op(op_cond,OpCond, Ncond-1);
                }
        }
};

/*
Sets the rule�s complete consequence
@param Conseq: Consequence Array. New complete consequnece
@param size: Integer. Number of consequences
*/
void Rule:: SetConsequence(Consequence * Conseq, int N_conseq){
	if(Nconseq > 0) delete [] conseq;
	Nconseq = N_conseq;
	if(Nconseq > 0){
                conseq = new Consequence[Nconseq];
        	Copy_Array_Conseq(conseq,Conseq, Nconseq);
        }
};

/*
Sets the rule degree of activation
@param Degree: Double. Rules degree
*/
void Rule::SetDegree(double Degree){
	degree = Degree;
};

/*
Adds a condition to the condition array and its operator
*/
void Rule:: AddCondition(Condition * condition, char op){
	Condition * aux = new Condition[Ncond+1];
        char * auxop = NULL;
        if(Ncond >= 0) auxop = new char[Ncond];
        int i = 0;
        for(i = 0; i < Ncond ; i++){
                aux[i].Equal(&cond[i]);
                if(i != Ncond-1) {auxop[i] = op_cond[i];}
        }
        aux[i].Equal(condition);
        if(i != 0) auxop[i-1] = op;
        SetCondition(aux,auxop,Ncond+1);
        delete [] aux;
        if(Ncond > 1) delete [] auxop;
};

void Rule::AddCondition(int In, int label, char op,int Modifier){
        Condition con(In,label,Modifier);
		AddCondition(&con, op);
};

/*
Adds a consequence to the consequence array
*/
void Rule:: AddConsequence(Consequence  * consequence){
	Consequence * aux = new Consequence[Nconseq+1];
        int i = 0;
        for(i = 0; i < Nconseq ; i++)
                aux[i].Equal(&conseq[i]);
        aux[i].Equal(consequence);
        SetConsequence(aux,Nconseq+1);
        delete [] aux;
};

void Rule::AddConsequence(int Out, int Singleton){
		Consequence con(Out,Singleton);
        AddConsequence(&con);
};

/*
Deletes a specific condition
@param index: Integer. Value position
@throw exception if index is and invalid position
*/
void Rule::DeleteCondition(int index){
        if(index < 0 || index >= Ncond) throw Error(20);
        Condition * aux = NULL;
        char * auxop = NULL;
        if((Ncond-1) > 0){
                aux = new Condition[Ncond-1];
                if((Ncond-2) > 0) auxop = new char[Ncond-2];
                int i = 0;
                for(i = 0; i < index ; i++){
                        aux[i].Equal(&cond[i]);
                        if(i != (index-1)) {auxop[i] = op_cond[i];}
                }
                for(i = (index+1); i < Ncond ; i++){
                        aux[i-1].Equal(&cond[i]);
                        if(i > 1){ auxop[i-2] = op_cond[i-1];}
                }
        }
        SetCondition(aux,auxop,Ncond-1);
        if((Ncond) > 0) delete [] aux;
        if((Ncond-1) > 0)delete [] auxop;
};

/*
Deletes a specific consequence
@param index: Integer. Value position
@throw exception if index is and invalid position
*/
void Rule::DeleteConsequence(int index){
        if(index < 0 || index >= Nconseq) throw Error(21);
        Consequence * aux = NULL;
        if((Nconseq-1)>0){
                aux = new Consequence[Nconseq-1];
                int i = 0;
                for(i = 0; i < index ; i++)
                        aux[i].Equal(&conseq[i]);
                for(i = (index+1); i < Nconseq ; i++)
                        aux[i-1].Equal(&conseq[i]);
        }
        SetConsequence(aux,Nconseq-1);
        if((Nconseq)>0) delete [] aux;
};


/*
Finds an entry on a rule condition
@param Entry_Index: Integer. Index of entry to be found
@param all_ap: Integer. If FALSE(0) asigns 1 to the first position of the array and breaks
@return Integer array: "boolean" array TRUE(1) is found in the position of the
condition where the entry was found
*/
void Rule:: Find_Entry_In_Rule(int * ret,int Entry_Index, int all_ap){
        for(int i = 0; i < GetNcond() ; i++){
                Condition c;
                GetConditionI(&c,i);
		if( (c.in) == Entry_Index){
			if (all_ap == FALSE) {
                                ret[0] = TRUE;
                                for(int x = 1; x < GetNcond(); x++) ret[x] = 0;
                                break;
                        }
			else ret[i] = TRUE;
		}else
			ret[i] = FALSE;
	}
};

/*
Finds a label on a rule condition
@param Entry_Index: Integer. Index of entry to be where the label belongs to
@param Label_Index: Integer. Index of entry to be found
@param all_ap: Integer. If FALSE(0) asigns 1 to the first position of the array and breaks
@return Integer array: "boolean" array TRUE(1) is found in the position of the
condition where the label was found
*/
void Rule::Find_Label_In_Rule(int * ret,int Entry_Index,int Label_Index,  int all_ap){
	for(int i = 0; i < GetNcond(); i++){
                Condition c;
                GetConditionI(&c,i);
		if(c.in == Entry_Index && c.label == Label_Index ){
			if (all_ap == FALSE) {
                                ret[0] = TRUE;
                                for(int x = 1; x < GetNcond(); x++) ret[x] = 0;
                                break;
                        }
			else ret[i] = TRUE;
		}else
			ret[i] = FALSE;

	}
};

/*
Finds an Out on a rule consequence
@param Out_Index: Integer. Index of Out to be found
@param all_ap: Integer. If FALSE(0) asigns 1 to the first position of the array and breaks
@return Integer array: "boolean" array TRUE(1) is found in the position of the
consequence where the out was found
*/
void Rule::Find_Out_In_Rule(int * ret,int Out_Index, int all_ap){
	for(int i = 0; i < Nconseq ; i++){
                Consequence c;
                GetConsequenceI(&c,i);
		if ( c.out == Out_Index){
			if(all_ap == FALSE) {
                                ret[0] = TRUE;
                                for(int x = 1; x < GetNconseq(); x++) ret[x] = 0;
                                break;
                        }
			else ret[i] = TRUE;
		}else
			ret[i] = FALSE;
	}
};

/*
Finds an Singleton on a rule consequence
@param Out_Index: Integer.
@param Singleton_Index: Integer. Index of Singleton to be found
@return Integer array: "boolean" array TRUE(1) is found in the position of the
consequence where the singleton was found
*/
void Rule::Find_Singleton_In_Rule(int * ret, int Out_Index, int Singleton_Index, int all_ap){
	for(int i = 0; i < GetNconseq() ; i++){
                Consequence c;
                GetConsequenceI(&c,i);
		if (c.out == Out_Index && c.singleton == Singleton_Index ){
			if(all_ap == FALSE) {
                                ret[0] = TRUE;
                                for(int x = 1; x < GetNconseq(); x++) ret[x] = 0;
                                break;
                        }
			else if (all_ap == TRUE) ret[i] = TRUE;
		}else
			ret[i] = FALSE;
	}
};

void Rule::Show(const char * x){
        cout <<"RULE "<< x <<"------------------------------------------------------\n";
        cout <<"CONSECUENTS (IN-MODIFICADOR-LABEL)"<< "\n";
        for(int i = 0 ; i < Ncond ; i++){
                if (i != 0) cout << op_cond[i-1]<<" ";
                else  cout << "  ";
                cout << cond[i].in<<"-"<<cond[i].modifier <<"-" <<cond[i].label<<endl;
        }
        cout <<"CONSECUENTS (OUT-SIGLETON)"<< "\n";
        for(int i = 0 ; i < Nconseq ; i++)
                cout << conseq[i].out << "-" << conseq[i].singleton << endl;
        cout <<"DEGREE: "<<degree << "\n";
};

/*--------------------------STRUCT RULEBASE----------------------------*/
/*
Constructor of struct RuleBase
@deprecated Empty constructor
*/
RuleBase::RuleBase(){
        strcpy(Name,"");
	//Rules = NULL;
	Nrules = 0;
	And = -1;
	Or = -1;
};

/*
Constructor of struct RuleBase
@deprecated Iniizializates the name and size of the rule base. Allocates space
for rules and sets the type of conjuction and disjuction to be used
*/
RuleBase::RuleBase(char * name, int conjunction, int disjunction){
	strcpy(Name,name);
	Nrules = 0;
        //Rules = NULL;
	And = conjunction;
	Or = disjunction;
};

/*
Constructor of struct RuleBase
@deprecated Empty constructor
*/
RuleBase::~RuleBase(){
        if (Nrules > 0) delete [] Rules;
};

/*
Returns the rulebase name
*/
void RuleBase::GetName(char * ret){strcpy(ret,Name);};

/*
Returns the number of rules
*/
int RuleBase::GetNrules(){return Nrules;};

/*
Returns the type of operator AND used in the rule base
*/
int RuleBase::GetAnd(){return And;};

/*
Returns the type of operator OR used in the rule base
*/
int RuleBase::GetOr(){return Or;};

/*
Returns the rulebase rules
*/
void RuleBase::GetRules(Rule * ret){Copy_Array_Rule(ret,Rules,Nrules);};

/*
Returns a specific rule
@param index: Integer. Index of rule
@throw exception if index is invalid on the rule array
*/
void RuleBase::GetRule(Rule * ret,int index){(*ret).Equal(&Rules[index]);};

/*
Sets the rulebase name
@param name: String. Rule base new name.
*/
void RuleBase::SetName(char * name){strcpy(Name,name);};

/*
Set type of operator AND
*/
void RuleBase::SetAnd(int conj){ And = conj;};

/*
Set type of operator OR
*/
void RuleBase::SetOr(int disj){ Or = disj;};

/*
Sets the rulebase rules
@param rules: Rule array. Rulebase new rules.
@param size: Integer. Rulebase Nrules.
*/
void RuleBase::SetRules(Rule * rules, int N_rules){
	if(Nrules > 0) delete [] Rules;
	Nrules = N_rules;
	if(Nrules > 0){
                Rules = new Rule[Nrules];
                Copy_Array_Rule(Rules,rules,N_rules);
        }
};

/*
Sets a rule in a specific position
@param index: Integert. Position
@param rule: Rule.
@throws exception for invalid index
*/
void RuleBase::SetRule(int index, Rule * rule){
	if (index < 0 || index >= Nrules) throw Error(17);
	Rules[index].Equal(rule);
};

/*
Add a new rule at the end of the array
*/
void RuleBase::AddRule(Rule * rule){
	Rule * aux = new Rule[Nrules+1];
        int i;
        for(i = 0; i < Nrules ; i++){
                aux[i].Equal(&Rules[i]);
        }
        aux[i].Equal(rule);
        SetRules(aux,Nrules+1);
        delete [] aux;

};

/*
Delete a specific rule
@param index: Integer. Position of the rule to be deleted
@throw exception for invalid index
*/
void RuleBase::DeleteRule(int index){
        if( index < 0 || index >= Nrules) throw Error(17);
        Rule * aux = NULL;
        if((Nrules-1)>0){
                aux = new Rule[Nrules-1];
                for(int i = 0;  i < index ; i++)
                        aux[i].Equal(&Rules[i]);
                for(int i = (index+1);  i < Nrules ; i++)
                        aux[i-1].Equal(&Rules[i]);
        }
        SetRules(aux, Nrules-1);
	if((Nrules)>0) delete [] aux;
};

void RuleBase::Show(){
        cout << "------------RULEBALSE: " << Name<< endl;
        cout << " AND =" << And << " OR =" << Or <<endl;
        for(int i = 0 ; i < Nrules ; i++){
                char a[2];
                sprintf(a,"%i",i);
                Rules[i].Show(a);
        }
};
/*******************MAIN CLASS: AOrbexDos(CONTROLER)*******************************/
/* 
Constructor of AOrbexDos class
*/
AOrbexDos::AOrbexDos(){
	Nentries = 0;
	Nout = 0;
    Nrb = 0;
	/*entries = NULL;
	outs  = NULL;
	RB = NULL;
	In_Values = NULL;
	Out_Values = NULL; */
};

/*
Destructor of AOrbexDos class
*/
AOrbexDos::~AOrbexDos(){
	if (Nentries > 0){
                delete [] entries;
                delete [] In_Values;
        }
	if (Nout > 0){
                delete [] outs;
                delete [] Out_Values;
        }
        if (Nrb > 0){
                delete [] RB;
        }
};

/* 
Constructor of class fuzzy
@deprecated	Reads a controler from a file a creates it.
@param	Filename: String. Name of the file that contains the information.
@throws	Exception if the file cannot be read.
@throws	Exception if the file is not writen in the right format or propper grammer
*/
AOrbexDos::AOrbexDos(const char * filename){
	ifstream f(filename, ifstream::in);
   	if (!f) throw Error(10,"(Constructor.fuzzy)");
	
	Nentries = 0;
	Nout = 0;
	Nrb = 0;
    /*entries = NULL;
	outs  = NULL;
	RB = NULL;
	In_Values = NULL;
	Out_Values = NULL; */
	char * read = new char[MAX_SIZE];
	int l = 0;
	char * msg = new char[MAX_SIZE];
	
	do{
		f >> read; //Read IN OUT or RULES else ERROR
		l = 0;
		if (strcmp(read,"IN") == 0) {
			f >> read; //Read input name
			l++;
			while( strcmp(read,"NI") != 0){
				Var_Entry entry;
				entry.SetName(read);
				f >> read; //Read limit INF of rgo
				sprintf(msg, "%s %i %s", "IN.Line-", (l++)/4 , ".inferior.range.NI.");
				entry.SetRangeInf(atod(read,msg));
				f >> read;//Read limit SUP of rgo
				sprintf(msg, "%s %i %s", "IN.Line-", (l++)/4 , ".superior.range.NI.");
				entry.SetRangeSup(atod(read,msg));
				f >> read; //Read key
				l++;
                                Add_Entry(&entry);
				if (strcmp(read,"{") == 0){
					f >> read; //Read label name
					l++;
					while( strcmp(read,"}") != 0){
						Label label;
                                                int Typ = -1;
						int size = 0;
						int size1 = 0;
						int Eq_Dist;
						label.SetName(read);	
						f >> read; //Read type
						l++;
						if( strcmp(read,"TRI") == 0){Typ = TRI;size = 3;}
						else if( strcmp(read,"TRA") == 0){Typ=TRA;size = 4;}
						else if( strcmp(read,"GSS") == 0){Typ=GSS;size = 2;}
						else if( strcmp(read,"BLL") == 0){Typ=BLL;size = 3;}
						else if( strcmp(read,"PI") == 0){Typ=FPI;size = 4;}
						else if (strcmp(read,"TRIDIST") == 0){Eq_Dist=EQ_TRI;size1 = 1;}//TO BE FIXED
						else if (strcmp(read,"TRADIST") == 0){Eq_Dist=(EQ_TRA);size1 = 2;}//TO BE FIXED
						else if (strcmp(read,"GSSDIST") == 0){Eq_Dist=(EQ_GSS);size1 = 2;}//TO BE FIXED
						else if (strcmp(read,"BLLDIST") == 0){Eq_Dist=(EQ_BLL);size1 = 3;}//TO BE FIXED
						else if (strcmp(read,"PIDIST") == 0){Eq_Dist=(EQ_PI);size1 = 2;}//TO BE FIXED
						else { 	sprintf(msg, "%s %i %s", "IN.Line-", l/4 , ".NI."); throw Error(11,msg);}
						if(size != 0){
							double val[4];
							int h;
							for(h = 0; h <size ; h++){
								f >> read;
								sprintf(msg, "%s %i %s", "IN.Line-", (l++)/4 , ".values.NI.");
								val[h] = atod(read,msg);
							}
							for(h; h<4 ; h++){
								val[h] = INFINITO;
							}
                                                        char nameAux[MAX_NAME_SIZE];
                                                        label.GetName(nameAux);
							Add_Label(GetNentries()-1,nameAux,Typ,val[0],val[1],val[2],val[3]);
						}else{//EQDIST-- size1
							double val[4];
							int h;
							for(h = 0; h <size1 ; h++){
								f >> read;
								sprintf(msg, "%s %i %s", "IN.Line-", (l++)/4 , ".values.Eqdist.NI.");
								val[h] = atod(read,msg);
							}
                                                        char nameAux[MAX_NAME_SIZE];
                                                        label.GetName(nameAux);
							switch (Eq_Dist) {
								case EQ_TRI:
									Make_Triangular_Labels(GetNentries()-1,(int)val[0],nameAux);
									break;
								case EQ_TRA:
									Make_Trapezium_Labels(GetNentries()-1,val[1],(int)val[0],nameAux);
									break;
								case EQ_GSS:
									Make_Gauss_Labels(GetNentries()-1,val[1],(int)val[0],nameAux);
									break;
								case EQ_BLL:
									Make_Bell_Labels(GetNentries()-1,val[1],val[2],(int)val[0],nameAux);
									break;
								case EQ_PI:
									Make_PI_Labels(GetNentries()-1,val[1],(int)val[0],nameAux);
									break;
							}
						}
						f >> read; //Read label name
						l++;
					}
				}else {sprintf(msg, "%s %i %s", "IN.Line-", l/4 , ".NI."); throw Error(11,msg);}
				f >> read;
				l++;
			}
		}else if(strcmp(read,"OUT") == 0){
			f >> read; //Read output name
			l++;
			while( strcmp(read,"TUO") != 0){
				Add_Out(read);
				f >> read; //Read key
				l++;
				if (strcmp(read,"{") == 0){
					f >> read; //Read name value or close key
					l++;
					while( strcmp(read,"}") != 0){
						char * name_sing = new  char[MAX_SIZE];
						strcpy(name_sing,read);
						f >> read; //Read singleton value
						sprintf(msg, "%s %i %s", "OUT.Line-", (l++)/3 , ".TUO ");
						if(strcmp(read,"SDIST") != 0){
							Add_Singleton(GetNouts()-1,name_sing,atod(read,msg));
						}else{
							double rge[2];
							int hm;
							f >> read; 
							sprintf(msg, "%s %i %s", "OUT.Line-", (l++)/3 , ".eqdist.TUO ");
							rge[INF] = atod(read,msg);
							f >> read; 
							sprintf(msg, "%s %i %s", "OUT.Line-", (l++)/3 , ".eqdist.TUO ");
							rge[SUP] = atod(read,msg);
							f >> read; 
							sprintf(msg, "%s %i %s", "OUT.Line-", (l++)/3 , ".eqdist.TUO ");
							hm = (int)atod(read,msg);
							Make_Singletons(GetNouts()-1,hm,rge[INF],rge[SUP],name_sing);
						}
						f >> read;
						l++;
						delete [] name_sing;

					}
				}else {sprintf(msg, "%s %i %s", "OUT.Line-", (l)/3 , "TUO "); throw Error(11,msg);}
				f >> read;
				l++;
			}
                }else if (strcmp(read, "RULES") == 0){
			f >> read;
			l++;
			while(strcmp(read,"SELUR") !=0){
				char * name_aux = new char[MAX_SIZE];
				char conjunction[4];
				char disjunction[4];
				strcpy(name_aux, read);
				f >> read;  
				l++;
				if(strcmp(read,"{") != 0){
					if (IsAnd(read) == -1) { sprintf(msg, "%s %i %s", "RULES.Line-", (l)/11 , "and.type.SELUR."); throw Error(11,msg);}
					else strcpy(conjunction, read);
					f >> read;  
					l++;
					if (IsOr(read) == -1) {sprintf(msg, "%s %i %s", "RULES.Line-", (l)/11 , "or.type.SELUR."); throw Error(11,msg);}
					else strcpy(disjunction, read);
					f >> read;  
					l++;
				}else{
					strcpy(conjunction, "MIN");
					strcpy(disjunction, "MAX");
				}
				Add_RB(name_aux,conjunction,disjunction);
				delete [] name_aux;
				if( strcmp(read,"{") != 0) {sprintf(msg, "%s %i %s", "RULES.Line-", (l)/11 , ".SELUR."); throw Error(11,msg);}
				f >> read;  
				l++;
				char * rule = new char[MAX_SIZE];
				strcpy(rule," ");
				while( strcmp(read, "}") != 0){
					strcpy(rule,read);
					f >> read;
					l++;
					while(strcmp(read,"IF") != 0 && strcmp(read,"}") != 0 ){
						strcat(rule," ");
						strcat(rule,read);
						f >> read;
						l++;
					}
					sprintf(msg, "%s %i %s", "RULES.Line-", (l)/11 , ".SELUR.");
					Add_RB_Rule(GetNrb()-1,rule,msg);
				}
				delete [] rule;
				f >> read; 
				l++;
			}
                }

	}while(!f.eof());
	f.close();
	delete [] read;
	delete [] msg;
};

/*
Returns the number of entries of the controler
*/
int AOrbexDos::GetNentries(){ return Nentries; };

/*
Returns the number of outs of the controler
*/
int AOrbexDos::GetNouts(){return Nout;};

/*
Returns the number of rulebases of the controler
*/
int AOrbexDos::GetNrb(){ return Nrb;};

/*
Returns a specific In_Value to be used in the controler
@param In_Index: Integer. Index of entry to find its value
@throw exception for invalid index
*/
double AOrbexDos::GetInValue(int In_Index){
	if (In_Index < 0 || In_Index >= Nentries) throw Error(2,"(GetInValue)");
	return In_Values[In_Index];
};

/*
Returns a specific In_Value to be used in the controler. Overload of method
@param Name_Entry: String. Name of entry to find its value
*/
double AOrbexDos::GetInValue(char * Name_Entry){ return GetInValue(Find_Entry_Name(Name_Entry));};

/*
Returns all In_Values to be used in the controler
*/
void AOrbexDos::GetInValues(double * ret){ Copy_Double_Array(ret,In_Values,Nentries);};

/*
Returns a specific Out_Value of the controler.
@param Out_Index: Integer. Index of out to find its value
@throw exception for invalid index
*/
double AOrbexDos::GetOutValue(int Out_Index){
	if (Out_Index < 0 || Out_Index >= Nout) throw Error(8,"(GetOutValue)");
	return Out_Values[Out_Index];
};

/*
Returns a specific Out_Value of the controler. Overload of method
@param Out_Name: String. Name of out to find its value
*/
double AOrbexDos::GetOutValue(char * Out_Name){return GetOutValue(Find_Out_Name(Out_Name));};

/*
Returns all Out_Values of the controler
*/
void AOrbexDos::GetOutValues(double * ret){Copy_Double_Array(ret,Out_Values,Nout);};

/*
Sets the in_value of the specified entry*/
void AOrbexDos::SetInValue(int In_Index, double in_value){
	if (In_Index < 0 || In_Index >= Nentries) throw Error(2,"(SetInValue)");
	In_Values[In_Index] = in_value;
}

/*Overloads SetInValue(int,double)*/
void AOrbexDos::SetInValue(char * Name_Entry, double in_value){SetInValue(Find_Entry_Name(Name_Entry),in_value);};



/*Sets the in_values of each entry*/
void AOrbexDos::SetInValues(double * in_value){
	for(int i = 0 ; i < GetNentries() ; i++){
		In_Values[i] = in_value[i];
		if(In_Values[i] > Get_Entry_RgeSup(i)){
			In_Values[i] = Get_Entry_RgeSup(i);
		}else if(In_Values[i] < Get_Entry_RgeInf(i)){
			In_Values[i] = Get_Entry_RgeInf(i);
		}
	}
};


/*METHODS ABOUT ENTRIES AND LABELS*/
/*
Add an Entry
@deprecated Adds a new entry. Allocates memory for the new entry and inizializates it.
@see Copy_Array_Entry, Duplicate_Entry
*/
void AOrbexDos::Add_Entry(Var_Entry * entry){
        Var_Entry * Aux_entries = new Var_Entry[Nentries+1];
	double * Aux_inValues = new double[Nentries+1];
	for( int i = 0 ; i < Nentries ; i++){
		Aux_entries[i].Equal(&entries[i]);
		Aux_inValues[i] = In_Values[i];
	}
        Aux_inValues[Nentries] = 0;
        char nameAux[MAX_NAME_SIZE];
        (*entry).GetName(nameAux);
        Aux_entries[Nentries].SetName(nameAux);
        Aux_entries[Nentries].SetRangeInf((*entry).GetRangeInf());
        Aux_entries[Nentries].SetRangeSup((*entry).GetRangeSup());
        Label * labels;
		labels = new Label();
        if((*entry).GetNlabels()>0){
                labels = new Label[(*entry).GetNlabels()];
                (*entry).GetLabels(labels);
                 Aux_entries[Nentries].SetLabels(labels,(*entry).GetNlabels());
                delete [] labels;
        }else{
                Aux_entries[Nentries].SetLabels(labels,0);
        }

	if(Nentries > 0){
		delete [] In_Values;
		delete [] entries;
	}
	Nentries++;
        entries = new Var_Entry[Nentries];
	Copy_Array_Entry(entries,Aux_entries, Nentries);
	In_Values = new double[Nentries];
	Copy_Double_Array(In_Values,Aux_inValues,Nentries);
        delete [] Aux_entries;
        delete [] Aux_inValues;

};

/*
Add an Entry
@deprecated	Adds a new entry. Allocates memory for the new entry and sets the name and range.
@see Copy_Array_Entry, Duplicate_Entry
*/
void AOrbexDos::Add_Entry(char* Entry_Name, double r0, double r1){
        Var_Entry * Aux_entries = new Var_Entry[Nentries+1];
	double * Aux_inValues = new double[Nentries+1];
	for( int i = 0 ; i < Nentries ; i++){
		Aux_entries[i].Equal(&entries[i]);
		Aux_inValues[i] = In_Values[i];
	}
        Aux_inValues[Nentries] = 0;
        Aux_entries[Nentries].SetName(Entry_Name);
        Aux_entries[Nentries].SetRangeInf(r0);
        Aux_entries[Nentries].SetRangeSup(r1);
        Label la[1];
        Aux_entries[Nentries].SetLabels(la,0);
	if(Nentries > 0){
		delete [] In_Values;
		delete [] entries;
	}
	Nentries++;
        entries = new Var_Entry[Nentries];
	Copy_Array_Entry(entries,Aux_entries, Nentries);
	In_Values = new double[Nentries];
	Copy_Double_Array(In_Values,Aux_inValues,Nentries);
        delete [] Aux_entries;
        delete [] Aux_inValues;

};

/*
Delete an entry.
@deprecated: Deletes an entry in the entries array of the controler. And also
deletes the rules asociated with than entry in the rule base.
@param Ind_Entry: Integer. index of the entry to be deleted
@throw Exception invalid index
@see Copy_Array_Entry, Duplicate_Entry, Delete_Rule_Asociatedwith_Entry
*/
void AOrbexDos::Delete_Entry(int Ind_Entry){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries())throw Error(2, "(Delete_Entry)");
	//Delete rule
	Delete_Rule_Asociatedwith_Entry(Ind_Entry);
	Var_Entry * Auxiliar = NULL;
        double * Aux = NULL;
        if((Nentries - 1)> 0){
                 Auxiliar = new Var_Entry[Nentries - 1];
                 Aux = new double[Nentries - 1];
	        for( int i = 0; i < Ind_Entry; i++){
        		Auxiliar[i].Equal(&entries[i]);
        		Aux[i] = In_Values[i];
                }
                for (int i = (Ind_Entry+1); i < Nentries; i++){
        		Auxiliar[i-1].Equal(&entries[i]);
        		Aux[i-1] = In_Values[i];
        	}
        }
	Nentries--;
	delete [] entries;
	delete [] In_Values;
	if((Nentries)> 0){
                In_Values = new double[Nentries];
        	Copy_Double_Array(In_Values,Aux,Nentries);
        	entries = new Var_Entry[Nentries];
        	Copy_Array_Entry(entries,Auxiliar, Nentries);
        	delete [] Auxiliar;
        	delete [] Aux;
        }
};

/*
Delete an entry. Overload method
@deprecated: Deletes an entry in the entries array of the controler. And also
deletes the rules asociated with than entry in the rule base.
@param Name_Entry: String. name of the entry to be deleted
*/
void AOrbexDos::Delete_Entry(char * Name_Entry){
	Delete_Entry(Find_Entry_Name(Name_Entry));
};

/*
Set of an Entry.
@deprecated	Asignes the range, name and labels.
@throws	Exception if the inferior limit is higger than the superior
@throws	Exception if the entry index is invalid
*/
void AOrbexDos::Set_Entry(int Ind_Entry, Var_Entry * entry){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Set_Entry)");
	if ( (*entry).GetRangeInf() > (*entry).GetRangeSup()) throw Error(1,"(Set_Entry)");
	entries[Ind_Entry].Equal(entry);
};

/*
Set of an Entry.
@deprecated	Asignes the range, name and labels.
@param	Ind_Entry: Integer. Index of array entries.
@param Entry_Name: String. Name of the entry
@param r0: Decimal. Inferior limit of the range of the labels
@param r1: Decimal. Superior limit of the range of the labels
@param n_labels: Integer. Number of labels.
@param labels: Label Array. Labels of the entry.
@throws	Exception if the inferior limit is higger than the superior
@throws	Exception if the entry index is invalid
*/
void AOrbexDos::Set_Entry(int Ind_Entry, char* Entry_Name, double r0, double r1, int n_labels, Label * labels){
	double rge[2];
	rge[INF] = r0;
	rge[SUP] = r1;
	Var_Entry entry(Entry_Name, n_labels,rge,labels);
	Set_Entry(Ind_Entry,&entry);
};

/*
Returns the entry in the position specified
@param Ind_Entry: Integer. Entry position.
@return Var_Entry.
@throw invalid index of entry
*/
void AOrbexDos::Get_Entry(Var_Entry * ret,int Ind_Entry){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(GetEntry)");
	(*ret).Equal(&entries[Ind_Entry]);
};

/*
Returns the entry in the position specified. Overload method
@param Name_Entry: String. Entry name.
@return Var_Entry.
*/
void AOrbexDos::Get_Entry(Var_Entry * ret,char * Name_Entry){
	Get_Entry(ret,Find_Entry_Name(Name_Entry));
};

/*
Changes an Entry name.
@param	Ind_Entry: Integer. Entry position.
@param New_Name: Strig. Name of the entry
@throws	Exception if the entry index is invalid
*/
 void AOrbexDos::Set_Entry_Name(int Ind_Entry, char * New_Name){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Set_Entry_Name)");
	entries[Ind_Entry].SetName(New_Name);
};

/*
Changes an Entry name. Overload method
@param	Old_Name: String. Entry name.
@param New_Name: Strig. new Name of the entry
*/
void AOrbexDos::Set_Entry_Name(char * Old_Name, char * New_Name){
	Set_Entry_Name(Find_Entry_Name(Old_Name), New_Name);
};

/*
Returns the name of the specified entry
@param	Ind_Entry: Integer. Entry position.
@throws	Exception if the entry index is invalid
*/
void AOrbexDos::Get_Entry_Name(char * ret,int Ind_Entry){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Get_Entry_Name)");
	entries[Ind_Entry].GetName(ret);
};

/*
Sets the inferior limit of the entry range
@param	Ind_Entry: Integer. Entry position.
@param ro: Double. New inferior limit of range.
@throws	Exception if the entry index is invalid
*/
void AOrbexDos::Set_Entry_RgeInf(int Ind_Entry, double r0){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Set_Entry_RgeInf)");
	entries[Ind_Entry].SetRangeInf(r0);
};

/*
Sets the inferior limit of the entry range. Overload method
@param	Name: String. Entry name.
@param ro: Double. New inferior limit of range.
*/
void AOrbexDos::Set_Entry_RgeInf(char * Name, double r0){
	Set_Entry_RgeInf(Find_Entry_Name(Name), r0);
};

/*
Sets the superior limit of the entry range
@param	Ind_Entry: Integer. Entry position.
@param ro: Double. New superior limit of range.
@throws	Exception if the entry index is invalid
*/
void AOrbexDos::Set_Entry_RgeSup(int Ind_Entry, double r1){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Set_Entry_RgeSup)");
	entries[Ind_Entry].SetRangeSup(r1);
};

/*
Sets the inferior limit of the entry range. Overload method
@param	Name: String. Entry name.
@param ro: Double. New inferior limit of range.
*/
void AOrbexDos::Set_Entry_RgeSup(char * Name, double r1){
	Set_Entry_RgeSup(Find_Entry_Name(Name), r1);
};

/*
Returns the entry inferior limit
@param	Ind_Entry: Integer. Entry position.
@throws	Exception if the entry index is invalid
*/
double AOrbexDos::Get_Entry_RgeInf(int Ind_Entry){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Get_Entry_RgeInf)");
	return entries[Ind_Entry].GetRangeInf();
};

/*
Returns the entry inferior limit. Overload Method
@param	Namr_Entry: String. Entry name.
*/
double AOrbexDos::Get_Entry_RgeInf(char * Name_Entry){
	return Get_Entry_RgeInf(Find_Entry_Name(Name_Entry));
};

/*
Returns the entry superior limit
@param	Ind_Entry: Integer. Entry position.
@throws	Exception if the entry index is invalid
*/
double AOrbexDos::Get_Entry_RgeSup(int Ind_Entry){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Get_Entry_RgeInf)");
	return entries[Ind_Entry].GetRangeSup();
};

/*
Returns the entry superior limit. Overload Method
@param	Namr_Entry: String. Entry name.
*/
double AOrbexDos::Get_Entry_RgeSup(char * Name_Entry){
	return Get_Entry_RgeSup(Find_Entry_Name(Name_Entry));
};

/*
Returns the number of labels of the entry
@param	Ind_Entry: Integer. Entry position.
@throws	Exception if the entry index is invalid
*/
int AOrbexDos::Get_Entry_Nlabels(int Ind_Entry){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Get_Entry_Nlabels)");
	return entries[Ind_Entry].GetNlabels();
};

/*
Returns the number of labels of the entry
@param	Ind_Entry: String. Entry name.
*/
int AOrbexDos::Get_Entry_Nlabels(char * Name_Entry){return Get_Entry_Nlabels(Find_Entry_Name(Name_Entry));};

/*
Add "manually" a label .
@deprecated	Adds a new label with type and values and name in the parameters.
@param Ind_Entry: Integer. Index of Entry.
@param name: String. Label name.
@param type: Intefer. Type of label. 0 triangle, 1 trapezium. 2 Gauss. 3 Bell. 4 FFPI.
@param v0,v1,v2,v3: Double. Values of the label. v2 and v3 have default values.
@throws	Exception invalid index on Entry array.
@throwss Exception invalid type of label.
@see Copy_Array_Label
*/
void AOrbexDos::Add_Label(int Ind_Entry,char * name1, int type, double v0 ,double v1, double v2, double v3){
	if (Ind_Entry < 0 || Ind_Entry > GetNentries()-1 ) throw Error(2,"(Add_Label)");
	if (type != TRI && type != TRA && type != GSS && type!= BLL && type != FPI)
		throw Error(4,"(Add_Label)"); // Possible types
	if ((type == TRI) && !( v0 <= v1 && v1 <= v2 && v3 == INFINITO)){
		throw Error(6,"(Add_Label)TRI-"); }//Restrictions TRI
	if ((type == TRA) && !( v0 <= v1 && v1 <= v2 && v2 <= v3 && v3 != INFINITO)){
		throw Error(6,"(Add_Label)TRA-"); }//Restrictions FPI
	if ( (type == GSS) && (v1 == 0 || v2 != INFINITO || v3 != INFINITO ))
		throw Error(12,"(Add_Label)GSS-"); //Restrictions Gauss
	if  ( (type == BLL) && (v2 < 0 || v3 != INFINITO || v1 == 0) )
		throw Error(12,"(Add_Label)BLL-");	//Restrictions bell
	if ((type == FPI) &&	!( v0 <= v1 && v1 <= v2 && v2 <= v3 && v3 != INFINITO)){
		throw Error(6,"(Add_Label)PI-"); }//Restrictions FPI
	double val[4];
        val[0] = v0;
	val[1] = v1;
	val[2] = v2;
	val[3] = v3;
        char nameAux[MAX_NAME_SIZE];
        Get_Entry_Name(nameAux,Ind_Entry);
	if( strcmp(name1, EMPTY) == 0) sprintf(name1,"%s_%i",nameAux, Get_Entry_Nlabels(Ind_Entry));
	Label label(type,val,name1);
	entries[Ind_Entry].AddLabel(&label);
};

/*
Add "manually" a label . Overload method
@deprecated	Adds a new label with type and values and name in the parameters.
@param Name_Entry: String. name of Entry.
@param name: String. Label name.
@param type: Intefer. Type of label. 0 triangle, 1 trapezium. 2 Gauss. 3 Bell. 4 FPI.
@param v0,v1,v2,v3: Double. Values of the label. v2 and v3 have default values.
*/
void AOrbexDos::Add_Label(char * Name_Entry,char * name , int type,double v0, double v1, double v2 , double v3 ){
	Add_Label(Find_Entry_Name(Name_Entry), name , type,v0, v1, v2, v3 );
};

void AOrbexDos::Add_Label(int Ind_Entry, Label * label_new){
        char nameAux[MAX_NAME_SIZE];
        (*label_new).GetName(nameAux);
        Add_Label(Ind_Entry,nameAux,(*label_new).GetType(),
                (*label_new).GetValue(0),(*label_new).GetValue(1),
                (*label_new).GetValue(2),(*label_new).GetValue(3));
};
void AOrbexDos::Add_Label(char * Name_Entry, Label * label_new){
        Add_Label(Find_Entry_Name(Name_Entry), label_new );
};

/*
Delete label.
@deprecated given an index of entries array and an index of labels array and
a new name. It errases de label from labels array in the specified entry. And also
deletes the rules asociated with that label in the rule base.
@param Ind_Entry: Integer. Index of entries.
@param Ind_Label: Integer. Index of labels
@thow Exception if any of the indexes is invalid
@see Duplicate_Label,Copy_Array_Label, Delete_Rule_Asociatedwith_Label
*/
void AOrbexDos::Delete_Label(int Ind_Entry, int Ind_Label){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Delete_Label)");
	if (Ind_Label < 0 || Ind_Label >= Get_Entry_Nlabels(Ind_Entry)) throw Error(16,"(Delete_Label)");
	Delete_Rule_Asociatedwith_Label(Ind_Entry, Ind_Label);//Delete rule
	entries[Ind_Entry].DeleteLabel(Ind_Label);
};

/*
Delete label. Overload method.
@deprecated given an index of entries array and an index of labels array and
a new name. It errases de label from labels array in the specified entry. And also
deletes the rules asociated with that label in the rule base.
@param Name_Entry: String. Name_ of entry
@param Name_Label: String. Name_ of label
*/
void AOrbexDos::Delete_Label(char * Name_Entry, char * Name_Label){
	Delete_Label(Find_Entry_Name(Name_Entry),
	Find_Label_Name(Find_Entry_Name(Name_Entry),Name_Label));
};

/*
Delete label. Overload method.
@deprecated given an index of entries array and an index of labels array and
a new name. It errases de label from labels array in the specified entry. And also
deletes the rules asociated with that label in the rule base.
@param Name_Entry: String. Name_ of entry
@param Ind_Label: Integer. Index of label
*/
void AOrbexDos::Delete_Label(char * Name_Entry, int Ind_Label){
	Delete_Label(Find_Entry_Name(Name_Entry), Ind_Label);
};

/*
Delete label. Overload method.
@deprecated given an index of entries array and an index of labels array and
a new name. It errases de label from labels array in the specified entry. And also
deletes the rules asociated with that label in the rule base.
@param Ind_Entry: Integer. Index of entry
@param Name_Label: String. Name_ of label
*/
void AOrbexDos::Delete_Label(int Ind_Entry, char * Name_Label){
	Delete_Label(Ind_Entry,  Find_Label_Name(Ind_Entry,Name_Label));
};

/*
Creates triangular labels in the specified entry.
@deprecated	Creates as many triangles sd the parameter N_labels indicates
@param Ind_Entry: Integer. Index of the entry in the entry array.
@param N_Labels: Integer. Number of triangular labels to be added.
@param name: String. New labels names. Opcional parameter with default value.
@throws	Exception if the index is not valid
@see Calculate_Triangles_Center, Add_Label
*/
void AOrbexDos::Make_Triangular_Labels(int Ind_Entry, int N_labels, const  char * name){
	if (Ind_Entry < 0 || Ind_Entry > GetNentries() -1 ) throw Error(2, "(Make_Triangular_Labels)");
        if(N_labels > 0){
	        int last = N_labels-1;
        	double * triangle_centers = new double[N_labels];
        	Calculate_Triangles_Center(triangle_centers,N_labels,
        	Get_Entry_RgeInf(Ind_Entry), Get_Entry_RgeSup(Ind_Entry));
        	char * name_aux = new char[MAX_NAME_SIZE];
        	if ( strcmp( name, EMPTY) == 0) strcpy(name_aux,EMPTY);
        	else sprintf(name_aux, "%s%i",name , 0 );
        	if(N_labels == 1){
	        	Add_Label(Ind_Entry,
                		name_aux, TRI,
        			entries[Ind_Entry].GetRangeInf(),
        			triangle_centers[0],
        			entries[Ind_Entry].GetRangeSup());
        	}else {
        		for(int i = 0; i < N_labels; i++){
        			if ( strcmp( name, EMPTY) == 0) strcpy(name_aux,EMPTY);
        			else sprintf(name_aux, "%s%i",name, i );
        			if( i != 0 && i != last){
        				Add_Label(Ind_Entry, name_aux, TRI,
        						triangle_centers[i-1],
	        					triangle_centers[i],
                						triangle_centers[i+1]);
        			}else if( i == 0){
        				Add_Label(Ind_Entry, name_aux, TRI,
        						entries[Ind_Entry].GetRangeInf(),
        						triangle_centers[i],
        						triangle_centers[i+1]);
        			}else if (i == last){
        				Add_Label(Ind_Entry, name_aux, TRI,
        						triangle_centers[i-1],
        						triangle_centers[i],
        						entries[Ind_Entry].GetRangeSup());
        			}
        		}
        	}
        	delete [] name_aux;
                delete []triangle_centers;
        }
};

/*
Creates triangular labels in the specified entry. Overload method
@deprecated	Creates as many triangles sd the parameter N_labels indicates
@param Name_Entry: String. Entry name.
@param N_Labels: Integer. Number of triangular labels to be added.
@param name: String. New labels names. Opcional parameter with default value.
*/
void AOrbexDos::Make_Triangular_Labels(char * Name_Entry, int N_labels, const  char * name){
	Make_Triangular_Labels(Find_Entry_Name(Name_Entry), N_labels, name);
};

/*
Creates trapezium labels.
@deprecated	Creates as many trapezium as parameter N_labels indicates.
@param Ind_Entry: Integer. Index of the entry in the entry array.
@param proportion: Decimal. The size of the superior base of the trapezium.
First it calculates the maximun posible size of the base and using the proportion
it calculates the actual size. (1 menas maximun size)
@param name: String. New labels names. Opcional parameter with default value.
@param N_labels: Integer. Numbre of trapeziums to be created.
@throws	Exception if the index is not valid
@throwss Excepction if proportion is not in [0,1]
@see Calculate_Triangles_Center, Add_Label
*/
void AOrbexDos::Make_Trapezium_Labels(int Ind_Entry, double proporcion, int N_labels, const char * name){
	if (Ind_Entry < 0 || Ind_Entry > GetNentries() - 1) throw Error(2, "(Make_Trapezium_Labels)");
	if (proporcion < 0 || proporcion > 1) throw Error(3, "(Make_Trapezium_Labels)");
	if(N_labels>0){
                char * name_aux = new char[MAX_NAME_SIZE];
        	double * triangle_centers = new double[N_labels];
        	Calculate_Triangles_Center(triangle_centers,N_labels,
        	Get_Entry_RgeInf(Ind_Entry), Get_Entry_RgeSup(Ind_Entry));

        	/*Determine S value*/
        	double s;
        	if(N_labels == 1){
        		s = proporcion * (Get_Entry_RgeSup(Ind_Entry) -
        	        		Get_Entry_RgeInf(Ind_Entry));
        		if ( strcmp( name, EMPTY) == 0) strcpy(name_aux,EMPTY);
        		else sprintf(name_aux, "%s%i",name, 0 );
        		Add_Label(Ind_Entry, name_aux, TRA,
        				Get_Entry_RgeInf(Ind_Entry),
        				triangle_centers[0] - s/2,
        				triangle_centers[0] + s/2,
        				Get_Entry_RgeSup(Ind_Entry));
        	}else {
        		s = proporcion * ((Get_Entry_RgeSup(Ind_Entry) -
        			Get_Entry_RgeInf(Ind_Entry)) / (N_labels - 1));
        		for(int i = 0; i < N_labels; i++){
        			if ( strcmp( name, EMPTY) == 0) strcpy(name_aux,EMPTY);
        			else sprintf(name_aux, "%s%i",name, i );
        			if( i == 0){
        				Add_Label(Ind_Entry, name_aux, TRA,
        					Get_Entry_RgeInf(Ind_Entry),
        					Get_Entry_RgeInf(Ind_Entry),
        					triangle_centers[i] + s/2,
        					triangle_centers[i+1] - s/2);

        			}else if (i == N_labels -1){
        				Add_Label(Ind_Entry, name_aux, TRA,
        					triangle_centers[i-1] + s/2,
        					triangle_centers[i] - s/2,
        					Get_Entry_RgeSup(Ind_Entry),
        					Get_Entry_RgeSup(Ind_Entry));
        			}else {
        				Add_Label(Ind_Entry, name_aux, TRA,
        					triangle_centers[i-1] + s/2,
	        				triangle_centers[i] - s/2,
        					triangle_centers[i] + s/2,
        					triangle_centers[i+1] - s/2);
        			}
        		}
        	}
        	delete [] name_aux;
                delete [] triangle_centers;
        }
};


/*
Creates trapezium labels.  Overload method
@deprecated	Creates as many trapezium as parameter N_labels indicates.
@param Name_Entry: String. Entry name.
@param proportion: Decimal. The size of the superior base of the trapezium.
First it calculates the maximun posible size of the base and using the proportion
it calculates the actual size. (1 menas maximun size)
@param name: String. New labels names. Opcional parameter with default value.
@param N_labels: Integer. Numbre of trapeziums to be created.
*/
void AOrbexDos::Make_Trapezium_Labels(char * Name_Entry, double proportion, int N_labels, const  char * name){
	Make_Trapezium_Labels(Find_Entry_Name(Name_Entry),proportion,N_labels,name);
};

/*
Creates Gauss labels.
@deprecated	Creates as many Gauss as the parameter N_labels indicates.
@param Ind_Entry: Integer. Index of the entry in the entry array.
@param sigma: Double. Sigma value on Gauss Function.
@param N_labels: Integer. Numbre of Gauss labels to be created.
@param name: String. New labels names. Opcional parameter with default value.
@throws	Exception if the index is not valid
@throwss Excepction if Sigma is zero.
@see Calculate_Triangles_Center, Add_Label
*/
void AOrbexDos::Make_Gauss_Labels(int Ind_Entry, double sigma, int N_labels, const  char * name){
	if (Ind_Entry < 0 || Ind_Entry > GetNentries() -1 ) throw Error(2,"(Make_Gauss_Labels)");
	if (sigma == 0) throw Error(7,"(Make_Gauss_Labels)");
	if(N_labels > 0){
                char  * name_aux = new char[MAX_SIZE];
        	double * triangle_centers = new double[N_labels];
                Calculate_Triangles_Center(triangle_centers,N_labels,
        		Get_Entry_RgeInf(Ind_Entry), Get_Entry_RgeSup(Ind_Entry));

        	for(int i = 0; i < N_labels; i++){
	        	if ( strcmp( name, EMPTY) == 0) strcpy(name_aux,EMPTY);
        		else sprintf(name_aux, "%s%i",name, i );
        		Add_Label(Ind_Entry, name_aux, GSS,
        			triangle_centers[i],
        			sigma);
        	}
        	delete [] name_aux;
                delete [] triangle_centers;
        }
};

/*
Creates Gauss labels.  Overload method
@deprecated	Creates as many Gauss as the parameter N_labels indicates.
@param Name_Entry: String. Entry name.
@param sigma: Double. Sigma value on Gauss Function.
@param N_labels: Integer. Numbre of Gauss labels to be created.
@param name: String. New labels names. Opcional parameter with default value.
*/
void AOrbexDos::Make_Gauss_Labels(char * Name_Entry, double sigma, int N_labels, const  char * name){
	Make_Gauss_Labels(Find_Entry_Name(Name_Entry),  sigma,  N_labels,  name);
};

/*
Creates BELL labels.
@deprecated	Creates as many Bell labels as the parameter N_labels indicates.
@param Ind_Entry: Integer. Index of the entry in the entry array.
@param A: Double. A value on Bell Function.
@param B: Double. B value on Bell Function.
@param N_labels: Integer. Numbre of Gauss labels to be created.
@param name: String. New labels names. Opcional parameter with default value.
@throws	Exception if the index is not valid
@see Calculate_Triangles_Center, Add_Label
*/
void AOrbexDos::Make_Bell_Labels(int Ind_Entry, double A, double B, int N_labels, const char * name){
	if (Ind_Entry < 0 || Ind_Entry > GetNentries() -1 ) throw Error(2,"(Make_Bell_Labels)");
	if (B < 0 || A == 0) throw Error(12,"(Make_Bell_Labels)");
        if(N_labels > 0){
	        char * name_aux = new char[MAX_SIZE];
        	double * triangle_centers = new double[N_labels];
                Calculate_Triangles_Center(triangle_centers,N_labels,
        		Get_Entry_RgeInf(Ind_Entry), Get_Entry_RgeSup(Ind_Entry));

        	for(int i = 0; i < N_labels; i++){//(v0)C  (v1)A (v2)B
        	        	if ( strcmp( name, EMPTY) == 0) strcpy(name_aux,EMPTY);
        		else sprintf(name_aux, "%s%i",name, i );
        		Add_Label(Ind_Entry, name_aux, BLL,	triangle_centers[i],A,B);
        	}
        	delete [] name_aux;
                delete [] triangle_centers;
        }
};

/*
Creates BELL labels.  Overload labels.
@deprecated	Creates as many Bell labels as the parameter N_labels indicates.
@param Name_Entry: String. Entry name.
@param A: Double. A value on Bell Function.
@param B: Double. B value on Bell Function.
@param N_labels: Integer. Numbre of Gauss labels to be created.
@param name: String. New labels names. Opcional parameter with default value.
*/
void AOrbexDos::Make_Bell_Labels(char * Name_Entry, double A, double B, int N_labels, const  char * name ){
	Make_Bell_Labels(Find_Entry_Name(Name_Entry),A,  B, N_labels,name);
};

/*
Creates PI labels.
@deprecated	Creates as many PI as parameter N_labels indicates.
@param Ind_Entry: Integer. Index of the entry in the entry array.
@param proportion: Decimal. The size of the superior base of the trapezium.
First it calculates the maximun posible size of the base and using the proportion
it calculates the actual size. (1 menas maximun size)
@param name: String. New labels names. Opcional parameter with default value.
@param N_labels: Integer. Numbre of trapeziums to be created.
@throws	Exception if the index is not valid
@throwss Excepction if proportion is not in [0,1]
@see Calculate_Triangles_Center, Add_Label
*/
void AOrbexDos::Make_PI_Labels(int Ind_Entry, double proporcion, int N_labels, const char * name){
	if (Ind_Entry < 0 || Ind_Entry > GetNentries() - 1) throw Error(2, "(Make_PI_Labels)");
	if (proporcion < 0 || proporcion > 1) throw Error(3, "(Make_PI_Labels)");
        if(N_labels > 0){
        	char * name_aux = new char[MAX_SIZE];
	double * triangle_centers = new double[N_labels];
                Calculate_Triangles_Center(triangle_centers,N_labels,
        		Get_Entry_RgeInf(Ind_Entry), Get_Entry_RgeSup(Ind_Entry));
        	/*Determine S*/
        	double s;
        	if(N_labels == 1){
        		s = proporcion * (Get_Entry_RgeSup(Ind_Entry) -
        			Get_Entry_RgeInf(Ind_Entry));
        		if ( strcmp( name, EMPTY) == 0) strcpy(name_aux,EMPTY);
        		else sprintf(name_aux, "%s%i",name, 0 );
        		Add_Label(Ind_Entry, name_aux, FPI,
        				Get_Entry_RgeInf(Ind_Entry),
        				triangle_centers[0] - s/2,
        				triangle_centers[0] + s/2,
        				Get_Entry_RgeSup(Ind_Entry));
        	}else {
        		s = proporcion * ((Get_Entry_RgeSup(Ind_Entry) -
        			Get_Entry_RgeInf(Ind_Entry)) / (N_labels - 1));
        		for(int i = 0; i < N_labels; i++){
        			if ( strcmp( name, EMPTY) == 0) strcpy(name_aux,EMPTY);
        			else sprintf(name_aux, "%s%i",name, i );
        			if( i == 0){
        				Add_Label(Ind_Entry, name_aux, FPI,
        					Get_Entry_RgeInf(Ind_Entry),
        					Get_Entry_RgeInf(Ind_Entry),
        					triangle_centers[i] + s/2,
        					triangle_centers[i+1] - s/2);

        			}else if (i == N_labels -1){
        				Add_Label(Ind_Entry, name_aux, FPI,
        					triangle_centers[i-1] + s/2,
        					triangle_centers[i] - s/2,
        					Get_Entry_RgeSup(Ind_Entry),
        					Get_Entry_RgeSup(Ind_Entry));
        			}else {
        				Add_Label(Ind_Entry, name_aux, FPI,
        					triangle_centers[i-1] + s/2,
        					triangle_centers[i] - s/2,
        					triangle_centers[i] + s/2,
        					triangle_centers[i+1] - s/2);
        			}
        		}
        	}
        	delete [] name_aux;
                delete [] triangle_centers;
        }
};

/*
Creates PI labels. Overload method.
@deprecated	Creates as many PI as parameter N_labels indicates.
@param Name_Entry: String. Entry name.
@param proportion: Decimal. The size of the superior base of the trapezium.
First it calculates the maximun posible size of the base and using the proportion
it calculates the actual size. (1 menas maximun size)
@param name: String. New labels names. Opcional parameter with default value.
@param N_labels: Integer. Numbre of trapeziums to be created.
*/
void AOrbexDos::Make_PI_Labels(char * Name_Entry, double proporcion, int N_labels, const char * name){
	Make_PI_Labels(Find_Entry_Name(Name_Entry), proporcion, N_labels,name);
};

/*
Returns the label in the specified position
@param	Ind_Entry: Integer. Entry position.
@param Ind_Label: Integer. Label position.
@throws	Exception if the entry o label index are invalid
*/
void AOrbexDos::Get_Label(Label * ret,int Ind_Entry, int Ind_Label){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Get_Label)");
	if (Ind_Label < 0 || Ind_Label >= Get_Entry_Nlabels(Ind_Entry)) throw Error(16,"(Get_Label)");
	entries[Ind_Entry].GetLabel(ret,Ind_Label);
};

/*
Returns the specified label. Overload method
@param	Name_Entry: String. Entry name.
@param Name_Label: String. Label name.
*/
void AOrbexDos::Get_Label(Label * ret,char * Name_Entry, char * Name_Label){
	Get_Label(ret,Find_Entry_Name(Name_Entry),Find_Label_Name(Find_Entry_Name(Name_Entry),Name_Label));
};

/*
Returns the specified label. Overload method
@param	Name_Entry: String. Entry name.
@param Ind_Label: Integer. Label position.
*/
void AOrbexDos::Get_Label(Label * ret,char * Name_Entry, int Ind_Label){
	Get_Label(ret,Find_Entry_Name(Name_Entry),Ind_Label);
};

/*
Returns the specified label. Overload method
@param	Ind_Entry: Integer. Entry position.
@param Name_Label: String. Label name.
*/
void AOrbexDos::Get_Label(Label * ret,int Ind_Entry, char * Name_Label){
	Get_Label(ret,Ind_Entry,Find_Label_Name(Ind_Entry,Name_Label));
};

/*
Sets the specified label on the specified position and entry.
@param Ind_Entry: Integer. Index of entry.
@param Ind_Label: Integer. Index of labels
@param label_new: Label. new label
@throws exception if the indexes are invalid
*/
void AOrbexDos::Set_Label(int Ind_Entry, int Ind_Label, Label * label_new){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Set_Label)");
	if (Ind_Label < 0 || Ind_Label >= Get_Entry_Nlabels(Ind_Entry)) throw Error(16,"(Set_Label)");

	if      ((*label_new).GetType() != TRI
                && (*label_new).GetType() != TRA
                && (*label_new).GetType() != GSS
                && (*label_new).GetType()!= BLL
                && (*label_new).GetType() != FPI)
                throw Error(4,"(Set_Label)"); // Possible types
	if      (((*label_new).GetType() == TRI)
                && !( (*label_new).GetValue(0) <= (*label_new).GetValue(1)
                && (*label_new).GetValue(1) <= (*label_new).GetValue(2))){
		throw Error(6,"(Set_Label)TRI-"); }//Restrictions TRI
	if      (((*label_new).GetType() == TRA)
                && !( (*label_new).GetValue(0) <= (*label_new).GetValue(1)
                        && (*label_new).GetValue(1) <= (*label_new).GetValue(2)
                        && (*label_new).GetValue(2) <= (*label_new).GetValue(3)
                        && (*label_new).GetValue(3) != INFINITO)){
		throw Error(6,"(Set_Label)TRA-"); }//RestrictionsFPI
	if      ( ((*label_new).GetType() == GSS)
                && ((*label_new).GetValue(1) == 0
                        || (*label_new).GetValue(2) != INFINITO
                        || (*label_new).GetValue(3) != INFINITO ))
		throw Error(12,"(Set_Label)GSS-"); //Restrictions Gauss
	if      ( ((*label_new).GetType() == BLL)
                && ((*label_new).GetValue(2) < 0
                        || (*label_new).GetValue(3) != INFINITO
                        || (*label_new).GetValue(1) == 0) )
		throw Error(12,"(Set_Label)BLL-");	//Restrictions bell
	if      (((*label_new).GetType() == FPI)
                        &&!( (*label_new).GetValue(0) <= (*label_new).GetValue(1)
                                && (*label_new).GetValue(1) <= (*label_new).GetValue(2)
                                && (*label_new).GetValue(2) <= (*label_new).GetValue(3)
                                && (*label_new).GetValue(3) != INFINITO)){
		throw Error(6,"(Set_Label)PI-"); }//Restrictions FPI
	entries[Ind_Entry].SetLabel(Ind_Label,label_new);
};

/*
Sets the specified label on the specified position and entry. Overload method
@param Name_Entry: String. Name of entry
@param Name_Label: String. Name of label
@param label_new: Label. new label
*/
void AOrbexDos::Set_Label(char * Name_Entry, char * Name_Label, Label * label_new){
	Set_Label(Find_Entry_Name(Name_Entry),
		Find_Label_Name(Find_Entry_Name(Name_Entry),Name_Label),label_new);
};

/*
Sets the specified label on the specified position and entry. Overload method
@param Name_Entry: String. Name of entry
@param Ind_Label: Integer. Index of labels
@param label_new: Label. new label
*/
void AOrbexDos::Set_Label(char * Name_Entry, int Ind_Label, Label * label_new){
	Set_Label(Find_Entry_Name(Name_Entry),Ind_Label,label_new);
};

/*
Sets the specified label on the specified position and entry. Overload method
@param Ind_Entry: Integer. Index of entry.
@param Name_Label: String. Name of label
@param label_new: Label. new label
*/
void AOrbexDos::Set_Label(int Ind_Entry, char * Name_Label, Label * label_new){
	Set_Label(Ind_Entry,Find_Label_Name(Ind_Entry,Name_Label),label_new);
};

/*
Returns the name of the specified label.
@param	Ind_Entry: Integer. Entry position.
@param Ind_Label: Integer. Label position.
@throws	Exception if the entry o label index are invalid
*/
void AOrbexDos::Get_Label_Name(char * ret,int Ind_Entry, int Ind_Label){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Get_Label_Name)");
	if (Ind_Label < 0 || Ind_Label >= Get_Entry_Nlabels(Ind_Entry)) throw Error(16,"(Get_Label_Name)");
        Label label;
        entries[Ind_Entry].GetLabel(&label,Ind_Label);
	label.GetName(ret);
};

/*
Returns the name of the specified label. Overload method
@param	Name_Entry: String. Entry name.
@param Ind_Label: Integer. Label position.
*/
void AOrbexDos::Get_Label_Name(char *ret,char * Name_Entry, int Ind_Label){
	Get_Label_Name(ret,Find_Entry_Name(Name_Entry),Ind_Label);
};

/*
Sets the name of the specified Label
@param Ind_Entry: Integer. Index of entries.
@param Ind_Label: Integer. Index of labels
@param New_Name: String. New name of the label.
@thow Exception if any of the indexes are invalid
*/
void AOrbexDos::Set_Label_Name(int Ind_Entry, int Ind_Label, char * New_Name ){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Change_Label_Name)");
	if (Ind_Label < 0 || Ind_Label >= Get_Entry_Nlabels(Ind_Entry)) throw Error(16,"(Change_Label_Name)");
	Label new_label;
        Get_Label(&new_label,Ind_Entry, Ind_Label);
	new_label.SetName(New_Name);
	Set_Label(Ind_Entry, Ind_Label, &new_label);
};

/*
Sets the name of the specified Label. Overload method
@param Name_Entry: String. Name of entrie
@param Ind_Label: Integer. Index of label
@param New_Name: String. New name of the label.
*/
void AOrbexDos::Set_Label_Name(char * Name_Entry, int Ind_Label, char * New_Name){
	Set_Label_Name(Find_Entry_Name(Name_Entry), Ind_Label,  New_Name);};

/*
Sets the name of the specified Label. Overload method
@param Ind_Entry: Integer. Index of entrie
@param Name_Label: String. Name of label
@param New_Name: String. New name of the label.
*/
void AOrbexDos::Set_Label_Name(int Ind_Entry, char * Name_Label, char * New_Name){
	Set_Label_Name(Ind_Entry, Find_Label_Name(Ind_Entry,Name_Label), New_Name);};

/*
Sets the name of the specified Label. Overload method
@param Name_Entry: String. Name of entrie
@param Name_Label: String. Name of label
@param New_Name: String. New name of the label.
*/
void AOrbexDos::Set_Label_Name(char * Name_Entry, char * Name_Label,
	char * New_Name){ Set_Label_Name(Find_Entry_Name(Name_Entry),
	Find_Label_Name(Find_Entry_Name(Name_Entry),Name_Label), New_Name);};

/*
Returns the tpye of the specified label.
@param Ind_Entry: Integer. Index of entries.
@param Ind_Label: Integer. Index of labels
@thow Exception if any of the indexes are invalid
*/
int AOrbexDos::Get_Label_Type(int Ind_Entry, int Ind_Label){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Get_Label_Type)");
	if (Ind_Label < 0 || Ind_Label >= Get_Entry_Nlabels(Ind_Entry)) throw Error(16,"(Get_Label_Type)");
        Label label;
        Get_Label(&label,Ind_Entry, Ind_Label);
	return label.GetType();
};

/*
Returns the tpye of the specified label. Overload method.
@param Name_Entry: String. Name of entry
@param Name_Label: String. Name of label
*/
int AOrbexDos::Get_Label_Type(char * Name_Entry, char * Name_Label){
	return Get_Label_Type(Find_Entry_Name(Name_Entry),
		Find_Label_Name(Find_Entry_Name(Name_Entry),Name_Label));
};

/*
Returns the tpye of the specified label. Overload method.
@param Name_Entry: String. Name of entry
@param Ind_Label: Integer. Index of labels
*/
int AOrbexDos::Get_Label_Type(char * Name_Entry, int Ind_Label){
	return Get_Label_Type(Find_Entry_Name(Name_Entry),Ind_Label);
};

/*
Returns the tpye of the specified label. Overload method.
@param Ind_Entry: Integer. Index of entry.
@param Name_Label: String. Name of label
*/
int AOrbexDos::Get_Label_Type(int Ind_Entry, char * Name_Label){
	return Get_Label_Type(Ind_Entry,Find_Label_Name(Ind_Entry,Name_Label));
};

/*
Returns the values of the specified label.
@param Ind_Entry: Integer. Index of entries.
@param Ind_Label: Integer. Index of labels
@thow Exception if any of the indexes are invalid
*/
void AOrbexDos::Get_Label_Values(double * ret,int Ind_Entry, int Ind_Label){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Get_Label_Values)");
	if (Ind_Label < 0 || Ind_Label >= Get_Entry_Nlabels(Ind_Entry)) throw Error(16,"(Get_Label_Values)");
        Label label;
	Get_Label(&label,Ind_Entry, Ind_Label);
        label.GetValues(ret);
};

/*
Returns the values of the specified label. Overload method.
@param Name_Entry: String. Name of entry.
@param Name_Label: String. Name of label
*/
void AOrbexDos::Get_Label_Values(double * ret,char * Name_Entry, char * Name_Label){
	Get_Label_Values(ret,Find_Entry_Name(Name_Entry),
		Find_Label_Name(Find_Entry_Name(Name_Entry),Name_Label));
};

/*
Returns the values of the specified label. Overload method.
@param Name_Entry: String. Name of entry.
@param Ind_Label: Integer. Index of labels
*/
void AOrbexDos::Get_Label_Values(double * ret,char * Name_Entry, int Ind_Label){
	Get_Label_Values(ret,Find_Entry_Name(Name_Entry),Ind_Label);
};

/*
Returns the values of the specified label. Overload method.
@param Ind_Entry: Integer. Index of entries.
@param Name_Label: String. Name of label
*/
void AOrbexDos::Get_Label_Values(double * ret,int Ind_Entry, char * Name_Label){
	Get_Label_Values(ret,Ind_Entry,Find_Label_Name(Ind_Entry,Name_Label));
};

/*
Sets the tpye and values of the specified label.
@param Ind_Entry: Integer. Index of entry.
@param Ind_Label: Integer. Index of labels
@param type: Integer. new type of label
@param values: Double array. New values
@thow Exception if any of the indexes are invalid
@throw exception if the new type is invalid
*/
void AOrbexDos::Set_Label_Values(int Ind_Entry, int Ind_Label, int type, double * values){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Set_Label_Values)");
	if (Ind_Label < 0 || Ind_Label >= Get_Entry_Nlabels(Ind_Entry)) throw Error(16,"(Set_Label_Values)");
	if (type != TRI && type != TRA && type != GSS && type!= BLL && type != FPI)
		throw Error(4,"(Set_Label_Values)"); // Posibles tipos
	Label label;
        Get_Label(&label,Ind_Entry, Ind_Label);
	label.SetValues(values,type);
	Set_Label(Ind_Entry, Ind_Label, &label);
};

/*
Sets the tpye and values of the specified label. Overload method
@param Name_Entry: String. Name of entry
@param Name_Label: String. Name of label
@param type: Integer. new type of label
@param values: Double array. New values
*/
void AOrbexDos::Set_Label_Values(char * Name_Entry, char * Name_Label, int type, double * values){
	Set_Label_Values(Find_Entry_Name(Name_Entry),
		Find_Label_Name(Find_Entry_Name(Name_Entry),Name_Label),type, values);
};

/*
Sets the tpye and values of the specified label. Overload method
@param Name_Entry: String. Name of entry
@param Ind_Label: Integer. Index of labels
@param type: Integer. new type of label
@param values: Double array. New values
*/
void AOrbexDos::Set_Label_Values(char * Name_Entry, int Ind_Label, int type, double * values){
	Set_Label_Values(Find_Entry_Name(Name_Entry),Ind_Label,type, values);
};

/*
Sets the tpye and values of the specified label. Overload method
@param Ind_Entry: Integer. Index of entry.
@param Name_Label: String. Name of label
@param type: Integer. new type of label
@param values: Double array. New values
*/
void AOrbexDos::Set_Label_Values(int Ind_Entry, char * Name_Label, int type, double * values){
	Set_Label_Values(Ind_Entry,Find_Label_Name(Ind_Entry,Name_Label),type, values);
};

/*
Returns the degree of the specified label.
@param Ind_Entry: Integer. Index of entry.
@param Ind_Label: Integer. Index of labels
@param type: Integer. new type of label
@param values: Double array. New values
@throw exception if any of the indexes are invalid
*/
double AOrbexDos::Get_Label_Degree(int Ind_Entry, int Ind_Label){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Get_Label_Degree)");
	if (Ind_Label < 0 || Ind_Label >= Get_Entry_Nlabels(Ind_Entry)) throw Error(16,"(Get_Label_Degree)");
        Label label;
        Get_Label(&label,Ind_Entry, Ind_Label);
        return label.GetDegree();
};

/*
Returns the degree of the specified label. Overload method
@param Name_Entry: String. Name of entry
@param Name_Label: String. Name of label
@param type: Integer. new type of label
@param values: Double array. New values
*/
double AOrbexDos::Get_Label_Degree(char * Name_Entry, char * Name_Label){
	return Get_Label_Degree(Find_Entry_Name(Name_Entry),
		Find_Label_Name(Find_Entry_Name(Name_Entry),Name_Label));
};

/*
Returns the degree of the specified label. Overload method
@param Name_Entry: String. Name of entry
@param Ind_Label: Integer. Index of labels
@param type: Integer. new type of label
@param values: Double array. New values
*/
double AOrbexDos::Get_Label_Degree(char * Name_Entry, int Ind_Label){
	return Get_Label_Degree(Find_Entry_Name(Name_Entry),Ind_Label);
};

/*
Returns the degree of the specified label. Overload method
@param Ind_Entry: Integer. Index of entry.
@param Name_Label: String. Name of label
@param type: Integer. new type of label
@param values: Double array. New values
*/
double AOrbexDos::Get_Label_Degree(int Ind_Entry, char * Name_Label){
	return Get_Label_Degree(Ind_Entry,Find_Label_Name(Ind_Entry,Name_Label));
};

/*
Sets the degree of the specified label.
@param Ind_Entry: Integer. Index of entry.
@param Ind_Label: Integer. Index of labels
*/
void AOrbexDos::Set_Label_Degree(int Ind_Entry, int Ind_Label, double Degree){
	Label label;
        Get_Label(&label,Ind_Entry,Ind_Label);
	label.SetDegree(Degree);
        Set_Label(Ind_Entry,Ind_Label, &label);
};

/*METHODS ABOUT OUTS AND SINGLETONS*/
/*
Add an Out
@deprecated Adds an instance of an Var_Out. Allocates memory for the new Out.
@see Copy_Array_Out, Duplicate_Out
*/
void AOrbexDos::Add_Out(Var_Out * outn){
	Var_Out * Aux_Out = new Var_Out[GetNouts()+1];
	double * Aux_OutValue = new double[GetNouts()+1];
	for( int i = 0 ; i < GetNouts() ; i++){
		Aux_Out[i].Equal(&(outs[i]));
		Aux_OutValue[i] = Out_Values[i];
	}
	Aux_OutValue[Nout] = 0;
        Aux_Out[Nout].Equal(outn);
	if(GetNouts() > 0){
		delete [] outs;
		delete [] Out_Values;
	}
        Nout++;
	outs = new Var_Out[Nout];
	Copy_Array_Out(outs,Aux_Out, Nout);
	Out_Values = new double[Nout];
        Copy_Double_Array(Out_Values,Aux_OutValue, Nout);
	delete [] Aux_Out;
	delete [] Aux_OutValue;
};

/*add out with name*/
void AOrbexDos::Add_Out(char * Out_Name){
        Singleton si[1];
        Var_Out vo(Out_Name,0,si);
	Add_Out(&vo);
};

/*
Delete Out
@param Ind_Out: Integer. index of the Var_Out to be deleted
@throw Exception invalid index
@see Copy_Array_Out, Duplicate_Out
*/
void AOrbexDos::Delete_Out(int Ind_Out){
	if (Ind_Out < 0 || Ind_Out >= Nout) throw Error(8,"(Delete_Out)");
	Delete_Rule_Asociatedwith_Out(Ind_Out);//Delete Rule and change other rules
	Var_Out * Aux_Out = NULL;
        double * Aux_OutValue = NULL;
        if((GetNouts()-1) > 0){
                 Aux_Out = new Var_Out[GetNouts()-1];
                 Aux_OutValue = new double[GetNouts()-1];
        	for( int i = 0; i < Ind_Out; i++){
        		Aux_Out[i].Equal(&(outs[i]));
        		Aux_OutValue[i] = Out_Values[i];
                }
                for( int i =(Ind_Out+1) ; i < Nout; i++){
        		Aux_Out[i-1].Equal(&(outs[i]));
        		Aux_OutValue[i-1] = Out_Values[i];
                }
        }
	delete [] outs;
	delete [] Out_Values;
	Nout--;
        if((GetNouts()) > 0){
                outs = new Var_Out[Nout];
        	Copy_Array_Out(outs,Aux_Out, Nout);
        	Out_Values = new double[Nout];
                Copy_Double_Array(Out_Values,Aux_OutValue, Nout);
        	delete [] Aux_Out;
        	delete [] Aux_OutValue;
        }
};

/*
Delete Out. Overload method
@param Name_Out: String. name of the Var_Out to be deleted
*/
void AOrbexDos::Delete_Out(char * Name_Out){ Delete_Out(Find_Out_Name(Name_Out));};

/*
Initialization of an Var_Out
*/
void AOrbexDos::Set_Out(int Ind_Out, Var_Out * outn){
	if (Ind_Out < 0 || Ind_Out >= Nout) throw Error(8,"(Set_Out)");
	outs[Ind_Out].Equal(outn);
}

/*
Initialization of an Var_Out
*/
void AOrbexDos::Set_Out(int Ind_Out, char* Out_Name, int n_values, Singleton * values){
	if (Ind_Out < 0 || Ind_Out >= Nout) throw Error(8,"(Set_Out)");
	Var_Out out(Out_Name, n_values, values);
	Set_Out(Ind_Out,&out);
};

/*
Returns the specified out
@param	Ind_Out: Integer. Index of array outs.
@throws	Exception if the Out index is invalid
*/
void AOrbexDos::Get_Out(Var_Out *ret,int Out_Index){
	if (Out_Index < 0 || Out_Index >= Nout) throw Error(8,"(Get_Out)");
        (*ret).Equal(&(outs[Out_Index]));
};

/*
Returns the specified out. Overload method
@param Out_Name: String. Name of the Out
*/
void AOrbexDos::Get_Out(Var_Out *ret,char * Out_Name){Get_Out(ret,Find_Out_Name(Out_Name));};

/*
Set the name of the specified.
@param	Ind_Out: Integer. Out index.
@param New_Name: Strig. Name of the out
@throws	Exception if the out index is invalid
*/
 void AOrbexDos::Set_Out_Name(int Ind_Out, char * New_Name){
	if (Ind_Out < 0 || Ind_Out >= Nout) throw Error(8,"(Set_Out_Name)");
	outs[Ind_Out].SetName(New_Name);
};

/*
Set the name of the specified. Overload method
@param	Name_Out: String. Out name.
@param New_Name: Strig. Name of the out
*/
void AOrbexDos::Set_Out_Name(char *  Name_Out, char * New_Name){
	Set_Out_Name(Find_Out_Name(Name_Out),New_Name);
};

/*
Returns the name of the specified out.
@param	Ind_Out: Integer. Index of out.
@throws	Exception if the out index is invalid
*/
void AOrbexDos::Get_Out_Name(char * ret,int Out_Index){
	if (Out_Index < 0 || Out_Index >= Nout) throw Error(8,"(Get_Out_Name)");
	outs[Out_Index].GetName(ret);
};

/*
Returns the number of singletons of the specified out.
@param	Ind_Out: Integer. Index of out.
@throws	Exception if the out index is invalid
*/
int AOrbexDos::Get_Out_Nvalues(int Out_Index){
	if (Out_Index < 0 || Out_Index >= Nout) throw Error(8,"(Get_Out_Nvalues)");
	return outs[Out_Index].GetNvalues();
};

/*
Returns the number of singletons of the specified out. Overload methodd.
@param	Name_Out: String. Out name.
*/
int AOrbexDos::Get_Out_Nvalues(char * Out_Name){
	return Get_Out_Nvalues(Find_Out_Name(Out_Name));
};


/*
Add "manually" a singleton .
@deprecated	Adds a new singleton with value and name in the parameters.
@param Ind_Out; Integer. Index of Out.
@param name: String. Singleton name.
@param value: Double. Value of the Singleton.
@param pos: Integer. Position in the array where the singleton will be added.
Default value, INFINITE represents last pos.
@throws	Exception invalid index on Out array.
@throwss Exception invalid posicion
@see Copy_Array_Singleton
*/
void AOrbexDos::Add_Singleton(int Ind_Out,char * name, double value, int pos){
	if (Ind_Out < 0 || Ind_Out > GetNouts() -1 ) throw Error(8,"(Add_Singleton)");
	if (pos != INFINITO && (pos < 0 || pos >= Get_Out_Nvalues(Ind_Out) ))
		throw Error(9,"(Add_Singleton)");
	Singleton sing(name,value);
	outs[Ind_Out].AddSingleton(&sing,pos);
};

/*
Add "manually" a singleton. Overload method
@deprecated	Adds a new singleton with value and name in the parameters.
@param Name_Out; String. Name of Out.
@param name: String. Singleton name.
@param value: Double. Value of the Singleton.
@param pos: Integer. Position in the array where the singleton will be added.
Default value, INFINITE represents last pos.
*/
void AOrbexDos::Add_Singleton(char * Name_Out,char * name, double value, int pos){
	Add_Singleton(Find_Out_Name(Name_Out),name, value, pos);
};

/*
Delete Singleton.
@deprecated given an index of out array and an index of singleton array.
It errases the singleton from singleto�s array in the specified Var_Out.
It also errases the rules asociated with that singleton.
@param Ind_Out: Integer. Index of Out.
@param Ind_Value: Integer. Index of Singleton.
@thow Exception if any of the indexes is invalid
@see Duplicate_Singleton,Copy_Array_Singleton,Delete_Rule_Asociatedwith_Singleton
*/
void AOrbexDos::Delete_Singleton(int Ind_Out, int Ind_Value){
	if (Ind_Out < 0 || Ind_Out >= Nout) throw Error(8,"(Delete_Singleton)");
	if (Ind_Value < 0 || Ind_Value >= Get_Out_Nvalues(Ind_Out)) throw Error(9,"(Delete_Singleton)");
	Delete_Rule_Asociatedwith_Singleton(Ind_Out, Ind_Value);
	outs[Ind_Out].DeleteSingleton(Ind_Value);
};

/*
Delete Singleton. Overload method
@deprecated given an index of out array and an index of singleton array.
It errases the singleton from singleto�s array in the specified Var_Out.
It also errases the rules asociated with that singleton.
@param name_Out: string. name  of Out.
@param name_Value: string. name  of Singleton.
*/
void AOrbexDos::Delete_Singleton(char * Name_Out, char * Name_Value){
	Delete_Singleton(Find_Out_Name(Name_Out),
		Find_Singleton_Name(Find_Out_Name(Name_Out),Name_Value));
};


/*
Delete Singleton. Overload method
@deprecated given an index of out array and an index of singleton array.
It errases the singleton from singleto�s array in the specified Var_Out.
It also errases the rules asociated with that singleton.
@param Ind_Out: Integer. Index of Out.
@param name_Value: string. name  of Singleton.
*/
void AOrbexDos::Delete_Singleton(int Ind_Out, char * Name_Value){
	Delete_Singleton(Ind_Out,Find_Singleton_Name(Ind_Out,Name_Value));
};

/*
Delete Singleton. Overload method
@deprecated given an index of out array and an index of singleton array.
It errases the singleton from singleto�s array in the specified Var_Out.
It also errases the rules asociated with that singleton.
@param name_Out: string. name  of Out.
@param Ind_Value: Integer. Index of Singleton.
*/
void AOrbexDos::Delete_Singleton(char * Name_Out, int Ind_Value){
	Delete_Singleton(Find_Out_Name(Name_Out), Ind_Value);
};

/*
Creates equally distributed Singletons. 
@deprecated	Creates as many singletons as parameter N_Singletons indicates.
@param Ind_Out: Integer. Index of the out in the out array.
@param name: String. New singleto�s names. Opcional parameter with default value.
@param N_Singleton: Integer. Numbre of singletons to be created.
@throws	Exception if the index is not valid
@throwss Excepction if N_Singleton is less than 0.
@see Calculate_Triangles_Center, Add_Label
*/
void AOrbexDos::Make_Singletons(int Ind_Out, int N_Singletons, double inf_limit, double sup_limit, const  char * name){
	if (Ind_Out < 0 || Ind_Out >= Nout) throw Error(8,"(Make_Singletons)");
	if( N_Singletons <= 0) throw Error(0,"(Make_Singletons)");
        if(N_Singletons>0){
        	char * name_aux = new char[MAX_SIZE];
        	char * name_aux1= new char[MAX_SIZE];
        	if(strcmp(name, "EMPTY") == 0 ) Get_Out_Name(name_aux,Ind_Out);
        	else strcpy(name_aux, name);
        	double * singletons = new double[N_Singletons];
                Calculate_Triangles_Center(singletons,N_Singletons,
        	inf_limit,sup_limit);
        	for(int i = 0; i < N_Singletons; i++){
        		sprintf(name_aux1,"%s%i",name_aux,i );
        		Add_Singleton(Ind_Out,name_aux1,singletons[i]);
        	}
        	delete [] name_aux;
        	delete [] name_aux1;
                delete [] singletons;
        }
};

/*
Creates equally distributed Singletons.  Overload method
@deprecated	Creates as many singletons as parameter N_Singletons indicates.
@param Name_Out: String.
@param name: String. New singleto�s names. Opcional parameter with default value.
@param N_Singleton: Integer. Numbre of singletons to be created.
*/
void AOrbexDos::Make_Singletons(char * Name_Out, int N_Singletons, double inf, double sup, const  char * name){
	Make_Singletons(Find_Out_Name(Name_Out), N_Singletons,inf,sup, name );
};

/*
Returns the specified singleton.
@param Ind_Out: Integer. Index of Out.
@param Ind_Value: Integer. Index of singleton.
@throws	Exception invalid index.
*/
void AOrbexDos::Get_Singleton(Singleton * ret,int Out_Index, int Ind_Value){
	if (Out_Index < 0 || Out_Index >= Nout) throw Error(8,"(Get_Singleton)");
	if (Ind_Value < 0 || Ind_Value >= Get_Out_Nvalues(Out_Index)) throw Error(9,"(Get_Singleton)");
	outs[Out_Index].GetSingleton(ret,Ind_Value);
};

/*
Returns the specified singleton. Overload method.
@param Name_Out: String. Name  of Out.
@param Name_Value: String. Name  of singleton.
*/
void AOrbexDos::Get_Singleton(Singleton * ret,char * Out_Name, char * Name_Value){
	Get_Singleton(ret,Find_Out_Name(Out_Name),Find_Singleton_Name(Find_Out_Name(Out_Name),Name_Value));
};

/*
Returns the specified singleton. Overload method.
@param Name_Out: String. Name  of Out.
@param Ind_Value: Integer. Index of singleton.
*/
void AOrbexDos::Get_Singleton(Singleton * ret,char * Out_Name, int Ind_Value){
	Get_Singleton(ret,Find_Out_Name(Out_Name),Ind_Value);
};

/*
Returns the specified singleton. Overload method.
@param Ind_Out: Integer. Index of Out.
@param Name_Value: String. Name  of singleton.
*/
void AOrbexDos::Get_Singleton(Singleton * ret,int Out_Index, char * Name_Value){
	Get_Singleton(ret,Out_Index,Find_Singleton_Name(Out_Index,Name_Value));
};

/*
Sets the specified singleton.
@param Ind_Out: Integer. Index of Out.
@param Ind_Value: Integer. Index of singleton.
@param sing: SIngleton. New.
@throw exception if indexes are invalid
*/
void AOrbexDos::Set_Singleton(int Out_Index,int Ind_Value, Singleton * sing){
	if (Out_Index < 0 || Out_Index >= Nout) throw Error(8,"(Set_Singleton)");
	if (Ind_Value < 0 || Ind_Value >= Get_Out_Nvalues(Out_Index)) throw Error(9,"(Set_Singleton)");
	outs[Out_Index].SetSingleton(Ind_Value,sing);
};

/*
Sets the specified singleton. Overload method
@param Name_Out: String. Name  of Out.
@param Name_Value: String. Name  of singleton.
@param sing: SIngleton. New.
*/
void AOrbexDos::Set_Singleton(char * Out_Name, char * Name_Value, Singleton * sing){
	Set_Singleton(Find_Out_Name(Out_Name)
		,Find_Singleton_Name(Find_Out_Name(Out_Name),Name_Value),sing);
};

/*
Sets the specified singleton. Overload method
@param Name_Out: String. Name  of Out.
@param Ind_Value: Integer. Index of singleton.
@param sing: SIngleton. New.
*/
void AOrbexDos::Set_Singleton(char * Out_Name, int Ind_Value,Singleton * sing){
	Set_Singleton(Find_Out_Name(Out_Name),Ind_Value,sing);
};


/*
Sets the specified singleton. Overload method
@param Ind_Out: Integer. Index of Out.
@param Name_Value: String. Name  of singleton.
@param sing: SIngleton. New.
*/
void AOrbexDos::Set_Singleton(int Out_Index, char * Name_Value,Singleton * sing){
	Set_Singleton(Out_Index,Find_Singleton_Name(Out_Index,Name_Value),sing);
};

/*
Set the value of a specefied Singleton.
@deprecated given an index of out array and an index of singleton array and
a new value. It changes the value of the singleton.
@param Ind_Out: Integer. Index of out.
@param Ind_Value: Integer. Index of singleton
@param value: Double. New value of the singleton.
@thow Exception if any of the indexes is invalid
*/
void AOrbexDos::Set_Singleton_Value(int Ind_Out, int Ind_Value, double Value){
	if (Ind_Out < 0 || Ind_Out >= Nout) throw Error(8,"(Set_Singleton_Value)");
	if (Ind_Value < 0 || Ind_Value >= Get_Out_Nvalues(Ind_Out)) throw Error(9,"(Set_Singleton_Value)");
	char nameAux[MAX_NAME_SIZE];
        outs[Ind_Out].Get_SingletonName(nameAux,Ind_Value);
        Singleton singNew(nameAux,Value);
	Set_Singleton(Ind_Out,Ind_Value,&singNew);
};

/*
Set the value of a specefied Singleton. Overload method.
@deprecated given an index of out array and an index of singleton array and
a new value. It changes the value of the singleton.
@param Name_out: String. Name  of out.
@param Name_Value: String. Name  of singleton.
@param value: Double. New value of the singleton.
*/
void AOrbexDos::Set_Singleton_Value(char * Name_Out, char * Name_Value, double value){
	Set_Singleton_Value(Find_Out_Name(Name_Out),
		Find_Singleton_Name(Find_Out_Name(Name_Out),Name_Value), value);};

/*
Set the value of a specefied Singleton. Overload method.
@deprecated given an index of out array and an index of singleton array and
a new value. It changes the value of the singleton.
@param Ind_Out: Integer. Index of out.
@param Name_Value: String. Name  of singleton.
@param value: Double. New value of the singleton.
*/
void AOrbexDos::Set_Singleton_Value(int Ind_Out, char * Name_Value, double value){
	Set_Singleton_Value(Ind_Out, Find_Singleton_Name(Ind_Out,Name_Value),value);};

/*
Set the value of a specefied Singleton. Overload method.
@deprecated given an index of out array and an index of singleton array and
a new value. It changes the value of the singleton.
@param Name_out: String. Name  of out.
@param Ind_Value: Integer. Index of singleton
@param value: Double. New value of the singleton.
*/
void AOrbexDos::Set_Singleton_Value(char * Name_Out, int Ind_Value, double value){
	Set_Singleton_Value(Find_Out_Name(Name_Out),Ind_Value, value);};

/*
Returns the value of a specefied Singleton.
@param Ind_Out: Integer. Index of out.
@param Ind_Value: Integer. Index of singleton
@thow Exception if any of the indexes is invalid
*/
double AOrbexDos::Get_Singleton_Value(int Out_Index, int Ind_Value){
	if (Out_Index < 0 || Out_Index >= GetNouts()) throw Error(8,"(Get_Singleton_Value)");
	if (Ind_Value < 0 || Ind_Value >= Get_Out_Nvalues(Out_Index)) throw Error(9,"(Get_Singleton_Value)");
        Singleton sing;
        outs[Out_Index].GetSingleton(&sing,Ind_Value);
	return sing.GetValue();
};

/*
Returns the value of a specefied Singleton. Overload method.
@param Name_out: String. Name  of out.
@param Name_singleton: String. Name  of singleton.
*/
double AOrbexDos::Get_Singleton_Value(char * Out_Name, char * Name_Value){
	return Get_Singleton_Value(Find_Out_Name(Out_Name)
		,Find_Singleton_Name(Find_Out_Name(Out_Name),Name_Value));
};

/*
Returns the value of a specefied Singleton. Overload method.
@param Name_out: String. Name  of out.
@param Ind_Value: Integer. Index of singleton
*/
double AOrbexDos::Get_Singleton_Value(char * Out_Name, int Ind_Value){
	return Get_Singleton_Value(Find_Out_Name(Out_Name),Ind_Value);
};


/*
Returns the value of a specefied Singleton. Overload method.
@param Ind_Out: Integer. Index of out.
@param Name_singleton: String. Name  of singleton.
*/
double AOrbexDos::Get_Singleton_Value(int Out_Index, char * Name_Value){
	return Get_Singleton_Value(Out_Index,Find_Singleton_Name(Out_Index,Name_Value));
};

/*
Set a specific Singleton�s name
@deprecated given an index of out array and an index of singleton array and
a new name. It changes the value of the singleton.
@param Ind_Out: Integer. Index of out.
@param Ind_Value: Integer. Index of singleton
@param New_Name: String. New name of the singleton.
@thow Exception if any of the indexes is invalid
*/
void AOrbexDos::Set_Singleton_Name(int Ind_Out, int Ind_Value, char * New_Name){
	if (Ind_Out < 0 || Ind_Out >= GetNouts()) throw Error(8,"(Set_Singleton_Name)");
	if (Ind_Value < 0 || Ind_Value >= Get_Out_Nvalues(Ind_Out)) throw Error(9,"(Set_Singleton_Name)");
    Singleton sing(New_Name,outs[Ind_Out].Get_SingletonValue(Ind_Value));
	Set_Singleton(Ind_Out,Ind_Value,&sing);
};

/*
Set a specific Singleton�s name. Overload method
@deprecated given an index of out array and an index of singleton array and
a new name. It changes the value of the singleton.
@param Name_out: String. Name  of out.
@param Name_value: String. Name  of singleton.
@param New_Name: String. New name of the singleton.
*/
void AOrbexDos::Set_Singleton_Name(char * Name_Out, char * Name_Value, char * name){
	Set_Singleton_Name(Find_Out_Name(Name_Out),
		Find_Singleton_Name(Find_Out_Name(Name_Out),Name_Value),name);
};


/*
Set a specific Singleton�s name. Overload method
@deprecated given an index of out array and an index of singleton array and
a new name. It changes the value of the singleton.
@param Ind_Out: Integer. Index of out.
@param Name_value: String. Name  of singleton.
@param New_Name: String. New name of the singleton.
*/
void AOrbexDos::Set_Singleton_Name(int Ind_Out, char * Name_Value, char * name){
	Set_Singleton_Name(Ind_Out, Find_Singleton_Name(Ind_Out,Name_Value), name);
};

/*
Set a specific Singleton�s name. Overload method
@deprecated given an index of out array and an index of singleton array and
a new name. It changes the value of the singleton.
@param Name_out: String. Name  of out.
@param Ind_Value: Integer. Index of singleton
@param New_Name: String. New name of the singleton.
*/
void AOrbexDos::Set_Singleton_Name(char * Name_Out, int Ind_Value, char * name){
	Set_Singleton_Name(Find_Out_Name(Name_Out), Ind_Value, name);
};

/*
Returns a specific Singleton�s name.
@deprecated given an index of out array and an index of singleton array and
a new name. It changes the value of the singleton.
@param Ind_Out: Integer. Index of out.
@param Ind_Value: Integer. Index of singleton
@thow Exception if any of the indexes is invalid
*/
void AOrbexDos::Get_Singleton_Name(char * ret,int Out_Index, int Ind_Value){
	if (Out_Index < 0 || Out_Index >= Nout) throw Error(8,"(Get_Singleton_Name)");
	if (Ind_Value < 0 || Ind_Value >=  Get_Out_Nvalues(Out_Index)) throw Error(9,"(Get_Singleton_Name)");
        outs[Out_Index].Get_SingletonName(ret,Ind_Value);
};

/*
Returns a specific Singleton�s name. Overload method
@deprecated given an index of out array and an index of singleton array and
a new name. It changes the value of the singleton.
@param Name_out: String. Name  of out.
@param Ind_Value: Integer. Index of singleton
*/
void AOrbexDos::Get_Singleton_Name(char * ret,char * Out_Name, int Ind_Value){
	Get_Singleton_Name(ret,Find_Out_Name(Out_Name), Ind_Value);
};

/*
Replaces all the values of the singletons in the specified out
@param Out_Index: Integer. Index of out.
@param new_values: Double array. New values of singletons.
@throws	Exception if the index is not valid
*/
void AOrbexDos::Replace_All_Singletons(int Out_Index, double * new_values){
	if (Out_Index < 0 || Out_Index >= Nout) throw Error(8,"(Replace_All_Singletons)");
	for(int i = 0; i <  Get_Out_Nvalues(Out_Index); i++)
		Set_Singleton_Value(Out_Index,i,new_values[i]);
};

/*
Replaces all the values of the singletons in the specified out. Overload method
@param Out_name: String. name of out.
@param new_values: Double array. New values of singletons.
@throws	Exception if the index is not valid
*/
void AOrbexDos::Replace_All_Singletons(char * Out_Name, double * new_values){
	Replace_All_Singletons(Find_Out_Name(Out_Name), new_values);
};


/*METHODS ABOUT RULES AND RULBASES*/
/*
Adds a new rulebase to the contoler
@param RB_Name: String. New name
@param RB_And: String. Type of AND to be used
@param RB_Or: String. Tyope of OR to be used
*/
void AOrbexDos::Add_RB(char * RB_Name, char * RB_And, char * RB_Or){
	RuleBase * Aux = new RuleBase[GetNrb()+1];
        Rule * aux_rules;
	for(int i = 0 ; i < Nrb ; i++){
                char nameAux[MAX_NAME_SIZE];
                RB[i].GetName(nameAux);
		Aux[i].SetName(nameAux);
		Aux[i].SetAnd(RB[i].GetAnd());
		Aux[i].SetOr(RB[i].GetOr());
                if(RB[i].GetNrules() > 0){
                        aux_rules = new Rule[RB[i].GetNrules()];
                        RB[i].GetRules(aux_rules);
                        Aux[i].SetRules(aux_rules,RB[i].GetNrules());
                        delete [] aux_rules;
                }else Aux[i].SetRules(aux_rules,0);
	}
	Aux[Nrb].SetName(RB_Name);
	Aux[Nrb].SetAnd(IsAnd(RB_And));
	Aux[Nrb].SetOr(IsOr(RB_Or));
        Aux[Nrb].SetRules(aux_rules,0);
	if( Nrb > 0) delete [] RB;
        Nrb++;
	RB = new RuleBase[Nrb];
	for(int i = 0 ; i < Nrb ; i++){
                char nameAux[MAX_NAME_SIZE];
                Aux[i].GetName(nameAux);
		RB[i].SetName(nameAux);
		RB[i].SetAnd(Aux[i].GetAnd());
		RB[i].SetOr(Aux[i].GetOr());
                if(Aux[i].GetNrules()){
                        aux_rules = new Rule[Aux[i].GetNrules()];
                        Aux[i].GetRules(aux_rules);
        		RB[i].SetRules(aux_rules,Aux[i].GetNrules());
                        delete [] aux_rules;
                }else RB[i].SetRules(aux_rules,0);
	}
	delete [] Aux;
};


/*
Returns the name of the a specific rule base
@param RB_Index: Integer. Rulebase position.
@throw exception if the index is invalid
*/
void AOrbexDos::Get_RB_Name(char * ret,int RB_Index){
	if (RB_Index < 0 || RB_Index >= GetNrb()) throw Error(19,"(Get_RB_Name)");
	RB[RB_Index].GetName(ret);
};

/*
Returns the number of rules of the a specific rule base
@param RB_Index: Integer. Rulebase position.
@throw exception if the index is invalid
*/
int AOrbexDos::Get_RB_Nrules(int RB_Index){
	if (RB_Index < 0 || RB_Index >= Nrb) throw Error(19,"(Get_RB_Nrules)");
	return RB[RB_Index].GetNrules();
};

/*
Returns the number of rules of the a specific rule base. Overload Method
@param RB_Name: String. Rulebase name.
*/
int AOrbexDos::Get_RB_Nrules(char * RB_Name){ return Get_RB_Nrules(Find_RB_Name(RB_Name));};

/*
Returns the type of AND used in a specific rule base
@param RB_Index: Integer. Rulebase position.
@throw exception if the index is invalid
*/
int AOrbexDos::Get_RB_And(int RB_Index){
	if (RB_Index < 0 || RB_Index >= Nrb) throw Error(19,"(Get_RB_And)");
	return RB[RB_Index].GetAnd();
};

/*
Returns the type of AND used in a specific rule base. Overload method
@param RB_Name: String. Rulebase name.
*/
int AOrbexDos::Get_RB_And(char * RB_Name){ return Get_RB_And(Find_RB_Name(RB_Name));};

/*
Returns the string of the type of AND used in a specific rule base.
@param RB_Index: Integer. Rulebase position.
@throw exception if the index is invalid
*/
const char * AOrbexDos::Get_RB_AndS(int rb_index){
	if (rb_index < 0 || rb_index >= Nrb) throw Error(19,"(Get_RB_AndS)");
	if( Get_RB_And(rb_index) == LIN) return "MIN";
	if( Get_RB_And(rb_index) == PRO) return "PRO";
        return "ERROR";
};

/*
Returns the string of type of AND used in a specific rule base
@param RB_Name: String. Rulebase name.
 */
const char * AOrbexDos::Get_RB_AndS(char * RB_Name){ return Get_RB_AndS(Find_RB_Name(RB_Name));};

/*
Sets the type of AND used in a specific rule base
@param RB_Index: Integer. Rulebase position.
@param conj: String. AND type.
@throw exception if the index is invalid
*/
void AOrbexDos::Set_RB_And(int RB_Index, char * conj){
	if (RB_Index < 0 || RB_Index >= GetNrb()) throw Error(19,"(Set_RB_And)");
	if (IsAnd(conj) == -1) throw Error(14,"(Set_RB_And)");
	RB[RB_Index].SetAnd(IsAnd(conj));
};

/*
Sets the type of AND used in a specific rule base. Overload method
@param RB_Name: String. Rulebase name.
@param conj: String. AND type.
*/
void AOrbexDos::Set_RB_And(char * RB_Name, char *  conj){ Set_RB_And(Find_RB_Name(RB_Name),conj);};

/*
Returns the type of OR used in a specific rule base
@param RB_Index: Integer. Rulebase position.
@throw exception if the index is invalid
*/
int AOrbexDos::Get_RB_Or(int RB_Index){
	if (RB_Index < 0 || RB_Index >= GetNrb()) throw Error(19,"(Get_RB_Or)");
	return RB[RB_Index].GetOr();
};

/*
Returns the type of OR used in a specific rule base
@param RB_Name: String. Rulebase name.
*/
int AOrbexDos::Get_RB_Or(char * RB_Name){ return Get_RB_Or(Find_RB_Name(RB_Name));};

/*
Returns the string of the type of OR used in a specific rule base
@param RB_Index: Integer. Rulebase position.
@throw exception if the index is invalid
*/
const char * AOrbexDos::Get_RB_OrS(int rb_index){
	if (rb_index < 0 || rb_index >= GetNrb()) throw Error(19,"(Get_RB_OrS)");
	if( Get_RB_Or(rb_index) == LAX) return "MAX";
	if( Get_RB_Or(rb_index) == SUM) return "SUM";
        return "ERROR";
}; 

/*
Returns the string of the type of AND used in a specific rule base
@param RB_Name: String. Rulebase name.
*/
const char * AOrbexDos::Get_RB_OrS(char * RB_Name){ return Get_RB_OrS(Find_RB_Name(RB_Name));};
 
/*
Sets the type of OR used in a specific rule base
@param RB_Index: Integer. Rulebase position.
@param disj: String. OR
@throw exception if the index is invalid
*/
void AOrbexDos::Set_RB_Or(int RB_Index, char * disj){
	if (RB_Index < 0 || RB_Index >= GetNrb()) throw Error(19,"(Set_RB_Or)");
	if (IsOr(disj) == -1) throw Error(14,"(Set_RB_Or)");
	RB[RB_Index].SetOr(IsOr(disj));
};

/*
Sets the type of OR used in a specific rule base
@param RB_Name: String. Rulebase name.
@param disj: String. OR
@throw exception if the index is invalid
*/
void AOrbexDos::Set_RB_Or(char * RB_Name, char * disj){ Set_RB_Or(Find_RB_Name(RB_Name),disj);};

/*
Adds a rule ti a rulebase froma a string.
@para, RB_Index: Integer.
@param new_rule: String. Of the rule
@param msg: String. Aditional message error.
*/
void AOrbexDos::Add_RB_Rule(int RB_Index, char * new_rule, const  char * msg){
        Rule rule;
        StringToRule(&rule,new_rule, msg);
	Add_RB_Rule(RB_Index,&rule );
};

/*
Adds a rule ti a rulebase froma a string. Overload method.
@para, RB_Name: String.
@param new_rule: String. Of the rule
@param msg: String. Aditional message error.
*/
void AOrbexDos::Add_RB_Rule(char * RB_Name, char * new_rule){
	Add_RB_Rule(Find_RB_Name(RB_Name),new_rule);
};

/*
Deletes a specific rule from a rulebase.
@param RB_Index: Integer.
@param Rule_Index: Integer
@throws exception for invalid indexes
*/
void AOrbexDos::Delete_RB_Rule(int RB_Index, int Rule_Index){
	if (RB_Index < 0 || RB_Index >= Nrb) throw Error(19,"(Delete_RB_Rule)");
	if (Rule_Index < 0 || Rule_Index >= Get_RB_Nrules(RB_Index)) throw Error(17,"(Delete_RB_Rule)");
	RB[RB_Index].DeleteRule(Rule_Index);
};

/*
Deletes a specific rule from a rulebase. Overload method
@param RB_Name: String.
@param Rule_Index: Integer
*/
void AOrbexDos::Delete_RB_Rule(char * RB_Name, int Rule_Index){
	Delete_RB_Rule(Find_RB_Name(RB_Name),Rule_Index);
};

/*
Returns the activation degree of a rule
@param RB_Index: Integer.
@param Rule_Index: Integer
@throws exception for invalid indexes
*/
double AOrbexDos::Get_RB_RuleDegree(int RB_Index, int Rule_Index){
	if (RB_Index < 0 || RB_Index >= Nrb) throw Error(19,"(Get_RB_RuleDegree)");
	if (Rule_Index < 0 || Rule_Index >= RB[RB_Index].GetNrules()) throw Error(17,"(Get_RB_RuleDegree)");
        Rule rule;
        RB[RB_Index].GetRule(&rule,Rule_Index);
        return rule.GetDegree();
};

/*
Returns the activation degree of a rule. Overload method.
@param RB_Name: String.
@param Rule_Index: Integer
*/
double AOrbexDos::Get_RB_RuleDegree(char * RB_Name, int Rule_Index){
	return Get_RB_RuleDegree(Find_RB_Name(RB_Name),Rule_Index);
};

/*Shows a Rule on Console*/
void AOrbexDos::ShowRule(int RB_Index, int Rule_Index){
        char nameAux[MAX_NAME_SIZE];
	cout << "IF ";
        Rule rule;
        RB[RB_Index].GetRule(&rule,Rule_Index);
	for(int j = 0; j < rule.GetNcond() ;j++){
                if (j > 0) cout << " " << GetOperatorString(rule.GetOp_condI(j-1));
		Condition con;
                rule.GetConditionI(&con,j);
                Get_Entry_Name(nameAux,con.in);
		cout << " " << nameAux<< " ";
		if (con.modifier != -1) cout << GetModifier(con.modifier) << " ";
                Get_Label_Name(nameAux,con.in,con.label);
		cout << nameAux;
	}
	cout << " THEN ";
	for(int j = 0; j < rule.GetNconseq() ;j++){
		if (j > 0) cout << " AND ";
		Consequence con;
                rule.GetConsequenceI(&con,j);
                Get_Out_Name(nameAux,con.out);
		cout << nameAux<< " ";
                Get_Singleton_Name(nameAux,con.out,con.singleton);
                cout << nameAux;
	}
	cout << "	� " << Get_RB_RuleDegree(RB_Index,Rule_Index)<< endl;
};

/*
Returns the activation degree of a rule
@param RB_Index: Integer.
@param Rule_Index: Integer
@param Degree: Double.
@throws exception for invalid indexes
*/
void AOrbexDos::Set_RB_RuleDegree(int RB_Index, int Rule_Index, double Degree){
	if (RB_Index < 0 || RB_Index >= GetNrb()) throw Error(19,"(Set_RB_RuleDegree)");
	if (Rule_Index < 0 || Rule_Index >= RB[RB_Index].GetNrules()) throw Error(17,"(Set_RB_RuleDegree)");
	Rule rule;
        RB[RB_Index].GetRule(&rule,Rule_Index);
	rule.SetDegree(Degree);
	Set_RB_Rule(RB_Index, Rule_Index, &rule);
};

/*
Returns the activation degree of a rule. Overload method.
@param RB_Name: String.
@param Rule_Index: Integer
@param Degree: Double.
*/
void AOrbexDos::Set_RB_RuleDegree(char * RB_Name, int Rule_Index, double Degree){
	Set_RB_RuleDegree(Find_RB_Name(RB_Name),Rule_Index, Degree);
};

/*
Adds a rule on a specific rulebase.
@param RB_Index: Integer. Rulebase position. 
@param rule: Rule. new Rule. 
@throw exception if any of the indexes is invalid
*/
void AOrbexDos::Add_RB_Rule(int RB_Index, Rule * rule){
	if (RB_Index < 0 || RB_Index >= GetNrb()) throw Error(19,"(Add_RB_Rule)");
        Condition * cond = NULL;
        Consequence * conseq = NULL;
        char * op = NULL;
        if((*rule).GetNcond()>0){
                cond = new Condition[(*rule).GetNcond()];
                (*rule).GetCondition(cond);
                if(((*rule).GetNcond())-1){
                        op = new char[((*rule).GetNcond())-1];
                        (*rule).GetOp_cond(op);
                }
        }
        if((*rule).GetNconseq()){
                conseq = new Consequence[(*rule).GetNconseq()];
                (*rule).GetConsequence(conseq);
        }
	CheckCondition(cond,(*rule).GetNcond());
	CheckOperator(op,(*rule).GetNcond()-1 );
	CheckConsequence(conseq,(*rule).GetNconseq());
	RB[RB_Index].AddRule(rule);
        if((*rule).GetNcond()>0){
                delete [] cond;
                if(((*rule).GetNcond())-1) delete [] op;
        }
        if((*rule).GetNconseq())delete [] conseq;

};

/*
Adds a rule on a specific rulebase. Overload method.
@param RB_Name: String. Rulebase name.
@param rule: Rule. new Rule.
@throw exception if any of the indexes is invalid
*/
void AOrbexDos::Add_RB_Rule(char * RB_Name, Rule* rule){Add_RB_Rule(Find_RB_Name(RB_Name),rule);};

/*
Sets a rule on a specific rulebase.
@param RB_Index: Integer. Rulebase position.
@param Rule_Index: Integer. Rule position.
@param rule: Rule. new Rule.
@throw exception if any of the indexes is invalid
*/
void AOrbexDos::Set_RB_Rule(int RB_Index, int Rule_Index, Rule * rule){
	if (RB_Index < 0 || RB_Index >= Nrb) throw Error(19,"(Set_RB_Rule)");
	if (Rule_Index < 0 || Rule_Index >= Get_RB_Nrules(RB_Index)) throw Error(17,"(Set_RB_Rule)");
	Condition * cond = NULL;
        Consequence * conseq = NULL;
        char * op = NULL;
        if((*rule).GetNcond()>0){
                cond = new Condition[(*rule).GetNcond()];
                (*rule).GetCondition(cond);
                if((((*rule).GetNcond())-1)>0){
                        op = new char[((*rule).GetNcond())-1];
                        (*rule).GetOp_cond(op);
                }
        }
        if((*rule).GetNconseq()>0){
                conseq = new Consequence[(*rule).GetNconseq()];
                (*rule).GetConsequence(conseq);
        }
	CheckCondition(cond,(*rule).GetNcond());
	CheckOperator(op,(*rule).GetNcond()-1 );
	CheckConsequence(conseq,(*rule).GetNconseq());
	RB[RB_Index].SetRule(Rule_Index,rule);
        if((*rule).GetNcond()>0){
                delete [] cond;
                if((((*rule).GetNcond())-1)>0) delete [] op;
        }
        if((*rule).GetNconseq()>0)delete [] conseq;

};

/*
Sets a rule on a specific rulebase. Overload method.
@param RB_name: string. Rulebase name
@param Rule_Index: Integer. Rule position.
@param rule: Rule. new Rule.
*/
void AOrbexDos::Set_RB_Rule(char * RB_Name,int Rule_Index, Rule * rule){
	Set_RB_Rule(Find_RB_Name(RB_Name),Rule_Index,rule);
};

/*
Convertes a string into a rule
@param srule: String. Of the rule
@param msg: String. Aditional message error.
@throw exception if the string is not using the proper format
*/
void AOrbexDos::StringToRule(Rule * rule,char * srule, const  char * msg){
	stringstream ss(srule);
	int l = 0;
	char * read = new char[MAX_SIZE];
	Condition aux_cond;
	char aux_op = 'N';
	char aux_op1 = 'N';
	Consequence aux_conseq;
	char * msge = new char[MAX_SIZE];
	ss >> read;
	l++;
	if(strcmp(read, "IF") != 0){ sprintf(msge, "%s.Token-%i-", msg, l); throw Error(24,msge);}
	do{
		aux_op = aux_op1;
		ss >> read; //Read in
		sprintf(msge, "%s.Token-%i-", msg, l++);
		aux_cond.in = Find_Entry_Name(read,msge);
		ss >> read; //Read Modifier or Label
		l++;
		if(IsModifier(read) == -1){
			aux_cond.modifier = -1;}
		else{
			aux_cond.modifier = IsModifier(read);
			ss >> read; //Read Label
			l++;
		}
		sprintf(msge, "%s.Token-%i-", msg, l);
		aux_cond.label = Find_Label_Name(aux_cond.in,read,msge);
		ss >> read; //Read Operator or Then
		l++;
		if(IsOperator(read) == AND)
			aux_op1 = AND;
		else if (IsOperator(read) == OR)
			aux_op1 = OR;
		else if(IsOperator(read) == 'N' && strcmp(read,"THEN") != 0) { sprintf(msge, "%s.Token-%i-", msg, l); throw Error(24,msge);}
		//INTRODUCE CONDITION
		(*rule).AddCondition(&aux_cond,aux_op);
	}while( strcmp(read,"THEN") != 0);
	(*rule).SetDegree(0);
	char * saux = new char[MAX_SIZE];
	//do{
		ss >> read; //Read out
		sprintf(msge, "%s.Token-%i-", msg, l++);
		aux_conseq.out = Find_Out_Name(read,msge);
		ss >> read; //Read singleton
		sprintf(msge, "%s.Token-%i-", msg, l++);
		strcpy(saux,read);
		aux_conseq.singleton= Find_Singleton_Name(aux_conseq.out,read, msge);
		ss >> read; //Read operator 
		l++;
		if (IsOperator(read) == OR) { sprintf(msge, "%s.Token-%i-", msg, l); throw Error(24,msge);}
		else if(IsOperator(read) == 'N' && strcmp(read,saux) != 0 )
		{sprintf(msge, "%s.Token-%i-", msg, l);} //throw Error(24,msge);
		
		(*rule).AddConsequence(&aux_conseq);
	//}while(strcmp(read,saux) != 0);
	delete [] saux;
	delete [] read;
	delete [] msge;
};

/*
Checks if a complete condition has the right entries and labels
*/
void AOrbexDos::CheckCondition(Condition * Cond, int size){
	for(int i = 0; i < size ; i++){
		if (Cond[i].in < 0 || Cond[i].in >= Nentries) throw Error(22,"(WRONG ENTRY)");
		if (Cond[i].label < 0 || Cond[i].label >= Get_Entry_Nlabels(Cond[i].in))
		        throw Error(22,"(WRONG LABEL)");
		GetModifier(Cond[i].modifier);
        }
};

/*
Checks if a complete consequence has the right outs and singletons
*/
void AOrbexDos::CheckConsequence(Consequence * Conseq, int size){
	for(int i = 0; i < size ; i++){
		if (Conseq[i].out < 0 || Conseq[i].out >= GetNouts()){
			throw Error(23,"(WRONG OUT)");
		}
	if (Conseq[i].singleton < 0 || Conseq[i].singleton >= Get_Out_Nvalues(Conseq[i].out))
		throw Error(23,"(WRONG SINGLETON)");
	}		
};

/*
Checks if a complete consequence has the right operator
*/
void AOrbexDos::CheckOperator(char * op, int size){
	for(int i = 0; i < size ; i++)
		if (op[i] != AND && op[i] != OR) throw Error(14, " IN CONDITION ");	
};

/*IMPORTANT METHODS*/

/*
Show controler on console
*/
void AOrbexDos::Show(){
	cout << "-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-FUZZY:" << endl;
	cout << "IN" << endl;
	for(int i = 0; i < Nentries; i++){
                Var_Entry entry;
                Get_Entry(&entry,i);
                entry.Show();
        }
	cout << "NI\n" << endl;
	cout << "OUT" << endl;
	for(int i = 0; i < Nout; i++){
                Var_Out out;
                Get_Out(&out,i);
                out.Show();
        }
	cout << "TUO\n" << endl;
	cout << "RULES" << endl;
	for(int k = 0; k < GetNrb() ; k++){
                char nameAux[MAX_NAME_SIZE];
                Get_RB_Name(nameAux,k);
		//OJO cout << nameAux << " "<< Get_RB_AndS(k) << " " << Get_RB_OrS(k) << " {"<<endl;
		for(int i = 0; i < Get_RB_Nrules(k); i++){
			cout << "	";
			ShowRule(k,i);
		}
		cout << "	}\n" << endl;
	}
	cout << "SELUR" << endl;
};

/*
Write controler in file
@param file_out: String. Filename.
*/
void AOrbexDos::WriteOnFile(const char * file_out){
	ofstream f(file_out);
        char auxName[MAX_NAME_SIZE];
	if(Nentries > 0){
		f << "IN" << endl;
		for(int i = 0; i < GetNentries(); i++){
                        Get_Entry_Name(auxName,i);
			f<< "	" << auxName << " " <<  Get_Entry_RgeInf(i) << " " <<
			Get_Entry_RgeSup(i) << " {" << endl;
			for(int j = 0; j < Get_Entry_Nlabels(i);j++){
                                Get_Label_Name(auxName,i,j);
				f << "		"<< auxName;
				int size;
				switch(Get_Label_Type(i,j)){
					case TRI: size = 3; f << " TRI "; break;
					case TRA: size = 4; f << " TRA "; break;
					case GSS: size = 2; f << " GSS "; break;
					case BLL: size = 3; f << " BLL "; break;
					case FPI: size = 4; f << " PI "; break;
				}
                                double * v = new double[size];
                                Get_Label_Values(v,i,j);
				for(int x = 0; x < size ; x ++)
					f << v[x] << " ";
				f << endl;
                                delete [] v;
			}
			f << "	}\n" << endl;
		}
		f << "NI\n" << endl;
	}
	if(Nout > 0){
		f << "OUT" << endl;
		for(int i = 0; i < Nout; i++){
                        Get_Out_Name(auxName,i);
			f<< "	" << auxName << " {" << endl;
			for(int j = 0; j < Get_Out_Nvalues(i); j++){
                                Get_Singleton_Name(auxName,i,j);
				f << "		"<<  auxName<< " " << Get_Singleton_Value(i,j) << endl;
                        }
			f << "	}\n" << endl;
		}
		f << "TUO\n" << endl;
	}
	if(Nrb > 0){
		f << "RULES" << endl;
		for(int k = 0; k < GetNrb() ; k++){
                        Get_RB_Name(auxName,k);
			f << auxName;
                        f << " "<< Get_RB_AndS(k) << " " << Get_RB_OrS(k) << " {"<<endl;
			for(int i = 0; i < Get_RB_Nrules(k); i++){
				f << "IF ";
                                Rule r;
                                RB[k].GetRule(&r,i);
				for(int j = 0; j < r.GetNcond() ;j++){
					if (j > 0) f << " " << GetOperatorString(r.GetOp_condI(j-1));
					Condition con;
                                        r.GetConditionI(&con,j);
                                        Get_Entry_Name(auxName,con.in);
					f << " " << auxName<< " ";
					if (con.modifier != -1) f << GetModifier(con.modifier) << " ";
                                        Get_Label_Name(auxName,con.in,con.label);
                                        f << auxName;
				}
				f << " THEN ";
				for(int j = 0; j < r.GetNconseq() ;j++){
					if (j > 0) f << " AND ";
					Consequence con;
                                        r.GetConsequenceI(&con,j);
                                        Get_Out_Name(auxName,con.out);
					f << auxName;
                                        Get_Singleton_Name(auxName,con.out,con.singleton);
                                        f<< " " << auxName;
				}
				f << endl;
			}
			f << "}\n" << endl;
		}
		f << "SELUR" << endl;

	}
	f.close();
};

/*
Finds the degree of membership for each value of entry on each label.
@deprecated Uses the correspondent membership function for each label type.
*/
void AOrbexDos::Fuzzification(){

	for(int i =0 ; i < Nentries; i++) {
		for(int j = 0 ; j < Get_Entry_Nlabels(i) ; j++){
                        double v[4];
                        Get_Label_Values(v,i,j);
			switch(Get_Label_Type(i,j)){
				case TRI:
					Set_Label_Degree(i, j,Triangle_Membership(In_Values[i],v));
					break;
				case TRA: 
					Set_Label_Degree(i, j, 
					Trapezium_Membership(In_Values[i],v));
					break;
				case GSS: 
					Set_Label_Degree(i, j, 
					Gauss_Membership(In_Values[i],v));
					break;
				case BLL: 
					Set_Label_Degree(i, j, 
					Bell_Membership(In_Values[i],v));
					break;
				case FPI: 
					Set_Label_Degree(i, j,
					PI_Membership(In_Values[i],v));
					break;
				default: throw Error(4); //Should never happen
			}	
		}
	}
};

/*
Evaluates the degree acording to the modifier
@deprecated if a rule has a modifier before a label then the degree of membership
to be used is different than the one saved.
@param Modifier: Int. Representes the number of posible modifier.
@param Ind_Entry, Ind_Label: Int. Entry and label index
@param In_Value: Int. Value to be evaluated by the controler.
@return double: The degree modified by the modifier.
@throws Exception if there is an invalid index or modifier
*/
double AOrbexDos::Modifier_Evaluation(int Modifier, int Ind_Entry, int Ind_Label, double In_Value){
	if (Ind_Entry < 0 || Ind_Entry >= GetNentries()) throw Error(2,"(Modifier_Evaluation)");
	if (Ind_Label < 0 || Ind_Label >= Get_Entry_Nlabels(Ind_Entry))
		throw Error(16,"(Modifier_Evaluation)");
	//Get degree
	double Degree = Get_Label_Degree(Ind_Entry,Ind_Label);
	//Get reference points
	double c;
	double c1;
        double v[4];
        Get_Label_Values(v,Ind_Entry,Ind_Label);
	switch (Get_Label_Type(Ind_Entry,Ind_Label)){
		case TRI:c = v[1]; c1 = c; break;
		case TRA:c = v[1]; c1 = v[2]; break;
		case GSS:c = v[0]; c1 = v[0]; break;
		case BLL:c = v[0]; c1 = v[0]; break;
		case FPI:c = v[1]; c1 = v[2]; break;
		default: throw Error(4);//Should never happen
	}
	switch (Modifier){
		case NO: return 1 - Degree;
		case FEW: return pow(Degree,0.5);
		case VERY: return pow(Degree,2);
		case EXTRA: return pow(Degree,3);
		case LESSTHAN: return In_Value < c ? 1-Degree: 0;
		case GREATERTHAN: return In_Value < c1 ? 0: 1-Degree;
		case LESSOREQUALTHAN: return In_Value < c1 ? 1: Degree;
		case GREATEROREQUALTHAN: return In_Value < c ? Degree: 1;
		case -1:  return Degree;
		default: throw Error(15);
	}
};

/*
Evaluates all the rules. Asings to each rule its evaluated Degree.
@depracated Iterates on the RuleBase, reasoning each rule, using T-Norme and S-Norme
and evaluating Modifiers if they exist.
*/
void AOrbexDos::Reasoning(){
	double auxiliar;
	for(int i =0 ; i < Nrb; i++){
		for(int j =0 ; j < Get_RB_Nrules(i); j++){
                        Rule rule;
                        RB[i].GetRule(&rule,j);
                        Condition cond;
			for(int k =0 ; k < rule.GetNcond(); k++){
                                rule.GetConditionI(&cond,k);
				auxiliar =Modifier_Evaluation(cond.modifier,cond.in,
                                                cond.label,GetInValue(cond.in));
				if( k == 0)	Set_RB_RuleDegree(i,j,auxiliar);
				else if (rule.GetOp_condI(k-1) == 'A')
					Set_RB_RuleDegree(i,j,T_Norma(auxiliar,Get_RB_RuleDegree(i,j), i));
				else if (rule.GetOp_condI(k-1) == 'O')
					Set_RB_RuleDegree(i,j,S_Norma(auxiliar,Get_RB_RuleDegree(i,j), i));
			}
		}
	}
};

/*
Defuzzificates each rule to obtain the Out_Values of each Var_Out in the controler
@deprecated Iterates in all the RuleBase and finds wich Var_Out correponds to each
rule Degree. It calculates the weithed average of each Var_Out and saves it in the
Out_Values of the controler.
*/
void AOrbexDos::Defuzzification(){
	double *adding = new double[Nout];
	double *degrees= new double[Nout];
	int index_aux;
	for(int i = 0; i < Nout ; i++){
		adding[i] = 0; degrees[i] = 0; 	Out_Values[i] = 0;
        }
	for(int i = 0 ; i < Nrb; i++){
		for(int j = 0; j < Get_RB_Nrules(i); j++){
                        Rule rule;
                        RB[i].GetRule(&rule,j);
			for(int k = 0; k < rule.GetNconseq() ; k++){
                                Consequence con;
                                rule.GetConsequenceI(&con,k);
				index_aux = con.out;
				adding[index_aux] +=
					(Get_Singleton_Value(index_aux,con.singleton)*
					Get_RB_RuleDegree(i,j));

				degrees[index_aux] += Get_RB_RuleDegree(i,j);
			}
		}
	}
	for(int i = 0; i < GetNouts() ; i++){
		if(degrees[i]!= 0) Out_Values[i] = adding[i]/degrees[i];
	}
	delete [] adding;
	delete [] degrees;
};

/*Sets all degrees in Zero*/
void AOrbexDos::Reset(){
	for(int i = 0; i < Nentries; i++){
		In_Values[i] = 0;
		for(int j = 0 ; j < Get_Entry_Nlabels(i) ; j++)
			Set_Label_Degree(i,j,0);
	}
	
	for(int i = 0; i < Nrb; i++){
		for(int j = 0 ; j < Get_RB_Nrules(i) ; j++)
			Set_RB_RuleDegree(i,j,0);
	}
	
	for(int i = 0; i < GetNouts() ; i++)
		Out_Values[i] = 0;	
};

/*
MAIN FUCTION
USES THE FUZZY CONTROLER TO FIND THE OUT_VALUES USING DE IN_VALUES
3 STEPS: FUZZIFICATION-REASONING-DEFZZIFICATION
*/
void AOrbexDos::Evaluate(double * in_value){
	SetInValues(in_value);
	Fuzzification();
	Reasoning();
	Defuzzification();
};

void AOrbexDos::Evaluate(){	Evaluate(In_Values);};

/*
Shows Out_Values on console
@deprecated if the parameter onfile is used, the Out_Values are writen also on
a file with that name.
@param string_print: String. To be printed.
@param onfile: Integer. "Bolean" to determinate if the Out_vAlues should also
be writen on a file
*/
void AOrbexDos::ShowOutValues(const char * string_print, int onfile){
	if (strcmp(string_print,"EMPTY") != 0) cout << string_print << endl;
        char auxName[MAX_NAME_SIZE];
	if(onfile) { //Write also on a file
		ofstream f(string_print);
		for(int i = 0; i < Nout; i++){
                        Get_Out_Name(auxName,i);
			f <<  auxName << " = " << GetOutValue(i) << endl;
			cout <<   auxName << " = " << GetOutValue(i) << endl;
		}
		f.close();
	}else //Only print in console
		for(int i = 0; i < Nout ; i++){
                        Get_Out_Name(auxName,i);
			cout <<   auxName << " = " << GetOutValue(i) << endl;
		}
};

/*
Finds the entry index given the name of the entry
@param Name: String. Name of the entry.
@param msg: String. Aditional error message
@return int: The entry index.
@throws Exception if the name does not match any entry name.
*/
int AOrbexDos::Find_Entry_Name(const char * Name, const char * msg){
	int i = 0;
	char * msge = new char[MAX_NAME_SIZE];
        char auxName[MAX_NAME_SIZE];
	sprintf(msge,"%s%s",msg,"(Find_Entry_Name)");
	for(i = 0; i < GetNentries() ; i++){
                Get_Entry_Name(auxName,i);
		if( strcmp(Name,auxName) == 0) break;
        }
	if (i == Nentries) throw Error(5,msge);
	delete [] msge;
	return i;
};

/*
Finds the label index given the name of the label
@param Ind_Entry: Integer. Entry index
@param Name: String. Name of the label
@param msg: String. Aditional error message
@return int: The label index.
@throws Exception if the name does not match any label name.
*/
int AOrbexDos::Find_Label_Name(int Ind_Entry, const char * Name, const  char * msg){
	int i = 0;
	char  * msge = new char[MAX_NAME_SIZE];
        char auxName[MAX_NAME_SIZE];
	sprintf(msge,"%s%s",msg,"(Find_Label_Name)");
	for(i = 0; i < Get_Entry_Nlabels(Ind_Entry); i++){
                Get_Label_Name(auxName,Ind_Entry,i);
		if( strcmp(Name,auxName) == 0) break;
        }
	if (i == Get_Entry_Nlabels(Ind_Entry)) throw Error(5,msge);
	delete [] msge;
	return i;
};

/*
Finds the OUT index given the name of the OUT
@param Name: String. Name of the OUT
@param msg: String. Aditional error message
@return int: The OUT index.
@throws Exception if the name does not match any OUT name.
*/
int AOrbexDos::Find_Out_Name(char * Name, const  char * msg){
	int i = 0;
	char  * msge = new char[MAX_SIZE];
        char auxName[MAX_NAME_SIZE];
	sprintf(msge,"%s%s",msg,"(Find_Out_Name)");
	for(i = 0; i < Nout ; i++){
                Get_Out_Name(auxName,i);
		if( strcmp(Name,auxName) == 0) break;
        }

	if (i == Nout) throw Error(5,msge);
	delete [] msge;
	return i;
};

/*
Finds the Singleton index given the name of the Singleton
@param Ind_Out: Integer.
@param Name: String. Name of the Singleton
@param msg: String. Aditional error message
@return int: The Singleton index.
@throws Exception if the name does not match any Singleton name.
*/
int AOrbexDos::Find_Singleton_Name(int Ind_Out,char * Name, const  char * msg){
	int i = 0;
	char  * msge = new char[MAX_SIZE];
        char auxName[MAX_NAME_SIZE];
	sprintf(msge,"%s%s",msg,"(Find_Singleton_Name)");
	for(i = 0; i < Get_Out_Nvalues(Ind_Out) ; i++){
                Get_Singleton_Name(auxName,Ind_Out,i);
		if( strcmp(Name,auxName) == 0) break;
        }
	if (i == Get_Out_Nvalues(Ind_Out)) throw Error(5,msge);
	delete [] msge;
	return i;
};

/*
Finds the Rulebase index given the name of the Rulebase
@param Name: String. Name of the Rulebase
@param msg: String. Aditional error message
@return int: The Singleton index.
@throws Exception if the name does not match any Rulebase name.
*/
int AOrbexDos::Find_RB_Name(char * Name){
	int i = 0;
        char auxName[MAX_NAME_SIZE];
	for(i = 0; i < Nrb; i++){
                Get_RB_Name(auxName,i);
		if( strcmp(Name,auxName) == 0) break;
        }
	if (i == Nrb) throw Error(5,"(Find_RB_Name)");
	return i;
};

/*
Deletes de rule in the rule base asociated with the Label
@param Entry_Index: Integer. Index of the entry.
@param Label_Index: Integer. Index of the label.
*/
void AOrbexDos::Delete_Rule_Asociatedwith_Label(int Entry_Index, int Label_Index){
	int i = 0;
	for(int k = 0; k < Nrb; k++){
		i = 0;
		while( i != Get_RB_Nrules(k)){
			Rule rule;
            RB[k].GetRule(&rule,i);
            int * isLabelInRule = new int[rule.GetNcond()];
            rule.Find_Label_In_Rule(isLabelInRule,Entry_Index, Label_Index);
			if (isLabelInRule[0] == TRUE)
				Delete_RB_Rule(k,i);
			else{
				for(int j = 0; j < Get_Entry_Nlabels(Entry_Index); j++){
                                        if(j != Label_Index){
					        rule.Find_Label_In_Rule(isLabelInRule,Entry_Index,j,TRUE);
					        for( int h = 1; h < rule.GetNcond() ; h++){
						        if( isLabelInRule[h] == TRUE){
                                                                int size = rule.GetNcond();
							        Condition * condition = new Condition[size];
                                                                rule.GetCondition(condition);
                                                                char * Op = new char[size-1];
                                                                rule.GetOp_cond(Op);
							        condition[h].label = j-1;
							        rule.SetCondition(condition,Op,size);
							        RB[k].SetRule(i,&rule);
                                                                delete [] condition;
                                                                delete [] Op;
						        }
					        }
				        }
				}
			}
            i++;
			delete [] isLabelInRule;
		}
	}
};

/*
Deletes de rule in the rule base asociated with the entry.
@param Entry_Index: Integer. Index of the entry.
*/
void AOrbexDos::Delete_Rule_Asociatedwith_Entry(int Entry_Index){
        int i = 0;
	for(int k = 0; k < Nrb; k++){
		i = 0;
		while( i != Get_RB_Nrules(k)){
			Rule rule;
                        RB[k].GetRule(&rule,i);
                        int * isLabelInRule = new int[rule.GetNcond()];
                        rule.Find_Entry_In_Rule(isLabelInRule,Entry_Index);
                        if (isLabelInRule[0] == TRUE)
				Delete_RB_Rule(k,i);
 	                else{
				for(int j = 0; j < Nentries; j++){
                                        if(j != Entry_Index){
					        rule.Find_Entry_In_Rule(isLabelInRule,j,TRUE);
                                                for( int h = 1; h < rule.GetNcond() ; h++){
                                                        if( isLabelInRule[h] == TRUE){
							        int size = rule.GetNcond();
							        Condition * condition = new Condition[size];
								rule.GetCondition(condition);
                                                                char * Op = new char[size-1];
                                                                rule.GetOp_cond(Op);
                                                                condition[h].in = j-1;
							        rule.SetCondition(condition,Op,size);
                                                                RB[k].SetRule(i,&rule);
                                                                delete [] condition;
                                                                delete [] Op;
				                        }
						}
				        }
				}
			}
			i++;
			delete [] isLabelInRule;
		}
	}
};

/*
Deletes de rule in the rule base asociated with the Singleton
@param Out_Index: Integer. Index of the Out.
@param Singleton_Index: Integer. Index of the Singleton.
*/
void AOrbexDos::Delete_Rule_Asociatedwith_Singleton(int Out_Index, int Singleton_Index){
        int i = 0;
	for(int k = 0; k < Nrb; k++){
		i = 0;
		while( i != Get_RB_Nrules(k)){
			Rule rule;
                        RB[k].GetRule(&rule,i);
                        int * isLabelInRule = new int[rule.GetNconseq()];
                        rule.Find_Singleton_In_Rule(isLabelInRule,Out_Index, Singleton_Index);
		if (isLabelInRule[0] == TRUE)
		        Delete_RB_Rule(k,i);
                else{
                  for(int j = 0; j < Get_Out_Nvalues(Out_Index); j++){
        		        if(j != Singleton_Index){
        			        rule.Find_Singleton_In_Rule(isLabelInRule,Out_Index,j,TRUE);
        				for( int h = 1; h < rule.GetNconseq() ; h++){
        				        if( isLabelInRule[h] == TRUE){
                                                        int size = rule.GetNconseq();
        						Consequence * consequence = new Consequence[size];
                                                        rule.GetConsequence(consequence);
        		        			consequence[h].singleton = j-1;
        						rule.SetConsequence(consequence,size);
        						RB[k].SetRule(i,&rule);
                                                        delete [] consequence;
                                                }
                                        }
                                }
                        }
                    }
        	    i++;
        	    delete [] isLabelInRule;
                }
        }
};

/*
Deletes de rule in the rule base asociated with the Out.
@param Out_Index: Integer. Index of the Out.
*/
void AOrbexDos::Delete_Rule_Asociatedwith_Out(int Out_Index){
        int i = 0;
        for(int k = 0; k < Nrb; k++){
                i = 0;
                while( i != Get_RB_Nrules(k)){
	                Rule rule;
                        RB[k].GetRule(&rule,i);
                        int * isLabelInRule = new int[rule.GetNconseq()];
                        rule.Find_Out_In_Rule(isLabelInRule,Out_Index);
                        if (isLabelInRule[0] == TRUE){
                                Delete_RB_Rule(k,i);
                        }else{
                                for(int j = 0; j < Nout; j++){
                                        if(j != Out_Index){
                                                rule.Find_Out_In_Rule(isLabelInRule,j,TRUE);
        			                for( int h = 1; h < rule.GetNconseq() ; h++){
                			                if( isLabelInRule[h] == TRUE){
                				                int size = rule.GetNconseq();
                	        				Consequence * consequence = new Consequence[size];
        	        	        			rule.GetConsequence(consequence);
                			        		consequence[h].out = j-1;
                				        	rule.SetConsequence(consequence,size);
                				        	RB[k].SetRule(i,&rule);
                					        delete [] consequence;
                                                        }
                                                }
                                        }
                                }
                        }
        	        i++;
                        delete [] isLabelInRule;
                }
        }
};

/*
Used for conjuction in rules
@param x,y: Decimal. Representes the degree of membership.
@return double: The result of the operation
*/
double AOrbexDos::T_Norma( double x, double y, int rb_index ){
	if (rb_index < 0 || rb_index >= GetNrb()) throw Error(19,"(T_Norma)");
	switch (Get_RB_And(rb_index)){
		case LIN: return std::min(x,y);
		case PRO: return x*y;
	}
        return -INFINITO;

};

/*
Used for disjuction in rules
@param x,y: Decimal. Representes the degree of membership.
@return double: The result of the operation
*/
double AOrbexDos::S_Norma( double x, double y, int rb_index ){
	if (rb_index < 0 || rb_index >= GetNrb()) throw Error(19,"(T_Norma)");
	switch (Get_RB_Or(rb_index)){
		case LAX: return max(x,y);
		case SUM: return x+y-(x*y);
	}
        return -INFINITO;
};

/************************DRAW METHODS (MATLAB)*********************************/
void AOrbexDos::CreateMesh(int values, char* in0, char* in1, char* out, const  char * filename ){
	CreateMesh(values, Find_Entry_Name(in0), Find_Entry_Name(in1), Find_Out_Name(out), filename);
};
void AOrbexDos::CreateMesh(int values, int in0, int in1, int out, const  char * filename){
	if (in0 < 0 || in0 >= GetNentries()) throw Error(2,"(CreateMesh)");
	if (in1 < 0 || in1 >= GetNentries()) throw Error(2,"(CreateMesh)");
	if (out < 0 || out >= GetNouts()) throw Error(8,"(CreateMesh)");
	if (values < 0) throw Error(0,"(CreateMesh)");
	
	
	double rateE = (Get_Entry_RgeSup(in0) - Get_Entry_RgeInf(in0))/values;
	double rateD = (Get_Entry_RgeSup(in1) - Get_Entry_RgeInf(in1))/values;
	
	ofstream filem(filename);
	int sizeE = values + 1;
	int sizeD = values + 1;
	
	double *e= new double[sizeE];
	double *d= new double[sizeD];
	char nameAux[MAX_NAME_SIZE];
cout << "Contar hasta:  " << sizeE-1 << endl;
	e[0] = Get_Entry_RgeInf(in0);
	d[0] = Get_Entry_RgeInf(in1);
	filem << "clc,close all;\n f=figure;\n" << endl; 
	
	filem << "p = [" << endl;
	for(int i = 0; i < sizeE ; i ++){
		if (i!= 0) e[i] = min((e[i-1]+rateE), Get_Entry_RgeSup(in0));
		for(int j = 0; j < sizeD ; j ++){
			if (j != 0)d[j] = min((d[j-1]+rateD),Get_Entry_RgeSup(in1));
			Reset();
			SetInValue(in0,e[i]);
			SetInValue(in1,d[j]);
			Evaluate();
			filem << " " <<GetOutValue(out);
		}
		cout << i <<": ";
		filem << " ;" << endl;	
	}
	filem << " ];" << endl;
	filem << "p(p>1) = 1;\n p(p<-1) = -1;" << endl;
	filem << "e= "<< e[0]<< ":"<<rateE<<":"<<e[sizeE-1]<<";" << endl;
	filem << "d= "<< d[0]<< ":"<<rateD<<":"<<d[sizeD-1]<<";" << endl;	
	filem << "[D, E] = meshgrid(d,e);" << endl;
	filem << "surf(E,D,p);" << endl;
	filem << "title('ControlerBehavior','fontsize',14);" << endl;
	Get_Entry_Name(nameAux,in0);
	filem << "xlabel('"<<nameAux<<"','fontsize',12);" << endl;
	Get_Entry_Name(nameAux,in1);
	filem << "ylabel('"<<nameAux<<"','fontsize',12);" << endl;
	Get_Out_Name(nameAux,out);
	filem << "zlabel('"<<nameAux<<"','fontsize',12);" << endl;
	filem << "set(gca,'FontSize',11);\n";
	filem << "saveas(f,'"<< filename  <<".png');" << endl;
	filem << "clear;" << endl; 
	filem.close();
	delete []e;
	delete []d;
cout << "\nGrid Drawed" << endl;
};

void AOrbexDos::Draw_Entry_Labels(const char* Entry_Name){Draw_Entry_Labels(Find_Entry_Name(Entry_Name));};

void AOrbexDos::Draw_Entry_Labels(int Entry_Index){
	if (Entry_Index < 0 || Entry_Index >= GetNentries()) throw Error(2,"(Draw_Entry_Labels)");
	char filename[MAX_NAME_SIZE];
	char nameAux[MAX_NAME_SIZE];
	Get_Entry_Name(filename,Entry_Index);
	strcat(filename,".m");
	ofstream f(filename);
	f << "clc,close all" << endl;
	f << "r0 = " << Get_Entry_RgeInf(Entry_Index)<< ";" << endl;
	f << "r1 = " << Get_Entry_RgeSup(Entry_Index)<< ";" << endl;
	f << "x = r0:0.002:r1;\n color = 'gbcmrkygbcmrkygbcmrkygbcmrky';\n f = figure;\n hold on;\n";
	f << "ylim([0 1.1]);\n xlim([r0 r1]);\n grid on,axis on;\n";
	f << "ylabel('Grado De Pertenencia');\n xlabel('Valor De Entrada');\n";
	f << "set(gca,'FontSize',11);\n";
	for(int i = 0; i < Get_Entry_Nlabels(Entry_Index) ; i ++){
		f << "plot(x,";
		double v[4];
		Get_Label_Values(v,Entry_Index,i);
		double c; 
		switch(Get_Label_Type(Entry_Index,i)){
			case TRI: f << "trimf(x,["<<v[0]<<" "<<v[1]<<" "<<v[2]<< "])"; c = v[1] ; break;
			case TRA: f << "trapmf(x,["<<v[0]<<" "<<v[1]<<" "<<v[2]<<" "<<v[3]<< "])";c = (v[1]+v[2])/2 ; break;
			case GSS: f << "gaussmf(x,["<<v[1]<<" "<<v[0]<<"])"; c = v[0] ;break;
			case BLL: f << "gbellmf(x,["<<v[1]<<" "<<v[2]<<" "<<v[0]<< "])"; c = v[0] ; break;
			case FPI: f << "pimf(x,["<<v[0]<<" "<<v[1]<<" "<<v[2]<<" "<<v[3]<< "])"; c = (v[1]+v[2])/2 ;break;
			
		}
		f << ",['-',color("<<i+1<<") ],'LineWidth',2);"<<endl;
		Get_Label_Name(nameAux,Entry_Index,i);
		f << "text("<<c<<",1.04,['"<<nameAux<<"'],'HorizontalAlignment','center');" << endl;
	}
	Get_Entry_Name(nameAux,Entry_Index);
	f << "title('"<< nameAux <<"' );" << endl;
	f << "saveas(f,'"<< nameAux<<".png');" << endl;
	f << "clear;" << endl; 
	f.close(); 
cout << "Draw Labels" << endl;
};
/*****************AUXILIAR METHODS*****************************/
/*
Copies a double array
@deprecated	Auxiliar Funtion.
@param array: Double Array. To duplicate.
@param size: int. Size of array.
@return array_copy: Arry of double. Copy of the entry.
*/
void Copy_Double_Array(double * a_copy,double * array, int size){
	for(int i = 0; i < size; i++){
		a_copy[i] = array[i];
	}
};

/*
Copies a label array
@deprecated	Auxiliar Funtion.
@param array: Label Array. To duplicate.
@param size: int. Size of array.
@return array_copy: Arry of labels. Copy of the entry.
@see Add_Label, Duplicate_Label
*/
void Copy_Array_Label(Label * array_copy,Label * array, int size){
	for(int i = 0 ; i < size ; i++) {
		array_copy[i].Equal(&array[i]);
	}
};

/*
Duplicates an existent array of entries.
@deprecated: Given an array of entries it copies it and returns it.
@param: Original: Var_Entry. The entry we want to duplicate.
@param size: Integer. Size of the array.
@return: Var_Entry. Duplicated entry.
@see Duplicate_Entry
*/
void Copy_Array_Entry(Var_Entry * array_copy,Var_Entry * array, int size){
	for( int i = 0 ; i < size ; i++)
		array_copy[i].Equal(&array[i]);
};

/*
Copies a singleton�s array
@deprecated	Auxiliar Funtion.
@param array: Singleton Array. To duplicate.
@param size: int. Size of array.
@return array_copy: Arry of singleton. Copy of the entry.
@see Add_Out_Value, Duplicate_Singleton
*/
void Copy_Array_Singleton(Singleton * array_copy,Singleton * array, int size){
	for(int i = 0 ; i < size ; i++)
                array_copy[i].Equal(&array[i]);
};

/*
Duplicates an existent array of out.
@deprecated: Given an array of out it copies it and returns it.
@param: Original: Var_Out. The out we want to duplicate.
@param size: Integer. Size of the array.
@return: array_copy. Duplicated array.
@see Duplicate_Out
*/
void Copy_Array_Out(Var_Out * array_copy, Var_Out * array, int size){
	for( int i = 0 ; i < size ; i++)
                array_copy[i].Equal(&array[i]);
};

/*
Duplicates a condition array
@param array: Condition array.Array to be copied.
@param size: Int. Size of array.
@return Rule: copied array.
@throws exception if the size is invalid
*/
void Copy_Array_Cond(Condition * array_copy,Condition * array,int size){
	if( size < 0) throw Error(0,"(Copy_Array_Cond)");
	for(int i = 0; i < size ; i++) array_copy[i].Equal(&array[i]);
};

/*
Duplicates a consequence array
@param array: consequence array.Array to be copied.
@param size: Int. Size of array.
@return Rule: copied array.
@throws exception if the size is invalid
*/
void Copy_Array_Conseq(Consequence * array_copy,Consequence * array,int size){
	if( size < 0) throw Error(0,"(Copy_Array_Conseq)");
	for(int i = 0; i < size ; i++) array_copy[i].Equal(&array[i]);
};

/*
Duplicates an operator array
@param array: operator array.Array to be copied.
@param size: Int. Size of array.
@return Rule: copied array.
@throws exception if the size is invalid
*/
void Copy_Array_Op(char * array_copy,char * array,int size){
	if( size < 0) throw Error(0,"(Copy_Array_Op)");
	for(int i = 0; i < size ; i++) {
		array_copy[i] = array[i];
	}
};

/*
Duplicates a rule array
@param array: rule array.Array to be copied.
@param size: Int. Size of array.
@return Rule: copied array.
@throws exception if the size is invalid
*/
void Copy_Array_Rule(Rule * array_copy,Rule * array,int size){
	if( size < 0) throw Error(0,"(Copy_Array_Rule)");
	for(int i = 0; i < size ; i++) array_copy[i].Equal(&array[i]);
};

/*
Calculates the center of the equally distributed triangles
on range.
@deprecated	Auxiliar function
@param N_labels: Int. Number of triangles.
@param Lim_Inf: Decimal. Inferior limite on range.
@param Lim_Sup: Decimal. Superior limite on range.
@return centers: Array of  Decimals. Centers.
@see Make_Triangular_Labels. Make_Trapezium_Labels
*/
void Calculate_Triangles_Center(double * centers,int N_labels,double Lim_Inf,double Lim_Sup){
	if( N_labels != 1){
		for(int i = 0; i < N_labels ; i++){
			centers[i] = i * ( (Lim_Sup - Lim_Inf) / (N_labels - 1) )
			+ Lim_Inf;
		}
	}else{centers[0] = (Lim_Sup + Lim_Inf) / 2;}
};

/*
Returns the string that represents the operator
*/
const char * GetOperatorString(int name){
	if(name == AND) return "AND";
	if(name == OR) return "OR";
        return "ERROR";
};

/*
Finds the modifier name asociated with the integer.
@param Mod: Int. Identifier of modifier
@return string: Name of modifier
@throws Exception if the modifier does not exists
*/
const char * GetModifier(int Mod){
	switch(Mod){
		case NO: return "NO";  
		case FEW:  return "FEW";  
		case VERY: return "VERY";  
		case EXTRA: return "EXTRA";  
		case LESSTHAN: return "LESSTHAN";  
		case GREATERTHAN: return "GREATERTHAN";  
		case LESSOREQUALTHAN: return "LESSOREQUALTHAN";  
		case GREATEROREQUALTHAN: return "GREATEROREQUALTHAN";  
		case -1: return "-1";
		default: throw Error(15, "(GetModifier)");
	}
};

/*
Verifies if a string is the name of an existen modifier
@param Mod: String. Modifier to be checked.
@return The number of modifier or -1 if the modifier does not exist.
*/
int IsModifier(const char * Mod){
	if (strcmp(Mod,"NO") == 0) return NO;
	else if (strcmp(Mod,"FEW") == 0) return FEW;
	else if (strcmp(Mod,"VERY") == 0) return VERY;
	else if (strcmp(Mod,"EXTRA") == 0) return EXTRA;
	else if (strcmp(Mod,"LESSTHAN") == 0) return LESSTHAN;
	else if (strcmp(Mod,"GREATERTHAN") == 0) return GREATERTHAN;
	else if (strcmp(Mod,"LESSOREQUALTHAN") == 0) return LESSOREQUALTHAN;
	else if (strcmp(Mod,"GREATEROREQUALTHAN") == 0) return GREATEROREQUALTHAN;
	else return -1;

};

/*
Verifies if a string is the name of an existen operator
@param Op: String. Operator to be checked.
@return The char of operator or 'N' if the operator does not exist.
*/
char IsOperator(const char * Op){
	if (strcmp(Op,"AND") == 0) return AND;
	else if (strcmp(Op,"OR") == 0) return OR;
	else return 'N';

};

/*
Verifies if a string is the name of an existen disjuction
@param disj: String. String to be checked.
@return The int of disjuntion or -1 if the disjunction does not exist.
*/
int IsOr(const char * disj){
	if (strcmp(disj, "MAX") == 0) return LAX;
	else if (strcmp(disj, "SUM") == 0) return SUM;
	else return -1;
};

/*
Verifies if a string is the name of an existen conjuction
@param Conj: String. String to be checked.
@return The int of conjuntion or -1 if the conjunction does not exist.
*/
int IsAnd(const char * conj){
	if (strcmp(conj, "MIN") == 0) return LIN;
	else if (strcmp(conj, "PRO") == 0) return PRO;
	else return -1;
};

/*MEMBERSHIP FUNCTIONS*/

/*
Calculates the degree of membership of the value in a triangular label
@param in_value: Value to be evaluated.
@param vertex: Array of double. Triangle vertex
*/
double Triangle_Membership(double in_value, double * vertex){ // a-0,b-1,c-0
	if(in_value < vertex[0]) return 0;
	else if(in_value > vertex[2]) return 0;
	else if (in_value <= vertex[1]) return (in_value - vertex[0])/(vertex[1]- vertex[0]);
	else if (in_value > vertex[1]) return (vertex[2]- in_value)/(vertex[2]- vertex[1]);
        return -INFINITO;
};

/*
Calculates the degree of membership of the value in a trapezium label
@param in_value: Value to be evaluated.
@param vertex: Array of double. Trapezium vertex
*/
double Trapezium_Membership(double in_value, double * vertex){ // a-0,b-1,c-1,d-0
	if(in_value < vertex[0]) return 0;
	else if(in_value > vertex[3]) return 0;
	else if (in_value < vertex[1]) return (in_value - vertex[0])/(vertex[1]- vertex[0]);
	else if (in_value >= vertex[1] && in_value <= vertex[2] ) return 1;
	else if (in_value > vertex[2]) return (vertex[3]- in_value)/(vertex[3]- vertex[2]);
        return -INFINITO;
};

/*
Calculates the degree of membership of the value in a Gauss label
@param in_value: Value to be evaluated.
@param vertex: Array of double. Gauss values. 
*/
double Gauss_Membership(double in_value, double * g_values){ // c(media), sigma(raiz de varianza)
	int sigma = 1;
	int c = 0;
	return pow(E, ((-1*pow((in_value - g_values[c]),2)) / (2*pow(g_values[sigma],2)) ) );
};

/*
Calculates the degree of membership of the value in a Bell label
@param in_value: Value to be evaluated.
@param vertex: Array of double. Bell values
*/
double Bell_Membership(double in_value, double * b_values){// c(centro) a(ancho) b(suavidad)
	return 1/ ( 1 + (pow(((in_value-b_values[0])/b_values[1]),(2*b_values[2])) ) );
};

/*
Calculates the degree of membership of the value in a PI label
@param in_value: Value to be evaluated.
@param vertex: Array of double. Trapezium vertex
*/
double PI_Membership(double in_value, double * vertex){ //a-0,b-1,c-2,d-3
	int a = 0; int b = 1; int c = 2; int d = 3;
	if(in_value < vertex[a]) return 0;
	else if(in_value > vertex[d]) return 0;
	else if (in_value < ((vertex[a] + vertex[b])/2)) 
		return 2*pow((in_value - vertex[a])/(vertex[b]- vertex[a]),2);
	else if (in_value >= ((vertex[a] + vertex[b])/2) && in_value < vertex[b])
		return 1 - 2*pow((in_value - vertex[b])/(vertex[b]- vertex[a]),2);
	else if (in_value >= vertex[b] && in_value <= vertex[c]) return 1;
	else if (in_value <= ((vertex[c] + vertex[d])/2) && in_value > vertex[c])
		return 1 - 2*pow((in_value - vertex[c])/(vertex[d]- vertex[c]),2);
	else if (in_value > ((vertex[c] + vertex[d])/2))
		return 2*pow((in_value - vertex[d])/(vertex[d]- vertex[c]),2);	 	
        return -INFINITO;
};

 /*
Converts a string into a double
@param string String to be converted
@param line Integer used in the exception
@return Double. String converted
@thows exception if the string does not represen a double*/
double atod(char * string, const char * msg){
	double x = atof(string);
	if( x == 0.0 && string[0]!= '0') {
		throw Error(11,msg);
	}
	return x;
};
/*********CLASS ERROR (to handle exceptions)***********/
/*
Function to get the error message that explains the exception.
@return	String. The error message.
*/
const char * Error::message_error() const throw(){
	char * ret= new char[MAX_SIZE];
	switch (motive){
		case 0: sprintf(ret, "%s%s",msg,"Number_Of_Elementes_Can_NOT_Be_Negative"); return ret;
		case 1: sprintf(ret, "%s%s",msg,"Invalid_Range"); return ret;
		case 2: sprintf(ret, "%s%s",msg,"Invalid_Index_Of_Entry"); return ret;
		case 3: sprintf(ret, "%s%s",msg,"Value_Out_Of_Range_[0,1]"); return ret;
		case 4: sprintf(ret, "%s%s",msg,"Invalid_Label_Type"); return ret;
		case 5: sprintf(ret, "%s%s",msg,"Invalid_Name"); return ret;
		case 6: sprintf(ret, "%s%s",msg,"Wrong_Vertex"); return ret;
		case 7: sprintf(ret, "%s%s",msg,"Invalid_Sigma_Value"); return ret;
		case 8: sprintf(ret, "%s%s",msg,"Invalid_Index_Of_Out"); return ret;
		case 9: sprintf(ret, "%s%s",msg,"Invalid_Index_Of_Singleton_Out"); return ret;
		case 10: sprintf(ret, "%s%s",msg,"File_OPEN_Failed"); return ret;
		case 11: sprintf(ret, "%s%s", msg,"Error_On_File"); return ret;
		case 12: sprintf(ret, "%s%s",msg,"Wrong_Value");return ret;
		case 13: sprintf(ret, "%s%s",msg,"Bell_Membership:Division_By_Cero"); return ret;
		case 14: sprintf(ret, "%s%s",msg,"Invalid_Operator"); return ret;
		case 15: sprintf(ret, "%s%s",msg,"Invalid_Modifier"); return ret;
		case 16: sprintf(ret, "%s%s",msg,"Invalid_Index_Of_Label"); return ret;
		case 17: sprintf(ret, "%s%s",msg,"Invalid_Index_Of_Rule"); return ret;
		case 18: sprintf(ret, "%s%s",msg,"Consequence_Cannot_Use_OR_Operator"); return ret;
		case 19: sprintf(ret, "%s%s",msg,"Invalid_Index_Of_RULEBASE"); return ret;
		case 20: sprintf(ret, "%s%s",msg,"Invalid_Index_Of_Condition"); return ret;
		case 21: sprintf(ret, "%s%s",msg,"Invalid_Index_Of_Consequence"); return ret;
		case 22: sprintf(ret, "%s%s",msg,"Invalid_Condition_In_Controler"); return ret;
		case 23: sprintf(ret, "%s%s",msg,"Invalid_Consequence_In_Controler"); return ret;
		case 24: sprintf(ret, "%s%s",msg,"Error_In_Rule"); return ret;
		case 25: sprintf(ret, "%s%s",msg,"Invalid_Index_Of_Label_Value"); return ret;
		case 100: return " ...1,2,3 PROBANDO";
		default: return "Unknown_Error"; // Should never happen.
	}
};

