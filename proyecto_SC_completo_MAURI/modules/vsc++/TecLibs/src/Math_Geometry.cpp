//============================================================================
// Name        : Math_Geometry.cpp
// Author      : RA-MM
// Version     : 0.1
// Description : Math library used for geometric calculation.
//============================================================================

#include "../inc/Math_Geometry.hpp"


namespace tri  {
namespace ad   {
namespace math {

/* * * Conversion functions * * * */

double rad2Deg(double rad){
    return rad*180/M_PI;
}

double deg2Rad(double deg){
    return deg*M_PI/180;
}

double normalizeAngleRad(double rad){
    double x, y;
    y = sin(rad);       // y component of a vector form by the angle
    x = cos(rad);       // x component of a vector form by the angle
    return atan2(y, x);
}


double normalizeAngleDeg(double deg){
    double tempVar;
    tempVar = deg2Rad(deg);               // change to radians
    tempVar = normalizeAngleRad(tempVar); // Normalize between -pi pi
    tempVar = rad2Deg(tempVar);           // lat conversion to deg again
    return tempVar;
}


double absoluteAngleRad(double rad){
    double tempVar = normalizeAngleRad(rad); // Firstly is normalized
    if(tempVar < 0) // in case of negative number is changed to pi to 2pi
        return tempVar + 2*M_PI;
    return tempVar;
}


double absoluteAngleDeg(double deg){
    double tempVar = normalizeAngleRad(deg); // Firstly is normalized
    if(tempVar < 0) // in case of negative number is changed to 180 to 360
        return tempVar + 2*180;
    return tempVar;
}


double ms2Kmh(double ms){
    return ms*3.6;
}


double kmh2Ms(double kmh){
    return kmh/3.6;
}


double gs2Ms2(double gs){
    return gs*GRAVITY;
}


double ms22Gs(double ms2){
    return ms2/GRAVITY;
}




/* * * Points functions * * * */

double distance2Points(const Eigen::VectorXd & p0, const Eigen::VectorXd & p1){
    Eigen::VectorXd pX;
    if( p0.size() == p1.size() ){
        pX = p1 - p0;
        return sqrt( pX.transpose()*pX );
    }else{
        std::cout << ERROR0002 << "\n" << std::endl;
        return -1.0;
    }
}


/* * * Points functions * * * */

/* * * Vectors functions * * * */

Eigen::Vector3d normalizedVectorOf(const Eigen::Vector3d & inputVector){
    if(inputVector.norm() == 0){
        //std::cout << ERROR0003 << "\n" << std::endl;
        return inputVector;
    }
    return  inputVector/inputVector.norm();
}


double angleBetweenVectorsRad(const Eigen::Vector3d & v1, const Eigen::Vector3d & v2){
    Eigen::Vector3d vNormal(1,1,1);
    double          angle;

    // Verifying the norms
    if( v1.norm() == 0 || v2.norm() == 0){
        //std::cout << ERROR0004 << "\n" << std::endl;
        return 0;
    }

    // Angle between vectors ith sign
    angle = fabs( acos( v1.dot(v2) / ( v1.norm() * v2.norm() ) ) );
    if(vNormal.dot(v1.cross(v2)) < 0)
        return -angle;
    return angle;
}


double angleVectorAroundZ(const Eigen::Vector3d & v1){
    Eigen::Vector3d vBase(1,0,0);
    return angleBetweenVectorsRad( vBase, v1 );
}


double distPointLine( const Eigen::Vector3d & linePoint, const Eigen::Vector3d & directVectorIJK, const Eigen::Vector3d & point, Eigen::Vector3d *shortestPoint ){
    Eigen::Vector3d vectorAC;
    double          distance;

    vectorAC       = point - linePoint;
    *shortestPoint = linePoint + vectorAC.dot(normalizedVectorOf(directVectorIJK))*normalizedVectorOf(directVectorIJK);
    distance       = distance2Points( *shortestPoint, point );

    if( normalizeAngleRad(angleBetweenVectorsRad( directVectorIJK , vectorAC ) ) < 0 )
        distance = -1*distance;
    return distance;
}


double distPointSegment( const Eigen::Vector3d & p0, const Eigen::Vector3d & p1, const Eigen::Vector3d & p2, Eigen::Vector3d *shortestPoint ){
    Eigen::Vector3d vectorAB, vectorAC;
    double          distance, factor;

    vectorAB       = p1 - p0;
    vectorAC       = p2 - p0;
    factor         = vectorAC.dot(normalizedVectorOf(vectorAB));

    if( factor > vectorAB.norm() ){
        *shortestPoint = p1;
        return NAN;
    }else if( factor < 0 ){
        *shortestPoint = p0;
        return NAN;
    }

    if(vectorAC.norm() == 0){
        *shortestPoint = p0;
        return 0;
    }


    *shortestPoint = p0 + factor*normalizedVectorOf(vectorAB);
    distance       = (*shortestPoint- p2 ).norm();

    if( normalizeAngleRad(angleBetweenVectorsRad( vectorAB , vectorAC ) ) < 0 )
        distance = -1*distance;
    return distance;
}


void   addElementVectorX(Eigen::VectorXd & v, double elem){
    v.conservativeResize(v.size() + 1);
    v(v.size()-1) = elem; // indexes start with 0 in c/c++
}

Eigen::Vector3d rotateVectorAroundZ(const Eigen::VectorXd & v, double rotationAngle){
    Eigen::Vector3d rotatedVector(0,0,0);
    rotatedVector(0) = v(0)*cos(rotationAngle) - v(1)*sin(rotationAngle);
    rotatedVector(1) = v(0)*sin(rotationAngle) + v(1)*cos(rotationAngle);
    return rotatedVector;
}





double speedLimiter(double inSpeed,double curvature, double maxSpeed, double comfort){

    double k = 1.4; // Formula parameter
    double speed;

    speed = std::min(inSpeed, maxSpeed);
    if(curvature != 0){
		speed     = std::min(sqrt(comfort/(k*fabs(curvature))),speed);
    }
    return speed;

}


bool intersectionBetweenSegmentsIn2D(const Eigen::Vector3d & s0P0, const Eigen::Vector3d & s0P1, const Eigen::Vector3d & s1P0, const Eigen::Vector3d & s1P1, Eigen::Vector3d & interPnt){
    // Intersection between 2 segments in 2D
    Eigen::Vector3d s0Vector = s0P1 - s0P0; //
    Eigen::Vector3d s1Vector = s1P1 - s1P0; // vectors for each segment

    Eigen::Vector3d v0CrossV1  = s0Vector.cross(s1Vector);
    double          v0CrossV1Z = v0CrossV1(2);

    // no intersection (basic conditions)
    if(s0Vector.norm() == 0 || s1Vector.norm() == 0 || v0CrossV1Z == 0){
        interPnt << 0,0,0;
        return false;
    }

    Eigen::Vector3d num0  = (s1P0 - s0P0).cross(s1Vector);
    double          num0Z = num0(2);

    Eigen::Vector3d num1  = (s1P0 - s0P0).cross(s0Vector);
    double          num1Z = num1(2);

    // no intersection in segments but they intersect as lines
    if( num0Z/v0CrossV1Z < 0 || num0Z/v0CrossV1Z > 1 || num1Z/v0CrossV1Z < 0 || num1Z/v0CrossV1Z > 1){
        interPnt << 0,0,0;
        return false;
    }

    interPnt = s0P0 + (num0Z/v0CrossV1Z)*s0Vector/(s0Vector.norm());
    return true;
}

} /* end of math */
} /* end of ad       */
} /* end of tri      */
