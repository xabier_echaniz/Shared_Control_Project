//============================================================================
// Name        : Math_Geometry.cpp
// Author      : RA-MM
// Version     : 0.1
// Description : Math library used for geometric calculation.
//============================================================================

#include "../inc/Math_Circle.hpp"

namespace tri  {
namespace ad   {
namespace math {

int intersectionLineSphere(Eigen::Vector3d linePto, Eigen::Vector3d lineVector, Eigen::Vector3d circleOrigin, double circleRadius, Eigen::Vector3d & ptoInter1, Eigen::Vector3d & ptoInter2){
    double          A,  B,  C, t1, t2, resol, tTemp;
    Eigen::Vector3d vA, vB, vC;

    vA = lineVector;
    vB = linePto - circleOrigin;

    A = pow(vA.norm(), 2);
    B = 2*vA.dot(vB);
    C = pow(vB.norm(), 2) - pow(circleRadius,2);

    resol = B*B - 4*A*C;

    if(resol < 0 || A == 0){
        ptoInter1 << NAN, NAN, NAN;
        ptoInter2 = ptoInter1;
        return 0;
    }

    t1 = (-B + sqrt(resol))/(2*A);
    t2 = (-B - sqrt(resol))/(2*A);

    if(fabs(t2) < fabs(t1)){
        tTemp = t1;
        t1    = t2;
        t2    = tTemp;
    }

    ptoInter1 = linePto + t1*lineVector;
    ptoInter2 = linePto + t2*lineVector;
    return 1;
}

int intersectionSegmentSphere(Eigen::Vector3d sgmPto1, Eigen::Vector3d sgmPto2, Eigen::Vector3d circleOrigin, double circleRadius, Eigen::Vector3d & ptoInter1, Eigen::Vector3d & ptoInter2){
    Eigen::Vector3d ptoTemp1, ptoTemp2;
    if( intersectionLineSphere(sgmPto1, sgmPto2 - sgmPto1, circleOrigin, circleRadius, ptoTemp1, ptoTemp2) == 0){
        ptoInter1 = ptoTemp1;
        ptoInter2 = ptoTemp2;
        return 0;
    }

    if((ptoTemp1 - sgmPto1).dot(sgmPto2 - sgmPto1) < 0 || (ptoTemp1 - sgmPto1).norm() > (sgmPto2 - sgmPto1).norm()){
        ptoInter1 << NAN, NAN, NAN;
    }else{
        ptoInter1 = ptoTemp1;
    }
    if((ptoTemp2 - sgmPto1).dot(sgmPto2 - sgmPto1) < 0 || (ptoTemp2 - sgmPto1).norm() > (sgmPto2 - sgmPto1).norm()){
        ptoInter2 << NAN, NAN, NAN;
    }else{
        ptoInter2 = ptoTemp2;
    }

    if( std::isnan(ptoInter1(0)) && std::isnan(ptoInter2(0)) ){
        return 0;
    }

    if( std::isnan(ptoInter1(0)) ){
        ptoInter1 = ptoInter2;
        ptoInter2 << NAN, NAN, NAN;
    }else if( ( ptoInter2 - sgmPto1 ).norm() < ( ptoInter1 - sgmPto1 ).norm()){
        ptoTemp2  = ptoInter2;
        ptoInter2 = ptoInter1;
        ptoInter1 = ptoInter2;
    }
    return 1;
}

int closestPtoSegmentSphere(Eigen::Vector3d sgmPto1, Eigen::Vector3d sgmPto2, Eigen::Vector3d circleOrigin, double circleRadius, Eigen::Vector3d & ptoInter1){
    Eigen::Vector3d ptoTemp1, ptoTemp2;
    if(intersectionSegmentSphere(sgmPto1, sgmPto2, circleOrigin, circleRadius, ptoTemp1, ptoTemp2) == 0){
        ptoInter1 << NAN, NAN, NAN;
        return 0;
    }
    if( (sgmPto1 - ptoTemp1).norm() > (sgmPto1 - ptoTemp2).norm() ){
        ptoInter1 = ptoTemp2;
    }else{
        ptoInter1 = ptoTemp1;
    }
    return 1;

}

void completeCircle(Eigen::Vector3d circleCenter, double circleRadius, double angle1Rad, double angle2Rad, double deltaAngleRad, int clockwise, std::vector<double> & x, std::vector<double> & y){
    Eigen::Vector3d vNormal(1,0,0);
    double angleBetween = tri::ad::math::absoluteAngleRad( tri::ad::math::normalizeAngleRad(angle2Rad) - tri::ad::math::normalizeAngleRad(angle1Rad) );
    double angleStep    = 0;
    if(clockwise >= 1){ // Clockwise 1 is true, 0 is false
        angleBetween = 2*M_PI - angleBetween;
    }
    if(angleBetween < deltaAngleRad){
        x.push_back( circleCenter(0) + circleRadius*cos(angle1Rad) );
        y.push_back( circleCenter(1) + circleRadius*sin(angle1Rad) );
        x.push_back( circleCenter(0) + circleRadius*cos(angle2Rad) );
        y.push_back( circleCenter(1) + circleRadius*sin(angle2Rad) );
    }else{
        while(angleStep < angleBetween){
            if(clockwise >= 1){
                x.push_back( circleCenter(0) + circleRadius*cos(angle1Rad - angleStep) );
                y.push_back( circleCenter(1) + circleRadius*sin(angle1Rad - angleStep) );
            }else{
                x.push_back( circleCenter(0) + circleRadius*cos(angle1Rad + angleStep) );
                y.push_back( circleCenter(1) + circleRadius*sin(angle1Rad + angleStep) );
            }
            angleStep = angleStep + deltaAngleRad;
        }
        // The last point is added manually because in the loop is not added
        x.push_back( circleCenter(0) + circleRadius*cos(angle2Rad) );
        y.push_back( circleCenter(1) + circleRadius*sin(angle2Rad) );
    }
}

Eigen::Vector3d circleCenter(const Eigen::Vector3d  & pnt1, const Eigen::Vector3d  & pnt2, const Eigen::Vector3d  & pnt3){
    double x1, y1, x2, y2, x3, y3;
    double x,y,z;
    double denom, numx, numy;


    x1 = (long double)pnt1(0); y1 = (long double)pnt1(1);
    x2 = (long double)pnt2(0); y2 = (long double)pnt2(1);
    x3 = (long double)pnt3(0); y3 = (long double)pnt3(1);

    // This calculation is to avoid the overflow when you multiply (double)*(doubble)
    x = x1;
    y = y1;
    x1 = x1 - x;
    y1 = y1 - y;
    x2 = x2 - x;
    y2 = y2 - y;
    x3 = x3 - x;
    y3 = y3 - y;

    //std::cout.precision(10);

    denom = (double)2.00*( x1*(y2 - y3) - (y1*(x2-x3)) + x2*y3 - x3*y2);
    numx  = (double)((x1*x1 + y1*y1)*(y2-y3) + (x2*x2 + y2*y2)*(y3 - y1) + (x3*x3 + y3*y3)*(y1 - y2));
    numy  = (double)((x1*x1 + y1*y1)*(x3-x2) + (x2*x2 + y2*y2)*(x1 - x3) + (x3*x3 + y3*y3)*(x2 - x1));
    if( denom == 0){
        x = pnt1(0) + x; y = pnt1(1) + y; z = pnt1(2);
    }else{
        x = numx/denom + x; y = numy/denom + y; z = pnt1(2);
    }
    return Eigen::Vector3d((double)x,(double)y,(double)z);
}

Eigen::Vector3d generateCircle3Pnts(const Eigen::Vector3d  & pnt1, const Eigen::Vector3d  & pnt2, const Eigen::Vector3d  & pnt3, double * curvature){
    Eigen::Vector3d circleCenterXYZ = circleCenter(pnt1, pnt2, pnt3);
    std::cout << "Circle center:   [" << pnt2(0) - pnt1(0) << " , " << pnt2(1) - pnt1(1) << "]" << std::endl;
    std::cout << "Circle center:   [" << pnt3(0) - pnt1(0) << " , " << pnt3(1) - pnt1(1) << "]" << std::endl;

    *curvature = 1/(circleCenterXYZ - pnt1).norm();
    if( tri::ad::math::angleBetweenVectorsRad(pnt2 - pnt1, pnt3 - pnt1) < 0 ){
        *curvature =  - *curvature;
    }

    return circleCenterXYZ;
}



} /* end of math */
} /* end of ad       */
} /* end of tri      */
