//============================================================================
// Name        : ADCOREBufferClass.cpp
// Author      : RL - MM
// Version     : 0.1
// Description :
//============================================================================


#include "../inc/Planning_GlobalMap.hpp"

using tri::ad::math::distance2Points;
using tri::ad::math::distPointSegment;

namespace tri      {
namespace ad       {
namespace planning {

/* * * * Overwrite from SimpleMap * * * */
void GlobalMap::clear(){
    this->globalMapNodes.clear();
}
unsigned int GlobalMap::getSize(){
	return this->globalMapNodes.getSize();
}

void GlobalMap::printFile(std::string directory){
	this->globalMapNodes.printFile(directory, "LogGlobalMap");
}

void GlobalMap::appendNode(SimpleMapNode node){
	this->globalMapNodes.appendNode(node);
}

SimpleMapNode GlobalMap::getNodeInfo(const int & idx){
	return this->globalMapNodes.getNodeInfo(idx);
}

void GlobalMap::reIndexing(){
    this->globalMapNodes.reIndexing();
}

void GlobalMap::overwriteMapNodes(std::vector<SimpleMapNode> map){
    this->globalMapNodes.overwriteMapNodes(map);
}




/* * * * Global map class methods * * * */

GlobalMap::GlobalMap(){}

GlobalMap::~GlobalMap(){}

SimpleMap GlobalMap::getGlobalMapNodes(){
    return this->globalMapNodes;
}

int GlobalMap::parseNode(tinyxml2::XMLElement  *pMap, SimpleMapNode *node){

    if(pMap->QueryUnsignedAttribute ("id" , &node->id ) != 0 ){
        std::cout << ERROR0009 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
        return 0;
    }

    std::string dumbString = pMap->Attribute("type");
    if( dumbString== "" ){
        std::cout << ERROR0010 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
        return 0;
    }else if(dumbString != "waypoint" && dumbString != "intersection" && dumbString != "roundabout" && dumbString != "lanechange" ){
        std::cout << ERROR0011 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
        return 0;
    }else{
        if(dumbString == "waypoint")
            node->type = WAYPOINT;
        if(dumbString == "intersection")
            node->type = INTERSECTION;
        if(dumbString == "roundabout")
            node->type = ROUNDABOUT;
        if(dumbString == "lanechange")
            node->type = LANECHANGE;
    }


    if(pMap->QueryDoubleAttribute ("x" , &node->x )  != 0 ){
        std::cout << ERROR0012 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
        return 0;
    }

    if(pMap->QueryDoubleAttribute ("y" , &node->y )  != 0 ){
        std::cout << ERROR0013 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
        return 0;
    }

    if(pMap->QueryDoubleAttribute ("z" , &node->z )  != 0 ){
        std::cout << ERROR0014 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
        return 0;
    }

    if( ( node->type == WAYPOINT || node->type == ROUNDABOUT ) && pMap->QueryDoubleAttribute ("k" , &node->k )  != 0 ){
        std::cout << ERROR0015 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
        return 0;
    }

    if(  pMap->QueryDoubleAttribute ("roadWidth" , &node->roadWidth )  != 0 ){
        std::cout << ERROR0016 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
        return 0;
    }

    if( node->type == ROUNDABOUT && pMap->QueryDoubleAttribute ("angleIn" , &node->angleInRad )  != 0 ){
        node->angleInRad = 0;
        std::cout << WARNING0001 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
    }

    if( node->type == ROUNDABOUT && pMap->QueryDoubleAttribute ("angleOut" , &node->angleOutRad )  != 0 ){
        node->angleOutRad = 0;
        std::cout << WARNING0002 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
    }

    if( pMap->QueryUnsignedAttribute ("nLanesRight" , &node->nLanesRight )  != 0 ){
        node->nLanesRight = 0;
        std::cout << WARNING0003 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
    }

    if( pMap->QueryUnsignedAttribute ("nLanesLeft" , &node->nLanesLeft )  != 0 ){
        node->nLanesLeft = 0;
        std::cout << WARNING0004 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
    }

    if(node->type == ROUNDABOUT){
        if( pMap->QueryUnsignedAttribute ("clockwise" , &node->rotateRight )  != 0 ){
            node->rotateRight = 0;
            std::cout << WARNING0005 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
        }
    }else if(node->type == LANECHANGE){
        if( pMap->QueryUnsignedAttribute ("toRight" , &node->rotateRight )  != 0 ){
            node->rotateRight = 0;
            std::cout << WARNING0006 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
        }
    }

    if( pMap->QueryDoubleAttribute ("maxSpeed" , &node->maxSpeed )  != 0 ){
        node->maxSpeed = 0;
        std::cout << ERROR0021 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
        return 0;
    }

    return 1;
}

int GlobalMap::readMap(std::string route){
    tinyxml2::XMLDocument  xmlDoc; // Xml file
    tinyxml2::XMLElement  *pMap;
    SimpleMapNode          node;
    int                   errorCode = xmlDoc.LoadFile(route.c_str());

    // Evaluation that the file exists
    if(errorCode != tinyxml2::XML_SUCCESS ){                      // opening xml file
        std::cout << ERROR0006 << " TinyXML.LoadFile method returns ID error#"<< errorCode << "." <<std::endl;
        return 0;
    }

    // evaluation the father node should be named simpleMAP
    pMap = xmlDoc.FirstChildElement("simpleMAP");
    if(!pMap){
        std::cout << ERROR0007 << std::endl;
        return 0;
    }

    // evaluate the first node
    pMap = pMap->FirstChildElement( "node" );
    if(!pMap && this->getSize() == 0){
        std::cout << ERROR0008 << std::endl;
        return 0;
    }

    // After all the error diagnosis the file is completely read
    while(pMap){
        node.clean();
        this->parseNode(pMap, &node); // Parsing the information of a node
        this->appendNode(node);       // adding the node to the list
        pMap = pMap->NextSiblingElement("node");
    }
    return 1;
}

int GlobalMap::mapCorrect(){
    int i;

    // Numero de puntos minimos
    if( this->getSize() < 3 ){
        std::cout << ERROR0018 << std::endl;
        return 0;
    }

    // first and last  point must be a waypoint
    if(this->getNodeInfo(0).type != WAYPOINT && this->getNodeInfo(this->getSize()-1).type != WAYPOINT){
        std::cout << ERROR0019 << std::endl;
        return 0;
    }

    // two roundabouts cannot be one after each or a roundabout after a lanechange
    for(i = 0; i < ( (int)this->getSize() - 1 ); i++){
        if( (this->getNodeInfo(i).id == this->getNodeInfo(i+1).id && this->getNodeInfo(i).id == ROUNDABOUT) || (this->getNodeInfo(i).id == LANECHANGE && this->getNodeInfo(i+1).id == ROUNDABOUT)){
            std::cout << ERROR0020 << std::endl;
            return 0;
        }
    }


    return 1;
}

void GlobalMap::completeMap(double distRoundabouts){
    int i, j;
    Eigen::Vector3d circleCenter(0,0,0), tempPto1, tempPto2, tempPto3;
    double          tempAngle1, tempAngle2;
    SimpleMapNode   node;
    std::vector<SimpleMapNode> newMap;
    std::vector<double> x, y;

    if(this->mapCorrect() == 1){
        for(i = 0; i < (int)this->getSize(); i++){
            if(this->getNodeInfo(i).type == ROUNDABOUT){
                // All the necesarry points will be created to complete the roundabout
                x.clear(); y.clear();
                circleCenter << this->getNodeInfo(i).x , this->getNodeInfo(i).y , 0;
                /*tempPto1     << newMap.back().x      , newMap.back().y      , 0;
                tempPto2     << this->getNodeInfo(i + 1).x , this->getNodeInfo(i + 1).y , 0;
                tri::ad::math::closestPtoSegmentSphere(tempPto1, circleCenter, circleCenter, 1/this->getNodeInfo(i).k, tempPto3);
                tempAngle1 = tri::ad::math::angleVectorAroundZ(tempPto3 - circleCenter);
                tri::ad::math::closestPtoSegmentSphere(tempPto2, circleCenter, circleCenter, 1/this->getNodeInfo(i).k, tempPto3);
                tempAngle2 = tri::ad::math::angleVectorAroundZ(tempPto3 - circleCenter);

                if(this->getNodeInfo(i).rotateRight >= 1){
                    tempAngle1 = tempAngle1 - this->getNodeInfo(i).angleInRad;
                    tempAngle2 = tempAngle2 + this->getNodeInfo(i).angleOutRad;
                }else{
                    tempAngle1 = tempAngle1 + this->getNodeInfo(i).angleInRad;
                    tempAngle2 = tempAngle2 - this->getNodeInfo(i).angleOutRad;
                }*/
                tempAngle1 = tri::ad::math::absoluteAngleRad(this->getNodeInfo(i).angleInRad);
                tempAngle2 = tri::ad::math::absoluteAngleRad(this->getNodeInfo(i).angleOutRad);

                tri::ad::math::completeCircle(circleCenter, 1/this->getNodeInfo(i).k, tempAngle1, tempAngle2, distRoundabouts*this->getNodeInfo(i).k, this->getNodeInfo(i).rotateRight, x, y);

                node = this->getNodeInfo(i);
                for(j=0; j < (int)x.size(); j++){
                    node.x = x[j];
                    node.y = y[j];
                    if(j == 0){
                        node.type = RBENTRANCE;
                    }else if(j == (int)x.size()-1){
                        node.type = RBEXIT;
                    }else{
                        node.type = RBMIDDLE;
                    }
                    newMap.push_back(node);
                }


            }else if(this->getNodeInfo(i).type == LANECHANGE){
                // the vector rotates and two new points are generated
                tempPto1  << newMap.back().x          , newMap.back().y          , 0;
                tempPto2  << this->getNodeInfo(i + 0).x , this->getNodeInfo(i + 0).y , 0;
                tempAngle1 = tri::ad::math::angleVectorAroundZ(tempPto2 - tempPto1);
                if(this->getNodeInfo(i).rotateRight == 1){
                    tempAngle1 = tempAngle1 - M_PI/2;
                }else{
                    tempAngle1 = tempAngle1 + M_PI/2;
                }
                node      = this->getNodeInfo(i);
                node.type = LCSTART;
                node.x    = this->getNodeInfo(i).x;
                node.y    = this->getNodeInfo(i).y;
                newMap.push_back(node);
                node.type = LCEND;
                node.x    = this->getNodeInfo(i).x + this->getNodeInfo(i).roadWidth*cos(tempAngle1);
                node.y    = this->getNodeInfo(i).y + this->getNodeInfo(i).roadWidth*sin(tempAngle1);
                newMap.push_back(node);
            }else{
                newMap.push_back(this->getNodeInfo(i));
            }
        }
    }

    this->overwriteMapNodes(newMap);
}


} /* end of planning */
} /* end of ad       */
} /* end of tri      */
