//============================================================================
// Name        : Math_Geometry.cpp
// Author      : RA-MM
// Version     : 0.1
// Description : Math library used for geometric calculation.
//============================================================================

#include "../inc/Math_Bezier.hpp"

namespace tri  {
namespace ad   {
namespace math {

void constructBezier3er(int nElements, const Eigen::Matrix<double, 4, 3> &controlPoints, std::vector<double> & x, std::vector<double> & y, std::vector<double> & z, std::vector<double> & k){
	Eigen::Vector3d cPnt0, cPnt1, cPnt2, cPnt3;
	Eigen::Vector3d k0, k1, k2, k3;
	Eigen::Vector3d bez, dBez, d2Bez;
	Eigen::MatrixXd matrixOut(nElements, 4);

	int    i;
	double t;
	cPnt0 << controlPoints(0,0), controlPoints(0,1), controlPoints(0,2);
	cPnt1 << controlPoints(1,0), controlPoints(1,1), controlPoints(1,2);
	cPnt2 << controlPoints(2,0), controlPoints(2,1), controlPoints(2,2);
	cPnt3 << controlPoints(3,0), controlPoints(3,1), controlPoints(3,2);

	k0 =     cPnt0;
	k1 = - 3*cPnt0 + 3*cPnt1;
	k2 =   3*cPnt0 - 6*cPnt1 + 3*cPnt2;
	k3 = -   cPnt0 + 3*cPnt1 - 3*cPnt2 +   cPnt3;

	for(i = 0; i < nElements; i++){
		t = ((double)i)/((double)(nElements-1));
		bez   =   k0 +   t*k1 +   pow(t,2)*k2 + pow(t,3)*k3;
		dBez  =   k1 + 2*t*k2 + 3*pow(t,2)*k3;
		d2Bez = 2*k2 + 6*t*k3;
		x.push_back( bez(0) );
		y.push_back( bez(1) );
		z.push_back( bez(2) );
		k.push_back( ( dBez(0)*d2Bez(1) - dBez(1)*d2Bez(0) ) / ( sqrt( pow( dBez(0)*dBez(0) + dBez(1)*dBez(1) ,3) ) ) );
	}
}

void constructBezier4to(int nElements, const Eigen::Matrix<double, 5, 3> &controlPoints, std::vector<double> & x, std::vector<double> & y, std::vector<double> & z, std::vector<double> & k){
	Eigen::Vector3d cPnt0, cPnt1, cPnt2, cPnt3, cPnt4;
	Eigen::Vector3d k0, k1, k2, k3, k4;
	Eigen::Vector3d bez, dBez, d2Bez;

	int    i;
	double t;
	cPnt0 << controlPoints(0,0), controlPoints(0,1), controlPoints(0,2);
	cPnt1 << controlPoints(1,0), controlPoints(1,1), controlPoints(1,2);
	cPnt2 << controlPoints(2,0), controlPoints(2,1), controlPoints(2,2);
	cPnt3 << controlPoints(3,0), controlPoints(3,1), controlPoints(3,2);
	cPnt4 << controlPoints(4,0), controlPoints(4,1), controlPoints(4,2);

	k0 =     cPnt0;
	k1 = - 4*cPnt0 +  4*cPnt1;
	k2 =   6*cPnt0 - 12*cPnt1 +  6*cPnt2;
	k3 = - 4*cPnt0 + 12*cPnt1 - 12*cPnt2 + 4*cPnt3;
	k4 =     cPnt0 -  4*cPnt1 +  6*cPnt2 - 4*cPnt3 + cPnt4;

	for(i = 0; i < nElements; i++){
		t =((double)i)/((double)(nElements-1));
		bez   =   k0 +   t*k1 +    pow(t,2)*k2 +   pow(t,3)*k3 +   pow(t,4)*k4;
		dBez  =   k1 + 2*t*k2 +  3*pow(t,2)*k3 + 4*pow(t,3)*k4;
		d2Bez = 2*k2 + 6*t*k3 + 12*pow(t,2)*k4;
		x.push_back( bez(0) );
		y.push_back( bez(1) );
		z.push_back( bez(2) );
		k.push_back( ( dBez(0)*d2Bez(1) - dBez(1)*d2Bez(0) ) / ( sqrt( pow( pow( dBez(0) ,2) + pow( dBez(1) ,2) ,3) ) ) );
	}
}

void constructBezier5to(int nElements, const Eigen::Matrix<double, 6, 3> &controlPoints, std::vector<double> & x, std::vector<double> & y, std::vector<double> & z, std::vector<double> & k){
	Eigen::Vector3d cPnt0, cPnt1, cPnt2, cPnt3, cPnt4, cPnt5;
	Eigen::Vector3d k0, k1, k2, k3, k4, k5;
	Eigen::Vector3d bez, dBez, d2Bez;
	Eigen::MatrixXd matrixOut(nElements, 4);

	int    i;
	double t;
	cPnt0 << controlPoints(0,0), controlPoints(0,1), controlPoints(0,2);
	cPnt1 << controlPoints(1,0), controlPoints(1,1), controlPoints(1,2);
	cPnt2 << controlPoints(2,0), controlPoints(2,1), controlPoints(2,2);
	cPnt3 << controlPoints(3,0), controlPoints(3,1), controlPoints(3,2);
	cPnt4 << controlPoints(4,0), controlPoints(4,1), controlPoints(4,2);
	cPnt5 << controlPoints(5,0), controlPoints(5,1), controlPoints(5,2);

	k0 =     cPnt0;
	k1 = - 5*cPnt0 +  5*cPnt1;
	k2 =  10*cPnt0 - 20*cPnt1 + 10*cPnt2;
	k3 = -10*cPnt0 + 30*cPnt1 - 30*cPnt2 + 10*cPnt3;
	k4 =   5*cPnt0 - 20*cPnt1 + 30*cPnt2 - 20*cPnt3 + 5*cPnt4;
	k5 = -   cPnt0 +  5*cPnt1 - 10*cPnt2 + 10*cPnt3 - 5*cPnt4 + cPnt5;

	x.clear(); y.clear(); z.clear(); k.clear();

	for(i = 0; i < nElements; i++){
		t = ((double)i)/((double)(nElements-1));
		bez   =   k0 +   t*k1 +    pow(t,2)*k2 +    pow(t,3)*k3 +   pow(t,4)*k4 + pow(t,5)*k5;
		dBez  =   k1 + 2*t*k2 +  3*pow(t,2)*k3 +  4*pow(t,3)*k4 + 5*pow(t,4)*k5;
		d2Bez = 2*k2 + 6*t*k3 + 12*pow(t,2)*k4 + 20*pow(t,3)*k5;
		x.push_back( bez(0) );
		y.push_back( bez(1) );
		z.push_back( bez(2) );
		k.push_back( ( dBez(0)*d2Bez(1) - dBez(1)*d2Bez(0) ) / ( sqrt( pow( pow( dBez(0) ,2) + pow( dBez(1) ,2) ,3) ) ) );
	}
}


} /* end of math */
} /* end of ad       */
} /* end of tri      */
