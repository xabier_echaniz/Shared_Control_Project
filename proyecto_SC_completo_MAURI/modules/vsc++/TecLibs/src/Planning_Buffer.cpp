//============================================================================
// Name        : ADCOREBufferClass.cpp
// Author      : RL - MM
// Version     : 0.1
// Description :
//============================================================================

#include "../inc/Planning_Buffer.hpp"

using tri::ad::math::distance2Points;
using tri::ad::math::distPointSegment;

namespace tri      {
namespace ad       {
namespace planning {

Buffer::Buffer(){}


Buffer::~Buffer(){}

unsigned int Buffer::getNumberPoints(){
    return this->nodes.size();
}


unsigned int Buffer::getNumberSegments(){
    return this->getNumberPoints()-1;
}


int    Buffer::sizeIsValid(){
    if(this->getNumberPoints() < 2){
        std::cout << ERROR0005 << "\n" << std::endl;
        return 0; // false
    }
    return 1; // true
}


void   Buffer::addNode(double x, double y, double z, double vx, double k){
    unsigned int lastIdx;
    double       angle;
    Eigen::Vector3d v0(0,0,0),v1(0,0,0);
    bufferNode node;
    node.x  = x;
    node.y  = y;
    node.z  = z;
    node.vx = vx;
    node.k  = k;

    // It is increased the size in 1 and the data is passed to this new element
    this->nodes.push_back(node);

    // Yaw Calculation
    if(this->getNumberPoints() == 1){ // if it is 0 means the buffer has 1 element
        this->nodes[0].yaw = 0;
    }else{
        lastIdx = this->getNumberPoints()-1;
        v0 << this->nodes[lastIdx - 1].x, this->nodes[lastIdx - 1].y, this->nodes[lastIdx - 1].z;
        v1 << this->nodes[lastIdx - 0].x, this->nodes[lastIdx - 0].y, this->nodes[lastIdx - 1].z;
        angle = tri::ad::math::angleVectorAroundZ(v1 - v0);

        this->nodes[lastIdx - 1].yaw = angle; // Yaw has1 element less
        this->nodes[lastIdx - 0].yaw = angle;
    }
}






double Buffer::shortDistPoint(const Eigen::Vector3d & point, unsigned int *idx){

    Eigen::VectorXd distance;
    unsigned int i;

    // When buffer invalid because not enough point to calculate distance
    if (!this->sizeIsValid()){
        *idx = 0;
        return NAN;
    }


    // Find min distance through buffer
    for(i = 0; i < this->getNumberSegments(); i++){
       double d = this->distSegmentIdx(point,i);
       std::cout << "distance: " << d << std::endl;
       tri::ad::math::addElementVectorX(distance, d);
    }

    return distance.minCoeff(&idx);
}

double Buffer::distPointIdx(const Eigen::Vector3d & point, unsigned int idx){

  Eigen::Vector3d bufferPoint(this->nodes[idx].x,this->nodes[idx].y,this->nodes[idx].z);

  return distance2Points(point,bufferPoint);

}

double Buffer::distSegmentIdx(const Eigen::Vector3d & point, unsigned int idx){

  Eigen::Vector3d bufferP0( this->nodes[idx].x,   this->nodes[idx].y,   this->nodes[idx].z   );
  Eigen::Vector3d bufferP1( this->nodes[idx+1].x, this->nodes[idx+1].y, this->nodes[idx+1].z );

  Eigen::Vector3d shortestPoint;

  return distPointSegment(bufferP0,bufferP1,point,&shortestPoint);

}


//void   Buffer::controlVariables(const Eigen::Vector3d * point){}
void   Buffer::controlVariables(const Eigen::Vector3d & point, double * dist, double * angleSegRad, double * curvature){
    unsigned int idx;

    *dist        = this->shortDistPoint(point, &idx);
    *angleSegRad = this->nodes[idx].yaw;
    *curvature   = this->nodes[idx].k;
}

void Buffer::print(){
    int i;
    std::cout << "\n" << std::endl;
    for(i = 0; i < (int)this->getNumberPoints(); i++){
        std::cout << "X["   << (i) << "]: " << this->nodes[i].x   << " ";
        std::cout << "Y["   << (i) << "]: " << this->nodes[i].y   << " ";
        std::cout << "Z["   << (i) << "]: " << this->nodes[i].z   << " ";
        std::cout << "Vx["  << (i) << "]: " << this->nodes[i].vx  << " ";
        std::cout << "K["   << (i) << "]: " << this->nodes[i].k   << " ";
        std::cout << "Yaw[" << (i) << "]: " << this->nodes[i].yaw << std::endl;
    }

}


} /* end of planning */
} /* end of ad       */
} /* end of tri      */
