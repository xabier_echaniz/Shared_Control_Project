/* ====================================================================== 
 * Name:        Lib_VehicleState.hpp
 * Description: Library of vehicle state, incorporates the information of
 *              input/output/configuration of the vehicle
 * Note:        - No notes - 
 * Author:      rayalejandro.lattarulo@tecnali.com 23/05/2017
 * Company:     Tecnalia
 * ======================================================================*/
#include "../inc/Lib_VehicleState.hpp"

/* ======================================================================
 * Classes function definition
 * ======================================================================*/
 /* Name:           (function) TRIClass_VehicleState (Constructor)
  * Description:    constructor
  * Note:           - no notes -
  * Author:         rayalejandro.lattarulo@tecnalia.com 24/05/2017 */
TRIClass_VehicleState::TRIClass_VehicleState(void){
 /*   this->i = ;
    this->o = ;
    this->c = ;*/
}

/* ======================================================================
 * Function definition
 * ======================================================================*/

