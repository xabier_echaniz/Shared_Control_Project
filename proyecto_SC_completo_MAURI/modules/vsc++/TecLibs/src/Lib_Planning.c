/*********************************************************************************************
 *** Name of the library:        CSV Management                                            ***
 *** Description of the library: This library is implemented to do the management of CSV   ***
 ***                             text files. All the procedures releated to them should be ***
 ***                             with the current library.                                 ***
 *** Autor notes:                TRI means Tecnalia Research and Innovation                ***
 *** Date:                       April 13th, 2016                                          ***
 *** Modifications date:         - - - - - - - - -                                         ***
 *** Version:                    V1.0                                                      ***
 *** Notes about modifications:                                                            ***
 *********************************************************************************************/

/*********************************************************************************************
 *** Libraries to be invoked                                                               ***
 *********************************************************************************************/

#include "../inc/Lib_Planning.h"

/*********************************************************************************************
 *** Functions                                                                             ***
 *********************************************************************************************/

int    TRI_Global_Planning(TRI_VehicleState *iVehState, tri_Planning *iPlanning, TRI_CSV_List *iPath, TRI_CSV_List **oGlobalPlanner){

    tri_point2D        Vehposition;
    tri_Planning_Track Track;
    
    TRI_CSV_List *tPath;
    int i;
    TRI_CSV_NodeInfo Node;
    
    printf("Global Planner: Inicio.\n");
    // Casos posibles:
        // 0:  Path con puntos.
        // 1:  Path con puntos haciendo loop.
        // 10: Path con dynamic generation
        // 11: Path con dynamic generation y loop
    Vehposition.x = iVehState->O.X;
    Vehposition.y = iVehState->O.Y;
    TRI_Path_LctPntPth(Vehposition, iPath, &Track, 1);

    iVehState->O.StartPointX = Track.oClosestPnt.x;
    iVehState->O.StartPointY = Track.oClosestPnt.y;
    iVehState->O.MaxSpeed    = Track.oMaxSpeed;

    iPlanning->O.StartPnt.x  = Track.oClosestPnt.x;
    iPlanning->O.StartPnt.y  = Track.oClosestPnt.y;
    iPlanning->O.StartId     = Track.oSegId;

    iPlanning->O.StartSeg.p[0].x = Track.oClosestSeg.p[0].x;
    iPlanning->O.StartSeg.p[0].y = Track.oClosestSeg.p[0].y;
    iPlanning->O.StartSeg.p[1].x = Track.oClosestSeg.p[1].x;
    iPlanning->O.StartSeg.p[1].y = Track.oClosestSeg.p[1].y;
    
    
    // Inicio 20 de julio 2016 - Introudccion nueva:
    Vehposition.x = iPath->Node.X;
    Vehposition.y = iPath->Node.Y;

    iPlanning->O.StartPnt.x  = iPath->Node.X;
    iPlanning->O.StartPnt.y  = iPath->Node.Y;
    iPlanning->O.StartId     = iPath->Node.Id;
    
    iPlanning->O.StartSeg.p[0].x = iPath->Node.X;
    iPlanning->O.StartSeg.p[0].y = iPath->Node.Y;
    iPlanning->O.StartSeg.p[1].x = iPath->Next->Node.X;
    iPlanning->O.StartSeg.p[1].y = iPath->Next->Node.Y;
    // Fin 20 de julio 2016 - Introudccion nueva:
    
    if(iPlanning->C.PathMode == (MPOINTSNOTLOOP) || iPlanning->C.PathMode == (MBEZIERNOTLOOP)){
        if(iPlanning->C.PathMode == (MPOINTSNOTLOOP)){printf("Global Planner: Generacion de ruta por puntos sin loop.\n");}
        if(iPlanning->C.PathMode == (MBEZIERNOTLOOP)){printf("Global Planner: Generacion de ruta por bezier sin loop.\n");}
        Vehposition.x = iPlanning->C.EndPnt.x;
        Vehposition.y = iPlanning->C.EndPnt.y;
        TRI_Path_LctPntPth(Vehposition, iPath, &Track, 1);
        iPlanning->O.EndPnt.x = Track.oClosestPnt.x;
        iPlanning->O.EndPnt.y = Track.oClosestPnt.y;
        if(Track.oSegId == TRI_SizeList(iPath)){
            iPlanning->O.EndId = 1;
        }else{
            iPlanning->O.EndId = Track.oSegId+1;
        }
        iPlanning->O.EndSeg.p[0].x = Track.oClosestSeg.p[0].x;
        iPlanning->O.EndSeg.p[0].y = Track.oClosestSeg.p[0].y;
        iPlanning->O.EndSeg.p[1].x = Track.oClosestSeg.p[1].x;
        iPlanning->O.EndSeg.p[1].y = Track.oClosestSeg.p[1].y;
    }
    if(iPlanning->C.PathMode == (MPOINTSLOOP) || iPlanning->C.PathMode == (MBEZIERLOOP)){
        // El punto final del path es el mismo que el del inicio. Los parametros Start los ubicamos en la funcion anterior.
        if(iPlanning->C.PathMode == (MPOINTSLOOP)){printf("Global Planner: Generacion de ruta por puntos con loop.\n");}
        if(iPlanning->C.PathMode == (MBEZIERLOOP)){printf("Global Planner: Generacion de ruta por bezier con loop.\n");}
        iPlanning->O.EndPnt.x  = iPlanning->O.StartPnt.x;
        iPlanning->O.EndPnt.y  = iPlanning->O.StartPnt.y;
        iPlanning->O.EndId     = iPlanning->O.StartId;
    }

    //Se procede a realizar el llenado del global planner
    tPath = iPath;
    tPath = TRI_ListPointerPosition(tPath, iPlanning->O.StartId);
    TRI_DeleteList(&(*oGlobalPlanner));
    iPlanning->O.TotalPoints = TRI_SizeList(iPath);
    i = iPlanning->O.StartId;
    //TRI_CSV_NodeInfo Node;
    while(i != iPlanning->O.EndId || *oGlobalPlanner == NULL){

        Node = TRI_ExtractNodeInfo(tPath->Node);
        if(i == iPlanning->O.TotalPoints){
            i = 1;
            tPath = iPath;
        }else{
            i = i + 1;
            tPath = tPath->Next;
        }

        if(iPlanning->C.PathMode == (MBEZIERNOTLOOP) || iPlanning->C.PathMode == (MBEZIERLOOP)){
            if(Node.Type != TURN && Node.Type != ROUNDABOUT && Node.Type != MERGING){
                printf("Global Planner: El nodo %d no es un tipo de punto valido.\n", Node.Id);
                TRI_DeleteList(&(*oGlobalPlanner));
                return -1;
            }

        }

        *oGlobalPlanner = TRI_AddNodeBottomList(*oGlobalPlanner, Node);
    }

    //En el caso de las trayectorias que tienen un fin, debemos agregar este ultimo punto.
    if(iPlanning->C.PathMode == (MPOINTSNOTLOOP) || iPlanning->C.PathMode == (MBEZIERNOTLOOP)){
        Node            = TRI_ExtractNodeInfo(tPath->Node);
        Node.X          = iPlanning->O.EndPnt.x; // este caso es especial se debe agregar el punto de fin (x).
        Node.Y          = iPlanning->O.EndPnt.y; // este caso es especial se debe agregar el punto de fin (y).
        *oGlobalPlanner = TRI_AddNodeBottomList(*oGlobalPlanner, Node);
    }

    // Realizado el llenado del mapa global y la incorporacion del ultimo punto en caso de aplicar,
    // se procede a rotar 90 grados hacia la derecha o hacia la izquierda dependiendo como se elija
    // el modo de conduccion.
    if(TRI_Rotate_Global_Planner(*iPlanning, &(*oGlobalPlanner)) < 0){ return -3;}
    // Se enumera nuevamente
    TRI_Enumerate_Global_Planner(&(*oGlobalPlanner));

    // Se imprime el mapa nuevo
    TRI_Log_GlobalPlanner(*oGlobalPlanner);

    iPlanning->O.TotalPoints = TRI_SizeList(*oGlobalPlanner);
    //Calcular la distancia total del recorrido
    iPlanning->O.PathLength = TRI_Path_Lenght((*iPlanning), *oGlobalPlanner);
    printf("Global Planner: Fin creacion de mapa global.\n");
    return 0;
}

int    TRI_Rotate_Global_Planner(tri_Planning iPlanning, TRI_CSV_List **iGlobalPlanner){
    // Declaracion de variables
    TRI_CSV_List *tGlobalPlanner    = NULL;
    TRI_CSV_List *tGlobalPlannerAux = NULL;
    TRI_CSV_NodeInfo tNode;
    double           tHeading1      = 0;
    double           tHeading2      = 0;
    double sign;
    tri_point2D   tPointPast, tPointCurrent, tPointFuture, tPoint;
    tri_segment2D tSeg0, tSeg1;
    double angle;
    
    if(*iGlobalPlanner == NULL){
        printf("Global Planner, rotating: La lista de puntos esta vacia por tanto no se puede rotar.");
        return -1;
    }

    if(TRI_SizeList(*iGlobalPlanner) == 1){
        printf("Global Planner, rotating: La lista de puntos contiene un unico punto.");
        return -2;
    }

    printf("Global Planner: Inicio la rotacion de los puntos del mapa.\n");
    tGlobalPlannerAux               = *iGlobalPlanner;

    tNode          = TRI_ExtractNodeInfo(tGlobalPlannerAux->Node);

    if(iPlanning.C.RightTraffic == 1){sign =  -1;}
    else                             {sign =   1;}

    tNode             = TRI_ExtractNodeInfo(tGlobalPlannerAux->Node);
    tPointPast.x      = tGlobalPlannerAux->Node.X;
    tPointPast.y      = tGlobalPlannerAux->Node.Y;
    tGlobalPlannerAux = tGlobalPlannerAux->Next;
    tPointCurrent.x   = tGlobalPlannerAux->Node.X;
    tPointCurrent.y   = tGlobalPlannerAux->Node.Y;
    tHeading1         = atan2(tPointCurrent.y - tPointPast.y, tPointCurrent.x - tPointPast.x);
    tPoint            = TRI_RotatePoint90DegreesOffset(tPointPast, tHeading1, sign*iPlanning.C.RoadWidth/2);
    tNode.X           = tPoint.x;
    tNode.Y           = tPoint.y;
    tGlobalPlanner    = TRI_AddNodeBottomList(tGlobalPlanner, tNode);
    tPointCurrent.x   = tPointPast.x;
    tPointCurrent.y   = tPointPast.y;

    while(tGlobalPlannerAux->Next != NULL){

        if( ( iPlanning.C.PathMode == (MBEZIERLOOP) || iPlanning.C.PathMode == (MBEZIERNOTLOOP) ) && tGlobalPlannerAux->Node.Type == ROUNDABOUT){ // el tPath es el de la siguiente iteracion (futuro).
            tri_circle  circle;
            tri_point2D Entrance, End;

            circle.center.x = tGlobalPlannerAux->Node.X;
            circle.center.y = tGlobalPlannerAux->Node.Y;
            circle.radius   = (float) 1.0f/tGlobalPlannerAux->Node.Curvature + iPlanning.C.RoadWidth/2.0f;

            tPointPast.x    = tPointCurrent.x;
            tPointPast.y    = tPointCurrent.y;
            tPointCurrent.x = tGlobalPlannerAux->Node.X;
            tPointCurrent.y = tGlobalPlannerAux->Node.Y;
            tPointFuture.x  = tGlobalPlannerAux->Next->Node.X;
            tPointFuture.y  = tGlobalPlannerAux->Next->Node.Y;
            tHeading1       = atan2(tPointCurrent.y - tPointPast.y    , tPointCurrent.x - tPointPast.x   );
            tHeading2       = atan2(tPointFuture.y  - tPointCurrent.y , tPointFuture.x  - tPointCurrent.x);
            
            printf("Global Planner: rotonda (%lf,%lf).\n", tPointCurrent.x, tPointCurrent.y);
            
            if(tNode.Type == MERGING){
                tSeg0.p[0] = tPointPast;  // Cuando el punto anterior eera de merging no se rota
            }else{
                tSeg0.p[0]      = TRI_RotatePoint90DegreesOffset(tPointPast   , tHeading1, sign*iPlanning.C.RoadWidth/2);
            }
            tSeg0.p[1]      = TRI_RotatePoint90DegreesOffset(tPointCurrent, tHeading1, sign*iPlanning.C.RoadWidth/2);
            tSeg1.p[0]      = TRI_RotatePoint90DegreesOffset(tPointCurrent, tHeading2, sign*iPlanning.C.RoadWidth/2);
            tSeg1.p[1]      = TRI_RotatePoint90DegreesOffset(tPointFuture , tHeading2, sign*iPlanning.C.RoadWidth/2);

            //Calculo del punto de entrada
            if(TRI_Intersection_segment_circle(tSeg0, circle, &Entrance) < 0){
                printf("Global Planner: El nodo %d tiene un roundabout sin interseccion a la entrada.\n", tGlobalPlannerAux->Node.Id);
                TRI_DeleteList(&(*iGlobalPlanner));
                return -2;
            }
            tSeg0.p[0].x = tSeg0.p[1].x + 2*circle.radius*cos(atan2(Entrance.y - tSeg0.p[1].y, Entrance.x - tSeg0.p[1].x) + tGlobalPlannerAux->Node.Angle_In*M_PI/180);
            tSeg0.p[0].y = tSeg0.p[1].y + 2*circle.radius*sin(atan2(Entrance.y - tSeg0.p[1].y, Entrance.x - tSeg0.p[1].x) + tGlobalPlannerAux->Node.Angle_In*M_PI/180);
            if(TRI_Intersection_segment_circle(tSeg0, circle, &Entrance) < 0){
                printf("Global Planner: El nodo %d tiene un roundabout sin interseccion a la entrada (en el angulo).\n", tGlobalPlannerAux->Node.Id);
                TRI_DeleteList(&(*iGlobalPlanner));
                return -2;
            }

            //Calculo del punto de salida
            if(TRI_Intersection_segment_circle(tSeg1, circle, &End) < 0){
                printf("Global Planner: El nodo %d tiene un roundabout sin interseccion a la salida.\n", tGlobalPlannerAux->Node.Id);
                TRI_DeleteList(&(*iGlobalPlanner));
                return -2;
            }
            tSeg1.p[1].x = tSeg1.p[0].x + 2*circle.radius*cos(atan2(End.y - tSeg1.p[0].y, End.x - tSeg1.p[0].x) - tGlobalPlannerAux->Node.Angle_Out*M_PI/180);
            tSeg1.p[1].y = tSeg1.p[0].y + 2*circle.radius*sin(atan2(End.y - tSeg1.p[0].y, End.x - tSeg1.p[0].x) - tGlobalPlannerAux->Node.Angle_Out*M_PI/180);
            if(TRI_Intersection_segment_circle(tSeg1, circle, &End) < 0){
                printf("Global Planner: El nodo %d tiene un roundabout sin interseccion a la salida (en el angulo).\n", tGlobalPlannerAux->Node.Id);
                TRI_DeleteList(&(*iGlobalPlanner));
                return -2;
            }

            // Carga del punto de entrada
            tNode   = TRI_ExtractNodeInfo(tGlobalPlannerAux->Node);
            tNode.X = Entrance.x;
            tNode.Y = Entrance.y;
            tNode.Curvature = 1/circle.radius;
            tNode.Type = RBTENTRANCE;
            tGlobalPlanner = TRI_AddNodeBottomList(tGlobalPlanner, tNode);

            //Carga de los puntos del medio
            angle = atan2( Entrance.y - circle.center.y, Entrance.x - circle.center.x);
            while(sqrt(pow(tNode.X - End.x,2) + pow(tNode.Y - End.y,2)) > 3.0){
                angle = angle + (-1*sign)*2.5/circle.radius;
                tNode.X = circle.center.x + (circle.radius)*cos(angle);
                tNode.Y = circle.center.y + (circle.radius)*sin(angle);
                tNode.Type = RBTMIDDLE;
                tGlobalPlanner = TRI_AddNodeBottomList(tGlobalPlanner, tNode);
            }

            // Carga del punto de salida, se deja para la iteracion (ultima parte).
            tNode.X = End.x;
            tNode.Y = End.y;
            tNode.Type = RBTEXIT;
        }

        if( ( iPlanning.C.PathMode == (MBEZIERLOOP) || iPlanning.C.PathMode == (MBEZIERNOTLOOP) ) && (tGlobalPlannerAux->Node.Type == TURN || tGlobalPlannerAux->Node.Type == MERGING)){
            tPointPast.x    = tPointCurrent.x;
            tPointPast.y    = tPointCurrent.y;
            tPointCurrent.x = tGlobalPlannerAux->Node.X;
            tPointCurrent.y = tGlobalPlannerAux->Node.Y;
            tPointFuture.x  = tGlobalPlannerAux->Next->Node.X;
            tPointFuture.y  = tGlobalPlannerAux->Next->Node.Y;
            tHeading1       = atan2(tPointCurrent.y - tPointPast.y    , tPointCurrent.x - tPointPast.x   );
            tHeading2       = atan2(tPointFuture.y  - tPointCurrent.y , tPointFuture.x  - tPointCurrent.x);
            if(tNode.Type == MERGING){
                tSeg0.p[0] = tPointPast; // Cuando el punto anterior eera de merging no se rota
            }else{
                tSeg0.p[0]      = TRI_RotatePoint90DegreesOffset(tPointPast   , tHeading1, sign*iPlanning.C.RoadWidth/2);
            }
            tSeg0.p[1]      = TRI_RotatePoint90DegreesOffset(tPointCurrent, tHeading1, sign*iPlanning.C.RoadWidth/2);
            tSeg1.p[0]      = TRI_RotatePoint90DegreesOffset(tPointCurrent, tHeading2, sign*iPlanning.C.RoadWidth/2);
            tSeg1.p[1]      = TRI_RotatePoint90DegreesOffset(tPointFuture , tHeading2, sign*iPlanning.C.RoadWidth/2);
            (int) TRI_Lines_Intersected(tSeg0, tSeg1, &tPoint);
            tNode           = TRI_ExtractNodeInfo(tGlobalPlannerAux->Node);
            tNode.X         = tPoint.x; // tPoint se refiere al punto de interseccion de las dos rectas
            tNode.Y         = tPoint.y;

            if(tGlobalPlannerAux->Node.Type == MERGING){
                printf("Global Planner: merging (%lf,%lf).\n", tPointCurrent.x, tPointCurrent.y);
                
                // Se carga el punto de la interseccion
                tGlobalPlanner    = TRI_AddNodeBottomList(tGlobalPlanner, tNode);
                //tGlobalPlannerAux = tGlobalPlannerAux->Next;

                // Generacion del punto de merging
                tHeading1 = atan2(tPointFuture.y - tPoint.y, tPointFuture.x - tPoint.x);
                if(tNode.Curvature > 0){ // el valor de curvatura tambien se refiere al valor de desplazamiento
                    tHeading1 = tHeading1 - M_PI/2;
                }else{
                    tHeading1 = tHeading1 + M_PI/2;
                }
                tPointCurrent.x = tPoint.x + fabs(tNode.Curvature)*cos(tHeading1);
                tPointCurrent.y = tPoint.y + fabs(tNode.Curvature)*sin(tHeading1);
                tNode.X         = tPointCurrent.x;
                tNode.Y         = tPointCurrent.y;
            } else{
                printf("Global Planner: interseccion (%lf,%lf).\n", tPointCurrent.x, tPointCurrent.y);
            }

        }

        tGlobalPlanner    = TRI_AddNodeBottomList(tGlobalPlanner, tNode);
        tGlobalPlannerAux = tGlobalPlannerAux->Next;
    }

    tNode = TRI_ExtractNodeInfo(tGlobalPlannerAux->Node);
    tPointCurrent.x = tNode.X;
    tPointCurrent.y = tNode.Y;
    tPoint = TRI_RotatePoint90DegreesOffset(tPointCurrent, tHeading2, sign*iPlanning.C.RoadWidth/2);
    tNode.X         = tPoint.x;
    tNode.Y         = tPoint.y;
    if(( iPlanning.C.PathMode == (MBEZIERNOTLOOP)) || ( iPlanning.C.PathMode == (MPOINTSNOTLOOP))){tNode.Type = PATHEND;}
    tGlobalPlanner = TRI_AddNodeBottomList(tGlobalPlanner, tNode);
    printf("Global Planner: Culmino la rotacion de los puntos del mapa.\n");
    TRI_DeleteList(&(*iGlobalPlanner));
    *iGlobalPlanner = tGlobalPlanner;
    return 0;
}

void   TRI_Enumerate_Global_Planner(TRI_CSV_List **iGlobalPlanner){
    TRI_CSV_List *tGlobalPlanner = *iGlobalPlanner;
    int i = 1;
    while(tGlobalPlanner != NULL){
        tGlobalPlanner->Node.Id = i;
        i = i + 1;
        tGlobalPlanner = tGlobalPlanner->Next;
    }
}

void   TRI_Log_GlobalPlanner(TRI_CSV_List *iGlobalPlanner){

    time_t timer;
    char   buffer[26];
    char   Location[50];
    struct tm* tm_info;
    TRI_CSV_List *tGlobalPlanner = iGlobalPlanner;
    time(&timer);
    tm_info = localtime(&timer);
    strftime(buffer, 26, "%Y_%m_%d_%H %M %S", tm_info);
    strcpy(Location, "data/logs/Log");
    strcat(Location, buffer);
    strcat(Location, "GlobalPlanner.csv");

    if(iGlobalPlanner == NULL){
        printf("Global Planner: No se puede imprimir el Log, mapa vacio.\n");
    }else{
        FILE *fp = fopen(Location,"w");
        if(fp == NULL){
            printf("Global Planner: Error al crear el archivo de Log.\n");
        }else{
            printf("Global Planner: Se creo con exito el archivo de Log.\n");
            tGlobalPlanner = iGlobalPlanner;
            fprintf(fp,"Id;X;Y;Max Speed;Type;Curvature;Angle in; Angle out;Di0;Di1;Di2;Di3;Di4;Di5;\n");
            while(tGlobalPlanner != NULL){
                fprintf(fp,"%d;" ,tGlobalPlanner->Node.Id);
                fprintf(fp,"%lf;",tGlobalPlanner->Node.X);
                fprintf(fp,"%lf;",tGlobalPlanner->Node.Y);
                fprintf(fp,"%lf;",tGlobalPlanner->Node.MaxSpeed);
                fprintf(fp,"%d;" ,tGlobalPlanner->Node.Type);
                fprintf(fp,"%lf;",tGlobalPlanner->Node.Curvature);
                fprintf(fp,"%lf;",tGlobalPlanner->Node.Angle_In);
                fprintf(fp,"%lf;",tGlobalPlanner->Node.Angle_Out);
                fprintf(fp,"%lf;",tGlobalPlanner->Node.Di[0]);
                fprintf(fp,"%lf;",tGlobalPlanner->Node.Di[1]);
                fprintf(fp,"%lf;",tGlobalPlanner->Node.Di[2]);
                fprintf(fp,"%lf;",tGlobalPlanner->Node.Di[3]);
                fprintf(fp,"%lf;",tGlobalPlanner->Node.Di[4]);
                fprintf(fp,"%lf;\n",tGlobalPlanner->Node.Di[5]);
                tGlobalPlanner = tGlobalPlanner->Next;
            }
        }
        fclose(fp);
    }
}

void   TRI_Path_LctPntPth(tri_point2D iVehposition, TRI_CSV_List *iPath, tri_Planning_Track *iTrack, int loop){
    TRI_CSV_List *pointerlist  = iPath;
    tri_segment2D segment, segmentBefore;
    tri_point2D   position, closestpointaux;

    int    i = 1;
    double mindistance  = 30e32;
    double distancetemp = 0;
    int    limit;
    double segment_speed;
    
    // Initialize variables
    position.x   = iVehposition.x;
    position.y   = iVehposition.y;

    // Se fija el limite maximo para iterar los puntos de la ruta.
    if(loop == 1){

        limit = TRI_SizeList(iPath);
    }else{
        limit = (TRI_SizeList(iPath)-1);
    }

    // Se actualiza el segmento pasado

    segmentBefore.p[0].x = pointerlist->Node.X;
    segmentBefore.p[0].y = pointerlist->Node.Y;
    segmentBefore.p[1].x = pointerlist->Next->Node.X;
    segmentBefore.p[1].y = pointerlist->Next->Node.Y;

    //Se iteran los puntos de la ruta para conseguir el punto de inicio.
    while(i<=limit){
        // Segmento pasado es el presente de la iteracion pasada

        segmentBefore.p[0].x = segment.p[0].x;
        segmentBefore.p[0].y = segment.p[0].y;
        segmentBefore.p[1].x = segment.p[1].x;
        segmentBefore.p[1].y = segment.p[1].y;

        //Generacion del segmento a evaluar
        segment.p[0].x = pointerlist->Node.X;
        segment.p[0].y = pointerlist->Node.Y;
        segment_speed  = pointerlist->Node.MaxSpeed;
        if(loop == 1 && i == limit){
            //Si estamos en un loop el ultimo nodo se evalua con el primero.
            segment.p[1].x = iPath->Node.X;
            segment.p[1].y = iPath->Node.Y;
        }else{
            //Si no estamos en loop o aun no llegamos al ultimo elemento de la lista estando en loop, seguimos el proceso normal
            segment.p[1].x = pointerlist->Next->Node.X;
            segment.p[1].y = pointerlist->Next->Node.Y;
        }

        //Distancia del punto de origen del coche al segmento
        distancetemp   = TRI_Distance_point_segment(position, segment, &closestpointaux);
        //Distancia minima
        if(distancetemp < mindistance){
            iTrack->oSegId             = pointerlist->Node.Id;
            iTrack->oClosestPnt.x      = closestpointaux.x;
            iTrack->oClosestPnt.y      = closestpointaux.y;
            iTrack->oClosestSeg.p[0].x = segment.p[0].x;
            iTrack->oClosestSeg.p[0].y = segment.p[0].y;
            iTrack->oClosestSeg.p[1].x = segment.p[1].x;
            iTrack->oClosestSeg.p[1].y = segment.p[1].y;
            iTrack->oHeadingBefore     = atan2(segmentBefore.p[1].y - segmentBefore.p[0].y, segmentBefore.p[1].x - segmentBefore.p[0].x);
            iTrack->oHeadingAfter      = atan2(segment.p[1].y - segment.p[0].y, segment.p[1].x - segment.p[0].x);
            iTrack->oMaxSpeed          = segment_speed;
            iTrack->oMaxCurvature      = pointerlist->Node.Curvature;
            mindistance                = distancetemp;
        }

        //Aumento del puntero, si llegamos al final, se retorna el puntero al inicio
        if(loop == 1 && i == limit){
            pointerlist = iPath;
        }else{
            pointerlist = pointerlist->Next;
        }

        // Incremento del contador

        i = i+1;
    }
 }

TRI_CSV_List *TRI_Path_LCTPntPthPointer(tri_point2D iVehposition, TRI_CSV_List *iPath, tri_Planning_Track *iTrack, int loop){
    TRI_CSV_List *pointerlist  = iPath;
    TRI_CSV_List *opointerlist = pointerlist;
    tri_segment2D segment;
    tri_point2D   position, closestpointaux;

    int    i = 1;
    double mindistance  = 30e32;
    double distancetemp = 0;
    int    limit;
    double segment_speed;
    
    // Initialize variables
    position.x   = iVehposition.x;
    position.y   = iVehposition.y;

    // Se fija el limite maximo para iterar los puntos de la ruta.
    if(loop == 1){

        limit = TRI_SizeList(iPath);
    }else{
        limit = (TRI_SizeList(iPath)-1);
    }
    //Se iteran los puntos de la ruta para conseguir el punto de inicio.
    while(i<=limit){
        //printf("%d     %d\n",i, limit);
        //Generacion del segmento a evaluar
        segment.p[0].x = pointerlist->Node.X;
        segment.p[0].y = pointerlist->Node.Y;
        segment_speed  = pointerlist->Node.MaxSpeed;
        if(loop == 1 && i == limit){
            //Si estamos en un loop el ultimo nodo se evalua con el primero.
            segment.p[1].x = iPath->Node.X;
            segment.p[1].y = iPath->Node.Y;
        }else{
            //Si no estamos en loop o aun no llegamos al ultimo elemento de la lista estando en loop, seguimos el proceso normal
            segment.p[1].x = pointerlist->Next->Node.X;
            segment.p[1].y = pointerlist->Next->Node.Y;
        }

        //Distancia del punto de origen del coche al segmento
        distancetemp   = TRI_Distance_point_segment(position, segment, &closestpointaux);
        //Distancia minima
        if(distancetemp < mindistance){
            iTrack->oSegId             = pointerlist->Node.Id;
            iTrack->oClosestPnt.x      = closestpointaux.x;
            iTrack->oClosestPnt.y      = closestpointaux.y;
            iTrack->oClosestSeg.p[0].x = segment.p[0].x;
            iTrack->oClosestSeg.p[0].y = segment.p[0].y;
            iTrack->oClosestSeg.p[1].x = segment.p[1].x;
            iTrack->oClosestSeg.p[1].y = segment.p[1].y;
            iTrack->oMaxSpeed          = segment_speed;
            iTrack->oMaxCurvature      = pointerlist->Node.Curvature;
            opointerlist               = pointerlist;
            mindistance                = distancetemp;
        }

        //Aumento del puntero, si llegamos al final, se retorna el puntero al inicio
        if(loop == 1 && i == limit){
            pointerlist = iPath;
        }else{
            pointerlist = pointerlist->Next;
        }

        // Incremento del contador

        i = i+1;
    }
    return opointerlist;
 }

double TRI_Path_Lenght   (tri_Planning iPlanning, TRI_CSV_List *iPath){
    TRI_CSV_List *tPath = iPath;
    int started;
    int i       = 1; //
    int mapsize = TRI_SizeList(iPath);
    tri_segment2D V1, V2;

    // Se coloca el apuntador en el lugar del path donde se esta actualmente
    tPath = TRI_ListPointerPosition(iPath, iPlanning.O.StartId);

    // Se verifica si el punto de fin esta en el mismo path que el de inicio
    if(iPlanning.O.EndId == iPlanning.O.StartId){
        // Se saca el producto punto del starting point con el punto siguiente del path y del punto de start con el de llegada
        // para verificar, si es positivo el punto de fin esta delante. Si es negativo esta detras.
        double dotproduct;
        V1.p[0].x = iPlanning.O.StartPnt.x;
        V1.p[0].y = iPlanning.O.StartPnt.y;
        if(iPlanning.O.StartId == mapsize){
            // si estoy en el ultimo elemento
            V1.p[1].x = iPath->Node.X;
            V1.p[1].y = iPath->Node.Y;
        }else{
            // no estoy en el ultimo elemento
            V1.p[1].x = tPath->Next->Node.X;
            V1.p[1].y = tPath->Next->Node.Y;
        }
        V2.p[0].x = iPlanning.O.StartPnt.x;
        V2.p[0].y = iPlanning.O.StartPnt.y;
        V2.p[1].x = iPlanning.O.EndPnt.x;
        V2.p[1].y = iPlanning.O.EndPnt.y;
        dotproduct = TRI_Dot_product(&V1,&V2);

        iPlanning.O.PathLength = sqrt(pow(iPlanning.O.EndPnt.x - iPlanning.O.StartPnt.x,2) + pow(iPlanning.O.EndPnt.y - iPlanning.O.StartPnt.y,2));
        if(dotproduct>0 && iPlanning.O.PathLength>0){
            // Esta delante
            return iPlanning.O.PathLength;
        }
    }
    iPlanning.O.PathLength = 0;
    started = 0;
    i = iPlanning.O.StartId;
    while(!(started == 1  && iPlanning.O.EndId == i)){ // mientras started sea distinto de 1
        if(started == 0){
            V1.p[0].x = iPlanning.O.StartPnt.x;
            V1.p[0].y = iPlanning.O.StartPnt.y;
            started   = 1;
        }else{
            V1.p[0].x = tPath->Node.X;
            V1.p[0].y = tPath->Node.Y;
        }

        if(tPath->Next ==  NULL){
            i = 1;
            tPath = iPath;
        }else{
            i = i + 1;
            tPath = tPath->Next;
        }
        V1.p[1].x = tPath->Node.X;
        V1.p[1].y = tPath->Node.Y;
        iPlanning.O.PathLength = iPlanning.O.PathLength + sqrt(pow(V1.p[0].x - V1.p[1].x,2)+pow(V1.p[0].y - V1.p[1].y,2));
    }
    iPlanning.O.PathLength = iPlanning.O.PathLength + sqrt(pow(tPath->Node.X - iPlanning.O.EndPnt.x,2 )+pow(tPath->Node.Y - iPlanning.O.EndPnt.y,2));
    return iPlanning.O.PathLength;

}

int    TRI_Local_Planning(TRI_VehicleState *iVehState, tri_Planning *iPlanning, TRI_CSV_List *oGlobalPlanner, TRI_LocalPlanner_List **oLocalPlanner){
    // NOTA 01: Se parte de que el global planner debe estar organizado,
    // es decir inicia en el segmento donde se encuentra el coche.

    // NOTA 02: Si los path se montan uno sobre el otro en el round about, se da una vuelta adicional.

    // NOTA 03: no se resuelve solapamiento en intersecciones.

    TRI_CSV_List          *tGlobalPlanner = oGlobalPlanner;
    TRI_LocalPlanner_List *tLocalPlanner  = NULL;
    tri_point2D tPastPoint, tPresentPoint,tFuturePoint;
    tri_segment2D tSeg1, tSeg2;
    tri_circle    tCircleAux, tRndbt;
    tri_point2D P0, P1, P2, P3, P4, P5;
    TRI_LocalPlanner_NodeInfo Node;
    double curvatureSign = 1;
    double nBez = 15;
    double i = 0;
    double t;
    double tSign;
    tri_point2D tAuxPoint;
    
    if(oGlobalPlanner       == NULL){return -1;}
    if(oGlobalPlanner->Next == NULL){return -2;}
    
    tPresentPoint.x = tGlobalPlanner->Node.X;
    tPresentPoint.y = tGlobalPlanner->Node.Y;
    Node            = TRI_LocalPlanner_ExtractNodeCSV(tGlobalPlanner->Node);
    tLocalPlanner   = TRI_LocalPlanner_AddNodeBottomList(tLocalPlanner, Node);
    tGlobalPlanner  = tGlobalPlanner->Next;
    while(tGlobalPlanner != NULL){
        if(tGlobalPlanner->Next == NULL && iPlanning->C.PathMode == MBEZIERNOTLOOP){
            Node = TRI_LocalPlanner_ExtractNodeCSV(tGlobalPlanner->Node);
            Node.Curvature = 0;
            tLocalPlanner  = TRI_LocalPlanner_AddNodeBottomList(tLocalPlanner, Node);
            break;
        }
        if(tGlobalPlanner->Node.Type == TURN || tGlobalPlanner->Node.Type == MERGING){
            Node            = TRI_LocalPlanner_ExtractNodeCSV(tGlobalPlanner->Node);
            tPastPoint      = tPresentPoint;
            tPresentPoint.x = Node.X;
            tPresentPoint.y = Node.Y;
            if(tGlobalPlanner->Next == NULL){
                tFuturePoint.x  = oGlobalPlanner->Node.X;
                tFuturePoint.y  = oGlobalPlanner->Node.Y;
            }else{
                tFuturePoint.x  = tGlobalPlanner->Next->Node.X;
                tFuturePoint.y  = tGlobalPlanner->Next->Node.Y;
            }

            // Creacion de los puntos
            tSeg1.p[0].x = tPresentPoint.x;
            tSeg1.p[0].y = tPresentPoint.y;
            tSeg1.p[1].x = tPastPoint.x;
            tSeg1.p[1].y = tPastPoint.y;
            
            if(tGlobalPlanner->Node.Type == MERGING){
                printf("Local planner: Nodo merging\n");
                tGlobalPlanner  = tGlobalPlanner->Next;                
                Node = TRI_LocalPlanner_ExtractNodeCSV(tGlobalPlanner->Node);
                if(Node.Type != MERGING){
                    printf("Local Planner: Error el merging solo se le asigno 1 punto. \n");
                    break;
                }
                tPastPoint      = tPresentPoint;
                tPresentPoint.x = Node.X;
                tPresentPoint.y = Node.Y;
                if(tGlobalPlanner->Next == NULL){
                    tFuturePoint.x  = oGlobalPlanner->Node.X;
                    tFuturePoint.y  = oGlobalPlanner->Node.Y;
                }else{
                    tFuturePoint.x  = tGlobalPlanner->Next->Node.X;
                    tFuturePoint.y  = tGlobalPlanner->Next->Node.Y;
                }
                tSeg2.p[0].x = tPresentPoint.x;
                tSeg2.p[0].y = tPresentPoint.y;
                tSeg2.p[1].x = tFuturePoint.x;
                tSeg2.p[1].y = tFuturePoint.y;
            }else{
                printf("Local planner: Nodo interseccion\n");
                tSeg2.p[0].x = tPresentPoint.x;
                tSeg2.p[0].y = tPresentPoint.y;
                tSeg2.p[1].x = tFuturePoint.x;
                tSeg2.p[1].y = tFuturePoint.y;
            }
            
            P0 = TRI_SetPointBasedSegment(tSeg1, tGlobalPlanner->Node.Di[0]);
            P1 = TRI_SetPointBasedSegment(tSeg1, tGlobalPlanner->Node.Di[1]);
            P2 = TRI_SetPointBasedSegment(tSeg1, tGlobalPlanner->Node.Di[2]);
            P3 = TRI_SetPointBasedSegment(tSeg2, tGlobalPlanner->Node.Di[3]);
            P4 = TRI_SetPointBasedSegment(tSeg2, tGlobalPlanner->Node.Di[4]);
            P5 = TRI_SetPointBasedSegment(tSeg2, tGlobalPlanner->Node.Di[5]);
            nBez = TRI_NBezier_Points(P0, P1, P2, P3, P4, P5);

            i = 0;
            while(i<=(nBez-1)){
                t = i/(nBez -1);
                if(nBez <= 1){ t = 0; }
                tPastPoint = tPresentPoint;
                tPresentPoint = TRI_Bezier5to(P0, P1, P2, P3, P4, P5, t);
                Node.X     = tPresentPoint.x;
                Node.Y     = tPresentPoint.y;
                if(i == 0 || i == (nBez-1)){
                    Node.Curvature = 0;
                }else{
                    t = (i+1)/(nBez -1);
                    tFuturePoint  = TRI_Bezier5to(P0, P1, P2, P3, P4, P5, t);
                    curvatureSign = TRI_CurvatureSign(tPastPoint, tPresentPoint, tFuturePoint);
                    TRI_Circle_Fit(tPastPoint, tPresentPoint, tFuturePoint, &tCircleAux);
                    Node.Curvature = curvatureSign*1/tCircleAux.radius;
                }
                if(nBez == 1){ // Si se tiene un solo punto se mantiene la curvatura del global planner
                    Node.Curvature = tGlobalPlanner->Node.Curvature;
                }
                tLocalPlanner = TRI_LocalPlanner_AddNodeBottomList(tLocalPlanner, Node);
                i = i+1;
            }
        }
        

        if(tGlobalPlanner->Node.Type == RBTENTRANCE){
            double tAuxDistanceInRBT;
            double tAngle;

            printf("Local planner: Nodo rotonda\n");
            // El roundabout debe tener por lo menos 3 puntos para hacer la aproximacion a circunferencia.
            // Si no se tienen 3 puntos dara error.

            // Signo para el giro.
            if(iPlanning->C.RightTraffic == 1){
                tSign = 1;
            }else{
                tSign = -1;
            }

            // Se obtiene el centro y radio de la rotonda.
            tPastPoint      = tPresentPoint;
            Node            = TRI_LocalPlanner_ExtractNodeCSV(tGlobalPlanner->Node);
            tPresentPoint.x = Node.X;
            tPresentPoint.y = Node.Y;
            tFuturePoint.x  = tGlobalPlanner->Next->Node.X;
            tFuturePoint.y  = tGlobalPlanner->Next->Node.Y;
            tAuxPoint.x     = tGlobalPlanner->Next->Next->Node.X;
            tAuxPoint.y     = tGlobalPlanner->Next->Next->Node.Y;
            TRI_Circle_Fit(tPresentPoint, tFuturePoint, tAuxPoint, &tRndbt);

            // Se realiza la generacion de la trayectoria de bezier para la entrada.
            // primero se debe calcular la ubicacion de los puntos.

            tSeg1.p[0].x = tPresentPoint.x;
            tSeg1.p[0].y = tPresentPoint.y;
            tSeg1.p[1].x = tPastPoint.x;
            tSeg1.p[1].y = tPastPoint.y;

            tSeg2.p[0].x = tRndbt.center.x + tRndbt.radius * cos(atan2(tPresentPoint.y - tRndbt.center.y,tPresentPoint.x - tRndbt.center.x) + tSign*tGlobalPlanner->Node.Di[5]/tRndbt.radius); // el punto presente es la entrada al roundabout.
            tSeg2.p[0].y = tRndbt.center.y + tRndbt.radius * sin(atan2(tPresentPoint.y - tRndbt.center.y,tPresentPoint.x - tRndbt.center.x) + tSign*tGlobalPlanner->Node.Di[5]/tRndbt.radius); // el punto presente es la entrada al roundabout.
            tSeg2.p[1].x = tRndbt.center.x + tRndbt.radius * cos(atan2(tPresentPoint.y - tRndbt.center.y,tPresentPoint.x - tRndbt.center.x) + tSign*(tGlobalPlanner->Node.Di[5] - 0.01)/tRndbt.radius); // el punto presente es la entrada al roundabout.
            tSeg2.p[1].y = tRndbt.center.y + tRndbt.radius * sin(atan2(tPresentPoint.y - tRndbt.center.y,tPresentPoint.x - tRndbt.center.x) + tSign*(tGlobalPlanner->Node.Di[5] - 0.01)/tRndbt.radius); // el punto presente es la entrada al roundabout.
            P0 = TRI_SetPointBasedSegment(tSeg1, tGlobalPlanner->Node.Di[0]);
            P1 = TRI_SetPointBasedSegment(tSeg1, tGlobalPlanner->Node.Di[1]);
            P2 = TRI_SetPointBasedSegment(tSeg1, tGlobalPlanner->Node.Di[2]);
            P3 = TRI_SetPointBasedSegment(tSeg2, tGlobalPlanner->Node.Di[3]);
            P4 = TRI_SetPointBasedSegment(tSeg2, tGlobalPlanner->Node.Di[4]);
            P5 = tSeg2.p[0];

            nBez = TRI_NBezier_Points(P0, P1, P2, P3, P4, P5);

            i = 0;
            while(i<=(nBez-1)){
                t = i/(nBez -1);
                if(nBez <= 1){ t = 0; }
                tPastPoint = tPresentPoint;
                tPresentPoint = TRI_Bezier5to(P0, P1, P2, P3, P4, P5, t);
                Node.X     = tPresentPoint.x;
                Node.Y     = tPresentPoint.y;
                if(i == 0){
                    Node.Curvature = 0;
                }else{
                    if(i == (nBez-1)){
                        Node.Curvature = curvatureSign*1/tRndbt.radius;
                    }else{
                        t = (i+1)/(nBez -1);
                        tFuturePoint = TRI_Bezier5to(P0, P1, P2, P3, P4, P5, t);
                        curvatureSign = TRI_CurvatureSign(tPastPoint, tPresentPoint, tFuturePoint);
                        TRI_Circle_Fit(tPastPoint, tPresentPoint, tFuturePoint, &tCircleAux);
                        Node.Curvature = curvatureSign*1/tCircleAux.radius;
                    }
                }
                tLocalPlanner = TRI_LocalPlanner_AddNodeBottomList(tLocalPlanner, Node);
                i = i+1;
            }

            // llevamos el apuntador hasta el fin de la rotonda.
            while(tGlobalPlanner->Node.Type != RBTEXIT){
                tGlobalPlanner  = tGlobalPlanner->Next;
            }

            // Se define el punto de salida P0 bezier, para poder generar los puntos intermedios de la rotonda.
            tAuxDistanceInRBT = 0.3;
            tAngle = atan2(P5.y - tRndbt.center.y,P5.x - tRndbt.center.x); // Angulo del ultimo punto.

            tPresentPoint.x = tGlobalPlanner->Node.X;
            tPresentPoint.y = tGlobalPlanner->Node.Y;

            // Se llena el mapa con puntos intermedios
            P0.x = tRndbt.center.x + tRndbt.radius * cos(atan2(tPresentPoint.y - tRndbt.center.y,tPresentPoint.x - tRndbt.center.x) - tSign* tGlobalPlanner->Node.Di[5]/tRndbt.radius);
            P0.y = tRndbt.center.y + tRndbt.radius * sin(atan2(tPresentPoint.y - tRndbt.center.y,tPresentPoint.x - tRndbt.center.x) - tSign* tGlobalPlanner->Node.Di[5]/tRndbt.radius);
            Node.X = P5.x;
            Node.Y = P5.y;

            while(sqrt(pow(Node.X - P0.x,2) + pow(Node.Y - P0.y,2)) > (tAuxDistanceInRBT + 0.5)){ // Este es el ultimo punto de bezier de la entrada.
                tAngle         = tAngle + tSign*(tAuxDistanceInRBT)/tRndbt.radius;
                Node.X         = tRndbt.center.x + tRndbt.radius*cos(tAngle);
                Node.Y         = tRndbt.center.y + tRndbt.radius*sin(tAngle);
                Node.Curvature = curvatureSign*1/tRndbt.radius;
                tLocalPlanner  = TRI_LocalPlanner_AddNodeBottomList(tLocalPlanner, Node);
            }

            // Se genera la salida

            tSeg1.p[0].x = P0.x; // el punto presente es la entrada al roundabout.
            tSeg1.p[0].y = P0.y; // el punto presente es la entrada al roundabout.
            tSeg1.p[1].x = tRndbt.center.x + tRndbt.radius * cos(atan2(tPresentPoint.y - tRndbt.center.y,tPresentPoint.x - tRndbt.center.x) - tSign*(tGlobalPlanner->Node.Di[5] - 0.01)/tRndbt.radius); // el punto presente es la entrada al roundabout.
            tSeg1.p[1].y = tRndbt.center.y + tRndbt.radius * sin(atan2(tPresentPoint.y - tRndbt.center.y,tPresentPoint.x - tRndbt.center.x) - tSign*(tGlobalPlanner->Node.Di[5] - 0.01)/tRndbt.radius); // el punto presente es la entrada al roundabout.

            tSeg2.p[0].x = tPresentPoint.x;
            tSeg2.p[0].y = tPresentPoint.y;
            if(tGlobalPlanner->Next == NULL){
                tSeg2.p[1].x  = oGlobalPlanner->Node.X;
                tSeg2.p[1].y  = oGlobalPlanner->Node.Y;
            }else{
                tSeg2.p[1].x  = tGlobalPlanner->Next->Node.X;
                tSeg2.p[1].y  = tGlobalPlanner->Next->Node.Y;
            }

            P0 = P0;
            P1 = TRI_SetPointBasedSegment(tSeg1, tGlobalPlanner->Node.Di[4]);
            P2 = TRI_SetPointBasedSegment(tSeg1, tGlobalPlanner->Node.Di[3]);
            P3 = TRI_SetPointBasedSegment(tSeg2, tGlobalPlanner->Node.Di[2]);
            P4 = TRI_SetPointBasedSegment(tSeg2, tGlobalPlanner->Node.Di[1]);
            P5 = TRI_SetPointBasedSegment(tSeg2, tGlobalPlanner->Node.Di[0]);

            nBez = TRI_NBezier_Points(P0, P1, P2, P3, P4, P5);
            i = 0;
            while(i<=(nBez-1)){
                t = i/(nBez -1);
                if(nBez <= 1){ t = 0; }
                tPastPoint = tPresentPoint;
                tPresentPoint = TRI_Bezier5to(P0, P1, P2, P3, P4, P5, t);
                Node.X     = tPresentPoint.x;
                Node.Y     = tPresentPoint.y;
                if(i == 0){
                    Node.Curvature = curvatureSign*1/tRndbt.radius;
                }else{
                    if(i == (nBez-1)){
                        Node.Curvature = 0;
                    }else{
                        t = (i+1)/(nBez -1);
                        tFuturePoint = TRI_Bezier5to(P0, P1, P2, P3, P4, P5, t);
                        curvatureSign = TRI_CurvatureSign(tPastPoint, tPresentPoint, tFuturePoint);
                        TRI_Circle_Fit(tPastPoint, tPresentPoint, tFuturePoint, &tCircleAux);
                        Node.Curvature = curvatureSign*1/tCircleAux.radius;
                    }
                }
                Node.Type = RBTEXIT;
                tLocalPlanner = TRI_LocalPlanner_AddNodeBottomList(tLocalPlanner, Node);
                i = i+1;
            }

            tPresentPoint.x = P5.x;
            tPresentPoint.y = P5.y;
        }
        tGlobalPlanner  = tGlobalPlanner->Next;
    }

    TRI_LocalPlanner_Heading(&tLocalPlanner, 1);
    TRI_Log_LocalPlanner(tLocalPlanner);
    *oLocalPlanner = tLocalPlanner;
    return 0;
}

void   TRI_Log_LocalPlanner(TRI_LocalPlanner_List *iLocalPlanner){

    time_t timer;
    char   buffer[26];
    char   Location[50];
    struct tm* tm_info;
    time(&timer);
    tm_info = localtime(&timer);
    strftime(buffer, 26, "%Y_%m_%d_%H %M %S", tm_info);
    strcpy(Location, "data/logs/Log");
    strcat(Location, buffer);
    strcat(Location, "LocalPlanner.csv");

    if(iLocalPlanner == NULL){
        printf("Local Planner: No se puede imprimir el Log, mapa vacio.\n");
    }else{
        FILE *fp = fopen(Location,"w");
        if(fp == NULL){
            printf("Local Planner: Error al crear el archivo de Log.\n");
        }else{
            TRI_LocalPlanner_List *tLocalPlanner = iLocalPlanner;
            printf("Local Planner: Se creo con exito el archivo de Log.\n");
            fprintf(fp,"Id;X;Y;Max Speed;Curvature;Type;Heading Before[Rad]; Heading[rad];\n");
            while(tLocalPlanner != NULL){
                fprintf(fp,"%d;",tLocalPlanner->Node.Id);
                fprintf(fp,"%lf;",tLocalPlanner->Node.X);
                fprintf(fp,"%lf;",tLocalPlanner->Node.Y);
                fprintf(fp,"%lf;",tLocalPlanner->Node.MaxSpeed);
                fprintf(fp,"%lf;",tLocalPlanner->Node.Curvature);
                fprintf(fp,"%d;",tLocalPlanner->Node.Type);
                fprintf(fp,"%lf;",tLocalPlanner->Node.HeadingBefRad);
                fprintf(fp,"%lf\n",tLocalPlanner->Node.HeadingRad);
                tLocalPlanner = tLocalPlanner->Next;
            }
        }
        fclose(fp);
    }
}

void   TRI_LocalPlanner_Heading(TRI_LocalPlanner_List **iLocalPlanner, int iLoop){
    TRI_LocalPlanner_List *tLocalPlanner = *iLocalPlanner;
    int    nodeID = 0;
    double pastHeading = atan2(tLocalPlanner->Next->Node.Y - tLocalPlanner->Node.Y, tLocalPlanner->Next->Node.X - tLocalPlanner->Node.X);

    while(tLocalPlanner->Next != NULL){
        nodeID = nodeID + 1;
        tLocalPlanner->Node.Id            = nodeID;
        tLocalPlanner->Node.HeadingBefRad = pastHeading;
        tLocalPlanner->Node.HeadingRad    = atan2(tLocalPlanner->Next->Node.Y - tLocalPlanner->Node.Y, tLocalPlanner->Next->Node.X - tLocalPlanner->Node.X);
        pastHeading                       = tLocalPlanner->Node.HeadingRad;

        tLocalPlanner                     = tLocalPlanner->Next;
    }
    if(iLoop == 1){
        nodeID = nodeID + 1;
        tLocalPlanner->Node.Id            = nodeID;
        tLocalPlanner->Node.HeadingBefRad = pastHeading;
        tLocalPlanner->Node.HeadingRad    = atan2((*iLocalPlanner)->Node.Y - tLocalPlanner->Node.Y, (*iLocalPlanner)->Node.X - tLocalPlanner->Node.X);
    }else{
        nodeID = nodeID + 1;
        tLocalPlanner->Node.Id            = nodeID;
        tLocalPlanner->Node.HeadingBefRad = pastHeading;
        tLocalPlanner->Node.HeadingRad    = pastHeading;
    }
}

TRI_LocalPlanner_NodeInfo TRI_LocalPlanner_ExtractNodeInfo(TRI_LocalPlanner_NodeInfo Node){
    TRI_LocalPlanner_NodeInfo oNode;
    oNode.X         = Node.X;
    oNode.Y         = Node.Y;
    oNode.MaxSpeed  = Node.MaxSpeed;
    oNode.Curvature = Node.Curvature;
    return oNode;
}

TRI_LocalPlanner_NodeInfo TRI_LocalPlanner_ExtractNodeCSV(TRI_CSV_NodeInfo Node){
    TRI_LocalPlanner_NodeInfo oNode;
    oNode.X            = Node.X;
    oNode.Y            = Node.Y;
    oNode.Type         = Node.Type;
    oNode.MaxSpeed     = Node.MaxSpeed;
    oNode.Curvature    = Node.Curvature;
    oNode.Rnd.center.x = 0;
    oNode.Rnd.center.y = 0;
    oNode.Rnd.radius   = 0;
    return oNode;
}

TRI_LocalPlanner_List *TRI_LocalPlanner_CreateNode(TRI_LocalPlanner_NodeInfo NodeInfo){
    TRI_LocalPlanner_List *Node;
    Node=(TRI_LocalPlanner_List *)malloc(sizeof(TRI_LocalPlanner_List));
    if(Node == NULL){
        return NULL;
    }
	Node->Node.X            = NodeInfo.X;
	Node->Node.Y            = NodeInfo.Y;
	Node->Node.MaxSpeed     = NodeInfo.MaxSpeed;
	Node->Node.Curvature    = NodeInfo.Curvature;
    Node->Node.Type         = NodeInfo.Type;
    Node->Node.Rnd.center.x = NodeInfo.Rnd.center.x;
    Node->Node.Rnd.center.y = NodeInfo.Rnd.center.y;
    Node->Node.Rnd.radius   = NodeInfo.Rnd.radius;
	Node->Next              = NULL;
	return Node;
}

void TRI_LocalPlanner_DeleteList(TRI_LocalPlanner_List **LinkedList){
    TRI_LocalPlanner_List *Current = *LinkedList;
    TRI_LocalPlanner_List *Next;
    while (Current != NULL) {
        Next = Current->Next;
        free(Current);
        Current = Next;
    }
    *LinkedList = NULL;
}

TRI_LocalPlanner_List *TRI_LocalPlanner_AddNodeBottomList(TRI_LocalPlanner_List *LinkedList, TRI_LocalPlanner_NodeInfo NodeInfo){
    TRI_LocalPlanner_List *NewNode = TRI_LocalPlanner_CreateNode(NodeInfo);
    if(LinkedList == (TRI_LocalPlanner_List*)NULL){
        LinkedList = TRI_LocalPlanner_CreateNode(NodeInfo);
    }
    else{
        TRI_LocalPlanner_List *Current = LinkedList;
        while (Current->Next != NULL){
            Current = Current->Next;
        }
        Current->Next = NewNode;
    }
    return LinkedList;
}

TRI_LocalPlanner_List *TRI_LocalPlanner_AddNodeTopList(TRI_LocalPlanner_List *LinkedList, TRI_LocalPlanner_NodeInfo NodeInfo){
    TRI_LocalPlanner_List *NewNode = TRI_LocalPlanner_CreateNode(NodeInfo);
    if(LinkedList == (TRI_LocalPlanner_List*)NULL){
        LinkedList = TRI_LocalPlanner_CreateNode(NodeInfo);
    }
    else{
        NewNode->Next = LinkedList;
        LinkedList    = NewNode;
    }
    return LinkedList;
}

TRI_LocalPlanner_List *TRI_LocalPlanner_AddNodeOffsetList(TRI_LocalPlanner_List *LinkedList, TRI_LocalPlanner_NodeInfo NodeInfo, int Offset){
    if(LinkedList == (TRI_LocalPlanner_List*)NULL || Offset < 0){
        LinkedList = TRI_LocalPlanner_CreateNode(NodeInfo);
    }
    else{
        if(Offset >= TRI_LocalPlanner_SizeList(LinkedList) || Offset <= 0){
            LinkedList = TRI_LocalPlanner_AddNodeBottomList(LinkedList, NodeInfo);
        }else{
            TRI_LocalPlanner_List *Current = LinkedList;
            TRI_LocalPlanner_List *NewNode = TRI_LocalPlanner_CreateNode(NodeInfo);
            int i = 1;
            while (i < Offset){
                Current = Current->Next;
                i = i + 1;
            }
            NewNode->Next = Current->Next;
            Current->Next = NewNode;
        }
    }
    return LinkedList;
}

int TRI_LocalPlanner_MovePointerForward(TRI_LocalPlanner_List **LinkedList){
    if((*LinkedList)->Next == NULL){return -1;}
    (*LinkedList) = (*LinkedList)->Next;
    return 0;
}

int TRI_LocalPlanner_SizeList(TRI_LocalPlanner_List *LinkedList){
    if(LinkedList == (TRI_LocalPlanner_List*)NULL){
        return 0;
    }
    else{
        int i = 1;
        TRI_LocalPlanner_List *Current = LinkedList;
        while (Current->Next != NULL){
            i = i + 1;
            Current = Current->Next;
        }
        return i;
    }
}

void TRI_LocalPlanner_LctPntPth(int iVehID, tri_point2D iVehposition, TRI_LocalPlanner_List *iPath, tri_Planning_Track *iTrack, int loop){
    TRI_LocalPlanner_List *pointerlist  = iPath;
    tri_segment2D segment, segmentBefore;
    tri_point2D   position, closestpointaux;

    int    i = 1;
    double mindistance  = 30e32;
    double distancetemp = 0;
    int    limit;
    double segment_speed;
    int    detID = 0;
    int    tempBool = 0;
    
    // Variable initialization
      position.x   = iVehposition.x;
      position.y   = iVehposition.y;
    
    // Se fija el limite maximo para iterar los puntos de la ruta.
    if(loop == 1){ limit = TRI_LocalPlanner_SizeList(iPath);
    }else{         limit = TRI_LocalPlanner_SizeList(iPath) - 1; }

    // Se actualiza el segmento pasado

    segmentBefore.p[0].x = pointerlist->Node.X;
    segmentBefore.p[0].y = pointerlist->Node.Y;
    segmentBefore.p[1].x = pointerlist->Next->Node.X;
    segmentBefore.p[1].y = pointerlist->Next->Node.Y;

    //Se iteran los puntos de la ruta para conseguir el punto de inicio.
    while(i <= limit){
        
        // Segmento pasado es el presente de la iteracion pasada
        segmentBefore.p[0].x = segment.p[0].x;
        segmentBefore.p[0].y = segment.p[0].y;
        segmentBefore.p[1].x = segment.p[1].x;
        segmentBefore.p[1].y = segment.p[1].y;

        //Generacion del segmento a evaluar
        segment.p[0].x = pointerlist->Node.X;
        segment.p[0].y = pointerlist->Node.Y;
        segment_speed  = pointerlist->Node.MaxSpeed;
        if(loop == 1 && i == limit){
            //Si estamos en un loop el ultimo nodo se evalua con el primero.
            segment.p[1].x = iPath->Node.X;
            segment.p[1].y = iPath->Node.Y;
        }else{
            //Si no estamos en loop o aun no llegamos al ultimo elemento de la lista estando en loop, seguimos el proceso normal
            segment.p[1].x = pointerlist->Next->Node.X;
            segment.p[1].y = pointerlist->Next->Node.Y;
        }

        //Distancia del punto de origen del coche al segmento
        distancetemp   = TRI_Distance_point_segment(position, segment, &closestpointaux);
        // Seleccion de punto por distancia minima 
        if(distancetemp < mindistance && detID <= 0){
            iTrack->oSegId             = pointerlist->Node.Id;
            iTrack->oClosestPnt.x      = closestpointaux.x;
            iTrack->oClosestPnt.y      = closestpointaux.y;
            iTrack->oClosestSeg.p[0].x = segment.p[0].x;
            iTrack->oClosestSeg.p[0].y = segment.p[0].y;
            iTrack->oClosestSeg.p[1].x = segment.p[1].x;
            iTrack->oClosestSeg.p[1].y = segment.p[1].y;
            iTrack->oHeadingBefore     = atan2(segmentBefore.p[1].y - segmentBefore.p[0].y, segmentBefore.p[1].x - segmentBefore.p[0].x);
            iTrack->oHeadingAfter      = atan2(segment.p[1].y - segment.p[0].y, segment.p[1].x - segment.p[0].x);
            iTrack->oMaxSpeed          = segment_speed;
            iTrack->oMaxCurvature      = pointerlist->Node.Curvature;
            mindistance               = distancetemp;
        }
        // Seleccion de punto por distancia minima  y ID
        if(distancetemp < mindistance && fabs(pointerlist->Node.Id - iVehID) <= 10){
            iTrack->oSegId             = pointerlist->Node.Id;
            iTrack->oClosestPnt.x      = closestpointaux.x;
            iTrack->oClosestPnt.y      = closestpointaux.y;
            iTrack->oClosestSeg.p[0].x = segment.p[0].x;
            iTrack->oClosestSeg.p[0].y = segment.p[0].y;
            iTrack->oClosestSeg.p[1].x = segment.p[1].x;
            iTrack->oClosestSeg.p[1].y = segment.p[1].y;
            iTrack->oHeadingBefore     = atan2(segmentBefore.p[1].y - segmentBefore.p[0].y, segmentBefore.p[1].x - segmentBefore.p[0].x);
            iTrack->oHeadingAfter      = atan2(segment.p[1].y - segment.p[0].y, segment.p[1].x - segment.p[0].x);
            iTrack->oMaxSpeed          = segment_speed;
            iTrack->oMaxCurvature      = pointerlist->Node.Curvature;
            mindistance                = distancetemp;
            detID = 1;
        }
        
        //Aumento del puntero, si llegamos al final, se retorna el puntero al inicio
        if(loop == 1 && i == limit){
            pointerlist = iPath;
        }else{
            pointerlist = pointerlist->Next;
        }

        // Incremento del contador

        i = i+1;
    }
 }

void TRI_LocalPlanner_LctPntPth_Buffer(int iVehID, tri_point2D iVehposition, TRI_LocalPlanner_List *iPath, tri_Planning_Track *iTrack, int loop, double iHorizon, TRI_LocalPlanner_NodeInfo iBuffer[BUFFERSIZE]){
    TRI_LocalPlanner_List *pointerlist    = iPath;
    TRI_LocalPlanner_List *pointerBuffer  = iPath;
    TRI_LocalPlanner_List *pointerBuffer1 = iPath;
    TRI_LocalPlanner_List *pointerBuffer2 = iPath;
    tri_segment2D segment;
    tri_segment2D segmentBefore;
    //tri_Planning_Track iTrack1;
    //tri_Planning_Track iTrack2;
    tri_point2D   position, closestpointaux;

    int    i = 1;
    double mindistance1  = 30e32;
    double mindistance2  = 30e32;
    double distancetemp = 0;
    int    limit;
    double segment_speed;
    int    detID = 0;
    int    tempBool = 0;
    
    double      distanceHorizon = 0;
    int         counter         = 3;
    tri_point2D auxPoint;
    
    // Variable initialization
      position.x   = iVehposition.x;
      position.y   = iVehposition.y;
    
    // Se fija el limite maximo para iterar los puntos de la ruta.
    if(loop == 1){

        limit = TRI_LocalPlanner_SizeList(iPath);
    }else{
        limit = (TRI_LocalPlanner_SizeList(iPath)-1);
    }

    // Se actualiza el segmento pasado

    segmentBefore.p[0].x = pointerlist->Node.X;
    segmentBefore.p[0].y = pointerlist->Node.Y;
    segmentBefore.p[1].x = pointerlist->Next->Node.X;
    segmentBefore.p[1].y = pointerlist->Next->Node.Y;

    //Se iteran los puntos de la ruta para conseguir el punto de inicio.
    while(i<=limit){
        // Segmento pasado es el presente de la iteracion pasada

        segmentBefore.p[0].x = segment.p[0].x;
        segmentBefore.p[0].y = segment.p[0].y;
        segmentBefore.p[1].x = segment.p[1].x;
        segmentBefore.p[1].y = segment.p[1].y;

        //Generacion del segmento a evaluar
        segment.p[0].x = pointerlist->Node.X;
        segment.p[0].y = pointerlist->Node.Y;
        segment_speed  = pointerlist->Node.MaxSpeed;
        if(loop == 1 && i == limit){
            //Si estamos en un loop el ultimo nodo se evalua con el primero.
            segment.p[1].x = iPath->Node.X;
            segment.p[1].y = iPath->Node.Y;
        }else{
            //Si no estamos en loop o aun no llegamos al ultimo elemento de la lista estando en loop, seguimos el proceso normal
            segment.p[1].x = pointerlist->Next->Node.X;
            segment.p[1].y = pointerlist->Next->Node.Y;
        }

        //Distancia del punto de origen del coche al segmento
        distancetemp   = TRI_Distance_point_segment(position, segment, &closestpointaux);
        //Distancia minima
        if(distancetemp < mindistance1 && detID <= 0){
            pointerBuffer               = pointerlist;
            iTrack->oSegId             = pointerlist->Node.Id;
            iTrack->oClosestPnt.x      = closestpointaux.x;
            iTrack->oClosestPnt.y      = closestpointaux.y;
            iTrack->oClosestSeg.p[0].x = segment.p[0].x;
            iTrack->oClosestSeg.p[0].y = segment.p[0].y;
            iTrack->oClosestSeg.p[1].x = segment.p[1].x;
            iTrack->oClosestSeg.p[1].y = segment.p[1].y;
            iTrack->oHeadingBefore     = atan2(segmentBefore.p[1].y - segmentBefore.p[0].y, segmentBefore.p[1].x - segmentBefore.p[0].x);
            iTrack->oHeadingAfter      = atan2(segment.p[1].y - segment.p[0].y, segment.p[1].x - segment.p[0].x);
            iTrack->oMaxSpeed          = segment_speed;
            iTrack->oMaxCurvature      = pointerlist->Node.Curvature;
            mindistance1               = distancetemp;
        }
        // Seleccion de punto por distancia minima  y ID
        
        if(fabs(pointerlist->Node.Id - iVehID) <= 10 || ((limit - iVehID <= 10) && (fabs(fabs(iVehID - limit) - pointerlist->Node.Id) <= 10))){
            tempBool = 1;
        }else{
            tempBool = 0;
        }
        if(distancetemp < mindistance2 && tempBool == 1){
            pointerBuffer              = pointerlist;
            iTrack->oSegId             = pointerlist->Node.Id;
            iTrack->oClosestPnt.x      = closestpointaux.x;
            iTrack->oClosestPnt.y      = closestpointaux.y;
            iTrack->oClosestSeg.p[0].x = segment.p[0].x;
            iTrack->oClosestSeg.p[0].y = segment.p[0].y;
            iTrack->oClosestSeg.p[1].x = segment.p[1].x;
            iTrack->oClosestSeg.p[1].y = segment.p[1].y;
            iTrack->oHeadingBefore     = atan2(segmentBefore.p[1].y - segmentBefore.p[0].y, segmentBefore.p[1].x - segmentBefore.p[0].x);
            iTrack->oHeadingAfter      = atan2(segment.p[1].y - segment.p[0].y, segment.p[1].x - segment.p[0].x);
            iTrack->oMaxSpeed          = segment_speed;
            iTrack->oMaxCurvature      = pointerlist->Node.Curvature;
            mindistance2               = distancetemp;
            detID = 1;
        }
        
        //Aumento del puntero, si llegamos al final, se retorna el puntero al inicio
        if(loop == 1 && i == limit){
            pointerlist = iPath;
        }else{
            pointerlist = pointerlist->Next;
        }

        // Incremento del contador

        i = i+1;
    }
    
    // Llenado del Buffer...
    distanceHorizon = 0;
    counter         = 3;
    //auxPoint;
    iBuffer[0]   = pointerBuffer->Node;

    if(pointerBuffer->Next == NULL && loop == 0){
        pointerBuffer = pointerBuffer;
        iBuffer[1]   = pointerBuffer->Node;
        return;
    }
    if(pointerBuffer->Next == NULL && loop == 1){
        pointerBuffer = iPath;
    }else{
        pointerBuffer = pointerBuffer->Next;
    }

    iBuffer[1]     = pointerBuffer->Node;

    distanceHorizon = sqrt(pow(iTrack->oClosestPnt.x - iBuffer[1].X,2) + pow(iTrack->oClosestPnt.y - iBuffer[1].Y,2));
    if(distanceHorizon > iHorizon){
        iBuffer[1].X = iTrack->oClosestPnt.x + iHorizon*(iBuffer[1].X-iBuffer[0].X)/sqrt(pow(iBuffer[1].X-iBuffer[0].X,2) + pow(iBuffer[1].Y-iBuffer[0].Y,2));
        iBuffer[1].Y = iTrack->oClosestPnt.y + iHorizon*(iBuffer[1].Y-iBuffer[0].Y)/sqrt(pow(iBuffer[1].X-iBuffer[0].X,2) + pow(iBuffer[1].Y-iBuffer[0].Y,2));
    }else{
        while(counter <= BUFFERSIZE && distanceHorizon < iHorizon){
            if(pointerBuffer == NULL && loop == 0){
                break;
            }
            auxPoint.x = pointerBuffer->Node.X;
            auxPoint.y = pointerBuffer->Node.Y;
            if(pointerBuffer->Next == NULL && loop == 1){
                pointerBuffer = iPath;
            }else{
                pointerBuffer = pointerBuffer->Next;
            }
            iBuffer[counter-1] = pointerBuffer->Node;
            
            if(distanceHorizon + sqrt(pow(pointerBuffer->Node.X - auxPoint.x,2) + pow(pointerBuffer->Node.Y - auxPoint.y,2)) > iHorizon){
                iBuffer[counter-1].X = auxPoint.x + (iHorizon - distanceHorizon)*(pointerBuffer->Node.X-auxPoint.x)/sqrt(pow(pointerBuffer->Node.X-auxPoint.x,2) + pow(pointerBuffer->Node.Y-auxPoint.y,2));
                iBuffer[counter-1].Y = auxPoint.y + (iHorizon - distanceHorizon)*(pointerBuffer->Node.Y-auxPoint.y)/sqrt(pow(pointerBuffer->Node.X-auxPoint.x,2) + pow(pointerBuffer->Node.Y-auxPoint.y,2));
            }
            distanceHorizon     = distanceHorizon + sqrt(pow(pointerBuffer->Node.X - auxPoint.x,2) + pow(pointerBuffer->Node.Y - auxPoint.y,2));
            counter             = counter + 1;
        }
    }
    while(counter <= BUFFERSIZE){
        iBuffer[counter-1].Type = EMPTYBUFFER;
        counter                  = counter + 1;
    }
 }


int TRI_LocalPlanner_ReturnID(tri_point2D XY, TRI_LocalPlanner_List *iPath){
    TRI_LocalPlanner_List *pointerlist = iPath;
    tri_segment2D segment;
    tri_point2D   trash;
    double distance = 1e30;
    int ID = 0;
    if(pointerlist == NULL){
        return -2;
    }
    if(pointerlist->Next == NULL){
        return -1;
    }
    while(pointerlist->Next != NULL){
        
        segment.p[0].x = pointerlist->Node.X;
        segment.p[0].y = pointerlist->Node.Y;
        segment.p[1].x = pointerlist->Next->Node.X;
        segment.p[1].y = pointerlist->Next->Node.Y;
        if(TRI_Distance_point_segment(XY, segment, &trash) < distance){
            distance = TRI_Distance_point_segment(XY, segment, &trash);
            ID = pointerlist->Node.Id;
        }
        pointerlist = pointerlist->Next;
    }
    return ID;
}


void TRI_Buffer_LctPntPath(tri_point2D iVehposition, TRI_LocalPlanner_NodeInfo iBuffer[BUFFERSIZE], tri_Planning_Track *iTrack){
    tri_segment2D segment, segmentBefore;
    tri_point2D   position, closestpointaux;
    int    i = 0;
    double mindistance  = 30e32;
    double distancetemp = 0;
    double segment_speed;
    
    // Initialize variables
      position.x   = iVehposition.x;
      position.y   = iVehposition.y;

    // Caso trivial de un solo punto en el buffer
    if(iBuffer[1].Type == EMPTYBUFFER){
        iTrack->oSegId             = 0;//iBuffer[0].Id;
        iTrack->oClosestPnt.x      = iBuffer[0].X;
        iTrack->oClosestPnt.y      = iBuffer[0].Y;
        iTrack->oClosestSeg.p[0].x = iBuffer[0].X;
        iTrack->oClosestSeg.p[0].y = iBuffer[0].Y;
        iTrack->oClosestSeg.p[1].x = iBuffer[0].X;
        iTrack->oClosestSeg.p[1].y = iBuffer[0].Y;
        iTrack->oHeadingBefore     = 0.0;
        iTrack->oHeadingAfter      = 0.0;
        iTrack->oMaxSpeed          = iBuffer[0].MaxSpeed;
        iTrack->oMaxCurvature      = iBuffer[0].Curvature;
        return;
    }
    // Se actualiza el segmento pasado

    segmentBefore.p[0].x = iBuffer[0].X;
    segmentBefore.p[0].y = iBuffer[0].Y;
    segmentBefore.p[1].x = iBuffer[1].X;
    segmentBefore.p[1].y = iBuffer[1].Y;

    //Se iteran los puntos de la ruta para conseguir el punto de inicio.
    while(i < (BUFFERSIZE - 1) ){
        // Segmento pasado es el presente de la iteracion pasada
        if(iBuffer[i+1].Type == EMPTYBUFFER){break;}
        segmentBefore.p[0].x = segment.p[0].x;
        segmentBefore.p[0].y = segment.p[0].y;
        segmentBefore.p[1].x = segment.p[1].x;
        segmentBefore.p[1].y = segment.p[1].y;

        //Generacion del segmento a evaluar
        segment.p[0].x = iBuffer[i].X;
        segment.p[0].y = iBuffer[i].Y;
        segment_speed  = iBuffer[i].MaxSpeed;

        segment.p[1].x = iBuffer[i+1].X;
        segment.p[1].y = iBuffer[i+1].Y;


        //Distancia del punto de origen del coche al segmento
        distancetemp   = TRI_Distance_point_segment(position, segment, &closestpointaux);
        //Distancia minima
        if(distancetemp < mindistance){
            iTrack->oSegId             = i;
            iTrack->oClosestPnt.x      = closestpointaux.x;
            iTrack->oClosestPnt.y      = closestpointaux.y;
            iTrack->oClosestSeg.p[0].x = segment.p[0].x;
            iTrack->oClosestSeg.p[0].y = segment.p[0].y;
            iTrack->oClosestSeg.p[1].x = segment.p[1].x;
            iTrack->oClosestSeg.p[1].y = segment.p[1].y;
            iTrack->oHeadingBefore     = iBuffer[i].HeadingBefRad;
            iTrack->oHeadingAfter      = atan2(segment.p[1].y - segment.p[0].y, segment.p[1].x - segment.p[0].x);
            iTrack->oMaxSpeed          = segment_speed;
            iTrack->oMaxCurvature      = iBuffer[i].Curvature;
            iTrack->oNextCurvature     = iBuffer[i+1].Curvature;
            mindistance                = distancetemp;
        }

        //Aumento del puntero, si llegamos al final, se retorna el puntero al inicio

        i = i+1;
    }
 }


int tri_Point_In_Polygon(tri_point2D iPto, tri_point2D iPolygon[POLYGONSIZE]){
    tri_segment2D s1, s2;
    tri_point2D p;
    int i = 0;
    int nIntersecciones = 0;
    
    s1.p[0].x = iPto.x; 
    s1.p[0].y = iPto.y;
    s1.p[1].x = iPto.x + 10000000; // alejo el punto 10mil metros
    s1.p[1].y = iPto.y;
    
    while(i < POLYGONSIZE){
      
        if(i == POLYGONSIZE - 1){
            s2.p[0].x = iPolygon[i + 0].x; 
            s2.p[0].y = iPolygon[i + 0].y;
            s2.p[1].x = iPolygon[0].x; // alejo el punto 10mil metros
            s2.p[1].y = iPolygon[0].y;
        }else{
            s2.p[0].x = iPolygon[i + 0].x; 
            s2.p[0].y = iPolygon[i + 0].y;
            s2.p[1].x = iPolygon[i + 1].x; // alejo el punto 10mil metros
            s2.p[1].y = iPolygon[i + 1].y;
        }
        
        if(TRI_Segments_Intersected(s1,s2, &p) == 1){
            nIntersecciones = nIntersecciones + 1;
        }
        
        i = i + 1;
    }
	return nIntersecciones;
}

int TRI_PolygonFromBuffer(tri_point2D iVehposition, double iRoadWidth, TRI_LocalPlanner_NodeInfo iBuffer[BUFFERSIZE], double iDistance, double iVerticalOffset, double iHorizontalOffset, tri_point2D iPolygon[POLYGONSIZE]){
    // Se supone que el buffer entrega la ubicacion del vehiculo desde la coordenada iBuffer[0]
    double tDistance = iDistance;
    double tVOffset  = iVerticalOffset;
    //int    IDBuffer;
    // Esta verificacion se realiza con la finalidad de saber si existen suficientes 
    // elementos para la distancia solicitada.
    int    i = 1;
    double x = iVehposition.x;
    double y = iVehposition.y;
    double d = 0;
    double delta ; 
    int    nPtosLateral;
    double Vx, Vy;
    double xt, yt;
    int it;
    double dBase;
    double sign ;
    double tetha;
    int nPtosAcumulado = 0;
    
    // Se verifica que las variables de entrada tengan valores correctos.
    if(tDistance < 0)                        {tDistance =              0;}
    if(tVOffset > HORIZON_METERS)            {tVOffset  = HORIZON_METERS;}
    if(tVOffset < 0)                         {tVOffset  =              0;}
    if(tDistance + tVOffset > HORIZON_METERS){tDistance = HORIZON_METERS - tVOffset;}
    if(iBuffer[1].Type < 0)       {return -1;} // Si el valor del buffer en la segunda posicion es tipo 
                                               // negativo, quiere decir que no se tiene ni un segmento.
    
    // Se genera el poligono.
    if(POLYGONSIZE     <  3){printf("Error: Poligono mal definido, menos de 3 elementos.\n");      return -1;}
    if(POLYGONSIZE % 2 != 0){printf("Error: Poligono mal definido, debe ser par la dimension.\n"); return -1;}
    if(POLYGONSIZE     > 30){printf("Warning: Poligono de ruta grande, posibilidad time delay.\n");          }
    
    // Se debe comenzar a generar los puntos que formaran el poligono
    delta        = tDistance/((POLYGONSIZE-2)/2); // Se restan los que seran los puntos que estaran paralelos al vehiculo.
    nPtosLateral = (POLYGONSIZE-2)/2;
    
    // Se construye el poligono.
    i = 1;
    d = 0;
    x = iVehposition.x;
    y = iVehposition.y;
    if(tVOffset > 0.1){
        while(i < (BUFFERSIZE - 1) && iBuffer[i].Type >= 0 && d < tVOffset){
            if(d + sqrt(pow(y - iBuffer[i].Y,2) + pow(x - iBuffer[i].X,2)) >= tVOffset){
                Vx = iBuffer[i].X - x; // Esta apuntando hacia atras.
                Vy = iBuffer[i].Y - y;
                Vx = Vx / sqrt(Vx*Vx + Vy*Vy);
                Vy = Vy / sqrt(Vx*Vx + Vy*Vy);
                x = x + Vx*(tVOffset - d);
                y = y + Vy*(tVOffset - d);
                d = tVOffset;
                i = i + 1;
            }else{
                d = d + sqrt(pow(y - iBuffer[i].Y,2) + pow(x - iBuffer[i].X,2));
                x = iBuffer[i].X;
                y = iBuffer[i].Y;
                i = i + 1;
            }
        }
    }
    
    
    d = 0;
    xt = x;
    yt = y;
    it = i;
    while(it < BUFFERSIZE && iBuffer[it].Type > 0){
        d = d + sqrt(pow(yt - iBuffer[it].Y,2) + pow(xt - iBuffer[it].X,2));
        if(d > tDistance){ break;} // Se verifico que la distancia solicitada existe en el buffer.
        xt = iBuffer[it].X;
        yt = iBuffer[it].Y;
        it = it + 1;
    }
    if(d < tDistance){tDistance = d;} // Se adapta la distancia final a una que si se puede.
    
    dBase = 0;
    sign  = (iHorizontalOffset)/fabs(iHorizontalOffset);
    if(fabs(iHorizontalOffset) < 0.01 ){sign = 1;}
    tetha = atan2(iBuffer[i].Y - iBuffer[i-1].Y,iBuffer[i].X - iBuffer[i-1].X);
    iPolygon[0].x = x + (iRoadWidth/2)*cos(tetha - M_PI/2) + fabs(iHorizontalOffset)*cos(tetha - M_PI/2*sign);
    iPolygon[0].y = y + (iRoadWidth/2)*sin(tetha - M_PI/2) + fabs(iHorizontalOffset)*sin(tetha - M_PI/2*sign);
    iPolygon[POLYGONSIZE-1].x = x + iRoadWidth/2*cos(tetha + M_PI/2) + fabs(iHorizontalOffset)*cos(tetha - M_PI/2*sign);
    iPolygon[POLYGONSIZE-1].y = y + iRoadWidth/2*sin(tetha + M_PI/2) + fabs(iHorizontalOffset)*sin(tetha - M_PI/2*sign);
    
    
    nPtosAcumulado = 0;
    d = 0;
    while(i < BUFFERSIZE && iBuffer[i].Type >= 0 && nPtosAcumulado < nPtosLateral){
        if(d + sqrt(pow(y - iBuffer[i].Y,2) + pow(x - iBuffer[i].X,2)) >= delta*(nPtosAcumulado + 1)){
            nPtosAcumulado = nPtosAcumulado + 1;
            Vx = iBuffer[i].X - x; // Esta apuntando hacia atras.
            Vy = iBuffer[i].Y - y;
            Vx = Vx / sqrt(Vx*Vx + Vy*Vy);
            Vy = Vy / sqrt(Vx*Vx + Vy*Vy);
            x = x + Vx*(delta*nPtosAcumulado - d);
            y = y + Vy*(delta*nPtosAcumulado - d);
            sign = (iHorizontalOffset)/fabs(iHorizontalOffset);
            if(fabs(iHorizontalOffset) < 0.01 ){sign = 1;}
            iPolygon[nPtosAcumulado].x = x + iRoadWidth/2*cos(atan2(Vy,Vx) - M_PI/2) + fabs(iHorizontalOffset)*cos(atan2(Vy,Vx) - M_PI/2*sign);
            iPolygon[nPtosAcumulado].y = y + iRoadWidth/2*sin(atan2(Vy,Vx) - M_PI/2) + fabs(iHorizontalOffset)*sin(atan2(Vy,Vx) - M_PI/2*sign);
            iPolygon[(POLYGONSIZE - 1 ) - nPtosAcumulado].x = x + iRoadWidth/2*cos(atan2(Vy,Vx) + M_PI/2) + fabs(iHorizontalOffset)*cos(atan2(Vy,Vx) - M_PI/2*sign);
            iPolygon[(POLYGONSIZE - 1 ) - nPtosAcumulado].y = y + iRoadWidth/2*sin(atan2(Vy,Vx) + M_PI/2) + fabs(iHorizontalOffset)*sin(atan2(Vy,Vx) - M_PI/2*sign);
            d = delta*nPtosAcumulado;
            dBase = d;
        }else{
            d = d + sqrt(pow(y - iBuffer[i].Y,2) + pow(x - iBuffer[i].X,2));
            dBase = d;
            x = iBuffer[i].X;
            y = iBuffer[i].Y;
            i = i + 1;
        }
    }

    if((i == BUFFERSIZE || iBuffer[i].Type < 0) && nPtosAcumulado < nPtosLateral){
        nPtosAcumulado = nPtosAcumulado + 1;
        Vx = iBuffer[i-1].X - iBuffer[i-2].X; // Esta apuntando hacia atras.
        Vy = iBuffer[i-1].Y - iBuffer[i-2].Y;
        Vx = Vx / sqrt(Vx*Vx + Vy*Vy);
        Vy = Vy / sqrt(Vx*Vx + Vy*Vy);
        x = iBuffer[i-1].X;
        y = iBuffer[i-1].Y;
        sign = (iHorizontalOffset)/fabs(iHorizontalOffset);
        if(fabs(iHorizontalOffset) < 0.01 ){sign = 1;}
        iPolygon[nPtosAcumulado].x = x + iRoadWidth/2*cos(atan2(Vy,Vx) - M_PI/2) + fabs(iHorizontalOffset)*cos(atan2(Vy,Vx) - M_PI/2*sign);
        iPolygon[nPtosAcumulado].y = y + iRoadWidth/2*sin(atan2(Vy,Vx) - M_PI/2) + fabs(iHorizontalOffset)*sin(atan2(Vy,Vx) - M_PI/2*sign);
        iPolygon[(POLYGONSIZE - 1 ) - nPtosAcumulado].x = x + iRoadWidth/2*cos(atan2(Vy,Vx) + M_PI/2) + fabs(iHorizontalOffset)*cos(atan2(Vy,Vx) - M_PI/2*sign);
        iPolygon[(POLYGONSIZE - 1 ) - nPtosAcumulado].y = y + iRoadWidth/2*sin(atan2(Vy,Vx) + M_PI/2) + fabs(iHorizontalOffset)*sin(atan2(Vy,Vx) - M_PI/2*sign);
    }
    
    while(nPtosAcumulado < nPtosLateral){
        nPtosAcumulado = nPtosAcumulado + 1;
        iPolygon[nPtosAcumulado].x = iPolygon[nPtosAcumulado - 1].x;
        iPolygon[nPtosAcumulado].y = iPolygon[nPtosAcumulado - 1].y;
        iPolygon[(POLYGONSIZE - 1 ) - nPtosAcumulado].x = iPolygon[(POLYGONSIZE - 1 ) - (nPtosAcumulado - 1)].x;
        iPolygon[(POLYGONSIZE - 1 ) - nPtosAcumulado].y = iPolygon[(POLYGONSIZE - 1 ) - (nPtosAcumulado - 1)].y;
    }
    
    return 0;
}




// Funciones Nuevas implementadas para manejo de intersecciones en ENABLES3
int TRI_GenerateNextInterFromBuffer(TRI_LocalPlanner_NodeInfo iBuffer[BUFFERSIZE], double maxHorizon, tri_point2D *interPto){
	if(iBuffer[1].Type == EMPTYBUFFER){
		// Verificacion de buffer no vacio.
		printf("Local Planner (Verificador Intersecciones): Error el buffer solo tiene un nodo.\n");
		return -1;
	}
	if(iBuffer[1].Type == TURN && fabs(iBuffer[0].Curvature) <= 0.00001){
		tri_segment2D s1, s2;
		tri_point2D p;
		// Se realiza las intersecciones de los segmentos
		// usando el heading del primer punto y del ultimo
		int i = 2;
		int lastIndex = i;
		while(i < BUFFERSIZE){
			if(iBuffer[i].Type == EMPTYBUFFER){
				lastIndex = i - 1;
				i = BUFFERSIZE;
			}else{
				if(sqrt(pow(iBuffer[i].X - iBuffer[i-1].X,2) + pow(iBuffer[i].Y - iBuffer[i-1].Y,2)) > 2.0 /*Distancia grande*/){
					lastIndex = i;
					i = BUFFERSIZE;
				}
			}
			i = i + 1;
		}
			
		// Calculo del punto de intersecciones, tiene dos casos que el producto cruz sea cero
		// en ese caso el punto de interseccion esta en el medio de los dos puntos
		// el otro caso es una interseccion de dos rectas (Tener cuidado con el caso de)
		s1.p[0].x = iBuffer[1].X;                              s1.p[0].y = iBuffer[1].Y;
		s1.p[1].x = iBuffer[1].X + cos(iBuffer[1].HeadingRad); s1.p[1].y = iBuffer[1].Y + sin(iBuffer[1].HeadingRad);
		s2.p[0].x = iBuffer[lastIndex].X;                                      s2.p[0].y = iBuffer[lastIndex].Y;
		s2.p[1].x = iBuffer[lastIndex].X + cos(iBuffer[lastIndex].HeadingRad); s2.p[1].y = iBuffer[lastIndex].Y + sin(iBuffer[lastIndex].HeadingRad);
		//printf("%lf ; %lf ; %lf ; %lf ; %lf ; %lf ; %lf ; %lf ;\n", s1.p[0].x, s1.p[0].y, s1.p[1].x, s1.p[1].y, s2.p[0].x, s2.p[0].y, s2.p[1].x, s2.p[1].y);
        if(TRI_Lines_Intersected(s1, s2, &p) < 0 ){
			p.x = (iBuffer[1].X + iBuffer[lastIndex].X)/2;
			p.y = (iBuffer[1].Y + iBuffer[lastIndex].Y)/2;
		}
		interPto->x = p.x;
		interPto->y = p.y;
		return 2;
	}else{
		interPto->x = 0;
		interPto->y = 0;
        if(fabs(iBuffer[0].Curvature) <= 0.00001){return 1;}else{return 0;}
		
	}
}