//============================================================================
// Name        : Control_DoubleProportionalK.cpp
// Author      : RL
// Version     : 0.1
// Description :
//============================================================================


#include "../inc/Control_DoubleProportionalK.hpp"


namespace tri     {
namespace ad      {
namespace control {


DoubleProportionalK::DoubleProportionalK(){
	this->clear();
}


DoubleProportionalK::~DoubleProportionalK(){
	this->clear();
}


int		DoubleProportionalK::getSize(){
	return this->speed.size();
}


void	DoubleProportionalK::clear(){
	this->speed.clear();
	this->kLatError.clear();
	this->kAngError.clear();
	this->kCurvature.clear();
}


bool	DoubleProportionalK::readConfig(std::string fileAddress){
	tinyxml2::XMLDocument  xmlDoc; // Xml file
    tinyxml2::XMLElement  *pMap;
    int                   errorCode = xmlDoc.LoadFile(fileAddress.c_str());

    // Evaluation that the file exists
    if(errorCode != tinyxml2::XML_SUCCESS ){                      // opening xml file
        std::cout << ERROR0029 << " TinyXML.LoadFile method returns ID error#"<< errorCode << "." <<std::endl;
        return false;
    }

    // evaluation the father node should be named simpleMAP
    pMap = xmlDoc.FirstChildElement("doubleProportionalCurvature");
    if(!pMap){
        std::cout << ERROR0030 << std::endl;
        return false;
    }

    // evaluate the first node
    pMap = pMap->FirstChildElement( "node" );
    if(!pMap && this->getSize() == 0){
        std::cout << ERROR0031 << std::endl;
        return false;
    }

    // After all the error diagnosis the file is completely read
	this->clear();
	double speed, kLatError, kAngError, kCurvature;
    while(pMap){

		if(pMap->QueryDoubleAttribute ("speed" , &speed ) != 0 ){
			std::cout << ERROR0034 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
			return false;
		}
		if(pMap->QueryDoubleAttribute ("kLatError" , &kLatError ) != 0 ){
			std::cout << ERROR0035 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
			return false;
		}
		if(pMap->QueryDoubleAttribute ("kAngError" , &kAngError ) != 0 ){
			std::cout << ERROR0036 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
			return false;
		}
		if(pMap->QueryDoubleAttribute ("kCurvature" , &kCurvature ) != 0 ){
			std::cout << ERROR0037 << "(Node#" << this->getSize()+1 << ")."<< std::endl;
			return false;
		}
        this->appendNode(speed, kLatError, kAngError, kCurvature);       // adding the node to the list
        pMap = pMap->NextSiblingElement("node");
    }
    return true;
}


void	DoubleProportionalK::appendNode(double speed, double kLatError, double kAngError, double kCurvature){
	this->speed.push_back(speed);
	this->kLatError.push_back(kLatError);
	this->kAngError.push_back(kAngError);
	this->kCurvature.push_back(kCurvature);
}


bool	DoubleProportionalK::verify(){
	for(int i = 1; i < this->getSize(); i++){
		if(this->speed[i-1] >= this->speed[i]){
			std::cout << ERROR0032 << std::endl;
			return false;
		}
	}
	if((this->speed.size() != this->kLatError.size()) || (this->kAngError.size() != this->kLatError.size()) || (this->kAngError.size() != this->kCurvature.size())){
		std::cout << ERROR0033 << std::endl;
		return false;
	}

	return true;
}


double	DoubleProportionalK::generateOutput(double speed, double latError, double angError, double curvature){
	if(this->getSize() == 0 || !this->verify())
		return 0.0;

	if(speed <= this->speed[0])
		return latError*this->kLatError[0] + angError*this->kAngError[0] + curvature*this->kCurvature[0];
	if(speed >= this->speed.back())
		return latError*this->kLatError.back() + angError*this->kAngError.back() + curvature*this->kCurvature.back();

	double latErrorGain, angErrorGain, curvatureGain, factor;
	for(int i = 0; i < ((int)this->getSize() - 1); i++){
		// linear interpolation (was not implemented a function because it is simple)
		if(speed >= this->speed[i] && speed <= this->speed[i+1]){
			factor        = (speed - this->speed[i])/(this->speed[i+1] - this->speed[i]);
			latErrorGain  = (this->kLatError[i+1]  - this->kLatError[i] )*factor + this->kLatError[i];
			angErrorGain  = (this->kAngError[i+1]  - this->kAngError[i] )*factor + this->kAngError[i];
			curvatureGain = (this->kCurvature[i+1] - this->kCurvature[i])*factor + this->kCurvature[i];
			return latError*latErrorGain + angError*angErrorGain + curvature*curvatureGain;
		}
	}
	return 0.0;
}

} /* end of control */
} /* end of ad      */
} /* end of tri     */
