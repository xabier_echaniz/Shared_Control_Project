/* ====================================================================== 
 * Name:        Lib_Control.cpp
 * Description: Library for control.
 * Note:        - No notes - 
 * Author:      rayalejandro.lattarulo@tecnali.com 26/05/2017
 * Company:     Tecnalia
 * ======================================================================*/
#include "../inc/Lib_Control.hpp"

/* ======================================================================
 * Classes function definition
 * ======================================================================*/
 /* Name:           (function) triClass_Ramp::triClass_Ramp (Constructor)
  * Description:    constructor
  * Note:           - no notes -
  * Author:         rayalejandro.lattarulo@tecnalia.com 26/05/2017 */
triClass_Ramp::triClass_Ramp(void){
    this->i.sp 			= (double)	0.0;
	this->i.reset 		= (int)		0;
	this->i.reset_value = (double)	0;
	
    this->o.reset 		= (int)		0;
	this->o.cv			= (double)	0.0;
	
    this->c.ts			= (double)	0.0;
	this->c.m			= (double)	0.0;
}

 /* Name:           (function) triClass_Ramp::config
  * Description:    configure the ramp
  * Note:           - no notes -
  * Author:         rayalejandro.lattarulo@tecnalia.com 26/05/2017 */
void	triClass_Ramp::config(double ts, double m){
    this->c.ts = (double) ts;
	this->c.m  = (double) m;
}

 /* Name:           (function) triClass_Ramp::input
  * Description:    set the input for the ramp
  * Note:           - no notes -
  * Author:         rayalejandro.lattarulo@tecnalia.com 26/05/2017 */
void	triClass_Ramp::input(double sp, int reset, double reset_value){
    this->i.sp 			= (double) 	sp;
	this->i.reset  		= (int) 	reset;
	this->i.reset_value = (double) 	reset_value;
}

 /* Name:           (function) triClass_Ramp::ramp
  * Description:    calculation of the ramp
  * Note:           - no notes -
  * Author:         rayalejandro.lattarulo@tecnalia.com 26/05/2017 */
double	triClass_Ramp::ramp(void){
	if(this->i.reset < 1){
		this->o.reset = (int)	 0;
		if(this->o.cv < this->i.sp){  
            this->o.cv = this->o.cv + this->c.m*this->c.ts;
            if(this->o.cv > this->i.sp){
                this->o.cv = this->i.sp;
            }
		}else{						  
            this->o.cv = this->o.cv - this->c.m*this->c.ts;
            if(this->o.cv < this->i.sp){
                this->o.cv = this->i.sp;
            }
        }
	}else{
		// The ramp was reset
		this->o.reset = (int)	1;
		this->o.cv    = (double)this->i.reset_value;
	}
	return this->o.cv;
}

/* ======================================================================
 * Function definition
 * ======================================================================*/
double TRI_MaxSpeed_BasedOnCurvature(double iaymax, double iCurvature, double iMaxSpeed){
    /*      aw < 0.315 m/s2 - - > Not uncomfortable
    0.315 < aw < 0.630 m/s2 - - > A little uncomfortable
    0.500 < aw < 1.000 m/s2 - - > Fairy uncomfortable   
    0.800 < aw < 1.600 m/s2 - - > Uncomfortable         
    1.250 < aw < 2.500 m/s2 - - > Very uncomfortable
            aw > 2.500 m/s2 - - > Extremely uncomfortable */
    if(fabs(iCurvature) < 0.001 || sqrt(iaymax/fabs(1.4*iCurvature))>iMaxSpeed){
        return iMaxSpeed;
    }
    return sqrt(iaymax/fabs(1.4*iCurvature));
}
