//============================================================================
// Name        : ADCOREBufferClass.cpp
// Author      : RL - MM
// Version     : 0.1
// Description :
//============================================================================


#include "../inc/Planning_LocalMap.hpp"

using tri::ad::math::distance2Points;
using tri::ad::math::distPointSegment;

namespace tri      {
namespace ad       {
namespace planning {

/* * * * Overwrite from SimpleMap * * * */
void LocalMap::clear(){
    this->localMapNodes.clear();
}
unsigned int LocalMap::getSize(){
	return this->localMapNodes.getSize();
}

void LocalMap::printFile(const std::string & directory){
	this->localMapNodes.printFile(directory, "LogLocalMap");
}

void LocalMap::appendNode(const SimpleMapNode & node){
	this->localMapNodes.appendNode(node);
}

void LocalMap::reverseNodes(){
	this->localMapNodes.reverseNodes();
}

SimpleMapNode LocalMap::getNodeInfo(const int & idx){
	return this->localMapNodes.getNodeInfo(idx);
}

void LocalMap::reIndexing(){
    this->localMapNodes.reIndexing();
}

void LocalMap::overwriteMapNodes(const std::vector<SimpleMapNode> & map){
    this->localMapNodes.overwriteMapNodes(map);
}


std::vector<SimpleMapNode> LocalMap::extractRoundaboutNodes(const int & idxRnbEntrance, int * idxRnbExit){
    return this->localMapNodes.extractRoundaboutNodes(idxRnbEntrance, &(*idxRnbExit) );
}


void LocalMap::generateDataBufferInLoop(int index, double offsetInitial, double bufferDistance, std::vector<SimpleMapNode> & map){
    this->localMapNodes.generateDataBufferInLoop(index, offsetInitial,bufferDistance, map);
}


void  LocalMap::yawCalcInLoop(){
    this->localMapNodes.yawCalcInLoop();
}

/* * * * Local map class methods * * * */

LocalMap::LocalMap(){}

LocalMap::~LocalMap(){}

unsigned int LocalMap::getParameterSize(){
    return (int)this->bezier.size();
}

BezierParameters LocalMap::getParameterInfo(const int & idx){
    return this->bezier[idx];
}

void LocalMap::setGlobalMap(GlobalMap globalMap){
    this->overwriteMapNodes(globalMap.getGlobalMapNodes().getMapNodes());
}

bool LocalMap::parseNode(tinyxml2::XMLElement  *pMap, BezierParameters *node){

    if(pMap->QueryUnsignedAttribute ("id" , &(node->id) ) != 0 ){
        std::cout << ERROR0022 << "(Node#" << this->getParameterSize()+1 << ")."<< std::endl;
        return false;
    }

    if(pMap->QueryDoubleAttribute ("d0" , &(node->d0) )  != 0 ){
        std::cout << ERROR0023 << "(Node#" << this->getParameterSize()+1 << ")."<< std::endl;
        return false;
    }
    if(pMap->QueryDoubleAttribute ("d1" , &(node->d1) )  != 0 ){
        std::cout << ERROR0024 << "(Node#" << this->getParameterSize()+1 << ")."<< std::endl;
        return false;
    }
    if(pMap->QueryDoubleAttribute ("d2" , &(node->d2) )  != 0 ){
        std::cout << ERROR0025 << "(Node#" << this->getParameterSize()+1 << ")."<< std::endl;
        return false;
    }
    if(pMap->QueryDoubleAttribute ("d3" , &(node->d3) )  != 0 ){
        std::cout << ERROR0026 << "(Node#" << this->getParameterSize()+1 << ")."<< std::endl;
        return false;
    }
    if(pMap->QueryDoubleAttribute ("d4" , &(node->d4) )  != 0 ){
        std::cout << ERROR0027 << "(Node#" << this->getParameterSize()+1 << ")."<< std::endl;
        return false;
    }
    if(pMap->QueryDoubleAttribute ("d5" , &(node->d5) )  != 0 ){
        node->d5 = node->d4;
        std::cout << WARNING0007 << "(Node#" << this->getParameterSize()+1 << ")."<< std::endl;
    }
    if(pMap->QueryUnsignedAttribute ("nPoints" , &(node->nPoints) )  != 0 ){
        node->nPoints = 10;
        std::cout << WARNING0008 << "(Node#" << this->getParameterSize()+1 << ")."<< std::endl;
    }
    return true;
}

bool LocalMap::readBezierParameters(const std::string & route){
    tinyxml2::XMLDocument  xmlDoc; // Xml file
    tinyxml2::XMLElement  *pMap;
    BezierParameters       bezierNode;
    int                    errorCode = xmlDoc.LoadFile(route.c_str());

    // Evaluation that the file exists
    if(errorCode != tinyxml2::XML_SUCCESS ){                      // opening xml file
        std::cout << ERROR0006 << " TinyXML.LoadFile method returns ID error#"<< errorCode << "." <<std::endl;
        return false;
    }

    // evaluation the father node should be named simpleMAP
    pMap = xmlDoc.FirstChildElement("bezierMAP");
    if(!pMap){
        std::cout << ERROR0007 << std::endl;
        return false;
    }

    // evaluate the first node
    pMap = pMap->FirstChildElement( "node" );
    if(!pMap && this->getSize() == 0){
        std::cout << ERROR0008 << std::endl;
        return false;
    }


    // After all the error diagnosis the file is completely read
    this->bezier.clear();
    while(pMap){
        this->parseNode(pMap, &bezierNode); // Parsing the information of a node
        this->bezier.push_back(bezierNode);       // adding the node to the list
        pMap = pMap->NextSiblingElement("node");
    }
    return true;
}

bool LocalMap::mapCorrect(){
    int  i, j;
    bool idFound;

    for(i = 0; i < (int)this->getSize(); i++){
        idFound = false;
        for(j=0; j < (int)this->getParameterSize(); j++){
            if( this->getNodeInfo(i).id == this->getParameterInfo(j).id ){
                idFound = true;
            }
        }
        if(!idFound)
            idFound = false;
    }


    return true;
}

BezierParameters LocalMap::getConfiguration(const int & id){
    int i;
    for(i = 0; i < (int) this->getParameterSize(); i++){
        if(id == (int)this->getParameterInfo(i).id){
            return this->getParameterInfo(i);
        }
    }
    return this->getParameterInfo(0);
}

bool LocalMap::completeMap(double deltaDistance){
    BezierParameters           bezParam;
    bool                       everythingCorrect = true;
    std::vector<SimpleMapNode> newMap, tempMap1,tempMap2;
    int                        dumbInt;

    newMap.clear();
    for(int i = 0; i < (int)this->getSize(); i++){
        switch(this->getNodeInfo(i).type){

            case WAYPOINT:
                newMap.push_back(this->getNodeInfo(i));
                break;
            case INTERSECTION:
                tempMap1.clear();
                bezParam = this->getConfiguration( this->getNodeInfo(i).id );
                this->generateInterAndLaneChange(this->getNodeInfo(i-1), this->getNodeInfo(i), this->getNodeInfo(i),this->getNodeInfo(i+1), bezParam, tempMap1);
                newMap.insert(newMap.end(), tempMap1.begin(), tempMap1.end());
                break;
            case RBENTRANCE:
                // It is resolved totally the roundabout
                tempMap1.clear();
                tempMap2.clear();
                tempMap1 = this->extractRoundaboutNodes(i, &dumbInt);
                bezParam = this->getConfiguration( this->getNodeInfo(i).id );
                generateRoundabout(this->getNodeInfo(i-1), tempMap1,this->getNodeInfo(dumbInt + 1), bezParam, deltaDistance, tempMap2);
                newMap.insert(newMap.end(), tempMap2.begin(), tempMap2.end());
                break;
            case RBMIDDLE:
                // nothing happen.
                break;
            case RBEXIT:
                // nothing happen.
                break;
            case LCSTART:
                // Resolve the lane change
                tempMap1.clear();
                bezParam = this->getConfiguration( this->getNodeInfo(i).id );
                this->generateInterAndLaneChange(this->getNodeInfo(i-1), this->getNodeInfo(i),this->getNodeInfo(i+1),this->getNodeInfo(i+2), bezParam, tempMap1);
                newMap.insert(newMap.end(), tempMap1.begin(), tempMap1.end());
                break;
            case LCEND:
                // nothing happen.
                break;

        }
    }
    this->overwriteMapNodes(newMap);
    return everythingCorrect;
}


void LocalMap::generateInterAndLaneChange(const SimpleMapNode & pntNodeBef, const SimpleMapNode & pntNode, const SimpleMapNode & pntNodeAfter1, const SimpleMapNode & pntNodeAfter2, const BezierParameters & paramNode, std::vector<SimpleMapNode> & finalNodes){
    finalNodes.clear();
    Eigen::Vector3d vBef, vAfter, pntBef, pnt, pnt1, pntAfter, tempPnt;
    Eigen::MatrixXd controlPoints(6,3);
    SimpleMapNode   tempNode;
    std::vector<double> x, y, z, k;
    int i;

    pntBef   << pntNodeBef.x, pntNodeBef.y, pntNodeBef.z;
    pnt      << pntNode.x, pntNode.y, pntNode.z;
    pnt1     << pntNodeAfter1.x, pntNodeAfter1.y, pntNodeAfter1.z;
    pntAfter << pntNodeAfter2.x, pntNodeAfter2.y, pntNodeAfter2.z;

    vBef   = pntBef    - pnt ; vBef   =   vBef/vBef.norm();
    vAfter = pntAfter - pnt1; vAfter = vAfter/vAfter.norm();
    tempPnt = pnt + paramNode.d0*vBef;
    controlPoints(0,0) = tempPnt(0); controlPoints(0,1) = tempPnt(1); controlPoints(0,2) = tempPnt(2);
    tempPnt = pnt + paramNode.d1*vBef;
    controlPoints(1,0) = tempPnt(0); controlPoints(1,1) = tempPnt(1); controlPoints(1,2) = tempPnt(2);
    tempPnt = pnt + paramNode.d2*vBef;
    controlPoints(2,0) = tempPnt(0); controlPoints(2,1) = tempPnt(1); controlPoints(2,2) = tempPnt(2);

    tempPnt = pnt1 + paramNode.d3*vAfter;
    controlPoints(3,0) = tempPnt(0); controlPoints(3,1) = tempPnt(1); controlPoints(3,2) = tempPnt(2);
    tempPnt = pnt1 + paramNode.d4*vAfter;
    controlPoints(4,0) = tempPnt(0); controlPoints(4,1) = tempPnt(1); controlPoints(4,2) = tempPnt(2);
    tempPnt = pnt1 + paramNode.d5*vAfter;
    controlPoints(5,0) = tempPnt(0); controlPoints(5,1) = tempPnt(1); controlPoints(5,2) = tempPnt(2);

    tri::ad::math::constructBezier5to(paramNode.nPoints, controlPoints, x, y, z, k);
    tempNode = pntNode;
    for(i=0; i<(int)x.size(); i++){
        tempNode.x    = x[i];
        tempNode.y    = y[i];
        tempNode.z    = z[i];
        tempNode.k    = k[i];
        tempNode.type = WAYPOINT;
        finalNodes.push_back(tempNode);
    }
    x.clear(); y.clear(); z.clear(); k.clear();
}


bool LocalMap::generateRoundabout(SimpleMapNode pntBef, std::vector<SimpleMapNode> roundaboutNodes, SimpleMapNode pntAft, const BezierParameters & paramNode,double deltaDistance, std::vector<SimpleMapNode> & finalNodes){
    Eigen::Vector3d rnbCenter, pntRb1, pntRb2, pntRb3, vBef, vAft, pntBf, pntAf, pntRbEnd, pntRbStart, tanEn, tanEx, pntD4Ent, pntD4Ex, auxPnt, auxPnt2;
    double curvature;
    double tethaArc;
    Eigen::MatrixXd controlPointsEn(5,3), controlPointsEx(5,3);
    SimpleMapNode   tempNode;
    std::vector<double> x1, y1, z1, k1, x2, y2, z2, k2, x3, y3, z3, k3;
    int clockwise, i;

    if((int)roundaboutNodes.size() < 3){
        std::cout << ERROR0028 << std::endl;
        return false;
    }

    pntBf      << pntBef.x, pntBef.y, pntBef.z;
    pntRbStart << roundaboutNodes[0].x, roundaboutNodes[0].y, roundaboutNodes[0].z;
    pntRb1     << roundaboutNodes[0].x, roundaboutNodes[0].y, roundaboutNodes[0].z;
    pntRb2     << roundaboutNodes[1].x, roundaboutNodes[1].y, roundaboutNodes[1].z;
    pntRb3     << roundaboutNodes[2].x, roundaboutNodes[2].y, roundaboutNodes[2].z;
    pntRbEnd   << roundaboutNodes.back().x, roundaboutNodes.back().y, roundaboutNodes.back().z;
    pntAf      << pntAft.x, pntAft.y, pntAft.z;

    vBef = pntBf - pntRbStart; vBef = vBef/vBef.norm();
    vAft = pntAf - pntRbEnd;   vAft = vAft/vAft.norm();
    rnbCenter = tri::ad::math::generateCircle3Pnts(pntRb1, pntRb2, pntRb3, &curvature);

    tethaArc = paramNode.d4*fabs(curvature);
    if(curvature > 0){
        pntD4Ent = tri::ad::math::rotateVectorAroundZ(pntRbStart - rnbCenter,  tethaArc) + rnbCenter;
        tanEn    = tri::ad::math::rotateVectorAroundZ(pntRbStart - rnbCenter,  tethaArc - tri::ad::math::deg2Rad(0.05))+ rnbCenter;
        tanEn    = tanEn - pntD4Ent; tanEn = tanEn/tanEn.norm();
        pntD4Ex  = tri::ad::math::rotateVectorAroundZ(pntRbEnd   - rnbCenter, -tethaArc) + rnbCenter;
        tanEx    = tri::ad::math::rotateVectorAroundZ(pntRbEnd   - rnbCenter, -tethaArc + tri::ad::math::deg2Rad(0.05))+ rnbCenter;
        tanEx    = tanEx - pntD4Ex; tanEx = tanEx/tanEx.norm();
    }else{
        pntD4Ent = tri::ad::math::rotateVectorAroundZ(pntRbStart - rnbCenter, -tethaArc) + rnbCenter;
        tanEn    = tri::ad::math::rotateVectorAroundZ(pntRbStart - rnbCenter, -tethaArc + tri::ad::math::deg2Rad(0.05))+ rnbCenter;
        tanEn    = tanEn - pntD4Ent; tanEn = tanEn/tanEn.norm();
        pntD4Ex  = tri::ad::math::rotateVectorAroundZ(pntRbEnd   - rnbCenter, +tethaArc) + rnbCenter;
        tanEx    = tri::ad::math::rotateVectorAroundZ(pntRbEnd   - rnbCenter, +tethaArc - tri::ad::math::deg2Rad(0.05))+ rnbCenter;
        tanEx    = tanEx - pntD4Ex; tanEx = tanEx/tanEx.norm();
    }

    /* Control points */
    auxPnt = vBef*paramNode.d0 + pntRbStart;
    controlPointsEn(0,0) = auxPnt(0); controlPointsEn(0,1) = auxPnt(1); controlPointsEn(0,2) = auxPnt(2);
    auxPnt = vBef*paramNode.d1 + pntRbStart;
    controlPointsEn(1,0) = auxPnt(0); controlPointsEn(1,1) = auxPnt(1); controlPointsEn(1,2) = auxPnt(2);
    auxPnt = vBef*paramNode.d2 + pntRbStart;
    controlPointsEn(2,0) = auxPnt(0); controlPointsEn(2,1) = auxPnt(1); controlPointsEn(2,2) = auxPnt(2);
    auxPnt = tanEn*paramNode.d3 + pntD4Ent;
    controlPointsEn(3,0) = auxPnt(0); controlPointsEn(3,1) = auxPnt(1); controlPointsEn(3,2) = auxPnt(2);
    auxPnt = pntD4Ent;
    controlPointsEn(4,0) = auxPnt(0); controlPointsEn(4,1) = auxPnt(1); controlPointsEn(4,2) = auxPnt(2);

    auxPnt = pntD4Ex;
    controlPointsEx(0,0) = auxPnt(0); controlPointsEx(0,1) = auxPnt(1); controlPointsEx(0,2) = auxPnt(2);
    auxPnt = tanEx*paramNode.d3 + pntD4Ex;
    controlPointsEx(1,0) = auxPnt(0); controlPointsEx(1,1) = auxPnt(1); controlPointsEx(1,2) = auxPnt(2);
    auxPnt = vAft*paramNode.d2 + pntRbEnd;
    controlPointsEx(2,0) = auxPnt(0); controlPointsEx(2,1) = auxPnt(1); controlPointsEx(2,2) = auxPnt(2);
    auxPnt = vAft*paramNode.d1 + pntRbEnd;
    controlPointsEx(3,0) = auxPnt(0); controlPointsEx(3,1) = auxPnt(1); controlPointsEx(3,2) = auxPnt(2);
    auxPnt = vAft*paramNode.d0 + pntRbEnd;
    controlPointsEx(4,0) = auxPnt(0); controlPointsEx(4,1) = auxPnt(1); controlPointsEx(4,2) = auxPnt(2);

    x1.clear(); y1.clear(); z1.clear(); k1.clear();
    x2.clear(); y2.clear(); z2.clear(); k2.clear();
    x3.clear(); y3.clear(); z3.clear(); k3.clear();
    tri::ad::math::constructBezier4to(paramNode.nPoints, controlPointsEn, x1, y1, z1, k1);
    tri::ad::math::constructBezier4to(paramNode.nPoints, controlPointsEx, x2, y2, z2, k2);

    // complete circle
    clockwise = 0;
    if(curvature < 0){
        clockwise = 1;
    }
    auxPnt  << controlPointsEn(4,0),controlPointsEn(4,1),controlPointsEn(4,2);
    auxPnt2 << controlPointsEx(0,0),controlPointsEx(0,1),controlPointsEx(0,2);
    tri::ad::math::completeCircle(rnbCenter, 1/fabs(curvature) , tri::ad::math::angleVectorAroundZ(auxPnt - rnbCenter), tri::ad::math::angleVectorAroundZ(auxPnt2 - rnbCenter), deltaDistance*fabs(curvature), clockwise, x3, y3);

    tempNode = roundaboutNodes[0];
    for(i=0; i<(int)x1.size(); i++){
        tempNode.x    = x1[i];
        tempNode.y    = y1[i];
        tempNode.z    = z1[i];
        tempNode.k    = k1[i];
        tempNode.type = WAYPOINT;
        finalNodes.push_back(tempNode);
    }
    for(i=1; i<(int)x3.size()-1; i++){
        tempNode.x    = x3[i];
        tempNode.y    = y3[i];
        tempNode.z    = roundaboutNodes[0].z;
        tempNode.k    = curvature;
        tempNode.type = WAYPOINT;
        finalNodes.push_back(tempNode);
    }
    for(i=0; i<(int)x2.size(); i++){
        tempNode.x    = x2[i];
        tempNode.y    = y2[i];
        tempNode.z    = z2[i];
        tempNode.k    = k2[i];
        tempNode.type = WAYPOINT;
        finalNodes.push_back(tempNode);
    }
    return true;
}

int LocalMap::shortDistPointInLoop(int idxBef, int deltaIdx, bool loop,  const Eigen::Vector3d & point, double & closestDistance, Eigen::Vector3d & closestPnt){
    return this->localMapNodes.shortDistPointInLoop(idxBef, deltaIdx, loop, point, closestDistance, closestPnt);
}

double LocalMap::comforSpeedPlanner(double maxSpeed, double comfort){

    double k = 1.4; // Formula parameter
    double curvature;
    double speed;

    for (int i = 0; i < (int)this->getSize(); i++){
        curvature = fabs(this->getNodeInfo(i).k);
        speed = std::min(this->getNodeInfo(i).maxSpeed, maxSpeed);
        if(curvature != 0){
            speed     = std::min(sqrt(comfort/(k*curvature)),speed);
        }
        this->localMapNodes.updateNodeSpeed(i,speed);
    }

    return speed;

}

int   LocalMap::distanceInFront(int idx, bool loop, double distance, Eigen::Vector3d & closestPnt){
    return this->localMapNodes.distanceInFront(idx, loop, distance, closestPnt);
}

} /* end of planning */
} /* end of ad       */
} /* end of tri      */
