//============================================================================
// Name        : Control_DoubleProportionalOvertaking.cpp
// Author      : RL
// Version     : 0.1
// Description :
//============================================================================


#include "../inc/Control_DoubleProportionalOvertaking.hpp"


namespace tri     {
namespace ad      {
namespace control {


DoubleProportionalOvertaking::DoubleProportionalOvertaking(){
	// in the construction is clear every vector variable
	this->clear();
}


DoubleProportionalOvertaking::~DoubleProportionalOvertaking(){
	// in the destructor is clear every vector variable
	this->clear();
}


void	DoubleProportionalOvertaking::clear(){
	// It is clear the ego vehicle projections, objects and bounds
	this->egoVeh.states.clear();

	for(int i = 0; i < (int)this->objs.size(); i++){
		this->objs[i].states.clear();
	}
	this->objs.clear();

	latBoundPos.lowerBounds.clear();
	latBoundPos.upperBounds.clear();

	latBoundSpeed.lowerBounds.clear();
	latBoundSpeed.upperBounds.clear();

	latBoundAcc.lowerBounds.clear();
	latBoundAcc.upperBounds.clear();

	lonBoundPos.lowerBounds.clear();
	lonBoundPos.upperBounds.clear();

	lonBoundSpeed.lowerBounds.clear();
	lonBoundSpeed.upperBounds.clear();

	lonBoundAcc.lowerBounds.clear();
	lonBoundAcc.upperBounds.clear();

	lonBoundJerk.lowerBounds.clear();
	lonBoundJerk.upperBounds.clear();
}


bool	DoubleProportionalOvertaking::configure(std::string fileAddress){
	tinyxml2::XMLDocument  xmlDoc; // Xml file
    tinyxml2::XMLElement  *pMap, *pMapBase;
    int                   errorCode = xmlDoc.LoadFile(fileAddress.c_str());

    // Evaluation that the file exists
    if(errorCode != tinyxml2::XML_SUCCESS ){                      // opening xml file
        std::cout << ERROR0038 << " TinyXML.LoadFile method returns ID error#" << errorCode << std::endl;
        return false;
    }

    // evaluation the father node should be named doubleProportionalOvertaking
    pMap = xmlDoc.FirstChildElement("doubleProportionalOvertaking");
    if(!pMap){
        std::cout << ERROR0039 << std::endl;
        return false;
    }
    pMapBase = pMap;

    // evaluate the configuration
    pMap = pMap->FirstChildElement( "config" );
    if(!pMap){
        printf("%s", ERROR0040);
        return false;
    }

    if(pMap->QueryDoubleAttribute ("delayPrediction" , &this->delayPrediction ) != 0 ){
        printf("%s", ERROR0041);
        return false;
	}

	if(pMap->QueryDoubleAttribute ("mpcTime" , &this->mpcTime ) != 0 ){
        printf("%s", ERROR0048);
        return false;
	}

    if(pMap->QueryDoubleAttribute ("safetyDistance" , &this->safetyDistance ) != 0 ){
        printf("%s", ERROR0042);
        return false;
	}

	std::string dumbString = pMap->Attribute("overtakingToRight");
    if( dumbString == "" ){
        printf("%s", ERROR0043);
        return 0;
    }else{
        this->overtakingToRight = false;
        if(dumbString == "true" || dumbString == "True" || dumbString=="TRUE")
            this->overtakingToRight = true;
    }


	// evaluate the lateral configuration
    pMap = pMapBase->FirstChildElement( "vehicleLat" );
    if(!pMap){
        printf("%s", ERROR0049);
        return false;
    }

	if(pMap->QueryDoubleAttribute ("minLatSpeed" , &this->minLatSpeed ) != 0 ){
        printf("%s", ERROR0059);
        return false;
	}

    if(pMap->QueryDoubleAttribute ("maxLatSpeed" , &this->maxLatSpeed ) != 0 ){
        printf("%s", ERROR0050);
        return false;
	}

	if(pMap->QueryDoubleAttribute ("minLatAcc" , &this->minLatAcc ) != 0 ){
        printf("%s", ERROR0060);
        return false;
	}

	if(pMap->QueryDoubleAttribute ("maxLatAcc" , &this->maxLatAcc ) != 0 ){
        printf("%s", ERROR0051);
        return false;
	}

	// evaluate the longitudinal configuration
	pMap = pMapBase->FirstChildElement( "vehicleLon" );
    if(!pMap){
        printf("%s", ERROR0052);
        return false;
    }

    if(pMap->QueryDoubleAttribute ("minLonPos" , &this->minLonPos ) != 0 ){
        printf("%s", ERROR0053);
        return false;
	}

	if(pMap->QueryDoubleAttribute ("minLonSpeed" , &this->minLonSpeed ) != 0 ){
        printf("%s", ERROR0054);
        return false;
	}

	if(pMap->QueryDoubleAttribute ("maxLonSpeed" , &this->maxLonSpeed ) != 0 ){
        printf("%s", ERROR0055);
        return false;
	}

	if(pMap->QueryDoubleAttribute ("minLonAcc" , &this->minLonAcc ) != 0 ){
        printf("%s", ERROR0056);
        return false;
	}

	if(pMap->QueryDoubleAttribute ("maxLonAcc" , &this->maxLonAcc ) != 0 ){
        printf("%s", ERROR0057);
        return false;
	}

	if(pMap->QueryDoubleAttribute ("lonJerk" , &this->lonJerk ) != 0 ){
        printf("%s", ERROR0058);
        return false;
	}



	// The map trajectory for the projections 
    pMap = pMapBase->FirstChildElement( "map" );
    if(!pMap){
        printf("%s", ERROR0044);
        return false;
    }

    pMap = pMap->FirstChildElement( "node" );
    if(!pMap){
        printf("%s", ERROR0045);
        return false;
    }
    // After all the error diagnosis the file is completely read
	this->clear();
    tri::ad::planning::SimpleMapNode node;
    int i = 1;
    while(pMap){
        node.id = i;
		if(pMap->QueryDoubleAttribute ("x" , &node.x ) != 0 ){
            printf("%s(Node#%d).", ERROR0046,i);
			return false;
		}
		if(pMap->QueryDoubleAttribute ("y" , &node.y ) != 0 ){
			printf("%s(Node#%d).", ERROR0047,i);
			return false;
		}
        this->obsTrajectory.appendNode(node);       // adding the node to the list
        this->obsTrajectoryInvert.appendNode(node);
        i = i + 1;
        pMap = pMap->NextSiblingElement("node");
    }
    this->obsTrajectory.yawCalcInLoop();
    this->obsTrajectoryInvert.reverseNodes();
    this->obsTrajectoryInvert.reIndexing();
    this->obsTrajectoryInvert.yawCalcInLoop();

    return true;
}


int		DoubleProportionalOvertaking::getNumberObstacles(){
	return (int)this->objs.size();
}


void	DoubleProportionalOvertaking::appendObstacle(int vehIDNumber, double frontalOffset, double rearOffset, double rightOffset, double leftOffset, const Eigen::Vector3d & refPosition, double yaw,	double speed, double acc){
    vehicleModel      vehicle;      // vehicle definition
    vehiclePrediction vehStateNode; // vehicle state node
    vehStateNode.refPosition << refPosition;
	vehStateNode.yaw         =  yaw;
	vehStateNode.speed       =  speed;
	vehStateNode.acc         =  acc;
	Eigen::Vector3d vFront   = frontalOffset*Eigen::Vector3d(cos(yaw         ), sin(yaw         ), 0);
    Eigen::Vector3d vBack    =    rearOffset*Eigen::Vector3d(cos(yaw + M_PI  ), sin(yaw + M_PI  ), 0);
    Eigen::Vector3d vRight   =   rightOffset*Eigen::Vector3d(cos(yaw + M_PI/2), sin(yaw + M_PI/2), 0);
    Eigen::Vector3d vLeft    =    leftOffset*Eigen::Vector3d(cos(yaw - M_PI/2), sin(yaw - M_PI/2), 0);
	vehStateNode.cornerRL    << vehStateNode.refPosition + vBack  + vLeft;
	vehStateNode.cornerRR    << vehStateNode.refPosition + vBack  + vRight;
	vehStateNode.cornerFR    << vehStateNode.refPosition + vFront + vRight;
	vehStateNode.cornerFL    << vehStateNode.refPosition + vFront + vLeft;
	vehicle.trajectoryInvert = false;
    vehicle.lastIdx          = -1;
    vehicle.vehIDNumber      = vehIDNumber;
    vehicle.frontalOffset    = frontalOffset;
	vehicle.rearOffset       = rearOffset;
	vehicle.rightOffset      = rightOffset;
	vehicle.leftOffset       = leftOffset;

    vehicle.states.clear();
    vehicle.states.push_back(vehStateNode);

	// it is calculated the shortest distance of the obstacle to the trajectory
    double          closestDistance;
    Eigen::Vector3d closestPnt;
    int             idx = obsTrajectory.shortDistPointInLoop(0, 32000, true, vehStateNode.refPosition, closestDistance, closestPnt);

	// in case of the obstacle is inverted the trajectory associated is inverted.
    Eigen::Vector3d vDirector1( cos(obsTrajectory.getNodeInfo(idx).yaw) , sin(obsTrajectory.getNodeInfo(idx).yaw) , 0);
    Eigen::Vector3d vDirector2( cos(vehStateNode.yaw)                   , sin(vehStateNode.yaw)                   , 0);
    vehicle.lastIdx            = idx;
    if(vDirector1.dot(vDirector2) < 0){
        idx =  obsTrajectoryInvert.shortDistPointInLoop(0, 32000, true, vehStateNode.refPosition, closestDistance, closestPnt);
        vehicle.lastIdx = idx;
        vehicle.trajectoryInvert = true;
    }

    this->objs.push_back(vehicle);
}


void	DoubleProportionalOvertaking::appendPredictionObs(int obsIdx, const Eigen::Vector3d & refPosition, double yaw, double speed, double acc){
    if( obsIdx >= 0 && obsIdx < (int)this->objs.size() ){
        vehiclePrediction vehStateNode; // vehicle state node

		// append future predictions of the obstacle based on a kinematic model.
        vehStateNode.refPosition << refPosition;
        vehStateNode.yaw         =  yaw;
        vehStateNode.speed       =  speed;
        vehStateNode.acc         =  acc;
        Eigen::Vector3d vFront   = this->objs[obsIdx].frontalOffset*Eigen::Vector3d(cos(vehStateNode.yaw         ), sin(vehStateNode.yaw         ), 0);
        Eigen::Vector3d vBack    =    this->objs[obsIdx].rearOffset*Eigen::Vector3d(cos(vehStateNode.yaw + M_PI  ), sin(vehStateNode.yaw + M_PI  ), 0);
        Eigen::Vector3d vRight   =   this->objs[obsIdx].rightOffset*Eigen::Vector3d(cos(vehStateNode.yaw + M_PI/2), sin(vehStateNode.yaw + M_PI/2), 0);
        Eigen::Vector3d vLeft    =    this->objs[obsIdx].leftOffset*Eigen::Vector3d(cos(vehStateNode.yaw - M_PI/2), sin(vehStateNode.yaw - M_PI/2), 0);
        vehStateNode.cornerRL    << vehStateNode.refPosition + vBack  + vLeft;
        vehStateNode.cornerRR    << vehStateNode.refPosition + vBack  + vRight;
        vehStateNode.cornerFR    << vehStateNode.refPosition + vFront + vRight;
        vehStateNode.cornerFL    << vehStateNode.refPosition + vFront + vLeft;

        this->objs[obsIdx].states.push_back(vehStateNode);
    }
}


void	DoubleProportionalOvertaking::generatePredictionObstacles(double time, int deltaIdx){
    vehiclePrediction node;
    Eigen::Vector3d   closestPnt;
    double delayDistance;
    double yaw, closestDistance;
    int idx;
    for(int i = 0; i < this->getNumberObstacles(); i++){
        node = this->objs[i].states.back();
        if( this->objs[i].trajectoryInvert ){
            idx = obsTrajectoryInvert.shortDistPointInLoop(this->objs[i].lastIdx, deltaIdx, true, node.refPosition, closestDistance, closestPnt);
            delayDistance = node.acc*time*time/2 + node.speed*time + (Eigen::Vector3d(obsTrajectoryInvert.getNodeInfo(idx).x, obsTrajectoryInvert.getNodeInfo(idx).y, 0) - closestPnt).norm();
            idx = obsTrajectoryInvert.distanceInFront(idx, true, delayDistance,closestPnt);
            yaw = obsTrajectoryInvert.getNodeInfo(idx).yaw;
        }else{
            idx = obsTrajectory.shortDistPointInLoop(this->objs[i].lastIdx, deltaIdx, true, node.refPosition, closestDistance, closestPnt);
            delayDistance = node.acc*time*time/2 + node.speed*time + (Eigen::Vector3d(obsTrajectory.getNodeInfo(idx).x, obsTrajectory.getNodeInfo(idx).y, 0) - closestPnt).norm();
            idx = obsTrajectory.distanceInFront(idx, true, delayDistance,closestPnt);
            yaw = obsTrajectory.getNodeInfo(idx).yaw;
        }
        if( this->objs[i].states.size() == 1 ){
            this->objs[i].latDistance = closestDistance;
        }
        closestDistance = this->objs[i].latDistance;
        if(closestDistance < 0){
            node.refPosition << closestPnt(0) + fabs(closestDistance)*cos(yaw - M_PI/2), closestPnt(1) + fabs(closestDistance)*sin(yaw - M_PI/2) , 0;
        }else{
            node.refPosition << closestPnt(0) + fabs(closestDistance)*cos(yaw + M_PI/2), closestPnt(1) + fabs(closestDistance)*sin(yaw + M_PI/2) , 0;
        }
        node.speed = node.speed + node.acc*time;
        this->appendPredictionObs(i, node.refPosition, yaw, node.speed, node.acc);
    }
}


void	DoubleProportionalOvertaking::appendEgoVeh(double frontalOffset, double rearOffset, double rightOffset, double leftOffset, const Eigen::Vector3d & refPosition, double yaw,	double speed, double acc, bool leftLaneEnable, bool rightLaneEnable, double roadWidth){
    vehiclePrediction vehStateNode; // vehicle state node
    vehStateNode.refPosition     << refPosition;
	vehStateNode.yaw             =  yaw;
	vehStateNode.speed           =  speed;
	vehStateNode.acc             =  acc;
    vehStateNode.leftLaneEnable  =  leftLaneEnable;
    vehStateNode.rightLaneEnable =  rightLaneEnable;
    vehStateNode.roadWidth       =  roadWidth;

	Eigen::Vector3d vFront   = frontalOffset*Eigen::Vector3d(cos(yaw         ), sin(yaw         ), 0);
    Eigen::Vector3d vBack    =    rearOffset*Eigen::Vector3d(cos(yaw + M_PI  ), sin(yaw + M_PI  ), 0);
    Eigen::Vector3d vRight   =   rightOffset*Eigen::Vector3d(cos(yaw + M_PI/2), sin(yaw + M_PI/2), 0);
    Eigen::Vector3d vLeft    =    leftOffset*Eigen::Vector3d(cos(yaw - M_PI/2), sin(yaw - M_PI/2), 0);

	vehStateNode.cornerRL    << vehStateNode.refPosition + vBack  + vLeft;
	vehStateNode.cornerRR    << vehStateNode.refPosition + vBack  + vRight;
	vehStateNode.cornerFR    << vehStateNode.refPosition + vFront + vRight;
	vehStateNode.cornerFL    << vehStateNode.refPosition + vFront + vLeft;

	this->egoVeh.trajectoryInvert = false;
    this->egoVeh.lastIdx          = 0;
    this->egoVeh.vehIDNumber      = 0;
    this->egoVeh.frontalOffset    = frontalOffset;
	this->egoVeh.rearOffset       = rearOffset;
	this->egoVeh.rightOffset      = rightOffset;
	this->egoVeh.leftOffset       = leftOffset;

    this->egoVeh.states.clear();
    this->egoVeh.states.push_back(vehStateNode);
}



void	DoubleProportionalOvertaking::appendPredictionEgoVeh(const Eigen::Vector3d & refPosition, double yaw, double speed, double acc, bool leftLaneEnable, bool rightLaneEnable, double roadWidth){
    vehiclePrediction vehStateNode; // vehicle state node

    vehStateNode.refPosition << refPosition;
    vehStateNode.yaw             =  yaw;
    vehStateNode.speed           =  speed;
    vehStateNode.acc             =  acc;
    vehStateNode.leftLaneEnable  = leftLaneEnable;
    vehStateNode.rightLaneEnable = rightLaneEnable;
    vehStateNode.roadWidth       = roadWidth;

    Eigen::Vector3d vFront   = (this->egoVeh.frontalOffset + this->safetyDistance)*Eigen::Vector3d(cos(vehStateNode.yaw         ), sin(vehStateNode.yaw         ), 0);
    Eigen::Vector3d vBack    =                             this->egoVeh.rearOffset*Eigen::Vector3d(cos(vehStateNode.yaw + M_PI  ), sin(vehStateNode.yaw + M_PI  ), 0);
    Eigen::Vector3d vRight   =                            this->egoVeh.rightOffset*Eigen::Vector3d(cos(vehStateNode.yaw + M_PI/2), sin(vehStateNode.yaw + M_PI/2), 0);
    Eigen::Vector3d vLeft    =                             this->egoVeh.leftOffset*Eigen::Vector3d(cos(vehStateNode.yaw - M_PI/2), sin(vehStateNode.yaw - M_PI/2), 0);
    vehStateNode.cornerRL    << vehStateNode.refPosition + vBack  + vLeft;
    vehStateNode.cornerRR    << vehStateNode.refPosition + vBack  + vRight;
    vehStateNode.cornerFR    << vehStateNode.refPosition + vFront + vRight;
    vehStateNode.cornerFL    << vehStateNode.refPosition + vFront + vLeft;

    this->egoVeh.states.push_back(vehStateNode);
}


void    DoubleProportionalOvertaking::generatePredictionEgoVeh(int startIdx, int deltaIdx, std::vector<double> distance, tri::ad::planning::LocalMap bufferMap){

    vehiclePrediction node;

    Eigen::Vector3d closestPnt;
    double          closestDistance;
    double          delayDistance;
    double          yaw;
    int             idx, idxTemp;
    node          = this->egoVeh.states[0];
    bool            leftLaneEnable, rightLaneEnable;
    idx           = bufferMap.shortDistPointInLoop(startIdx, deltaIdx, false, this->egoVeh.states[0].refPosition, closestDistance, closestPnt);
    delayDistance = ( Eigen::Vector3d( bufferMap.getNodeInfo(idx).x , bufferMap.getNodeInfo(idx).y , 0 ) - closestPnt ).norm();
    for(int i = 0; i < (int)distance.size(); i++){
        idxTemp          = bufferMap.distanceInFront(idx, false, delayDistance + distance[i] ,closestPnt);
        yaw              = bufferMap.getNodeInfo(idxTemp).yaw;
        node.speed       = bufferMap.getNodeInfo(idxTemp).maxSpeed;
        node.refPosition = closestPnt;
        leftLaneEnable  = false;
        rightLaneEnable = false;
        if(bufferMap.getNodeInfo(idxTemp).nLanesLeft > 0){
            leftLaneEnable = true;
        }
        if(bufferMap.getNodeInfo(idxTemp).nLanesRight > 0){
            rightLaneEnable = true;
        }
        this->appendPredictionEgoVeh(node.refPosition, yaw, node.speed, node.acc, leftLaneEnable, rightLaneEnable, bufferMap.getNodeInfo(idxTemp).roadWidth);
    }
}


void	DoubleProportionalOvertaking::generateParallelEgoVeh(){
    vehiclePrediction   vehStateNode;
    Eigen::Vector3d     vec;

    this->egoVehParallel.clear();
    for(int i = 0; i < (int)this->egoVeh.states.size(); i++){
        vehStateNode =  this->egoVeh.states[i];
        if(this->overtakingToRight){
            vec << cos(vehStateNode.yaw - M_PI/2) , sin(vehStateNode.yaw - M_PI/2) , 0;
        }else{
            vec << cos(vehStateNode.yaw + M_PI/2) , sin(vehStateNode.yaw + M_PI/2) , 0;
        }
        vehStateNode.refPosition = vehStateNode.refPosition + vehStateNode.roadWidth*vec;
        vehStateNode.cornerRL    = vehStateNode.cornerRL    + vehStateNode.roadWidth*vec;
        vehStateNode.cornerRR    = vehStateNode.cornerRR    + vehStateNode.roadWidth*vec;
        vehStateNode.cornerFR    = vehStateNode.cornerFR    + vehStateNode.roadWidth*vec;
        vehStateNode.cornerFL    = vehStateNode.cornerFL    + vehStateNode.roadWidth*vec;
        this->egoVehParallel.push_back(vehStateNode);
    }

}

void    DoubleProportionalOvertaking::verifyCollision(){
    this->colissionLane.clear();
    this->colissionParallelLane.clear();

    tri::ad::math::Square s0, s1, s2;
    int i, j;
    for(i = 0; i < (int)this->egoVeh.states.size(); i++){
        this->colissionLane.push_back(0);
        this->colissionParallelLane.push_back(0);
    }

    for( j = 0; j < this->objs.size(); j++){
        for( i = 0; i < (int)this->egoVeh.states.size(); i++){

            s0.p0 = this->egoVeh.states[i].cornerRL;
            s0.p1 = this->egoVeh.states[i].cornerRR;
            s0.p2 = this->egoVeh.states[i].cornerFR;
            s0.p3 = this->egoVeh.states[i].cornerFL;

            s1.p0 = this->egoVehParallel[i].cornerRL;
            s1.p1 = this->egoVehParallel[i].cornerRR;
            s1.p2 = this->egoVehParallel[i].cornerFR;
            s1.p3 = this->egoVehParallel[i].cornerFL;


            s2.p0 = this->objs[j].states[i].cornerRL;
            s2.p1 = this->objs[j].states[i].cornerRR;
            s2.p2 = this->objs[j].states[i].cornerFR;
            s2.p3 = this->objs[j].states[i].cornerFL;

            if( tri::ad::math::squareIntersect(s0, s2) || this->colissionLane[i]){
                this->colissionLane[i] = 1 ;
            }else{
                this->colissionLane[i] = 0 ;
            }
            if( tri::ad::math::squareIntersect(s1, s2) || this->colissionParallelLane[i] ){
                this->colissionParallelLane[i] = 1 ;
            }else{
                this->colissionParallelLane[i] = 0 ;
            }

        }

    }

}


void	DoubleProportionalOvertaking::generateSpeedReference(){
    this->speedReference.clear();
    for(int i = 0; i < this->egoVeh.states.size(); i++){
        this->speedReference.push_back(std::max(0.0,this->egoVeh.states[i].speed));
    }
}


void	DoubleProportionalOvertaking::generateLatOffsetReference(){
    this->latOffsetReference.clear();
    for(int i = 0; i < this->egoVeh.states.size(); i++){
        this->latOffsetReference.push_back(0.0);
    }
}


void 	DoubleProportionalOvertaking::generateLatBoundPos(){
    this->latBoundPos.lowerBounds.clear();
    this->latBoundPos.upperBounds.clear();
    for(int i = 0; i<this->latOffsetReference.size(); i++){
        this->latBoundPos.lowerBounds.push_back( -std::max(this->egoVeh.states[i].roadWidth,1.0)/2   );
        this->latBoundPos.upperBounds.push_back(  std::max(this->egoVeh.states[i].roadWidth,1.0)*3/2 );
    }
}


void 	DoubleProportionalOvertaking::generateLatBoundSpeed(double maxSpeed, double minSpeed, double latPosState0, double latSpeedState0){
    double delta         = fabs( this->egoVeh.states[0].roadWidth/2 - std::max( std::min( this->egoVeh.states[0].roadWidth , fabs(latPosState0) ) , 0.0 ) );
    double maxSpeedBound = fabs( maxSpeed + (minSpeed - maxSpeed)*(delta)/(this->egoVeh.states[0].roadWidth/2) );
    maxSpeedBound = std::max( maxSpeedBound , latSpeedState0 );
    this->latBoundSpeed.lowerBounds.clear();
    this->latBoundSpeed.upperBounds.clear();
    for(int i = 0; i<this->latOffsetReference.size(); i++){
        this->latBoundSpeed.lowerBounds.push_back( -fabs(maxSpeedBound) );
        this->latBoundSpeed.upperBounds.push_back(  fabs(maxSpeedBound) );
    }
}


void 	DoubleProportionalOvertaking::generateLatBoundAcc(double maxLatAcc, double minLatAcc, double latPosState0, double latAccState0){
    double delta       = fabs( this->egoVeh.states[0].roadWidth/2 - std::max( std::min( this->egoVeh.states[0].roadWidth , fabs(latPosState0) ) , 0.0 ) );
    double maxAccBound = fabs( maxLatAcc + (minLatAcc - maxLatAcc)*(delta)/(this->egoVeh.states[0].roadWidth/2) );
    maxAccBound = std::max( maxAccBound , latAccState0 );
    this->latBoundAcc.lowerBounds.clear();
    this->latBoundAcc.upperBounds.clear();
    for(int i = 0; i<this->latOffsetReference.size(); i++){
        this->latBoundAcc.lowerBounds.push_back( -fabs(maxAccBound) );
        this->latBoundAcc.upperBounds.push_back(  fabs(maxAccBound) );
    }
}


void 	DoubleProportionalOvertaking::generateLonBoundPos(double maxDistance, double minDistance){
    this->lonBoundPos.lowerBounds.clear();
    this->lonBoundPos.upperBounds.clear();
    for(int i = 0; i<this->speedReference.size(); i++){
        this->lonBoundPos.lowerBounds.push_back(-fabs(minDistance));
        this->lonBoundPos.upperBounds.push_back( fabs(maxDistance));
    }
}


void 	DoubleProportionalOvertaking::generateLonBoundSpeed(double addUpperLimit, double minimumSpeed){
    this->lonBoundSpeed.lowerBounds.clear();
    this->lonBoundSpeed.upperBounds.clear();
    for(int i = 0; i<this->speedReference.size(); i++){
        this->lonBoundSpeed.lowerBounds.push_back(minimumSpeed);
        this->lonBoundSpeed.upperBounds.push_back(this->speedReference[i] + fabs(addUpperLimit));
    }
}


void 	DoubleProportionalOvertaking::generateLonBoundAcc(double maxAcc, double maxDec){
    this->lonBoundAcc.lowerBounds.clear();
    this->lonBoundAcc.upperBounds.clear();
    for(int i = 0; i<this->speedReference.size(); i++){
        this->lonBoundAcc.lowerBounds.push_back(-fabs(maxDec));
        this->lonBoundAcc.upperBounds.push_back(fabs(maxAcc));
    }
}

void 	DoubleProportionalOvertaking::generateLonBoundJerk(double maxJerks){
    this->lonBoundJerk.lowerBounds.clear();
    this->lonBoundJerk.upperBounds.clear();
    for(int i = 0; i<this->speedReference.size(); i++){
        this->lonBoundJerk.lowerBounds.push_back(-fabs(maxJerks));
        this->lonBoundJerk.upperBounds.push_back( fabs(maxJerks));
    }
}


double	DoubleProportionalOvertaking::interpolateLateralOffset(double timestep, double time){
    if( (int)this->latStatePos.size() < 1){
        return 0;
    }

    if( time > ( (int)this->latStatePos.size() - 1 )*timestep ){
        return this->latStatePos.back();
    }

    double delta;
    for(int i = 0; i < ( (int)this->latStatePos.size() - 1 ) ; i++ ){
        if( time >= i*timestep && time <= ( i + 1 )*timestep ){
            delta  = (time - i*timestep)/timestep;
            return this->latStatePos[i] + delta*(this->latStatePos[i+1] - this->latStatePos[i]);
        }
    }
    return 0.0;
}

double	DoubleProportionalOvertaking::interpolateLateralSpeed(double timestep, double time){
    if( (int)this->latStateSpeed.size() < 1){
        return 0;
    }

    if( time > ( (int)this->latStateSpeed.size() - 1 )*timestep ){
        return this->latStateSpeed.back();
    }

    double delta;
    for(int i = 0; i < ( (int)this->latStateSpeed.size() - 1 ) ; i++ ){
        if( time >= i*timestep && time <= ( i + 1 )*timestep ){
            delta  = (time - i*timestep)/timestep;
            return this->latStateSpeed[i] + delta*(this->latStateSpeed[i+1] - this->latStateSpeed[i]);
        }
    }
    return 0.0;
}

double	DoubleProportionalOvertaking::interpolateLateralAcc(double timestep, double time){
    if( (int)this->latStateAcc.size() < 1){
        return 0;
    }

    if( time > ( (int)this->latStateAcc.size() - 1 )*timestep ){
        return this->latStateAcc.back();
    }

    double delta;
    for(int i = 0; i < ( (int)this->latStateAcc.size() - 1 ) ; i++ ){
        if( time >= i*timestep && time <= ( i + 1 )*timestep ){
            delta  = (time - i*timestep)/timestep;
            return this->latStateAcc[i] + delta*(this->latStateAcc[i+1] - this->latStateAcc[i]);
        }
    }
    return 0.0;
}


double	DoubleProportionalOvertaking::interpolateSpeed(double timestep, double time){
    if( (int)this->lonStateSpeed.size() < 1){
        return 0;
    }

    if( time > ( (int)this->lonStateSpeed.size() - 1 )*timestep ){
        return this->lonStateSpeed.back();
    }

    double delta;
    for(int i = 0; i < ( (int)this->lonStateSpeed.size() - 1 ) ; i++ ){
        if( time >= i*timestep && time <= ( i + 1 )*timestep ){
            delta  = (time - i*timestep)/timestep;
            return this->lonStateSpeed[i] + delta*(this->lonStateSpeed[i+1] - this->lonStateSpeed[i]);
        }
    }
    return 0.0;
}


double	DoubleProportionalOvertaking::interpolateAcc(double timestep, double time){
    if( (int)this->lonStateAcc.size() < 1){
        return 0;
    }

    if( time > ( (int)this->lonStateAcc.size() - 1 )*timestep ){
        return this->lonStateAcc.back();
    }

    double delta;
    for(int i = 0; i < ( (int)this->lonStateAcc.size() - 1 ) ; i++ ){
        if( time >= i*timestep && time <= ( i + 1 )*timestep ){
            delta  = (time - i*timestep)/timestep;
            return this->lonStateAcc[i] + delta*(this->lonStateAcc[i+1] - this->lonStateAcc[i]);
        }
    }
    return 0.0;
}


void    DoubleProportionalOvertaking::overtakingReferencesBounds(){
    int     totalIterations = (int)this->colissionLane.size();
    int     i;
    double  maxDistance = 9999999;
    int     indexRefChange = -1;
    int     indexLonChange = totalIterations;
    double  width = this->egoVeh.leftOffset + this->egoVeh.rightOffset;
    for(i=0; i < totalIterations; i++){
        // los bounds lateral son colocados en funci�n de los obstaculos
        if( this->colissionLane[i] == 1){
            // en caso de colision
            this->latBoundPos.lowerBounds[i] = this->egoVeh.states[i].roadWidth - width/2;
            if(indexRefChange < 0){ // this is used to move the reference fast.
                indexRefChange = i;
            }
        }else{
            // en caso de no colision en la linea original
            this->latBoundPos.lowerBounds[i] = -this->egoVeh.states[i].roadWidth/2 + width/2;
        }

        if( this->colissionParallelLane[i] == 1  || (this->overtakingToRight && !this->egoVeh.states[i].rightLaneEnable) || (!this->overtakingToRight && !this->egoVeh.states[i].leftLaneEnable)){
            // en caso de colision en el otro carril
            this->latBoundPos.upperBounds[i] = width/2;
        }else{
            // en caso de no colisionar en el otro carril
            this->latBoundPos.upperBounds[i] = 3*this->egoVeh.states[i].roadWidth/2 - width/2;
        }

        // Lateral position over upper bound or less than the lower
        if( this->latBoundPos.upperBounds[i] + 1.0 < this->latStatePos[i] || this->latBoundPos.lowerBounds[i]-1.0 > this->latStatePos[i] ){
            if(i < indexLonChange){
                indexLonChange = i;
            }
            maxDistance = std::max( 0.0 , std::min( this->lonStatePos[i] , maxDistance ) );
        }

        // Tambien se debe evaluar que el sistema genere cambios en el upper/lower bound en dos 3 muestras
        /*if( this->latBoundPos.upperBounds[i] < this->latBoundPos.lowerBounds[std::min(totalIterations-1 , i+1)] ||
              this->latBoundPos.lowerBounds[i] > this->latBoundPos.upperBounds[std::min(totalIterations-1 , i+1)] ){

            if(i < indexLonChange){
                indexLonChange = i;
            }
            maxDistance = std::max( 0.0 , std::min( this->lonStatePos[i] , maxDistance ) );
        }*/
    }



    // close distance between two
    int possibleColissionIdx   = totalIterations;
    int colissionLaneFirstIdx1 = totalIterations;
    int colissionLaneFirstIdx2 = totalIterations;
    int otherLaneFirstIdx1     = totalIterations;
    int otherLaneFirstIdx2     = totalIterations;
    for(i=0; i < totalIterations; i++){
        if( this->colissionLane[i] == 1 && colissionLaneFirstIdx1 >= totalIterations ){
            colissionLaneFirstIdx1 = i;
        }
        if( this->colissionLane[i] == 0 && colissionLaneFirstIdx1 <  totalIterations && colissionLaneFirstIdx2 >= totalIterations ){
            colissionLaneFirstIdx2 = i;
        }
        if( this->colissionParallelLane[i] == 1 && otherLaneFirstIdx1 >= totalIterations ){
            otherLaneFirstIdx1 = i;
        }
        if( this->colissionParallelLane[i] == 0 && otherLaneFirstIdx1 <  totalIterations && otherLaneFirstIdx2 >= totalIterations ){
            otherLaneFirstIdx2 = i;
        }
    }
    int  delta = 2;
    if( colissionLaneFirstIdx1 < totalIterations && otherLaneFirstIdx1 < totalIterations ){
        if(colissionLaneFirstIdx1                                        <= otherLaneFirstIdx1&&
           std::max( colissionLaneFirstIdx1 - delta , 0)                 <= otherLaneFirstIdx1&&
           std::min( colissionLaneFirstIdx2 + delta , totalIterations-1) >= otherLaneFirstIdx1){
            possibleColissionIdx = colissionLaneFirstIdx1;
        }
        if(colissionLaneFirstIdx1                                        >  otherLaneFirstIdx1    &&
           std::max( otherLaneFirstIdx1 - delta , 0)                     <= colissionLaneFirstIdx1&&
           std::min( otherLaneFirstIdx2 + delta , totalIterations-1)     >= colissionLaneFirstIdx1){
            possibleColissionIdx = otherLaneFirstIdx1;
        }
        if(indexRefChange >= possibleColissionIdx){
            indexRefChange = -1;
        }
    }

        // Start of the reference (displace)
    for(i=0; i < totalIterations; i++){
        if( i <= indexRefChange || this->latBoundPos.lowerBounds[i] > 0 ){
            this->latOffsetReference[i] = this->egoVeh.states[i].roadWidth;
        }
    }

    // Distance Limitation
    int flagColission = 0;
    for(i=0; i < totalIterations; i++){
        if( i >= possibleColissionIdx){
            this->latBoundPos.upperBounds[i] =   3*this->egoVeh.states[i].roadWidth/2 - width/2;
            this->latBoundPos.lowerBounds[i] =  -1*this->egoVeh.states[i].roadWidth/2 + width/2;
            this->latOffsetReference[i]      =  0;
        }
        if( this->latBoundPos.lowerBounds[i] >= this->latBoundPos.upperBounds[i]){

            this->latBoundPos.upperBounds[i] =  3*this->egoVeh.states[i].roadWidth/2 - width/2;
            this->latBoundPos.lowerBounds[i] = -1*this->egoVeh.states[i].roadWidth/2 + width/2;
            this->latOffsetReference[i]      = 0;
            flagColission = 1;
        }
        if(i >= indexLonChange || flagColission == 1 ){
            this->lonBoundPos.upperBounds[i] = std::min(this->lonBoundPos.upperBounds[i], maxDistance);
            this->speedReference[i]          = 0;
        }
    }

    // number of references


}

} /* end of control */
} /* end of ad      */
} /* end of tri     */
