
#ifndef MATLAB_MEX_FILE
	#define MATLAB_MEX_FILE
	#define REMOVE_MATLAB_MEX_FILE
#endif
#include "simstruc.h"
#ifndef REMOVE_MATLAB_MEX_FILE
	#undef MATLAB_MEX_FILE
	#undef REMOVE_MATLAB_MEX_FILE
#endif

#include "..\inc\Lib_Control.h"
#include "..\inc\Lib_CSVAndListManagement.h"
#include "..\inc\Lib_Planning.h"
#include "..\inc\Lib_Math.h"
#include "..\inc\Lib_VehicleState.h"
#include "..\inc\Lib_Communications.h"
#define DynaBuffer_CarPC      		7
#define DynaBuffer_HMI       		41
#define DynaBuffer_Commands   		3
#define DynaBuffer_VehState  		61
#define DynaBuffer_Planning  		22
#define DynaBuffer_Node      		20
#define DynaBuffer_NElemBuff 		11
#define DynaBuffer_Comm      		12
#define DynaBuffer_LaserMaxPoints	2500
#define DynaBuffer_LaserMaxSquares	5000


#define DynaObstacle_Buffer 20

// *****************************************************************************************************
// ** Definicion de las entradas en matlab                                                            **
// *****************************************************************************************************

TRI_VehicleState_Input ExtractCarPC(InputRealPtrsType inCarPC){
	TRI_VehicleState_Input EgoVehicleIn;
	
	EgoVehicleIn.X            = (double)*inCarPC[0]; // X
    EgoVehicleIn.Y            = (double)*inCarPC[1]; // Y
	EgoVehicleIn.CarSpeed     = (int)   *inCarPC[2]; // Speed of the car
	EgoVehicleIn.Ctrl         = (short) *inCarPC[3];
    EgoVehicleIn.HeadingAngle = (float) *inCarPC[4]; // Yaw
    EgoVehicleIn.Empty1       = (float) *inCarPC[5]; // Empty space
    EgoVehicleIn.WheelAngle   = (double)*inCarPC[6]; // Wheel angle
	
	return EgoVehicleIn;
}

TRI_VehicleState_Config ExtractHMIConfig(InputRealPtrsType inHMI){
	TRI_VehicleState_Config EgoVehicleConfig;

    EgoVehicleConfig.Long      = (float) *inHMI[ 0];            // Long in meters
    EgoVehicleConfig.Width     = (float) *inHMI[ 1];           // Width in meters
    EgoVehicleConfig.Height    = (float) *inHMI[ 2];          // Height in meters
    EgoVehicleConfig.Wheelbase = (float) *inHMI[ 3];       // in spanish Batalla in meters
    EgoVehicleConfig.Track     = (float) *inHMI[ 4];           // the width between the middle of the tires in meters
    EgoVehicleConfig.Weight    = (float) *inHMI[ 5];          // in kilograms
    EgoVehicleConfig.WeightFL  = (float) *inHMI[ 6];
    EgoVehicleConfig.WeightFR  = (float) *inHMI[ 7];
    EgoVehicleConfig.WeightRL  = (float) *inHMI[ 8];
    EgoVehicleConfig.WeightRR  = (float) *inHMI[ 9];
    
    EgoVehicleConfig.GPSOffset_Front    = (float) *inHMI[10];
    EgoVehicleConfig.GPSOffset_Back     = (float) *inHMI[11];
    EgoVehicleConfig.GPSOffset_Left     = (float) *inHMI[12];  // from the front view of the car
    EgoVehicleConfig.GPSOffset_Right    = (float) *inHMI[13]; // from the front view of the car
    EgoVehicleConfig.IMUFrontal         = (float) *inHMI[14];
    EgoVehicleConfig.IMULateral         = (float) *inHMI[15];
    EgoVehicleConfig.IMUAngleCorrection = (float) *inHMI[16];
    EgoVehicleConfig.IMUHeight          = (float) *inHMI[17];
    
    EgoVehicleConfig.PlanningRightTraffic = (  int) *inHMI[18];    //
    EgoVehicleConfig.PlanningRoadWidth    = (float) *inHMI[19];
    EgoVehicleConfig.PlanningPathMode     = (  int) *inHMI[20];
    EgoVehicleConfig.PlanningEndPnt.x     = (float) *inHMI[21];
    EgoVehicleConfig.PlanningEndPnt.y     = (float) *inHMI[22];
    
    EgoVehicleConfig.ControlTimestamp     = (  int) *inHMI[23];     // miliseconds
    EgoVehicleConfig.ControlLat_Timestamp = (  int) *inHMI[24];
    EgoVehicleConfig.ControlLat_Algorithm = (  int) *inHMI[25];
    EgoVehicleConfig.Lookahead            = (float) *inHMI[26];
    EgoVehicleConfig.ControlLat_Kp        = (float) *inHMI[27];
    EgoVehicleConfig.ControlLat_Ki        = (float) *inHMI[28];
    EgoVehicleConfig.ControlLat_Kd        = (float) *inHMI[29];
	
    EgoVehicleConfig.ControlLong_Timestamp = (  int) *inHMI[30];
    EgoVehicleConfig.ControlLong_Algorithm = (  int) *inHMI[31];
    EgoVehicleConfig.ControlLong_ExtSp     = (  int) *inHMI[32];
    EgoVehicleConfig.ControlLong_MaxSP     = (float) *inHMI[33];
    EgoVehicleConfig.ControlLong_Kp        = (float) *inHMI[34];
    EgoVehicleConfig.ControlLong_Ki        = (float) *inHMI[35];
    EgoVehicleConfig.ControlLong_Kd        = (float) *inHMI[36];
   
	return EgoVehicleConfig;
}

TRI_VehicleState_Commands ExtractHMICmd(InputRealPtrsType inCmd){
	TRI_VehicleState_Commands EgoVehicleCmd;
	EgoVehicleCmd.Run_Stop       = (int)   *inCmd[37];
    EgoVehicleCmd.Emergency_Stop = (int)   *inCmd[38];
    EgoVehicleCmd.SPSpeed        = (double)*inCmd[39];
    EgoVehicleCmd.PathMode       = (int)   *inCmd[40];
	return EgoVehicleCmd;
}

TRI_VehicleState ExtractVehicleState(InputRealPtrsType inEgoVehState){
    TRI_VehicleState EgoVehicleState;
    
    EgoVehicleState.I.X =  (double)*inEgoVehState[0];
    EgoVehicleState.I.Y =  (double)*inEgoVehState[1];
    EgoVehicleState.I.CarSpeed =  (int)   *inEgoVehState[2];
    EgoVehicleState.I.Ctrl =  (short) *inEgoVehState[3];
    EgoVehicleState.I.HeadingAngle =  (float) *inEgoVehState[4];
    EgoVehicleState.I.Empty1 =  (float) *inEgoVehState[5];
    EgoVehicleState.I.WheelAngle =  (double)*inEgoVehState[6];

    EgoVehicleState.C.Long           = (float)*inEgoVehState[7];
    EgoVehicleState.C.Width      = (float)*inEgoVehState[8];
    EgoVehicleState.C.Height     = (float)*inEgoVehState[9];
    EgoVehicleState.C.Wheelbase  = (float)*inEgoVehState[10];
    EgoVehicleState.C.Track      = (float)*inEgoVehState[11];
    EgoVehicleState.C.Weight     = (float)*inEgoVehState[12];
    EgoVehicleState.C.WeightFL   = (float)*inEgoVehState[13];
    EgoVehicleState.C.WeightFR   = (float)*inEgoVehState[14];
    EgoVehicleState.C.WeightRL   = (float)*inEgoVehState[15];
    EgoVehicleState.C.WeightRR   = (float)*inEgoVehState[16];
    EgoVehicleState.C.GPSOffset_Front     = (float)*inEgoVehState[17];
    EgoVehicleState.C.GPSOffset_Back      = (float)*inEgoVehState[18];
    EgoVehicleState.C.GPSOffset_Left      = (float)*inEgoVehState[19];
    EgoVehicleState.C.GPSOffset_Right     = (float)*inEgoVehState[20];
    EgoVehicleState.C.IMUFrontal          = (float)*inEgoVehState[21];
    EgoVehicleState.C.IMULateral          = (float)*inEgoVehState[22];
    EgoVehicleState.C.IMUAngleCorrection  = (float)*inEgoVehState[23];
    EgoVehicleState.C.IMUHeight           = (float)*inEgoVehState[24];
    EgoVehicleState.C.PlanningRightTraffic  = (  int)*inEgoVehState[25];
    EgoVehicleState.C.PlanningRoadWidth     = (float)*inEgoVehState[26];
    EgoVehicleState.C.PlanningPathMode      = (  int)*inEgoVehState[27];
    EgoVehicleState.C.PlanningEndPnt.x = (float)*inEgoVehState[28];
    EgoVehicleState.C.PlanningEndPnt.y = (float)*inEgoVehState[29];
    EgoVehicleState.C.ControlTimestamp      = (  int)*inEgoVehState[30];
    EgoVehicleState.C.ControlLat_Timestamp  = (int)*inEgoVehState[31];
    EgoVehicleState.C.ControlLat_Algorithm  = (  int)*inEgoVehState[32];
    EgoVehicleState.C.Lookahead             = (float)*inEgoVehState[33];
    EgoVehicleState.C.ControlLat_Kp         = (float)*inEgoVehState[34];
    EgoVehicleState.C.ControlLat_Ki         = (float)*inEgoVehState[35];
    EgoVehicleState.C.ControlLat_Kd         = (float)*inEgoVehState[36];
    EgoVehicleState.C.ControlLong_Timestamp  = (  int)*inEgoVehState[37];
    EgoVehicleState.C.ControlLong_Algorithm  = (  int)*inEgoVehState[38];
    EgoVehicleState.C.ControlLong_ExtSp      = (  int)*inEgoVehState[39];
    EgoVehicleState.C.ControlLong_MaxSP      = (float)*inEgoVehState[40];
    EgoVehicleState.C.ControlLong_Kp         = (float)*inEgoVehState[41];
    EgoVehicleState.C.ControlLong_Ki         = (float)*inEgoVehState[42];
    EgoVehicleState.C.ControlLong_Kd         = (float)*inEgoVehState[43];

    EgoVehicleState.Cmd.Run_Stop =  (int)   *inEgoVehState[44];
    EgoVehicleState.Cmd.Emergency_Stop =  (int)   *inEgoVehState[45];
    EgoVehicleState.Cmd.SPSpeed =  (double)*inEgoVehState[46];
    EgoVehicleState.Cmd.PathMode =  (int)   *inEgoVehState[47];

    EgoVehicleState.O.Sts_Run_Stop =  (double)*inEgoVehState[48];
    EgoVehicleState.O.Run_Stop_Past =  (double)*inEgoVehState[49];
    EgoVehicleState.O.StartPointX =  (double)*inEgoVehState[50];
    EgoVehicleState.O.StartPointY =  (double)*inEgoVehState[51];
    EgoVehicleState.O.EndPointX =  (double)*inEgoVehState[52];
    EgoVehicleState.O.EndPointY =  (double)*inEgoVehState[53];
    EgoVehicleState.O.CarSpeed =  (float) *inEgoVehState[54];
    EgoVehicleState.O.X =  (double)*inEgoVehState[55];
    EgoVehicleState.O.Y =  (double)*inEgoVehState[56];
    EgoVehicleState.O.HeadingAngleRad =  (float) *inEgoVehState[57];
    EgoVehicleState.O.HeadingAngleDeg =  (float) *inEgoVehState[58];
    EgoVehicleState.O.WheelBase =  (double)*inEgoVehState[59];
    EgoVehicleState.O.MaxSpeed =  (double)*inEgoVehState[60];
   
    return EgoVehicleState;
}

tri_Planning ExtractPlanning(InputRealPtrsType inPlanning){
	tri_Planning iPlanning;
	iPlanning.C.PathMode        = *inPlanning[ 0];
    iPlanning.C.RoadWidth       = *inPlanning[ 1];
    iPlanning.C.RightTraffic    = *inPlanning[ 2];
	iPlanning.C.EndPnt.x        = *inPlanning[ 3];
    iPlanning.C.EndPnt.y        = *inPlanning[ 4];
	
    iPlanning.O.RunningMode     = *inPlanning[ 5];
    iPlanning.O.TotalPoints     = *inPlanning[ 6];
    iPlanning.O.StartId         = *inPlanning[ 7];
    iPlanning.O.StartPnt.x      = *inPlanning[ 8];
    iPlanning.O.StartPnt.y      = *inPlanning[ 9];
    iPlanning.O.StartSeg.p[0].x = *inPlanning[10];
    iPlanning.O.StartSeg.p[0].y = *inPlanning[11];
    iPlanning.O.StartSeg.p[1].x = *inPlanning[12];
    iPlanning.O.StartSeg.p[1].y = *inPlanning[13];
    iPlanning.O.EndId           = *inPlanning[14];
    iPlanning.O.EndPnt.x        = *inPlanning[15];
    iPlanning.O.EndPnt.y        = *inPlanning[16];
    iPlanning.O.EndSeg.p[0].x   = *inPlanning[17];
    iPlanning.O.EndSeg.p[0].y   = *inPlanning[18];
    iPlanning.O.EndSeg.p[1].x   = *inPlanning[19];
    iPlanning.O.EndSeg.p[1].y   = *inPlanning[20];
    iPlanning.O.PathLength      = *inPlanning[21];
	return iPlanning;
}

TRI_CSV_NodeInfo ExtractNode(InputRealPtrsType inMap){
	TRI_CSV_NodeInfo Node;
	Node.Id        = *inMap[ 0];
    Node.X         = *inMap[ 1];
    Node.Y         = *inMap[ 2];
    Node.MaxSpeed  = *inMap[ 3];
    Node.Type      = *inMap[ 4];
    Node.Curvature = *inMap[ 5];
    Node.Angle_In  = *inMap[ 6];
    Node.Angle_Out = *inMap[ 7];
    
    Node.Di[0]     = *inMap[ 8];
    Node.Di[1]     = *inMap[ 9];
    Node.Di[2]     = *inMap[10];
    Node.Di[3]     = *inMap[11];
    Node.Di[4]     = *inMap[12];
    Node.Di[5]     = *inMap[13];
    
    Node.Do[0]     = *inMap[14];
    Node.Do[1]     = *inMap[15];
    Node.Do[2]     = *inMap[16];
    Node.Do[3]     = *inMap[17];
    Node.Do[4]     = *inMap[18];
    Node.Do[5]     = *inMap[19];
    
	return Node;
}

void ExtractBuffer(InputRealPtrsType In6, TRI_LocalPlanner_NodeInfo Buffer[BUFFERSIZE]){
	int i = 0;
    double x = *In6[0];
    double y = *In6[1];
    int offset = 2;
    for(i = 0; i < BUFFERSIZE; i++){
        Buffer[i].Id            = *In6[i*DynaBuffer_NElemBuff + 2 ];
        Buffer[i].Type          = *In6[i*DynaBuffer_NElemBuff + 3 ];
        Buffer[i].X             = *In6[i*DynaBuffer_NElemBuff + 4 ];
        Buffer[i].Y             = *In6[i*DynaBuffer_NElemBuff + 5 ];
        Buffer[i].MaxSpeed      = *In6[i*DynaBuffer_NElemBuff + 6 ];
        Buffer[i].Curvature     = *In6[i*DynaBuffer_NElemBuff + 7 ];
        Buffer[i].Rnd.center.x  = *In6[i*DynaBuffer_NElemBuff + 8 ];
        Buffer[i].Rnd.center.y  = *In6[i*DynaBuffer_NElemBuff + 9 ];
        Buffer[i].Rnd.radius    = *In6[i*DynaBuffer_NElemBuff + 10];
        Buffer[i].HeadingBefRad = *In6[i*DynaBuffer_NElemBuff + 11];
        Buffer[i].HeadingRad    = *In6[i*DynaBuffer_NElemBuff + 12];
    }
}

void ExtractCommNode(InputRealPtrsType inCommObject, tri_CommStruct *CommNode){
    CommNode->id             = (unsigned int)(*inCommObject[0]);
    CommNode->type           = (unsigned int)(*inCommObject[1]);
    CommNode->x              = (signed long) (*inCommObject[2]);
    CommNode->y              = (signed long) (*inCommObject[3]);
    CommNode->heading        = (signed int)  (*inCommObject[4]);
    CommNode->speed          = (signed int)  (*inCommObject[5]);
    CommNode->datetime.year  = (unsigned int)(*inCommObject[6]);
    CommNode->datetime.month = (unsigned int)(*inCommObject[7]);
    CommNode->datetime.day   = (unsigned int)(*inCommObject[8]);
    CommNode->datetime.hour  = (unsigned int)(*inCommObject[9]);
    CommNode->datetime.min   = (unsigned int)(*inCommObject[10]);
    CommNode->datetime.sec   = (unsigned int)(*inCommObject[11]);
}

// *****************************************************************************************************
// ** Definicion de las salidas en matlab                                                             **
// *****************************************************************************************************

void OutputVehState(TRI_VehicleState EgoVehicleState, real_T *oEgoVehState){
	oEgoVehState[0] = EgoVehicleState.I.X;
    oEgoVehState[1] = EgoVehicleState.I.Y;
    oEgoVehState[2] = EgoVehicleState.I.CarSpeed;
    oEgoVehState[3] = EgoVehicleState.I.Ctrl;
    oEgoVehState[4] = EgoVehicleState.I.HeadingAngle;
    oEgoVehState[5] = EgoVehicleState.I.Empty1;
    oEgoVehState[6] = EgoVehicleState.I.WheelAngle;

    oEgoVehState[7] = EgoVehicleState.C.Long          ;
    oEgoVehState[8] = EgoVehicleState.C.Width     ;
    oEgoVehState[9] = EgoVehicleState.C.Height    ;
    oEgoVehState[10] = EgoVehicleState.C.Wheelbase ;
    oEgoVehState[11] = EgoVehicleState.C.Track     ;
    oEgoVehState[12] = EgoVehicleState.C.Weight    ;
    oEgoVehState[13] = EgoVehicleState.C.WeightFL  ;
    oEgoVehState[14] = EgoVehicleState.C.WeightFR  ;
    oEgoVehState[15] = EgoVehicleState.C.WeightRL  ;
    oEgoVehState[16] = EgoVehicleState.C.WeightRR  ;
    oEgoVehState[17] = EgoVehicleState.C.GPSOffset_Front    ;
    oEgoVehState[18] = EgoVehicleState.C.GPSOffset_Back     ;
    oEgoVehState[19] = EgoVehicleState.C.GPSOffset_Left     ;
    oEgoVehState[20] = EgoVehicleState.C.GPSOffset_Right    ;
    oEgoVehState[21] = EgoVehicleState.C.IMUFrontal         ;
    oEgoVehState[22] = EgoVehicleState.C.IMULateral         ;
    oEgoVehState[23] = EgoVehicleState.C.IMUAngleCorrection ;
    oEgoVehState[24] = EgoVehicleState.C.IMUHeight          ;
    oEgoVehState[25] = EgoVehicleState.C.PlanningRightTraffic ;
    oEgoVehState[26] = EgoVehicleState.C.PlanningRoadWidth    ;
    oEgoVehState[27] = EgoVehicleState.C.PlanningPathMode     ;
    oEgoVehState[28] = EgoVehicleState.C.PlanningEndPnt.x;
    oEgoVehState[29] = EgoVehicleState.C.PlanningEndPnt.y;
    oEgoVehState[30] = EgoVehicleState.C.ControlTimestamp     ;
    oEgoVehState[31] = EgoVehicleState.C.ControlLat_Timestamp ;
    oEgoVehState[32] = EgoVehicleState.C.ControlLat_Algorithm ;
    oEgoVehState[33] = EgoVehicleState.C.Lookahead            ;
    oEgoVehState[34] = EgoVehicleState.C.ControlLat_Kp        ;
    oEgoVehState[35] = EgoVehicleState.C.ControlLat_Ki        ;
    oEgoVehState[36] = EgoVehicleState.C.ControlLat_Kd        ;
    oEgoVehState[37] = EgoVehicleState.C.ControlLong_Timestamp ;
    oEgoVehState[38] = EgoVehicleState.C.ControlLong_Algorithm ;
    oEgoVehState[39] = EgoVehicleState.C.ControlLong_ExtSp     ;
    oEgoVehState[40] = EgoVehicleState.C.ControlLong_MaxSP     ;
    oEgoVehState[41] = EgoVehicleState.C.ControlLong_Kp        ;
    oEgoVehState[42] = EgoVehicleState.C.ControlLong_Ki        ;
    oEgoVehState[43] = EgoVehicleState.C.ControlLong_Kd        ;

    oEgoVehState[44] = EgoVehicleState.Cmd.Run_Stop;
    oEgoVehState[45] = EgoVehicleState.Cmd.Emergency_Stop;
    oEgoVehState[46] = EgoVehicleState.Cmd.SPSpeed;
    oEgoVehState[47] = EgoVehicleState.Cmd.PathMode;

    oEgoVehState[48] = EgoVehicleState.O.Sts_Run_Stop;
    oEgoVehState[49] = EgoVehicleState.O.Run_Stop_Past;
    oEgoVehState[50] = EgoVehicleState.O.StartPointX;
    oEgoVehState[51] = EgoVehicleState.O.StartPointY;
    oEgoVehState[52] = EgoVehicleState.O.EndPointX;
    oEgoVehState[53] = EgoVehicleState.O.EndPointY;
    oEgoVehState[54] = EgoVehicleState.O.CarSpeed;
    oEgoVehState[55] = EgoVehicleState.O.X;
    oEgoVehState[56] = EgoVehicleState.O.Y;
    oEgoVehState[57] = EgoVehicleState.O.HeadingAngleRad;
    oEgoVehState[58] = EgoVehicleState.O.HeadingAngleDeg;
    oEgoVehState[59] = EgoVehicleState.O.WheelBase;
    oEgoVehState[60] = EgoVehicleState.O.MaxSpeed;

}

void OutputCarPC(TRI_VehicleState_Input inCarPC, real_T *oCarPC){
	oCarPC[0] = inCarPC.X;  			
    oCarPC[1] = inCarPC.Y;   
	oCarPC[2] = inCarPC.CarSpeed;   
    oCarPC[3] = inCarPC.Ctrl;
    oCarPC[4] = inCarPC.HeadingAngle;	
    oCarPC[5] = inCarPC.Empty1;     
    oCarPC[6] = inCarPC.WheelAngle; 
}

void OutputPlanning(tri_Planning inPlanning, real_T *oPlanning){
	oPlanning[ 0] = inPlanning.C.PathMode    ; 
    oPlanning[ 1] = inPlanning.C.RoadWidth   ;  
    oPlanning[ 2] = inPlanning.C.RightTraffic;      
    oPlanning[ 3] = inPlanning.C.EndPnt.x    ; 
	oPlanning[ 4] = inPlanning.C.EndPnt.y    ;

    oPlanning[ 5] = inPlanning.O.RunningMode    ;
    oPlanning[ 6] = inPlanning.O.TotalPoints    ;
    oPlanning[ 7] = inPlanning.O.StartId        ;
    oPlanning[ 8] = inPlanning.O.StartPnt.x     ;
	oPlanning[ 9] = inPlanning.O.StartPnt.y     ;
	oPlanning[10] = inPlanning.O.StartSeg.p[0].x;
	oPlanning[11] = inPlanning.O.StartSeg.p[0].y;
	oPlanning[12] = inPlanning.O.StartSeg.p[1].x;
	oPlanning[13] = inPlanning.O.StartSeg.p[1].y;
    oPlanning[14] = inPlanning.O.EndId          ;
    oPlanning[15] = inPlanning.O.EndPnt.x       ;
	oPlanning[16] = inPlanning.O.EndPnt.y       ;
    oPlanning[17] = inPlanning.O.EndSeg.p[0].x  ;
	oPlanning[18] = inPlanning.O.EndSeg.p[0].y  ;
	oPlanning[19] = inPlanning.O.EndSeg.p[1].x  ;
	oPlanning[20] = inPlanning.O.EndSeg.p[1].y  ;
    oPlanning[21] = inPlanning.O.PathLength     ;
}

void OutputPlanningEmpty(real_T *oPlanning){
	oPlanning[ 0] = (int)   0; 
    oPlanning[ 1] = (double)0.0;  
    oPlanning[ 2] = (int)   0;      
    oPlanning[ 3] = (double)0.0; 
	oPlanning[ 4] = (double)0.0;
    oPlanning[ 5] = (int)   EMPTYBUFFER;
    oPlanning[ 6] = (int)   0;
    oPlanning[ 7] = (int)   0;
    oPlanning[ 8] = (double)0.0;
	oPlanning[ 9] = (double)0.0;
	oPlanning[10] = (double)0.0;
	oPlanning[11] = (double)0.0;
	oPlanning[12] = (double)0.0;
	oPlanning[13] = (double)0.0;
    oPlanning[14] = (int)   0;
    oPlanning[15] = (double)0.0;
	oPlanning[16] = (double)0.0;
    oPlanning[17] = (double)0.0;
	oPlanning[18] = (double)0.0;
	oPlanning[19] = (double)0.0;
	oPlanning[20] = (double)0.0;
    oPlanning[21] = (double)0.0;
}

void OutputGlobalMap(TRI_CSV_NodeInfo inCSV, real_T *oCSV){
	oCSV[0]  = (double)inCSV.Id;
    oCSV[1]  = (double)inCSV.X;
    oCSV[2]  = (double)inCSV.Y;
    oCSV[3]  = (double)inCSV.MaxSpeed;
    oCSV[4]  = (double)inCSV.Type;
    oCSV[5]  = (double)inCSV.Curvature;
    printf("Recepcion curva: %lf\n", oCSV[5]);
    oCSV[6]  = (double)inCSV.Angle_In;
    oCSV[7]  = (double)inCSV.Angle_Out;
    oCSV[8]  = (double)inCSV.Di[0];
    oCSV[9]  = (double)inCSV.Di[1];
    oCSV[10] = (double)inCSV.Di[2];
    oCSV[11] = (double)inCSV.Di[3];
    oCSV[12] = (double)inCSV.Di[4];
    oCSV[13] = (double)inCSV.Di[5];
    oCSV[14] = (double)inCSV.Do[0];
    oCSV[15] = (double)inCSV.Do[1];
    oCSV[16] = (double)inCSV.Do[2];
    oCSV[17] = (double)inCSV.Do[3];
    oCSV[18] = (double)inCSV.Do[4];
    oCSV[19] = (double)inCSV.Do[5];
}

void OutputGlobalMapEmpty(real_T *oCSV){
	oCSV[0]  = 0;
    oCSV[1]  = 0;
    oCSV[2]  = 0;
    oCSV[3]  = 0;
    oCSV[4]  = EMPTYBUFFER;
    oCSV[5]  = 0;
    oCSV[6]  = 0;
    oCSV[7]  = 0;
    oCSV[8]  = 0;
    oCSV[9]  = 0;
    oCSV[10] = 0;
    oCSV[11] = 0;
    oCSV[12] = 0;
    oCSV[13] = 0;
    oCSV[14] = 0;
    oCSV[15] = 0;
    oCSV[16] = 0;
    oCSV[17] = 0;
    oCSV[18] = 0;
    oCSV[19] = 0;
}