
#include "../inc/Lib_ControlV2.h"

void TRI_PID_Simple_Error(tri_PIDe *PID, double iOffset){
    double kp = PID->iKp;
    double ki = PID->iKi;
    double kd = PID->iKd;
    double Gp = PID->iE*kp;
    double Gi = 0;
    double Gd = (PID->iE - PID->oEn_1)*kd;

    // Calculo de la ganancia integral para que no sature [0,100]
    Gi = PID->iE*ki + PID->oGin_1;
    if ((Gp + Gi + Gd + fabs(iOffset)) > 100){
        Gi = 100 - Gd - Gp - fabs(iOffset);
    }else{
        if((Gp + Gi + Gd + fabs(iOffset)) < 0){
            Gi = 0 - Gd - Gp - fabs(iOffset);
        }
    }

    // Salidas del PID
    PID->oCV    = Gp + Gi + Gd + fabs(iOffset);
    PID->oEn_1  = PID->iE;
    PID->oGin_1 = Gi;
}

double TRI_MaxSpeed_BasedOnCurvature(int iMode, double iMaxSpeed, double iCurvature){
/*
        aw < 0.315 m/s2   - - >            aw < 4.0824 km/h2   - - >   Not uncomfortable         - - >   Modo 1
0.315 < aw < 0.630 m/s2   - - >   4.0824 < aw < 8.1648 km/h2   - - >   A little uncomfortable    - - >   Modo 2
0.500 < aw < 1.000 m/s2   - - >   8.1648 < aw < 12.960 km/h2   - - >   Fairy uncomfortable       - - >   Modo 3
0.800 < aw < 1.600 m/s2   - - >   12.960 < aw < 20.736 km/h2   - - >   Uncomfortable             - - >   Modo 4
1.250 < aw < 2.500 m/s2   - - >   20.736 < aw < 32.400 km/h2   - - >   Very uncomfortable        - - >   Modo 5
        aw > 2.500 m/s2   - - >            aw > 32.400 km/h2   - - >   Extremely uncomfortable   - - >   Modo 6
*/
    // Aw = V*V*K
    double awMax;

    switch(iMode){
        case 1:
            awMax = 4082.4;
            break;
        case 2:
            awMax = 8164.8;
            break;
        case 3:
            awMax = 12960;
            break;
        case 4:
            awMax = 20736;
            break;
        case 5:
            awMax = 32400;
            break;
        default:
            return iMaxSpeed;
    }

    if(fabs(iCurvature) < 0.001 || sqrt(awMax/fabs(iCurvature*1000))>iMaxSpeed){
        return iMaxSpeed;
    }
    return sqrt(awMax/fabs(iCurvature*1000));
}

double TRI_Longitudinal_Control_KinematicBase_T(double iVo, double it, double iAcc, double iMaxSpeed){
    double tVo = iVo;
    if( tVo > iMaxSpeed ){
        if(iVo - fabs(iAcc)*it/3600 < 0){
            return 0;
        }
        return iVo - fabs(iAcc)*it/3600;
    }else{
        if(iVo + fabs(iAcc)*it/3600 > iMaxSpeed){
            return iMaxSpeed;
        }
        return iVo + fabs(iAcc)*it/3600;
    }
    return iMaxSpeed;
}

double TRI_Longitudinal_Control_KinematicBase_D(double iVo, double iVf, double id, double iAcc, double iMaxSpeed){
    double tVf;
    if(iVf > iMaxSpeed){tVf = iMaxSpeed;}
    else               {tVf = iVf;      }
    if(tVf - iVo > 0.001 && sqrt(iVo*iVo + 2*fabs(iAcc)*fabs(id)) > tVf){
        return tVf;
    }

    if(iVo - tVf > 0.001 && sqrt(iVo*iVo - 2*fabs(iAcc)*fabs(id)) < tVf){
        return tVf;
    }

    return iVo;
}

double TRI_MaxSpeed_Buffer(double iX, double iY, double iVo, double iAcc, TRI_LocalPlanner_NodeInfo iBuffer[BUFFERSIZE], double *odmax){
    double tVfMax = iBuffer[0].MaxSpeed;
    double tdmax = 0;
    double tx     = iX;
    double ty     = iY;
    double td     = 0;
    int    i      = 1;
    
    if(iBuffer[0].Type == EMPTYBUFFER || iBuffer[1].Type == EMPTYBUFFER){
		*odmax = 0;
        return 0;
    }

	*odmax = 0;
    while(i < BUFFERSIZE){
        if(iBuffer[i].Type < 0){
            break;
        }
        td  = td + (sqrt(pow(iBuffer[i].X - tx,2) + pow(iBuffer[i].Y - ty,2)))/1000;
        if(iVo > iBuffer[i].MaxSpeed){
            if(iVo*iVo - 2*iAcc*td > 0){
                if(sqrt(iVo*iVo - 2*iAcc*td) > iBuffer[i].MaxSpeed){
					*odmax = td;
                    tdmax = td;
                    tVfMax = iBuffer[i].MaxSpeed;
                }
            }
        }
        tx  = iBuffer[i].X;
        ty  = iBuffer[i].Y;
        i   = i + 1;
    }

    return tVfMax;
}


// Function:    TRI_Speed_SP
// Description: It gives the value of the Speed SP based on the type of algorithm.
double TRI_Speed_SP(int algorithm, int confortlevel, double vehX, double vehY, double vehV, double maxDec_ms, TRI_LocalPlanner_NodeInfo buffer[BUFFERSIZE]){
    double out = 0;
    double d = 0;
    double x = vehX;
    double y = vehY;
    double a = maxDec_ms*0.3;
    double vo = vehV;
    int    i  = 0;
    int    j  = 0;
    switch(algorithm){
        case  0: // Maximum Speed
            out = buffer[0].MaxSpeed/3.6;
            break;
        case  1: // Maximum Speed with curvature
            out = TRI_MaxSpeed_BasedOnCurvature(confortlevel, buffer[0].MaxSpeed, buffer[0].Curvature);
            out = out / 3.6;
            break;
        case  2: // based on maximum speed
            out = buffer[0].MaxSpeed/3.6;
            i = 1;
            while(i < BUFFERSIZE && buffer[i].Type > 0){
                d = d + sqrt(pow(x - buffer[i].X,2) + pow(y - buffer[i].Y,2));
                if (vo*vo-2*a*d < 0 ){
                    i = BUFFERSIZE;
                    printf("final\n");
                }else{
                    vo = sqrt(vo*vo - 2*a*d);
                    if(vo > buffer[i].MaxSpeed/3.6 && buffer[i].MaxSpeed/3.6 < out){
                        out = buffer[i].MaxSpeed/3.6;
                    }
                }
                i = i + 1;
            }
            break;
        case  3: // based on maximum speed
            j = 0;
            while(j < BUFFERSIZE && buffer[j].Type > 0 ){
                buffer[j].MaxSpeed = TRI_MaxSpeed_BasedOnCurvature(confortlevel, buffer[j].MaxSpeed, buffer[j].Curvature);;
                buffer[j].MaxSpeed = buffer[j].MaxSpeed/3.6;
                j = j +1; 
            }
            out = buffer[0].MaxSpeed;
            i = 1;
            d = 0;
            while(i < BUFFERSIZE && buffer[i].Type > 0){
                d = d + sqrt(pow(x - buffer[i].X,2) + pow(y - buffer[i].Y,2));
                if (fabs(buffer[i].MaxSpeed) <  fabs(vehV) && d > 0){
                    if(fabs(pow(buffer[i].MaxSpeed,2) - pow(vehV,2))/(2*d)>a){
                        if(buffer[i].MaxSpeed<out){
                            out = buffer[i].MaxSpeed;
                        }
                    }
                }
                x = buffer[i].X;
                y = buffer[i].Y;
                i = i + 1;
            }
            break;
        default:
            out = buffer[0].MaxSpeed/3.6;
            break;
    }
    return out;
}


// Function:    TRI_Ramp
// Description: Standard ramp function.
void   TRI_Ramp(tri_Ramp *ramp){
    // input variables
    double sp     = ramp->sp;
    double cv     = ramp->cv;
    double cv_bef = ramp->cv_bef;
    double m      = fabs(ramp->m);
    double ts     = fabs(ramp->ts);
    
    // function variables
    double out = 0;
    
    // Function
    if ( sp < cv_bef ){
        // Decreasing ramp
        out = ramp->cv_bef - m*ts;
        if(out < sp){
            // limit the output
            out = sp;
        }
    }else{
        // Rising ramp
        out = ramp->cv_bef + m*ts;
        if(out > sp){
            // limit the output
            out = sp;
        }
    }
    
    // Output
    ramp->cv = out;
}