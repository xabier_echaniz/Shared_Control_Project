//============================================================================
// Name        : Math_Geometry.cpp
// Author      : RA-MM
// Version     : 0.1
// Description : Math library used for geometric calculation.
//============================================================================

#include "../inc/Time_Calc.hpp"


namespace tri  {
namespace ad   {
namespace time {

std::string timeString(){
    time_t       now      = std::time(0);
    tm          *dataTime = gmtime(&now);
    std::string  dataTimeString;
    std::ostringstream ss;
    ss.str(""); ss.clear();
    ss << (dataTime->tm_year + 1900);
    dataTimeString = ss.str()+ "_";
    if(dataTime->tm_mon + 1 < 10){
        dataTimeString = dataTimeString + "0";
    }
    ss.str(""); ss.clear();
    ss << (dataTime->tm_mon + 1);
    dataTimeString = dataTimeString + ss.str() + "_";
    ss.str(""); ss.clear();
    ss << (dataTime->tm_mday);
    dataTimeString = dataTimeString + ss.str() + "_";
    ss.str(""); ss.clear();
    ss << (dataTime->tm_hour);
    dataTimeString = dataTimeString + ss.str() + "_";
    ss.str(""); ss.clear();
    ss << (dataTime->tm_min );
    dataTimeString = dataTimeString + ss.str() + "_";
    ss.str(""); ss.clear();
    ss << (dataTime->tm_sec );
    dataTimeString = dataTimeString + ss.str() + "";

    return dataTimeString;
}

} /* end of time */
} /* end of ad       */
} /* end of tri      */
