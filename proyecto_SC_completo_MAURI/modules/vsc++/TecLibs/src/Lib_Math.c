/*********************************************************************************************
 *** Name of the library:        Tecnalia Math library                                     ***
 *** Description of the library:                                                           ***
 *** Autor notes:                TRI means Tecnalia Research and Innovation                ***
 *** Date:                       April 15th, 2016                                          ***
 *** Modifications date:         - - - - - - - - -                                         ***
 *** Version:                    V1.0                                                      ***
 *** Notes about modifications:                                                            ***
 *********************************************************************************************/

/*********************************************************************************************
 *** Libraries to be invoked                                                               ***
 *********************************************************************************************/
#include "../inc/Lib_Math.h"

/*********************************************************************************************
 *** Functions                                                                             ***
 *********************************************************************************************/

double TRI_Point2D_X(tri_point2D p){
    return p.x;
}

double TRI_Point2D_Y(tri_point2D p){
    return p.y;
}

void   TRI_Rotate_point_around_origin(tri_point2D *point, double angle){
    double x = point->x;
	double y = point->y;

	point->x = (x * (double)cos(angle)) - (y * (double)sin(angle));
	point->y = (x * (double)sin(angle)) + (y * (double)cos(angle));
}

void   TRI_Rotate_point_around_pivot(tri_point2D *point, tri_point2D *pivot, double angle){
    point->x = point->x - pivot->x;
	point->y = point->y - pivot->y;

	TRI_Rotate_point_around_origin(&(*point), angle);

	point->x = point->x + pivot->x;
	point->y = point->y + pivot->y;
}

double TRI_Dot_product(tri_segment2D *s1, tri_segment2D *s2){
    double V1x = s1->p[1].x - s1->p[0].x;
    double V1y = s1->p[1].y - s1->p[0].y;
    double V2x = s2->p[1].x - s2->p[0].x;
    double V2y = s2->p[1].y - s2->p[0].y;

    return V1x * V1y + V2x * V2y;
}

double TRI_Cross_product(tri_segment2D *s1, tri_segment2D *s2){
    double V1x = s1->p[1].x - s1->p[0].x;
    double V1y = s1->p[1].y - s1->p[0].y;
    double V2x = s2->p[1].x - s2->p[0].x;
    double V2y = s2->p[1].y - s2->p[0].y;

    return V1x * V2y - V1y * V2x;
}

double TRI_Distance_point_segment(tri_point2D point, tri_segment2D segment, tri_point2D *closestpoint){
    double V1x = point.x - segment.p[0].x;
	double V1y = point.y - segment.p[0].y;
	double V2x = segment.p[1].x - segment.p[0].x;
	double V2y = segment.p[1].y - segment.p[0].y;

	double dot = V1x * V2x + V1y * V2y;
	double len_sq = V2x * V2x + V2y * V2y;
	if (len_sq == 0.0) {
		closestpoint->x = TRI_Point2D_X(segment.p[0]);
        closestpoint->y = TRI_Point2D_Y(segment.p[0]);
	} else {
		double param = dot / len_sq;
		if(param < 0.0) {
			closestpoint->x = TRI_Point2D_X(segment.p[0]);
			closestpoint->y = TRI_Point2D_Y(segment.p[0]);
		} else if(param > 1.0) {
			closestpoint->x = TRI_Point2D_X(segment.p[1]);
			closestpoint->y = TRI_Point2D_Y(segment.p[1]);
		} else {
			closestpoint->x = TRI_Point2D_X(segment.p[0]) + param * V2x;
			closestpoint->y = TRI_Point2D_Y(segment.p[0]) + param * V2y;
		}
	}

	return sqrt((point.x - closestpoint->x)*(point.x - closestpoint->x) + (point.y - closestpoint->y)*(point.y - closestpoint->y));
}

int    TRI_Segments_Intersected(tri_segment2D s1, tri_segment2D s2, tri_point2D *p){
    double m1, m2, b1, b2, x1_1, x1_2, x2_1, x2_2, y1_1, y1_2, y2_1, y2_2, x, y;
    double s1_xmin, s1_xmax, s2_xmin, s2_xmax, s1_ymin, s1_ymax, s2_ymin, s2_ymax;
    
    // Definimos los limites
    if(s1.p[0].x < s1.p[1].x){ s1_xmin = s1.p[0].x; s1_xmax = s1.p[1].x;
    }else{                     s1_xmin = s1.p[1].x; s1_xmax = s1.p[0].x;}
    
    if(s1.p[0].y < s1.p[1].y){ s1_ymin = s1.p[0].y; s1_ymax = s1.p[1].y;
    }else{                     s1_ymin = s1.p[1].y; s1_ymax = s1.p[0].y;}
    
    if(s2.p[0].x < s2.p[1].x){ s2_xmin = s2.p[0].x; s2_xmax = s2.p[1].x;
    }else{                     s2_xmin = s2.p[1].x; s2_xmax = s2.p[0].x;}
    
    if(s2.p[0].y < s2.p[1].y){ s2_ymin = s2.p[0].y; s2_ymax = s2.p[1].y;
    }else{                     s2_ymin = s2.p[1].y; s2_ymax = s2.p[0].y;}
    
    // Se evaluan los casos particulares, linea horizontal en los segmentos.
    y1_1 = s1.p[0].y;
    x1_1 = s1.p[0].x;
    y1_2 = s1.p[1].y;
    x1_2 = s1.p[1].x;
            
    y2_1 = s2.p[0].y;
    x2_1 = s2.p[0].x;
    y2_2 = s2.p[1].y;
    x2_2 = s2.p[1].x;
    if(fabs(x1_1 - x1_2) < 0.0001){     // Segmento S1 es vertical
        if(fabs(x2_1 - x2_2) < 0.0001){ // Segmento S2 es vertical
            return 0; // Son lineas paralelas verticales
        }else{
            m2   = (y2_2 - y2_1)/(x2_2 - x2_1);
            b2   = y2_2 - m2*x2_2;
            
            x    = x1_1;
            y    = m2 * x + b2;
            
            if(y >= s1_ymin && y < s1_ymax){
                p->x = x;
                p->y = y;
                return 1;
            }else{
                p->x = 0;
                p->y = 0;
                return 0;
            }
        }
    }else{
        if(fabs(x2_1 - x2_2) < 0.0001){
            m1   = (y1_2 - y1_1)/(x1_2 - x1_1);
            b1   = y1_2 - m1*x1_2;
            
            x    = x2_1;
            y    = m1 * x + b1;
            
            if(y >= s2_ymin && y < s2_ymax){
                p->x = x;
                p->y = y;
                return 1;
            }else{
                p->x = 0;
                p->y = 0;
                return 0;
            }
        }else{ // Caso de las lineas que no son verticales
            m1   = (y1_2 - y1_1)/(x1_2 - x1_1);
            b1   = y1_2 - m1*x1_2;
    
            m2   = (y2_2 - y2_1)/(x2_2 - x2_1);
            b2   = y2_2 - m2*x2_2;
            
            if(fabs(m1 - m2) > 0.0001){
                x = (b2 - b1)/(m1 - m2);
                y = m1*p->x + b1;
                if(x >= s1_xmin && x < s1_xmax && x >= s2_xmin && x < s2_xmax && y >= s1_ymin && y < s1_ymax && y >= s2_ymin && y < s2_ymax){
                    p->x = x;
                    p->y = y;
                    return 1;
                }else{
                    p->x = 0;
                    p->y = 0;
                    return 0;
                }
            }else{
                p->x = 0;
                p->y = 0;
                return 0;
            }
        }
    }
    
    
    
}

int    TRI_Lines_Intersected(tri_segment2D s1, tri_segment2D s2, tri_point2D *p){
    double m1, m2, b1, b2;
    if(fabs(s1.p[0].x - s1.p[1].x) < 0.000001){
        p->x = s1.p[0].x ;
        if(fabs(s2.p[0].x - s2.p[1].x) < 0.000001){
            p->y = s1.p[1].y;
            return -1;
        }else{
            m2   = (s2.p[1].y - s2.p[0].y)/(s2.p[1].x - s2.p[0].x);
            b2   =  s2.p[1].y - m2*s2.p[1].x;
            p->y = m2 * p->x + b2;
        }
    }else{
        if(fabs(s2.p[0].x - s2.p[1].x) < 0.000001){
            p->x =  s2.p[0].x ;
            m1   = (s1.p[1].y - s1.p[0].y)/(s1.p[1].x - s1.p[0].x);
            b1   =  s1.p[1].y - m1*s1.p[1].x;
            p->y =  m1 * p->x + b1;
        }else{
            m1 = (s1.p[1].y - s1.p[0].y)/(s1.p[1].x - s1.p[0].x);
            b1 =  s1.p[1].y - m1*s1.p[1].x;
            m2 = (s2.p[1].y - s2.p[0].y)/(s2.p[1].x - s2.p[0].x);
            b2 =  s2.p[1].y - m2*s2.p[1].x;
            if(fabs(m1 - m2)<0.000001){
                p->x = s2.p[0].x;
                p->y = s2.p[0].y;
                return -1;
            }else{
                p->x = (b2 - b1)/(m1 - m2);
                p->y = m1 * p->x + b1;
            }
        }
    }
    return 0;



}

double TRI_Segments_distance(tri_segment2D s1, tri_segment2D s2){
    tri_point2D closest_point;
	double dist, temp_dist;

	// Find the minimum distance between the segments --
	if (TRI_Segments_Intersected(s1, s2, &closest_point) == 1) {
    // The line segments intersect --
		dist = 0.0;
	} else {
		// If there is no intersection, then the distance between
		// a line endpoint and a line segment is the minimum distance
		temp_dist = TRI_Distance_point_segment(s1.p[0], s2, &closest_point);
		dist      = temp_dist;

		temp_dist = TRI_Distance_point_segment(s1.p[1], s2, &closest_point);
		if (temp_dist < dist)
			dist  = temp_dist;

		temp_dist = TRI_Distance_point_segment(s2.p[0], s1, &closest_point);
		if (temp_dist < dist)
			dist  = temp_dist;

		temp_dist = TRI_Distance_point_segment(s2.p[1], s1, &closest_point);
		if (temp_dist < dist)
			dist  = temp_dist;
	}

	return dist;
}

int    TRI_Intersection_segment_circle(tri_segment2D seg, tri_circle circle, tri_point2D *inter){
    double x1, x2, y1, y2;
    double xc, yc, r, xmax, ymax;
	double m, b;
    double A, B, C;

    double Resol;

    xc = circle.center.x;
    yc = circle.center.y;
    r  = circle.radius ;

    if(pow(seg.p[0].x - xc,2) + pow(seg.p[0].y - yc,2) > pow(seg.p[1].x - xc,2) + pow(seg.p[1].y - yc,2)){
        xmax = seg.p[0].x;
        ymax = seg.p[0].y;
    }else{
        xmax = seg.p[1].x;
        ymax = seg.p[1].y;
    }

    // Caso trivial 1
    if(fabs(seg.p[0].x - seg.p[1].x) < 0.00001){
        x1 = seg.p[0].x;
        x2 = x1;
        if(pow(r,2) - pow(x1 - xc,2) < 0){
            printf("Math: Segmento no intersecta circulo.\n");
            return -1;
        }else{
            y1 = yc + sqrt(pow(r,2) - pow(x1 - xc,2));
            y2 = yc - sqrt(pow(r,2) - pow(x2 - xc,2));

            if(sqrt(pow(xmax - x1,2)+pow(ymax - y1,2))  < sqrt(pow(xmax - x2,2)+pow(ymax - y2,2))){
                inter->x = x1;
                inter->y = y1;
            }else{
                inter->x = x2;
                inter->y = y2;
            }
            return 0;
        }
    }

        // Caso trivial 2
    if(fabs(seg.p[0].y - seg.p[1].y) < 0.00001){
        y1 = seg.p[0].y;
        y2 = y1;
        if(pow(r,2) - pow(y1 - yc,2) < 0){
            printf("Math: Segmento no intersecta circulo.\n");
            return -1;
        }else{
            x1 = xc + sqrt(pow(r,2) - pow(y1 - yc,2));
            x2 = xc - sqrt(pow(r,2) - pow(y2 - yc,2));

            if(sqrt(pow(xmax - x1,2)+pow(ymax - y1,2))  < sqrt(pow(xmax - x2,2)+pow(ymax - y2,2))){
                inter->x = x1;
                inter->y = y1;
            }else{
                inter->x = x2;
                inter->y = y2;
            }
            return 0;
        }
    }

    m = (seg.p[1].y - seg.p[0].y) / (seg.p[1].x - seg.p[0].x);
    b =  seg.p[1].y - m*seg.p[1].x;
    A = m*m + 1;
    B = 2*m*(b - yc) - 2*xc;
    C = (b - yc)*(b - yc) + xc*xc - r*r;

    Resol = B*B - 4*A*C;

    if(Resol < 0 ){
        printf("Math: Segmento no intersecta circulo.\n");
        return -1;
    }

    x1 = (-1*B + sqrt(Resol))/(2*A);
    x2 = (-1*B - sqrt(Resol))/(2*A);
    y1 =  x1 * m + b;
    y2 =  x2 * m + b;

    if(sqrt(pow(xmax - x1,2)+pow(ymax - y1,2))  < sqrt(pow(xmax - x2,2)+pow(ymax - y2,2))){
        inter->x = x1;
        inter->y = y1;
    }else{
        inter->x = x2;
        inter->y = y2;
    }

    return 0;
}

int    TRI_Intersection_point_circle(tri_point2D pnt, tri_circle circle, tri_point2D *inter){
    double r, xo, yo;
    double x1, x2, y1, y2;
    tri_segment2D seg;

    xo = circle.center.x;
    yo = circle.center.y;
    r  = circle.radius;

    seg.p[0].x = pnt.x;
    seg.p[0].y = pnt.y;
    seg.p[1].x = xo;
    seg.p[1].y = yo;

    if(fabs(seg.p[0].x - seg.p[1].x) < 0.000001){
        x1 = seg.p[0].x;
        x2 = x1;
        y1 = yo + circle.radius;
        y2 = yo - circle.radius;

    }else{
        if(fabs(seg.p[0].y - seg.p[1].y) < 0.000001){
            y1 = seg.p[0].y;
            y2 = y1;
            x1 = xo + circle.radius;
            x2 = xo - circle.radius;
        }else{
            double m = (seg.p[1].y - seg.p[0].y)/(seg.p[1].x - seg.p[0].x);
            double b =  seg.p[1].y - m * seg.p[1].x;
            double A,B,C, Resol;

            A = m*m + 1;
            B = 2*m*(b - yo) - 2*xo;
            C = (b - yo)*(b - yo) + xo*xo - r*r;

            Resol = B*B - 4*A*C;

            if(Resol < 0 ){
                printf("Math: Segmento no intersecta circulo.");
                return -1;
            }

            x1 = (-1*B + sqrt(Resol))/(2*A);
            x2 = (-1*B - sqrt(Resol))/(2*A);
            y1 =  x1 * m + b;
            y2 =  x2 * m + b;
        }
    }
    if(sqrt(pow(x1-seg.p[0].x,2)+pow(y1-seg.p[0].y,2)) < sqrt(pow(x2-seg.p[0].x,2)+pow(y2-seg.p[0].y,2))){
        inter->x = x1;
        inter->y = y1;
    }else{
        inter->x = x2;
        inter->y = y2;
    }
    return 0;
}

void   TRI_Circle_Fit(tri_point2D point1_p, tri_point2D point2_p, tri_point2D point3_p, tri_circle *circle_p){
	tri_point2D p1_p, p2_p, p3_p;

	double aSlope, bSlope;
	double yDelta_a, xDelta_a, yDelta_b, xDelta_b;

	circle_p->radius = -1; // error checking
	if (TRI_Is_Perpendicular(point1_p, point2_p, point3_p)==0) { p1_p = point1_p; p2_p = point2_p; p3_p = point3_p; }//if
	else if (TRI_Is_Perpendicular(point1_p, point3_p, point2_p)==0) { p1_p = point1_p; p2_p = point3_p; p3_p = point2_p; }//else if
	else if (TRI_Is_Perpendicular(point2_p, point1_p, point3_p)==0) { p1_p = point2_p; p2_p = point1_p; p3_p = point3_p; }//else if
	else if (TRI_Is_Perpendicular(point2_p, point3_p, point1_p)==0) { p1_p = point2_p; p2_p = point3_p; p3_p = point1_p; }//else if
	else if (TRI_Is_Perpendicular(point3_p, point2_p, point1_p)==0) { p1_p = point3_p; p2_p = point2_p; p3_p = point1_p; }//else if
	else if (TRI_Is_Perpendicular(point3_p, point1_p, point2_p)==0) { p1_p = point3_p; p2_p = point1_p; p3_p = point2_p; }//else if
	else return; // The three points are perpendicular to axis

	// Find the circle
	yDelta_a = p2_p.y - p1_p.y;
	xDelta_a = p2_p.x - p1_p.x;
	yDelta_b = p3_p.y - p2_p.y;
	xDelta_b = p3_p.x - p2_p.x;

	if (fabs(xDelta_a) <= 0.000000001 && fabs(yDelta_b) <= 0.000000001) {
		circle_p->center.x = 0.5f*(p2_p.x + p3_p.x);
		circle_p->center.y = 0.5f*(p1_p.y + p2_p.y);
		circle_p->radius = (float) sqrt(pow(circle_p->center.x - p1_p.x,2)+pow(circle_p->center.y - p1_p.y,2));
		return;
	}
	if (fabs(xDelta_b) <= 0.000000001 && fabs(yDelta_a) <= 0.000000001) {
		circle_p->center.x = 0.5f*(p1_p.x + p2_p.x);
		circle_p->center.y = 0.5f*(p2_p.y + p3_p.y);
		circle_p->radius = (float) sqrt(pow(circle_p->center.x - p1_p.x,2)+pow(circle_p->center.y - p1_p.y,2));
		return;
	}

	// is_perpendicular() assures that xDelta(s) are not zero
	aSlope = yDelta_a/xDelta_a;
	bSlope = yDelta_b/xDelta_b;
	if (fabs(aSlope-bSlope) <= 0.000000001) return; //The three pts are colinear

	// Calculate center
	circle_p->center.x = (double)((aSlope*bSlope*(p1_p.y - p3_p.y) + bSlope*(p1_p.x + p2_p.x) -
		aSlope*(p2_p.x + p3_p.x)) / (2.0*(bSlope-aSlope)));
	circle_p->center.y = (double)(-1.0*(circle_p->center.x - (p1_p.x+p2_p.x)/2.0)/aSlope +
		(p1_p.y+p2_p.y)/2.0);
	circle_p->radius = (float) sqrt(pow(circle_p->center.x - p1_p.x,2)+pow(circle_p->center.y - p1_p.y,2));

	return;
}

int    TRI_OneShotRising(int current, int *before){
    int shot;
    if(current >= 1 && *before <= 0){
        shot = 1;
    }else{
        shot = 0;
    }
    *before = current;
    return shot;
}

int    TRI_OneShotFalling(int current, int *before){
    int shot;
    if(*before >= 1 && current <= 0){
        shot = 1;
    }else{
        shot = 0;
    }
    *before = current;
    return shot;
}

double TRI_LateralError(tri_point2D p, tri_point2D p_seg, tri_segment2D s){
    double x1, y1, x2, y2, sign;
    x1   = s.p[1].x - s.p[0].x;
    y1   = s.p[1].y - s.p[0].y;
    x2   = p.x      - s.p[0].x;
    y2   = p.y      - s.p[0].y;
    sign = x1*y2    - y1*x2;
    if(fabs(sign)<0.0001){sign = 1;}
    sign = sign/fabs(sign);
    return sign*sqrt(pow(p.x-p_seg.x,2)+pow(p.y-p_seg.y,2));
}

double TRI_AngularError(double heading, tri_segment2D s){
    double V1x, V1y, V2x, V2y, angle;
    double sign, angle1, angle2;
    V1x = cos(heading);
    V1y = sin(heading);
    V2x = s.p[1].x - s.p[0].x;
    V2y = s.p[1].y - s.p[0].y;
    sign = V1x*V2y - V1y*V2x;
    angle1 = atan2(V1y,V1x)*180/M_PI;
    angle2 = atan2(V2y,V2x)*180/M_PI;
    if(angle1<0){angle1 = 360 + angle1;}
    if(angle2<0){angle2 = 360 + angle2;}
    angle = fabs(angle1-angle2);
    if(angle>180){angle = 360 - angle;}
    if(sign > 0){
        return -1*angle;
    }
    return angle;
}

double TRI_AngularErrorBetAngle(double heading, double segmentangle){
    double V1x, V1y, V2x, V2y, angle;
    double sign, angle1, angle2;
    V1x = cos(heading);
    V1y = sin(heading);
    V2x = cos(segmentangle);
    V2y = sin(segmentangle);
    sign = V1x*V2y - V1y*V2x;
    angle1 = atan2(V1y,V1x)*180/M_PI;
    angle2 = atan2(V2y,V2x)*180/M_PI;
    if(angle1<0){angle1 = 360 + angle1;}
    if(angle2<0){angle2 = 360 + angle2;}
    angle = fabs(angle1-angle2);
    if(angle>180){angle = 360 - angle;}
    if(sign > 0){
        return -1*angle;
    }
    return angle;
}

int    TRI_Is_Perpendicular(tri_point2D point1_p, tri_point2D point2_p, tri_point2D point3_p){
	// Checks if the given points are perpendicular to x or y axis
	double yDelta_a = point2_p.y - point1_p.y;
	double xDelta_a = point2_p.x - point1_p.x;
	double yDelta_b = point3_p.y - point2_p.y;
	double xDelta_b = point3_p.x - point2_p.x;

	// Checking whether the lines of the two points are vertical
	if (fabs(xDelta_a) <= 0.000000001 && fabs(yDelta_b) <= 0.000000001) return 0; // The points are perpendicular and parallel to x-y axis
	else if (fabs(yDelta_a) <= 0.0000001)   return 1; // A line of two point are perpendicular to x-axis 1
	else if (fabs(yDelta_b) <= 0.000000001) return 1; // A line of two point are perpendicular to x-axis 2
	else if (fabs(xDelta_a)<= 0.000000001)  return 1; // A line of two point are perpendicular to y-axis 1
	else if (fabs(xDelta_b)<= 0.000000001)  return 1; // A line of two point are perpendicular to y-axis 2
	else return 0;
}

tri_point2D TRI_RotatePoint90DegreesOffset(tri_point2D iPoint, double iHeadingRad, double iOffset){
    tri_point2D oPoint;
    double      tV1x, tV1y, tRadians;

    if(iOffset > 0){ tRadians = (+1) * M_PI_2;}
    else{            tRadians = (-1) * M_PI_2;}
    tV1x = fabs(iOffset) * cos(iHeadingRad + tRadians);
    tV1y = fabs(iOffset) * sin(iHeadingRad + tRadians);

    oPoint.x = iPoint.x + tV1x;
    oPoint.y = iPoint.y + tV1y;

    return oPoint;
}

tri_point2D TRI_RotatePointSegment90DegreesOffset(tri_point2D iPoint, tri_segment2D iSegment, double iOffset){
    tri_point2D oPoint;
    double      tV1x, tV1y, tRadians;

    if(iOffset > 0){ tRadians = (+1) * M_PI_2;}
    else{            tRadians = (-1) * M_PI_2;}
    tV1x = fabs(iOffset) * cos(atan2(iSegment.p[1].y - iSegment.p[0].y, iSegment.p[1].x - iSegment.p[0].x) + tRadians);
    tV1y = fabs(iOffset) * sin(atan2(iSegment.p[1].y - iSegment.p[0].y, iSegment.p[1].x - iSegment.p[0].x) + tRadians);

    oPoint.x = iPoint.x + tV1x;
    oPoint.y = iPoint.y + tV1y;

    return oPoint;
}

tri_point2D TRI_Bezier5to(tri_point2D P0, tri_point2D P1, tri_point2D P2, tri_point2D P3, tri_point2D P4, tri_point2D P5, double t){
    tri_point2D oPoint;

    double fP0 =             pow(1-t,5);
    double fP1 =  5*pow(t,1)*pow(1-t,4);
    double fP2 = 10*pow(t,2)*pow(1-t,3);
    double fP3 = 10*pow(t,3)*pow(1-t,2);
    double fP4 =  5*pow(t,4)*pow(1-t,1);
    double fP5 =    pow(t,5);

    oPoint.x = P0.x*fP0 + P1.x*fP1 + P2.x*fP2 + P3.x*fP3 + P4.x*fP4 + P5.x*fP5;
    oPoint.y = P0.y*fP0 + P1.y*fP1 + P2.y*fP2 + P3.y*fP3 + P4.y*fP4 + P5.y*fP5;

    return oPoint;
}

tri_point2D TRI_SetPointBasedSegment(tri_segment2D iSeg, double iOffset){
    tri_point2D oPnt;
    double Vx = iSeg.p[1].x - iSeg.p[0].x;
    double Vy = iSeg.p[1].y - iSeg.p[0].y;
    double Mg = sqrt(Vx*Vx + Vy*Vy);
    Vx = Vx/Mg;
    Vy = Vy/Mg;
    oPnt.x = iSeg.p[0].x + iOffset*Vx;
    oPnt.y = iSeg.p[0].y + iOffset*Vy;
    return oPnt;
}

double TRI_NBezier_Points(tri_point2D P0, tri_point2D P1, tri_point2D P2, tri_point2D P3, tri_point2D P4, tri_point2D P5){
    double      distance = 0;
    double      t        = 0.25;
	tri_point2D tPnt025, tPnt050, tPnt075, tPnt100;
    tPnt025  = TRI_Bezier5to(P0, P1, P2, P3, P4, P5, t);

    distance = distance + sqrt(pow(P0.x      - tPnt025.x,2) + pow(P0.y      - tPnt025.y,2));
    tPnt050  = TRI_Bezier5to(P0, P1, P2, P3, P4, P5, t);
    distance = distance + sqrt(pow(tPnt025.x - tPnt050.x,2) + pow(tPnt025.y - tPnt050.y,2));
    t        = 0.75;
    tPnt075  = TRI_Bezier5to(P0, P1, P2, P3, P4, P5, t);
    distance = distance + sqrt(pow(tPnt050.x - tPnt075.x,2) + pow(tPnt050.y - tPnt075.y,2));
    t        = 1.00;
    tPnt100  = TRI_Bezier5to(P0, P1, P2, P3, P4, P5, t);
    distance = distance + sqrt(pow(tPnt075.x - tPnt100.x,2) + pow(tPnt075.y - tPnt100.y,2));
    if(distance < 3) {return 1; }
    return ((double)((int)distance/0.15));
}

double TRI_CurvatureSign(tri_point2D iPast, tri_point2D iCurrent, tri_point2D iFuture){
    tri_point2D V1, V2;
    V1.x = iCurrent.x - iPast.x;
    V1.y = iCurrent.y - iPast.y;
    V2.x = iFuture.x  - iCurrent.x;
    V2.y = iFuture.y  - iCurrent.y;
    if(V1.x*V2.y - V1.y*V2.x < 0){
        return 1;
    }
    return -1;
}

double TRI_CurvatureBezier5(tri_point2D iP0, tri_point2D iP1, tri_point2D iP2, tri_point2D iP3, tri_point2D iP4, tri_point2D iP5, double it){
    // B5(t) = K0 + K1 * t + K2 * t^2 + K3 * t^3 + K4 * t^4 + K5 * t^5
    // K0 =   P0
    // K1 =  -5P0 +  P1
    // K2 =  10P0 - 4P1 +  P2
    // K3 = -10P0 + 6P1 - 3P2 +  P3
    // K4 =   5P0 - 4P1 + 3P2 - 2P3 + P4
    // K5 =   -P0 +  P1 -  P2 +  P3 - P4 + P5

    double K0x, K1x, K2x, K3x, K4x, K5x;
    double K0y, K1y, K2y, K3y, K4y, K5y;
    double P0x, P1x, P2x, P3x, P4x, P5x;
    double P0y, P1y, P2y, P3y, P4y, P5y;
	
    double B5tx, dB5tx, d2B5tx;
    double B5ty, dB5ty, d2B5ty;
    double t = it;

    double num, den;

    P0x = iP0.x;
    P0y = iP0.y;
    P1x = iP1.x;
    P1y = iP1.y;
    P2x = iP2.x;
    P2y = iP2.y;
    P3x = iP3.x;
    P3y = iP3.y;
    P4x = iP4.x;
    P4y = iP4.y;
    P5x = iP5.x;
    P5y = iP5.y;

    // Calculo de K0
    K0x = P0x;
    K0y = P0y;

    // Calculo de K1
    K1x = -5*P0x + 5*P1x;
    K1y = -5*P0y + 5*P1y;

    // Calculo de K2
    K2x = 10*P0x - 20*P1x + 10*P2x;
    K2y = 10*P0y - 20*P1y + 10*P2y;

    // Calculo de K3
    K3x = -10*P0x + 30*P1x - 30*P2x + 10*P3x;
    K3y = -10*P0y + 30*P1y - 30*P2y + 10*P3y;

    // Calculo de K4
    K4x = 5*P0x - 20*P1x + 30*P2x - 20*P3x + 5*P4x;
    K4y = 5*P0y - 20*P1y + 30*P2y - 20*P3y + 5*P4y;

    // Calculo de K5
    K5x = -P0x + 5*P1x - 10*P2x + 10*P3x - 5*P4x + P5x;
    K5y = -P0y + 5*P1y - 10*P2y + 10*P3y - 5*P4y + P5y;

    B5tx   = K0x + K1x*t + K2x*t*t + K3x*t*t*t + K4x*t*t*t*t + K5x*t*t*t*t*t;
    B5ty   = K0y + K1y*t + K2y*t*t + K3y*t*t*t + K4y*t*t*t*t + K5y*t*t*t*t*t;
    dB5tx  = K1x + 2*K2x*t + 3*K3x*t*t + 4*K4x*t*t*t + 5*K5x*t*t*t*t;
    dB5ty  = K1y + 2*K2y*t + 3*K3y*t*t + 4*K4y*t*t*t + 5*K5y*t*t*t*t;
    d2B5tx = 2*K2x + 6*K3x*t + 12*K4x*t*t + 20*K5x*t*t*t;
    d2B5ty = 2*K2y + 6*K3y*t + 12*K4y*t*t + 20*K5y*t*t*t;

    num = dB5tx*d2B5ty - dB5ty*d2B5tx;
    den = sqrt((dB5tx*dB5tx + dB5ty*dB5ty)*(dB5tx*dB5tx + dB5ty*dB5ty)*(dB5tx*dB5tx + dB5ty*dB5ty));
    if(den == 0){
        den = 0.00001;
    }
    return num/den;
}