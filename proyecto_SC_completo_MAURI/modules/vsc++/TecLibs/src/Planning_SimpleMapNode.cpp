//============================================================================
// Name        : Planning_SimpleMapNode.cpp
// Author      : RL - MM
// Version     : 0.1
// Description :
//============================================================================


#include "../inc/Planning_SimpleMapNode.hpp"

using tri::ad::math::distance2Points;
using tri::ad::math::distPointSegment;

namespace tri      {
namespace ad       {
namespace planning {

SimpleMapNode::SimpleMapNode(){
    this->id          = 0;
    this->x           = 0;
    this->y           = 0;
    this->z           = 0;
    this->type        = 0;
    this->k           = 0;
    this->angleInRad  = 0;
    this->angleOutRad = 0;
    this->nLanesRight = 0;
    this->nLanesLeft  = 0;
    this->roadWidth   = 0;
    this->rotateRight = 0;
    this->maxSpeed    = 0;
}

SimpleMapNode::SimpleMapNode(unsigned int id, double x, double y, double z, unsigned int type, double k, double angleInRad, double angleOutRad, int nLanesRight, int nLaneLeft, double roadWidth, unsigned int rotateRight, double maxSpeed){
    this->id          = id;
    this->x           = x;
    this->y           = y;
    this->z           = z;
    this->type        = type;
    this->k           = k;
    this->angleInRad  = angleInRad;
    this->angleOutRad = angleOutRad;
    this->nLanesRight = nLanesRight;
    this->nLanesLeft  = nLaneLeft;
    this->roadWidth   = roadWidth;
    this->rotateRight = rotateRight;
    this->maxSpeed    = maxSpeed;
}

void SimpleMapNode::clean(){
    this->id          = 0;
    this->x           = 0;
    this->y           = 0;
    this->z           = 0;
    this->type        = 0;
    this->k           = 0;
    this->angleInRad  = 0;
    this->angleOutRad = 0;
    this->nLanesRight = 0;
    this->nLanesLeft  = 0;
    this->roadWidth   = 0;
    this->rotateRight = 0;
    this->maxSpeed    = 0;
}


} /* end of planning */
} /* end of ad       */
} /* end of tri      */
