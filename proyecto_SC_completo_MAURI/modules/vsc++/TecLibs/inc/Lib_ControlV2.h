#ifndef LIB_CONTROL_H_INCLUDED
#define LIB_CONTROL_H_INCLUDED

/* * * * * * * * * *  Libraries * * * * * * * * * */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "Lib_Math.h"
#include "Lib_CSVAndListManagement.h"
#include "Lib_Planning.h"


/* * * * * * * * * * Structures * * * * * * * * * */
typedef struct tri_PIDe{
    double iE;
    double iKp;
    double iKi;
    double iKd;
    double oGin_1;
    double oEn_1;
    double oCV;
}tri_PIDe;

typedef struct tri_Ramp{
    double sp;     // [unit]
    double cv;     // [unit]
    double cv_bef; // [unit]
    double m;      // [unit]/sec
    double ts;     // sec
}tri_Ramp;

/* * * * * * * * * * Functions * * * * * * * * * */
void   TRI_PID_Simple_Error(tri_PIDe *PID, double iOffset);
double TRI_MaxSpeed_BasedOnCurvature(int iMode, double iMaxSpeed, double iCurvature);
double TRI_Longitudinal_Control_KinematicBase_T(double iVo, double it, double iAcc, double iMaxSpeed);
double TRI_Longitudinal_Control_KinematicBase_D(double iVo, double iVf, double id, double iAcc, double iMaxSpeed);
double TRI_MaxSpeed_Buffer(double iX, double iY, double iVo, double iAcc, TRI_LocalPlanner_NodeInfo iBuffer[BUFFERSIZE], double *otmax);

// Function:    TRI_Speed_SP
// Description: It gives the value of the Speed SP based on the type of algorithm.
// Created:     April 18th, 2017. Ray A. Lattarulo A.
// Version:     V1.0
double TRI_Speed_SP(int algorithm, int confortlevel, double vehX, double vehY, double vehV, double maxDec_ms, TRI_LocalPlanner_NodeInfo buffer[BUFFERSIZE]);

// Function:    TRI_Ramp
// Description: Standard ramp function.
// Created:     April 18th, 2017. Ray A. Lattarulo A.
// Version:     V1.0
void   TRI_Ramp(tri_Ramp *ramp);


#endif // LIB_CONTROL_H_INCLUDED
