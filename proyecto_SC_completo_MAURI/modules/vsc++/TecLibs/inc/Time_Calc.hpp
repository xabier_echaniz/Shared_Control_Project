//============================================================================
// Name        : BufferClass.hpp
// Author      : RL - MM
// Version     : 0.1
// Description :
//============================================================================

#ifndef TIME_CALC_HPP_
#define TIME_CALC_HPP_

/* * * * * *  Standard Libraries * * * * * */
#include<stdlib.h>  // Libreria estandar de C/C++
#include<iostream>  // Libreria estandar de C/C++
#include<list>      // Libreria para el manejo de listas
#include<string>    // Manejo de strings en C/C++
#include <ctime>
#include <sstream>

/* * * * * * External Libraries * * * * * */

/* * * * * *   Custom Libraries  * * * * * */
#include "errors.hpp"

/* * * * * * Forward declaration * * * * * */

/* * * * * *     Definition      * * * * * */


namespace tri  {
namespace ad   {
namespace time {

/* timeString: Returns the time string in the format YYYY_MM_DD_HH_MM_SS */
std::string timeString();

} /* end of time */
} /* end of ad       */
} /* end of tri      */
#endif /* PLANNING_BUFFER_HPP_ */
