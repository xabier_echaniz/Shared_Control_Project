//============================================================================
// Name        : Planning_GlobalMap.hpp
// Author      : RL - MM
// Version     : 0.1
// Description :
//============================================================================

#ifndef PLANNING_GLOBALMAP_HPP_
#define PLANNING_GLOBALMAP_HPP_

/* * * * * *  Standard Libraries * * * * * */
#include<stdlib.h>  // Libreria estandar de C/C++
#include<iostream>  // Libreria estandar de C/C++
#include<list>      // Libreria para el manejo de listas
#include<string>    // Manejo de strings en C/C++
#include<vector>
#include <fstream>

/* * * * * * External Libraries * * * * * */
#include "Eigen/Dense"
#include "tinyxml2.h"

/* * * * * *   Custom Libraries  * * * * * */
#include "Errors.hpp"
#include "Planning_SimpleMap.hpp"
#include "Math_Circle.hpp"

/* * * * * * Forward declaration * * * * * */

/* * * * * *     Definition      * * * * * */

namespace tri      {
namespace ad       {
namespace planning {

class GlobalMap{
	private:
		SimpleMap globalMapNodes;

	public:
		/* clear: erase all the elements in the class */
		void clear();

		/* size: returns the size */
        unsigned int getSize();

		/* printFile: printing the map in a .csv file */
		void printFile(std::string directory);

		/* appendNode: Add a node at the end of the vector */
        void appendNode(SimpleMapNode node);

		/* getNodeInfo: get informatio of the node*/
		SimpleMapNode getNodeInfo(const int & idx);

		void reIndexing();

		void overwriteMapNodes(std::vector<SimpleMapNode> map);

		/* GlobalMap: Constructor de Mapa Global */
		GlobalMap();

		/* GlobalMap: Destructor de Mapa Global */
		~GlobalMap();

		SimpleMap getGlobalMapNodes();

        /* parseNode: parse the content of a xml map node */
        int parseNode(tinyxml2::XMLElement  *pMap, SimpleMapNode *node);

		/* readMap: read the xml, return a 0 if there a error and 1 if everything is ok*/
		int readMap(std::string route);

		/* mapCorrect: returns 1 if everything is ok or 0 in case of an error */
        int mapCorrect();

		/* completeMap: adds the roundabout points and the lane change points */
		void completeMap(double dist);
};

} /* end of planning */
} /* end of ad       */
} /* end of tri      */
#endif /* PLANNING_BUFFER_HPP_ */
