/* ====================================================================== 
 * Name:        Lib_VehicleState.hpp
 * Description: Library of vehicle state, incorporates the information of
 *              input/output/configuration of the vehicle
 * Note:        - No notes - 
 * Author:      rayalejandro.lattarulo@tecnali.com 23/05/2017
 * Company:     Tecnalia
 * ======================================================================*/
#ifndef LIB_VEHICLESTATE_HPP_INCLUDED
#define LIB_VEHICLESTATE_HPP_INCLUDED

/* ======================================================================
 * Libraries definition 
 * ======================================================================*/
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<list>
#define _USE_MATH_DEFINES
#include<math.h>
using namespace std;

/* ======================================================================
 * Structures definition 
 * ======================================================================*/
/* Name:        (struct) tri_Veh_Input
 * Description: the structures contains the input values for the vehicle
 * Note:        (1) X positive is the front of the vehicle, Y positive is
 *              the left side of the vehicle, Z positive is top.
 *              (2) The angles keep the positive and negative signs of the
 *              XYZ coordinate framework (1).
 * Author:      rayalejandro.lattarulo@tecnalia.com
 * Company:     Tecnalia */
typedef struct tri_Veh_InOutputs{
 // Data Type   Name                    Observations            Units 
    double      lat;                //  latitude value          [�]
    double      lon;                //  longitude value         [�]
    double      x;                  //  x-position              [m]
    double      y;                  //  y-position              [m]
    float       vx;                 //  speed x-axis            [m/s]
    float       vy;                 //  speed y-axis            [m/s]
    float       vz;                 //  speed z-axis            [m/s]
    float       ax;                 //  acc. x-axis             [m/s^2]
    float       ay;                 //  acc. y-axis             [m/s^2]
    float       az;                 //  acc. z-axis             [m/s^2]
    float       yaw;                //  yaw orientation         [rad]
    float       dyaw;               //  dyaw/dt                 [rad/s]
    float       d2yaw;              //  d2yaw/dt2               [rad/s^2]
    float       roll;               //  roll angle              [rad]
    float       droll;              //  droll/dt                [rad/s]
    float       d2roll;             //  d2roll/dt2              [rad/s^2]
    float       pitch;              //  pitch angle             [rad]
    float       dpitch;             //  dpitch/dt               [rad/s]
    float       d2pitch;            //  d2pitch/dt^2            [rad/s^2]
    float       steering;           //  str. wheel angle (r- l+)[rad]
    float       wheel_fr;           //  front right wheel angle [rad]
    float       wheel_fl;           //  front left wheel angle  [rad]
    float       throttle;           //  throttle position       [0 - 1]
    float       brake;              //  brake position          [0 - 1]
    int         GPS_N_satelites;    //  number of satellites    [~]
    int         GPS_pos_quality;    //  position quality        [~]
    int         GPS_vel_quality;    //  velocity quality        [~]
    int         GPS_ori_quality;    //  orientation quality     [~]
    // Note: the minus means right side turnings
}tri_Veh_InOutputs;

/* Name:        (struct) tri_Veh_Output
 * Description: the output structure of the vehicle
 * Note:        - no notes - 
 * Author:      rayalejandro.lattarulo@tecnalia.com
 * Company:     Tecnalia */
typedef struct tri_Veh_Configuration{
    // Vehicle geometry
 // Data Type   Name                    Observations            Units 
    float       mass;               //  Vehicle mass            [kg]
    float       mass_dis_fr;        //  Front right mass dist.  [kg]
    float       mass_dis_fl;        //  Front left mass  dist.  [kg]
    float       mass_dis_rr;        //  Rear right mass  dist.  [kg]
    float       mass_dis_rl;        //  Rear left mass   dist.  [kg]
    float       I_x;                //  Inercia in X-Axis       [kg.m^2]
    float       I_y;                //  Inercia in Y-Axis       [kg.m^2]
    float       I_z;                //  Inercia in Z-Axis       [kg.m^2]
    float       height;             //  Height of the veh.      [m]
    float       width;              //  Width of the veh.       [m]
    float       lenght;             //  Lenght of the vehicle   [m]
    float       wheelbase;          //  Wheelbase of veh.       [m]
    float       track;              //  Vehicle track           [m]
    
    // Wheels characteristics
 // Data Type   Name                    Observations            Units 
    float       wheel_mass_fr;      //  Wheel mass front right  [kg]
    float       wheel_mass_fl;      //  Wheel mass front left   [kg]
    float       wheel_mass_rr;      //  Wheel mass rear right   [kg]
    float       wheel_mass_rl;      //  Wheel mass rear left    [kg]
    float       wheel_radio_fr;     //  Wheel radio front right [m]
    float       wheel_radio_fl;     //  Wheel radio front left  [m]
    float       wheel_radio_rr;     //  Wheel radio rear right  [m]
    float       wheel_radio_rl;     //  Wheel radio rear left   [m]
    float       cornering_fr;       //  Cornering stiffness F.R [~]
    float       cornering_fl;       //  Cornering stiffness F.L [~]
    float       cornering_rr;       //  Cornering stiffness R.R [~]
    float       cornering_rl;       //  Cornering stiffness R.L [~]
    
    // Center of gravity parameters
 // Data Type   Name                    Observations            Units 
    float       COG_height;         //  COG height              [m]
    float       COG_d_raxis;        //  Distance COG rear axis  [m]
    float       COG_d_faxis;        //  Distance COG front axis [m]  
    
    // Data from GPS and IMU   
 // Data Type   Name                    Observations            Units 
    float       GPS_height;         //  GPS height in the veh.  [m]
    float       GPS_right_offset;   //  GPSoffset to right side [m]
    float       GPS_front_offset;   //  GPSoffset to front side [m]
    float       IMU_height;         //  IMU height in the veh.  [m]
    float       IMU_right_offset;   //  IMUoffset to right side [m]
    float       IMU_front_offset;   //  IMUoffset to front side [m]
    
    // Data for Controllers
 // Data Type   Name                    Observations            Units 
    unsigned int platform_timestamp;//  Platform Timestamp      [milisec]
    unsigned int control_timestamp; //  Control Timestamp       [milisec]
    unsigned int control_lat_alg;   //  Lateral control alg.    [~]
    unsigned int control_lon_alg;   //  Longitud. control alg.  [~]
}tri_Veh_Configuration;

/* ======================================================================
 * Classes definition
 * ======================================================================*/
class TRIClass_VehicleState{
  private:
 // Data Type               Name	Observations 
    tri_Veh_Configuration   c;   // structure for the configuration param.
    tri_Veh_InOutputs       i;   // struct. for the input values
    tri_Veh_InOutputs       o;   // struct. for the output values
    
  public:
 /* Name:           (function) TRIClass_VehicleState (Constructor)
  * Description:    constructor
  * Note:           - no notes -
  * Author:         rayalejandro.lattarulo@tecnalia.com 24/05/2017 */
    TRIClass_VehicleState(void);
    
 /* Name:           (function) write_input
  * Description:    writting the input parameters to the class
  * Note:           - no notes -
  * Author:         rayalejandro.lattarulo@tecnalia.com 24/05/2017 */
    void write_input(tri_Veh_InOutputs i);
  
 /* Name:           (function) read_output
  * Description:    Reading the output parameters
  * Note:           - no notes -
  * Author:         rayalejandro.lattarulo@tecnalia.com 24/05/2017 */
    tri_Veh_InOutputs read_input(tri_Veh_InOutputs o);
};

/* ======================================================================
 * Function definition
 * ======================================================================*/
/* Name:           (function) TRI_Initialize_VehicleInput
 * Description:    Initialize the input/output structure
 * Note:           - no notes -
 * Author:         rayalejandro.lattarulo@tecnalia.com 24/05/2017 */
tri_Veh_InOutputs TRI_Initialize_VehicleInOutput(void);

/* Name:           (function) TRI_Initialize_VehicleConfig
 * Description:    Initialize the configuration structure
 * Note:           - no notes -
 * Author:         rayalejandro.lattarulo@tecnalia.com 24/05/2017 */
tri_Veh_InOutputs TRI_Initialize_VehicleConfig(void);

 #endif
