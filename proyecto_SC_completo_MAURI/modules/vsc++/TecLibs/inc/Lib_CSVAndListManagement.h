/*********************************************************************************************
 *** Name of the library:        CSV Management                                            ***
 *** Description of the library: This library is implemented to do the management of CSV   ***
 ***                             text files. All the procedures releated to them should be ***
 ***                             with the current library.                                 ***
 *** Autor notes:                TRI means Tecnalia Research and Innovation                ***
 *** Date:                       April 13th, 2016                                          ***
 *** Modifications date:         - - - - - - - - -                                         ***
 *** Version:                    V1.0                                                      ***
 *** Notes about modifications:                                                            ***
 *********************************************************************************************/
#ifndef LIB_CSVANDLISTMANAGEMENT_H_INCLUDED
#define LIB_CSVANDLISTMANAGEMENT_H_INCLUDED

/*********************************************************************************************
 *** Libraries to be invoked                                                               ***
 *********************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "Lib_Math.h"

#define EMPTYBUFFER   -999
#define TURN           1  // Common turn
#define ROUNDABOUT     2  // Roundabout intersection
#define RBTENTRANCE    3  // Entrance point to the roundabout
#define RBTMIDDLE      4  // Inside the roundabout
#define RBTEXIT        5  // Exit point from the roundabout
#define MERGING        6  // Point of a merging
#define BEZTURN        50 // Bezier points generated in turn
#define BEZRBENTRANCE  51 // Bezier points generated in the entrance of a roundabout
#define BEZRBEXIT      52 // Bezier points generated in the exit of a roundabout
#define PATHEND		   99 // End of the given path



/*********************************************************************************************
 *** Structures to be defined                                                              ***
 *********************************************************************************************/

// Name:     TRI_Node
// Function: Basic information of each line of the CSV
typedef struct TRI_CSV_NodeInfo{
    int    Id;        // Numero del punto leido
    double X;         // Posicion en el eje X
    double Y;         // Posicion en el eje Y
    double MaxSpeed;  // Velocidad Maxima en el tramo
    int    Type;      // Tipo de punto
    double Curvature; // Curvatura, inverso del radio de la rotonda o la distancia donde se realizara el merging, viene expresado en metros.
    double Angle_In;  // Angulo de entrada en grados [0 , 90]
    double Angle_Out; // Angulo de salida [0 , 90]
    double Di[6];     // La magnitud es metros (es la misma magnitud que el path).
    double Do[6];     // La magnitud es metros (es la misma magnitud que el path).
}TRI_CSV_NodeInfo;

// Name:     TRI_CSV_Data
// Function: List of nodes with the information
typedef struct TRI_CSV_List{
    TRI_CSV_NodeInfo Node;
    struct TRI_CSV_List *Next;
}TRI_CSV_List;

/*********************************************************************************************
 *** Declaration of functions                                                              ***
 *********************************************************************************************/


TRI_CSV_List *TRI_CreateNode(TRI_CSV_NodeInfo NodeInfo);

void TRI_DeleteList(TRI_CSV_List **LinkedList);

TRI_CSV_List *TRI_AddNodeBottomList(TRI_CSV_List *LinkedList, TRI_CSV_NodeInfo NodeInfo);

TRI_CSV_List *TRI_AddNodeTopList(TRI_CSV_List *LinkedList, TRI_CSV_NodeInfo NodeInfo);

TRI_CSV_List *TRI_AddNodeOffsetList(TRI_CSV_List *LinkedList, TRI_CSV_NodeInfo NodeInfo, int Offset);

int TRI_MovePointerForward(TRI_CSV_List **LinkedList);

TRI_CSV_NodeInfo TRI_ExtractNodeInfo(TRI_CSV_NodeInfo Node);

int TRI_SizeList(TRI_CSV_List *LinkedList);

TRI_CSV_List *TRI_ListPointerPosition(TRI_CSV_List *LinkedList, int Position);

int TRI_Read_CSV(TRI_CSV_List **LinkedList, char *FileName);

size_t TRI_Getdelim(char **linep, size_t *n, int delim, FILE *fp);

size_t TRI_GetLine(char **lineptr, size_t *n, FILE *stream);

#endif
