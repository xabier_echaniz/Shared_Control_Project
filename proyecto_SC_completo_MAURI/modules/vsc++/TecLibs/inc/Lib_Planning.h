/*********************************************************************************************
 *** Name of the library:        CSV Management                                            ***
 *** Description of the library: This library is implemented to do the management of CSV   ***
 ***                             text files. All the procedures releated to them should be ***
 ***                             with the current library.                                 ***
 *** Autor notes:                TRI means Tecnalia Research and Innovation                ***
 *** Date:                       April 13th, 2016                                          ***
 *** Modifications date:         - - - - - - - - -                                         ***
 *** Version:                    V1.0                                                      ***
 *** Notes about modifications:                                                            ***
 *********************************************************************************************/
#ifndef LIB_PLANNING_H_INCLUDED
#define LIB_PLANNING_H_INCLUDED

/*********************************************************************************************
 *** Libraries to be invoked                                                               ***
 *********************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include "Lib_CSVAndListManagement.h"
#include "Lib_Math.h"
#include "Lib_VehicleState.h"

#define MPOINTSNOTLOOP 0
#define MPOINTSLOOP    1
#define MBEZIERNOTLOOP 10
#define MBEZIERLOOP    11

#define BUFFERSIZE     150
#define HORIZON_METERS 50.0

#define  POLYGONSIZE 16 // Se define un poligono como un conjunto de 14 puntos.

/*********************************************************************************************
 *** Structures to be defined                                                              ***
 *********************************************************************************************/

typedef struct tri_Planning_Config{
    int    PathMode; // 0:  Follow a point track                      QUITAR ESTO DE AQUI!!!!
                     // 1:  Follow a point track and to do loops
                     // 10: Dynamic Generation
                     // 11: Dynamic Generation  and doing loops
    double      RoadWidth;    // Para realizar el planning
    int         RightTraffic; // Right-Hand Traffic                   QUITAR ESTO DE AQUI
    tri_point2D EndPnt;                                               //QUITAR ESTO DE AQUI
}tri_Planning_Config;

typedef struct tri_Planning_Output{
    int           RunningMode;
    int           TotalPoints;
    int           StartId;
    tri_point2D   StartPnt;
    tri_segment2D StartSeg;
    int           EndId;
    tri_point2D   EndPnt;
    tri_segment2D EndSeg;
    double        PathLength; // in the units of x and y coordinates, most of the time meters.
}tri_Planning_Output;

typedef struct tri_Planning{
    tri_Planning_Config C;
    tri_Planning_Output O;
}tri_Planning;

typedef struct tri_Planning_Track{
    double        cHorizon;
    int           oSegId;
    tri_point2D   oClosestPnt;
    tri_segment2D oClosestSeg;
    double        oHeadingBefore; // El heading del primer punto del semento [rad]
    double        oHeadingAfter;  // El heading del segundo punto [rad]
    double        oMaxSpeed;
    double        oMaxCurvature;
    double        oNextCurvature;
}tri_Planning_Track;

typedef struct TRI_LocalPlanner_NodeInfo{
    int        Id;
    int        Type;
    double     X;
    double     Y;
    double     MaxSpeed;
    double     Curvature;
    tri_circle Rnd;
    double     HeadingRad;
    double     HeadingBefRad;
}TRI_LocalPlanner_NodeInfo;

typedef struct TRI_LocalPlanner_List{
    TRI_LocalPlanner_NodeInfo     Node;
    struct TRI_LocalPlanner_List *Next;
}TRI_LocalPlanner_List;


/*********************************************************************************************
 *** Declaration of functions: Global Planning                                             ***
 *********************************************************************************************/

int  TRI_Global_Planning(TRI_VehicleState *iVehState, tri_Planning *iPlanning, TRI_CSV_List *iPath, TRI_CSV_List **oGlobalPlanner);
int  TRI_Rotate_Global_Planner(tri_Planning iPlanning, TRI_CSV_List **iGlobalPlanner);
void TRI_Enumerate_Global_Planner(TRI_CSV_List **iGlobalPlanner);
void TRI_Log_GlobalPlanner(TRI_CSV_List *iGlobalPlanner);
void TRI_Path_LctPntPth(tri_point2D iVehposition, TRI_CSV_List *iPath, tri_Planning_Track *iTrack, int loop);
TRI_CSV_List *TRI_Path_LCTPntPthPointer(tri_point2D iVehposition, TRI_CSV_List *iPath, tri_Planning_Track *iTrack, int loop);
double TRI_Path_Lenght   (tri_Planning      iPlanning,    TRI_CSV_List *iPath);


/*********************************************************************************************
 *** Declaration of functions: Local  Planning                                             ***
 *********************************************************************************************/

int TRI_Local_Planning(TRI_VehicleState *iVehState, tri_Planning *iPlanning, TRI_CSV_List *oGlobalPlanner, TRI_LocalPlanner_List **oLocalPlanner);
void   TRI_Log_LocalPlanner(TRI_LocalPlanner_List *iLocalPlanner);
void   TRI_LocalPlanner_Heading(TRI_LocalPlanner_List **iLocalPlanner, int iLoop);
TRI_LocalPlanner_NodeInfo TRI_LocalPlanner_ExtractNodeInfo(TRI_LocalPlanner_NodeInfo Node);
TRI_LocalPlanner_NodeInfo TRI_LocalPlanner_ExtractNodeCSV(TRI_CSV_NodeInfo Node);
TRI_LocalPlanner_List *TRI_LocalPlanner_CreateNode(TRI_LocalPlanner_NodeInfo NodeInfo);
void TRI_LocalPlanner_DeleteList(TRI_LocalPlanner_List **LinkedList);
TRI_LocalPlanner_List *TRI_LocalPlanner_AddNodeBottomList(TRI_LocalPlanner_List *LinkedList, TRI_LocalPlanner_NodeInfo NodeInfo);
TRI_LocalPlanner_List *TRI_LocalPlanner_AddNodeTopList(TRI_LocalPlanner_List *LinkedList, TRI_LocalPlanner_NodeInfo NodeInfo);
TRI_LocalPlanner_List *TRI_LocalPlanner_AddNodeOffsetList(TRI_LocalPlanner_List *LinkedList, TRI_LocalPlanner_NodeInfo NodeInfo, int Offset);
int TRI_LocalPlanner_MovePointerForward(TRI_LocalPlanner_List **LinkedList);
int TRI_LocalPlanner_SizeList(TRI_LocalPlanner_List *LinkedList);
void TRI_LocalPlanner_LctPntPth(int iVehID, tri_point2D iVehposition, TRI_LocalPlanner_List *iPath, tri_Planning_Track *iTrack, int loop);
void TRI_LocalPlanner_LctPntPth_Buffer(int iVehID, tri_point2D iVehposition, TRI_LocalPlanner_List *iPath, tri_Planning_Track *iTrack, int loop, double iHorizon, TRI_LocalPlanner_NodeInfo iBuffer[BUFFERSIZE]);
int  TRI_LocalPlanner_ReturnID(tri_point2D XY, TRI_LocalPlanner_List *iPath);


void TRI_Buffer_LctPntPath(tri_point2D iVehposition, TRI_LocalPlanner_NodeInfo iBuffer[BUFFERSIZE], tri_Planning_Track *iTrack);

int TRI_PolygonFromBuffer(tri_point2D iVehposition, double iRoadWidth, TRI_LocalPlanner_NodeInfo iBuffer[BUFFERSIZE], double iDistance, double iVerticalOffset, double iHorizontalOffset, tri_point2D iPolygon[POLYGONSIZE]);
int tri_Point_In_Polygon(tri_point2D iPto, tri_point2D iPolygon[POLYGONSIZE]);

// Funciones Nuevas implementadas para manejo de intersecciones en ENABLES3
int TRI_GenerateNextInterFromBuffer(TRI_LocalPlanner_NodeInfo iBuffer[BUFFERSIZE], double maxHorizon, tri_point2D *interPto);

#endif // LIB_GLOBALPLANNER_H_INCLUDED
