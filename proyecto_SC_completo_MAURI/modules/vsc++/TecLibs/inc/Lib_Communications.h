/* ************************************************************************************************
   ** Libraries:                                                                                 **
   ************************************************************************************************ */
#ifndef LIB_COMMUNICATIONS_H_INCLUDED
#define LIB_COMMUNICATIONS_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include "Lib_Math.h"
   
   
/* ************************************************************************************************
   ** Definitions:                                                                               **
   ************************************************************************************************ */

   
   
   
/* ************************************************************************************************
   ** Structure definitions:                                                                     **
   ************************************************************************************************ */
typedef struct tri_CommDateTime{
	unsigned int year;
	unsigned int month;
	unsigned int day;
	unsigned int hour;
	unsigned int min;
	unsigned int sec;
}tri_CommDateTime;

typedef struct tri_CommStruct{
	unsigned int id;      // por lo menos se pueden identificar 32mil cosas.
	signed int   type;    // para definir los tipos, por ahora (1) representa vehiculos.
	signed long  x;       // posicion en "x" del objeto en cuestion que se comunica.
    signed long  y;       // posicion en "y" del objeto en cuestion que se comunica.
	signed int   heading; // angulo de orientacion del objeto. esta acotado entre [-pi, pi]
    unsigned int speed;
	tri_CommDateTime datetime; // Marca de tiempo de la ultima vez que se recibio data de este ID.
}tri_CommStruct;

typedef struct tri_CommList{   // Lista de elementos que se estan comunicando 
    tri_CommStruct       Node;
    struct tri_CommList *Next;
	struct tri_CommList *Prev;
}tri_CommList;



/* ************************************************************************************************
   ** Functions definition:                                                                      **
   ************************************************************************************************ */
void TRI_CommDateTime(tri_CommDateTime *datetime);
unsigned long TRI_CommSubTime(tri_CommDateTime datetime);
// TRI_CommInitNode: Se encarga se inicializar un node de data con toda la informacion a cero.
void TRI_CommInitNode(tri_CommStruct *node);

// TRI_CommPrintfNode:
void TRI_CommPrintfNode(tri_CommStruct node);

// TRI_CommCreateNode: Se crea un node informacion con la data introducida.
tri_CommList *TRI_CommCreateNode(tri_CommStruct node);

// TRI_CommAddNodeList: A�ade un nodo al final 
tri_CommList *TRI_CommAddNodeList(tri_CommList *commList, tri_CommStruct node);

// TRI_CommUpdateNodeIdList: Dado un nodo, busca otro que tenga el mismo identificador en la lista y actualiza su informaci�n.
//                           Si no se encuentra el ID, lo a�ade al final
tri_CommList *TRI_CommUpdateNodeIdList(tri_CommList *commList, tri_CommStruct node);

// TRI_CommDeleteNodeIdList:  
tri_CommList *TRI_CommDeleteNodeIdList(tri_CommList *commList, unsigned int id);

// TRI_CommDeleteNodeTimeList: deletes nodes with timestamp older than indicated time
tri_CommList *TRI_CommDeleteNodeTimeList(tri_CommList *commList, int timeToClean);

// TRI_CommDeleteList:  
void TRI_CommDeleteList(tri_CommList **commList);

// TRI_CommSizeOfList:
unsigned int TRI_CommSizeOfList(tri_CommList *commList);

#endif