/**
 * @ copyright INRIA (c) 2014
 * @ license This source-code is submitted to the CeCILL A license (http://www.cecill.info/)
 * @ Joshue Perez and Vicente Milanes
 * @ joshue.perez_rastelli@inria.fr, vicente.milanes@inria.fr
 * @ file FEMot.hpp
 * @ brief TemplateProject RTMaps Component source file
 * @ details This component implements a template project
 * @ version 1.0
 * @ date 02-07-2014
 * @ bug	No known bugs
 * @ warning No warnings
 */

//#include <exception>
#include <cstring>
//#include <stdio.h>
//#include <cstdlib>
#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <cmath>
#include <sstream>

/*Range*/
#define INF 0
#define SUP 1
/*Boolean*/
#define FALSE 0
#define TRUE 1
/*Label-Types*/
#define TRI 0
#define TRA 1
#define GSS 2
#define BLL 3
#define FPI 4
#define EQ_TRI 10
#define EQ_TRA 11
#define EQ_GSS 12
#define EQ_BLL 13
#define EQ_PI 14
/*Modifiers*/
#define NO		0
#define FEW		1
#define VERY	    2
#define EXTRA		3
#define LESSTHAN	4
#define GREATERTHAN	5
#define LESSOREQUALTHAN 6
#define GREATEROREQUALTHAN 7
/*Operators*/
#define AND 'A'
#define LIN 1 //"LIN" instead of "MIN" 
#define PRO 2
#define OR 'O'
#define LAX 1 //"LAX" instead of "MAX" 
#define SUM 2
/*Reserved Words*/
#define TRIDIST "TRIDIST"
#define TRADIST "TRADIST"
#define GSSDIST "GSSDIST"
#define BLLDIST "BLLDIST"
#define PIDIST "PIDIST"
/*Other values*/
#define MAX_SIZE 1024
#define MAX_NAME_SIZE 128
#define EMPTY "EMPTY"
#define FILENAME "WriteControler.fuzzy"
#define E 2.718281828459045235360287471352
#define INFINITO 18444581
struct Label {
private:
	int type;
	double values[4];
	char name[MAX_NAME_SIZE];
	double degree;
public:
	Label();
	Label(int Type, double * Values, char * Name);
	~Label();
	void Equal(Label * original);
	int GetType();
	void GetValues(double * labelValues);
	void GetName(char * ret);
	double GetDegree();
	double GetValue(int index);
	void SetValues(double * Values, int type);
	void SetName(char * Name);
	void SetDegree(double Degree);
	void SetValue(int index,double Value);
	void Show();
};

struct Var_Entry {
private:
	char name[MAX_NAME_SIZE];
	int Nlabels;
	double range[2];
	Label * Labels;
public:
	Var_Entry();
	Var_Entry(char * Name, int Nlabels, double range[2], Label * labels);
	~Var_Entry();
	void Equal(Var_Entry * original);
	void GetName(char * ret);
	int GetNlabels();
	void GetRange(double * Range);
	double GetRangeInf();
	double GetRangeSup();
	void GetLabels(Label * ret);
	void GetLabel(Label * ret,int index);
	void SetName(char * Name);
	void SetRange(double rge[2]);
	void SetRangeInf(double inf);
	void SetRangeSup(double sup);
	void SetLabels(Label * labels, int Nlabels);
	void SetLabel(int index, Label * label);
	void AddLabel(Label * label);
	void DeleteLabel(int index);
	void Show();
};

struct Singleton {
public:
	char name[MAX_NAME_SIZE];
	double value;
	Singleton(){strcpy(name,"");value = 0.0;};
        ~Singleton(){};
	Singleton(char * Name, double Value);
	void Equal(Singleton * original);
	void GetName(char * ret);
	double GetValue();
	void SetName(char * Name);
	void SetValue(double Value);
	void Show();
};

struct Var_Out {
private:
        char name[MAX_NAME_SIZE];
	int Nvalues;
	Singleton * values;
public:
	Var_Out();
	Var_Out(char * Name, int N_values, Singleton * Values);
        ~Var_Out();
	void Equal(Var_Out  * original);
        void GetName(char * ret);
	int GetNvalues();
	void GetSingletons(Singleton * ret);
	void GetSingleton(Singleton * ret,int index);
        void SetName(char * Name);
	void SetSingletons(Singleton * Values, int size);
	void SetSingleton(int index, Singleton * Value);
	void AddSingleton(Singleton * sing, int index = INFINITO);
        void AddSingleton(char * Name, double Value, int index = INFINITO);
	void DeleteSingleton(int index);
        void Set_SingletonName(int index,char * Name);
        void Set_SingletonValue(int index,double Value);
        void Get_SingletonName(char * ret, int index);
        double Get_SingletonValue(int index);
	void Show();
};

struct Condition{
public:
	int in;
	int label;
	int modifier;
        Condition(): in(-1), label(-1), modifier(-1){};
	Condition(int In, int Label, int Modifier = -1): in(In), label(Label), modifier(Modifier){};
        ~Condition(){};
	void Equal(Condition * to_cpy){in = (*to_cpy).in; label = (*to_cpy).label;modifier = (*to_cpy).modifier;};
};

struct Consequence{
public:
	int out;
	int singleton;
        Consequence(): out(-1),singleton(-1){};
	Consequence(int Out, int Singleton): out(Out), singleton(Singleton){};
        ~Consequence(){};
	void Equal(Consequence  * to_cpy){out = (*to_cpy).out;singleton = (*to_cpy).singleton;}
};

struct Rule{
private:
	Condition * cond;
	Consequence * conseq;
        char * op_cond;
	int Ncond;
	int Nconseq;
	double degree;
public:
	Rule();
	Rule(Condition * Cond,char * Op_cond, Consequence * Conseq, int N_cond, int N_conseq);
        ~Rule();
	void Equal(Rule *to_cpy);
        void GetCondition(Condition * ret);
        void GetOp_cond(char * ret);
	void GetConsequence(Consequence * ret);
	int GetNcond();
	int GetNconseq();
	double GetDegree();
        void GetConditionI(Condition * ret,int index);
	void GetConsequenceI(Consequence * ret,int index);
	char GetOp_condI(int index);
	void SetCondition(Condition * Cond, char * OpCond, int N_cond);
	void SetConsequence(Consequence * Conseq, int N_conseq);
	void SetDegree(double Degree);
	void AddCondition(Condition * condition, char op = 'N');
        void AddCondition(int In, int Label, char op = 'N',int Modifier = -1);
        void AddConsequence(Consequence  * consequence);
	void AddConsequence(int Out, int Singleton);
        void DeleteCondition(int index);
        void DeleteConsequence(int index);

	void Find_Entry_In_Rule(int * ret,int Entry_Index, int all_ap = FALSE);
	void Find_Label_In_Rule(int * ret,int Entry_Index,int Label_Index, int all_ap = FALSE);
	void Find_Out_In_Rule(int * ret,int Out_Index, int all_ap = FALSE);
	void Find_Singleton_In_Rule(int * ret,int Out_Index, int Singleton_Index, int all_ap = FALSE);
        void Show(const char * x = " ");
};

struct RuleBase{
private:
	char Name[MAX_NAME_SIZE];
	Rule * Rules;
	int Nrules; 
	int And;
	int Or;
public:
	RuleBase();
	RuleBase(char * name,int And, int Or);
        ~RuleBase();
	void GetName(char * ret);
	void GetRules(Rule * ret);
	void GetRule(Rule * ret,int index);
        int GetNrules();
	int GetAnd();
	int GetOr();
        void SetName(char * name);
        void SetAnd(int conj);
	void SetOr(int disj);
        void SetRules(Rule * rules, int N_rules);
	void SetRule(int index, Rule * rule);
        void AddRule(Rule * rule);
	void DeleteRule(int index);
        void Show(); 
};

class AOrbexDos{
private:
	int Nentries;
	int Nout;
	Var_Entry * entries;
	Var_Out * outs;
	int Nrb;
	RuleBase * RB;
	double * In_Values;
	double * Out_Values;

public:
	/*CONSTRUCTOR*/
	AOrbexDos();
        ~AOrbexDos();
	AOrbexDos(const char * filename);



	/*GETS ANF SETS*/
	int GetNentries();
	int GetNouts();
	int GetNrb();
	double GetInValue(int In_Index);
	double GetInValue(char * Name_Entry);
        void GetInValues(double * ret);
        double GetOutValue(int Out_Index);
	double GetOutValue(char * Out_Name);
	void GetOutValues(double * ret);
	void SetInValue(int In_Index, double in_value);
	void SetInValue(char * Name_Entry, double in_value);
	void SetInValues(double * in_values);


	/*METHODS ABOUT ENTRIES AND LABELS*/
	void Add_Entry(Var_Entry * entry);
	void Add_Entry(char* Entry_Name, double r0, double r1);
	void Delete_Entry(int Ind_Entry);
	void Delete_Entry(char * Name_Entry);
	void Set_Entry(int Ind_Entry, Var_Entry * entry);
	void Set_Entry(int Ind_Entry, char* Entry_Name, double r0, double r1, int n_labels, Label * labels);
	void Get_Entry(Var_Entry * ret,int Ind_Entry);
	void Get_Entry(Var_Entry * ret,char * Name_Entry);
	void Set_Entry_Name(int Ind_Entry, char * New_Name);
	void Set_Entry_Name(char * Old_Name, char * New_Name);
	void Get_Entry_Name(char * ret,int Ind_Entry);
	void Set_Entry_RgeInf(int Ind_Entry, double r0);
	void Set_Entry_RgeInf(char * Name, double r0);
	void Set_Entry_RgeSup(int Ind_Entry, double r1);
	void Set_Entry_RgeSup(char * Name, double r1);
	double Get_Entry_RgeInf(int Ind_Entry);
	double Get_Entry_RgeInf(char * Name_Entry);
	double Get_Entry_RgeSup(int Ind_Entry);
	double Get_Entry_RgeSup(char * Name_Entry);
	int Get_Entry_Nlabels(int Ind_Entry);
	int Get_Entry_Nlabels(char * Name_Entry);
        void Add_Label(int Ind_Entry,char * name , int type,double v0,double v1, double v2 = INFINITO, double v3 = INFINITO);
	void Add_Label(char * Name_Entry,char * name , int type,double v0,double v1, double v2 = INFINITO, double v3 = INFINITO);
        void Add_Label(int Ind_Entry, Label * label_new);
	void Add_Label(char * Name_Entry, Label * label_new);
	void Delete_Label(int Ind_Entry, int Ind_Label);
	void Delete_Label(int Ind_Entry, char * Name_Label);
	void Delete_Label(char * Name_Entry, int Ind_Label);
	void Delete_Label(char * Name_Entry, char * Name_Label);
        void Make_Triangular_Labels(int Ind_Entry, int N_labels, const  char * name = EMPTY);
	void Make_Triangular_Labels(char * Name_Entry, int N_labels, const  char * name = EMPTY);
	void Make_Trapezium_Labels(int Ind_Entry, double proportion, int N_labels, const  char * name = EMPTY);
	void Make_Trapezium_Labels(char * Name_Entry, double proportion, int N_labels, const  char * name = EMPTY);
	void Make_Gauss_Labels(int Ind_Entry, double sigma, int N_labels, const  char * name = EMPTY);
	void Make_Gauss_Labels(char * Name_Entry, double sigma, int N_labels, const  char * name = EMPTY);
	void Make_Bell_Labels(int Ind_Entry, double A, double B, int N_labels, const  char * name = EMPTY);
	void Make_Bell_Labels(char * Name_Entry, double A, double B, int N_labels, const  char * name = EMPTY);
	void Make_PI_Labels(int Ind_Entry, double proporcion, int N_labels, const char * name = EMPTY);
	void Make_PI_Labels(char * Name_Entry, double proporcion, int N_labels, const char * name = EMPTY);
        void Get_Label(Label * ret,int Ind_Entry, int Ind_Label);
	void Get_Label(Label * ret,char * Name_Entry, char * Name_Label);
	void Get_Label(Label * ret,char * Name_Entry, int Ind_Label);
	void Get_Label(Label * ret,int Ind_Entry, char * Name_Label);
        void Set_Label(int Ind_Entry, int Ind_Label, Label * label_new);
	void Set_Label(char * Name_Entry, char * Name_Label, Label * label_new);
	void Set_Label(char * Name_Entry, int Ind_Label, Label * label_new);
	void Set_Label(int Ind_Entry, char * Name_Label, Label * label_new);
        void Get_Label_Name(char * ret,int Ind_Entry, int Ind_Label);
	void Get_Label_Name(char * ret,char * Name_Entry, int Ind_Label);
        void Set_Label_Name(int Ind_Entry, int Ind_Label, char * New_Name);
	void Set_Label_Name(char * Name_Entry, int Ind_Label, char * New_Name);
	void Set_Label_Name(int Ind_Entry, char * Name_Label, char * New_Name);
	void Set_Label_Name(char * Name_Entry, char * Name_Label, char * New_Name);
        int Get_Label_Type(int Ind_Entry, int Ind_Label);
	int Get_Label_Type(char * Name_Entry, char * Name_Label);
	int Get_Label_Type(char * Name_Entry, int Ind_Label);
	int Get_Label_Type(int Ind_Entry, char * Name_Label);
        void Get_Label_Values(double * ret,int Ind_Entry, int Ind_Label);
	void Get_Label_Values(double * ret,char * Name_Entry, char * Name_Label);
	void Get_Label_Values(double * ret,char * Name_Entry, int Ind_Label);
	void Get_Label_Values(double * ret,int Ind_Entry, char * Name_Label);
        void Set_Label_Values(int Ind_Entry, int Ind_Label, int type, double * values);
	void Set_Label_Values(char * Name_Entry, char * Name_Label, int type, double * values);
	void Set_Label_Values(char * Name_Entry, int Ind_Label, int type, double * values);
	void Set_Label_Values(int Ind_Entry, char * Name_Label, int type, double * values);
        double Get_Label_Degree(int Ind_Entry, int Ind_Label);
	double Get_Label_Degree(char * Name_Entry, char * Name_Label);
	double Get_Label_Degree(char * Name_Entry, int Ind_Label);
	double Get_Label_Degree(int Ind_Entry, char * Name_Label);
private:
	void Set_Label_Degree(int Ind_Entry, int Ind_Label, double Degree);

	/*METHODS ABOUT OUTS AND SINGLETONS*/
public: void Add_Out(Var_Out * outn);
	void Add_Out(char* Out_Name);
	void Delete_Out(int Ind_Out);
	void Delete_Out(char * Name_Out);
	void Set_Out(int Ind_Out, Var_Out * outn);
	void Set_Out(int Ind_Out, char* Out_Name, int n_values, Singleton * values);
	void Get_Out(Var_Out * ret,int Out_Index);
	void Get_Out(Var_Out * ret,char * Out_Name);
        void Set_Out_Name(int Ind_Out, char * New_Name);
	void Set_Out_Name(char * Name_Out, char * New_Name);
	void Get_Out_Name(char * ret,int Out_Index);
        int Get_Out_Nvalues(int Out_Index);
	int Get_Out_Nvalues(char * Out_Name);
        void Add_Singleton(int Ind_Out,char * name, double value, int pos = INFINITO);
	void Add_Singleton(char * Name_Out,char * name, double value, int pos = INFINITO);
        void Delete_Singleton(int Ind_Out, int Ind_Value);
	void Delete_Singleton(char * Name_Out, char * Name_Value);
	void Delete_Singleton(int Ind_Out, char * Name_Value);
	void Delete_Singleton(char * Name_Out, int Ind_Value);
        void Make_Singletons(int Ind_Out, int N_Singletons, double inf_limit, double sup_limit, const  char * name = EMPTY);
	void Make_Singletons(char * Name_Out, int N_Singletons, double inf_limit, double sup_limit, const  char * name = EMPTY);
        void Get_Singleton(Singleton * ret,int Out_Index, int Ind_Value);
	void Get_Singleton(Singleton * ret,char * Out_Name, char * Name_Value);
	void Get_Singleton(Singleton * ret,char * Out_Name, int Ind_Value);
	void Get_Singleton(Singleton * ret,int Out_Index, char * Name_Value);
        void Set_Singleton(int Out_Index,int Ind_Value, Singleton * sing);
	void Set_Singleton(char * Out_Name, char * Name_Value, Singleton * sing);
	void Set_Singleton(char * Out_Name, int Ind_Value,Singleton * sing);
	void Set_Singleton(int Out_Index, char * Name_Value,Singleton * sing);
        void Set_Singleton_Value(int Ind_Out, int Ind_Value, double value);
	void Set_Singleton_Value(char * Name_Out, char * Name_Value, double value);
	void Set_Singleton_Value(int Ind_Out, char * Name_Value, double value);
	void Set_Singleton_Value(char * Name_Out, int Ind_Value, double value);
	double Get_Singleton_Value(int Out_Index, int Ind_Value);
	double Get_Singleton_Value(char * Out_Name, char * Name_Value);
	double Get_Singleton_Value(char * Out_Name, int Ind_Value);
	double Get_Singleton_Value(int Out_Index, char * Name_Value);
        void Set_Singleton_Name(int Ind_Out, int Ind_Value, char * name);
	void Set_Singleton_Name(char * Name_Out, char * Name_Value, char * name);
	void Set_Singleton_Name(int Ind_Out, char * Name_Value, char * name);
	void Set_Singleton_Name(char * Name_Out, int Ind_Value, char * name);
	void Get_Singleton_Name(char * ret,int Out_Index, int Ind_Value);
	void Get_Singleton_Name(char * ret,char * Out_Name, int Ind_Value);
        void Replace_All_Singletons(int Out_Index, double * new_values);
	void Replace_All_Singletons(char * Out_Name, double * new_values);

	/*METHODS ABOUT RULES AND RULBASES*/
public:
	void Add_RB(char * RB_Name, char * RB_And, char * RB_Or);
	void Get_RB_Name(char * ret,int RB_Index);
        int Get_RB_Nrules(int RB_Index);
	int Get_RB_Nrules(char * RB_Name);
        int Get_RB_And(int RB_Index);
	int Get_RB_And(char * RB_Name);
	const char * Get_RB_AndS(int rb_index = 0);
	const char * Get_RB_AndS(char * rb_name);
        void Set_RB_And(int RB_Index, char *  conj);
	void Set_RB_And(char * RB_Name, char *  conj);
        int Get_RB_Or(int RB_Index);
	int Get_RB_Or(char * RB_Name);
    const char * Get_RB_OrS(int rb_index= 0);
	const char * Get_RB_OrS(char * rb_name);
        void Set_RB_Or(int RB_Index, char * disj);
	void Set_RB_Or(char * RB_Namex, char * disj);
        void Add_RB_Rule(int RB_Index, char * new_rule, const  char * msg = "");
        void Add_RB_Rule(char * RB_Name, char * new_rule);
        void Delete_RB_Rule(int RB_Index, int Rule_Index);
	void Delete_RB_Rule(char * RB_Name, int Rule_Index);
        void Set_RB_Rule(int RB_Index, int Rule_Index, Rule * rule);
        void Set_RB_Rule(char * RB_Name, int Rule_Index, Rule * rule);
        double Get_RB_RuleDegree(int RB_Index, int Rule_Index);
	double Get_RB_RuleDegree(char * RB_Name, int Rule_Index);
        void ShowRule(int RB_Index, int Rule_Index);
private:
        void Set_RB_RuleDegree(int RB_Index, int Rule_Index, double Degree);
        void Set_RB_RuleDegree(char * RB_Name, int Rule_Index, double Degree);
        void Add_RB_Rule(int RB_Index, Rule * rule);
        void Add_RB_Rule(char * RB_Name, Rule * rule);
        void StringToRule(Rule * rule,char * srule, const char * msg = "");
        void CheckCondition(Condition * Cond, int size);
        void CheckConsequence(Consequence * Conseq, int size);
        void CheckOperator(char * op, int size);


	/*IMPORTANT METHODS*/
public:
	void Show();
	void WriteOnFile(const char file_out[MAX_SIZE] = FILENAME);
private:
	void Fuzzification();
	double Modifier_Evaluation(int Modifier, int Ind_Entry, int Ind_Label, double In_Value);
	void Reasoning();
	void Defuzzification();
public:
	void Reset();
	void Evaluate(double * In_Value);/*MAIN FUNCTION*/
	void Evaluate();/*MAIN FUNCTION*/
	void ShowOutValues(const char * string_print = EMPTY, int onfile = FALSE);

	/*AUXILIAR METHODS OF CLASS */
private:
	int Find_Entry_Name(const char * Name, const  char * msg = "");
	int Find_Label_Name(int Ind_Entry,const char * Name, const  char * msg = "");
	int Find_Out_Name(char * Name, const  char * msg = "");
	int Find_Singleton_Name(int Ind_Out,char * Name, const  char * msg = "");
	int Find_RB_Name(char * Name);
        void Delete_Rule_Asociatedwith_Label(int Entry_Index, int Label_Index);
	void Delete_Rule_Asociatedwith_Entry(int Entry_Index);
	void Delete_Rule_Asociatedwith_Singleton(int Out_Index, int Singleton_Index);
	void Delete_Rule_Asociatedwith_Out(int Out_Index);
	double T_Norma( double x, double y , int rb_index = 0);
	double S_Norma(double x, double y, int rb_index = 0);

public:/*MATLAB*/
	void CreateMesh(int Number_Of_Values, int in0, int in1, int out, const  char * filename = "CreateMesh.m");
	void CreateMesh(int Number_Of_Values, char* in0, char* in1, char* out, const  char * filename = "CreateMesh.m");
	void Draw_Entry_Labels(int Entry_Index);
	void Draw_Entry_Labels(const char* Entry_Name);
	void CreateTable(int one_size = 100, const  char * filename = "ControlerTable.txt" );
	void CreateTable(int *size, const  char * filename = "ControlerTable.txt");
	void RW(int *size, int n_entry);

};


/*AUXILIAR METHODS*/
void Copy_Double_Array(double * a_copy,double * array, int size);
void Copy_Array_Label(Label * a_copy,Label * array, int size);
void Copy_Array_Entry(Var_Entry * array_copy,Var_Entry * array, int size);
void Copy_Array_Singleton(Singleton * array_copy,Singleton * array, int size);
void Copy_Array_Out(Var_Out * array_copy, Var_Out * array, int size);
void Copy_Array_Cond(Condition * array_copy,Condition * array,int size);
void Copy_Array_Conseq(Consequence * array_copy,Consequence * array,int size);
void Copy_Array_Op(char * array_copy,char * array,int size);
void Copy_Array_Rule(Rule * array_copy,Rule * array,int size);
void Calculate_Triangles_Center(double * centers,int N_labels,double Lim_Inf,double Lim_Sup);
const char * GetOperatorString(int name);
const char * GetModifier(int Mod);
int IsModifier(const char * Mod);
char IsOperator(const char * Op);
int IsOr(const char * disj);
int IsAnd(const char * conj);
double atod(char * string, const char * msg = "");

/*MEMBERSHIP FUNCTIONS*/
double Triangle_Membership(double in_value, double * vertex); // v1-0,v2-1,v3-0
double Trapezium_Membership(double in_value, double * vertex); // v1-0,v2-1,v3-1,v4-0
double Gauss_Membership(double in_value, double * g_values); // b(median)-0, c(root)-1
double Bell_Membership(double in_value, double * b_values);// c(center)-0 a(width)-1 b(smooth)-2
double PI_Membership(double in_value, double * vertex);// a-0,b-1,c-2,d-3

class Error {
	public:
		Error() : motive(100) {};// Default Constructor
		Error(int num_error, const char  * appen_msg= ""): motive(num_error), msg(appen_msg) {}; //Constructor with int parameter. Indicates the Error number.
		const char * message_error() const throw(); // Method to get the error mesagge.
	private:
		int motive;
		const char * msg;
 };


