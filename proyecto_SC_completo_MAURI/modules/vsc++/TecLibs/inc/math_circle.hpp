//============================================================================
// Name        : Math_Geometry.hpp
// Author      : RL-MM
// Version     : 0.1
// Description : Math library used for geometric calculation.
//============================================================================

#ifndef MATH_CIRCLE_HPP_
#define MATH_CIRCLE_HPP_

/* * * * * *  Standard Libraries * * * * * */
#include<stdlib.h>  // Libreria estandar de C/C++
#include<iostream>  // Libreria estandar de C/C++
#include<string>    // Manejo de strings en C/C++
#include<cmath>     // Math library
#include<vector>


/* * * * * * External Libraries * * * * * */
#include "Eigen/Dense"

/* * * * * *   Custom Libraries  * * * * * */
#include "errors.hpp"
#include "Math_Geometry.hpp"


/* * * * * * Forward declaration * * * * * */
#define GRAVITY 9.81

/* * * * * *     Definition      * * * * * */
namespace tri  {
namespace ad   {
namespace math {

/* intersectionLineSphere: returns if the line intersects the circle (0 false or 1 true) and pass as pointer those
points */
int intersectionLineSphere(Eigen::Vector3d linePto, Eigen::Vector3d lineVector, Eigen::Vector3d circleOrigin, double circleRadius, Eigen::Vector3d & ptoInter1, Eigen::Vector3d & ptoInter2);

int intersectionSegmentSphere(Eigen::Vector3d sgmPto1, Eigen::Vector3d sgmPto2, Eigen::Vector3d circleOrigin, double circleRadius, Eigen::Vector3d & ptoInter1, Eigen::Vector3d & ptoInter2);

int closestPtoSegmentSphere(Eigen::Vector3d sgmPto1, Eigen::Vector3d sgmPto2, Eigen::Vector3d circleOrigin, double circleRadius, Eigen::Vector3d & ptoInter1);

void completeCircle(Eigen::Vector3d circleCenter, double circleRadius, double angle1Rad, double angle2Rad, double deltaAngleRad, int clockwise, std::vector<double> & x, std::vector<double> & y);

Eigen::Vector3d circleCenter(const Eigen::Vector3d  & pnt1, const Eigen::Vector3d  & pnt2, const Eigen::Vector3d  & pnt3);

Eigen::Vector3d generateCircle3Pnts(const Eigen::Vector3d  & pnt1, const Eigen::Vector3d  & pnt2, const Eigen::Vector3d  & pnt3, double * curvature);

} /* end of math */
} /* end of ad   */
} /* end of tri      */
#endif /* MATH_GEOMETRY_HPP_ */
