//============================================================================
// Name        : Math_Geometry.hpp
// Author      : RL-MM
// Version     : 0.1
// Description : Math library used for geometric calculation.
//============================================================================

#ifndef MATH_GEOMETRY_HPP_
#define MATH_GEOMETRY_HPP_

/* * * * * *  Standard Libraries * * * * * */
#include<stdlib.h>  // Libreria estandar de C/C++
#include<iostream>  // Libreria estandar de C/C++
#include<string>    // Manejo de strings en C/C++
#include<cmath>     // Math library



/* * * * * * External Libraries * * * * * */
#include "Eigen/Dense"

/* * * * * *   Custom Libraries  * * * * * */
#include "../inc/errors.hpp"


/* * * * * * Forward declaration * * * * * */
#define GRAVITY 9.81

/* * * * * *     Definition      * * * * * */
namespace tri  {
namespace ad   {
namespace math {

/* * * Conversion functions * * * */

/* rad2Deg: transform an double input in radians to degrees */
double rad2Deg(double rad);

/* deg2rad: transform an double input in degrees to radians */
double deg2Rad(double deg);

/* normalizeAngleRad: normalize the angle between -PI and PI */
double normalizeAngleRad(double rad);

/* normalizeAngleDeg: normalize the angle between -180 and 180*/
double normalizeAngleDeg(double deg);

/* absoluteAngleRad: normalize the angle between 0 and 2PI */
double absoluteAngleRad(double rad);

/* absoluteAngleDeg: normalize the angle between 0 and 360*/
double absoluteAngleDeg(double deg);

/* ms2Kmh: converts from meters per second to kilometers per hour*/
double ms2Kmh(double ms);

/* kmh2ms: converts from kilometers per hour to meters per second*/
double kmh2Ms(double kmh);

/* gs2Ms2: converts from G's to m/s^2 */
double gs2Ms2(double gs);

/* ms22Gs: converts from G's to m/s^2 */
double ms22Gs(double ms2);

/* * *   Point functions   * * * */

/* distance2Points: receives 2 points and calcs the distance between them */
double distance2Points(const Eigen::VectorXd & p0, const Eigen::VectorXd & p1);

/* * * Vectorial functions * * * */

/* normalizedVectorOf: normalizes a vector*/
Eigen::Vector3d normalizedVectorOf(const Eigen::Vector3d & inputVector);

/* angleBetweenVectorsRad: calculates the angle between two vectors in radians */
double angleBetweenVectorsRad(const Eigen::Vector3d & v1, const Eigen::Vector3d & v2);

/* angleVector: Returns the angle of a vector considering clockwise rotation around z*/
double angleVectorAroundZ(const Eigen::Vector3d & v1);

/* distPointLine: computes the shortest distance from a point to a line and returns the shortest point
 as a parameter. The inputs are a point in the line and the vector of the directions. In 2D is normally
 cos(a)i + sin(a)j + 0k. The distance will be negative if the point is at the right side of the vector*/
double distPointLine( const Eigen::Vector3d & linePoint, const Eigen::Vector3d & directVectorIJK, const Eigen::Vector3d & point, Eigen::Vector3d *shortestPoint );


/* distPointSegment: calculates the distance between a point and a segment, returns nan if the point p2 is out of the segment
 formed by p0 in the base and p1 at the end.  */
double distPointSegment( const Eigen::Vector3d & p0, const Eigen::Vector3d & p1, const Eigen::Vector3d & p2, Eigen::Vector3d *shortestPoint );

/* addElementVectorX: add an element to a vector of dimention n */
void   addElementVectorX(Eigen::VectorXd & v, double elem);

Eigen::Vector3d rotateVectorAroundZ(const Eigen::VectorXd & v, double rotationAngle);

/* */
double speedLimiter(double inSpeed,double curvature, double maxSpeed, double comfort);

/* intersectionBetweenSegmentsIn2D: this return a bool if the segments intersect one with each other and return as parameter the intersection point */
bool intersectionBetweenSegmentsIn2D(const Eigen::Vector3d & s0P0, const Eigen::Vector3d & s0P1, const Eigen::Vector3d & s1P0, const Eigen::Vector3d & s1P1, Eigen::Vector3d & interPnt);
} /* end of math */
} /* end of ad   */
} /* end of tri      */
#endif /* MATH_GEOMETRY_HPP_ */
