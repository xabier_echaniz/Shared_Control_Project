/*********************************************************************************************
 *** Name of the library:        Vehicle State                                             ***
 *** Description of the library: This library is implemented to do all the implementations ***
                                 related with the vehicle state                            ***
 *** Autor notes:                TRI means Tecnalia Research and Innovation                ***
 *** Date:                       April 15th, 2016                                          ***
 *** Modifications date:         - - - - - - - - -                                         ***
 *** Version:                    V1.0                                                      ***
 *** Notes about modifications:                                                            ***
 *********************************************************************************************/
#ifndef LIB_VEHICLESTATE_H_INCLUDED
#define LIB_VEHICLESTATE_H_INCLUDED

/*********************************************************************************************
 *** Libraries to be invoked                                                               ***
 *********************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "Lib_Math.h"

/*********************************************************************************************
 *** Structures to be defined                                                              ***
 *********************************************************************************************/

// Name:     TRI_VehicleState_Input
// Function: Input data from sockets
typedef struct TRI_VehicleState_Input{
	double 			X;  			// X
    double 			Y;   			// Y
	int    			CarSpeed;   	// Speed of the car
    unsigned short	Ctrl;
    float  			HeadingAngle;	// Yaw
    float  			Empty1;     	// Empty space
    double 			WheelAngle; 	// Wheel angle
}TRI_VehicleState_Input;

// Name:     TRI_VehicleState_Config
// Function: Configuration of the vehicle state
typedef struct TRI_VehicleState_Config{
    
    float       Long;            // Long in meters
    float       Width;           // Width in meters
    float       Height;          // Height in meters
    float       Wheelbase;       // in spanish Batalla in meters
    float       Track;           // the width between the middle of the tires in meters
    float       Weight;          // in kilograms
    float       WeightFL;
    float       WeightFR;
    float       WeightRL;
    float       WeightRR;
    
    float       GPSOffset_Front;
    float       GPSOffset_Back;
    float       GPSOffset_Left;  // from the front view of the car
    float       GPSOffset_Right; // from the front view of the car
    float       IMUFrontal;
    float       IMULateral;
    float       IMUAngleCorrection;
    float       IMUHeight;
    
    int         PlanningRightTraffic;    //
    float       PlanningRoadWidth;
    int         PlanningPathMode;
    tri_point2D PlanningEndPnt;
    
    int         ControlTimestamp;     // miliseconds
    int         ControlLat_Timestamp;
    int         ControlLat_Algorithm;
    float       Lookahead;
    float       ControlLat_Kp;
    float       ControlLat_Ki;
    float       ControlLat_Kd;
	
    int         ControlLong_Timestamp;
    int         ControlLong_Algorithm;
    int         ControlLong_ExtSp;
    float       ControlLong_MaxSP;
    float       ControlLong_Kp;
    float       ControlLong_Ki;
    float       ControlLong_Kd;
}TRI_VehicleState_Config;

// Name:     TRI_VehicleState_Config
// Function: Configuration of the vehicle state
typedef struct TRI_VehicleState_Commands{
    int    Run_Stop;
    int    Emergency_Stop;
    double SPSpeed;
    int    PathMode;
}TRI_VehicleState_Commands;

// Name:     TRI_VehicleState_Output
// Function: Output of the vehicle state
typedef struct TRI_VehicleState_Output{
    int    PositionID;
    int    Sts_Run_Stop;  // Estado que indica si el sistema esta en Run (1) o Stop (0)
    int    Run_Stop_Past; // Siempre se lleva registro del bit anterior que se dio con el boton de Run / Stop.
    double StartPointX;   //
    double StartPointY;   //
    double EndPointX;     //
    double EndPointY;     //
    float  CarSpeed;      // speed in m/s
    double X;
    double Y;
    float  HeadingAngleRad;
    float  HeadingAngleDeg;
    double WheelBase;
    double MaxSpeed;
}TRI_VehicleState_Output;

// Name:     TRI_VehicleState_Output
// Function: Output of the vehicle state
typedef struct TRI_VehicleState_Actuators{
    unsigned short Acc;
    unsigned short Brake;
    signed short   Steering;
}TRI_VehicleState_Actuators;


// Name:     TRI_VehicleState
// Function: Vehicle state
typedef struct TRI_VehicleState{
    TRI_VehicleState_Input     I;
    TRI_VehicleState_Output    O;
    TRI_VehicleState_Config    C;
    TRI_VehicleState_Commands  Cmd;
    TRI_VehicleState_Actuators Act;
}TRI_VehicleState;

/*********************************************************************************************
 *** Declaration of functions                                                              ***
 *********************************************************************************************/

/* Name:              TRI_VehicleStateBaseConfig
   Note:              - - - - - - -
   Input Parameters:
                      VehState:       Structure with the vehicle state.
                      VehStateConfig: Configuration file.
   Output Parameters:
                      returns a pointer to the vehicle state.
 */
 void TRI_VehicleStateConfigUpdate(TRI_VehicleState *VehState, TRI_VehicleState_Config VehStateConfig);

/* Name:              TRI_VehicleConfigInit
   Note:              - - - - - - -
   Input Parameters:
                      VehStateConfig: Structure with the configuration.
   Output Parameters:
                      returns a pointer to the configuration of vehicle state.
 */
 void TRI_VehicleStateConfigInit(TRI_VehicleState_Config *VehStateConfig);

 /* Name:              TRI_VehicleCmdInit
   Note:              - - - - - - -
   Input Parameters:
                      VehStateConfig: Structure with the commands.
   Output Parameters:
                      returns a pointer to the configuration of vehicle state.
 */
 void TRI_VehicleStateCmdInit(TRI_VehicleState_Commands *VehStateCmd);

/* Name:              TRI_VehicleStateInputUpdate
   Note:              - - - - - - -
   Input Parameters:
                      VehState:      Structure with the vehicle state.
                      VehStateInput: the input data.
   Output Parameters:
                      returns a pointer to the vehicle state.
 */
 void TRI_VehicleStateInputInit(TRI_VehicleState_Input *VehStateInput);

/* Name:              TRI_VehicleStateInputUpdate
   Note:              - - - - - - -
   Input Parameters:
                      VehState: Structure with the vehicle state.
                      Input:    the input data.
   Output Parameters:
                      returns a pointer to the vehicle state.
 */
 void TRI_VehicleStateInputUpdate(TRI_VehicleState *VehState, TRI_VehicleState_Input Input);

/* Name:              TRI_VehicleStateInputUpdate
   Note:              - - - - - - -
   Input Parameters:
                      VehState:      Structure with the vehicle state.
                      VehStateInput: the input data.
   Output Parameters:
                      returns a pointer to the vehicle state.
 */
 void TRI_VehicleStateOutputUpdate(TRI_VehicleState *VehState, TRI_VehicleState_Output VehStateOutput);

/* Name:              TRI_VehicleStateInputUpdate
   Note:              - - - - - - -
   Input Parameters:
                      VehState:      Structure with the vehicle state.
                      VehStateInput: the input data.
   Output Parameters:
                      returns a pointer to the vehicle state.
 */
 void TRI_VehicleStateOutputInit( TRI_VehicleState_Output *VehStateOutput);

/* Name:              TRI_VehicleInit
   Note:              - - - - - - -
   Input Parameters:
                      VehState: Structure with the vehicle state.
   Output Parameters:
                      returns a pointer to the vehicle state.
 */
 void TRI_VehicleStateInit(TRI_VehicleState *VehState);

/* Name:              TRI_VehicleStateScale
   Note:              - - - - - - -
   Input Parameters:
                      VehState: Structure with the vehicle state.
                      Input:    the input data.
   Output Parameters:
                      returns a pointer to the vehicle state.
 */
 void TRI_VehicleStateScale(TRI_VehicleState *VehState);

 #endif
