//============================================================================
// Name        : BufferClass.hpp
// Author      : RL - MM
// Version     : 0.1
// Description :
//============================================================================

#ifndef PLANNING_LOCALMAP_HPP_
#define PLANNING_LOCALMAP_HPP_

/* * * * * *  Standard Libraries * * * * * */
#include <stdlib.h>  // Libreria estandar de C/C++
#include <iostream>  // Libreria estandar de C/C++
#include <list>      // Libreria para el manejo de listas
#include <string>    // Manejo de strings en C/C++
#include <vector>
#include <fstream>

/* * * * * * External Libraries * * * * * */
#include "Eigen/Dense"
#include "tinyxml2.h"

/* * * * * *   Custom Libraries  * * * * * */
#include "Errors.hpp"
#include "Planning_SimpleMap.hpp"
#include "Planning_GlobalMap.hpp"
#include "Math_Circle.hpp"
#include "Math_Bezier.hpp"
/* * * * * * Forward declaration * * * * * */

/* * * * * *     Definition      * * * * * */

namespace tri      {
namespace ad       {
namespace planning {

class BezierParameters{

     public:
        unsigned int    id;
        double          d0;
        double          d1;
        double          d2;
        double          d3;
        double          d4;
        double          d5;
        unsigned int    nPoints;
};


class LocalMap{

    private:
		SimpleMap localMapNodes;
        std::vector<BezierParameters> bezier;

	public:
		/* clear: erase all the elements in the class */
		void clear();

		/* size: returns the size */
        unsigned int getSize();

		/* printFile: printing the map in a .csv file */
		void printFile(const std::string & directory);

		/* appendNode: Add a node at the end of the vector */
        void appendNode(const SimpleMapNode & node);

		/* reverseNodes: swap the nodes */
		void reverseNodes();
		
		/* getNodeInfo: get informatio of the node*/
		SimpleMapNode getNodeInfo(const int & idx);

		void reIndexing();

		void overwriteMapNodes(const std::vector<SimpleMapNode> & map);

		std::vector<SimpleMapNode> extractRoundaboutNodes(const int & idxRnbEntrance, int * idxRnbExit);

		void generateDataBufferInLoop(int index, double offsetInitial, double bufferDistance, std::vector<SimpleMapNode> & map);

		void yawCalcInLoop();

		/* LocalMap: Constructor de Mapa Local */
		LocalMap();

		/* LocalMap: Destructor de Mapa Local */
		~LocalMap();

		unsigned int getParameterSize();

		BezierParameters getParameterInfo(const int & idx);

		void setGlobalMap(GlobalMap globalMap);

		bool parseNode(tinyxml2::XMLElement  *pMap,  BezierParameters *node);

		bool readBezierParameters(const std::string & route);

		bool mapCorrect();

		bool completeMap(double deltaDistance);

		BezierParameters getConfiguration(const int & id);

		bool generateRoundabout(SimpleMapNode pntBef, std::vector<SimpleMapNode> roundaboutNodes, SimpleMapNode pntAft, const BezierParameters & paramNode,double deltaDistance, std::vector<SimpleMapNode> & finalNodes);

		void generateInterAndLaneChange(const SimpleMapNode & pntNodeBef, const SimpleMapNode & pntNode, const SimpleMapNode & pntNodeAfter1, const SimpleMapNode & pntNodeAfter2, const BezierParameters & paramNode, std::vector<SimpleMapNode> & lcNodes);

		int  shortDistPointInLoop(int idxBef, int deltaIdx,bool loop,  const Eigen::Vector3d & point, double & closestDistance, Eigen::Vector3d & closestPnt);

        void generateDataBuffer(int index, Eigen::Vector3d & closestPnt, const int & loop, double bufferDistance);

        double comforSpeedPlanner(double maxSpeed, double comfort);
		
		int   distanceInFront(int idx, bool loop, double distance, Eigen::Vector3d & closestPnt);
};

} /* end of planning */
} /* end of ad       */
} /* end of tri      */
#endif /* PLANNING_LOCALMAP_HPP_ */
