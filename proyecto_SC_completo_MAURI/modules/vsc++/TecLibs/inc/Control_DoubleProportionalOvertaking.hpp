//============================================================================
// Name        : Control_DDoubleProportionalOvertaking.hpp
// Author      : RL
// Version     : 0.1
// Description :
//============================================================================

#ifndef CONTROL_DOUBLEPROPORTIONALOVERTAKING_HPP_
#define CONTROL_DOUBLEPROPORTIONALOVERTAKING_HPP_

/* * * * * *  Standard Libraries * * * * * */
#include<stdlib.h>  // Libreria estandar de C/C++
#include<iostream>  // Libreria estandar de C/C++
#include<list>      // Libreria para el manejo de listas
#include<string>    // Manejo de strings en C/C++
#include<vector>
#include <fstream>

/* * * * * * External Libraries * * * * * */
#include "Eigen/Dense"
#include "tinyxml2.h"

/* * * * * *   Custom Libraries  * * * * * */
#include "Errors.hpp"
#include "Math_Square.hpp"
#include "Planning_SimpleMap.hpp"
#include "Planning_LocalMap.hpp"

/* * * * * * Forward declaration * * * * * */

/* * * * * *     Definition      * * * * * */

namespace tri     {
namespace ad      {
namespace control {


struct vehiclePrediction{
	Eigen::Vector3d refPosition;
	double          yaw;
	double          speed;
	double          acc;

	bool			leftLaneEnable;
	bool			rightLaneEnable;
	double			roadWidth;

	Eigen::Vector3d cornerRL;
	Eigen::Vector3d cornerRR;
	Eigen::Vector3d cornerFR;
	Eigen::Vector3d cornerFL;
};


struct vehicleModel{
    // parameters based on the trajectory
	bool    trajectoryInvert;
	int     lastIdx;

	// vehicle ID
	int     vehIDNumber;

	// Offset vehicle dimension
	double  frontalOffset;
	double  rearOffset;
	double  rightOffset;
	double  leftOffset;

	// Lateral distance:
	double  latDistance;

	// vehicle future predictions.
	std::vector<vehiclePrediction> states;
};

struct OptimizationBounds{
	std::vector<double> lowerBounds;
	std::vector<double> upperBounds;
};

class DoubleProportionalOvertaking{
	public:
		bool                           overtakingToRight;
		double						   delayPrediction;
		double						   mpcTime;
		double						   safetyDistance;
		double						   maxLatSpeed;
		double						   minLatSpeed;
		double						   maxLatAcc;
		double						   minLatAcc;
		double						   minLonPos;
		double						   minLonSpeed;
		double						   maxLonSpeed;
		double						   minLonAcc;
		double						   maxLonAcc;
		double						   lonJerk;

		tri::ad::planning::LocalMap    obsTrajectory;
		tri::ad::planning::LocalMap    obsTrajectoryInvert;
		vehicleModel                   egoVeh;
		std::vector<vehiclePrediction> egoVehParallel;
		std::vector<vehicleModel>      objs;

		std::vector<int> 			   colissionLane;
		std::vector<int>			   colissionParallelLane;

		std::vector<double>			   speedReference;
		std::vector<double>			   latOffsetReference;
		OptimizationBounds             latBoundPos;
		OptimizationBounds             latBoundSpeed;
		OptimizationBounds             latBoundAcc;
		OptimizationBounds             lonBoundPos;
		OptimizationBounds             lonBoundSpeed;
		OptimizationBounds             lonBoundAcc;
		OptimizationBounds             lonBoundJerk;

		std::vector<double>			   latStatePos;
		std::vector<double>			   latStateSpeed;
		std::vector<double>			   latStateAcc;

		std::vector<double>			   lonStatePos;
		std::vector<double>			   lonStateSpeed;
		std::vector<double>			   lonStateAcc;
		std::vector<double>			   lonStateJerk;

		/* Constructor */
		DoubleProportionalOvertaking();

		/* Destructor */
		~DoubleProportionalOvertaking();

		/* clear: erase all the vectors' elements */
		void   clear();

		/* configure: read all the vectors' elements from configuration*/
		bool   configure(std::string fileAddress);

		/* getNumberObstacles: return the number of obstacles */
		int		getNumberObstacles();

		/* generatePredictionsEgoVehicle:  */
		int		generatePredictionsEgoVehicle(const std::vector<double> & distance);

		/* appendNode: add an element to the vectors configuration */
		void	appendObstacle(int vehIDNumber, double frontalOffset, double rearOffset, double rightOffset, double leftOffset, const Eigen::Vector3d & refPosition, double yaw,	double speed, double acc);

		/* appendNode: add an element to the vectors configuration */
		void	appendPredictionObs(int obsIdx, const Eigen::Vector3d & refPosition, double yaw,	double speed, double acc);

		/* generatePredictionObstacles: generate the predictions of the obstacles */
		void	generatePredictionObstacles(double time, int deltaIdx);


		void	appendEgoVeh(double frontalOffset, double rearOffset, double rightOffset, double leftOffset, const Eigen::Vector3d & refPosition, double yaw,	double speed, double acc, bool leftLaneEnable, bool rightLaneEnable, double roadWidth);


		void	appendPredictionEgoVeh(const Eigen::Vector3d & refPosition, double yaw, double speed, double acc, bool leftLaneEnable, bool rightLaneEnable, double roadWidth);


		void    generatePredictionEgoVeh(int startIdx, int deltaIdx, std::vector<double> distance, tri::ad::planning::LocalMap bufferMap);


		void	generateParallelEgoVeh();


		void    verifyCollision();


		void	generateSpeedReference();


		void	generateLatOffsetReference();


		void 	generateLatBoundPos();


		void 	generateLatBoundSpeed(double maxSpeed, double minSpeed, double latPosState0, double latSpeedState0);


		void 	generateLatBoundAcc(double maxLatAcc, double minLatAcc, double latPosState0, double latAccState0);


		
		void 	generateLonBoundPos(double maxDistance, double minDistance);


		void 	generateLonBoundSpeed(double addUpperLimit, double minimumSpeed);


		void 	generateLonBoundAcc(double maxAcc, double maxDec);


		void 	generateLonBoundJerk(double maxJerks);


		double	interpolateLateralOffset(double timestep, double time);


		double 	interpolateLateralSpeed(double timestep, double time);

		double	interpolateLateralAcc(double timestep, double time);

		double	interpolateSpeed(double timestep, double time);

		double	interpolateAcc(double timestep, double time);

		void	overtakingReferencesBounds();

};


} /* end of control */
} /* end of ad      */
} /* end of tri     */
#endif /* CONTROL_DOUBLEPROPORTIONALOVERTAKING_HPP_ */
