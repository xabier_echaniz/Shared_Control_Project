//============================================================================
// Name        : Planning_SimpleMapNode.hpp
// Author      : RL - MM
// Version     : 0.1
// Description :
//============================================================================

#ifndef PLANNING_SIMPLEMAPNODE_HPP_
#define PLANNING_SIMPLEMAPNODE_HPP_

/* * * * * *  Standard Libraries * * * * * */
#include<stdlib.h>  // Libreria estandar de C/C++
#include<iostream>  // Libreria estandar de C/C++
#include<list>      // Libreria para el manejo de listas
#include<string>    // Manejo de strings en C/C++
#include<vector>
#include <fstream>

/* * * * * * External Libraries * * * * * */
#include "Eigen/Dense"
#include "tinyxml2.h"

/* * * * * *   Custom Libraries  * * * * * */
#include "Errors.hpp"
#include "Math_Geometry.hpp"

/* * * * * * Forward declaration * * * * * */

/* * * * * *     Definition      * * * * * */
#define WAYPOINT     0
#define INTERSECTION 1
#define ROUNDABOUT   2
#define RBENTRANCE   3
#define RBMIDDLE     4
#define RBEXIT       5
#define LANECHANGE   6
#define LCSTART      7
#define LCEND        8

namespace tri      {
namespace ad       {
namespace planning {

class SimpleMapNode{
    public:
        unsigned int id;
        double       x;
        double       y;
        double       z;
        unsigned int type;
        double       k;
        double       yaw;
        double       angleInRad;
        double       angleOutRad;
        unsigned int nLanesRight;
        unsigned int nLanesLeft;
        double       roadWidth;
        unsigned int rotateRight;
        double       maxSpeed;

        /* simpleGlobalMapNode: simple constructor*/
        SimpleMapNode();

        /* simpleGlobalMapNode: constructor initialized */
        SimpleMapNode(unsigned int id, double x, double y, double z, unsigned int type,
                      double k, double angleInRad, double angleOutRad, int nLanesRight,
                      int nLaneLeft, double roadWidth, unsigned int rotateRight, double maxSpeed);

        /* clean the node */
        void clean();
};

} /* end of planning */
} /* end of ad       */
} /* end of tri      */
#endif /* PLANNING_SIMPLEMAP_HPP_ */
