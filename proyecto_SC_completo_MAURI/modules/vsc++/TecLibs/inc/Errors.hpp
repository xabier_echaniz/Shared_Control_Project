//============================================================================
// Name        : ADCOREErrors.hpp
// Author      : rayalejandro.lattarulo@tecnalia.com
// Version     : 0.1
// Description :
//============================================================================

#ifndef ERRORS_HPP_
#define ERRORS_HPP_

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

#define ERROR0001 "<Error0001>: The buffer size and the total number of elements is different."
#define ERROR0002 "<Error0002>: The vectors doesn't have the same dimention."
#define ERROR0003 "<Error0003>: The vector to be normalized is <0,0,0>"
#define ERROR0004 "<Error0004>: In the angle between vectors one of them is <0,0,0>"
#define ERROR0005 "<Error0005>: The buffer size has less than 2 elements"
#define ERROR0006 "<Error0006>: The Simple global map route does not exists."
#define ERROR0007 "<Error0007>: Error in the father node. It should be simpleMAP."
#define ERROR0008 "<Error0008>: No nodes where added to the XML."
#define ERROR0009 "<Error0009>: The node doesn't have the id tag."
#define ERROR0010 "<Error0010>: The node doesn't have the type tag."
#define ERROR0011 "<Error0011>: The defined type doesn't exist."
#define ERROR0012 "<Error0012>: The node-x tag doesn't exist."
#define ERROR0013 "<Error0013>: The node-y tag doesn't exist."
#define ERROR0014 "<Error0014>: The node-z tag doesn't exist."
#define ERROR0015 "<Error0015>: The curvature mUst be added to the waypoints or roundabouts points."
#define ERROR0016 "<Error0016>: The road width parameter tag must be specified."
#define ERROR0017 "<Error0017>: The Global Log file was not created."
#define ERROR0018 "<Error0018>: The minimum amount of points must be 3 for map/buffer generation."
#define ERROR0019 "<Error0019>: The start and ending point must be waypoints."
#define ERROR0020 "<Error0020>: Two roundabouts cannot be one to each other or a lane change before a roundabout."
#define ERROR0021 "<Error0021>: The map must have maxSpeed tag in meters per second."
#define ERROR0022 "<Error0022>: In the local map XML the node's id must be specified."
#define ERROR0023 "<Error0023>: In the local map XML the node's d0 must be specified."
#define ERROR0024 "<Error0024>: In the local map XML the node's d1 must be specified."
#define ERROR0025 "<Error0025>: In the local map XML the node's d2 must be specified."
#define ERROR0026 "<Error0026>: In the local map XML the node's d3 must be specified."
#define ERROR0027 "<Error0027>: In the local map XML the node's d4 must be specified."
#define ERROR0028 "<Error0028>: The roundabout must be defined at least with 3 points."
#define ERROR0029 "<Error0029>: The double proportional controller configuration (xml file) does not exists."
#define ERROR0030 "<Error0030>: Error in the father node of the controller. It should be doubleProportionalCurvature."
#define ERROR0031 "<Error0031>: No nodes where added to the XML in the double proportional controller."
#define ERROR0032 "<Error0032>: The speed in the controller configuration must increase from one node to the other."
#define ERROR0033 "<Error0033>: The size of the elements is in the double proportional configuration is not equal."
#define ERROR0034 "<Error0034>: In the double proportional configuration there are no speed tag."
#define ERROR0035 "<Error0035>: In the double proportional configuration there are no kLatError tag."
#define ERROR0036 "<Error0036>: In the double proportional configuration there are no kAngError tag."
#define ERROR0037 "<Error0037>: In the double proportional configuration there are no kCurvature tag."
#define ERROR0038 "<Error0038>: The overtaking double proportional controller configuration (xml file) does not exists."
#define ERROR0039 "<Error0039>: Error in the father node of the overtaking controller. It should be doubleProportionalOvertaking."
#define ERROR0040 "<Error0040>: No config node where added to the XML in the overtaking double proportional controller."
#define ERROR0041 "<Error0041>: The delayPrediction TAG in the configuration of doubleProportionalOvertaking does not exist."
#define ERROR0042 "<Error0042>: The safetyDistance TAG in the configuration of doubleProportionalOvertaking does not exist."
#define ERROR0043 "<Error0043>: The overtakingToRight TAG in the configuration of doubleProportionalOvertaking does not exist."
#define ERROR0044 "<Error0044>: There are not map in the configuration of doubleProportionalOvertaking."
#define ERROR0045 "<Error0045>: There are not nodes in the doubleProportionalOvertaking configuration file."
#define ERROR0046 "<Error0046>: There are not x component in the map nodes."
#define ERROR0047 "<Error0047>: There are not y component in the map nodes."
#define ERROR0048 "<Error0048>: There are not mpcTime attribute in the overtaking controller config."
#define ERROR0049 "<Error0049>: There are not vehicleLat node in the overtaking controller config."
#define ERROR0050 "<Error0050>: There are not maxLatSpeed attribute in the overtaking controller config."
#define ERROR0051 "<Error0051>: There are not maxLatAcc attribute in the overtaking controller config."
#define ERROR0052 "<Error0052>: There are not vehicleLon node in the overtaking controller config."
#define ERROR0053 "<Error0053>: There are not minLonPos attribute in the overtaking controller config."
#define ERROR0054 "<Error0054>: There are not minLonSpeed attribute in the overtaking controller config."
#define ERROR0055 "<Error0055>: There are not maxLonSpeed attribute in the overtaking controller config."
#define ERROR0056 "<Error0056>: There are not minLonAcc attribute in the overtaking controller config."
#define ERROR0057 "<Error0057>: There are not maxLonAcc attribute in the overtaking controller config."
#define ERROR0058 "<Error0058>: There are not lonJerk attribute in the overtaking controller config."
#define ERROR0059 "<Error0059>: There are not minLatSpeed attribute in the overtaking controller config."
#define ERROR0060 "<Error0060>: There are not minLatAcc attribute in the overtaking controller config."

#define WARNING0001 "<Warning0001>: In roundabout node type the angle in was not declared and it was set to 0."
#define WARNING0002 "<Warning0002>: In roundabout node type the angle out was not declared and it was set to 0."
#define WARNING0003 "<Warning0003>: The number of lanes in the right side was not declared and it was set to 0."
#define WARNING0004 "<Warning0004>: The number of lanes in the left side was not declared and it was set to 0."
#define WARNING0005 "<Warning0005>: The sense of rotation in the roundabout was assumed counter clockwise."
#define WARNING0006 "<Warning0006>: The sense of the lane change was not declared (toRight) and it is selected to the left side by default."
#define WARNING0007 "<Warning0007>: In the local map XML the node's d5 will be set by default equal to d4."
#define WARNING0008 "<Warning0008>: The parameter nPoints was set to 10 by default."
#endif /* ERRORS_HPP_ */
