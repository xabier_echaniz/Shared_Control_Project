//============================================================================
// Name        : Planning_GlobalMap.hpp
// Author      : RL - MM
// Version     : 0.1
// Description :
//============================================================================

#ifndef CONTROL_DOUBLEPROPORTIONALK_HPP_
#define CONTROL_DOUBLEPROPORTIONALK_HPP_

/* * * * * *  Standard Libraries * * * * * */
#include<stdlib.h>  // Libreria estandar de C/C++
#include<iostream>  // Libreria estandar de C/C++
#include<list>      // Libreria para el manejo de listas
#include<string>    // Manejo de strings en C/C++
#include<vector>
#include <fstream>

/* * * * * * External Libraries * * * * * */
#include "Eigen/Dense"
#include "tinyxml2.h"

/* * * * * *   Custom Libraries  * * * * * */
#include "Errors.hpp"

/* * * * * * Forward declaration * * * * * */

/* * * * * *     Definition      * * * * * */

namespace tri     {
namespace ad      {
namespace control {

class DoubleProportionalK{
	private:
		std::vector<double> speed;
		std::vector<double> kLatError;
		std::vector<double> kAngError;
		std::vector<double> kCurvature;
	public:
		/* Constructor */
		DoubleProportionalK();
		
		/* Destructor */
		~DoubleProportionalK();
		
		/* clear: erase all the vectors' elements */
		void   clear();
		
		/* getSize: num. elements */
		int		getSize();
		
		/* readConfiguration: read all the vectors' elements from configuration*/
		bool   readConfig(std::string fileAddress);
		
		/* appendNode: add an element to the vectors configuration */
		void	appendNode(double speed, double kLatError, double kAngError, double kCurvature);
		
		/* verify: verify if every elements is in order and the configuration is ok */
		bool   verify();
		
		/* generateOutput: generates the output based on linear interpolation */
		double generateOutput(double speed, double latError, double angError, double curvature);
};

} /* end of control */
} /* end of ad      */
} /* end of tri     */
#endif /* CONTROL_DOUBLEPROPORTIONALK_HPP_ */
