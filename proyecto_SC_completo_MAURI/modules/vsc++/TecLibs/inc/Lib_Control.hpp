/* ====================================================================== 
 * Name:        Lib_Control.hpp
 * Description: Library for the control of the vehicle
 * Note:        - No notes - 
 * Author:      rayalejandro.lattarulo@tecnali.com 23/05/2017
 * Company:     Tecnalia
 * ======================================================================*/
#ifndef LIB_CONTROL_HPP_INCLUDED
#define LIB_CONTROL_HPP_INCLUDED

/* ======================================================================
 * Libraries definition 
 * ======================================================================*/
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<list>
#define _USE_MATH_DEFINES
#include<math.h>
using namespace std;

/* ======================================================================
 * Structures definition 
 * ======================================================================*/
/* Name:        (struct) tri_Ramp_Input
 * Description: the structures contains the input values for a control 
 *				ramp.
 * Author:      rayalejandro.lattarulo@tecnalia.com
 * Company:     Tecnalia */
typedef struct tri_Ramp_Input{
//  Data Type   Name           	Observations            		Units
	double		sp;			//	Set point of the ramp			[~]
	int			reset;		//	Ramp reset						[0-1]
	double		reset_value;//	Reset value for the ramp		[]
}tri_Ramp_Input;

/* Name:        (struct) tri_Ramp_Config
 * Description: the structures contains the Config. values for a control 
 *				ramp.
 * Author:      rayalejandro.lattarulo@tecnalia.com
 * Company:     Tecnalia */
typedef struct tri_Ramp_Config{
//  Data Type   Name           	Observations            		Units
	double		ts;			//	Sample time for the ramp		[s]
	double		m;			//	Slope of the ramp				[~/s]
}tri_Ramp_Config;

/* Name:        (struct) tri_Ramp_Output
 * Description: the structures contains the Output values for a control 
 *				ramp.
 * Author:      rayalejandro.lattarulo@tecnalia.com
 * Company:     Tecnalia */
typedef struct tri_Ramp_Output{
//  Data Type   Name           	Observations            		Units
	double		cv;			//	Sample time for the ramp		[s]
	int			reset;		//	Slope of the ramp				[~/s]
}tri_Ramp_Output;

/* ======================================================================
 * Classes definition
 * ======================================================================*/
class triClass_Ramp{
  private:
 // Data Type               Name	Observations 
    tri_Ramp_Config			c;   // structure for the configuration param.
    tri_Ramp_Input       	i;   // struct. for the input values
    tri_Ramp_Output			o;   // struct. for the output values
    
  public:
 /* Name:           (function) triClass_Ramp (Constructor)
  * Description:    constructor
  * Note:           - no notes -
  * Author:         rayalejandro.lattarulo@tecnalia.com 24/05/2017 */
    triClass_Ramp(void);
    
 /* Name:           (function) config
  * Description:    to configure the ramp
  * Note:           - no notes -
  * Author:         rayalejandro.lattarulo@tecnalia.com 24/05/2017 */
    void config(double ts, double m);
	
 /* Name:           (function) input
  * Description:    the inputs of the ramp
  * Note:           - no notes -
  * Author:         rayalejandro.lattarulo@tecnalia.com 24/05/2017 */
    void input(double sp, int reset, double reset_value);
	
 /* Name:           (function) config
  * Description:    to configure the ramp
  * Note:           - no notes -
  * Author:         rayalejandro.lattarulo@tecnalia.com 24/05/2017 */
    double ramp(void);
};

/* ======================================================================
 * Function definition
 * ======================================================================*/

double TRI_MaxSpeed_BasedOnCurvature(double acc, double iMaxSpeed, double iCurvature);

 #endif
