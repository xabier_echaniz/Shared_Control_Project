/* ====================================================================== 
 * Name:        Lib_Tecnalia.hpp
 * Description: Includes all the .h, .hpp, .c and .cpp that should be 
 *              include in all the modules of a Tecnalia S-Function.
 * Note:        - - No notes - -
 * Author:      rayalejandro.lattarulo@tecnalia.com 19/05/2017
 * Company:     Tecnalia
 * ======================================================================*/
#ifndef Lib_Tecnalia_HPP
#define Lib_Tecnalia_HPP

/* ======================================================================
 * Standard libraries
 * ======================================================================*/
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<list>
#define _USE_MATH_DEFINES
#include<math.h>

/* ======================================================================
 * Tecnalia libraries
 * ======================================================================*/
#include "Lib_Control.hpp"
extern "C" {
	//#include "Lib_CSVAndListManagement.h"
	//#include "Lib_Planning.h"
	#include "Lib_Math.h"
	#include "Lib_VehicleState.h"
	//#include "Lib_Communications.h"
	#include "Lib_Matlab_Dynacar.h"
}

/*
#include "../src/Lib_Control.cpp"
//#include "../src/Lib_CSVAndListManagement.c"
//#include "../src/Lib_Planning.c"
#include "../src/Lib_Math.c"
#include "../src/Lib_VehicleState.c"
//#include "../src/Lib_Communications.c" 
//*/

#define BUFFERSIZE     150
#define HORIZON_METERS 50

//#include "../src/Lib_Matlab_Dynacar.c"

/* ======================================================================
 * MATLAB libraries
 * ======================================================================*/
#include "simstruc.h"

#endif