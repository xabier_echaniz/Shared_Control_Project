//============================================================================
// Name        : Math_Geometry.hpp
// Author      : RL-MM
// Version     : 0.1
// Description : Math library used for geometric calculation.
//============================================================================

#ifndef MATH_BEZIER_HPP_
#define MATH_BEZIER_HPP_

/* * * * * *  Standard Libraries * * * * * */
#include<stdlib.h>  // Libreria estandar de C/C++
#include<iostream>  // Libreria estandar de C/C++
#include<string>    // Manejo de strings en C/C++
#include<cmath>     // Math library
#include<vector>


/* * * * * * External Libraries * * * * * */
#include "Eigen/Dense"

/* * * * * *   Custom Libraries  * * * * * */
#include "errors.hpp"
#include "Math_Geometry.hpp"


/* * * * * * Forward declaration * * * * * */
#define GRAVITY 9.81

/* * * * * *     Definition      * * * * * */
namespace tri  {
namespace ad   {
namespace math {

void constructBezier3er(int nElements, const Eigen::Matrix<double, 4, 3> &controlPoints, std::vector<double> & x, std::vector<double> & y, std::vector<double> & z, std::vector<double> & k);


void constructBezier4to(int nElements, const Eigen::Matrix<double, 5, 3> &controlPoints, std::vector<double> & x, std::vector<double> & y, std::vector<double> & z, std::vector<double> & k);


void constructBezier5to(int nElements, const Eigen::Matrix<double, 6, 3> &controlPoints, std::vector<double> & x, std::vector<double> & y, std::vector<double> & z, std::vector<double> & k);

} /* end of math */
} /* end of ad   */
} /* end of tri      */
#endif /* MATH_GEOMETRY_HPP_ */
