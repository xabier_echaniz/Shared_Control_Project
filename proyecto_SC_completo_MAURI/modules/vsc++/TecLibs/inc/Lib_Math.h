/*********************************************************************************************
 *** Name of the library:        Tecnalia Math library                                     ***
 *** Description of the library:                                                           ***
 *** Autor notes:                TRI means Tecnalia Research and Innovation                ***
 *** Date:                       April 15th, 2016                                          ***
 *** Modifications date:         - - - - - - - - -                                         ***
 *** Version:                    V1.0                                                      ***
 *** Notes about modifications:                                                            ***
 *********************************************************************************************/
#ifndef LIB_MATH_H_INCLUDED
#define LIB_MATH_H_INCLUDED

/*********************************************************************************************
 *** Libraries to be invoked                                                               ***
 *********************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <string.h>

/*********************************************************************************************
 *** Structures to be defined                                                              ***
 *********************************************************************************************/

// Name:     TRI_CSV_Data
// Function: List of nodes with the information
typedef struct tri_point2D{
    double x;
    double y;
}tri_point2D;

typedef struct tri_segment2D{
	tri_point2D p[2];
}tri_segment2D;

typedef struct tri_circle{
	tri_point2D center;
	float       radius;
}tri_circle;

typedef struct tri_ramp{
    double start;
    double end;
    double timestamp;
    double timeAcc;
    double time;
    double out;
}tri_ramp;
/*********************************************************************************************
 *** Declaration of functions                                                              ***
 *********************************************************************************************/

double TRI_Point2D_X(tri_point2D p);

double TRI_Point2D_Y(tri_point2D p);

/* @Name:              TRI_Rotate_point_around_origin
    Note:              - - - - - - -
   @Input  Parameters:
                       @point: point to be rotated.
                       @angle: angle in radians.
   @Output Parameters:
                       @point: pointer to the point that was rotated.
*/
void   TRI_Rotate_point_around_origin(tri_point2D *point, double angle);

void   TRI_Rotate_point_around_pivot(tri_point2D *point, tri_point2D *pivot, double angle);

double TRI_Dot_product(tri_segment2D *s1, tri_segment2D *s2);

double TRI_Cross_product(tri_segment2D *s1, tri_segment2D *s2);

double TRI_Distance_point_segment(tri_point2D point, tri_segment2D segment, tri_point2D *closestpoint);

int    TRI_Segments_Intersected(tri_segment2D s1, tri_segment2D s2, tri_point2D *p);

double TRI_Segments_distance(tri_segment2D s1, tri_segment2D s2);

int    TRI_Intersection_point_circle(tri_point2D pnt, tri_circle circle, tri_point2D *inter);
int    TRI_Intersection_segment_circle(tri_segment2D seg, tri_circle circle, tri_point2D *inter);
int    TRI_Lines_Intersected(tri_segment2D s1, tri_segment2D s2, tri_point2D *p);

void   TRI_Circle_Fit(tri_point2D point1_p, tri_point2D point2_p, tri_point2D point3_p, tri_circle *circle_p);

int    TRI_OneShotRising(int current, int *before);

int    TRI_OneShotFalling(int current, int *before);

double TRI_LateralError(tri_point2D p, tri_point2D p_seg, tri_segment2D s);

double TRI_AngularError(double heading, tri_segment2D s);

double TRI_AngularErrorBetAngle(double heading, double segmentangle);

int    TRI_Is_Perpendicular(tri_point2D point1_p, tri_point2D point2_p, tri_point2D point3_p);

tri_point2D TRI_RotatePoint90DegreesOffset(tri_point2D iPoint, double iHeadingRad, double iOffset);

tri_point2D TRI_RotatePointSegment90DegreesOffset(tri_point2D iPoint, tri_segment2D iSegment, double iOffset);

tri_point2D TRI_Bezier5to(tri_point2D P0, tri_point2D P1, tri_point2D P2, tri_point2D P3, tri_point2D P4, tri_point2D P5, double t);

tri_point2D TRI_SetPointBasedSegment(tri_segment2D iSeg, double iOffset);

double TRI_NBezier_Points(tri_point2D P0, tri_point2D P1, tri_point2D P2, tri_point2D P3, tri_point2D P4, tri_point2D P5);

double TRI_CurvatureBezier5(tri_point2D iP0, tri_point2D iP1, tri_point2D iP2, tri_point2D iP3, tri_point2D iP4, tri_point2D iP5, double it);
double TRI_CurvatureSign(tri_point2D iPast, tri_point2D iCurrent, tri_point2D iFuture);

#endif
