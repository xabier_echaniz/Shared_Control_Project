//============================================================================
// Name        : BufferClass.hpp
// Author      : RL - MM
// Version     : 0.1
// Description :
//============================================================================

#ifndef PLANNING_BUFFER_HPP_
#define PLANNING_BUFFER_HPP_

/* * * * * *  Standard Libraries * * * * * */
#include<stdlib.h>  // Libreria estandar de C/C++
#include<iostream>  // Libreria estandar de C/C++
#include<list>      // Libreria para el manejo de listas
#include<string>    // Manejo de strings en C/C++
#include <vector>

/* * * * * * External Libraries * * * * * */
#include "Eigen/Dense"

/* * * * * *   Custom Libraries  * * * * * */
#include "errors.hpp"
#include "Math_Geometry.hpp"

/* * * * * * Forward declaration * * * * * */

/* * * * * *     Definition      * * * * * */
namespace tri      {
namespace ad       {
namespace planning {

struct bufferNode{
    double x;   // x-coordinate in meters
    double y;   // y-coordinate in meters
    double z;   // z-coordinate in meters
    double vx;  // Longitudinal speed in meters per second
    double k;   // Curvature of the point in meters^-1
    double yaw; // Angle in radians

};

class Buffer {
	private:
		std::vector<bufferNode> nodes;

	public:
		/* Buffer: Declaraci�n del constructor del buffer por defecto */
		Buffer();

		/* ~Buffer: Declaraci�n del destructor del buffer.*/
		~Buffer();

		/* getNumberPoints: get number of points added to buffer */
		unsigned int getNumberPoints();

		/* getNumberSegments: get number of segments added to buffer */
		unsigned int getNumberSegments();

		/* sizeIsValid: returns 0 (false) if the buffer has less than two points and 1 (true)
		 if it has more than 2 (including 2)*/
		int sizeIsValid();

		/* AddNode: A�ade un nodo a la lista */
		void addNode(double x, double y, double z, double vx, double k);

		/* ShortDistIndex:  Regresa el indice que tenga menor distancia contra la trayectoria,
		si regresa un valor negativo significa que el buffer tiene menos de dos puntos */
		double shortDistPoint(const Eigen::Vector3d & point, unsigned int *idx);

        /* distPointIdx: Distance from point to the idx Point in buffer  */
		double distPointIdx(const Eigen::Vector3d & point, unsigned int idx);

        /* distSegmentIdx: Distance from point to the idx Segment in buffer  */
		double distSegmentIdx(const Eigen::Vector3d & point, unsigned int idx);

		/* controlVariables:  Regresa el punto m�s cercano a la trayectoria, el
		angulo del punto y la curvatura */
		void controlVariables(const Eigen::Vector3d & point, double * dist, double * angleSegment, double * curvature);

		/* print: print the content of buffer in screen */
		void print();

};

} /* end of planning */
} /* end of ad       */
} /* end of tri      */
#endif /* PLANNING_BUFFER_HPP_ */
