#include "simstruc.h"
#include "tmwtypes.h"

#include "..\inc\Lib_Control.h"
#include "..\inc\Lib_CSVAndListManagement.h"
#include "..\inc\Lib_Planning.h"
#include "..\inc\Lib_Math.h"
#include "..\inc\Lib_VehicleState.h"
#include "..\inc\Lib_Communications.h"

#define DynaBuffer_CarPC      		7
#define DynaBuffer_HMI       		41
#define DynaBuffer_Commands   		3
#define DynaBuffer_VehState  		61
#define DynaBuffer_Planning  		22
#define DynaBuffer_Node      		20
#define DynaBuffer_NElemBuff 		11
#define DynaBuffer_Comm      		12
#define DynaBuffer_LaserMaxPoints	2500
#define DynaBuffer_LaserMaxSquares	5000


#define DynaObstacle_Buffer 20

// *****************************************************************************************************
// ** Definicion de las entradas en matlab                                                            **
// *****************************************************************************************************

TRI_VehicleState_Input ExtractCarPC(InputRealPtrsType inCarPC);
TRI_VehicleState_Config ExtractHMIConfig(InputRealPtrsType inHMI);
TRI_VehicleState_Commands ExtractHMICmd(InputRealPtrsType inCmd);
TRI_VehicleState ExtractVehicleState(InputRealPtrsType inEgoVehState);
tri_Planning ExtractPlanning(InputRealPtrsType inPlanning);
TRI_CSV_NodeInfo ExtractNode(InputRealPtrsType inMap);
void ExtractBuffer(InputRealPtrsType In6, TRI_LocalPlanner_NodeInfo Buffer[BUFFERSIZE]);
void ExtractCommNode(InputRealPtrsType inCommObject, tri_CommStruct *CommNode);

// *****************************************************************************************************
// ** Definicion de las salidas en matlab                                                             **
// *****************************************************************************************************

void OutputVehState(TRI_VehicleState EgoVehicleState, real_T *oEgoVehState);
void OutputCarPC(TRI_VehicleState_Input inCarPC, real_T *oCarPC);
void OutputPlanning(tri_Planning inPlanning, real_T *oPlanning);
void OutputPlanningEmpty(real_T *oPlanning);
void OutputGlobalMap(TRI_CSV_NodeInfo inCSV, real_T *oCSV);
void OutputGlobalMapEmpty(real_T *oCSV);