//============================================================================
// Name        : Math_squares.hpp
// Author      : RL-MM
// Version     : 0.1
// Description : Math library used for geometric calculation.
//============================================================================

#ifndef MATH_SQUARE_HPP_
#define MATH_SQUARE_HPP_

/* * * * * *  Standard Libraries * * * * * */
#include<stdlib.h>  // Libreria estandar de C/C++
#include<iostream>  // Libreria estandar de C/C++
#include<string>    // Manejo de strings en C/C++
#include<cmath>     // Math library
#include<vector>


/* * * * * * External Libraries * * * * * */
#include "Eigen/Dense"

/* * * * * *   Custom Libraries  * * * * * */
#include "errors.hpp"
#include "Math_Geometry.hpp"


/* * * * * * Forward declaration * * * * * */
#define GRAVITY 9.81

/* * * * * *     Definition      * * * * * */
namespace tri  {
namespace ad   {
namespace math {

struct Square{
	Eigen::Vector3d p0;
	Eigen::Vector3d p1;
	Eigen::Vector3d p2;
	Eigen::Vector3d p3;
};

/*  */
bool isPointIntoSquare(Eigen::Vector3d pnt, Square sqForm);


/*  */
bool squareIntersect( Square square0 , Square square1);


} /* end of math */
} /* end of ad   */
} /* end of tri      */
#endif /* MATH_SQUARE_HPP_ */
