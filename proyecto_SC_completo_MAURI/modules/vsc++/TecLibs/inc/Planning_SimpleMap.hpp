//============================================================================
// Name        : Planning_SimpleMap.hpp
// Author      : RL - MM
// Version     : 0.1
// Description :
//============================================================================

#ifndef PLANNING_SIMPLEMAP_HPP_
#define PLANNING_SIMPLEMAP_HPP_

/* * * * * *  Standard Libraries * * * * * */
#include<stdlib.h>  // Libreria estandar de C/C++
#include<iostream>  // Libreria estandar de C/C++
#include<list>      // Libreria para el manejo de listas
#include<string>    // Manejo de strings en C/C++
#include<vector>
#include <algorithm>
#include <fstream>
#include <sstream>

/* * * * * * External Libraries * * * * * */
#include "Eigen/Dense"
#include "tinyxml2.h"

/* * * * * *   Custom Libraries  * * * * * */
#include "Errors.hpp"
#include "Math_Geometry.hpp"
#include "Time_Calc.hpp"
#include "Planning_SimpleMapNode.hpp"

/* * * * * * Forward declaration * * * * * */

/* * * * * *     Definition      * * * * * */

namespace tri      {
namespace ad       {
namespace planning {

class SimpleMap{

	private:
		std::vector<SimpleMapNode> mapNodes;

	public:
		/* simpleGlobalPlanning: Declaración del constructor del buffer por defecto */
		SimpleMap();

		/* ~simpleGlobalPlanning: Declaración del destructor del buffer.*/
		~SimpleMap();

		/* clear: erase all the elements in the class */
		void clear();

		/* getNodeInfo: get information of the node*/
		SimpleMapNode getNodeInfo(const int & idx);

		/* updateNodeSpeed: Set new speed to node with specified id*/
		void updateNodeSpeed(const int & idx, double speed);

		/* size: returns the size */
        unsigned int getSize();

        /* appendNode: Add a node at the end of the vector */
        void appendNode(SimpleMapNode node);

		/* reverseNodes: swap the nodes */
		void reverseNodes();
		
		/* printFile: printing the map in a .csv file */
		void printFile(std::string directory, std::string name);

		/* allIdsAreDifferent: all ids are different one from each other return 1 if all are different*/
		int allIdsAreDifferent();

		/* overwriteMapNodes: Overwrites the list of points with a new list */
		void overwriteMapNodes(std::vector<SimpleMapNode> map);

		/* reIndexing: overwrite the ids with an organized sequence (1 to n) */
		void reIndexing();
		/* getMapNodes: get all the nodes in the map */
		std::vector<SimpleMapNode> getMapNodes();

		/* shortDistPoint: caluclate the shortest distance to the point list considering if the point vector is closed in a LOOP (1) or not (0)
		   returns the index of the closest segment and the closest point and distance as parameters (pointers) */
		int shortDistPointInLoop(int idxBef, int deltaIdx,bool loop,  const Eigen::Vector3d & point, double & closestDistance, Eigen::Vector3d & closestPnt);

		/* extractRoundaboutNodes: given the index of a roundabout entrance the function will return all the points until a roundabout exit
            adding the roundabout exit at the end.*/
		std::vector<SimpleMapNode> extractRoundaboutNodes(const int & idxRnbEntrance, int * idxRnbExit);

		void   generateDataBufferInLoop(int index, double offsetInitial, double bufferDistance, std::vector<SimpleMapNode> & map);

        double distanceSegment(int index, bool loop);

        double angleSegment(int index, bool loop);

        bool   lastNode(int *index, bool loop);

        void  yawCalcInLoop();
		
		int   distanceInFront(int idx, bool loop, double distance, Eigen::Vector3d & closestPnt);
};

} /* end of planning */
} /* end of ad       */
} /* end of tri      */
#endif /* PLANNING_SIMPLEMAP_HPP_ */
