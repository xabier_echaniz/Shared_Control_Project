/*
 * Name : 		Lib_Tecnalia.h
 * Descripcion: Engloba las librerias necesarias para los bloques de Tecnalia.
 * Author: 		rayalejandro.lattarulo@tecnalia.com 18/05/2017 V1.0
 */

#ifndef Lib_Tecnalia_H
#define Lib_Tecnalia_H


/*
 * Librerias basicas de lenguaje C y C++
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#define _USE_MATH_DEFINES
#include "math.h"


/*
 * Librerias asociadas a MATLAB y al sistema (e.g. Manejo de puertos).
 */
#include "simstruc.h"


/*
 * Librerias de la base de datos Tecnalia
 */
#include "Lib_ControlV2.h"
#include "Lib_CSVAndListManagement.h"
#include "Lib_Planning.h"
#include "Lib_Math.h"
#include "Lib_VehicleState.h"
#include "Lib_Communications.h"
#include "Lib_Matlab_Dynacar.h"

/*#include "../src/Lib_ControlV2.c"
#include "../src/Lib_CSVAndListManagement.c"
#include "../src/Lib_Planning.c"
#include "../src/Lib_Math.c"
#include "../src/Lib_VehicleState.c"
#include "../src/Lib_Communications.c"

#include "../src/Lib_Matlab_Dynacar.c"*/


#endif