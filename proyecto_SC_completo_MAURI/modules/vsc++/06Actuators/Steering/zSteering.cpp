/* ====================================================================== 
 * Name:        zLaser.cpp
 * Description: Code to read the data from the laser Ibeo Lux
 * Author:      mauricio.marcano@tecnalia.com
 * Company:     Tecnalia
 * ======================================================================*/
#define	S_FUNCTION_NAME		zSteering 
#define	S_FUNCTION_LEVEL	2		

/* ======================================================================
 * Libraries definition 
 * ======================================================================*/

#include <windows.h>
#include <iostream>
#include "simplemotion.h"
#include <stdio.h>
#include <thread>
#include <string>

#include "../../TecLibs/inc/Lib_Tecnalia.hpp"

using namespace std;  

int  init   = 1;
int  count_ = 0;

#define COM(S)   ssGetSFcnParam(S, 0)
#define MODE(S)  ssGetSFcnParam(S, 1)

struct steering {
	//std::unique_ptr<std::thread> t1;
	std::thread t1;
	smbus	 busHandle		= 0;
	smint32	 str_position32	= 0;
	smint32  str_velocity32	= 0;
	smint32	 str_torque32	= 0;	
	smuint16 str_position	= 0;
	smuint16 str_velocity	= 0;
	smuint16 str_torque		= 0;
	smuint16 str_status	    = 0;
	int	     control_mode	= 3;
	int	     com_port	    = 0;
	double   version;
	smint32   pos32  = 0;
	smuint16  pos16 = 0;
	smuint16  pos  = 0;
	smint32   fric = 0;
	smint32   damp = 0;
	bool  stop;

};


void move(steering *s, SimStruct *S) {

	real_T  *position  = ssGetOutputPortRealSignal(S, 0);
	real_T	*velocity  = ssGetOutputPortRealSignal(S, 1);
	real_T	*torque    = ssGetOutputPortRealSignal(S, 2);


	std::string str     = std::to_string(s->com_port);
	std::string com     = "COM";
	std::string com_str = com + str;
	char com_[4];
	strcpy(com_, com_str.c_str());
	s->busHandle = smOpenBus(com_);
	//smSetParameter(s->busHandle, 1, SMP_FAST_UPDATE_CYCLE_FORMAT, FAST_UPDATE_CYCLE_FORMAT_DEFAULT);
	//std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	smSetParameter(s->busHandle, 1, SMP_CONTROL_MODE, s->control_mode);
	//std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	smSetParameter(s->busHandle, 1, SMP_TORQUE_EFFECT_FRICTION, s->fric);
	smint32 nul;
	while (!s->stop) {

		if (s->control_mode == CM_TORQUE) {
			//s->pos = (s->pos32 & 0xFFFF);
			//smFastUpdateCycle(s->busHandle, 1, s->pos16, 0, &(s->str_position), &(s->str_status));
			
			//smAppendSetParamCommandToQueue(s->busHandle, SMP_ABSOLUTE_SETPOINT, s->pos32);
			
			smAppendSMCommandToQueue(s->busHandle, SM_SET_WRITE_ADDRESS, SMP_ABSOLUTE_SETPOINT);
			smAppendSMCommandToQueue(s->busHandle, SM_WRITE_VALUE_32B, s->pos32);

			smAppendSMCommandToQueue(s->busHandle, SM_SET_WRITE_ADDRESS, SMP_TORQUE_EFFECT_FRICTION);
			smAppendSMCommandToQueue(s->busHandle, SM_WRITE_VALUE_32B, s->fric);

			smAppendSMCommandToQueue(s->busHandle, SM_SET_WRITE_ADDRESS, SMP_TORQUE_EFFECT_DAMPING);
			smAppendSMCommandToQueue(s->busHandle, SM_WRITE_VALUE_32B, s->damp);

			smAppendSMCommandToQueue(s->busHandle, SM_SET_WRITE_ADDRESS, SMP_RETURN_PARAM_ADDR);
			
			smAppendSMCommandToQueue(s->busHandle, SM_RETURN_VALUE_32B, SMP_ACTUAL_POSITION_FB);
			smAppendSMCommandToQueue(s->busHandle, SM_RETURN_VALUE_32B, SMP_ACTUAL_VELOCITY_FB);
			smAppendSMCommandToQueue(s->busHandle, SM_RETURN_VALUE_32B, SMP_ACTUAL_TORQUE);
			
			smExecuteCommandQueue(s->busHandle, 1);

			smGetQueuedSMCommandReturnValue(s->busHandle, &nul);
			smGetQueuedSMCommandReturnValue(s->busHandle, &nul);			
			
			smGetQueuedSMCommandReturnValue(s->busHandle, &nul);
			smGetQueuedSMCommandReturnValue(s->busHandle, &nul);

			smGetQueuedSMCommandReturnValue(s->busHandle, &nul);
			smGetQueuedSMCommandReturnValue(s->busHandle, &nul);

			smGetQueuedSMCommandReturnValue(s->busHandle, &nul);

			smGetQueuedSMCommandReturnValue(s->busHandle, &(s->str_position32));
			smGetQueuedSMCommandReturnValue(s->busHandle, &(s->str_velocity32));
			smGetQueuedSMCommandReturnValue(s->busHandle, &(s->str_torque32));

			//*position = (double)((s->str_position) / 40000.00);
			*position = (double)(s->str_position32 / 40000.00);
			*velocity = s->str_velocity32;
			*torque = s->str_torque32;
			//std::this_thread::sleep_for(std::chrono::milliseconds(1));
			
			//smSetParameter(s->busHandle, 1, SMP_ABSOLUTE_SETPOINT, s->pos32);
		}
		/*smRead3Parameters(s->busHandle, 1,
			SMP_ACTUAL_POSITION_FB, &(s->str_position32),
			SMP_ACTUAL_VELOCITY_FB, &(s->str_velocity32),
			SMP_ACTUAL_TORQUE, &(s->str_torque32));*/
	}
}                                                                          


/* ====================================================================== 
 * S-Function Configuration 
 * ======================================================================*/
// Numero de puertos de entrada y dimension de los puertos de entrada
#define nInputs  3 //(sizeof nInputsSize / sizeof nInputsSize[0])
#define nOutputs (sizeof nOutputsSize / sizeof nOutputsSize[0])
static int nInputsSize[]  = {1,1,1};			    				// Actually no Inputs, but illegal to declare empty array in strict C++ (GCC allows it, though)
static int nOutputsSize[] = {1,1,1}; 	// Points and Objects
static int nParameters    = 2;								// Ip and Port
static int nPWorks        = 1;								// Laser Class

/* ====================================================================== 
 * Configuration parameters method 
 * ======================================================================*/
//#define cMode(S) ssGetSFcnParam(S, 0)
#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE)
	static void mdlCheckParameters(SimStruct *S){}
#endif

/* ====================================================================== 
 * Initialization method
 * ======================================================================*/
#define MDL_INITIALIZE_CONDITIONS 
#if defined(MDL_INITIALIZE_CONDITIONS) && defined(MATLAB_MEX_FILE) 
static void mdlInitializeConditions(SimStruct *S){} 
#endif 

/* ====================================================================== 
 * Start method (Just runs pushing play button)
 * ======================================================================*/
#define MDL_START 
#if defined(MDL_START) 
static void mdlStart(SimStruct *S)
{
/* ====================================================================== 
 * Start your code here.
 * ======================================================================*/
    //mxGetString(IP(S), c->LaserIP, bufLen);
	//ssGetPWork(S)[0] = (void *)c; // new std::thread(foo);
	int com_port     = *mxGetPr(COM(S));
	int control_mode = *mxGetPr(MODE(S));
	steering * s	 = new steering();
	
	ssGetPWork(S)[0] = (void *)s;
	s->com_port		 = com_port;
	s->control_mode  = control_mode;
	s->stop			 = false;
	s->t1			 = std::thread(move, s, S);
/* ====================================================================== 
 * Finish your code here.
 * ======================================================================*/ 
}
#endif

/* ======================================================================= 
 * Initialize method (Just runs pushing play button)
 * ======================================================================*/
static	void	mdlInitializeSizes( SimStruct *S ){
	ssSetNumSFcnParams( S, nParameters  );
    #if defined(MATLAB_MEX_FILE)
		if(nParameters != 0){
			if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)){
				mdlCheckParameters(S);
				if (ssGetErrorStatus(S) != NULL){
					ssSetErrorStatus(S, "Parameter to S-function are not \
                                         configure.");
					return;
				}
			}else{
				ssSetErrorStatus(S, "The number of parameters is not  \
                                     consistent.");
				return;
			}
		}
    #endif
	int i;
    for ( i = 0; i < ssGetNumSFcnParams(S); i++ ){
        ssSetSFcnParamTunable( S, i, SS_PRM_SIM_ONLY_TUNABLE );
    }
	
	ssSetNumContStates( S, 0 );						
	ssSetNumDiscStates( S, 0 );

	ssSetNumInputPorts( S, nInputs );	
	for(i=0; i < nInputs ; i = i + 1){
		ssSetInputPortWidth( S, i, nInputsSize[i]); 				
		ssSetInputPortDirectFeedThrough( S, i, 1 );
	}

	ssSetNumOutputPorts( S, nOutputs );	
	for(i=0; i < nOutputs ; i = i + 1){
		ssSetOutputPortWidth( S, i, nOutputsSize[i]);
		ssSetOutputPortDataType(S, i, SS_DOUBLE);
		ssSetOutputPortComplexSignal(S, i, COMPLEX_NO);
	}
    
	ssSetNumSampleTimes( S, 1);						
    ssSetOptions(S, (SS_OPTION_EXCEPTION_FREE_CODE |
                     SS_OPTION_USE_TLC_WITH_ACCELERATOR | 
		             SS_OPTION_WORKS_WITH_CODE_REUSE));    
	ssSetNumPWork(S, nPWorks); 
}

static	void	mdlInitializeSampleTimes( SimStruct *S ){
	ssSetSampleTime( S, 0, 0.01); //INHERITED_SAMPLE_TIME  				
	ssSetOffsetTime( S, 0, 0.0);						
}

/* ====================================================================== 
 * Output method.
 * ======================================================================*/			
static	void	mdlOutputs( SimStruct *S, int_T tid ){
	
/* ====================================================================== 
 * Assign Input, Outputs and PWorks. Initialization Function
 * ======================================================================*/
	InputRealPtrsType	set_point		= ssGetInputPortRealSignalPtrs(S, 0);
	InputRealPtrsType	set_point_fric	= ssGetInputPortRealSignalPtrs(S, 1);
	InputRealPtrsType	set_point_damp  = ssGetInputPortRealSignalPtrs(S, 2);

	steering * s = (steering *)ssGetPWork(S)[0];
	
	if (init == 1) {
		init = 0;
	}

	s->pos32        = (smint32) *set_point[0];
	s->pos16        = (smint16) *set_point[0];
	s->fric         = (int)     *set_point_fric[0];
	s->damp         = (int)     *set_point_damp[0];
}
/* ====================================================================== 
 * Terminate method
 * ======================================================================*/
static	void	mdlTerminate( SimStruct *S ){
/* ====================================================================== 
 * Start your code here.
 * ======================================================================*/	
    init  = 1;
    count_ = 0;

	steering * s = (steering *)ssGetPWork(S)[0];

	s->stop = true;
	s->t1.join();

	smCloseBus(s->busHandle);
	delete s;


	printf("Terminate OK\n");
	
/* ====================================================================== 
 * Finish your code here.
 * ======================================================================*/
}

#ifdef	MATLAB_MEX_FILE						
	#include	"simulink.c"				
#else								
	#include	"cg_sfun.h"				
#endif

