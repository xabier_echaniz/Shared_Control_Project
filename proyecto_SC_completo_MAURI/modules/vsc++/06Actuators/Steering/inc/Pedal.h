#pragma once

#define STRICT
#define DIRECTINPUT_VERSION 0x0800
#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_SECURE_NO_WARNINGS
#ifndef _WIN32_DCOM
#define _WIN32_DCOM
#endif

#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <commctrl.h>
#include <basetsd.h>

#pragma warning(push)
#pragma warning(disable:6000 28251)
#include <dinput.h>
#pragma warning(pop)

#include <dinputd.h>
#include <iostream>
#include <assert.h>
#include <oleauto.h>
#include <shellapi.h>



//-----------------------------------------------------------------------------
// Defines, constants, and global variables
//-----------------------------------------------------------------------------
#define SAFE_DELETE(p)  { if(p) { delete (p);     (p)=nullptr; } }
#define SAFE_RELEASE(p) { if(p) { (p)->Release(); (p)=nullptr; } }

class Pedal
{
private:
	// Variables

	int acceleratorAxis;
	int brakeAxis;
	double accelerator;
	double brake;

	
public:
	Pedal();
	~Pedal();
	//Functions
	static LPDIRECTINPUT8       di;
	static LPDIRECTINPUTDEVICE8 joystick;

	HRESULT   poll();
	int       init();
	int       getAcceleratorAxis();
	int       getBrakeAxis();
	double    getBrake();
	double    getAccelerator();
	void      update();
	static    BOOL CALLBACK enumCallback(const DIDEVICEINSTANCE* instance, VOID* context);
	static    BOOL CALLBACK enumAxesCallback(const DIDEVICEOBJECTINSTANCE* instance, VOID* context);

};

