/* ====================================================================== 
 * Name:        zLaser.cpp
 * Description: Code to read the data from the laser Ibeo Lux
 * Author:      mauricio.marcano@tecnalia.com
 * Company:     Tecnalia
 * ======================================================================*/
#define	S_FUNCTION_NAME		zPedals
#define	S_FUNCTION_LEVEL	2		

/* ======================================================================
 * Libraries definition 
 * ======================================================================*/

#include <iostream>
#include <windows.h>
#include <SDL.h>
#include "../../TecLibs/inc/Lib_Tecnalia.hpp"

#define ID(S)   ssGetSFcnParam(S, 0)
using namespace std;  

int  init   = 1;
int  count_ = 0;
int  count2 = 0;
int  INIT_ERROR;


/* ====================================================================== 
 * S-Function Configuration 
 * ======================================================================*/
// Numero de puertos de entrada y dimension de los puertos de entrada
#define nInputs  0 //(sizeof nInputsSize / sizeof nInputsSize[0])
#define nOutputs (sizeof nOutputsSize / sizeof nOutputsSize[0])
static int nInputsSize[]  = {0};			    				// Actually no Inputs, but illegal to declare empty array in strict C++ (GCC allows it, though)
static int nOutputsSize[] = {1,1}; 	// Points and Objects
static int nParameters    = 1;								// Ip and Port
static int nPWorks        = 0;								// Laser Class

/* ====================================================================== 
 * Configuration parameters method 
 * ======================================================================*/
//#define cMode(S) ssGetSFcnParam(S, 0)
#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE)
	static void mdlCheckParameters(SimStruct *S){}
#endif

/* ====================================================================== 
 * Initialization method
 * ======================================================================*/
#define MDL_INITIALIZE_CONDITIONS 
#if defined(MDL_INITIALIZE_CONDITIONS) && defined(MATLAB_MEX_FILE) 
static void mdlInitializeConditions(SimStruct *S){} 
#endif 

/* ====================================================================== 
 * Start method (Just runs pushing play button)
 * ======================================================================*/
#define MDL_START 
#if defined(MDL_START) 
static void mdlStart(SimStruct *S)
{
/* ====================================================================== 
 * Start your code here.
 * ======================================================================*/
	
	INIT_ERROR = SDL_Init(SDL_INIT_JOYSTICK);
	
	
/* ====================================================================== 
 * Finish your code here.
 * ======================================================================*/ 
}
#endif

/* ======================================================================= 
 * Initialize method (Just runs pushing play button)
 * ======================================================================*/
static	void	mdlInitializeSizes( SimStruct *S ){
	ssSetNumSFcnParams( S, nParameters  );
    #if defined(MATLAB_MEX_FILE)
		if(nParameters != 0){
			if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)){
				mdlCheckParameters(S);
				if (ssGetErrorStatus(S) != NULL){
					ssSetErrorStatus(S, "Parameter to S-function are not \
                                         configure.");
					return;
				}
			}else{
				ssSetErrorStatus(S, "The number of parameters is not  \
                                     consistent.");
				return;
			}
		}
    #endif
	int i;
    for ( i = 0; i < ssGetNumSFcnParams(S); i++ ){
        ssSetSFcnParamTunable( S, i, SS_PRM_SIM_ONLY_TUNABLE );
    }
	
	ssSetNumContStates( S, 0 );						
	ssSetNumDiscStates( S, 0 );

	ssSetNumInputPorts( S, nInputs );	
	for(i=0; i < nInputs ; i = i + 1){
		ssSetInputPortWidth( S, i, nInputsSize[i]); 				
		ssSetInputPortDirectFeedThrough( S, i, 1 );
	}

	ssSetNumOutputPorts( S, nOutputs );	
	for(i=0; i < nOutputs ; i = i + 1){
		ssSetOutputPortWidth( S, i, nOutputsSize[i]);
		ssSetOutputPortDataType(S, i, SS_DOUBLE);
		ssSetOutputPortComplexSignal(S, i, COMPLEX_NO);
	}
    
	ssSetNumSampleTimes( S, 1);						
    ssSetOptions(S, (SS_OPTION_EXCEPTION_FREE_CODE |
                     SS_OPTION_USE_TLC_WITH_ACCELERATOR | 
		             SS_OPTION_WORKS_WITH_CODE_REUSE));    
	ssSetNumPWork(S, nPWorks); 
}

static	void	mdlInitializeSampleTimes( SimStruct *S ){
	ssSetSampleTime( S, 0, 0.001); //INHERITED_SAMPLE_TIME  				
	ssSetOffsetTime( S, 0, 0.0);						
}

/* ====================================================================== 
 * Output method.
 * ======================================================================*/			
static	void	mdlOutputs( SimStruct *S, int_T tid ){
	
/* ====================================================================== 
 * Assign Input, Outputs and PWorks. Initialization Function
 * ======================================================================*/
 

	real_T				*accel          = ssGetOutputPortRealSignal(S, 0);
	real_T				*brake			= ssGetOutputPortRealSignal(S, 1);

	int id = *mxGetPr(ID(S));
	/* Other initializtion code goes here */

	int min_brake = -32220;
	int max_brake = -21000;
	int max_accel = 32767;
	int min_accel = -32768;

	double brake_t = 0;
	double accel_t = 0;

	// Init
	if (init == 1) {
		
		if (INIT_ERROR < 0)
		{
			ssPrintf("error\n");
			exit(1);
		}
		count_ = SDL_NumJoysticks();
		ssPrintf("test %d\n", count_);
		init = 0;
	}

	// Joystick Variable
	SDL_Joystick *joystick;
    SDL_JoystickEventState(SDL_ENABLE);
	joystick = SDL_JoystickOpen(id);
	SDL_Event event;


		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_KEYDOWN:
				/* handle keyboard stuff here */
				break;

			case SDL_QUIT:
				/* Set whatever flags are necessary to */
				/* end the main game loop here */
				break;
				
			case SDL_JOYAXISMOTION:  /* Handle Joystick Motion */
				//if ((event.jaxis.value < -3200) || (event.jaxis.value > 3200))
				//{
				if (event.jaxis.axis == 1) {
					brake_t = (double)(event.jaxis.value);
					brake_t = (brake_t - min_brake) / (max_brake - min_brake);
					brake_t = max(min(1, brake_t), 0);
					*brake  = brake_t;
				}
				else if (event.jaxis.axis == 2) {
					accel_t = (double)(event.jaxis.value);
					accel_t = (accel_t - min_accel) / (max_accel - min_accel);
					accel_t = max(min(1, accel_t), 0);
					*accel  = accel_t;
				}
				break;
			}
		}
}
/* ====================================================================== 
 * Terminate method
 * ======================================================================*/
static	void	mdlTerminate( SimStruct *S ){
/* ====================================================================== 
 * Start your code here.
 * ======================================================================*/	
    init  = 1;
    count_ = 0;

	SDL_Quit();

	printf("Terminate OK\n");
	
/* ====================================================================== 
 * Finish your code here.
 * ======================================================================*/
}

#ifdef	MATLAB_MEX_FILE						
	#include	"simulink.c"				
#else								
	#include	"cg_sfun.h"				
#endif

