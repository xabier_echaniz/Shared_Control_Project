#include "../inc/Pedal.h"
#include "../inc/resource.h"

#include <functional>



LPDIRECTINPUT8        Pedal::di       = nullptr;
LPDIRECTINPUTDEVICE8  Pedal::joystick = nullptr;


Pedal::Pedal()
{

}

Pedal::~Pedal()
{

}

int Pedal::getBrakeAxis() {
	return this->brakeAxis;
}

int Pedal::getAcceleratorAxis() {
	return this->acceleratorAxis;
}

void Pedal::update() {
	this->poll();
	this->brake = 2; // ((double)this->getBrakeAxis() + 990) / 340;
	this->accelerator = ((double)this->getAcceleratorAxis() + 705) / 580;
}

double Pedal::getBrake() {
	return this->brake;
}

double Pedal::getAccelerator() {
	return this->accelerator;
}

int Pedal::init() {
	
	HRESULT hr;

	// Create a DirectInput device
	if (FAILED(hr = DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION,
		IID_IDirectInput8, (VOID**)&di, NULL))) {
		return 1; // hr;
	}

	// Look for the first simple joystick we can find.
	if (FAILED(hr = di->EnumDevices(DI8DEVCLASS_GAMECTRL, enumCallback, NULL, DIEDFL_ATTACHEDONLY))) {
		return 2; // hr;
	}
	//}	if (FAILED(hr = di->EnumDevices(DI8DEVCLASS_GAMECTRL, std::bind(&Pedal::enumCallback, this, 
	//	std::placeholders::_1, std::placeholders::_2),NULL, DIEDFL_ATTACHEDONLY))) {
	//	return 2; // hr;
	//}

	// Make sure we got a joystick
	if (joystick == NULL) {
		printf("Joystick not found.\n");
		return 3; // E_FAIL;
	}

	DIDEVCAPS capabilities;

	// Set the data format to "simple joystick" - a predefined data format 
	//
	// A data format specifies which controls on a device we are interested in,
	// and how they should be reported. This tells DInput that we will be
	// passing a DIJOYSTATE2 structure to IDirectInputDevice::GetDeviceState().
	if (FAILED(hr = joystick->SetDataFormat(&c_dfDIJoystick2))) {
		return 4; // hr;
	}
	// Set the cooperative level to let DInput know how this device should
	// interact with the system and with other DInput applications.
	if (FAILED(hr = joystick->SetCooperativeLevel(NULL, DISCL_NONEXCLUSIVE |
		DISCL_BACKGROUND))) {
		return 5; // hr;
	}
	// Determine how many axis the joystick has (so we don't error out setting
	// properties for unavailable axis)
	capabilities.dwSize = sizeof(DIDEVCAPS);
	if (FAILED(hr = joystick->GetCapabilities(&capabilities))) {
		return 6; // hr;
	}
	if (FAILED(hr = joystick->EnumObjects(enumAxesCallback, NULL, DIDFT_AXIS))) {
		return 7; // hr;
	}

	return S_OK;
 }


BOOL CALLBACK Pedal::enumAxesCallback(const DIDEVICEOBJECTINSTANCE* instance, VOID* context)
{
	HWND hDlg = (HWND)context;

	DIPROPRANGE propRange;
	propRange.diph.dwSize = sizeof(DIPROPRANGE);
	propRange.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	propRange.diph.dwHow = DIPH_BYID;
	propRange.diph.dwObj = instance->dwType;
	propRange.lMin = -1000;
	propRange.lMax = +1000;

	// Set the range for the axis
	if (FAILED(joystick->SetProperty(DIPROP_RANGE, &propRange.diph))) {
		return DIENUM_STOP;
	}

	return DIENUM_CONTINUE;
}


BOOL CALLBACK Pedal::enumCallback(const DIDEVICEINSTANCE* instance, VOID* context)
{
	HRESULT hr;

	// Obtain an interface to the enumerated joystick.
	hr = di->CreateDevice(instance->guidInstance, &joystick, NULL);

	// If it failed, then we can't use this joystick. (Maybe the user unplugged
	// it while we were in the middle of enumerating it.)
	if (FAILED(hr)) {
		return DIENUM_CONTINUE;
	}

	// Stop enumeration. Note: we're just taking the first joystick we get. You
	// could store all the enumerated joysticks and let the user pick.
	return DIENUM_STOP;
}

HRESULT Pedal::poll() {
	HRESULT     hr;
	DIJOYSTATE2 js;

	if (joystick == NULL) {
		return S_OK;
	}

	// Poll the device to read the current state
	hr = joystick->Poll();
	if (FAILED(hr)){
		// DInput is telling us that the input stream has been
		// interrupted. We aren't tracking any state between polls, so
		// we don't have any special reset that needs to be done. We
		// just re-acquire and try again.

		hr = joystick->Acquire();
		while (hr == DIERR_INPUTLOST) {
			hr = joystick->Acquire();
		}


		// If we encounter a fatal error, return failure.
		if ((hr == DIERR_INVALIDPARAM) || (hr == DIERR_NOTINITIALIZED)) {
			return E_FAIL;
		}

		// If another application has control of this device, return successfully.
		// We'll just have to wait our turn to use the joystick.
		if (hr == DIERR_OTHERAPPHASPRIO) {
			return S_OK;
		}
	}

	// Get the input's device state
	if (FAILED(hr = joystick->GetDeviceState(sizeof(DIJOYSTATE2), &js))) {
		return hr; // The device should have been acquired during the Poll()
	}

	this->brakeAxis       = js.lY;
	this->acceleratorAxis = js.lZ;

	return S_OK;
}
