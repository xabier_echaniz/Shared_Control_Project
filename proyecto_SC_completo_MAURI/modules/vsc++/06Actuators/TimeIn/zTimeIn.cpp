/* ====================================================================== 
 * Name:        zLaser.cpp
 * Description: Code to read the data from the laser Ibeo Lux
 * Author:      mauricio.marcano@tecnalia.com
 * Company:     Tecnalia
 * ======================================================================*/
#define	S_FUNCTION_NAME		zTimeIn 
#define	S_FUNCTION_LEVEL	2		

/* ======================================================================
 * Libraries definition 
 * ======================================================================*/

#include <windows.h>
#include <iostream>
#include <chrono>
#include <stdio.h>
#include <thread>
#include <string>

#include "../../TecLibs/inc/Lib_Tecnalia.hpp"

using namespace std;  

int  init   = 1;
int  count_ = 0;


struct timeIn {
	//std::unique_ptr<std::thread> t1;
	std::thread t1;
	bool stop = false;
	long to = 0;
};


void measure(timeIn *t, SimStruct *S) {

	real_T  *time1 = ssGetOutputPortRealSignal(S, 0);
	
	while (!t->stop) {


		auto start_time = chrono::high_resolution_clock::now();
		auto tw = chrono::duration_cast<std::chrono::microseconds>(start_time.time_since_epoch()).count();

		*time1 = tw;
		
	    //std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
}                                                                          


/* ====================================================================== 
 * S-Function Configuration 
 * ======================================================================*/
// Numero de puertos de entrada y dimension de los puertos de entrada
#define nInputs  0 //(sizeof nInputsSize / sizeof nInputsSize[0])
#define nOutputs (sizeof nOutputsSize / sizeof nOutputsSize[0])
static int nInputsSize[]  = {0};			    				// Actually no Inputs, but illegal to declare empty array in strict C++ (GCC allows it, though)
static int nOutputsSize[] = {1}; 	// Points and Objects
static int nParameters    = 0;								// Ip and Port
static int nPWorks        = 1;								// Laser Class

/* ====================================================================== 
 * Configuration parameters method 
 * ======================================================================*/
//#define cMode(S) ssGetSFcnParam(S, 0)
#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE)
	static void mdlCheckParameters(SimStruct *S){}
#endif

/* ====================================================================== 
 * Initialization method
 * ======================================================================*/
#define MDL_INITIALIZE_CONDITIONS 
#if defined(MDL_INITIALIZE_CONDITIONS) && defined(MATLAB_MEX_FILE) 
static void mdlInitializeConditions(SimStruct *S){} 
#endif 

/* ====================================================================== 
 * Start method (Just runs pushing play button)
 * ======================================================================*/
#define MDL_START 
#if defined(MDL_START) 
static void mdlStart(SimStruct *S)
{
/* ====================================================================== 
 * Start your code here.
 * ======================================================================*/
    //mxGetString(IP(S), c->LaserIP, bufLen);
	//ssGetPWork(S)[0] = (void *)c; // new std::thread(foo);

	timeIn * t	 = new timeIn();
	
	ssGetPWork(S)[0] = (void *)t;
	t->t1			 = std::thread(measure, t, S);
/* ====================================================================== 
 * Finish your code here.
 * ======================================================================*/ 
}
#endif

/* ======================================================================= 
 * Initialize method (Just runs pushing play button)
 * ======================================================================*/
static	void	mdlInitializeSizes( SimStruct *S ){
	ssSetNumSFcnParams( S, nParameters  );
    #if defined(MATLAB_MEX_FILE)
		if(nParameters != 0){
			if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)){
				mdlCheckParameters(S);
				if (ssGetErrorStatus(S) != NULL){
					ssSetErrorStatus(S, "Parameter to S-function are not \
                                         configure.");
					return;
				}
			}else{
				ssSetErrorStatus(S, "The number of parameters is not  \
                                     consistent.");
				return;
			}
		}
    #endif
	int i;
    for ( i = 0; i < ssGetNumSFcnParams(S); i++ ){
        ssSetSFcnParamTunable( S, i, SS_PRM_SIM_ONLY_TUNABLE );
    }
	
	ssSetNumContStates( S, 0 );						
	ssSetNumDiscStates( S, 0 );

	ssSetNumInputPorts( S, nInputs );	
	for(i=0; i < nInputs ; i = i + 1){
		ssSetInputPortWidth( S, i, nInputsSize[i]); 				
		ssSetInputPortDirectFeedThrough( S, i, 1 );
	}

	ssSetNumOutputPorts( S, nOutputs );	
	for(i=0; i < nOutputs ; i = i + 1){
		ssSetOutputPortWidth( S, i, nOutputsSize[i]);
		ssSetOutputPortDataType(S, i, SS_DOUBLE);
		ssSetOutputPortComplexSignal(S, i, COMPLEX_NO);
	}
    
	ssSetNumSampleTimes( S, 1);						
    ssSetOptions(S, (SS_OPTION_EXCEPTION_FREE_CODE |
                     SS_OPTION_USE_TLC_WITH_ACCELERATOR | 
		             SS_OPTION_WORKS_WITH_CODE_REUSE));    
	ssSetNumPWork(S, nPWorks); 
}

static	void	mdlInitializeSampleTimes( SimStruct *S ){
	ssSetSampleTime( S, 0, 0.001); //INHERITED_SAMPLE_TIME  				
	ssSetOffsetTime( S, 0, 0.0);						
}

/* ====================================================================== 
 * Output method.
 * ======================================================================*/			
static	void	mdlOutputs( SimStruct *S, int_T tid ){
	
/* ====================================================================== 
 * Assign Input, Outputs and PWorks. Initialization Function
 * ======================================================================*/


	//timeIn * t = (timeIn *)ssGetPWork(S)[0];
	
	if (init == 1) {
		init = 0;
	}
	//*time1 = 1;
	//std::this_thread::sleep_for(std::chrono::milliseconds(1));

}
/* ====================================================================== 
 * Terminate method
 * ======================================================================*/
static	void	mdlTerminate( SimStruct *S ){
/* ====================================================================== 
 * Start your code here.
 * ======================================================================*/	
    init  = 1;
    count_ = 0;

	timeIn * t = (timeIn *)ssGetPWork(S)[0];

	t->stop = true;
	t->t1.join();
	delete t;


	printf("Terminate OK\n");
	
/* ====================================================================== 
 * Finish your code here.
 * ======================================================================*/
}

#ifdef	MATLAB_MEX_FILE						
	#include	"simulink.c"				
#else								
	#include	"cg_sfun.h"				
#endif

