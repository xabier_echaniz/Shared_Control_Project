This Granity package contains also a Linux version of Granity.

The Linux version has been compiled on LinuxCNC 2.6 distribution, and there
is no guarantee that it will work on other distributions due to 
system library differences.

To use it, extract zip and make GranityLinux file executable on a console with command:

  chmod +x GranityLinux

Then start it by:

  ./GranityLinux

This works in an ideal case where all necessary system libraries are installed. 
However, in typical cases few additional libraries (like 32 bit Qt4) are needed. 
For distribution specific instructions and library dependencies, please see:

http://granitedevices.com/wiki/Granity_for_Linux

