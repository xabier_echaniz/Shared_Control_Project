
#include <wx/wx.h>
#include <wx/slider.h>
#include <wx/sizer.h>
#include <iostream>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float32.hpp"

using namespace std;

#define ARRAY_SIZE 2

class MinimalPublisher : public rclcpp::Node
{
public:
    MinimalPublisher()
        : Node("minimal_publisher")
    {
        publisher_ = this->create_publisher<std_msgs::msg::Float32>("torque_command", 1);
    }

    void publishFloat(float torque_value)
    {

		int i;
        std_msgs::msg::Float32 message;
        message.data = torque_value;

		publisher_->publish(message);

    }

private:
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr publisher_;
};

struct mySlider {

        string name;
        double value;
        int min_value;
        int max_value;
};

class MyFrame : public wxFrame {
public:

    int resolution = 100; // 10 = 1 decimal || 100 = 2 decimal || 1000 = 3 decimal || ...

//  mySlider ---> name_slider = {"title", init_value, min_value, max_value}
    mySlider slider01 = {"torque",    0,    -3000,  3000  }; 
    mySlider slider02 = {"...",       0,    0,  15  };

    MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
        : wxFrame(nullptr, wxID_ANY, title, pos, size), node_(std::make_shared<MinimalPublisher>()) {
        

        // Store the node
        node_ = std::make_shared<MinimalPublisher>();

        // Create a vertical box sizer
        wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

        // ============ SLIDER 01 =============
        // Create a label for the slider
        wxString labelText01 = wxString::Format("%s : %.2f [0 - %d]", slider01.name, slider01.value, slider01.max_value);
        m_label01 = new wxStaticText(this, wxID_ANY, labelText01);
        sizer->Add(m_label01, 0, wxALIGN_CENTER | wxALL, 5);
        // Create the slider
        m_slider01 = new wxSlider(this, wxID_ANY, slider01.value * resolution, slider01.min_value, slider01.max_value * resolution, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
        sizer->Add(m_slider01, 0, wxEXPAND | wxALL, 5);
        // Connect the event for the first slider
        m_slider01->Bind(wxEVT_SCROLL_THUMBTRACK, &MyFrame::OnSlider01Scroll, this);

        // ============ SLIDER 02 =============
        wxString labelText02 = wxString::Format("%s : %.2f [0 - %d]", slider02.name, slider02.value, slider02.max_value);
        m_label02 = new wxStaticText(this, wxID_ANY, labelText02);
        sizer->Add(m_label02, 0, wxALIGN_CENTER | wxALL, 5);
        m_slider02 = new wxSlider(this, wxID_ANY, slider02.value * resolution, slider02.min_value, slider02.max_value * resolution, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
        sizer->Add(m_slider02, 0, wxEXPAND | wxALL, 5);
        m_slider02->Bind(wxEVT_SCROLL_THUMBTRACK, &MyFrame::OnSlider02Scroll, this);

        SetSizerAndFit(sizer);
        Centre();

        // Create a timer to continuously log slider values
        m_timer = new wxTimer(this, ID_TIMER);
        m_timer->Start(10); // Log values every 1 second
        Bind(wxEVT_TIMER, &MyFrame::OnTimer, this);
    }

private:
    enum {
        ID_TIMER = 1000
    };

    wxSlider* m_slider01;
    wxSlider* m_slider02;


    wxStaticText* m_label01;
    wxStaticText* m_label02;


    wxTimer* m_timer;
    float mySliderData[ARRAY_SIZE];
    std::shared_ptr<MinimalPublisher> node_;
    

    void OnSlider01Scroll(wxScrollEvent& event) {
        slider01.value = static_cast<double>(m_slider01->GetValue())/resolution;
        m_label01->SetLabel(wxString::Format("%s : %.2f [0 - %d]", slider01.name, slider01.value, slider01.max_value));
    }

    void OnSlider02Scroll(wxScrollEvent& event) {
        slider02.value = static_cast<double>(m_slider02->GetValue())/resolution;
        m_label02->SetLabel(wxString::Format("%s : %.2f [0 - %d]", slider02.name, slider02.value, slider02.max_value));
    }


    void OnTimer(wxTimerEvent& event) {
        
        mySliderData[0] = slider01.value;
        mySliderData[1] = slider02.value;

        std::cout << "Data published: " << mySliderData[0] << "     " << mySliderData[1] << std::endl;

        node_->publishFloat(slider01.value); // Publish the float value
        rclcpp::spin_some(node_); // Process any callbacks once
    }
};

class MyApp : public wxApp {
public:
    virtual bool OnInit() {
        MyFrame* frame = new MyFrame("Steer test interface", wxPoint(50, 50), wxSize(400, 200));
        frame->Show(true);
        return true;
    }
};

// Custom main function
int main(int argc, char **argv) {

    rclcpp::init(argc, argv); //Start ros node

    MyApp myApp;
    wxApp::SetInstance(&myApp);
    wxEntryStart(argc, argv);

    

    //execute application
    wxTheApp->OnInit();
    wxTheApp->OnRun();
    wxTheApp->OnExit();

    rclcpp::shutdown();
    wxEntryCleanup();

    return 0;
}