
# CMakeLists files in this project can
# refer to the root source directory of the project as ${HELLO_SOURCE_DIR} and
# to the root binary directory of the project as ${HELLO_BINARY_DIR}.
cmake_minimum_required (VERSION 3.0.2)
project (SMV2)

# if (POLICY CMP0074)
  # cmake_policy(SET CMP0074 NEW)
# endif (POLICY CMP0074)

# Use this snippet *after* PROJECT(xxx):
if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
  set(CMAKE_INSTALL_PREFIX "${CMAKE_CURRENT_SOURCE_DIR}/install/"
       CACHE PATH "Install path prefix, prepended onto install directories." FORCE)
endif(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)



set(TARGET_NAME simplemotionv2)
set(TEST_NAME "${TARGET_NAME}_test")

set(INCLUDE_BUILT_IN_DRIVERS true CACHE BOOL "Include built-in drivers")
set(INCLUDE_BUILT_IN_DRIVERS true)

set(SUPPORT_FTDI_D2XX_DRIVER true CACHE BOOL "Add support for FTDI_D2XX driver")
set(SUPPORT_FTDI_D2XX_DRIVER true)


set(SRC_FILES 
	sm_consts.c 
	simplemotion.c 
	busdevice.c 
    bufferedmotion.c 
	devicedeployment.c
    simplemotion_private.h
    busdevice.h  
	simplemotion.h 
	sm485.h 
	simplemotion_defs.h 
    bufferedmotion.h 
	devicedeployment.h
)

if (${INCLUDE_BUILT_IN_DRIVERS}) 
	set(SRC_FILES
	${SRC_FILES}
    drivers/serial/pcserialport.c 
	drivers/tcpip/tcpclient.c
    drivers/serial/pcserialport.h
	drivers/tcpip/tcpclient.h
    )
	
	add_definitions(
	-DENABLE_BUILT_IN_DRIVERS
	)
	set(LIBS ${LIBS} ws2_32)


    #If FTDI D2XX support is enabled
    if(${SUPPORT_FTDI_D2XX_DRIVER})  
		set(SRC_FILES
		${SRC_FILES}
		drivers/ftdi_d2xx/sm_d2xx.c
		)
		
		link_directories(drivers/ftdi_d2xx/third_party/win_64bit)
		set(LIBS ${LIBS} ftd2xx)
		
    add_definitions(
	-DFTDI_D2XX_SUPPORT
	)
    endif()
endif()

include_directories(${CMAKE_CURRENT_SOURCE_DIR})
add_definitions(-D_CRT_SECURE_NO_WARNINGS)
add_library(${TARGET_NAME} STATIC ${SRC_FILES})


add_executable(${TEST_NAME} main.cpp)
target_link_libraries(${TEST_NAME}	
		${LIBS} ${TARGET_NAME} 
)


install(TARGETS ${TARGET_NAME} 
			ARCHIVE DESTINATION lib
)
install(TARGETS ${TEST_NAME} 
			RUNTIME DESTINATION bin
)
