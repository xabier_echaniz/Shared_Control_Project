# INTRODUCTION
# ============
This is a docker to work with the Tecnalia Simulator. It includes the minimun packages and programs to work with it.

# CREATE DOCKER IMAGE AND LAUNCH
# ==============================
 1. Navigate to your work directory (it must contain dockerfile and dockercompose)

 2. Execute this command line:

        simulator=${PWD##*/} docker compose up

    It will create a volume with your current working directory (PWD) in a folder named as your current folder.
    If you want to change the volume path or similar, modify the docker compose.

3. Open terminal in your container.

4. Install carla_msgs package:

        mkdir -p ~/carla-ros-bridge && cd ~/carla-ros-bridge
        git clone --recurse-submodules https://github.com/carla-simulator/ros-bridge.git src/ros-bridge
        source /opt/ros/humble/setup.bash
        rosdep update
        rosdep install --from-paths src --ignore-src -r
        colcon build
        source ./install/setup.bash
        colcon build --packages-up-to carla_msgs
    
    It is possible to have an error with "#include <tf2_eigen/tf2_eigen.h>", but don't worry about it.

5. Enjoy the docker :)



=======================================
NOTAS drivers volante:
- Me he descargado granity para linux de aquí: https://granitedevices.com/wiki/Granity#Download
- Le he cambiado el nombre de "Granity 1.15.1" a "Granity_1_15_1" para evitar problemas de espacios
- Dentro de esta carpeta, para abrir el programa: 
    chmod +x GranityLinux64
    sudo ./GranityLinux64

=======================================
NOTAS ejecutar sin sudo:
- Ejecutar:
    sudo usermod -aG dialout simulator_user
- Reiniciar el docker


====================================
Notas no me encontraba la libreria de carla_msgs:
- he ejecutado estos comandos para añadir al path de librerias la localización del paquete:

    export LD_LIBRARY_PATH=/home/simulator_user/carla-ros-bridge/build/carla_msgs:$LD_LIBRARY_PATH
    export LD_LIBRARY_PATH=/home/simulator_user/carla-ros-bridge/install/carla_msgs/lib:$LD_LIBRARY_PATH

======================================
NOTA: No olvidar el ROS_DOMAIN_ID



