/* ======================================



==========================================*/

#include <memory> 
#include <chrono>
#include <iostream>
#include <SDL.h>
#include "simplemotion.h"
#include <csignal>
#include <cstdlib> // For system function

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float32.hpp"

using namespace std;
using std::placeholders::_1;

const char* clearCommand = "clear";

#define GAIN_DAMPING    970.87
#define GAIN_FRICTION   210
#define GAIN_TORQUE     517

//=======================================
//============ CONFIGURATION ============
//=======================================

#define LOOP_TIME_MS        5      /* Execution period (ms) */
#define MAX_STEER_ANGLE     540     /* Limit of angle for each direction (º)*/
#define SET_LIMIT_TORQUE    1       /* Applies a torque when steer angle pass max values (1 = active | 0 = not active)*/
#define INIT_DAMP           0       /* Set the init damp value */
#define INIT_FRICTION       0       /* Set the init friction value */
#define DISPLAY_DATA        1       /* Print in terminal steer and pedal values */

//=======================================

class SimulatorNode : public rclcpp::Node{
public:
    SimulatorNode()
        : Node("simulator_node")
    {
        t_subscription_ = this->create_subscription<std_msgs::msg::Float32>(
            "sim_manager/steer_torque", 1, std::bind(&SimulatorNode::steer_torque_callback, this, _1));
        f_subscription_ = this->create_subscription<std_msgs::msg::Float32>(
            "sim_manager/steer_friction", 1, std::bind(&SimulatorNode::steer_friction_callback, this, _1));
        d_subscription_ = this->create_subscription<std_msgs::msg::Float32>(
            "sim_manager/steer_damp", 1, std::bind(&SimulatorNode::steer_damp_callback, this, _1));

        a_publisher_ = this->create_publisher<std_msgs::msg::Float32>("simulator/steer_angle", 1);
        w_publisher_ = this->create_publisher<std_msgs::msg::Float32>("simulator/steer_angular_velocity", 1);
        t_publisher_ = this->create_publisher<std_msgs::msg::Float32>("simulator/throttle", 1);
        b_publisher_ = this->create_publisher<std_msgs::msg::Float32>("simulator/brake", 1);
        d_publisher_ = this->create_publisher<std_msgs::msg::Float32>("simulator/steer_driver_torque", 1);
    }

    void steer_torque_callback(const std_msgs::msg::Float32::SharedPtr msg){ steer_torque_cmd  = msg->data; }
    void steer_friction_callback(const std_msgs::msg::Float32::SharedPtr msg){ steer_friction_cmd  = msg->data; }
    void steer_damp_callback(const std_msgs::msg::Float32::SharedPtr msg){ steer_damp_cmd = msg->data; }
    
    float get_steer_torque_cmd(){return steer_torque_cmd;}
    float get_steer_friction_cmd(){return steer_friction_cmd;}
    float get_steer_damp_cmd(){return steer_damp_cmd;} 
    
    void publishData(float steer_angle, float steer_angular_velocity, float throttle, float brake, float steer_driver_torque)
    {
        std_msgs::msg::Float32 a_msg;
        std_msgs::msg::Float32 w_msg;
        std_msgs::msg::Float32 t_msg;
        std_msgs::msg::Float32 b_msg;
        std_msgs::msg::Float32 d_msg;

        a_msg.data = steer_angle;
        w_msg.data = steer_angular_velocity;
        t_msg.data = throttle;
        b_msg.data = brake;
        d_msg.data = steer_driver_torque;
        
        a_publisher_->publish(a_msg);
        w_publisher_->publish(w_msg);
        t_publisher_->publish(t_msg);
        b_publisher_->publish(b_msg);
        d_publisher_->publish(d_msg);
    }

private:

    float steer_torque_cmd = 0.0;
    float steer_friction_cmd = INIT_FRICTION; 
    float steer_damp_cmd = INIT_DAMP;  

    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr t_subscription_;
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr f_subscription_;
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr d_subscription_;

    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr a_publisher_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr w_publisher_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr t_publisher_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr b_publisher_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr d_publisher_;
};

float calculate_angular_velocity(float steer_angle, float prev_steer_angle){
    float dt = LOOP_TIME_MS / 1000.0;
    return (3.1416/180.0)*(steer_angle - prev_steer_angle)/dt;
}

int main(int argc, char *argv[]){

    cout << "\n ================== INITIALIZING SIMULATOR =================== \n" << endl;

    // Set loop time
    constexpr auto loop_time = std::chrono::milliseconds(LOOP_TIME_MS);

    // Create ROS node
    rclcpp::init(argc, argv);
    auto simulator_node = std::make_shared<SimulatorNode>();

    float steer_angle = 0.0;                /* Steer angle in grades (º)*/
    float steer_angle_norm = 0.0;           /* Steer angle normalized between [-1 1]*/
    float steer_angular_velocity = 0.0;     /* Steer angular velocity (rad/s)*/
    float steer_driver_torque = 0.0;        /* Torque applied by the driver */
    float throttle = 0.0;                   /* Throttle value between [0 1]*/
    float brake = 0.0;                      /* Brake value between [0 1]*/
    float steer_limiter_torque = 0.0;       /* Torque applied whe the steer pass limit values*/
    float steer_limiter_damp = 0.0;
    float steer_limiter_friction = 0.0;
    float steer_torque = 0.0;               /* Applied torque value */
    float steer_damp = 0.0;                 /* Applied damp value */
    float steer_friction = 0.0;             /* Applied friction value */
    float prev_steer_angle = 0.0;
    float steer_angular_velocity_2 = 0.0;
    float steer_angular_velocity_array[3];
    float prev_steer_angular_velocity;
    float max_steer_angular_velocity;
    float min_steer_angular_velocity;

    float steer_torque_cmd = 0.0;
    float steer_friction_cmd = 0.0;
    float steer_damp_cmd = 0.0;

    bool init_flag = true;

    int min_brake = -32220;
    int max_brake = -21000;
    int max_accel = 32767;
    int min_accel = -32768;

    double brake_t = 0;
    double accel_t = 0;


    // Steer and pedal setting (NO MODIFY IT PLEASE)
    smbus	busHandle = 0;
    smint32 nul;
    smint32 str_position32, str_velocity32, str_torque32;
    busHandle = smOpenBus("/dev/ttyUSB0");

    int init_error;
    int id = 0; // Set your joystick ID here

    init_error = SDL_Init(SDL_INIT_JOYSTICK);
    if (init_error < 0) {
        std::cout << "Initialization error" << std::endl;
        return 1;
    }

    SDL_Joystick *joystick;
    SDL_JoystickEventState(SDL_ENABLE);
    joystick = SDL_JoystickOpen(id);
    SDL_Event event;

    // MAIN LOOP
    while(rclcpp::ok()){
        
        // Initialize loop clock
        auto init_loop_time = std::chrono::high_resolution_clock::now();

        // Set torque value
        smAppendSMCommandToQueue(busHandle, SM_SET_WRITE_ADDRESS, SMP_ABSOLUTE_SETPOINT);
	    smAppendSMCommandToQueue(busHandle, SM_WRITE_VALUE_32B, steer_torque);

        // Set friction value
        smAppendSMCommandToQueue(busHandle, SM_SET_WRITE_ADDRESS, SMP_TORQUE_EFFECT_FRICTION);
		smAppendSMCommandToQueue(busHandle, SM_WRITE_VALUE_32B, steer_friction);

        // Set damp value
		smAppendSMCommandToQueue(busHandle, SM_SET_WRITE_ADDRESS, SMP_TORQUE_EFFECT_DAMPING);
		smAppendSMCommandToQueue(busHandle, SM_WRITE_VALUE_32B, steer_damp);

        smAppendSMCommandToQueue(busHandle, SM_SET_WRITE_ADDRESS, SMP_RETURN_PARAM_ADDR); 

        smAppendSMCommandToQueue(busHandle, SM_RETURN_VALUE_32B, SMP_ACTUAL_POSITION_FB);
        smAppendSMCommandToQueue(busHandle, SM_RETURN_VALUE_32B, SMP_ACTUAL_VELOCITY_FB);
        smAppendSMCommandToQueue(busHandle, SM_RETURN_VALUE_32B, SMP_ACTUAL_TORQUE);

        smExecuteCommandQueue(busHandle, 1);

        smGetQueuedSMCommandReturnValue(busHandle, &nul);
	    smGetQueuedSMCommandReturnValue(busHandle, &nul);

        smGetQueuedSMCommandReturnValue(busHandle, &nul);
	    smGetQueuedSMCommandReturnValue(busHandle, &nul);

        smGetQueuedSMCommandReturnValue(busHandle, &nul);
	    smGetQueuedSMCommandReturnValue(busHandle, &nul);

        smGetQueuedSMCommandReturnValue(busHandle, &nul);

        smGetQueuedSMCommandReturnValue(busHandle, &(str_position32));
        smGetQueuedSMCommandReturnValue(busHandle, &(str_velocity32));
        smGetQueuedSMCommandReturnValue(busHandle, &(str_torque32));

// ==================================================================
// ============================ STEERING ============================
// ==================================================================

        // Read and transform steer data
        steer_angle = static_cast<float>(str_position32);
        steer_angle = 360*steer_angle/40000;
        steer_angular_velocity = calculate_angular_velocity(steer_angle, prev_steer_angle); //static_cast<float>(str_velocity32);

        // for(int i = 2; i > 0; i--) steer_angular_velocity_array[i] = steer_angular_velocity_array[i-1];
        // steer_angular_velocity_array[0] = steer_angular_velocity;
        // steer_angular_velocity = (steer_angular_velocity_array[0]+steer_angular_velocity_array[1]+steer_angular_velocity_array[2])/3; // Mean of last values
        
        // max_steer_angular_velocity = 2.0*(LOOP_TIME_MS/1000) + prev_steer_angular_velocity;
        // min_steer_angular_velocity = -2.0*(LOOP_TIME_MS/1000) + prev_steer_angular_velocity;

        // if (steer_angular_velocity > max_steer_angular_velocity) steer_angular_velocity = max_steer_angular_velocity;
        // if (steer_angular_velocity < min_steer_angular_velocity) steer_angular_velocity = min_steer_angular_velocity;

        steer_angular_velocity_2 = static_cast<float>(str_velocity32);
        steer_driver_torque = static_cast<float>(str_torque32);

        steer_angle_norm = steer_angle/(MAX_STEER_ANGLE);
        if (steer_angle_norm > 1.0) steer_angle_norm = 1.0;
        if (steer_angle_norm < -1.0) steer_angle_norm =-1.0;

        // When initialize, the steer wheel moves automatically to the center position (aprox)
        if (init_flag){
            
            if(steer_angle > 5){
                cout << "\rMoving steering to 0º position --> " << steer_angle << flush;
                steer_torque = -4000;
                steer_damp = 1000;
                steer_friction = 600;
            }else if(steer_angle < -5){
                cout << "\rMoving steering to 0º position --> " << steer_angle << flush;
                steer_torque = 4000;
                steer_damp = 1000;
                steer_friction = 600;
            }else{
                if (steer_angular_velocity != 0){
                    cout << "\rWaiting to 0 rad/s velocity --> " << steer_angular_velocity << flush;
                    steer_torque = 0;
                    steer_damp = 1700;
                    steer_friction = 500;
                } else{
                    init_flag = false;
                    steer_torque = 0.0;
                    steer_damp = INIT_DAMP;
                    steer_friction = INIT_FRICTION;
                    cout << "\n\n ====================== SIMULATOR READY ====================== \n" << endl;
                }
            }
        }else{
            if(SET_LIMIT_TORQUE){
                // Apply a steer torque to limit max and min angle values
                if (steer_angle >= MAX_STEER_ANGLE-20){
                    steer_limiter_torque = -(steer_angle-(MAX_STEER_ANGLE-20))*100;
                    steer_limiter_damp = (steer_angle-(MAX_STEER_ANGLE-20))*25;
                    steer_limiter_friction = (steer_angle-(MAX_STEER_ANGLE-20))*25;
                    }
                else if (steer_angle <= -(MAX_STEER_ANGLE-20)) {
                    steer_limiter_torque = -(steer_angle+(MAX_STEER_ANGLE-20))*100;
                    steer_limiter_damp = -(steer_angle+(MAX_STEER_ANGLE-20))*25;
                    steer_limiter_friction = -(steer_angle+(MAX_STEER_ANGLE-20))*25;
                }
                else steer_limiter_torque = 0;
            }

            // read ROS values
            steer_torque_cmd = simulator_node->get_steer_torque_cmd();
            steer_friction_cmd = simulator_node->get_steer_friction_cmd();
            steer_damp_cmd = simulator_node->get_steer_damp_cmd();

            // Calculate final values 
            steer_torque = steer_torque_cmd*GAIN_TORQUE + steer_limiter_torque;
            steer_damp = steer_damp_cmd*GAIN_DAMPING + steer_limiter_damp;
            steer_friction = steer_friction_cmd*GAIN_FRICTION + steer_limiter_friction;
        }

// ==================================================================
// ================ ACCELERATOR AND BRAKE VALUES ====================
// ==================================================================

        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                    case SDL_KEYDOWN:
                        // Handle keyboard input
                        break;
                    case SDL_QUIT:
                        // Handle quit event
                        break;
                    case SDL_JOYAXISMOTION:
                        if (event.jaxis.axis == 1) {
                            brake_t = static_cast<double>(event.jaxis.value);
                            brake_t = (brake_t - min_brake) / static_cast<double>(max_brake - min_brake);
                            brake_t = brake_t - 0.08;
                            brake_t = std::max(std::min(1.0, brake_t), 0.0);             
                            brake = static_cast<double>(brake_t);
                            
                        } else if (event.jaxis.axis == 2) {
                            accel_t = static_cast<double>(event.jaxis.value);
                            accel_t = (accel_t - min_accel) / static_cast<double>(max_accel - min_accel);
                            accel_t = std::max(std::min(1.0, accel_t), 0.0);
                            accel_t = accel_t*1;
                            throttle = static_cast<double>(accel_t);
                            throttle = (throttle-0.09)*(1/0.225);
                        }
                        break;
                }
        }

        // send by ROS the steer and pedal values
        simulator_node->publishData(steer_angle_norm*MAX_STEER_ANGLE*3.1416/180,steer_angular_velocity,throttle,brake,steer_angular_velocity_2); //steer_driver_torque);
        rclcpp::spin_some(simulator_node); // Process any callbacks once

        prev_steer_angle = steer_angle;
        prev_steer_angular_velocity = steer_angular_velocity;

        // Calculate execution time
		auto end_exec_time = std::chrono::high_resolution_clock::now();
        auto exec_time = std::chrono::duration_cast<std::chrono::microseconds>(end_exec_time - init_loop_time);

        // Sleep until the next desired wake-up time
        auto next_init_time = init_loop_time + loop_time;
        std::this_thread::sleep_until(next_init_time);

        // Calculate loop time
        auto end_loop_time = std::chrono::high_resolution_clock::now();
        auto elapsed_loop_time = std::chrono::duration_cast<std::chrono::microseconds>(end_loop_time - init_loop_time);

        if (DISPLAY_DATA && init_flag == false){
        // Clear the terminal
        system(clearCommand);

        cout << "\n =========================\n ======= SIMULATOR =======\n =========================" << endl;

        cout << fixed << setprecision(3) 
        << "\n      SENSOR VALUES"
        << "\n--------------------------"
        << "\n Angle [º]:     " << steer_angle
        << "\n Angle norm[%]: " << steer_angle_norm 
        << "\n Throttle [%]:  " << throttle 
        << "\n Brake [%]:     " << brake 
        << "\n Driver Torque: " << steer_driver_torque
        << endl;
        cout << fixed << setprecision(3) 
        << "\n      COMMAND VALUES"
        << "\n--------------------------"
        << "\n Torque [Nm]:   " << steer_torque/GAIN_TORQUE
        << "\n Friction:      " << steer_friction/GAIN_FRICTION
        << "\n Damping:       " << steer_damp/GAIN_DAMPING
        << endl;
        cout << fixed << setprecision(3) 
        << "\n        TIME VALUES"
        << "\n--------------------------"
        << "\n Loop time [ms]:   " << elapsed_loop_time.count()/1000.0
        << "\n Exec time [ms]:   " << exec_time.count()/1000.0
        << endl;
        cout << "\n =========================" <<endl;
        }

    }

    cout << "\n ==== ABORT EXECUTION ==== \n" << endl;

    rclcpp::shutdown();

        // Set torque value to 0
        smAppendSMCommandToQueue(busHandle, SM_SET_WRITE_ADDRESS, SMP_ABSOLUTE_SETPOINT);
	    smAppendSMCommandToQueue(busHandle, SM_WRITE_VALUE_32B, 0);

        // Set friction value to 0
        smAppendSMCommandToQueue(busHandle, SM_SET_WRITE_ADDRESS, SMP_TORQUE_EFFECT_FRICTION);
		smAppendSMCommandToQueue(busHandle, SM_WRITE_VALUE_32B, 0);

        // Set damp value to 0
		smAppendSMCommandToQueue(busHandle, SM_SET_WRITE_ADDRESS, SMP_TORQUE_EFFECT_DAMPING);
		smAppendSMCommandToQueue(busHandle, SM_WRITE_VALUE_32B, 0);

        smExecuteCommandQueue(busHandle, 1);

        smGetQueuedSMCommandReturnValue(busHandle, &nul);
	    smGetQueuedSMCommandReturnValue(busHandle, &nul);

        smGetQueuedSMCommandReturnValue(busHandle, &nul);
	    smGetQueuedSMCommandReturnValue(busHandle, &nul);

        smGetQueuedSMCommandReturnValue(busHandle, &nul);
	    smGetQueuedSMCommandReturnValue(busHandle, &nul);

    SDL_Quit();

}