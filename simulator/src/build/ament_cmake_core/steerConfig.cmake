# generated from ament/cmake/core/templates/nameConfig.cmake.in

# prevent multiple inclusion
if(_steer_CONFIG_INCLUDED)
  # ensure to keep the found flag the same
  if(NOT DEFINED steer_FOUND)
    # explicitly set it to FALSE, otherwise CMake will set it to TRUE
    set(steer_FOUND FALSE)
  elseif(NOT steer_FOUND)
    # use separate condition to avoid uninitialized variable warning
    set(steer_FOUND FALSE)
  endif()
  return()
endif()
set(_steer_CONFIG_INCLUDED TRUE)

# output package information
if(NOT steer_FIND_QUIETLY)
  message(STATUS "Found steer: 0.0.0 (${steer_DIR})")
endif()

# warn when using a deprecated package
if(NOT "" STREQUAL "")
  set(_msg "Package 'steer' is deprecated")
  # append custom deprecation text if available
  if(NOT "" STREQUAL "TRUE")
    set(_msg "${_msg} ()")
  endif()
  # optionally quiet the deprecation message
  if(NOT ${steer_DEPRECATED_QUIET})
    message(DEPRECATION "${_msg}")
  endif()
endif()

# flag package as ament-based to distinguish it after being find_package()-ed
set(steer_FOUND_AMENT_PACKAGE TRUE)

# include all config extra files
set(_extras "")
foreach(_extra ${_extras})
  include("${steer_DIR}/${_extra}")
endforeach()
