#include "shared_control_basics.hpp"

#define LOOP_TIME_MS    1      // Loop time in milliseconds
#define DISPLAY_DATA    1       // 1 --> Display data || 0 --> No display data :)

class PlannerNode : public rclcpp::Node
{
public:
    PlannerNode()
        : Node("planner_node")
    {
        // SUBSCRIBERS
        carla_waypoints_sub_ = this->create_subscription<std_msgs::msg::Float32MultiArray>(
            "hero0/waypoints", 1, std::bind(&PlannerNode::carla_waypoints_callback, this, _1));
        mpc_predictions_sub_ = this->create_subscription<std_msgs::msg::Float32MultiArray>(
            "mpc/predictions", 1, std::bind(&PlannerNode::mpc_predictions_callback, this, _1));
        gui_weights_sub_ = this->create_subscription<std_msgs::msg::Float32MultiArray>(
            "gui/weights", 1, std::bind(&PlannerNode::gui_weights_callback, this, _1));
        carla_vehicle_sub_ = this->create_subscription<std_msgs::msg::Float32MultiArray>(
            "hero0/vehicle", 1, std::bind(&PlannerNode::carla_vehicle_callback, this, _1));
        sim_manager_simulator_status_sub_ = this->create_subscription<std_msgs::msg::Float32MultiArray>(
            "sim_manager/simulator_status", 1, std::bind(&PlannerNode::sim_manager_simulator_status_callback, this, _1));

        // PUBLISHERS
        planner_predictions_pub_ = this->create_publisher<std_msgs::msg::Float32MultiArray>("planner/predictions", 1);
        planner_references_pub_ = this->create_publisher<std_msgs::msg::Float32MultiArray>("planner/references", 1);
        planner_vehicle_pub_ = this->create_publisher<std_msgs::msg::Float32MultiArray>("planner/vehicle", 1);
        planner_weights_pub_ = this->create_publisher<std_msgs::msg::Float32MultiArray>("planner/weights", 1);
        planner_online_data_pub_ = this->create_publisher<std_msgs::msg::Float32MultiArray>("planner/online_data", 1);
    }

    bool mpc_received = false;
    bool carla_received = false;

    // CALLBACKS
    void carla_waypoints_callback(const std_msgs::msg::Float32MultiArray::SharedPtr msg) 
    {
        // cout << "carla_waypoints_callback" << endl;
        int i = 0;
        for(i = 0; i < N+1; i++){
            waypoints.x[i]   = msg->data[i*3]; 
            waypoints.y[i]   = msg->data[i*3 + 1]; 
            waypoints.yaw[i] = msg->data[i*3 + 2]; 
        }
        //RCLCPP_INFO(get_logger(), "Element %zu: %f", i, i);
    }

    void mpc_predictions_callback(const std_msgs::msg::Float32MultiArray::SharedPtr msg)
    {
        // cout << "mpc_predictions_callback" << endl;
        int i = 0;
        mpc_received = true;
        for(i = 0; i<N+1; i++){
            predictions.x[i]   = msg->data[i*PREDICT_NUM]; 
            predictions.y[i]   = msg->data[i*PREDICT_NUM + 1]; 
            predictions.yaw[i] = msg->data[i*PREDICT_NUM + 2]; 
            predictions.T[i]   = msg->data[i*PREDICT_NUM + 3];
        }
    }

    void gui_weights_callback(const std_msgs::msg::Float32MultiArray::SharedPtr msg)
    {
        // cout << "gui_weights_callback" << endl;
        weights.c_ay = msg->data[0];
        weights.c_T = msg->data[1];
        weights.c_dpsi = msg->data[2];
        weights.c_th = msg->data[3];
        weights.c_vy = msg->data[4];
        weights.c_dt = msg->data[5];
        weights.w_ay = msg->data[6];
        weights.c_T = msg->data[7];
        weights.w_epsi = msg->data[8];
        weights.w_dpsi = msg->data[9];
        weights.w_ey = msg->data[10];
        weights.w_dT = msg->data[11];
        weights.Lambda = msg->data[12];
        weights.w_T = msg->data[13];
    }

    void carla_vehicle_callback(const std_msgs::msg::Float32MultiArray::SharedPtr msg)
    {
        carla_received = true;
        cout << "carla_vehicle_callback" << endl;
        vehicle.location.x = msg->data[0];
        vehicle.location.y = msg->data[1];
        vehicle.location.z = msg->data[2];

        vehicle.rotation.roll = msg->data[3];
        vehicle.rotation.pitch = msg->data[4];
        vehicle.rotation.yaw = msg->data[5];

        vehicle.velocity.x = msg->data[6];
        vehicle.velocity.y = msg->data[7];
        vehicle.velocity.z = msg->data[8];

        vehicle.acceleration.x = msg->data[9];
        vehicle.acceleration.y = msg->data[10];
        vehicle.acceleration.z = msg->data[11];

        vehicle.angular_velocity.x = msg->data[12];
        vehicle.angular_velocity.y = msg->data[13];
        vehicle.angular_velocity.z = msg->data[14];
        
        vehicle.error.y = msg->data[15];
        vehicle.error.psi = msg->data[16];
    }

    void sim_manager_simulator_status_callback(const std_msgs::msg::Float32MultiArray::SharedPtr msg)
    {
        // cout << "sim_manager_simulator_status_callback" << endl;
        vehicle.sw.theta = msg->data[0];
        vehicle.sw.w     = msg->data[1];
    }

    void publishPredictions(Predictions out_predictions)
    {
        // Publish predictions
        std_msgs::msg::Float32MultiArray predictions_msg;
        predictions_msg.data.resize((N+1)*PREDICT_NUM);
        for (int i = 0; i < N+1; i++){ 
            predictions_msg.data[i*PREDICT_NUM]     = out_predictions.x[i];
            predictions_msg.data[i*PREDICT_NUM + 1] = out_predictions.y[i];
            predictions_msg.data[i*PREDICT_NUM + 2] = out_predictions.yaw[i];
            predictions_msg.data[i*PREDICT_NUM + 3] = out_predictions.T[i];
        }
        planner_predictions_pub_->publish(predictions_msg);
    }
    
    void publishData(Vehicle out_vehicle , Weights out_weights, References out_references, OnlineData out_online_data)
    {
        // cout << "publishData" << endl;
        int i;

        // Publish references
        std_msgs::msg::Float32MultiArray references_msg;
        references_msg.data.resize((N+1)*REF_NUM);
        for (i = 0; i < N+1; i++){ 
            references_msg.data[i*REF_NUM]     = out_references.x[i];
            references_msg.data[i*REF_NUM + 1] = out_references.y[i];
            references_msg.data[i*REF_NUM + 2] = out_references.yaw[i];
        }
        planner_references_pub_->publish(references_msg);

        // Publish vehicle
        std_msgs::msg::Float32MultiArray vehicle_msg;
        vehicle_msg.data.resize(20);
        
        vehicle_msg.data[0]  = out_vehicle.location.x;
        vehicle_msg.data[1]  = out_vehicle.location.y;
        vehicle_msg.data[2]  = out_vehicle.location.z;

        vehicle_msg.data[3]  = out_vehicle.rotation.roll;
        vehicle_msg.data[4]  = out_vehicle.rotation.pitch;
        vehicle_msg.data[5]  = out_vehicle.rotation.yaw;

        vehicle_msg.data[6]  = out_vehicle.velocity.x;
        vehicle_msg.data[7]  = out_vehicle.velocity.y;
        vehicle_msg.data[8]  = out_vehicle.velocity.z;

        vehicle_msg.data[9]  = out_vehicle.acceleration.x;
        vehicle_msg.data[10] = out_vehicle.acceleration.y;
        vehicle_msg.data[11] = out_vehicle.acceleration.z;

        vehicle_msg.data[12] = out_vehicle.angular_velocity.x;
        vehicle_msg.data[13] = out_vehicle.angular_velocity.y;
        vehicle_msg.data[14] = out_vehicle.angular_velocity.z;

        vehicle_msg.data[15] = out_vehicle.error.y;
        vehicle_msg.data[16] = out_vehicle.error.psi;

        vehicle_msg.data[17] = out_vehicle.sw.theta;
        vehicle_msg.data[18] = out_vehicle.sw.w;
        vehicle_msg.data[19] = out_vehicle.sw.torque_control;

        planner_vehicle_pub_->publish(vehicle_msg);

        // Publish weights
        std_msgs::msg::Float32MultiArray weights_msg;
        weights_msg.data.resize(WEIGHT_NUM);
        weights_msg.data[0] = out_weights.c_ay;
        weights_msg.data[1] = out_weights.c_T;
        weights_msg.data[2] = out_weights.c_dpsi;
        weights_msg.data[3] = out_weights.c_th;
        weights_msg.data[4] = out_weights.c_vy;
        weights_msg.data[5] = out_weights.c_dt;
        weights_msg.data[6] = out_weights.w_ay;
        weights_msg.data[7] = out_weights.c_T;
        weights_msg.data[8] = out_weights.w_epsi;
        weights_msg.data[9] = out_weights.w_dpsi;
        weights_msg.data[10] = out_weights.w_ey;
        weights_msg.data[11] = out_weights.w_dT;
        weights_msg.data[12] = out_weights.Lambda;
        weights_msg.data[13] = out_weights.w_T;

        planner_weights_pub_->publish(weights_msg);

        // Publish online data
        std_msgs::msg::Float32MultiArray online_data_msg;
        online_data_msg.data.resize((N+1)*ONLINE_NUM);
        for (i = 0; i < N+1; i++){ 
            online_data_msg.data[i*ONLINE_NUM]     = out_online_data.lambda[i];
            online_data_msg.data[i*ONLINE_NUM + 1] = out_online_data.ax[i];
            online_data_msg.data[i*ONLINE_NUM + 2] = out_online_data.curvature[i];
        }
        planner_online_data_pub_->publish(online_data_msg);
    }

    void get_mpc_predictions(Predictions& get_predictions)
    {
        get_predictions = predictions;
    }

    void get_data(Vehicle& get_vehicle , Weights& get_weights, Waypoints& get_waypoints)
    {
        get_vehicle =  vehicle;
        get_weights = weights;
        get_waypoints = waypoints;
    }

private:

    Weights weights;
    Vehicle vehicle;
    Waypoints waypoints;
    Predictions predictions;

    rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr carla_waypoints_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr mpc_predictions_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr gui_weights_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr carla_vehicle_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr sim_manager_simulator_status_sub_;


    rclcpp::Publisher<std_msgs::msg::Float32MultiArray>::SharedPtr planner_predictions_pub_;
    rclcpp::Publisher<std_msgs::msg::Float32MultiArray>::SharedPtr planner_references_pub_;
    rclcpp::Publisher<std_msgs::msg::Float32MultiArray>::SharedPtr planner_vehicle_pub_;
    rclcpp::Publisher<std_msgs::msg::Float32MultiArray>::SharedPtr planner_weights_pub_;
    rclcpp::Publisher<std_msgs::msg::Float32MultiArray>::SharedPtr planner_online_data_pub_;
};

float cross_product(Point p1, Point p2,Point p3) {
    // Vector from p1 to p2
    float v_x = p2.x - p1.x;
    float v_y = p2.y - p1.y;

    // Vector from p2 to p3
    float w_x = p3.x - p2.x;
    float w_y = p3.y - p2.y;

    // z-component of cross product
    return v_x * w_y - v_y * w_x;
}

float calculate_distance(Point p1, Point p2)
{
    return sqrt(pow(p2.x-p1.x,2)+pow(p2.y-p1.y,2));
}

float calculate_area(Point p1, Point p2, Point p3)
{
    float base, height, area;
    float m, b;

    // Base ==> Distance between p1 and p3
    base = calculate_distance(p1, p3);

    // Height ==> Minimum distance between p2 and the line p1-p3
    m = (p3.y - p1.y)/(p3.x - p1.x);
    b = p1.y - m*p1.x;

    height = abs(m*p2.x - p2.y + b)/sqrt(m*m + 1);

    area = 0.5 * base * height;

    // cout << "p1[ " << p1.x << ", " << p1.y << " ]" << endl;
    // cout << "p2[ " << p2.x << ", " << p2.y << " ]" << endl;
    // cout << "p3[ " << p3.x << ", " << p3.y << " ]" << endl;
    // cout << "Base: " << base << endl;
    // cout << "m: " << m << endl;
    // cout << "b: " << b << endl;
    // cout << "area: " << area << endl;

    return area;
}

float calculate_curvature(Point p1, Point p2, Point p3){
    
    float area;
    float d12, d23, d31;
    float curvature;
    float crossProduct;
    
    // Calculate area
    area = calculate_area(p1,p2,p3);

    // Calculate lengths
    d12 = calculate_distance(p1,p2);
    d23 = calculate_distance(p2,p3);
    d31 = calculate_distance(p3,p1);
    
    // Calculate curvature [1/Radio]
    curvature = 4*area/(d12*d23*d31);
    // cout << "Area: " << area << endl;
    // cout << "(d12*d23*d31): " << (d12*d23*d31) << endl;

    if(isnan(curvature)) curvature = 0.0;
    if(curvature > 1.0) curvature = 0.0;

    // Calculate direction of the curvature
    crossProduct = cross_product(p1,p2,p3);
    if (crossProduct < 0) curvature = -curvature;

    return curvature;
}

void compute_curvatures(Waypoints waypoints, OnlineData& online_data)
{
    Point p1, p2, p3;

    // Execute N-2 times
    for( int i = 1; i < N; i++){
        p1.x = waypoints.x[i-1];
        p1.y = waypoints.y[i-1];

        p2.x = waypoints.x[i];
        p2.y = waypoints.y[i];

        p3.x = waypoints.x[i+1];
        p3.y = waypoints.y[i+1];

        online_data.curvature[i] = calculate_curvature(p1, p2, p3);

        // cout << fixed << setprecision(3)
        //      << i << ":\t"
        //      << "p1[" << p1.x << ", " << p1.y << "]  " 
        //      << "p2[" << p2.x << ", " << p2.y << "]  " 
        //      << "p3[" << p3.x << ", " << p3.y << "]  "
        //      << "Kurv: " << online_data.curvature[i] << endl;
    
    }
    // First element = second element
    online_data.curvature[0] = online_data.curvature[1];
    // Last element == penultimate element
    online_data.curvature[N] = online_data.curvature[N-1];
}

void compute_references(Waypoints waypoints, Vehicle vehicle, References& references){
    int i;
}

// Returns 1 or -1 if the error is positive or negative
float error_sign(Point loc, Point ref, Point next_ref) {

    vector<float> a = {next_ref.x - ref.x, next_ref.y - ref.y, 0.0};
    vector<float> b = {loc.x - ref.x, loc.y - ref.y, 0.0};
    float result;

    result = a[0] * b[1] - a[1] * b[0];
    if(result > 0) result =  1;
    if(result < 0) result = -1;

    return result;
}

void calculate_actual_error(Waypoints waypoints, Vehicle& vehicle, Vehicle prev_vehicle)
{
    // Lateral error [ey]
    Point p1, p2, p3;
    float ey;

    p1.x = waypoints.x[0];
    p1.y = waypoints.y[0];

    p2.x = prev_vehicle.location.x;
    p2.y = prev_vehicle.location.y;

    p3.x = waypoints.x[1];
    p3.y = waypoints.y[1];

    // std::cout << "p1.x: " << p1.x << ", p1.y: " << p1.y << std::endl;
    // std::cout << "p2.x: " << p2.x << ", p2.y: " << p2.y << std::endl;
    // std::cout << "p3.x: " << p3.x << ", p3.y: " << p3.y << std::endl;

    ey = -calculate_distance(p1,p2)*error_sign(p2,p1,p3);
    vehicle.error.y = ey;

    // cout << "ey: " << ey << std::endl;

    // Heading error [epsi]
    vehicle.error.psi = -(vehicle.rotation.yaw - waypoints.yaw[0]);
    if(vehicle.error.psi > PI){
        vehicle.error.psi = -(2*PI - vehicle.error.psi);
    }
    if(vehicle.error.psi < -PI){
        vehicle.error.psi = 2*PI + vehicle.error.psi;
    }
    // cout << "vehicle rotation" << vehicle.rotation.yaw << endl;
    // cout << "reference rotation" << waypoints.yaw[0] << endl;

    // std::cout << "vehicle.error.psi: " << vehicle.error.psi << std::endl;
}

void calculate_differential_variables(Vehicle& vehicle, Vehicle prev_vehicle)
{
    float dt = LOOP_TIME_MS / 1000.0;
    float delta_theta, delta_vel;

    delta_theta = vehicle.sw.theta - prev_vehicle.sw.theta;
    delta_vel = vehicle.velocity.x - prev_vehicle.velocity.x;

    vehicle.sw.w = delta_theta/dt;
    vehicle.acceleration.x = delta_vel/dt;
}

void calculate_references(References& references, Waypoints waypoints, Vehicle vehicle){
    float epsi;

    // cout << " ===================== " << endl;
    // cout << " Vehicle: " << vehicle.rotation.yaw << endl;
    for (int i = 0; i < N+1; i++){

            references.x[i]   = waypoints.x[i];
            references.y[i]   = waypoints.y[i];

            epsi = waypoints.yaw[i] - vehicle.rotation.yaw;
            epsi = atan2(sin(epsi),cos(epsi));
            
            references.yaw[i] = vehicle.rotation.yaw + epsi;
            // cout << fixed << setprecision(2) << i << ": " << references.yaw[i]<< "  ";
            // references.yaw[i] = waypoints.yaw[i];
            // if(references.yaw[i] < -PI) references.yaw[i] = references.yaw[i] + 2*PI;
            // if(references.yaw[i] > PI) references.yaw[i] = references.yaw[i] - 2*PI;
        }
    // cout << " " << endl;
}

int main(int argc, char *argv[]){

    Weights weights;
    Vehicle vehicle, prev_vehicle, prev_vehicle_value;
    Waypoints waypoints;
    References references;
    OnlineData online_data;
    Predictions predictions;
    Point p1, p2, p3;

    int cont_wait_mpc = 0;

    // Set loop time
    constexpr auto loop_time = std::chrono::milliseconds(LOOP_TIME_MS);

    // Create ROS node
    rclcpp::init(argc, argv);
    auto planner_node = std::make_shared<PlannerNode>();

    // Initialize loop clock
    auto init_loop_time = std::chrono::high_resolution_clock::now();
    auto prev_init_loop_time = std::chrono::high_resolution_clock::now();

    // MAIN LOOP
    while(rclcpp::ok()){

        // 1. Wait to MPC predictions
        // ==========================
        cout << "Waiting to MPC predictions..." << endl;
        while(planner_node -> mpc_received == false){
            rclcpp::spin_some(planner_node); // Process any callbacks
        }
        planner_node -> mpc_received = false; // Reset MPC flag
        
        prev_init_loop_time = init_loop_time;
        init_loop_time = std::chrono::high_resolution_clock::now();

        // 2. Get predictions from MPC
        // ===========================
        planner_node -> get_mpc_predictions(predictions);

        // 3. Send predictions to CARLA
        // ============================
        planner_node -> publishPredictions(predictions);

        auto prediction_send_time = std::chrono::high_resolution_clock::now();

        // 4. Wait to carla data
        // =====================
        cout << "Waiting to CARLA data..." << endl;
        while(planner_node -> carla_received == false){
            rclcpp::spin_some(planner_node);

        }
        planner_node -> carla_received = false; // Reset CARLA flag

        auto carla_received_time = std::chrono::high_resolution_clock::now();

        // 5. Get waypoints, status and weights
        // ====================================
        planner_node -> get_data(vehicle, weights, waypoints);

        // 6. Calculate online data
        // ========================
        compute_curvatures(waypoints, online_data);

        for (int i = 0; i < N+1; i++){
            online_data.lambda[i]    = 2.2*max(3.0,weights.Lambda)-5.6;
            online_data.ax[i]        = vehicle.acceleration.x;
        }

        // 7. Calculate references
        // =======================
        calculate_references(references, waypoints, vehicle);

        // 8. Set next torque value for MPC status
        // =======================================
        vehicle.sw.torque_control = predictions.T[1];

        // 9. Send status, waypoints and online data to MPC
        // ================================================
        planner_node -> publishData(vehicle, weights, references, online_data);

        auto end_exec_time = std::chrono::high_resolution_clock::now();
        // Save previous vehicle state
        // if(prev_vehicle_value.location.x != vehicle.location.x) {
        //     prev_vehicle = prev_vehicle_value;   
        // }
    
        
        //calculate_differential_variables(vehicle, prev_vehicle);
        //calculate_actual_error(waypoints, vehicle, prev_vehicle);


        // float abs_ey,x,y;
        // x = references.x[0] - prev_vehicle.location.x;
        // y = references.y[0] - prev_vehicle.location.y;

        // abs_ey = sqrt(x*x + y*y);
        

        //prev_vehicle_value = vehicle;
        // Calculate sendig prediction time
        auto prediction_send_duration = std::chrono::duration_cast<std::chrono::microseconds>(prediction_send_time - init_loop_time);

        // Calculate carla data time
        auto carla_received_duration = std::chrono::duration_cast<std::chrono::microseconds>(carla_received_time - init_loop_time);

        // Calculate execution time
        auto exec_duration = std::chrono::duration_cast<std::chrono::microseconds>(end_exec_time - init_loop_time);

        // Calculate loop time
        auto loop_duration = std::chrono::duration_cast<std::chrono::microseconds>(init_loop_time - prev_init_loop_time);

        

        // Sleep until the next desired wake-up time
        // auto next_init_time = init_loop_time + loop_time;
        //std::this_thread::sleep_until(next_init_time);

        
        if(DISPLAY_DATA){
            // Clear the terminal
            system(clearCommand);
            // cout <<   "current x: " << vehicle.location.x 
            //      << "\n   prev x: " << prev_vehicle.location.x 
            //      << "\n   diff x: " << vehicle.location.x - prev_vehicle.location.x << endl;

            cout << "\n =============================\n ========== PLANNER ==========\n =============================" << endl;

            cout << fixed << setprecision(3) 
            << "\n        PREDICTIONS"
            << "\n---------------------------"
            << "\n Torque [0]:     " << predictions.T[0]
            << "\n Curvature[0]:   " << online_data.curvature[0]
            << "\n Curvature[end]: " << online_data.curvature[N]
            << endl;

            cout << "\n      VEHICLE DATA"
            << "\n---------------------------\n"
            << vehicle
            // << "Abs error: " << abs_ey
            << endl;

            cout << fixed << setprecision(3) 
            << "\n      TIME VALUES"
            << "\n---------------------------"
            << "\n pred      [ms]:  " << prediction_send_duration.count()/1000.0
            << "\n carla     [ms]:  " << carla_received_duration.count()/1000.0
            << "\n Exec time [ms]:  " << exec_duration.count()/1000.0
            << "\n Loop time [ms]:  " << loop_duration.count()/1000.0
            << endl;

            cout << "=============================" << endl;
        }

        // To get road points and print
        // ----------------------------
        // if(cont%50 == 0)
        //     cout << waypoints.x[0] << ",\t" << waypoints.y[0] << ",\t" << waypoints.yaw[0] << "," << endl;

        // cont ++;
        
    }

    rclcpp::shutdown();
}