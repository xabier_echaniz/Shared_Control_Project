#include <iostream>
#include <vector>

// Function to calculate the cross product of two vectors
std::vector<double> crossProduct(const std::vector<double>& v1, const std::vector<double>& v2) {
    if (v1.size() != 3 || v2.size() != 3) {
        std::cerr << "Vectors must be three-dimensional." << std::endl;
        return {}; // or throw an exception
    }

    std::vector<double> result(3);
    result[0] = v1[1] * v2[2] - v1[2] * v2[1];
    result[1] = v1[2] * v2[0] - v1[0] * v2[2];
    result[2] = v1[0] * v2[1] - v1[1] * v2[0];
    return result;
}

int main() {
    std::vector<double> v1 = {3.0, 3.0, 0.0}; // i
    std::vector<double> v2 = {4.0, 0.0, 0.0}; // j

    std::vector<double> result = crossProduct(v1, v2);
    std::cout << "Cross product: [" << result[0] << ", " << result[1] << ", " << result[2] << "]" << std::endl;

    return 0;
}