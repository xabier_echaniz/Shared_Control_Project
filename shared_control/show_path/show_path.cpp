#include <wx/wx.h>
#include <wx/slider.h>
#include <wx/sizer.h>
#include <vector>

#include "shared_control_basics.hpp"

#define WIDTH           1200
#define HEIGHT          900
#define GLOBAL_PATH     0   // 0 --> Show local path || 1 --> show global path

float road[] = {    67.2925,	54.8467,	2.41363,
                    64.1452,	57.4349,	2.49309,
                    60.7536,	59.7964,	2.57369,
                    57.2802,	61.8253,	2.65213,
                    53.514,	    63.6385,	2.73365,
                    49.8053,	65.0735,	2.8112,
                    45.7942,	66.2686,	2.89282,
                    41.9176,	67.077,	    3.0067,
                    37.6842,	67.1401,	3.24672,
                    33.8431,	66.2949,	3.46961,
                    30.0621,	64.4831,	3.70729,
                    26.9832,	62.0051,	3.93127,
                    24.4221,	58.727,	    4.16708,
                    22.7531,	55.111,	    4.3928,
                    21.8889,	51.1983,	-1.73494,
                    21.2245,	47.084,	    -1.72053,
                    20.676,	    43.1177,	-1.6959,
                    20.2151,	39.0374,	-1.67065,
                    19.8632,	35.0236,	-1.64588,
                    19.6065,	30.9184,	-1.62058,
                    19.4551,	26.8628,	-1.59563,
                    19.4051,	22.7373,	-1.57025,
                    19.4566,	18.7319,	-1.54562,
                    19.6127,	14.6092,	-1.52025,
                    19.8133,	10.6448,	-1.52025,
                    19.9017,	6.32283,	-1.64551,
                    19.1208,	2.38609,	-1.88772,
                    17.3334,	-1.38282,	-2.13951,
                    14.7167,	-4.53781,	-2.38691,
                    11.3293,	-6.99884,	-2.63966,
                    7.63355,	-8.4811,	-2.8839,
                    3.42844,	-8.97978,	-3.16321,
                    -0.521231,	-8.36917,	-3.42674,
                    -4.26297,	-6.69694,	-3.69702,
                    -7.4251,	-4.08343,	-3.96756,
                    -9.75359,	-0.758998,	-4.23521,
                    -11.503,	2.75729,	2.03196,
                    -13.2838,	6.44016,	1.99513,
                    -14.858,	10.2409,	1.93182,
                    -16.1614,	14.0453,	1.86993,
                    -17.2526,	18.029,	    1.80636,
                    -18.0749,	21.9965,	1.744,
                    -18.6591,	26.1041,	1.68014,
                    -18.9741,	30.1146,	1.61823,
                    -19.0387,	34.2472,	1.55462,
                    -18.9979,	38.1726,	1.59785,
                    -19.3627,	42.2521,	1.72212,
                    -20.2156,	46.2025,	1.84475,
                    -21.6106,	50.1737,	1.97246,
                    -23.428,	53.8112,	2.09584,
                    -25.6969,	57.2207,	2.22011,
                    -28.3699,	60.3071,	2.35446,
                    -31.5062,	63.0195,	2.50257,
                    -34.8989,	65.1807,	2.64625,
                    -38.7281,	66.8927,	2.79608,
                    -42.6152,	67.9828,	2.94027,
                    -46.7787,	68.5117,	3.09019,
                    -50.8118,	68.5875,	3.12767,
                    -54.9273,	68.581,	    3.16344,
                    -58.9579,	68.4114,	3.20388,
                    -62.967,	68.08,	    3.24422,
                    -67.0251,	67.5779,	3.28521,
                    -71.0111,	66.9187,	3.32572,
                    -75.0872,	66.0713,	3.36746,
                    -78.9276,	65.1082,	3.40715,
                    -82.8814,	63.9445,	3.44848,
                    -86.6951,	62.7033,	-2.82598,
                    -90.6247,	61.4202,	-2.82598,
                    -94.4193,	60.1004,	-2.78466,
                    -98.2704,	58.5623,	-2.73854,
                    -101.934,	56.9026,	-2.69381,
                    -105.688,	54.9895,	-2.64695,
                    -109.156,	53.0186,	-2.60258,
                    -112.604,	50.8488,	-2.55728,
                    -115.934,	48.5374,	-2.5122,
                    -119.243,	46.008,	    -2.46587,
                    -122.292,	43.4517,	-2.42163,
                    -125.342,	40.6498,	-2.37556,
                    -128.157,	37.8182,	-2.33115,
                    -130.958,	34.7602,	-2.30464,
                    -133.674,	31.7479,	-2.30464,
                    -136.408,	28.7169,	-2.30464,
                    -139.118,	25.7123,	-2.30464,
                    -141.837,	22.6499,	-2.26309,
                    -144.25,	19.3852,	-2.15143,
                    -146.244,	15.9512,	-2.04222,
                    -147.923,	12.1313,	-1.92745,
                    -149.121,	8.27167,	-1.81629,
                    -149.906,	4.1377,	    -1.70055,
                    -150.205,	0.132327,	-1.59008,
                    -150.047,	-4.0213,	-1.47575,
                    -149.452,	-7.96257,	-1.36611,
                    -148.371,	-11.9942,	-1.2513,
                    -146.91,	-15.7107,	-1.14147,
                    -144.986,	-19.353,	-1.02817,
                    -142.735,	-22.7911,	-0.981449,
                    -140.429,	-26.2242,	-0.963075,
                    -138.023,	-29.4261,	-0.889597,
                    -135.27,	-32.5679,	-0.812962,
                    -132.481,	-35.3929,	-0.771079,
                    -129.475,	-38.1906,	-0.727947,
                    -126.401,	-40.8682,	-0.714321,
                    -123.295,	-43.561,	-0.714321,
                    -120.264,	-46.1893,	-0.714321,
                    -117.2,	    -48.8457,	-0.714321,
                    -114.062,	-51.5669,	-0.714321,
                    -110.947,	-54.2675,	-0.714321,
                    -107.957,	-56.8365,	-0.704688,
                    -104.867,	-59.4369,	-0.694458,
                    -101.681,	-62.0615,	-0.684004,
                    -98.5424,	-64.5942,	-0.673787,
                    -95.3333,	-67.1294,	-0.663428,
                    -92.1481,	-69.5929,	-0.653228,
                    -88.8505,	-72.0893,	-0.642752,
                    -85.6604,	-74.4532,	-0.632695,
                    -82.28,	    -76.9045,	-0.622118,
                    -79.0205,	-79.217,	-0.611995,
                    -75.6021,	-81.5896,	-0.601455,
                    -72.3438,	-83.8019,	-0.591479,
                    -68.8437,	-86.126,	-0.580837,
                    -65.5127,	-88.2884,	-0.570777,
                    -62.0127,	-90.5098,	-0.560277,
                    -58.5933,	-92.6307,	-0.550085,
                    -55.148,	-94.7193,	-0.539879,
                    -51.642,	-96.7958,	-0.529558,
                    -48.1588,	-98.8109,	-0.519365,
                    -44.5497,	-100.849,	-0.508866,
                    -41.0328,	-102.788,	-0.498694,
                    -37.3862,	-104.749,	-0.488206,
                    -33.8059,	-106.627,	-0.477965,
                    -30.1841,	-108.48,	-0.46766,
                    -26.5951,	-110.269,	-0.457502,
                    -22.8521,	-112.088,	-0.446961,
                    -19.2277,	-113.802,	-0.436805,
                    -15.5102,	-115.515,	-0.426438,
                    -11.8103,	-117.173,	-0.416168,
                    -8.03081,	-118.82,	-0.405725,
                    -4.3546,	-120.377,	-0.395612,
                    -0.491343,	-121.966,	-0.38503,
                    3.18766,	-123.436,	-0.374996,
                    6.92181,	-124.884,	-0.364851,
                    10.8105,	-126.346,	-0.354328,
                    14.6909,	-127.758,	-0.343868,
                    18.506,	    -129.102,	-0.333622,
                    22.2378,	-130.377,	-0.327058,
                    26.198,	    -131.721,	-0.327058,
                    29.9574,	-132.996,	-0.327058,
                    33.9169,	-134.339,	-0.327058,
                    37.6655,	-135.611,	-0.327058,
                    41.6214,	-136.953,	-0.327058,
                    45.3662,	-138.223,	-0.327058,
                    49.2761,	-139.55,	-0.327058,
                    53.0922,	-140.844,	-0.327058,
                    57.0477,	-142.186,	-0.327058,
                    60.7864,	-143.453,	-0.32025,
                    64.7628,	-144.604,	-0.243148,
                    68.7676,	-145.437,	-0.16696,
                    72.8379,	-145.964,	-0.090517,
                    76.8605,	-146.177,	-0.0154894,
                    80.8636,	-146.09,	0.0590876,
                    85.0053,	-145.683,	0.1366,
                    88.9443,	-144.992,	0.211087,
                    92.9521,	-143.97,	0.288121,
                    96.7475,	-142.689,	0.362727,
                    100.594,	-141.055,	0.440571,
                    104.122,	-139.229,	0.514563,
                    107.719,	-137.005,	0.593321,
                    110.946,	-134.649,	0.66774,
                    114.111,	-131.948,	0.745253,
                    116.935,	-129.141,	0.819412,
                    119.698,	-125.975,	0.886056,
                    122.118,	-122.817,	0.948112,
                    124.391,	-119.429,	1.01176,
                    126.446,	-115.898,	1.07548,
                    128.282,	-112.222,	1.13957,
                    129.873,	-108.452,	1.2034,
                    131.22,	    -104.59,	1.2672,
                    132.294,	-100.746,	1.32945,
                    133.14,	    -96.7712,	1.39284,
                    133.74,	    -92.6777,	1.45738,
                    134.067,	-88.721,	1.51931,
                    134.145,	-84.4947,	1.58524,
                    133.967,	-80.5559,	1.64674,
                    133.503,	-76.3179,	1.71325,
                    132.825,	-72.4437,	1.77459,
                    131.858,	-68.4075,	1.82804,
                    130.825,	-64.4823,	1.82804,
                    129.765,	-60.4505,	1.82804,
                    128.745,	-56.573,	1.82804,
                    127.701,	-52.6056,	1.82804,
                    126.684,	-48.741,	1.82804,
                    125.606,	-44.7464,	1.84369,
                    124.495,	-40.9128,	1.86232,
                    123.303,	-37.0715,	1.88109,
                    122.008,	-33.1626,	1.9003,
                    120.673,	-29.3765,	1.91904,
                    119.227,	-25.5097,	1.93831,
                    117.756,	-21.7913,	1.95697,
                    116.157,	-17.9663,	1.97632,
                    114.519,	-14.2476,	1.99528,
                    112.811,	-10.5623,	2.01424,
                    111.055,	-6.95236,	2.03297,
                    109.171,	-3.26213,	2.05231,
                    107.284,	0.269748,	2.07099,
                    105.24,	    3.92465,	2.09054,
                    103.254,	7.32105,	2.1089,
                    101.073,	10.8966,	2.12845,
                    98.9452,	14.2386,	2.14693,
                    96.6369,	17.717,	    2.16642,
                    94.3665,	21.0007,	2.18505,
                    91.9465,	24.3626,	2.20438,
                    89.5128,	27.6105,	2.22332,
                    87.0059,	30.8494,	2.23104,
                    84.5627,	33.9958,	2.23104,
                    82.0999,	37.1675,	2.23104,
                    79.5788,	40.4142,	2.23104,
                    77.1025,	43.6033,	2.23104,
                    74.5439,	46.8983,	2.23104,
                    72.0553,	49.9987,	2.28103};

class ShowPathNode : public rclcpp::Node
{
public:
    ShowPathNode()
        : Node("show_path_node")
    {
        // SUBSCRIBERS
        planner_references_sub_ = this->create_subscription<std_msgs::msg::Float32MultiArray>(
            "planner/references", 1, std::bind(&ShowPathNode::planner_references_callback, this, _1));
        planner_vehicle_sub_ = this->create_subscription<std_msgs::msg::Float32MultiArray>(
            "planner/vehicle", 1, std::bind(&ShowPathNode::planner_vehicle_callback, this, _1));
        planner_predictions_sub_ = this->create_subscription<std_msgs::msg::Float32MultiArray>(
            "planner/predictions", 1, std::bind(&ShowPathNode::planner_predictions_callback, this, _1));
    }

    // CALLBACKS
    void planner_references_callback(const std_msgs::msg::Float32MultiArray::SharedPtr msg) 
    {
        int i = 0;
        for(i = 0; i < N+1; i++){
            references.x[i]   = msg->data[i*REF_NUM]; 
            references.y[i]   = msg->data[i*REF_NUM + 1]; 
            references.yaw[i] = msg->data[i*REF_NUM + 2]; 
        }
    }

    void planner_vehicle_callback(const std_msgs::msg::Float32MultiArray::SharedPtr msg)
    {
        vehicle.location.x = msg->data[0];
        vehicle.location.y = msg->data[1];
        vehicle.location.z = msg->data[2];

        vehicle.rotation.roll = msg->data[3];
        vehicle.rotation.pitch = msg->data[4];
        vehicle.rotation.yaw = msg->data[5];

        vehicle.velocity.x = msg->data[6];
        vehicle.velocity.y = msg->data[7];
        vehicle.velocity.z = msg->data[8];

        vehicle.acceleration.x = msg->data[9];
        vehicle.acceleration.y = msg->data[10];
        vehicle.acceleration.z = msg->data[11];

        vehicle.angular_velocity.x = msg->data[12];
        vehicle.angular_velocity.y = msg->data[13];
        vehicle.angular_velocity.z = msg->data[14];

        vehicle.error.y = msg->data[15];
        vehicle.error.psi = msg->data[16];

        vehicle.sw.theta = msg->data[17];
        vehicle.sw.w = msg->data[18];
        vehicle.sw.torque_control = msg->data[19];
    }

    void planner_predictions_callback(const std_msgs::msg::Float32MultiArray::SharedPtr msg)
    {
        int i = 0;
        for(i = 0; i < N+1; i++){
            predictions.x[i]   = msg->data[i*PREDICT_NUM]; 
            predictions.y[i]   = msg->data[i*PREDICT_NUM + 1]; 
            predictions.yaw[i] = msg->data[i*PREDICT_NUM + 2]; 
            predictions.T[i]   = msg->data[i*PREDICT_NUM + 3];
        }
    }

    void get_ros_data(Vehicle& get_vehicle, References& get_references, Predictions& get_predictions)
    {
        get_references = references;
        get_vehicle = vehicle;
        get_predictions = predictions;
    }

private:

    References references;
    Vehicle vehicle;
    Predictions predictions;

    rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr planner_references_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr planner_vehicle_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr planner_predictions_sub_;
};

class MyApp : public wxApp {
public:
    virtual bool OnInit();
};

class MyFrame : public wxFrame {
public:
    MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
    : wxFrame(nullptr, wxID_ANY, title, pos, size), node_(std::make_shared<ShowPathNode>()) {
        
        // Store the node
        node_ = std::make_shared<ShowPathNode>();
        // Create a timer to continuously log slider values
        m_timer = new wxTimer(this, ID_TIMER);
        m_timer->Start(50); // Log values every 1 second
        Bind(wxEVT_TIMER, &MyFrame::OnTimer, this);
    }

private:
    enum {
        ID_TIMER = 50
    };

    std::shared_ptr<ShowPathNode> node_;

    wxTimer* m_timer;
    std::vector<wxPoint> ref_points;
    std::vector<wxPoint> predict_points;
    std::vector<wxPoint> center_points;
    std::vector<wxPoint> road_points;
    std::vector<wxPoint> vehicle_point;
    int flag = true;
    int n_road = static_cast<int>((sizeof(road)/sizeof(road[0]))/3);

    References references;
    Vehicle vehicle;
    Predictions predictions;

    int g_escale = 3.5;
    int l_escale = 20;
    int global_offset_x = WIDTH/2;
    int global_offset_y = HEIGHT/2 - 130;

    wxDECLARE_EVENT_TABLE();

    void OnPaint(wxPaintEvent& event) {
        wxPaintDC dc(this);
        int x_vehicle, y_vehicle;
        int x_direction, y_direction;
        int x_pred, y_pred;
        int x_ref, y_ref;

        dc.SetBackground(*wxWHITE_BRUSH);
        dc.Clear();

        if(GLOBAL_PATH){

            // =======================================
            // ============ GLOBAL VIEW ==============
            // =======================================

            // Draw road 
            // ==============
            dc.SetPen(*wxBLACK_PEN);
            dc.SetBrush(*wxBLACK_BRUSH);
            for (const auto& point : road_points) {
                dc.DrawCircle(point, 1); 
            }

            // Draw vehicle orientation
            // ==============================
            dc.SetBrush(*wxBLACK_BRUSH);
            dc.SetPen( wxPen( wxColor(255,0,0), 3 ) ); // Red line, 3 pixels thick

            x_vehicle = static_cast<int>(-g_escale*vehicle.location.x + global_offset_x);
            y_vehicle = static_cast<int>(-g_escale*vehicle.location.y + global_offset_y);

            x_direction = static_cast<int>(x_vehicle-15*cos(vehicle.rotation.yaw));
            y_direction = static_cast<int>(y_vehicle-15*sin(vehicle.rotation.yaw));

            dc.DrawLine( x_vehicle, y_vehicle, x_direction, y_direction); // draw line across the rectangle

            // Draw references
            // =============================
            dc.SetPen(*wxGREEN_PEN);
            dc.SetBrush(*wxGREEN_BRUSH);
            for (const auto& point : ref_points) {
                dc.DrawCircle(point, 2); // Draw point as a small circle
            }

            dc.SetBrush(*wxBLACK_BRUSH);
            dc.SetPen( wxPen( wxColor(0,255,0), 2 ) ); 

            for(int i= 0; i<N+1; i++){
                x_ref = static_cast<int>(-g_escale*references.x[i] + global_offset_x); 
                y_ref = static_cast<int>(-g_escale*references.y[i] + global_offset_y);

                x_direction = static_cast<int>(x_ref-10*cos(references.yaw[i]));
                y_direction = static_cast<int>(y_ref-10*sin(references.yaw[i]));

                dc.DrawLine( x_ref, y_ref, x_direction, y_direction); // draw line across the rectangle
            }

            // Draw predictions
            // =============================
            dc.SetPen(*wxBLUE_PEN);
            dc.SetBrush(*wxBLUE_BRUSH);
            for (const auto& point : predict_points) {
                dc.DrawCircle(point, 2); 
            }

            dc.SetBrush(*wxBLACK_BRUSH);
            dc.SetPen( wxPen( wxColor(0,0,255), 2 ) ); 

            for(int i= 0; i<N+1; i++){
                x_pred = static_cast<int>(-g_escale*predictions.x[i] + global_offset_x); 
                y_pred = static_cast<int>(-g_escale*predictions.y[i] + global_offset_y);

                x_direction = static_cast<int>(x_pred-10*cos(predictions.yaw[i]));
                y_direction = static_cast<int>(y_pred-10*sin(predictions.yaw[i]));

                dc.DrawLine( x_pred, y_pred, x_direction, y_direction); // draw line across the rectangle
            }

            // Draw vehicle
            // ============================
            dc.SetBrush(*wxRED_BRUSH); // red filling
            dc.SetPen( wxPen( wxColor(255,0,0), 5 ) ); // 5-pixels-thick red outline
            for (const auto& point : vehicle_point) {
                dc.DrawCircle(point, 6); // Draw point as a small circle
            }

            // Write text
            // ============================
            dc.SetBrush(*wxBLACK_BRUSH);
            dc.SetPen( wxPen( wxColor(0,0,255), 2 ) );
            dc.DrawText(wxT("GLOBAL VIEW"), 30, 40);

        }else{

            // =======================================
            // ============= LOCAL VIEW ==============
            // =======================================
            
            // Draw center line
            // ============================
            // dc.SetPen(*wxBLACK_PEN);
            // dc.SetBrush(*wxBLACK_BRUSH);
            // dc.SetPen( wxPen( wxColor(0,0,0), 3 ) ); // black line, 3 pixels thick
            // dc.DrawLine( WIDTH/2, 0, WIDTH/2, HEIGHT ); // draw line across the rectangle
            // for (const auto& point : center_points) {
            //     dc.DrawCircle(point, 1); // Draw point as a small circle
            // }

            // Draw vehicle
            // ============================
            dc.SetPen( wxPen( wxColor(0,0,0), 3 ) ); // black line, 3 pixels thick
            dc.SetBrush(*wxRED_BRUSH); // red filling
            dc.DrawRectangle( (WIDTH/2 - 20), (HEIGHT - 80), 40, 60 );

            // Draw references
            // ============================
            dc.SetPen(*wxGREEN_PEN);
            dc.SetBrush(*wxGREEN_BRUSH);
            for (const auto& point : ref_points) {
                dc.DrawCircle(point, 2); // Draw point as a small circle
            }

            // Draw predictions
            // =============================
            dc.SetPen(*wxBLUE_PEN);
            dc.SetBrush(*wxBLUE_BRUSH);
            for (const auto& point : predict_points) {
                dc.DrawCircle(point, 2); 
            }

            // Write text
            // ============================
            wxFont font = dc.GetFont();
            font.MakeBold();
            font.SetPointSize(16);
            dc.SetFont(font);

            dc.SetTextForeground(*wxBLACK);
            dc.DrawText(wxT("LOCAL VIEW"), 30, 40);

            font.SetPointSize(12);
            dc.SetFont(font);
            
            dc.SetTextForeground(*wxBLUE);
            dc.DrawText(wxT("Predictions"), 30, 90);

            dc.SetTextForeground(*wxGREEN);
            dc.DrawText(wxT("References"), 30, 110);

            font.SetPointSize(14);
            dc.SetTextForeground(*wxBLACK);
            dc.DrawText(wxT("Vehicle data:"), 30, 150);

            font.SetPointSize(12);

            wxString vxStr   = wxString::Format(wxT("%.2f"), vehicle.velocity.x*3.6);
            wxString psiStr  = wxString::Format(wxT("%.2f"), vehicle.rotation.yaw*180/PI);
            wxString eyStr   = wxString::Format(wxT("%.2f"), vehicle.error.y);
            wxString epsiStr = wxString::Format(wxT("%.2f"), vehicle.error.psi);

            dc.DrawText(wxT("Velocity: ") + vxStr + wxT(" km/h"), 30, 170);
            dc.DrawText(wxT("Orientation: ") + psiStr + wxT("º"), 30, 190);
            dc.DrawText(wxT("Lateral error: ") + eyStr + wxT(" m"), 30, 210);
            dc.DrawText(wxT("Heading error: ") + epsiStr + wxT(" rad"), 30, 230);
        }
    }

    void OnTimer(wxTimerEvent& event) {
        int x,y;
        int x_pred, y_pred;
        float xp, yp;
        float beta, alpha, l;


        rclcpp::spin_some(node_); // Process any callbacks once
        node_->get_ros_data(vehicle,references,predictions); // Get reference values
        
        ref_points.clear();
        predict_points.clear();
        vehicle_point.clear();

        if(GLOBAL_PATH){
            if(flag){
                flag = false;
                for(int i = 0; i < n_road; i++){
                    x = static_cast<int>(-g_escale*road[i*3] + global_offset_x);
                    y = static_cast<int>(-g_escale*road[i*3 + 1] + global_offset_y);
                    road_points.emplace_back(x, y); 
                }
            }

            x = static_cast<int>(-g_escale*vehicle.location.x + global_offset_x);
            y = static_cast<int>(-g_escale*vehicle.location.y + global_offset_y);
            vehicle_point.emplace_back(x, y);

            for(int i = 0; i < N+1; i++){
                x = static_cast<int>(-g_escale*references.x[i] + global_offset_x);
                y = static_cast<int>(-g_escale*references.y[i] + global_offset_y);

                x_pred = static_cast<int>(-g_escale*predictions.x[i] + global_offset_x); 
                y_pred = static_cast<int>(-g_escale*predictions.y[i] + global_offset_y);

                ref_points.emplace_back(x, y);
                predict_points.emplace_back(x_pred,y_pred);
            }

        }else{
            if(flag){
                flag = false;
                for(int i = 0; i < N+1; i++){
                    x = static_cast<int>(WIDTH/2); 
                    y = static_cast<int>(HEIGHT/30*i);
                    center_points.emplace_back(x, y); 
                }
            }

            for(int i = 0; i < N+1; i++){
                xp = references.x[i] - vehicle.location.x;
                yp = references.y[i]- vehicle.location.y;

                l = sqrt(xp*xp + yp*yp);
                beta = atan(yp/(xp+0.000001));
                if(xp < 0) beta = beta + PI;
                alpha = vehicle.rotation.yaw;

                xp = l*cos(alpha-beta);
                yp = l*sin(alpha-beta);

                x = static_cast<int>(-l_escale*yp + WIDTH/2); 
                y = static_cast<int>(-l_escale*xp + HEIGHT - 30);
                ref_points.emplace_back(x, y); 

                xp = predictions.x[i] - vehicle.location.x;
                yp = predictions.y[i]- vehicle.location.y;

                l = sqrt(xp*xp + yp*yp);
                beta = atan(yp/(xp+0.000001));
                if(xp < 0) beta = beta + PI;
                alpha = vehicle.rotation.yaw;

                xp = l*cos(alpha-beta);
                yp = l*sin(alpha-beta);

                x = static_cast<int>(-l_escale*yp + WIDTH/2); 
                y = static_cast<int>(-l_escale*xp + HEIGHT - 30);
                predict_points.emplace_back(x, y); 
            }
            
        }
        //cout << "Rotation: " << vehicle.rotation.yaw << "º"<< endl;
        Refresh(); // Request the frame to repaint itself
    }
};

wxBEGIN_EVENT_TABLE(MyFrame, wxFrame)
    EVT_PAINT(MyFrame::OnPaint)
    EVT_TIMER(wxID_ANY, MyFrame::OnTimer)
wxEND_EVENT_TABLE()

bool MyApp::OnInit() {
    MyFrame *frame = new MyFrame("Point Plotter", wxPoint(50, 50), wxSize(WIDTH, HEIGHT));
    frame->Show(true);
    return true;
}

// MyFrame::MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
//     : wxFrame(NULL, wxID_ANY, title, pos, size), m_timer(this, wxID_ANY) {
//     m_timer.Start(100); // Adjust the interval as needed (in milliseconds)
// }


// wxIMPLEMENT_APP(MyApp);

// Custom main function
int main(int argc, char **argv) {

    rclcpp::init(argc, argv); //Start ros node

    MyApp myApp;
    wxApp::SetInstance(&myApp);
    wxEntryStart(argc, argv);

    //execute application
    wxTheApp->OnInit();
    wxTheApp->OnRun();
    wxTheApp->OnExit();

    rclcpp::shutdown();
    wxEntryCleanup();

    return 0;
}


