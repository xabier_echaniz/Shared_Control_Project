set(_AMENT_PACKAGE_NAME "show_path")
set(show_path_VERSION "0.0.0")
set(show_path_MAINTAINER "user <user@todo.todo>")
set(show_path_BUILD_DEPENDS "rclcpp")
set(show_path_BUILDTOOL_DEPENDS "ament_cmake")
set(show_path_BUILD_EXPORT_DEPENDS "rclcpp")
set(show_path_BUILDTOOL_EXPORT_DEPENDS )
set(show_path_EXEC_DEPENDS "rclcpp")
set(show_path_TEST_DEPENDS "ament_lint_auto" "ament_lint_common")
set(show_path_GROUP_DEPENDS )
set(show_path_MEMBER_OF_GROUPS )
set(show_path_DEPRECATED "")
set(show_path_EXPORT_TAGS)
list(APPEND show_path_EXPORT_TAGS "<build_type>ament_cmake</build_type>")
