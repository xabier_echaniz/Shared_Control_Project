import matplotlib.pyplot as plt
import numpy as np
import rclpy
import time

from rclpy.node import Node
from std_msgs.msg import Float32
from std_msgs.msg import Float32MultiArray

REF_NUM = 3

class References:
    def __init__(self):
        self.x = [0.0]*31
        self.y = [0.0]*31
        self.yaw = [0.0]*31

class Location:
    def __init__(self, x=0.0, y=0.0, z=0.0):
        self.x = x
        self.y = y
        self.z = z

class Rotation:
    def __init__(self, roll=0.0, pitch=0.0, yaw=0.0):
        self.roll = roll
        self.pitch = pitch
        self.yaw = yaw

class Velocity:
    def __init__(self, x=0.0, y=0.0, z=0.0):
        self.x = x
        self.y = y
        self.z = z

class Acceleration:
    def __init__(self, x=0.0, y=0.0, z=0.0):
        self.x = x
        self.y = y
        self.z = z

class AngularVelocity:
    def __init__(self, x=0.0, y=0.0, z=0.0):
        self.x = x
        self.y = y
        self.z = z

class Error:
    def __init__(self, y=0.0, psi=0.0):
        self.y = y
        self.psi = psi

class SW:
    def __init__(self, theta=0.0, w=0.0, torque_control=0.0):
        self.theta = theta
        self.w = w
        self.torque_control = torque_control

class VehicleData:
    def __init__(self):
        self.location = Location()
        self.rotation = Rotation()
        self.velocity = Velocity()
        self.acceleration = Acceleration()
        self.angular_velocity = AngularVelocity()
        self.error = Error()
        self.sw = SW()

class SubscribeNode(Node):
    def __init__(self):
        super().__init__('subscribe_node')
        

class PlotNode(Node):
    """
    This node will in charge of plot data.
    """
    def __init__(self):
        """
        This constructor initializes the node with the parameters.
        """
        super().__init__('plot_node')
        self.plot_variable = 0.0
        self.steering_wheel = 0.0
        self.vehicle = VehicleData()
        self.references = References()

        self.sub_steering_wheel = self.create_subscription(
            Float32,
            'simulator/steer_angle',
            self.sub_steering_wheel_callback,
            1)

        self.vehicle_sub = self.create_subscription(
            Float32MultiArray,
            'planner/vehicle',
            self.vehicle_callback,
            10)

        self.references_sub = self.create_subscription(
            Float32MultiArray,
            'planner/references',
            self.references_callback,
            10)
    
    def sub_steering_wheel_callback(self, msg):
        self.steering_wheel = msg.data
    
    def vehicle_callback(self, msg):
        data = msg.data
        self.vehicle.location.x = data[0]
        self.vehicle.location.y = data[1]
        self.vehicle.location.z = data[2]
        
        self.vehicle.rotation.roll = data[3]
        self.vehicle.rotation.pitch = data[4]
        self.vehicle.rotation.yaw = data[5]
        
        self.vehicle.velocity.x = data[6]
        self.vehicle.velocity.y = data[7]
        self.vehicle.velocity.z = data[8]
        
        self.vehicle.acceleration.x = data[9]
        self.vehicle.acceleration.y = data[10]
        self.vehicle.acceleration.z = data[11]
        
        self.vehicle.angular_velocity.x = data[12]
        self.vehicle.angular_velocity.y = data[13]
        self.vehicle.angular_velocity.z = data[14]
        
        self.vehicle.error.y = data[15]
        self.vehicle.error.psi = data[16]
        
        self.vehicle.sw.theta = data[17]
        self.vehicle.sw.w = data[18]
        self.vehicle.sw.torque_control = data[19]

    def references_callback(self, msg):
        data = msg.data
        num_references = len(data) // REF_NUM
        self.references.x = data[::REF_NUM]
        self.references.y = data[1::REF_NUM]
        self.references.yaw = data[2::REF_NUM]

# Initialize an empty plot
plt.ion()  # Turn on interactive mode
fig, ax = plt.subplots()
line, = ax.plot([], [])  # Create an empty line object

rclpy.init(args=None)
node = PlotNode()

# Initialize text annotation
text_annotation = ax.text(0.95, 0.95, '', transform=ax.transAxes, va='top', ha='right')

# Main loop to update the plot in real time
x_data = []
y_data = []
i = 0


while True:
    rclpy.spin_once(node, timeout_sec=0.01)
    x = i

    #y = 0.0
    # ===============================================
    # ===============================================
    y = node.vehicle.error.y
    # ===============================================
    # ===============================================


    x_data.append(x)
    y_data.append(y)

    # Update the plot data
    line.set_xdata(x_data)
    line.set_ydata(y_data)

    # Update text annotation with current value
    #text_annotation.set_text(f'Value: {y:.2f}')

    # Adjust plot limits if needed
    ax.relim()
    ax.autoscale_view()

    # Draw the updated plot
    fig.canvas.draw()
    fig.canvas.flush_events()
    i = i + 0.01
    # Pause for a short time to control the plot refresh rate
    time.sleep(0.01)

# Keep the plot window open after the loop finishes
plt.ioff()  # Turn off interactive mode
plt.show()

