#include <wx/wx.h>
#include <mathplot.h>

class MyFrame : public wxFrame {
public:
    MyFrame() : wxFrame(nullptr, wxID_ANY, "Simple MathPlot Example", wxDefaultPosition, wxSize(800, 600)) {
        // Create the plot window
        mpWindow *plot = new mpWindow(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER);

        // Create the sine function plot
        mpScaleX *xAxis = new mpScaleX(wxT("X"), mpALIGN_BOTTOM, true);
        mpScaleY *yAxis = new mpScaleY(wxT("Y"), mpALIGN_LEFT, true);
        mpFXYVector *sinePlot = new mpFXYVector(_("Sine"));

        // Prepare vectors to store data points
        std::vector<double> xData, yData;

        // Populate the data vectors with the sine function values
        for (double x = 0; x < 10; x += 0.1) {
            xData.push_back(x);
            yData.push_back(sin(x));
        }

        // Set the data for the sine plot
        sinePlot->SetData(xData, yData);

        // Add the layers to the plot
        plot->AddLayer(xAxis);
        plot->AddLayer(yAxis);
        plot->AddLayer(sinePlot);

        // Set margins and fit the plot
        plot->SetMargins(40, 40, 40, 40);
        plot->Fit();

        // Set up the layout
        wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);
        sizer->Add(plot, 1, wxEXPAND);
        SetSizerAndFit(sizer);
    }
};

class MyApp : public wxApp {
public:
    virtual bool OnInit() {
        MyFrame *frame = new MyFrame();
        frame->Show(true);
        return true;
    }
};

wxIMPLEMENT_APP(MyApp);


class MyApp : public wxApp {
public:
    virtual bool OnInit() {
        MyFrame *frame = new MyFrame();
        frame->Show(true);
        return true;
    }
};

wxIMPLEMENT_APP(MyApp);
