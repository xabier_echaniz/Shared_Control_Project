# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.22

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot/build

# Include any dependencies generated for this target.
include samples/sample1/CMakeFiles/mpSample1.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include samples/sample1/CMakeFiles/mpSample1.dir/compiler_depend.make

# Include the progress variables for this target.
include samples/sample1/CMakeFiles/mpSample1.dir/progress.make

# Include the compile flags for this target's objects.
include samples/sample1/CMakeFiles/mpSample1.dir/flags.make

samples/sample1/CMakeFiles/mpSample1.dir/mp1.o: samples/sample1/CMakeFiles/mpSample1.dir/flags.make
samples/sample1/CMakeFiles/mpSample1.dir/mp1.o: ../samples/sample1/mp1.cpp
samples/sample1/CMakeFiles/mpSample1.dir/mp1.o: samples/sample1/CMakeFiles/mpSample1.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object samples/sample1/CMakeFiles/mpSample1.dir/mp1.o"
	cd /home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot/build/samples/sample1 && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT samples/sample1/CMakeFiles/mpSample1.dir/mp1.o -MF CMakeFiles/mpSample1.dir/mp1.o.d -o CMakeFiles/mpSample1.dir/mp1.o -c /home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot/samples/sample1/mp1.cpp

samples/sample1/CMakeFiles/mpSample1.dir/mp1.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/mpSample1.dir/mp1.i"
	cd /home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot/build/samples/sample1 && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot/samples/sample1/mp1.cpp > CMakeFiles/mpSample1.dir/mp1.i

samples/sample1/CMakeFiles/mpSample1.dir/mp1.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/mpSample1.dir/mp1.s"
	cd /home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot/build/samples/sample1 && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot/samples/sample1/mp1.cpp -o CMakeFiles/mpSample1.dir/mp1.s

# Object files for target mpSample1
mpSample1_OBJECTS = \
"CMakeFiles/mpSample1.dir/mp1.o"

# External object files for target mpSample1
mpSample1_EXTERNAL_OBJECTS =

samples/sample1/mpSample1: samples/sample1/CMakeFiles/mpSample1.dir/mp1.o
samples/sample1/mpSample1: samples/sample1/CMakeFiles/mpSample1.dir/build.make
samples/sample1/mpSample1: libmathplot.a
samples/sample1/mpSample1: samples/sample1/CMakeFiles/mpSample1.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable mpSample1"
	cd /home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot/build/samples/sample1 && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/mpSample1.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
samples/sample1/CMakeFiles/mpSample1.dir/build: samples/sample1/mpSample1
.PHONY : samples/sample1/CMakeFiles/mpSample1.dir/build

samples/sample1/CMakeFiles/mpSample1.dir/clean:
	cd /home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot/build/samples/sample1 && $(CMAKE_COMMAND) -P CMakeFiles/mpSample1.dir/cmake_clean.cmake
.PHONY : samples/sample1/CMakeFiles/mpSample1.dir/clean

samples/sample1/CMakeFiles/mpSample1.dir/depend:
	cd /home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot /home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot/samples/sample1 /home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot/build /home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot/build/samples/sample1 /home/user/shared_control/plotter/wxMathPlot_0.2.0/mathplot/build/samples/sample1/CMakeFiles/mpSample1.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : samples/sample1/CMakeFiles/mpSample1.dir/depend

