import matplotlib.pyplot as plt
import numpy as np
import rclpy
import time

from rclpy.node import Node
from std_msgs.msg import Float32
from std_msgs.msg import Float32MultiArray

REF_NUM = 3

class References:
    def __init__(self):
        self.x = [0.0]*31
        self.y = [0.0]*31
        self.yaw = [0.0]*31

class Location:
    def __init__(self, x=0.0, y=0.0, z=0.0):
        self.x = x
        self.y = y
        self.z = z

class Rotation:
    def __init__(self, roll=0.0, pitch=0.0, yaw=0.0):
        self.roll = roll
        self.pitch = pitch
        self.yaw = yaw

class Velocity:
    def __init__(self, x=0.0, y=0.0, z=0.0):
        self.x = x
        self.y = y
        self.z = z

class Acceleration:
    def __init__(self, x=0.0, y=0.0, z=0.0):
        self.x = x
        self.y = y
        self.z = z

class AngularVelocity:
    def __init__(self, x=0.0, y=0.0, z=0.0):
        self.x = x
        self.y = y
        self.z = z

class Error:
    def __init__(self, y=0.0, psi=0.0):
        self.y = y
        self.psi = psi

class SW:
    def __init__(self, theta=0.0, w=0.0, torque_control=0.0):
        self.theta = theta
        self.w = w
        self.torque_control = torque_control

class VehicleData:
    def __init__(self):
        self.location = Location()
        self.rotation = Rotation()
        self.velocity = Velocity()
        self.acceleration = Acceleration()
        self.angular_velocity = AngularVelocity()
        self.error = Error()
        self.sw = SW()

class SubscribeNode(Node):
    def __init__(self):
        super().__init__('subscribe_node')
        

class PlotNode(Node):
    """
    This node will in charge of plot data.
    """
    def __init__(self):
        """
        This constructor initializes the node with the parameters.
        """
        super().__init__('plot_node')
        self.plot_variable = 0.0
        self.steering_wheel = 0.0
        self.vehicle = VehicleData()
        self.references = References()

        # self.sub_steering_wheel = self.create_subscription(
        #     Float32,
        #     'simulator/steer_angle',
        #     self.sub_steering_wheel_callback,
        #     1)

        self.vehicle_sub = self.create_subscription(
            Float32MultiArray,
            'planner/vehicle',
            self.vehicle_callback,
            1)

        self.references_sub = self.create_subscription(
            Float32MultiArray,
            'planner/references',
            self.references_callback,
            1)
    
    # def sub_steering_wheel_callback(self, msg):
    #     self.steering_wheel = msg.data
    
    def vehicle_callback(self, msg):
        data = msg.data
        self.vehicle.location.x = data[0]
        self.vehicle.location.y = data[1]
        self.vehicle.location.z = data[2]
        
        self.vehicle.rotation.roll = data[3]
        self.vehicle.rotation.pitch = data[4]
        self.vehicle.rotation.yaw = data[5]
        
        self.vehicle.velocity.x = data[6]
        self.vehicle.velocity.y = data[7]
        self.vehicle.velocity.z = data[8]
        
        self.vehicle.acceleration.x = data[9]
        self.vehicle.acceleration.y = data[10]
        self.vehicle.acceleration.z = data[11]
        
        self.vehicle.angular_velocity.x = data[12]
        self.vehicle.angular_velocity.y = data[13]
        self.vehicle.angular_velocity.z = data[14]
        
        self.vehicle.error.y = data[15]
        self.vehicle.error.psi = data[16]
        
        self.vehicle.sw.theta = data[17]
        self.vehicle.sw.w = data[18]
        self.vehicle.sw.torque_control = data[19]

    def references_callback(self, msg):
        data = msg.data
        num_references = len(data) // REF_NUM
        self.references.x = data[::REF_NUM]
        self.references.y = data[1::REF_NUM]
        self.references.yaw = data[2::REF_NUM]

# Initialize an empty plot
# plt.ion()  # Turn on interactive mode
# fig, ax = plt.subplots()
# line, = ax.plot([], [])  # Create an empty line object

# rclpy.init(args=None)
# node = PlotNode()

# # Initialize text annotation
# text_annotation = ax.text(0.95, 0.95, '', transform=ax.transAxes, va='top', ha='right')

# # Main loop to update the plot in real time
# x_data = []
# y_data = []
# i = 0

# # Set the title of the window
# plt.title('Your Window Title')

# while True:
#     rclpy.spin_once(node, timeout_sec=0.01)
#     x = i

#     #y = 0.0
#     # ===============================================
#     # ===============================================
#     y = node.vehicle.error.psi
#     # ===============================================
#     # ===============================================


#     x_data.append(x)
#     y_data.append(y)

#     # Update the plot data
#     line.set_xdata(x_data)
#     line.set_ydata(y_data)

#     # Update text annotation with current value
#     #text_annotation.set_text(f'Value: {y:.2f}')

#     # Adjust plot limits if needed
#     ax.relim()
#     ax.autoscale_view()

#     # Draw the updated plot
#     fig.canvas.draw()
#     fig.canvas.flush_events()
#     i = i + 0.01
#     # Pause for a short time to control the plot refresh rate
#     time.sleep(0.01)

# # Keep the plot window open after the loop finishes
# plt.ioff()  # Turn off interactive mode
# plt.show()

# ===================================================================================================
same_graph = False

if same_graph == True:
    plt.ion()  # Turn on interactive mode
    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(8, 6))  # Create two subplots
    line1, = ax1.plot([], [], label='Plot 1')  # Create empty line objects for each plot
    line2, = ax2.plot([], [], label='Plot 2')

    rclpy.init(args=None)
    node = PlotNode()

    # Activate grid on both plots
    ax1.grid(True)
    ax2.grid(True)

    # Set labels for each plot
    ax1.set_xlabel('Heading state [rad]')
    ax2.set_xlabel('Heading ref. [rad]')

    # Initialize text annotation
    text_annotation = ax1.text(0.95, 0.95, '', transform=ax1.transAxes, va='top', ha='right')

    # Main loop to update the plot in real-time
    x_data = []
    y_data1 = []  # Data for plot 1
    y_data2 = []  # Data for plot 2
    i = 0

    while True:
        # Update vehicle data
        rclpy.spin_once(node, timeout_sec=0.01)
        
        # Update x and y data for plot 1
        x = i
        y1 = node.vehicle.rotation.yaw
        x_data.append(x)
        y_data1.append(y1)

        # Update the plot data for plot 1
        line1.set_xdata(x_data)
        line1.set_ydata(y_data1)

        # Update x and y data for plot 2 (dummy data for demonstration)
        y2 = node.references.yaw[0]  # Example random data
        y_data2.append(y2)

        # Update the plot data for plot 2
        line2.set_xdata(x_data)
        line2.set_ydata(y_data2)

        # Adjust plot limits if needed
        ax1.relim()
        ax1.autoscale_view()
        ax2.relim()
        ax2.autoscale_view()

        # Draw the updated plots
        fig.canvas.draw()
        fig.canvas.flush_events()

        # Increment x for next data point
        i += 0.01

        # Pause for a short time to control the plot refresh rate
        time.sleep(0.01)

    # Keep the plot window open after the loop finishes
    plt.ioff()  # Turn off interactive mode
    plt.show()

else:
    plt.ion()  # Turn on interactive mode
    fig, ax = plt.subplots(figsize=(8, 6))  # Create a single subplot
    line1, = ax.plot([], [], label='Heading state')  # Create empty line for vehicle rotation
    line2, = ax.plot([], [], label='Heading ref')  # Create empty line for reference yaw

    rclpy.init(args=None)
    node = PlotNode()

    # Activate grid on the plot
    ax.grid(True)

    # Set labels for the axes
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Heading [rad]')

    # Initialize text annotation
    text_annotation = ax.text(0.95, 0.95, '', transform=ax.transAxes, va='top', ha='right')

    # Main loop to update the plot in real-time
    x_data = []
    y_data1 = []  # Data for vehicle rotation (heading state)
    y_data2 = []  # Data for reference yaw (heading ref)
    i = 0

    while True:
        # Update vehicle data
        rclpy.spin_once(node, timeout_sec=0.01)
        
        # Update x and y data for vehicle rotation (heading state)
        x = i
        y1 = node.vehicle.rotation.yaw
        x_data.append(x)
        y_data1.append(y1)

        # Update the plot data for vehicle rotation (heading state)
        line1.set_xdata(x_data)
        line1.set_ydata(y_data1)

        # Update x and y data for reference yaw (heading ref) (dummy data for demonstration)
        y2 = node.references.yaw[0]  # Example random data
        y_data2.append(y2)

        # Update the plot data for reference yaw (heading ref)
        line2.set_xdata(x_data)
        line2.set_ydata(y_data2)

        # Adjust plot limits if needed
        ax.relim()
        ax.autoscale_view()

        # Draw the updated plot
        fig.canvas.draw()
        fig.canvas.flush_events()

        # Increment x for next data point
        i += 0.01

        # Pause for a short time to control the plot refresh rate
        time.sleep(0.01)

    # Keep the plot window open after the loop finishes
    plt.ioff()  # Turn off interactive mode
    plt.show()


