# generated from ament/cmake/core/templates/nameConfig.cmake.in

# prevent multiple inclusion
if(_plotter_CONFIG_INCLUDED)
  # ensure to keep the found flag the same
  if(NOT DEFINED plotter_FOUND)
    # explicitly set it to FALSE, otherwise CMake will set it to TRUE
    set(plotter_FOUND FALSE)
  elseif(NOT plotter_FOUND)
    # use separate condition to avoid uninitialized variable warning
    set(plotter_FOUND FALSE)
  endif()
  return()
endif()
set(_plotter_CONFIG_INCLUDED TRUE)

# output package information
if(NOT plotter_FIND_QUIETLY)
  message(STATUS "Found plotter: 0.0.0 (${plotter_DIR})")
endif()

# warn when using a deprecated package
if(NOT "" STREQUAL "")
  set(_msg "Package 'plotter' is deprecated")
  # append custom deprecation text if available
  if(NOT "" STREQUAL "TRUE")
    set(_msg "${_msg} ()")
  endif()
  # optionally quiet the deprecation message
  if(NOT ${plotter_DEPRECATED_QUIET})
    message(DEPRECATION "${_msg}")
  endif()
endif()

# flag package as ament-based to distinguish it after being find_package()-ed
set(plotter_FOUND_AMENT_PACKAGE TRUE)

# include all config extra files
set(_extras "")
foreach(_extra ${_extras})
  include("${plotter_DIR}/${_extra}")
endforeach()
