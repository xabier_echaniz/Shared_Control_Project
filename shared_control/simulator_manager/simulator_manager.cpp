#include "shared_control_basics.hpp" 

#define LOOP_TIME_MS    5

class SimulatorManagerNode : public rclcpp::Node
{
public:
    SimulatorManagerNode()
        : Node("sim_manager_node")
    {
        // SUBSCRIBERS
        a_sub_ = this->create_subscription<std_msgs::msg::Float32>(
            "simulator/steer_angle", 1, std::bind(&SimulatorManagerNode::sim_a_callback, this, _1));
        w_sub_ = this->create_subscription<std_msgs::msg::Float32>(
            "simulator/steer_angular_velocity", 1, std::bind(&SimulatorManagerNode::sim_w_callback, this, _1));
        t_sub_ = this->create_subscription<std_msgs::msg::Float32>(
            "simulator/throttle", 1, std::bind(&SimulatorManagerNode::sim_t_callback, this, _1));
        b_sub_ = this->create_subscription<std_msgs::msg::Float32>(
            "simulator/brake", 1, std::bind(&SimulatorManagerNode::sim_b_callback, this, _1));
        d_sub_ = this->create_subscription<std_msgs::msg::Float32>(
            "simulator/steer_driver_torque", 1, std::bind(&SimulatorManagerNode::sim_d_callback, this, _1));
        mpc_control_cmd_sub_ = this->create_subscription<std_msgs::msg::Float32>(
            "mpc/control_cmd", 1, std::bind(&SimulatorManagerNode::mpc_control_cmd_callback, this, _1));
        carla_vehicle_sub_ = this->create_subscription<std_msgs::msg::Float32MultiArray>(
            "hero0/vehicle", 1, std::bind(&SimulatorManagerNode::carla_vehicle_callback, this, _1));
        planner_online_data_sub_ = this->create_subscription<std_msgs::msg::Float32MultiArray>(
            "planner/online_data", 1, std::bind(&SimulatorManagerNode::planner_online_data_callback, this, _1));
        planner_weights_sub_ = this->create_subscription<std_msgs::msg::Float32MultiArray>(
            "planner/weights", 1, std::bind(&SimulatorManagerNode::planner_weights_callback, this, _1));

        // PUBLISHERS
        t_pub_ = this->create_publisher<std_msgs::msg::Float32>("sim_manager/steer_torque", 1);
        f_pub_ = this->create_publisher<std_msgs::msg::Float32>("sim_manager/steer_friction", 1);
        d_pub_ = this->create_publisher<std_msgs::msg::Float32>("sim_manager/steer_damp", 1);
        simulator_data_pub_ = this->create_publisher<std_msgs::msg::Float32MultiArray>("sim_manager/simulator_status", 1);
        carla_control_pub_ = this->create_publisher<carla_msgs::msg::CarlaEgoVehicleControl>("sim_manager/hero0_control", 1);
    }

    // CALLBACKS
    void sim_a_callback(const std_msgs::msg::Float32::SharedPtr msg) {simulator.status.sw_angle  = msg->data;}
    void sim_w_callback(const std_msgs::msg::Float32::SharedPtr msg) {simulator.status.sw_angular_velocity  = msg->data;}
    void sim_t_callback(const std_msgs::msg::Float32::SharedPtr msg) {simulator.status.throttle  = msg->data;}
    void sim_b_callback(const std_msgs::msg::Float32::SharedPtr msg) {simulator.status.brake  = msg->data;}
    void sim_d_callback(const std_msgs::msg::Float32::SharedPtr msg) {simulator.status.sw_driver_torque  = msg->data;}
    void mpc_control_cmd_callback(const std_msgs::msg::Float32::SharedPtr msg) {mpc_control_cmd  = msg->data;}
    void carla_vehicle_callback(const std_msgs::msg::Float32MultiArray::SharedPtr msg)
    {
        vehicle.location.x = msg->data[0];
        vehicle.location.y = msg->data[1];
        vehicle.location.z = msg->data[2];

        vehicle.rotation.roll = msg->data[3];
        vehicle.rotation.pitch = msg->data[4];
        vehicle.rotation.yaw = msg->data[5];

        vehicle.velocity.x = msg->data[6];
        vehicle.velocity.y = msg->data[7];
        vehicle.velocity.z = msg->data[8];

        vehicle.acceleration.x = msg->data[9];
        vehicle.acceleration.y = msg->data[10];
        vehicle.acceleration.z = msg->data[11];

        vehicle.angular_velocity.x = msg->data[12];
        vehicle.angular_velocity.y = msg->data[13];
        vehicle.angular_velocity.z = msg->data[14];
    }
    void planner_online_data_callback(const std_msgs::msg::Float32MultiArray::SharedPtr msg)
    {
        int i = 0;
        for(i = 0; i < N+1; i++){
            online_data.lambda[i]    = msg->data[i*ONLINE_NUM];
            online_data.ax[i]        = msg->data[i*ONLINE_NUM + 1];
            online_data.curvature[i] = msg->data[i*ONLINE_NUM + 2];
        }
    }
    void planner_weights_callback(const std_msgs::msg::Float32MultiArray::SharedPtr msg)
    {
        weights.c_ay   = msg->data[0];
        weights.c_T    = msg->data[1];
        weights.c_dpsi = msg->data[2];
        weights.c_th   = msg->data[3];
        weights.c_vy   = msg->data[4];
        weights.c_dt   = msg->data[5];
        weights.w_ay   = msg->data[6];
        weights.w_w    = msg->data[7];
        weights.w_epsi = msg->data[8];
        weights.w_dpsi = msg->data[9];
        weights.w_ey   = msg->data[10];
        weights.w_dT   = msg->data[11];
        weights.Lambda = msg->data[12];
        weights.w_T    = msg->data[13];
    }

    // PUBLISH DATA 
    void publishData(Simulator simulator, CarlaControl carla_control)
    {
        // Send torque
        std_msgs::msg::Float32 t_msg;
        t_msg.data = simulator.command.sw_torque;
        t_pub_->publish(t_msg);

        // Send frinction
        std_msgs::msg::Float32 f_msg;
        f_msg.data = simulator.command.sw_friction;
        f_pub_->publish(f_msg);

        // Send damp
        std_msgs::msg::Float32 d_msg;
        d_msg.data = simulator.command.sw_damp;
        d_pub_->publish(d_msg);

        // Send control
        carla_msgs::msg::CarlaEgoVehicleControl ctrl_msg;
        ctrl_msg.steer = carla_control.steering;
        ctrl_msg.throttle = carla_control.throttle;
        ctrl_msg.brake = carla_control.brake;
        carla_control_pub_->publish(ctrl_msg);

        // Send simulator data
        std_msgs::msg::Float32MultiArray simulator_msg;
        simulator_msg.data.resize(2);
        simulator_msg.data[0] = simulator.status.sw_angle;                 
        simulator_msg.data[1] = simulator.status.sw_angular_velocity;      
        simulator_msg.data[2] = simulator.status.throttle;                 
        simulator_msg.data[3] = simulator.status.brake;                   
        simulator_msg.data[4] = simulator.status.sw_driver_torque;  
        simulator_data_pub_->publish(simulator_msg);       
    }

    void get_ros_data(Vehicle& get_vehicle, Simulator& get_simulator, float& get_mpc_control_cmd, OnlineData& get_online_data, Weights& get_weights)
    {
        get_vehicle = vehicle;
        get_simulator = simulator;
        get_mpc_control_cmd = mpc_control_cmd;
        get_online_data = online_data;
        get_weights = weights;
    }

private:

    // Input data from the simulator
    Vehicle vehicle;
    Simulator simulator;
    OnlineData online_data;
    Weights weights;
    
    // Input data from MPC
    float mpc_control_cmd;

    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr a_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr w_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr t_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr b_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr d_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr mpc_control_cmd_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr carla_vehicle_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr planner_online_data_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr planner_weights_sub_;
    
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr t_pub_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr f_pub_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr d_pub_;
    rclcpp::Publisher<std_msgs::msg::Float32MultiArray>::SharedPtr simulator_data_pub_;
    rclcpp::Publisher<carla_msgs::msg::CarlaEgoVehicleControl>::SharedPtr carla_control_pub_;
};

float calculate_sat(Vehicle vehicle, Simulator simulator)
{
    float sat, alphaf, Ksat, Cf, Cr, lf, dpsi, vx, vy, Fyf, df, kd;
    float lim_sat = 15;

    // Constants
    Cf = 94000/10;
    Cr = 118000/10;
    kd = MAX_SW_ANGLE/MAX_STEER_ANGLE;
    df = simulator.status.sw_angle/kd;
    
    lf = 1.61;

    // Calculate dpsi (yaw rate)
    dpsi = vehicle.angular_velocity.z;
    

    // Vehicle velocities
    vy = vehicle.velocity.y;
    vx = vehicle.velocity.x;

    cout << fixed << setprecision(3)
    << "\nvx: " << vx << endl;
    cout << "vy: " << vy << endl;
    cout << "dpsi: " << dpsi << endl;
    

    // Calculate slip angle
    alphaf = df - atan((lf * dpsi + vy) / (vx + 1e-3));
    // cout << "alphaf: " << alphaf << endl;
    // Constants for self-aligning torque calculation
    Ksat = 1.0 / 300.0;

    // Calculate lateral force at the front axle
    Fyf = Cf * alphaf;
    cout << "theta: " << simulator.status.sw_angle << endl;
    cout << "Fyf: " << Fyf << endl;
    cout << "delta: " << df << endl;
    // cout << "dpsi:  " << dpsi << endl;
    // Calculate self-aligning torque
    
    if (abs(vx) < 0.3){
        sat = -1.5*simulator.status.sw_angle;
    }
    else{
    sat = -Ksat * Fyf;
    }

    if(sat > lim_sat) sat = lim_sat;
    if(sat < -lim_sat) sat = -lim_sat;
    
    // cout << "ksat: " << Ksat << endl;
    // cout << "sat : " << sat << endl;

    return sat;
}

int main(int argc, char *argv[]){

    // Set loop time
    constexpr auto loop_time = std::chrono::milliseconds(LOOP_TIME_MS);

    // Create ROS node
    rclcpp::init(argc, argv);
    auto simulator_manager_node = std::make_shared<SimulatorManagerNode>();

    // Input data
    Simulator simulator;
    Vehicle vehicle;
    CarlaControl carla_control;
    OnlineData online_data;
    Weights weights;

    float mpc_control_cmd;
    float sat;
    float sat_array[3];
    float mpc_control_array[3];

    // MAIN LOOP
    while(rclcpp::ok()){

        
        
        // Initialize loop clock
        auto init_loop_time = std::chrono::high_resolution_clock::now();

        // Process any callbacks once and get data
        rclcpp::spin_some(simulator_manager_node); 
        simulator_manager_node->get_ros_data(vehicle, simulator, mpc_control_cmd, online_data, weights);

        // Carla control commands
        carla_control.steering = simulator.status.sw_angle/(MAX_SW_ANGLE*PI/180);
        carla_control.throttle = simulator.status.throttle+weights.c_dpsi;
        carla_control.brake = simulator.status.brake;

        // Simulator commands
        simulator.command.sw_damp = 0.65*sqrt((online_data.lambda[0]+1.0)/2.0);
            
        for(int i = 2; i > 0; i--) sat_array[i] = sat_array[i-1];
        sat_array[0] = calculate_sat(vehicle, simulator);
        sat = (sat_array[0]+sat_array[1]+sat_array[2])/3; // Mean of last values

        for(int i = 2; i > 0; i--) mpc_control_array[i] = mpc_control_array[i-1];
        mpc_control_array[0] = mpc_control_cmd;
        mpc_control_cmd = (mpc_control_array[0]+mpc_control_array[1]+mpc_control_array[2])/3; // Mean of last values

        simulator.command.sw_torque = sat + mpc_control_cmd; // + 0.3*sign_function(mpc_control_cmd); 

        // Send Carla and Simulator parameters
        simulator_manager_node->publishData(simulator, carla_control); 
        
        // Calculate execution time
		auto end_exec_time = std::chrono::high_resolution_clock::now();
        auto exec_time = std::chrono::duration_cast<std::chrono::microseconds>(end_exec_time - init_loop_time);

        // Sleep until the next desired wake-up time
        auto next_init_time = init_loop_time + loop_time;
        std::this_thread::sleep_until(next_init_time);

        // Calculate loop time
        auto end_loop_time = std::chrono::high_resolution_clock::now();
        auto elapsed_loop_time = std::chrono::duration_cast<std::chrono::microseconds>(end_loop_time - init_loop_time);
        
        // Clear the terminal
        system(clearCommand);

        cout << "\n =========================\n ======= SIMULATOR =======\n ======== MANAGER ========\n =========================\n" << endl;

        cout << fixed << setprecision(3) 
        << "\n        PREDICTIONS"
        << "\n---------------------------"
        << "\n SAT [Nm]:    " << sat
        << "\n MPC [Nm]:    " << mpc_control_cmd
        << "\n Total [Nm]:  " << simulator.command.sw_torque
        << endl;

        cout << fixed << setprecision(3) 
        << "\n        TIME VALUES"
        << "\n--------------------------"
        << "\n Loop time [ms]:   " << elapsed_loop_time.count()/1000.0
        << "\n Exec time [ms]:   " << exec_time.count()/1000.0
        << endl;  
    }

    rclcpp::shutdown();

}