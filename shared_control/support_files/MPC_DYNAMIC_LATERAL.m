close all; clear all; clc;

DifferentialState X Y PSI dx dy dpsi epsi ey theta w T ay  % Differential variables (states)
AlgebraicState    Fyf Fyr bef delta                     % Algebraic variables
OnlineData        ka ax kurv                            % Exogenous input vector
Control           dT                                    % Control variables or inputs

%% MPC
Ts  = 0.05; % Timestep of the samples
N   = 30;   % Number of samples

%% Motor
J    = 0.1; % Inertia
b    = 0.65;   % Damping

%bef = b*sqrt((ksat+ka)/ksat);

%% Intermediate State
%Tsat = k*theta;
%Taut = ka*(theta-theta_d) - (bef - b)*wd;

%% Baseline values
L  = 3.05;      % wheelbase [m]
lf = 1.40;      % front ale to CG [m]
lr = L-lf;      % rear ale to CG [m]
m  = 1650;      % mass of the vehicle[kg]
Iz = 3234;      % moment of inertia about z [kg.m^2]
Cf = 94000;
Cr = 118000;

ls  = 2;
kd  = 8.775;
ksa = (Cf/(1200));

%s = T/(sqrt(T*T +(1e-3)^2));

%% -- Differential Equation --
f =[dot(X)     ==  dx*cos(PSI) - dy*sin(PSI);                       % x position (absolute)
    dot(Y)     ==  dx*sin(PSI) + dy*cos(PSI);                       % y position (absolute) & cross track error
    dot(PSI)   ==  dpsi;
    dot(dx)    ==  (m*ax*cos(delta) + m*dy*dpsi - Fyf*sin(delta))/m;
    dot(dy)    ==  (-m*dx*dpsi + m*ax*sin(delta) + Fyf*cos(delta) + Fyr)/m;                   
    dot(dpsi)  ==  (lf*(m*ax*sin(delta)+Fyf*cos(delta)) - lr*Fyr)/Iz;
    dot(epsi)  ==  dpsi - (kurv/(1-kurv*ey))*(dx*cos(epsi)- dy*sin(epsi)); %dpsi - (kurv/(1-kurv*ey))*(dx*cos(epsi)- dy*sin(epsi));
    dot(ey)    ==  dx*sin(epsi) + dy*cos(epsi);                     % v*sin(PSI - PSIr);
    dot(theta) ==  w;                                                             % Definition of the state number 1
    dot(w)     ==  (-bef/J)*w + (T)/(J) + (Fyf/(900))/J; % - 0.5*w/(J*sqrt(w*w +(1e-3)^2)); % - (1-mode)*((ka-1)*(theta-thetad))/J;     % (-Fyf/750)(1/1000)*(-Cf*alfaf)% Definition of the state number 2
    %dot(w)    ==  (-bef/J)*w + (T)/J - (-Fyf/1200)/J - (ka*(theta-theta_d))/J;   % (-Fyf/750) (1/1000)*(-Cf*alfaf)% Definition of the state number 2
    %dot(w)    ==  (-bef/J)*w +  T/J - (k*theta)/J - (ka*(theta-theta_d) - (bef - b)*wd)/J;     % Definition of the state number 2
    dot(T)     ==  ka*dT; %mode*ka*dT + (1-mode)*dT;                                      % Definition of the state number 3
    dot(ay)    ==  (2*ax*delta + dx*w/(-kd))*dx/L;
    %dot(beta)  == -(Cr+Cf)*beta/(m*vx) + ((Cr*lr - Cf*lf)/(m*vx^2) - 1)*r +Cf*delta/(m*vx);
     0         ==  Fyf - (Cf)*(delta - atan((dy + lf*dpsi)/(dx+1e-3)));
     0         ==  Fyr - (Cr)*atan((-dy + lr*dpsi)/(dx+1e-3));
     0         ==  bef - (b*sqrt((ka+1)/2)); %mode*b*sqrt(ka)- (1-mode)*b*sqrt((ksa+(ka-1))/ksa);
     0         ==  delta  + theta/kd];
 
%% MPCexport
fprintf('----------------------------\n         MPCexport         \n----------------------------\n');
acadoSet('problemname', 'mpc');

n_XD   = length([X Y PSI dpsi w T]); %xr vr]);%diffStates);
n_U    = length([dT]);         %controls;
ocp    = acado.OCP( 0.0, N*Ts, N );
       
W_mat  = eye(n_XD+n_U,n_XD+n_U);
WN_mat = eye(n_XD,n_XD);
W      = acado.BMatrix(W_mat);
WN     = acado.BMatrix(WN_mat);
       
h      = [X;Y;PSI;dpsi;w;T;dT]; % diffStates;controls];
hN     = [X;Y;PSI;dpsi;w;T];    % diffStates];
       
ocp.minimizeLSQ( W, h );
ocp.minimizeLSQEndTerm( WN, hN );
       
%% -- state and control constraints
ocp.subjectTo(-1 <= dT <= 1);
ocp.subjectTo(-1 <= dpsi <= 1);
ocp.subjectTo(-1 <= ey <= 1);
%ocp.subjectTo( -1 <= X <= 1);
%ocp.subjectTo( -1 <= Y <= 1);
% ocp.subjectTo(-1 <= theta <= 1);
ocp.subjectTo(-1 <= w <= 1);
ocp.subjectTo(-1 <= T <= 1);
ocp.subjectTo(-1 <= ay <= 1);

ocp.setModel(f);

mpc = acado.OCPexport( ocp );
mpc.set( 'HESSIAN_APPROXIMATION',       'GAUSS_NEWTON'      );
mpc.set( 'DISCRETIZATION_TYPE',         'MULTIPLE_SHOOTING' );
mpc.set( 'QP_SOLVER',                   'QP_QPOASES3'    	);
%mpc.set( 'QP_SOLVER',                   'QP_QPOASES'    	);
mpc.set( 'HOTSTART_QP',                 'NO'             	);
%mpc.set( 'HOTSTART_QP',                 'YES'             	);
mpc.set( 'LEVENBERG_MARQUARDT', 		 1e-10				);
mpc.set( 'CG_HARDCODE_CONSTRAINT_VALUES','NO' 				);
mpc.set( 'SPARSE_QP_SOLUTION',          'FULL_CONDENSING'   );
mpc.set( 'INTEGRATOR_TYPE',             'INT_IRK_GL2'       );
%mpc.set( 'INTEGRATOR_TYPE',               'INT_RK4'        );
mpc.set( 'NUM_INTEGRATOR_STEPS',        2*N                 );
mpc.exportCode( 'export_MPC' );

global ACADO_;
copyfile([ACADO_.pwd '/../../external_packages/qpoases3'], 'export_MPC/qpoases3')

MPC_Dynamic_Lateral_sfunction();

delete mpc.cpp mpc_RUN.m mpc_data_acadodata_M1.txt mpc_data_acadodata_M2.txt mpc_RUN.mexw64;
copyfile('*.mexw64', '..\..\mex');