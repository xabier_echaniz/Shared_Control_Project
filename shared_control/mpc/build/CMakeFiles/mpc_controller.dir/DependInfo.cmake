
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/user/shared_control/mpc/include/acado_library_generated/acado_auxiliary_functions.c" "CMakeFiles/mpc_controller.dir/include/acado_library_generated/acado_auxiliary_functions.c.o" "gcc" "CMakeFiles/mpc_controller.dir/include/acado_library_generated/acado_auxiliary_functions.c.o.d"
  "/home/user/shared_control/mpc/include/acado_library_generated/acado_integrator.c" "CMakeFiles/mpc_controller.dir/include/acado_library_generated/acado_integrator.c.o" "gcc" "CMakeFiles/mpc_controller.dir/include/acado_library_generated/acado_integrator.c.o.d"
  "/home/user/shared_control/mpc/include/acado_library_generated/acado_qpoases3_interface.c" "CMakeFiles/mpc_controller.dir/include/acado_library_generated/acado_qpoases3_interface.c.o" "gcc" "CMakeFiles/mpc_controller.dir/include/acado_library_generated/acado_qpoases3_interface.c.o.d"
  "/home/user/shared_control/mpc/include/acado_library_generated/acado_solver.c" "CMakeFiles/mpc_controller.dir/include/acado_library_generated/acado_solver.c.o" "gcc" "CMakeFiles/mpc_controller.dir/include/acado_library_generated/acado_solver.c.o.d"
  "/home/user/shared_control/mpc/include/qpoases3/src/Bounds.c" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/Bounds.c.o" "gcc" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/Bounds.c.o.d"
  "/home/user/shared_control/mpc/include/qpoases3/src/Constraints.c" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/Constraints.c.o" "gcc" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/Constraints.c.o.d"
  "/home/user/shared_control/mpc/include/qpoases3/src/Flipper.c" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/Flipper.c.o" "gcc" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/Flipper.c.o.d"
  "/home/user/shared_control/mpc/include/qpoases3/src/Indexlist.c" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/Indexlist.c.o" "gcc" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/Indexlist.c.o.d"
  "/home/user/shared_control/mpc/include/qpoases3/src/Matrices.c" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/Matrices.c.o" "gcc" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/Matrices.c.o.d"
  "/home/user/shared_control/mpc/include/qpoases3/src/MessageHandling.c" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/MessageHandling.c.o" "gcc" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/MessageHandling.c.o.d"
  "/home/user/shared_control/mpc/include/qpoases3/src/Options.c" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/Options.c.o" "gcc" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/Options.c.o.d"
  "/home/user/shared_control/mpc/include/qpoases3/src/QProblem.c" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/QProblem.c.o" "gcc" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/QProblem.c.o.d"
  "/home/user/shared_control/mpc/include/qpoases3/src/QProblemB.c" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/QProblemB.c.o" "gcc" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/QProblemB.c.o.d"
  "/home/user/shared_control/mpc/include/qpoases3/src/Utils.c" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/Utils.c.o" "gcc" "CMakeFiles/mpc_controller.dir/include/qpoases3/src/Utils.c.o.d"
  "/home/user/shared_control/mpc/include/mpc_bridge_cpp/mpc_bridge.cpp" "CMakeFiles/mpc_controller.dir/include/mpc_bridge_cpp/mpc_bridge.cpp.o" "gcc" "CMakeFiles/mpc_controller.dir/include/mpc_bridge_cpp/mpc_bridge.cpp.o.d"
  "/home/user/shared_control/mpc/mpc_controller.cpp" "CMakeFiles/mpc_controller.dir/mpc_controller.cpp.o" "gcc" "CMakeFiles/mpc_controller.dir/mpc_controller.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
