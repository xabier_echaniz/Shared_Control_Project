/*
 *    This file was auto-generated using the ACADO Toolkit.
 *    
 *    While ACADO Toolkit is free software released under the terms of
 *    the GNU Lesser General Public License (LGPL), the generated code
 *    as such remains the property of the user who used ACADO Toolkit
 *    to generate this code. In particular, user dependent data of the code
 *    do not inherit the GNU LGPL license. On the other hand, parts of the
 *    generated code that are a direct copy of source code from the
 *    ACADO Toolkit or the software tools it is based on, remain, as derived
 *    work, automatically covered by the LGPL license.
 *    
 *    ACADO Toolkit is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *    
 */


#include "acado_common.h"


void acado_rhs(const real_t* in, real_t* out)
{
const real_t* xd = in;
const real_t* xa = in + 11;
const real_t* u = in + 15;
const real_t* od = in + 16;
/* Vector of auxiliary variables; number of elements: 17. */
real_t* a = acadoWorkspace.rhs_aux;

/* Compute intermediate quantities: */
a[0] = (cos(xd[2]));
a[1] = (sin(xd[2]));
a[2] = (sin(xd[2]));
a[3] = (cos(xd[2]));
a[4] = (cos(xa[3]));
a[5] = (sin(xa[3]));
a[6] = (sin(xa[3]));
a[7] = (cos(xa[3]));
a[8] = (sin(xa[3]));
a[9] = (cos(xa[3]));
a[10] = (cos(xd[6]));
a[11] = (sin(xd[6]));
a[12] = (sin(xd[6]));
a[13] = (cos(xd[6]));
a[14] = (atan(((xd[4]+((real_t)(1.6100000143051147e+00)*xd[5]))/(xd[3]+(real_t)(1.0000000000000000e-03)))));
a[15] = (atan(((((real_t)(0.0000000000000000e+00)-xd[4])+((real_t)(1.3999999761581421e+00)*xd[5]))/(xd[3]+(real_t)(1.0000000000000000e-03)))));
a[16] = (sqrt(((od[0]+(real_t)(1.0000000000000000e+00))/(real_t)(2.0000000000000000e+00))));

/* Compute outputs: */
out[0] = ((xd[3]*a[0])-(xd[4]*a[1]));
out[1] = ((xd[3]*a[2])+(xd[4]*a[3]));
out[2] = xd[5];
out[3] = ((((((real_t)(1.8450000000000000e+03)*od[1])*a[4])+(((real_t)(1.8450000000000000e+03)*xd[4])*xd[5]))-(xa[0]*a[5]))/(real_t)(1.8450000000000000e+03));
out[4] = (((((((real_t)(-1.8450000000000000e+03)*xd[3])*xd[5])+(((real_t)(1.8450000000000000e+03)*od[1])*a[6]))+(xa[0]*a[7]))+xa[1])/(real_t)(1.8450000000000000e+03));
out[5] = ((((real_t)(1.6100000143051147e+00)*((((real_t)(1.8450000000000000e+03)*od[1])*a[8])+(xa[0]*a[9])))-((real_t)(1.3999999761581421e+00)*xa[1]))/(real_t)(3.2340000000000000e+03));
out[6] = (xd[5]-((od[2]/((real_t)(1.0000000000000000e+00)-(od[2]*xd[7])))*((xd[3]*a[10])-(xd[4]*a[11]))));
out[7] = ((xd[3]*a[12])+(xd[4]*a[13]));
out[8] = xd[9];
out[9] = ((((((real_t)(0.0000000000000000e+00)-xa[2])/(real_t)(1.0000000149011612e-01))*xd[9])+(xd[10]/(real_t)(1.0000000149011612e-01)))+((xa[0]/(real_t)(3.0000000000000000e+02))/(real_t)(1.0000000149011612e-01)));
out[10] = (od[0]*u[0]);
out[11] = (xa[0]-((real_t)(9.4000000000000000e+03)*(xa[3]-a[14])));
out[12] = (xa[1]-((real_t)(1.1800000000000000e+04)*a[15]));
out[13] = (xa[2]-((real_t)(6.4999997615814209e-01)*a[16]));
out[14] = (xa[3]-(xd[8]/(real_t)(7.7142858505249023e+00)));
}



void acado_diffs(const real_t* in, real_t* out)
{
const real_t* xd = in;
const real_t* xa = in + 11;
const real_t* od = in + 16;
/* Vector of auxiliary variables; number of elements: 49. */
real_t* a = acadoWorkspace.rhs_aux;

/* Compute intermediate quantities: */
a[0] = ((real_t)(-1.0000000000000000e+00)*(sin(xd[2])));
a[1] = (cos(xd[2]));
a[2] = (cos(xd[2]));
a[3] = (sin(xd[2]));
a[4] = (cos(xd[2]));
a[5] = ((real_t)(-1.0000000000000000e+00)*(sin(xd[2])));
a[6] = (sin(xd[2]));
a[7] = (cos(xd[2]));
a[8] = ((real_t)(1.0000000000000000e+00)/(real_t)(1.8450000000000000e+03));
a[9] = (sin(xa[3]));
a[10] = ((real_t)(1.0000000000000000e+00)/(real_t)(1.8450000000000000e+03));
a[11] = ((real_t)(-1.0000000000000000e+00)*(sin(xa[3])));
a[12] = (cos(xa[3]));
a[13] = ((real_t)(1.0000000000000000e+00)/(real_t)(1.8450000000000000e+03));
a[14] = (cos(xa[3]));
a[15] = ((real_t)(1.0000000000000000e+00)/(real_t)(1.8450000000000000e+03));
a[16] = (cos(xa[3]));
a[17] = ((real_t)(-1.0000000000000000e+00)*(sin(xa[3])));
a[18] = (cos(xa[3]));
a[19] = ((real_t)(1.0000000000000000e+00)/(real_t)(3.2340000000000000e+03));
a[20] = (cos(xa[3]));
a[21] = ((real_t)(-1.0000000000000000e+00)*(sin(xa[3])));
a[22] = (cos(xd[6]));
a[23] = (sin(xd[6]));
a[24] = ((real_t)(-1.0000000000000000e+00)*(sin(xd[6])));
a[25] = (cos(xd[6]));
a[26] = ((real_t)(1.0000000000000000e+00)/((real_t)(1.0000000000000000e+00)-(od[2]*xd[7])));
a[27] = (a[26]*a[26]);
a[28] = (sin(xd[6]));
a[29] = (cos(xd[6]));
a[30] = (cos(xd[6]));
a[31] = ((real_t)(-1.0000000000000000e+00)*(sin(xd[6])));
a[32] = ((real_t)(1.0000000000000000e+00)/(real_t)(1.0000000149011612e-01));
a[33] = ((real_t)(1.0000000000000000e+00)/(real_t)(3.0000000000000000e+02));
a[34] = ((real_t)(1.0000000000000000e+00)/(real_t)(1.0000000149011612e-01));
a[35] = ((real_t)(1.0000000000000000e+00)/(real_t)(1.0000000149011612e-01));
a[36] = ((real_t)(1.0000000000000000e+00)/(xd[3]+(real_t)(1.0000000000000000e-03)));
a[37] = (a[36]*a[36]);
a[38] = ((real_t)(1.0000000000000000e+00)/((real_t)(1.0000000000000000e+00)+(pow(((xd[4]+((real_t)(1.6100000143051147e+00)*xd[5]))/(xd[3]+(real_t)(1.0000000000000000e-03))),2))));
a[39] = (((real_t)(0.0000000000000000e+00)-((xd[4]+((real_t)(1.6100000143051147e+00)*xd[5]))*a[37]))*a[38]);
a[40] = (a[36]*a[38]);
a[41] = (((real_t)(1.6100000143051147e+00)*a[36])*a[38]);
a[42] = ((real_t)(1.0000000000000000e+00)/(xd[3]+(real_t)(1.0000000000000000e-03)));
a[43] = (a[42]*a[42]);
a[44] = ((real_t)(1.0000000000000000e+00)/((real_t)(1.0000000000000000e+00)+(pow(((((real_t)(0.0000000000000000e+00)-xd[4])+((real_t)(1.3999999761581421e+00)*xd[5]))/(xd[3]+(real_t)(1.0000000000000000e-03))),2))));
a[45] = (((real_t)(0.0000000000000000e+00)-((((real_t)(0.0000000000000000e+00)-xd[4])+((real_t)(1.3999999761581421e+00)*xd[5]))*a[43]))*a[44]);
a[46] = ((((real_t)(0.0000000000000000e+00)-(real_t)(1.0000000000000000e+00))*a[42])*a[44]);
a[47] = (((real_t)(1.3999999761581421e+00)*a[42])*a[44]);
a[48] = ((real_t)(1.0000000000000000e+00)/(real_t)(7.7142858505249023e+00));

/* Compute outputs: */
out[0] = (real_t)(0.0000000000000000e+00);
out[1] = (real_t)(0.0000000000000000e+00);
out[2] = ((xd[3]*a[0])-(xd[4]*a[1]));
out[3] = a[2];
out[4] = ((real_t)(0.0000000000000000e+00)-a[3]);
out[5] = (real_t)(0.0000000000000000e+00);
out[6] = (real_t)(0.0000000000000000e+00);
out[7] = (real_t)(0.0000000000000000e+00);
out[8] = (real_t)(0.0000000000000000e+00);
out[9] = (real_t)(0.0000000000000000e+00);
out[10] = (real_t)(0.0000000000000000e+00);
out[11] = (real_t)(0.0000000000000000e+00);
out[12] = (real_t)(0.0000000000000000e+00);
out[13] = (real_t)(0.0000000000000000e+00);
out[14] = (real_t)(0.0000000000000000e+00);
out[15] = (real_t)(0.0000000000000000e+00);
out[16] = (real_t)(0.0000000000000000e+00);
out[17] = (real_t)(0.0000000000000000e+00);
out[18] = ((xd[3]*a[4])+(xd[4]*a[5]));
out[19] = a[6];
out[20] = a[7];
out[21] = (real_t)(0.0000000000000000e+00);
out[22] = (real_t)(0.0000000000000000e+00);
out[23] = (real_t)(0.0000000000000000e+00);
out[24] = (real_t)(0.0000000000000000e+00);
out[25] = (real_t)(0.0000000000000000e+00);
out[26] = (real_t)(0.0000000000000000e+00);
out[27] = (real_t)(0.0000000000000000e+00);
out[28] = (real_t)(0.0000000000000000e+00);
out[29] = (real_t)(0.0000000000000000e+00);
out[30] = (real_t)(0.0000000000000000e+00);
out[31] = (real_t)(0.0000000000000000e+00);
out[32] = (real_t)(0.0000000000000000e+00);
out[33] = (real_t)(0.0000000000000000e+00);
out[34] = (real_t)(0.0000000000000000e+00);
out[35] = (real_t)(0.0000000000000000e+00);
out[36] = (real_t)(0.0000000000000000e+00);
out[37] = (real_t)(1.0000000000000000e+00);
out[38] = (real_t)(0.0000000000000000e+00);
out[39] = (real_t)(0.0000000000000000e+00);
out[40] = (real_t)(0.0000000000000000e+00);
out[41] = (real_t)(0.0000000000000000e+00);
out[42] = (real_t)(0.0000000000000000e+00);
out[43] = (real_t)(0.0000000000000000e+00);
out[44] = (real_t)(0.0000000000000000e+00);
out[45] = (real_t)(0.0000000000000000e+00);
out[46] = (real_t)(0.0000000000000000e+00);
out[47] = (real_t)(0.0000000000000000e+00);
out[48] = (real_t)(0.0000000000000000e+00);
out[49] = (real_t)(0.0000000000000000e+00);
out[50] = (real_t)(0.0000000000000000e+00);
out[51] = (real_t)(0.0000000000000000e+00);
out[52] = (((real_t)(1.8450000000000000e+03)*xd[5])*a[8]);
out[53] = (((real_t)(1.8450000000000000e+03)*xd[4])*a[8]);
out[54] = (real_t)(0.0000000000000000e+00);
out[55] = (real_t)(0.0000000000000000e+00);
out[56] = (real_t)(0.0000000000000000e+00);
out[57] = (real_t)(0.0000000000000000e+00);
out[58] = (real_t)(0.0000000000000000e+00);
out[59] = (((real_t)(0.0000000000000000e+00)-a[9])*a[10]);
out[60] = (real_t)(0.0000000000000000e+00);
out[61] = (real_t)(0.0000000000000000e+00);
out[62] = (((((real_t)(1.8450000000000000e+03)*od[1])*a[11])-(xa[0]*a[12]))*a[10]);
out[63] = (real_t)(0.0000000000000000e+00);
out[64] = (real_t)(0.0000000000000000e+00);
out[65] = (real_t)(0.0000000000000000e+00);
out[66] = (real_t)(0.0000000000000000e+00);
out[67] = (((real_t)(-1.8450000000000000e+03)*xd[5])*a[13]);
out[68] = (real_t)(0.0000000000000000e+00);
out[69] = (((real_t)(-1.8450000000000000e+03)*xd[3])*a[13]);
out[70] = (real_t)(0.0000000000000000e+00);
out[71] = (real_t)(0.0000000000000000e+00);
out[72] = (real_t)(0.0000000000000000e+00);
out[73] = (real_t)(0.0000000000000000e+00);
out[74] = (real_t)(0.0000000000000000e+00);
out[75] = (a[14]*a[15]);
out[76] = a[15];
out[77] = (real_t)(0.0000000000000000e+00);
out[78] = (((((real_t)(1.8450000000000000e+03)*od[1])*a[16])+(xa[0]*a[17]))*a[15]);
out[79] = (real_t)(0.0000000000000000e+00);
out[80] = (real_t)(0.0000000000000000e+00);
out[81] = (real_t)(0.0000000000000000e+00);
out[82] = (real_t)(0.0000000000000000e+00);
out[83] = (real_t)(0.0000000000000000e+00);
out[84] = (real_t)(0.0000000000000000e+00);
out[85] = (real_t)(0.0000000000000000e+00);
out[86] = (real_t)(0.0000000000000000e+00);
out[87] = (real_t)(0.0000000000000000e+00);
out[88] = (real_t)(0.0000000000000000e+00);
out[89] = (real_t)(0.0000000000000000e+00);
out[90] = (real_t)(0.0000000000000000e+00);
out[91] = (((real_t)(1.6100000143051147e+00)*a[18])*a[19]);
out[92] = (((real_t)(0.0000000000000000e+00)-(real_t)(1.3999999761581421e+00))*a[19]);
out[93] = (real_t)(0.0000000000000000e+00);
out[94] = (((real_t)(1.6100000143051147e+00)*((((real_t)(1.8450000000000000e+03)*od[1])*a[20])+(xa[0]*a[21])))*a[19]);
out[95] = (real_t)(0.0000000000000000e+00);
out[96] = (real_t)(0.0000000000000000e+00);
out[97] = (real_t)(0.0000000000000000e+00);
out[98] = (real_t)(0.0000000000000000e+00);
out[99] = ((real_t)(0.0000000000000000e+00)-((od[2]/((real_t)(1.0000000000000000e+00)-(od[2]*xd[7])))*a[22]));
out[100] = ((real_t)(0.0000000000000000e+00)-((od[2]/((real_t)(1.0000000000000000e+00)-(od[2]*xd[7])))*((real_t)(0.0000000000000000e+00)-a[23])));
out[101] = (real_t)(1.0000000000000000e+00);
out[102] = ((real_t)(0.0000000000000000e+00)-((od[2]/((real_t)(1.0000000000000000e+00)-(od[2]*xd[7])))*((xd[3]*a[24])-(xd[4]*a[25]))));
out[103] = ((real_t)(0.0000000000000000e+00)-(((real_t)(0.0000000000000000e+00)-((od[2]*((real_t)(0.0000000000000000e+00)-od[2]))*a[27]))*((xd[3]*a[22])-(xd[4]*a[23]))));
out[104] = (real_t)(0.0000000000000000e+00);
out[105] = (real_t)(0.0000000000000000e+00);
out[106] = (real_t)(0.0000000000000000e+00);
out[107] = (real_t)(0.0000000000000000e+00);
out[108] = (real_t)(0.0000000000000000e+00);
out[109] = (real_t)(0.0000000000000000e+00);
out[110] = (real_t)(0.0000000000000000e+00);
out[111] = (real_t)(0.0000000000000000e+00);
out[112] = (real_t)(0.0000000000000000e+00);
out[113] = (real_t)(0.0000000000000000e+00);
out[114] = (real_t)(0.0000000000000000e+00);
out[115] = a[28];
out[116] = a[29];
out[117] = (real_t)(0.0000000000000000e+00);
out[118] = ((xd[3]*a[30])+(xd[4]*a[31]));
out[119] = (real_t)(0.0000000000000000e+00);
out[120] = (real_t)(0.0000000000000000e+00);
out[121] = (real_t)(0.0000000000000000e+00);
out[122] = (real_t)(0.0000000000000000e+00);
out[123] = (real_t)(0.0000000000000000e+00);
out[124] = (real_t)(0.0000000000000000e+00);
out[125] = (real_t)(0.0000000000000000e+00);
out[126] = (real_t)(0.0000000000000000e+00);
out[127] = (real_t)(0.0000000000000000e+00);
out[128] = (real_t)(0.0000000000000000e+00);
out[129] = (real_t)(0.0000000000000000e+00);
out[130] = (real_t)(0.0000000000000000e+00);
out[131] = (real_t)(0.0000000000000000e+00);
out[132] = (real_t)(0.0000000000000000e+00);
out[133] = (real_t)(0.0000000000000000e+00);
out[134] = (real_t)(0.0000000000000000e+00);
out[135] = (real_t)(0.0000000000000000e+00);
out[136] = (real_t)(0.0000000000000000e+00);
out[137] = (real_t)(1.0000000000000000e+00);
out[138] = (real_t)(0.0000000000000000e+00);
out[139] = (real_t)(0.0000000000000000e+00);
out[140] = (real_t)(0.0000000000000000e+00);
out[141] = (real_t)(0.0000000000000000e+00);
out[142] = (real_t)(0.0000000000000000e+00);
out[143] = (real_t)(0.0000000000000000e+00);
out[144] = (real_t)(0.0000000000000000e+00);
out[145] = (real_t)(0.0000000000000000e+00);
out[146] = (real_t)(0.0000000000000000e+00);
out[147] = (real_t)(0.0000000000000000e+00);
out[148] = (real_t)(0.0000000000000000e+00);
out[149] = (real_t)(0.0000000000000000e+00);
out[150] = (real_t)(0.0000000000000000e+00);
out[151] = (real_t)(0.0000000000000000e+00);
out[152] = (real_t)(0.0000000000000000e+00);
out[153] = (((real_t)(0.0000000000000000e+00)-xa[2])/(real_t)(1.0000000149011612e-01));
out[154] = a[32];
out[155] = (a[33]*a[34]);
out[156] = (real_t)(0.0000000000000000e+00);
out[157] = ((((real_t)(0.0000000000000000e+00)-(real_t)(1.0000000000000000e+00))*a[35])*xd[9]);
out[158] = (real_t)(0.0000000000000000e+00);
out[159] = (real_t)(0.0000000000000000e+00);
out[160] = (real_t)(0.0000000000000000e+00);
out[161] = (real_t)(0.0000000000000000e+00);
out[162] = (real_t)(0.0000000000000000e+00);
out[163] = (real_t)(0.0000000000000000e+00);
out[164] = (real_t)(0.0000000000000000e+00);
out[165] = (real_t)(0.0000000000000000e+00);
out[166] = (real_t)(0.0000000000000000e+00);
out[167] = (real_t)(0.0000000000000000e+00);
out[168] = (real_t)(0.0000000000000000e+00);
out[169] = (real_t)(0.0000000000000000e+00);
out[170] = (real_t)(0.0000000000000000e+00);
out[171] = (real_t)(0.0000000000000000e+00);
out[172] = (real_t)(0.0000000000000000e+00);
out[173] = (real_t)(0.0000000000000000e+00);
out[174] = (real_t)(0.0000000000000000e+00);
out[175] = od[0];
out[176] = (real_t)(0.0000000000000000e+00);
out[177] = (real_t)(0.0000000000000000e+00);
out[178] = (real_t)(0.0000000000000000e+00);
out[179] = ((real_t)(0.0000000000000000e+00)-((real_t)(9.4000000000000000e+03)*((real_t)(0.0000000000000000e+00)-a[39])));
out[180] = ((real_t)(0.0000000000000000e+00)-((real_t)(9.4000000000000000e+03)*((real_t)(0.0000000000000000e+00)-a[40])));
out[181] = ((real_t)(0.0000000000000000e+00)-((real_t)(9.4000000000000000e+03)*((real_t)(0.0000000000000000e+00)-a[41])));
out[182] = (real_t)(0.0000000000000000e+00);
out[183] = (real_t)(0.0000000000000000e+00);
out[184] = (real_t)(0.0000000000000000e+00);
out[185] = (real_t)(0.0000000000000000e+00);
out[186] = (real_t)(0.0000000000000000e+00);
out[187] = (real_t)(1.0000000000000000e+00);
out[188] = (real_t)(0.0000000000000000e+00);
out[189] = (real_t)(0.0000000000000000e+00);
out[190] = ((real_t)(0.0000000000000000e+00)-(real_t)(9.4000000000000000e+03));
out[191] = (real_t)(0.0000000000000000e+00);
out[192] = (real_t)(0.0000000000000000e+00);
out[193] = (real_t)(0.0000000000000000e+00);
out[194] = (real_t)(0.0000000000000000e+00);
out[195] = ((real_t)(0.0000000000000000e+00)-((real_t)(1.1800000000000000e+04)*a[45]));
out[196] = ((real_t)(0.0000000000000000e+00)-((real_t)(1.1800000000000000e+04)*a[46]));
out[197] = ((real_t)(0.0000000000000000e+00)-((real_t)(1.1800000000000000e+04)*a[47]));
out[198] = (real_t)(0.0000000000000000e+00);
out[199] = (real_t)(0.0000000000000000e+00);
out[200] = (real_t)(0.0000000000000000e+00);
out[201] = (real_t)(0.0000000000000000e+00);
out[202] = (real_t)(0.0000000000000000e+00);
out[203] = (real_t)(0.0000000000000000e+00);
out[204] = (real_t)(1.0000000000000000e+00);
out[205] = (real_t)(0.0000000000000000e+00);
out[206] = (real_t)(0.0000000000000000e+00);
out[207] = (real_t)(0.0000000000000000e+00);
out[208] = (real_t)(0.0000000000000000e+00);
out[209] = (real_t)(0.0000000000000000e+00);
out[210] = (real_t)(0.0000000000000000e+00);
out[211] = (real_t)(0.0000000000000000e+00);
out[212] = (real_t)(0.0000000000000000e+00);
out[213] = (real_t)(0.0000000000000000e+00);
out[214] = (real_t)(0.0000000000000000e+00);
out[215] = (real_t)(0.0000000000000000e+00);
out[216] = (real_t)(0.0000000000000000e+00);
out[217] = (real_t)(0.0000000000000000e+00);
out[218] = (real_t)(0.0000000000000000e+00);
out[219] = (real_t)(0.0000000000000000e+00);
out[220] = (real_t)(0.0000000000000000e+00);
out[221] = (real_t)(1.0000000000000000e+00);
out[222] = (real_t)(0.0000000000000000e+00);
out[223] = (real_t)(0.0000000000000000e+00);
out[224] = (real_t)(0.0000000000000000e+00);
out[225] = (real_t)(0.0000000000000000e+00);
out[226] = (real_t)(0.0000000000000000e+00);
out[227] = (real_t)(0.0000000000000000e+00);
out[228] = (real_t)(0.0000000000000000e+00);
out[229] = (real_t)(0.0000000000000000e+00);
out[230] = (real_t)(0.0000000000000000e+00);
out[231] = (real_t)(0.0000000000000000e+00);
out[232] = ((real_t)(0.0000000000000000e+00)-a[48]);
out[233] = (real_t)(0.0000000000000000e+00);
out[234] = (real_t)(0.0000000000000000e+00);
out[235] = (real_t)(0.0000000000000000e+00);
out[236] = (real_t)(0.0000000000000000e+00);
out[237] = (real_t)(0.0000000000000000e+00);
out[238] = (real_t)(1.0000000000000000e+00);
out[239] = (real_t)(0.0000000000000000e+00);
}



void acado_solve_dim15_triangular( real_t* const A, real_t* const b )
{

b[14] = b[14]/A[224];
b[13] -= + A[209]*b[14];
b[13] = b[13]/A[208];
b[12] -= + A[194]*b[14];
b[12] -= + A[193]*b[13];
b[12] = b[12]/A[192];
b[11] -= + A[179]*b[14];
b[11] -= + A[178]*b[13];
b[11] -= + A[177]*b[12];
b[11] = b[11]/A[176];
b[10] -= + A[164]*b[14];
b[10] -= + A[163]*b[13];
b[10] -= + A[162]*b[12];
b[10] -= + A[161]*b[11];
b[10] = b[10]/A[160];
b[9] -= + A[149]*b[14];
b[9] -= + A[148]*b[13];
b[9] -= + A[147]*b[12];
b[9] -= + A[146]*b[11];
b[9] -= + A[145]*b[10];
b[9] = b[9]/A[144];
b[8] -= + A[134]*b[14];
b[8] -= + A[133]*b[13];
b[8] -= + A[132]*b[12];
b[8] -= + A[131]*b[11];
b[8] -= + A[130]*b[10];
b[8] -= + A[129]*b[9];
b[8] = b[8]/A[128];
b[7] -= + A[119]*b[14];
b[7] -= + A[118]*b[13];
b[7] -= + A[117]*b[12];
b[7] -= + A[116]*b[11];
b[7] -= + A[115]*b[10];
b[7] -= + A[114]*b[9];
b[7] -= + A[113]*b[8];
b[7] = b[7]/A[112];
b[6] -= + A[104]*b[14];
b[6] -= + A[103]*b[13];
b[6] -= + A[102]*b[12];
b[6] -= + A[101]*b[11];
b[6] -= + A[100]*b[10];
b[6] -= + A[99]*b[9];
b[6] -= + A[98]*b[8];
b[6] -= + A[97]*b[7];
b[6] = b[6]/A[96];
b[5] -= + A[89]*b[14];
b[5] -= + A[88]*b[13];
b[5] -= + A[87]*b[12];
b[5] -= + A[86]*b[11];
b[5] -= + A[85]*b[10];
b[5] -= + A[84]*b[9];
b[5] -= + A[83]*b[8];
b[5] -= + A[82]*b[7];
b[5] -= + A[81]*b[6];
b[5] = b[5]/A[80];
b[4] -= + A[74]*b[14];
b[4] -= + A[73]*b[13];
b[4] -= + A[72]*b[12];
b[4] -= + A[71]*b[11];
b[4] -= + A[70]*b[10];
b[4] -= + A[69]*b[9];
b[4] -= + A[68]*b[8];
b[4] -= + A[67]*b[7];
b[4] -= + A[66]*b[6];
b[4] -= + A[65]*b[5];
b[4] = b[4]/A[64];
b[3] -= + A[59]*b[14];
b[3] -= + A[58]*b[13];
b[3] -= + A[57]*b[12];
b[3] -= + A[56]*b[11];
b[3] -= + A[55]*b[10];
b[3] -= + A[54]*b[9];
b[3] -= + A[53]*b[8];
b[3] -= + A[52]*b[7];
b[3] -= + A[51]*b[6];
b[3] -= + A[50]*b[5];
b[3] -= + A[49]*b[4];
b[3] = b[3]/A[48];
b[2] -= + A[44]*b[14];
b[2] -= + A[43]*b[13];
b[2] -= + A[42]*b[12];
b[2] -= + A[41]*b[11];
b[2] -= + A[40]*b[10];
b[2] -= + A[39]*b[9];
b[2] -= + A[38]*b[8];
b[2] -= + A[37]*b[7];
b[2] -= + A[36]*b[6];
b[2] -= + A[35]*b[5];
b[2] -= + A[34]*b[4];
b[2] -= + A[33]*b[3];
b[2] = b[2]/A[32];
b[1] -= + A[29]*b[14];
b[1] -= + A[28]*b[13];
b[1] -= + A[27]*b[12];
b[1] -= + A[26]*b[11];
b[1] -= + A[25]*b[10];
b[1] -= + A[24]*b[9];
b[1] -= + A[23]*b[8];
b[1] -= + A[22]*b[7];
b[1] -= + A[21]*b[6];
b[1] -= + A[20]*b[5];
b[1] -= + A[19]*b[4];
b[1] -= + A[18]*b[3];
b[1] -= + A[17]*b[2];
b[1] = b[1]/A[16];
b[0] -= + A[14]*b[14];
b[0] -= + A[13]*b[13];
b[0] -= + A[12]*b[12];
b[0] -= + A[11]*b[11];
b[0] -= + A[10]*b[10];
b[0] -= + A[9]*b[9];
b[0] -= + A[8]*b[8];
b[0] -= + A[7]*b[7];
b[0] -= + A[6]*b[6];
b[0] -= + A[5]*b[5];
b[0] -= + A[4]*b[4];
b[0] -= + A[3]*b[3];
b[0] -= + A[2]*b[2];
b[0] -= + A[1]*b[1];
b[0] = b[0]/A[0];
}

real_t acado_solve_dim15_system( real_t* const A, real_t* const b, int* const rk_perm )
{
real_t det;

int i;
int j;
int k;

int indexMax;

int intSwap;

real_t valueMax;

real_t temp;

for (i = 0; i < 15; ++i)
{
rk_perm[i] = i;
}
det = 1.0000000000000000e+00;
for( i=0; i < (14); i++ ) {
	indexMax = i;
	valueMax = fabs(A[i*15+i]);
	for( j=(i+1); j < 15; j++ ) {
		temp = fabs(A[j*15+i]);
		if( temp > valueMax ) {
			indexMax = j;
			valueMax = temp;
		}
	}
	if( indexMax > i ) {
for (k = 0; k < 15; ++k)
{
	acadoWorkspace.rk_dim15_swap = A[i*15+k];
	A[i*15+k] = A[indexMax*15+k];
	A[indexMax*15+k] = acadoWorkspace.rk_dim15_swap;
}
	acadoWorkspace.rk_dim15_swap = b[i];
	b[i] = b[indexMax];
	b[indexMax] = acadoWorkspace.rk_dim15_swap;
	intSwap = rk_perm[i];
	rk_perm[i] = rk_perm[indexMax];
	rk_perm[indexMax] = intSwap;
	}
	det *= A[i*15+i];
	for( j=i+1; j < 15; j++ ) {
		A[j*15+i] = -A[j*15+i]/A[i*15+i];
		for( k=i+1; k < 15; k++ ) {
			A[j*15+k] += A[j*15+i] * A[i*15+k];
		}
		b[j] += A[j*15+i] * b[i];
	}
}
det *= A[224];
det = fabs(det);
acado_solve_dim15_triangular( A, b );
return det;
}

void acado_solve_dim15_system_reuse( real_t* const A, real_t* const b, int* const rk_perm )
{

acadoWorkspace.rk_dim15_bPerm[0] = b[rk_perm[0]];
acadoWorkspace.rk_dim15_bPerm[1] = b[rk_perm[1]];
acadoWorkspace.rk_dim15_bPerm[2] = b[rk_perm[2]];
acadoWorkspace.rk_dim15_bPerm[3] = b[rk_perm[3]];
acadoWorkspace.rk_dim15_bPerm[4] = b[rk_perm[4]];
acadoWorkspace.rk_dim15_bPerm[5] = b[rk_perm[5]];
acadoWorkspace.rk_dim15_bPerm[6] = b[rk_perm[6]];
acadoWorkspace.rk_dim15_bPerm[7] = b[rk_perm[7]];
acadoWorkspace.rk_dim15_bPerm[8] = b[rk_perm[8]];
acadoWorkspace.rk_dim15_bPerm[9] = b[rk_perm[9]];
acadoWorkspace.rk_dim15_bPerm[10] = b[rk_perm[10]];
acadoWorkspace.rk_dim15_bPerm[11] = b[rk_perm[11]];
acadoWorkspace.rk_dim15_bPerm[12] = b[rk_perm[12]];
acadoWorkspace.rk_dim15_bPerm[13] = b[rk_perm[13]];
acadoWorkspace.rk_dim15_bPerm[14] = b[rk_perm[14]];
acadoWorkspace.rk_dim15_bPerm[1] += A[15]*acadoWorkspace.rk_dim15_bPerm[0];

acadoWorkspace.rk_dim15_bPerm[2] += A[30]*acadoWorkspace.rk_dim15_bPerm[0];
acadoWorkspace.rk_dim15_bPerm[2] += A[31]*acadoWorkspace.rk_dim15_bPerm[1];

acadoWorkspace.rk_dim15_bPerm[3] += A[45]*acadoWorkspace.rk_dim15_bPerm[0];
acadoWorkspace.rk_dim15_bPerm[3] += A[46]*acadoWorkspace.rk_dim15_bPerm[1];
acadoWorkspace.rk_dim15_bPerm[3] += A[47]*acadoWorkspace.rk_dim15_bPerm[2];

acadoWorkspace.rk_dim15_bPerm[4] += A[60]*acadoWorkspace.rk_dim15_bPerm[0];
acadoWorkspace.rk_dim15_bPerm[4] += A[61]*acadoWorkspace.rk_dim15_bPerm[1];
acadoWorkspace.rk_dim15_bPerm[4] += A[62]*acadoWorkspace.rk_dim15_bPerm[2];
acadoWorkspace.rk_dim15_bPerm[4] += A[63]*acadoWorkspace.rk_dim15_bPerm[3];

acadoWorkspace.rk_dim15_bPerm[5] += A[75]*acadoWorkspace.rk_dim15_bPerm[0];
acadoWorkspace.rk_dim15_bPerm[5] += A[76]*acadoWorkspace.rk_dim15_bPerm[1];
acadoWorkspace.rk_dim15_bPerm[5] += A[77]*acadoWorkspace.rk_dim15_bPerm[2];
acadoWorkspace.rk_dim15_bPerm[5] += A[78]*acadoWorkspace.rk_dim15_bPerm[3];
acadoWorkspace.rk_dim15_bPerm[5] += A[79]*acadoWorkspace.rk_dim15_bPerm[4];

acadoWorkspace.rk_dim15_bPerm[6] += A[90]*acadoWorkspace.rk_dim15_bPerm[0];
acadoWorkspace.rk_dim15_bPerm[6] += A[91]*acadoWorkspace.rk_dim15_bPerm[1];
acadoWorkspace.rk_dim15_bPerm[6] += A[92]*acadoWorkspace.rk_dim15_bPerm[2];
acadoWorkspace.rk_dim15_bPerm[6] += A[93]*acadoWorkspace.rk_dim15_bPerm[3];
acadoWorkspace.rk_dim15_bPerm[6] += A[94]*acadoWorkspace.rk_dim15_bPerm[4];
acadoWorkspace.rk_dim15_bPerm[6] += A[95]*acadoWorkspace.rk_dim15_bPerm[5];

acadoWorkspace.rk_dim15_bPerm[7] += A[105]*acadoWorkspace.rk_dim15_bPerm[0];
acadoWorkspace.rk_dim15_bPerm[7] += A[106]*acadoWorkspace.rk_dim15_bPerm[1];
acadoWorkspace.rk_dim15_bPerm[7] += A[107]*acadoWorkspace.rk_dim15_bPerm[2];
acadoWorkspace.rk_dim15_bPerm[7] += A[108]*acadoWorkspace.rk_dim15_bPerm[3];
acadoWorkspace.rk_dim15_bPerm[7] += A[109]*acadoWorkspace.rk_dim15_bPerm[4];
acadoWorkspace.rk_dim15_bPerm[7] += A[110]*acadoWorkspace.rk_dim15_bPerm[5];
acadoWorkspace.rk_dim15_bPerm[7] += A[111]*acadoWorkspace.rk_dim15_bPerm[6];

acadoWorkspace.rk_dim15_bPerm[8] += A[120]*acadoWorkspace.rk_dim15_bPerm[0];
acadoWorkspace.rk_dim15_bPerm[8] += A[121]*acadoWorkspace.rk_dim15_bPerm[1];
acadoWorkspace.rk_dim15_bPerm[8] += A[122]*acadoWorkspace.rk_dim15_bPerm[2];
acadoWorkspace.rk_dim15_bPerm[8] += A[123]*acadoWorkspace.rk_dim15_bPerm[3];
acadoWorkspace.rk_dim15_bPerm[8] += A[124]*acadoWorkspace.rk_dim15_bPerm[4];
acadoWorkspace.rk_dim15_bPerm[8] += A[125]*acadoWorkspace.rk_dim15_bPerm[5];
acadoWorkspace.rk_dim15_bPerm[8] += A[126]*acadoWorkspace.rk_dim15_bPerm[6];
acadoWorkspace.rk_dim15_bPerm[8] += A[127]*acadoWorkspace.rk_dim15_bPerm[7];

acadoWorkspace.rk_dim15_bPerm[9] += A[135]*acadoWorkspace.rk_dim15_bPerm[0];
acadoWorkspace.rk_dim15_bPerm[9] += A[136]*acadoWorkspace.rk_dim15_bPerm[1];
acadoWorkspace.rk_dim15_bPerm[9] += A[137]*acadoWorkspace.rk_dim15_bPerm[2];
acadoWorkspace.rk_dim15_bPerm[9] += A[138]*acadoWorkspace.rk_dim15_bPerm[3];
acadoWorkspace.rk_dim15_bPerm[9] += A[139]*acadoWorkspace.rk_dim15_bPerm[4];
acadoWorkspace.rk_dim15_bPerm[9] += A[140]*acadoWorkspace.rk_dim15_bPerm[5];
acadoWorkspace.rk_dim15_bPerm[9] += A[141]*acadoWorkspace.rk_dim15_bPerm[6];
acadoWorkspace.rk_dim15_bPerm[9] += A[142]*acadoWorkspace.rk_dim15_bPerm[7];
acadoWorkspace.rk_dim15_bPerm[9] += A[143]*acadoWorkspace.rk_dim15_bPerm[8];

acadoWorkspace.rk_dim15_bPerm[10] += A[150]*acadoWorkspace.rk_dim15_bPerm[0];
acadoWorkspace.rk_dim15_bPerm[10] += A[151]*acadoWorkspace.rk_dim15_bPerm[1];
acadoWorkspace.rk_dim15_bPerm[10] += A[152]*acadoWorkspace.rk_dim15_bPerm[2];
acadoWorkspace.rk_dim15_bPerm[10] += A[153]*acadoWorkspace.rk_dim15_bPerm[3];
acadoWorkspace.rk_dim15_bPerm[10] += A[154]*acadoWorkspace.rk_dim15_bPerm[4];
acadoWorkspace.rk_dim15_bPerm[10] += A[155]*acadoWorkspace.rk_dim15_bPerm[5];
acadoWorkspace.rk_dim15_bPerm[10] += A[156]*acadoWorkspace.rk_dim15_bPerm[6];
acadoWorkspace.rk_dim15_bPerm[10] += A[157]*acadoWorkspace.rk_dim15_bPerm[7];
acadoWorkspace.rk_dim15_bPerm[10] += A[158]*acadoWorkspace.rk_dim15_bPerm[8];
acadoWorkspace.rk_dim15_bPerm[10] += A[159]*acadoWorkspace.rk_dim15_bPerm[9];

acadoWorkspace.rk_dim15_bPerm[11] += A[165]*acadoWorkspace.rk_dim15_bPerm[0];
acadoWorkspace.rk_dim15_bPerm[11] += A[166]*acadoWorkspace.rk_dim15_bPerm[1];
acadoWorkspace.rk_dim15_bPerm[11] += A[167]*acadoWorkspace.rk_dim15_bPerm[2];
acadoWorkspace.rk_dim15_bPerm[11] += A[168]*acadoWorkspace.rk_dim15_bPerm[3];
acadoWorkspace.rk_dim15_bPerm[11] += A[169]*acadoWorkspace.rk_dim15_bPerm[4];
acadoWorkspace.rk_dim15_bPerm[11] += A[170]*acadoWorkspace.rk_dim15_bPerm[5];
acadoWorkspace.rk_dim15_bPerm[11] += A[171]*acadoWorkspace.rk_dim15_bPerm[6];
acadoWorkspace.rk_dim15_bPerm[11] += A[172]*acadoWorkspace.rk_dim15_bPerm[7];
acadoWorkspace.rk_dim15_bPerm[11] += A[173]*acadoWorkspace.rk_dim15_bPerm[8];
acadoWorkspace.rk_dim15_bPerm[11] += A[174]*acadoWorkspace.rk_dim15_bPerm[9];
acadoWorkspace.rk_dim15_bPerm[11] += A[175]*acadoWorkspace.rk_dim15_bPerm[10];

acadoWorkspace.rk_dim15_bPerm[12] += A[180]*acadoWorkspace.rk_dim15_bPerm[0];
acadoWorkspace.rk_dim15_bPerm[12] += A[181]*acadoWorkspace.rk_dim15_bPerm[1];
acadoWorkspace.rk_dim15_bPerm[12] += A[182]*acadoWorkspace.rk_dim15_bPerm[2];
acadoWorkspace.rk_dim15_bPerm[12] += A[183]*acadoWorkspace.rk_dim15_bPerm[3];
acadoWorkspace.rk_dim15_bPerm[12] += A[184]*acadoWorkspace.rk_dim15_bPerm[4];
acadoWorkspace.rk_dim15_bPerm[12] += A[185]*acadoWorkspace.rk_dim15_bPerm[5];
acadoWorkspace.rk_dim15_bPerm[12] += A[186]*acadoWorkspace.rk_dim15_bPerm[6];
acadoWorkspace.rk_dim15_bPerm[12] += A[187]*acadoWorkspace.rk_dim15_bPerm[7];
acadoWorkspace.rk_dim15_bPerm[12] += A[188]*acadoWorkspace.rk_dim15_bPerm[8];
acadoWorkspace.rk_dim15_bPerm[12] += A[189]*acadoWorkspace.rk_dim15_bPerm[9];
acadoWorkspace.rk_dim15_bPerm[12] += A[190]*acadoWorkspace.rk_dim15_bPerm[10];
acadoWorkspace.rk_dim15_bPerm[12] += A[191]*acadoWorkspace.rk_dim15_bPerm[11];

acadoWorkspace.rk_dim15_bPerm[13] += A[195]*acadoWorkspace.rk_dim15_bPerm[0];
acadoWorkspace.rk_dim15_bPerm[13] += A[196]*acadoWorkspace.rk_dim15_bPerm[1];
acadoWorkspace.rk_dim15_bPerm[13] += A[197]*acadoWorkspace.rk_dim15_bPerm[2];
acadoWorkspace.rk_dim15_bPerm[13] += A[198]*acadoWorkspace.rk_dim15_bPerm[3];
acadoWorkspace.rk_dim15_bPerm[13] += A[199]*acadoWorkspace.rk_dim15_bPerm[4];
acadoWorkspace.rk_dim15_bPerm[13] += A[200]*acadoWorkspace.rk_dim15_bPerm[5];
acadoWorkspace.rk_dim15_bPerm[13] += A[201]*acadoWorkspace.rk_dim15_bPerm[6];
acadoWorkspace.rk_dim15_bPerm[13] += A[202]*acadoWorkspace.rk_dim15_bPerm[7];
acadoWorkspace.rk_dim15_bPerm[13] += A[203]*acadoWorkspace.rk_dim15_bPerm[8];
acadoWorkspace.rk_dim15_bPerm[13] += A[204]*acadoWorkspace.rk_dim15_bPerm[9];
acadoWorkspace.rk_dim15_bPerm[13] += A[205]*acadoWorkspace.rk_dim15_bPerm[10];
acadoWorkspace.rk_dim15_bPerm[13] += A[206]*acadoWorkspace.rk_dim15_bPerm[11];
acadoWorkspace.rk_dim15_bPerm[13] += A[207]*acadoWorkspace.rk_dim15_bPerm[12];

acadoWorkspace.rk_dim15_bPerm[14] += A[210]*acadoWorkspace.rk_dim15_bPerm[0];
acadoWorkspace.rk_dim15_bPerm[14] += A[211]*acadoWorkspace.rk_dim15_bPerm[1];
acadoWorkspace.rk_dim15_bPerm[14] += A[212]*acadoWorkspace.rk_dim15_bPerm[2];
acadoWorkspace.rk_dim15_bPerm[14] += A[213]*acadoWorkspace.rk_dim15_bPerm[3];
acadoWorkspace.rk_dim15_bPerm[14] += A[214]*acadoWorkspace.rk_dim15_bPerm[4];
acadoWorkspace.rk_dim15_bPerm[14] += A[215]*acadoWorkspace.rk_dim15_bPerm[5];
acadoWorkspace.rk_dim15_bPerm[14] += A[216]*acadoWorkspace.rk_dim15_bPerm[6];
acadoWorkspace.rk_dim15_bPerm[14] += A[217]*acadoWorkspace.rk_dim15_bPerm[7];
acadoWorkspace.rk_dim15_bPerm[14] += A[218]*acadoWorkspace.rk_dim15_bPerm[8];
acadoWorkspace.rk_dim15_bPerm[14] += A[219]*acadoWorkspace.rk_dim15_bPerm[9];
acadoWorkspace.rk_dim15_bPerm[14] += A[220]*acadoWorkspace.rk_dim15_bPerm[10];
acadoWorkspace.rk_dim15_bPerm[14] += A[221]*acadoWorkspace.rk_dim15_bPerm[11];
acadoWorkspace.rk_dim15_bPerm[14] += A[222]*acadoWorkspace.rk_dim15_bPerm[12];
acadoWorkspace.rk_dim15_bPerm[14] += A[223]*acadoWorkspace.rk_dim15_bPerm[13];


acado_solve_dim15_triangular( A, acadoWorkspace.rk_dim15_bPerm );
b[0] = acadoWorkspace.rk_dim15_bPerm[0];
b[1] = acadoWorkspace.rk_dim15_bPerm[1];
b[2] = acadoWorkspace.rk_dim15_bPerm[2];
b[3] = acadoWorkspace.rk_dim15_bPerm[3];
b[4] = acadoWorkspace.rk_dim15_bPerm[4];
b[5] = acadoWorkspace.rk_dim15_bPerm[5];
b[6] = acadoWorkspace.rk_dim15_bPerm[6];
b[7] = acadoWorkspace.rk_dim15_bPerm[7];
b[8] = acadoWorkspace.rk_dim15_bPerm[8];
b[9] = acadoWorkspace.rk_dim15_bPerm[9];
b[10] = acadoWorkspace.rk_dim15_bPerm[10];
b[11] = acadoWorkspace.rk_dim15_bPerm[11];
b[12] = acadoWorkspace.rk_dim15_bPerm[12];
b[13] = acadoWorkspace.rk_dim15_bPerm[13];
b[14] = acadoWorkspace.rk_dim15_bPerm[14];
}



/** Column vector of size: 1 */
static const real_t acado_Ah_mat[ 1 ] = 
{ 1.2500000000000001e-02 };


/* Fixed step size:0.025 */
int acado_integrate( real_t* const rk_eta, int resetIntegrator )
{
int error;

int i;
int j;
int k;
int run;
int run1;
int tmp_index1;
int tmp_index2;

real_t det;

acadoWorkspace.rk_ttt = 0.0000000000000000e+00;
acadoWorkspace.rk_xxx[15] = rk_eta[195];
acadoWorkspace.rk_xxx[16] = rk_eta[196];
acadoWorkspace.rk_xxx[17] = rk_eta[197];
acadoWorkspace.rk_xxx[18] = rk_eta[198];

if( resetIntegrator ) {
acadoWorkspace.rk_kkk[11] = rk_eta[11];
acadoWorkspace.rk_kkk[12] = rk_eta[12];
acadoWorkspace.rk_kkk[13] = rk_eta[13];
acadoWorkspace.rk_kkk[14] = rk_eta[14];
}
for (run = 0; run < 2; ++run)
{
if( run > 0 ) {
for (i = 0; i < 11; ++i)
{
acadoWorkspace.rk_diffsPrev2[i * 12] = rk_eta[i * 11 + 15];
acadoWorkspace.rk_diffsPrev2[i * 12 + 1] = rk_eta[i * 11 + 16];
acadoWorkspace.rk_diffsPrev2[i * 12 + 2] = rk_eta[i * 11 + 17];
acadoWorkspace.rk_diffsPrev2[i * 12 + 3] = rk_eta[i * 11 + 18];
acadoWorkspace.rk_diffsPrev2[i * 12 + 4] = rk_eta[i * 11 + 19];
acadoWorkspace.rk_diffsPrev2[i * 12 + 5] = rk_eta[i * 11 + 20];
acadoWorkspace.rk_diffsPrev2[i * 12 + 6] = rk_eta[i * 11 + 21];
acadoWorkspace.rk_diffsPrev2[i * 12 + 7] = rk_eta[i * 11 + 22];
acadoWorkspace.rk_diffsPrev2[i * 12 + 8] = rk_eta[i * 11 + 23];
acadoWorkspace.rk_diffsPrev2[i * 12 + 9] = rk_eta[i * 11 + 24];
acadoWorkspace.rk_diffsPrev2[i * 12 + 10] = rk_eta[i * 11 + 25];
acadoWorkspace.rk_diffsPrev2[i * 12 + 11] = rk_eta[i + 180];
}
}
if( resetIntegrator ) {
for (i = 0; i < 1; ++i)
{
for (run1 = 0; run1 < 1; ++run1)
{
for (j = 0; j < 11; ++j)
{
acadoWorkspace.rk_xxx[j] = rk_eta[j];
tmp_index1 = j;
acadoWorkspace.rk_xxx[j] += + acado_Ah_mat[run1]*acadoWorkspace.rk_kkk[tmp_index1];
}
for (j = 0; j < 4; ++j)
{
tmp_index1 = j + 11;
acadoWorkspace.rk_xxx[j + 11] = acadoWorkspace.rk_kkk[(tmp_index1) + (run1)];
}
acado_diffs( acadoWorkspace.rk_xxx, &(acadoWorkspace.rk_diffsTemp2[ run1 * 240 ]) );
for (j = 0; j < 15; ++j)
{
tmp_index1 = (run1 * 15) + (j);
acadoWorkspace.rk_A[tmp_index1 * 15] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 1] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 1)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 2] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 2)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 3] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 3)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 4] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 4)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 5] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 5)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 6] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 6)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 7] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 7)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 8] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 8)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 9] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 9)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 10] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 10)];
if( 0 == run1 ) acadoWorkspace.rk_A[(tmp_index1 * 15) + (j)] -= 1.0000000000000000e+00;
if( 0 == run1 ) {
acadoWorkspace.rk_A[tmp_index1 * 15 + 11] = acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 11)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 12] = acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 12)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 13] = acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 13)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 14] = acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 14)];
}
 else {
acadoWorkspace.rk_A[tmp_index1 * 15 + 11] = 0.0000000000000000e+00;
acadoWorkspace.rk_A[tmp_index1 * 15 + 12] = 0.0000000000000000e+00;
acadoWorkspace.rk_A[tmp_index1 * 15 + 13] = 0.0000000000000000e+00;
acadoWorkspace.rk_A[tmp_index1 * 15 + 14] = 0.0000000000000000e+00;
}
}
acado_rhs( acadoWorkspace.rk_xxx, acadoWorkspace.rk_rhsTemp );
acadoWorkspace.rk_b[run1 * 15] = acadoWorkspace.rk_kkk[run1] - acadoWorkspace.rk_rhsTemp[0];
acadoWorkspace.rk_b[run1 * 15 + 1] = acadoWorkspace.rk_kkk[run1 + 1] - acadoWorkspace.rk_rhsTemp[1];
acadoWorkspace.rk_b[run1 * 15 + 2] = acadoWorkspace.rk_kkk[run1 + 2] - acadoWorkspace.rk_rhsTemp[2];
acadoWorkspace.rk_b[run1 * 15 + 3] = acadoWorkspace.rk_kkk[run1 + 3] - acadoWorkspace.rk_rhsTemp[3];
acadoWorkspace.rk_b[run1 * 15 + 4] = acadoWorkspace.rk_kkk[run1 + 4] - acadoWorkspace.rk_rhsTemp[4];
acadoWorkspace.rk_b[run1 * 15 + 5] = acadoWorkspace.rk_kkk[run1 + 5] - acadoWorkspace.rk_rhsTemp[5];
acadoWorkspace.rk_b[run1 * 15 + 6] = acadoWorkspace.rk_kkk[run1 + 6] - acadoWorkspace.rk_rhsTemp[6];
acadoWorkspace.rk_b[run1 * 15 + 7] = acadoWorkspace.rk_kkk[run1 + 7] - acadoWorkspace.rk_rhsTemp[7];
acadoWorkspace.rk_b[run1 * 15 + 8] = acadoWorkspace.rk_kkk[run1 + 8] - acadoWorkspace.rk_rhsTemp[8];
acadoWorkspace.rk_b[run1 * 15 + 9] = acadoWorkspace.rk_kkk[run1 + 9] - acadoWorkspace.rk_rhsTemp[9];
acadoWorkspace.rk_b[run1 * 15 + 10] = acadoWorkspace.rk_kkk[run1 + 10] - acadoWorkspace.rk_rhsTemp[10];
acadoWorkspace.rk_b[run1 * 15 + 11] = - acadoWorkspace.rk_rhsTemp[11];
acadoWorkspace.rk_b[run1 * 15 + 12] = - acadoWorkspace.rk_rhsTemp[12];
acadoWorkspace.rk_b[run1 * 15 + 13] = - acadoWorkspace.rk_rhsTemp[13];
acadoWorkspace.rk_b[run1 * 15 + 14] = - acadoWorkspace.rk_rhsTemp[14];
}
det = acado_solve_dim15_system( acadoWorkspace.rk_A, acadoWorkspace.rk_b, acadoWorkspace.rk_dim15_perm );
for (j = 0; j < 1; ++j)
{
acadoWorkspace.rk_kkk[j] += acadoWorkspace.rk_b[j * 11];
acadoWorkspace.rk_kkk[j + 1] += acadoWorkspace.rk_b[j * 11 + 1];
acadoWorkspace.rk_kkk[j + 2] += acadoWorkspace.rk_b[j * 11 + 2];
acadoWorkspace.rk_kkk[j + 3] += acadoWorkspace.rk_b[j * 11 + 3];
acadoWorkspace.rk_kkk[j + 4] += acadoWorkspace.rk_b[j * 11 + 4];
acadoWorkspace.rk_kkk[j + 5] += acadoWorkspace.rk_b[j * 11 + 5];
acadoWorkspace.rk_kkk[j + 6] += acadoWorkspace.rk_b[j * 11 + 6];
acadoWorkspace.rk_kkk[j + 7] += acadoWorkspace.rk_b[j * 11 + 7];
acadoWorkspace.rk_kkk[j + 8] += acadoWorkspace.rk_b[j * 11 + 8];
acadoWorkspace.rk_kkk[j + 9] += acadoWorkspace.rk_b[j * 11 + 9];
acadoWorkspace.rk_kkk[j + 10] += acadoWorkspace.rk_b[j * 11 + 10];
acadoWorkspace.rk_kkk[j + 11] += acadoWorkspace.rk_b[j * 4 + 11];
acadoWorkspace.rk_kkk[j + 12] += acadoWorkspace.rk_b[j * 4 + 12];
acadoWorkspace.rk_kkk[j + 13] += acadoWorkspace.rk_b[j * 4 + 13];
acadoWorkspace.rk_kkk[j + 14] += acadoWorkspace.rk_b[j * 4 + 14];
}
}
}
for (i = 0; i < 5; ++i)
{
for (run1 = 0; run1 < 1; ++run1)
{
for (j = 0; j < 11; ++j)
{
acadoWorkspace.rk_xxx[j] = rk_eta[j];
tmp_index1 = j;
acadoWorkspace.rk_xxx[j] += + acado_Ah_mat[run1]*acadoWorkspace.rk_kkk[tmp_index1];
}
for (j = 0; j < 4; ++j)
{
tmp_index1 = j + 11;
acadoWorkspace.rk_xxx[j + 11] = acadoWorkspace.rk_kkk[(tmp_index1) + (run1)];
}
acado_rhs( acadoWorkspace.rk_xxx, acadoWorkspace.rk_rhsTemp );
acadoWorkspace.rk_b[run1 * 15] = acadoWorkspace.rk_kkk[run1] - acadoWorkspace.rk_rhsTemp[0];
acadoWorkspace.rk_b[run1 * 15 + 1] = acadoWorkspace.rk_kkk[run1 + 1] - acadoWorkspace.rk_rhsTemp[1];
acadoWorkspace.rk_b[run1 * 15 + 2] = acadoWorkspace.rk_kkk[run1 + 2] - acadoWorkspace.rk_rhsTemp[2];
acadoWorkspace.rk_b[run1 * 15 + 3] = acadoWorkspace.rk_kkk[run1 + 3] - acadoWorkspace.rk_rhsTemp[3];
acadoWorkspace.rk_b[run1 * 15 + 4] = acadoWorkspace.rk_kkk[run1 + 4] - acadoWorkspace.rk_rhsTemp[4];
acadoWorkspace.rk_b[run1 * 15 + 5] = acadoWorkspace.rk_kkk[run1 + 5] - acadoWorkspace.rk_rhsTemp[5];
acadoWorkspace.rk_b[run1 * 15 + 6] = acadoWorkspace.rk_kkk[run1 + 6] - acadoWorkspace.rk_rhsTemp[6];
acadoWorkspace.rk_b[run1 * 15 + 7] = acadoWorkspace.rk_kkk[run1 + 7] - acadoWorkspace.rk_rhsTemp[7];
acadoWorkspace.rk_b[run1 * 15 + 8] = acadoWorkspace.rk_kkk[run1 + 8] - acadoWorkspace.rk_rhsTemp[8];
acadoWorkspace.rk_b[run1 * 15 + 9] = acadoWorkspace.rk_kkk[run1 + 9] - acadoWorkspace.rk_rhsTemp[9];
acadoWorkspace.rk_b[run1 * 15 + 10] = acadoWorkspace.rk_kkk[run1 + 10] - acadoWorkspace.rk_rhsTemp[10];
acadoWorkspace.rk_b[run1 * 15 + 11] = - acadoWorkspace.rk_rhsTemp[11];
acadoWorkspace.rk_b[run1 * 15 + 12] = - acadoWorkspace.rk_rhsTemp[12];
acadoWorkspace.rk_b[run1 * 15 + 13] = - acadoWorkspace.rk_rhsTemp[13];
acadoWorkspace.rk_b[run1 * 15 + 14] = - acadoWorkspace.rk_rhsTemp[14];
}
acado_solve_dim15_system_reuse( acadoWorkspace.rk_A, acadoWorkspace.rk_b, acadoWorkspace.rk_dim15_perm );
for (j = 0; j < 1; ++j)
{
acadoWorkspace.rk_kkk[j] += acadoWorkspace.rk_b[j * 11];
acadoWorkspace.rk_kkk[j + 1] += acadoWorkspace.rk_b[j * 11 + 1];
acadoWorkspace.rk_kkk[j + 2] += acadoWorkspace.rk_b[j * 11 + 2];
acadoWorkspace.rk_kkk[j + 3] += acadoWorkspace.rk_b[j * 11 + 3];
acadoWorkspace.rk_kkk[j + 4] += acadoWorkspace.rk_b[j * 11 + 4];
acadoWorkspace.rk_kkk[j + 5] += acadoWorkspace.rk_b[j * 11 + 5];
acadoWorkspace.rk_kkk[j + 6] += acadoWorkspace.rk_b[j * 11 + 6];
acadoWorkspace.rk_kkk[j + 7] += acadoWorkspace.rk_b[j * 11 + 7];
acadoWorkspace.rk_kkk[j + 8] += acadoWorkspace.rk_b[j * 11 + 8];
acadoWorkspace.rk_kkk[j + 9] += acadoWorkspace.rk_b[j * 11 + 9];
acadoWorkspace.rk_kkk[j + 10] += acadoWorkspace.rk_b[j * 11 + 10];
acadoWorkspace.rk_kkk[j + 11] += acadoWorkspace.rk_b[j * 4 + 11];
acadoWorkspace.rk_kkk[j + 12] += acadoWorkspace.rk_b[j * 4 + 12];
acadoWorkspace.rk_kkk[j + 13] += acadoWorkspace.rk_b[j * 4 + 13];
acadoWorkspace.rk_kkk[j + 14] += acadoWorkspace.rk_b[j * 4 + 14];
}
}
for (run1 = 0; run1 < 1; ++run1)
{
for (j = 0; j < 11; ++j)
{
acadoWorkspace.rk_xxx[j] = rk_eta[j];
tmp_index1 = j;
acadoWorkspace.rk_xxx[j] += + acado_Ah_mat[run1]*acadoWorkspace.rk_kkk[tmp_index1];
}
for (j = 0; j < 4; ++j)
{
tmp_index1 = j + 11;
acadoWorkspace.rk_xxx[j + 11] = acadoWorkspace.rk_kkk[(tmp_index1) + (run1)];
}
acado_diffs( acadoWorkspace.rk_xxx, &(acadoWorkspace.rk_diffsTemp2[ run1 * 240 ]) );
for (j = 0; j < 15; ++j)
{
tmp_index1 = (run1 * 15) + (j);
acadoWorkspace.rk_A[tmp_index1 * 15] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 1] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 1)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 2] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 2)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 3] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 3)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 4] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 4)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 5] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 5)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 6] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 6)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 7] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 7)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 8] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 8)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 9] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 9)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 10] = + acado_Ah_mat[run1]*acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 10)];
if( 0 == run1 ) acadoWorkspace.rk_A[(tmp_index1 * 15) + (j)] -= 1.0000000000000000e+00;
if( 0 == run1 ) {
acadoWorkspace.rk_A[tmp_index1 * 15 + 11] = acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 11)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 12] = acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 12)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 13] = acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 13)];
acadoWorkspace.rk_A[tmp_index1 * 15 + 14] = acadoWorkspace.rk_diffsTemp2[(run1 * 240) + (j * 16 + 14)];
}
 else {
acadoWorkspace.rk_A[tmp_index1 * 15 + 11] = 0.0000000000000000e+00;
acadoWorkspace.rk_A[tmp_index1 * 15 + 12] = 0.0000000000000000e+00;
acadoWorkspace.rk_A[tmp_index1 * 15 + 13] = 0.0000000000000000e+00;
acadoWorkspace.rk_A[tmp_index1 * 15 + 14] = 0.0000000000000000e+00;
}
}
}
for (run1 = 0; run1 < 11; ++run1)
{
for (i = 0; i < 1; ++i)
{
acadoWorkspace.rk_b[i * 15] = - acadoWorkspace.rk_diffsTemp2[(i * 240) + (run1)];
acadoWorkspace.rk_b[i * 15 + 1] = - acadoWorkspace.rk_diffsTemp2[(i * 240) + (run1 + 16)];
acadoWorkspace.rk_b[i * 15 + 2] = - acadoWorkspace.rk_diffsTemp2[(i * 240) + (run1 + 32)];
acadoWorkspace.rk_b[i * 15 + 3] = - acadoWorkspace.rk_diffsTemp2[(i * 240) + (run1 + 48)];
acadoWorkspace.rk_b[i * 15 + 4] = - acadoWorkspace.rk_diffsTemp2[(i * 240) + (run1 + 64)];
acadoWorkspace.rk_b[i * 15 + 5] = - acadoWorkspace.rk_diffsTemp2[(i * 240) + (run1 + 80)];
acadoWorkspace.rk_b[i * 15 + 6] = - acadoWorkspace.rk_diffsTemp2[(i * 240) + (run1 + 96)];
acadoWorkspace.rk_b[i * 15 + 7] = - acadoWorkspace.rk_diffsTemp2[(i * 240) + (run1 + 112)];
acadoWorkspace.rk_b[i * 15 + 8] = - acadoWorkspace.rk_diffsTemp2[(i * 240) + (run1 + 128)];
acadoWorkspace.rk_b[i * 15 + 9] = - acadoWorkspace.rk_diffsTemp2[(i * 240) + (run1 + 144)];
acadoWorkspace.rk_b[i * 15 + 10] = - acadoWorkspace.rk_diffsTemp2[(i * 240) + (run1 + 160)];
acadoWorkspace.rk_b[i * 15 + 11] = - acadoWorkspace.rk_diffsTemp2[(i * 240) + (run1 + 176)];
acadoWorkspace.rk_b[i * 15 + 12] = - acadoWorkspace.rk_diffsTemp2[(i * 240) + (run1 + 192)];
acadoWorkspace.rk_b[i * 15 + 13] = - acadoWorkspace.rk_diffsTemp2[(i * 240) + (run1 + 208)];
acadoWorkspace.rk_b[i * 15 + 14] = - acadoWorkspace.rk_diffsTemp2[(i * 240) + (run1 + 224)];
}
if( 0 == run1 ) {
det = acado_solve_dim15_system( acadoWorkspace.rk_A, acadoWorkspace.rk_b, acadoWorkspace.rk_dim15_perm );
}
 else {
acado_solve_dim15_system_reuse( acadoWorkspace.rk_A, acadoWorkspace.rk_b, acadoWorkspace.rk_dim15_perm );
}
for (i = 0; i < 1; ++i)
{
acadoWorkspace.rk_diffK[i] = acadoWorkspace.rk_b[i * 11];
acadoWorkspace.rk_diffK[i + 1] = acadoWorkspace.rk_b[i * 11 + 1];
acadoWorkspace.rk_diffK[i + 2] = acadoWorkspace.rk_b[i * 11 + 2];
acadoWorkspace.rk_diffK[i + 3] = acadoWorkspace.rk_b[i * 11 + 3];
acadoWorkspace.rk_diffK[i + 4] = acadoWorkspace.rk_b[i * 11 + 4];
acadoWorkspace.rk_diffK[i + 5] = acadoWorkspace.rk_b[i * 11 + 5];
acadoWorkspace.rk_diffK[i + 6] = acadoWorkspace.rk_b[i * 11 + 6];
acadoWorkspace.rk_diffK[i + 7] = acadoWorkspace.rk_b[i * 11 + 7];
acadoWorkspace.rk_diffK[i + 8] = acadoWorkspace.rk_b[i * 11 + 8];
acadoWorkspace.rk_diffK[i + 9] = acadoWorkspace.rk_b[i * 11 + 9];
acadoWorkspace.rk_diffK[i + 10] = acadoWorkspace.rk_b[i * 11 + 10];
acadoWorkspace.rk_diffK[i + 11] = acadoWorkspace.rk_b[i * 4 + 11];
acadoWorkspace.rk_diffK[i + 12] = acadoWorkspace.rk_b[i * 4 + 12];
acadoWorkspace.rk_diffK[i + 13] = acadoWorkspace.rk_b[i * 4 + 13];
acadoWorkspace.rk_diffK[i + 14] = acadoWorkspace.rk_b[i * 4 + 14];
}
for (i = 0; i < 11; ++i)
{
acadoWorkspace.rk_diffsNew2[(i * 12) + (run1)] = (i == run1-0);
acadoWorkspace.rk_diffsNew2[(i * 12) + (run1)] += + acadoWorkspace.rk_diffK[i]*(real_t)2.5000000000000001e-02;
}
if( run == 0 ) {
for (i = 0; i < 4; ++i)
{
acadoWorkspace.rk_diffsNew2[(i * 12 + 132) + (run1)] = + acadoWorkspace.rk_diffK[i + 11];
}
}
}
for (run1 = 0; run1 < 1; ++run1)
{
for (i = 0; i < 1; ++i)
{
for (j = 0; j < 15; ++j)
{
tmp_index1 = (i * 15) + (j);
tmp_index2 = (run1) + (j * 16);
acadoWorkspace.rk_b[tmp_index1] = - acadoWorkspace.rk_diffsTemp2[(i * 240) + (tmp_index2 + 15)];
}
}
acado_solve_dim15_system_reuse( acadoWorkspace.rk_A, acadoWorkspace.rk_b, acadoWorkspace.rk_dim15_perm );
for (i = 0; i < 1; ++i)
{
acadoWorkspace.rk_diffK[i] = acadoWorkspace.rk_b[i * 11];
acadoWorkspace.rk_diffK[i + 1] = acadoWorkspace.rk_b[i * 11 + 1];
acadoWorkspace.rk_diffK[i + 2] = acadoWorkspace.rk_b[i * 11 + 2];
acadoWorkspace.rk_diffK[i + 3] = acadoWorkspace.rk_b[i * 11 + 3];
acadoWorkspace.rk_diffK[i + 4] = acadoWorkspace.rk_b[i * 11 + 4];
acadoWorkspace.rk_diffK[i + 5] = acadoWorkspace.rk_b[i * 11 + 5];
acadoWorkspace.rk_diffK[i + 6] = acadoWorkspace.rk_b[i * 11 + 6];
acadoWorkspace.rk_diffK[i + 7] = acadoWorkspace.rk_b[i * 11 + 7];
acadoWorkspace.rk_diffK[i + 8] = acadoWorkspace.rk_b[i * 11 + 8];
acadoWorkspace.rk_diffK[i + 9] = acadoWorkspace.rk_b[i * 11 + 9];
acadoWorkspace.rk_diffK[i + 10] = acadoWorkspace.rk_b[i * 11 + 10];
acadoWorkspace.rk_diffK[i + 11] = acadoWorkspace.rk_b[i * 4 + 11];
acadoWorkspace.rk_diffK[i + 12] = acadoWorkspace.rk_b[i * 4 + 12];
acadoWorkspace.rk_diffK[i + 13] = acadoWorkspace.rk_b[i * 4 + 13];
acadoWorkspace.rk_diffK[i + 14] = acadoWorkspace.rk_b[i * 4 + 14];
}
for (i = 0; i < 11; ++i)
{
acadoWorkspace.rk_diffsNew2[(i * 12) + (run1 + 11)] = + acadoWorkspace.rk_diffK[i]*(real_t)2.5000000000000001e-02;
}
if( run == 0 ) {
for (i = 0; i < 4; ++i)
{
acadoWorkspace.rk_diffsNew2[(i * 12 + 132) + (run1 + 11)] = + acadoWorkspace.rk_diffK[i + 11];
}
}
}
rk_eta[0] += + acadoWorkspace.rk_kkk[0]*(real_t)2.5000000000000001e-02;
rk_eta[1] += + acadoWorkspace.rk_kkk[1]*(real_t)2.5000000000000001e-02;
rk_eta[2] += + acadoWorkspace.rk_kkk[2]*(real_t)2.5000000000000001e-02;
rk_eta[3] += + acadoWorkspace.rk_kkk[3]*(real_t)2.5000000000000001e-02;
rk_eta[4] += + acadoWorkspace.rk_kkk[4]*(real_t)2.5000000000000001e-02;
rk_eta[5] += + acadoWorkspace.rk_kkk[5]*(real_t)2.5000000000000001e-02;
rk_eta[6] += + acadoWorkspace.rk_kkk[6]*(real_t)2.5000000000000001e-02;
rk_eta[7] += + acadoWorkspace.rk_kkk[7]*(real_t)2.5000000000000001e-02;
rk_eta[8] += + acadoWorkspace.rk_kkk[8]*(real_t)2.5000000000000001e-02;
rk_eta[9] += + acadoWorkspace.rk_kkk[9]*(real_t)2.5000000000000001e-02;
rk_eta[10] += + acadoWorkspace.rk_kkk[10]*(real_t)2.5000000000000001e-02;
if( run == 0 ) {
rk_eta[11] = + acadoWorkspace.rk_kkk[11];
rk_eta[12] = + acadoWorkspace.rk_kkk[12];
rk_eta[13] = + acadoWorkspace.rk_kkk[13];
rk_eta[14] = + acadoWorkspace.rk_kkk[14];
}
if( run == 0 ) {
for (i = 0; i < 11; ++i)
{
for (j = 0; j < 11; ++j)
{
tmp_index2 = (j) + (i * 11);
rk_eta[tmp_index2 + 15] = acadoWorkspace.rk_diffsNew2[(i * 12) + (j)];
}
for (j = 0; j < 1; ++j)
{
tmp_index2 = (j) + (i);
rk_eta[tmp_index2 + 180] = acadoWorkspace.rk_diffsNew2[(i * 12) + (j + 11)];
}
}
for (i = 11; i < 15; ++i)
{
for (j = 0; j < 11; ++j)
{
tmp_index2 = (j) + (i * 11);
rk_eta[tmp_index2 + 15] = acadoWorkspace.rk_diffsNew2[(i * 12) + (j)];
}
for (j = 0; j < 1; ++j)
{
tmp_index2 = (j) + (i);
rk_eta[tmp_index2 + 180] = acadoWorkspace.rk_diffsNew2[(i * 12) + (j + 11)];
}
}
}
else {
for (i = 0; i < 11; ++i)
{
for (j = 0; j < 11; ++j)
{
tmp_index2 = (j) + (i * 11);
rk_eta[tmp_index2 + 15] = + acadoWorkspace.rk_diffsNew2[i * 12]*acadoWorkspace.rk_diffsPrev2[j];
rk_eta[tmp_index2 + 15] += + acadoWorkspace.rk_diffsNew2[i * 12 + 1]*acadoWorkspace.rk_diffsPrev2[j + 12];
rk_eta[tmp_index2 + 15] += + acadoWorkspace.rk_diffsNew2[i * 12 + 2]*acadoWorkspace.rk_diffsPrev2[j + 24];
rk_eta[tmp_index2 + 15] += + acadoWorkspace.rk_diffsNew2[i * 12 + 3]*acadoWorkspace.rk_diffsPrev2[j + 36];
rk_eta[tmp_index2 + 15] += + acadoWorkspace.rk_diffsNew2[i * 12 + 4]*acadoWorkspace.rk_diffsPrev2[j + 48];
rk_eta[tmp_index2 + 15] += + acadoWorkspace.rk_diffsNew2[i * 12 + 5]*acadoWorkspace.rk_diffsPrev2[j + 60];
rk_eta[tmp_index2 + 15] += + acadoWorkspace.rk_diffsNew2[i * 12 + 6]*acadoWorkspace.rk_diffsPrev2[j + 72];
rk_eta[tmp_index2 + 15] += + acadoWorkspace.rk_diffsNew2[i * 12 + 7]*acadoWorkspace.rk_diffsPrev2[j + 84];
rk_eta[tmp_index2 + 15] += + acadoWorkspace.rk_diffsNew2[i * 12 + 8]*acadoWorkspace.rk_diffsPrev2[j + 96];
rk_eta[tmp_index2 + 15] += + acadoWorkspace.rk_diffsNew2[i * 12 + 9]*acadoWorkspace.rk_diffsPrev2[j + 108];
rk_eta[tmp_index2 + 15] += + acadoWorkspace.rk_diffsNew2[i * 12 + 10]*acadoWorkspace.rk_diffsPrev2[j + 120];
}
for (j = 0; j < 1; ++j)
{
tmp_index2 = (j) + (i);
rk_eta[tmp_index2 + 180] = acadoWorkspace.rk_diffsNew2[(i * 12) + (j + 11)];
rk_eta[tmp_index2 + 180] += + acadoWorkspace.rk_diffsNew2[i * 12]*acadoWorkspace.rk_diffsPrev2[j + 11];
rk_eta[tmp_index2 + 180] += + acadoWorkspace.rk_diffsNew2[i * 12 + 1]*acadoWorkspace.rk_diffsPrev2[j + 23];
rk_eta[tmp_index2 + 180] += + acadoWorkspace.rk_diffsNew2[i * 12 + 2]*acadoWorkspace.rk_diffsPrev2[j + 35];
rk_eta[tmp_index2 + 180] += + acadoWorkspace.rk_diffsNew2[i * 12 + 3]*acadoWorkspace.rk_diffsPrev2[j + 47];
rk_eta[tmp_index2 + 180] += + acadoWorkspace.rk_diffsNew2[i * 12 + 4]*acadoWorkspace.rk_diffsPrev2[j + 59];
rk_eta[tmp_index2 + 180] += + acadoWorkspace.rk_diffsNew2[i * 12 + 5]*acadoWorkspace.rk_diffsPrev2[j + 71];
rk_eta[tmp_index2 + 180] += + acadoWorkspace.rk_diffsNew2[i * 12 + 6]*acadoWorkspace.rk_diffsPrev2[j + 83];
rk_eta[tmp_index2 + 180] += + acadoWorkspace.rk_diffsNew2[i * 12 + 7]*acadoWorkspace.rk_diffsPrev2[j + 95];
rk_eta[tmp_index2 + 180] += + acadoWorkspace.rk_diffsNew2[i * 12 + 8]*acadoWorkspace.rk_diffsPrev2[j + 107];
rk_eta[tmp_index2 + 180] += + acadoWorkspace.rk_diffsNew2[i * 12 + 9]*acadoWorkspace.rk_diffsPrev2[j + 119];
rk_eta[tmp_index2 + 180] += + acadoWorkspace.rk_diffsNew2[i * 12 + 10]*acadoWorkspace.rk_diffsPrev2[j + 131];
}
}
}
resetIntegrator = 0;
acadoWorkspace.rk_ttt += 5.0000000000000000e-01;
}
for (i = 0; i < 11; ++i)
{
}
for (i = 11; i < 15; ++i)
{
}
if( det < 1e-12 ) {
error = 2;
} else if( det < 1e-6 ) {
error = 1;
} else {
error = 0;
}
return error;
}



