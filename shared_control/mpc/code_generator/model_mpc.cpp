// Vamos a empezar a implementar ROS2 :)
// En esta prueba vamos a probar a cambiar el archivo test.c generado por un test.cpp

#include <acado_toolkit.hpp>
#include <iostream>

using namespace std;

int main( )
{
    USING_NAMESPACE_ACADO

    // VARIABLES:
    DifferentialState X, Y, PSI, dx, dy, dpsi, epsi, ey, theta, w, T;   // Differential variables (states)
    AlgebraicState    Fyf, Fyr, bef, delta;                                 // Algebraic variables
    OnlineData        ka, ax, kurv;                                         // Exogenous input vector
    Control           dT;                                                   // Control variables or inputs

    // MPC:
    float  Ts = 0.05;
    int     N = 30;

    // MOTOR:
    float   J = 0.1;            // Inertia
    float   b = 0.65;           // Damping

    // BASELINE VALUES:
    float  L  = 3.01;           // wheelbase [m]
    float  lf = 1.61;           // front ale to CG [m]
    float  lr = L-lf;           // rear ale to CG [m]
    int    m  = 1845;           // mass of the vehicle[kg]
    int    Iz = 3234;           // moment of inertia about z [kg.m^2]
    int    Cf = 94000/10;
    int    Cr = 118000/10;
    int   ls  = 2;
    float kd  = 540.0/70.0;
    float ksa = (Cf/(1200));


    // DIFFERENTIAL EQUATION:
    // -------------------------------
    DifferentialEquation f;

    f << dot(X)     ==  dx*cos(PSI) - dy*sin(PSI);                                  //x position (absolute)
    f << dot(Y)     ==  dx*sin(PSI) + dy*cos(PSI);                                  //y position (absolute) & cross track error
    f << dot(PSI)   ==  dpsi;
    f << dot(dx)    ==  (m*ax*cos(delta) + m*dy*dpsi - Fyf*sin(delta))/m;
    f << dot(dy)    ==  (-m*dx*dpsi + m*ax*sin(delta) + Fyf*cos(delta) + Fyr)/m;                   
    f << dot(dpsi)  ==  (lf*(m*ax*sin(delta)+Fyf*cos(delta)) - lr*Fyr)/Iz;
    f << dot(epsi)  ==  dpsi - (kurv/(1-kurv*ey))*(dx*cos(epsi)- dy*sin(epsi));     //dpsi - (kurv/(1-kurv*ey))*(dx*cos(epsi)- dy*sin(epsi));
    f << dot(ey)    ==  dx*sin(epsi) + dy*cos(epsi);                                //v*sin(PSI - PSIr);
    f << dot(theta) ==  w;                                                          //Definition of the state number 1
    f << dot(w)     ==  (-bef/J)*w + (T)/(J) + (Fyf/(300))/J;                       //- 0.5*w/(J*sqrt(w*w +(1e-3)^2));//- (1-mode)*((ka-1)*(theta-thetad))/J;    //(-Fyf/750)(1/1000)*(-Cf*alfaf)% Definition of the state number 2
    f << dot(T)     ==  ka*dT; //mode*ka*dT + (1-mode)*dT;                                           //Definition of the state number 3
    f << 0          ==  Fyf - (Cf)*(delta - atan((dy + lf*dpsi)/(dx+1e-3)));
    f << 0          ==  Fyr - (Cr)*atan((-dy + lr*dpsi)/(dx+1e-3));
    f << 0          ==  bef - (b*sqrt((ka+1)/2)); //mode*b*sqrt(ka)- (1-mode)*b*sqrt((ksa+(ka-1))/ksa);
    f << 0          ==  delta  - theta/kd;


    // DEFINE LEAST SQUARE FUNCTION:
    // -----------------------------
    Function h,hN;

    h << X << Y << PSI << dpsi << w << T << dT;
    hN << X << Y << PSI << dpsi << w << T;
    // h << X << Y << PSI << dpsi << T << dT;
    // hN << X << Y << PSI << dpsi << T;

    // LSQ coefficient matrix (En MATLAB se hace de otra forma)
    BMatrix W = eye<bool>( h.getDim() );
	BMatrix WN = eye<bool>( hN.getDim() );

    // Reference
    //DVector r(5),rN(4); 
    //r.setAll( 0.0 );
    //rN.setAll( 0.0);

    // DEFINE AN OPTIMAL CONTROL PROBLEM:
    // ----------------------------------

    OCP ocp( 0.0, N*Ts, N );

    ocp.subjectTo( f );

    ocp.minimizeLSQ( W, h );
    ocp.minimizeLSQEndTerm(WN,hN);

    // State and control contraints   
    ocp.subjectTo(-1 <= dT <= 1);
    // ocp.subjectTo(-1 <= dpsi <= 1);
    // ocp.subjectTo(-1 <= ey <= 1);
    // ocp.subjectTo(-1 <= w <= 1);
    ocp.subjectTo(-1 <= T <= 1);


    // Export the code:
    OCPexport mpc( ocp );

    mpc.set( HESSIAN_APPROXIMATION,       GAUSS_NEWTON      );
    mpc.set( DISCRETIZATION_TYPE,         MULTIPLE_SHOOTING );
    mpc.set( QP_SOLVER,                   QP_QPOASES3   	);
    mpc.set( HOTSTART_QP,                 NO             	);

    mpc.set( LEVENBERG_MARQUARDT, 		 1e-10				);
    mpc.set( CG_HARDCODE_CONSTRAINT_VALUES,NO 				);
    mpc.set( SPARSE_QP_SOLUTION,          FULL_CONDENSING   );
    mpc.set( INTEGRATOR_TYPE,             INT_IRK_GL2       );

    mpc.set( NUM_INTEGRATOR_STEPS,        2*30              );

    OptimizationAlgorithm solver(ocp);     
    solver.set(MAX_NUM_ITERATIONS,1);

    if (mpc.exportCode( "model_code_generated" ) != SUCCESSFUL_RETURN)
        exit( EXIT_FAILURE );

    mpc.printDimensionsQP( );

    return EXIT_SUCCESS;
}
