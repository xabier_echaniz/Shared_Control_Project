#include "shared_control_basics.hpp"

#include "acado_common.h"
#include "acado_auxiliary_functions.h"

/* Some convenient definitions. */
#define NX          ACADO_NX  	/* Number of differential state variables.  */
#define NXA         ACADO_NXA 	/* Number of algebraic variables. */
#define NU          ACADO_NU  	/* Number of control inputs. */
#define NOD         ACADO_NOD 	/* Number of online data values. */

#define NY          ACADO_NY  	/* Number of measurements/references on nodes 0..N - 1. */
#define NYN         ACADO_NYN 	/* Number of measurements/references on node N. */

// Is defined in shared_control_basics.hpp
// #define N           ACADO_N   	/* Number of intervals in the horizon. */

#define NUM_STEPS       1       /* Number of real-time iterations. */
#define VERBOSE         0       /* Show iterations: 1, silent: 0.  */

#define LOOP_TIME_MS    10		/* Loop time in milliseconds */
#define DISPLAY_DATA    1       // 1 --> Display data || 0 --> No display data :)

#define RESET_WHEN_NAN  1
#define KKT_MIN         -1
#define KKT_MAX         1e15


/* Global variables used by the solver. */
ACADOvariables acadoVariables;
ACADOworkspace acadoWorkspace;

class mpcNode : public rclcpp::Node
{
public:
    mpcNode()
        : Node("mpc_node")
    {
        // SUBSCRIBERS
        planner_references_sub_ = this->create_subscription<std_msgs::msg::Float32MultiArray>(
            "planner/references", 1, std::bind(&mpcNode::planner_references_callback, this, _1));
		planner_vehicle_sub_ = this->create_subscription<std_msgs::msg::Float32MultiArray>(
            "planner/vehicle", 1, std::bind(&mpcNode::planner_vehicle_callback, this, _1));
        planner_weights_sub_ = this->create_subscription<std_msgs::msg::Float32MultiArray>(
            "planner/weights", 1, std::bind(&mpcNode::planner_weights_callback, this, _1));
        planner_online_data_sub_ = this->create_subscription<std_msgs::msg::Float32MultiArray>(
            "planner/online_data", 1, std::bind(&mpcNode::planner_online_data_callback, this, _1));

        // PUBLISHERS
        mpc_predictions_pub_ = this->create_publisher<std_msgs::msg::Float32MultiArray>("mpc/predictions", 1);
        mpc_control_cmd_pub_ = this->create_publisher<std_msgs::msg::Float32>("mpc/control_cmd", 1);
    }

    // CALLBACKS
    void planner_references_callback(const std_msgs::msg::Float32MultiArray::SharedPtr msg) 
    {
        int i = 0;
        for(i = 0; i<N+1; i++){
            references.x[i]   = msg->data[i*REF_NUM]; 
            references.y[i]   = msg->data[i*REF_NUM + 1]; 
            references.yaw[i] = msg->data[i*REF_NUM + 2]; 
        }
    }
	
    void planner_vehicle_callback(const std_msgs::msg::Float32MultiArray::SharedPtr msg)
    {
        vehicle.location.x = msg->data[0];
        vehicle.location.y = msg->data[1];
        vehicle.location.z = msg->data[2];

        vehicle.rotation.roll = msg->data[3];
        vehicle.rotation.pitch = msg->data[4];
        vehicle.rotation.yaw = msg->data[5];

        vehicle.velocity.x = msg->data[6];
        vehicle.velocity.y = msg->data[7];
        vehicle.velocity.z = msg->data[8];

        vehicle.acceleration.x = msg->data[9];
        vehicle.acceleration.y = msg->data[10];
        vehicle.acceleration.z = msg->data[11];

        vehicle.angular_velocity.x = msg->data[12];
        vehicle.angular_velocity.y = msg->data[13];
        vehicle.angular_velocity.z = msg->data[14];

        vehicle.error.y = msg->data[15];
        vehicle.error.psi = msg->data[16];

        vehicle.sw.theta = msg->data[17];
        vehicle.sw.w = msg->data[18];
        vehicle.sw.torque_control = msg->data[19];
    }

    void planner_weights_callback(const std_msgs::msg::Float32MultiArray::SharedPtr msg)
    {
        weights.c_ay   = msg->data[0];
        weights.c_T    = msg->data[1];
        weights.c_dpsi = msg->data[2];
        weights.c_th   = msg->data[3];
        weights.c_vy   = msg->data[4];
        weights.c_dt   = msg->data[5];
        weights.w_ay   = msg->data[6];
        weights.w_w    = msg->data[7];
        weights.w_epsi = msg->data[8];
        weights.w_dpsi = msg->data[9];
        weights.w_ey   = msg->data[10];
        weights.w_dT   = msg->data[11];
        weights.Lambda = msg->data[12];
        weights.w_T    = msg->data[13];
    }

    void planner_online_data_callback(const std_msgs::msg::Float32MultiArray::SharedPtr msg)
    {
        int i = 0;
        for(i = 0; i < N+1; i++){
            online_data.lambda[i]    = msg->data[i*ONLINE_NUM];
            online_data.ax[i]        = msg->data[i*ONLINE_NUM + 1];
            online_data.curvature[i] = msg->data[i*ONLINE_NUM + 2];
        }
    }

    void get_ros_data(Vehicle& get_vehicle, Weights& get_weights, References& get_references, OnlineData& get_online_data)
    {
        get_vehicle = vehicle;
        get_weights = weights;
        get_references = references;
        get_online_data = online_data;
    }

    // PUBLISH DATA 
    void publishData(Predictions out_predictions, float out_torque_control_cmd)
    {
        int i;

        std_msgs::msg::Float32MultiArray predictions_msg;
        predictions_msg.data.resize((N+1)*PREDICT_NUM);
        for (i = 0; i < N+1; i ++){ 
            predictions_msg.data[i*PREDICT_NUM]     = out_predictions.x[i];
            predictions_msg.data[i*PREDICT_NUM + 1] = out_predictions.y[i];
            predictions_msg.data[i*PREDICT_NUM + 2] = out_predictions.yaw[i];
            predictions_msg.data[i*PREDICT_NUM + 3] = out_predictions.T[i];
        }
        mpc_predictions_pub_-> publish(predictions_msg);

        std_msgs::msg::Float32 control_cmd_msg;
        control_cmd_msg.data = out_torque_control_cmd;
        mpc_control_cmd_pub_-> publish(control_cmd_msg);
    }

private:

    Vehicle vehicle;
    Weights weights;
    OnlineData online_data;
    References references;

    rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr planner_references_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr planner_vehicle_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr planner_weights_sub_;
    rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr planner_online_data_sub_;

    rclcpp::Publisher<std_msgs::msg::Float32MultiArray>::SharedPtr mpc_predictions_pub_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr mpc_control_cmd_pub_;
};

// Set references for each output variable
void get_acadoVariables_y(ACADOvariables& acadoVariables, References references)
{
    for (int i = 0; i < N; i++){
        acadoVariables.y[i*NY]     = static_cast<double>(references.x[i]);      // X
        acadoVariables.y[i*NY + 1] = static_cast<double>(references.y[i]);      // Y
        acadoVariables.y[i*NY + 2] = static_cast<double>(references.yaw[i]);    // psi
        acadoVariables.y[i*NY + 3] = static_cast<double>(0.0);                  // dpsi 
        acadoVariables.y[i*NY + 4] = static_cast<double>(0.0);                  // w
        acadoVariables.y[i*NY + 5] = static_cast<double>(0.0);                  // T 
        acadoVariables.y[i*NY + 6] = static_cast<double>(0.0);                  // dT 
        
        //cout << fixed << setprecision(2) << i << ": " << references.yaw[i]<< "  ";
    }
}

// Set last value of each output reference
void get_acadoVariables_yN(ACADOvariables& acadoVariables, References references)
{
    acadoVariables.yN[0] = static_cast<double>(references.x[N]);    // X
    acadoVariables.yN[1] = static_cast<double>(references.y[N]);    // Y
    acadoVariables.yN[2] = static_cast<double>(references.yaw[N]);  // psi
    acadoVariables.yN[3] = static_cast<double>(0.0);                // dpsi
    acadoVariables.yN[4] = static_cast<double>(0.0);                // w
    acadoVariables.yN[5] = static_cast<double>(0.0);                // T
}

void get_acadoVariables_od(ACADOvariables& acadoVariables, OnlineData online_data)
{
    for (int i = 0; i < N+1; i++){
        acadoVariables.od[i*NOD]     = static_cast<double>(online_data.lambda[i]); //2.2*max(3.0,static_cast<double>(online_data.lambda[i]))-5.6;  
        acadoVariables.od[i*NOD + 1] = static_cast<double>(0.0); //online_data.ax[i]); 
        acadoVariables.od[i*NOD + 2] = static_cast<double>(online_data.curvature[i]);     
    }
}

void get_acadoVariables_W (ACADOvariables& acadoVariables, Weights weights)
{
    acadoVariables.W[0]        = weights.w_ey;      // X
    acadoVariables.W[NY*1 + 1] = weights.w_ey;      // Y
    acadoVariables.W[NY*2 + 2] = weights.w_epsi;    // psi
    acadoVariables.W[NY*3 + 3] = weights.w_dpsi;    // dpsi
    acadoVariables.W[NY*4 + 4] = weights.w_w;       // w
    acadoVariables.W[NY*5 + 5] = weights.w_T;       // T
    acadoVariables.W[NY*6 + 6] = weights.w_dT;      // dT

    // cout << weights << endl;
}
void get_acadoVariables_WN(ACADOvariables& acadoVariables, Weights weights)
{
    acadoVariables.WN[0]         = 0.1*weights.w_ey;
    acadoVariables.WN[NYN*1 + 1] = 0.1*weights.w_ey;
    acadoVariables.WN[NYN*2 + 2] = 0.1*weights.w_epsi;
    acadoVariables.WN[NYN*3 + 3] = 0.1*weights.w_dpsi;
    acadoVariables.WN[NYN*4 + 4] = 0.1*weights.w_w;
    acadoVariables.WN[NYN*5 + 5] = 0.1*weights.w_T; 
}

void get_predictions (ACADOvariables acadoVariables, Predictions& predictions)
{
    for(int i = 0; i < N+1; i++){
        predictions.x[i]   = acadoVariables.x[i*NX];
        predictions.y[i]   = acadoVariables.x[i*NX + 1];
        predictions.yaw[i] = acadoVariables.x[i*NX + 2];
        predictions.T[i]   = acadoVariables.x[i*NX + 10];
    }
}

int main(int argc, char *argv[])
{
    // Set loop time in milliseconds
	constexpr auto loop_time = std::chrono::milliseconds(LOOP_TIME_MS);
    
    // Initialize node
	rclcpp::init(argc, argv);
    auto mpc_node = std::make_shared<mpcNode>();
    int cont_nan = 0;

    bool mpc_broken = false;
    while(rclcpp::ok()){

        /* Some temporary variables. */
        int    i, iter, cont;
        acado_timer t;
        real_t kkt;

        // Input ROS
        Vehicle vehicle, prev_vehicle;
        Weights weights;
        OnlineData online_data;
        References references;

        // Output ROS
        Predictions predictions;
        float torque_control_cmd = 0.0;;
        
        real_t x_prev[ (N+1)*NX ];
        real_t z_prev[ N*NXA ];
        real_t u_prev[ N*NU ];

        int preparation_status;
        int status = -5;
        
        // acado_printDifferentialVariables();
        //Initialize the solver.
        acado_initializeSolver();
        //acado_preparationStep();

        //Initialize the states and controls. 
        for (i = 0; i < NX * (N + 1); ++i)  acadoVariables.x[ i ] = 0.0;
        for (i = 0; i < NU * N; ++i)  acadoVariables.u[ i ] = 0.0;
        for (i = 0; i < N * NXA; ++i ) acadoVariables.z[ i ] = 0.0;
        
        while(rclcpp::ok()){

            auto init_loop_time = std::chrono::high_resolution_clock::now();

            // Process any callbacks once and get ros data
            rclcpp::spin_some(mpc_node); 
            mpc_node -> get_ros_data(vehicle, weights, references, online_data);

            double states[] = { static_cast<double>(vehicle.location.x),
                                static_cast<double>(vehicle.location.y),
                                static_cast<double>(vehicle.rotation.yaw),
                                static_cast<double>(abs(vehicle.velocity.x)),
                                static_cast<double>(vehicle.velocity.y),
                                static_cast<double>(vehicle.angular_velocity.z),
                                static_cast<double>(vehicle.error.psi),
                                static_cast<double>(vehicle.error.y),
                                static_cast<double>(vehicle.sw.theta),
                                static_cast<double>(vehicle.sw.w),
                                static_cast<double>(vehicle.sw.torque_control) }; // Segunda predicción del calculo anterior

            
            if( mpc_broken ) { //vehicle.velocity.x < 0.5 ||
                // states[0] = 0.0;    // X
                // states[1] = 0.0;    // Y
                // states[2] = 0.0;    // PSI
                states[3] = 0.0;    // vx
                states[4] = 0.0;    // vy
                states[5] = 0.0;    // dpsi
                states[6] = 0.0;    // epsi
                states[7] = 0.0;    // eys
                // states[8] = 0.0;    // theta
                states[9] = 0.0;    // w
                states[10] = 0.0;   // T
            }else{
                // states[0] = 0.0;    // X
                // states[1] = 0.0;    // Y
                // states[2] = 0.0;    // PSI
                // states[3] = 0.0;    // vx
                // states[4] = 0.0;    // vy
                // states[5] = 0.0;    // dpsi
                // states[6] = 0.0;    // epsi
                // states[7] = 0.0;    // ey
                // states[8] = 0.0;    // theta
                // states[9] = 0.0;    // w
                // states[10] = 0.0;   // T
            }

            float abs_ey,x,y;
            x = references.x[0] - vehicle.location.x;
            y = references.y[0] - vehicle.location.y;

            abs_ey = sqrt(x*x + y*y);


            // ==========================================================================
            // =============================== MPC ======================================
            // ==========================================================================

            // Initialize the measurements/reference.
            get_acadoVariables_y (acadoVariables, references);
            get_acadoVariables_yN(acadoVariables, references);
            get_acadoVariables_od(acadoVariables, online_data);
            get_acadoVariables_W (acadoVariables, weights);
            get_acadoVariables_WN(acadoVariables, weights);        

            // MPC Constraints
            for( i = 0; i < QPOASES_NVMAX; ++i )  {
                acadoVariables.lbValues[i] = -weights.c_dt;      
                acadoVariables.ubValues[i] =  weights.c_dt;
            }
            for( i = 0; i < QPOASES_NCMAX; ++i )  {
                acadoVariables.lbAValues[i] = -weights.Lambda;
                acadoVariables.ubAValues[i] =  weights.Lambda;
            }

            // MPC: initialize the current state feedback. 
            //#if ACADO_INITIAL_STATE_FIXED
            for (i = 0; i < NX; ++i) acadoVariables.x0[ i ] = states[ i ];
            //#endif

            if( VERBOSE ) acado_printHeader();

            //Get the time before start of the loop. 
            acado_tic( &t );

            if((prev_vehicle.rotation.yaw>0 && vehicle.rotation.yaw<0) || (prev_vehicle.rotation.yaw<0 && vehicle.rotation.yaw>0)){
                //cout<< " Esto es tremenda chapuza, lo sé :(" << endl;
                for(int i = 0; i < 1; i++){
                    preparation_status = acado_preparationStep();
                    status = acado_feedbackStep();
                    status = -5;
                }
            }

            // Prepare first step 
            preparation_status = acado_preparationStep();
            status = acado_feedbackStep();

            // The "real-time iterations" loop.
            // for(iter = 0; iter < NUM_STEPS; ++iter)
            // {
            // 	// Perform the feedback step.
            // 	//status = acado_feedbackStep( );

            // 	// Apply the new control immediately to the process, first NU components.

            // 	if( VERBOSE ) printf("\tReal-Time Iteration %d:  KKT Tolerance = %.3e\n\n", iter, acado_getKKT() );

            // 	// Optional: shift the initialization (look at acado_common.h). 
                // acado_shiftStates(2, 0, 0); 
                // acado_shiftControls( 0 ); 

            // 	// Prepare for the next step.
            // 	//acado_preparationStep();
            // }

            if((prev_vehicle.rotation.yaw>0 && vehicle.rotation.yaw<0) || (prev_vehicle.rotation.yaw<0 && vehicle.rotation.yaw>0)){
                for( i = 0; i < N*NU; ++i )   acadoVariables.u[ i ] = u_prev[ i ];
            }
            
            get_predictions(acadoVariables, predictions);

            // float sat = 0.0;
            // float Fyf = 0.0;
            // int    Cf = 94000/2;
            // float kd  = 540.0/70.0;
            // float  df = states[8]/kd;
            // float  lf = 1.61;
            // float alpha = 0.0;

            // alpha = df - atan((states[4] + lf*states[5])/(states[3]+1e-3));
            // Fyf = (Cf)*alpha;
            // sat = Fyf/1100.0;

            // Read the elapsed time.
            real_t te = acado_toc( &t );

            // || acadoVariables.x[30*11+7] > 1.5 && acadoVariables.x[1*11+7] < 2.0     // Check last and first error if there are very differents reset

            if (isnan(predictions.T[0])
            || isnan(acadoVariables.z[0])
            || isnan(acadoVariables.z[1])
            || isnan(acadoVariables.z[2])
            || isnan(acadoVariables.z[3])
            || status == -1) 
            {
                mpc_broken = true;
                cont_nan++;
            }else{
                mpc_broken =  false;
            }

            if(!mpc_broken){ // If mpc is not broken, recalculate torque command
                torque_control_cmd = predictions.T[0];
                //if (torque_control_cmd >  weights.Lambda) torque_control_cmd =  weights.Lambda;
                //if (torque_control_cmd < -weights.Lambda) torque_control_cmd = -weights.Lambda;
                if (vehicle.velocity.x < 0.5) torque_control_cmd = 0.0;
            }
            
            // Publish data by ROS
            mpc_node->publishData(predictions, torque_control_cmd); 
            

            // =================================================================================
            // =================================== END =========================================
            // =================================================================================

            prev_vehicle = vehicle;
            for( i = 0; i < N*NXA; ++i )    z_prev[ i ] = acadoVariables.z[ i ];
            for( i = 0; i < N*NU;  ++i )    u_prev[ i ] = acadoVariables.u[ i ];


            // Calculate execution time
            auto end_exec_time = std::chrono::high_resolution_clock::now();
            auto exec_time = std::chrono::duration_cast<std::chrono::microseconds>(end_exec_time - init_loop_time);

            // Sleep until the next desired wake-up time
            auto next_init_time = init_loop_time + loop_time;
            std::this_thread::sleep_until(next_init_time);

            // Calculate loop time
            auto end_loop_time = std::chrono::high_resolution_clock::now();
            auto elapsed_loop_time = std::chrono::duration_cast<std::chrono::microseconds>(end_loop_time - init_loop_time);
            
            if (DISPLAY_DATA){
                // Clear the terminal
                system(clearCommand);

                // cout << "\n =============================\n ============ MPC ============\n =============================" << endl;

                cout << fixed << setprecision(3) 
                << "\n        MPC STATUS"
                << "\n---------------------------"
                << "\n Torque cmd:  " << predictions.T[0] //torque_control_cmd
                << "\n Status:      " << status
                << "\n Prep status: " << preparation_status
                << "\n Nº NAN:      " << cont_nan
                << "\n future error: " << acadoVariables.x[11*30+7]
                << endl;

                cout << fixed << setprecision(3) 
                << "\n      TIME VALUES"
                << "\n---------------------------"
                << "\n Loop time [ms]:  " << elapsed_loop_time.count()/1000.0
                << "\n Exec time [ms]:  " << exec_time.count()/1000.0
                << endl;

                cout << "\n      STATES"
                << "\n---------------------------"
                << "\n       X:\t" << acadoVariables.x[0] << "\t" << acadoVariables.x[0+1*NX] 
                << "\n       Y:\t" << acadoVariables.x[1] << "\t" << acadoVariables.x[1+1*NX] 
                << "\n     PSI:\t" << acadoVariables.x[2]*180/PI << "\t" << acadoVariables.x[2+1*NX]*180/PI 
                << "\n      vx:\t" << acadoVariables.x[3] //<< "\t" << acadoVariables.x[3+1*NX] 
                << "\n      vy:\t" << acadoVariables.x[4] //<< "\t" << acadoVariables.x[4+1*NX] 
                << "\n    dpsi:\t" << acadoVariables.x[5] //<< "\t" << acadoVariables.x[5+1*NX] 
                << "\n    epsi:\t" << acadoVariables.x[6] << "\t" << acadoVariables.x[6+1*NX] 
                << "\n      ey:\t" << acadoVariables.x[7] << "\t" << acadoVariables.x[7+1*NX] 
                << "\n abs(ey):\t" << abs_ey
                << "\n   theta:\t" << acadoVariables.x[8] //<< "\t" << acadoVariables.x[8+1*NX] 
                << "\n       w:\t" << acadoVariables.x[9] << "\t" << acadoVariables.x[9+1*NX] 
                << "\n       T:\t" << acadoVariables.x[10] << "\t" << acadoVariables.x[10+1*NX] 
                << endl;
                cout << "\n      ONLINE DATA"
                << "\n---------------------------"
                << "\n Lambda: " << acadoVariables.od[0]
                << "\n ax:     " << acadoVariables.od[1]
                << "\n kurv:   " << acadoVariables.od[2]
                << endl;
                cout << "\n    ALGEBRAIC VARIABLES"
                << "\n---------------------------"
                << "\n Fyf:      " << acadoVariables.z[0]
                << "\n Fyr:   " << acadoVariables.z[1]
                << "\n bef:   " << acadoVariables.z[2]
                << "\n delta: " << acadoVariables.z[3]
                << endl;
            
                cout << "=============================" << endl;

                if (mpc_broken) break; // Exit the while loop;
                
                // acado_printDifferentialVariables();
                // acado_printControlVariables();
            }
            // cout << "ey: " << vehicle.error.y << endl;
            // cout << "   Ref: " << references.x[0] << "\t" << references.y[0] << endl;
            // cout << "   Loc: " << vehicle.location.x << "\t" << vehicle.location.y << endl;

        }
    }

	rclcpp::shutdown();

    return 0;
}
