#!/bin/bash

echo Refreshing libraries...

# Define directories:
MPC_DIR=~/shared_control/mpc
CODE_GENERATOR_DIR=$MPC_DIR/code_generator
MPC_INCLUDE_DIR=$MPC_DIR/include/acado_library_generated

# Build code generator
cd $CODE_GENERATOR_DIR/build
cmake ..
make

# Generate new code by executable
cd $CODE_GENERATOR_DIR
./exec_to_generate_code

# Remove existing old generated libraries
cd $MPC_INCLUDE_DIR

rm acado_auxiliary_functions.c
rm acado_auxiliary_functions.h
rm acado_common.h
rm acado_integrator.c
rm acado_qpoases3_interface.c
rm acado_qpoases3_interface.h
rm acado_solver.c

# Copy new generated libraries
cd $CODE_GENERATOR_DIR/model_code_generated

cp acado_auxiliary_functions.c $MPC_INCLUDE_DIR
cp acado_auxiliary_functions.h $MPC_INCLUDE_DIR
cp acado_common.h $MPC_INCLUDE_DIR
cp acado_integrator.c $MPC_INCLUDE_DIR
cp acado_qpoases3_interface.c $MPC_INCLUDE_DIR
cp acado_qpoases3_interface.h $MPC_INCLUDE_DIR
cp acado_solver.c $MPC_INCLUDE_DIR

# Build mpc program
cd $MPC_DIR/build
cmake ..
make

echo  
echo ========================= MPC READY ========================= 
echo  