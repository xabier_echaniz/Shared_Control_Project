#include "mpc_bridge.hpp"

MPC_Output out;
MPC_Input in;

class Ros2_MPC_Node : public rclcpp::Node  /* Carla bridge node class */
{
    public:
        Ros2_MPC_Node() : Node("MPC_Node"){ // Constructor

            // Define topics to publish data
            carla_pub_1 = this->create_publisher<carla_msgs::msg::CarlaEgoVehicleControl>("topic1", 1);
            carla_pub_2 = this->create_publisher<carla_msgs::msg::CarlaEgoVehicleControl>("topic2", 1);
            carla_pub_3 = this->create_publisher<carla_msgs::msg::CarlaEgoVehicleControl>("topic3", 1);
        }

        // Publish ---- data for carla
        void publish_carla_1(float array[]){   
                // MIRAR ABAJO COMO HACER. 
                // Hay que meterle los datos necesarios para luego 
                // mandarlo con el comando 
                // publisher_->publish(message);
        }


    private:
        rclcpp::Publisher<carla_msgs::msg::CarlaEgoVehicleControl>::SharedPtr carla_pub_1;
        rclcpp::Publisher<carla_msgs::msg::CarlaEgoVehicleControl>::SharedPtr carla_pub_2;
        rclcpp::Publisher<carla_msgs::msg::CarlaEgoVehicleControl>::SharedPtr carla_pub_3;
};

/* Update input data with new values [ROS2]*/
void getInputData(){

}

/* Update output data with new values [ROS2] */
void setOutputData(){
    
}


// ============================================================================================================

class MinimalPublisher : public rclcpp::Node
{
public:
    MinimalPublisher()
        : Node("minimal_publisher")
    {
        publisher_ = this->create_publisher<std_msgs::msg::Float64MultiArray>("topic", 10);
    }

    void publishFloat(float array[])
    {
		int i;
        std_msgs::msg::Float64MultiArray message;

		message.data.resize(20);

		for(i = 0; i < 20; i++ ){
			//cout << i << endl;
			//message.data.push_back(0);
            message.data[i] = array[i];
		}

		publisher_->publish(message);

    }

private:
    rclcpp::Publisher<std_msgs::msg::Float64MultiArray>::SharedPtr publisher_;
};
