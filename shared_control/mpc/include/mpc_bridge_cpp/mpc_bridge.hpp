#ifndef MPC_BRIDGE_HPP
#define MPC_BRIDGE_HPP

#include <stdio.h>
#include <iostream>

using namespace std;

#include "../acado_library_generated/acado_common.h"
#include "../acado_library_generated/acado_auxiliary_functions.h"

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float64_multi_array.hpp"
#include "carla_msgs/msg/carla_ego_vehicle_control.hpp"

struct MPC_Output {

    double u0[ACADO_NU] = {};  					    /* Control signal [Nm] */
    double xTraj[ACADO_N+1][ACADO_NX] = {};		    
    double uTraj[ACADO_N][ACADO_NU] = {};
    double status = 0.0;   
    double kktTol = 0.0;
    double cpuTime = 0.0;
    double nIter = 0.0;
    double objVal = 0.0;

};

struct MPC_Input {

    double x[ACADO_NX] = {};
	double ref[ACADO_N*ACADO_NY+ACADO_NYN] = {};
	double W[ACADO_N][ACADO_NU] = {};
	double WN = 0.0;
	double bValues = 0.0;
	double od = 0.0;
	double nIter = 0.0;

};

extern MPC_Output out;      /* MPC output variables */
extern MPC_Input in;        /* MPC input variables */

void getInputData();
void setOutputData();

#endif