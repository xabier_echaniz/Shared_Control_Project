#include <memory>
#include <iostream>
#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float32_multi_array.hpp"

#define LOOP_TIME_MS 100

using namespace std;
using std::placeholders::_1;

float prev_msg[20];  // Assuming a fixed size for the array
float actual_msg[20];  // Assuming a fixed size for the array
int cont = 0;
int i;

class MinimalSubscriber : public rclcpp::Node
{
public:
    MinimalSubscriber()
        : Node("minimal_subscriber")
    {
        subscription_ = this->create_subscription<std_msgs::msg::Float32MultiArray>(
            "gui/weights", 1, std::bind(&MinimalSubscriber::topic_callback, this, _1));
    }

private:
    void topic_callback(const std_msgs::msg::Float32MultiArray::SharedPtr msg) const
    {
        cout << "Array recibido de tamaño: " << msg->data.size() << endl;
        for (i = 0; i < msg->data.size(); i++)
        {
            actual_msg[i] = msg->data[i];
            cout << actual_msg[i] << "   ";
        }
        cout << "\n Fin array" << endl;
    }

    rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr subscription_;
};

int main(int argc, char *argv[])
{
    constexpr auto loop_time = std::chrono::milliseconds(LOOP_TIME_MS);

    rclcpp::init(argc, argv);
    auto node = std::make_shared<MinimalSubscriber>();

    while (rclcpp::ok())
    {
        auto init_loop_time = std::chrono::high_resolution_clock::now();

        rclcpp::spin_some(node);
        // Your custom logic can be added here
        // This loop will keep processing messages/events until you manually stop it.

        auto next_init_time = init_loop_time + loop_time;
        std::this_thread::sleep_until(next_init_time);

    }

    rclcpp::shutdown();
    return 0;
}


