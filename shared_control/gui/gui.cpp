#include <wx/wx.h>
#include <wx/slider.h>
#include <wx/sizer.h>

#include "shared_control_basics.hpp"

class guiNode : public rclcpp::Node
{
public:
    guiNode()
        : Node("gui_node")
    {
        publisher_ = this->create_publisher<std_msgs::msg::Float32MultiArray>("gui/weights", 1);
    }

    void publishFloat(float array[], int size)
    {

		int i;
        std_msgs::msg::Float32MultiArray message;

		message.data.resize(size);

		for(i = 0; i < size; i++ ){
            message.data[i] = array[i];
		}

		publisher_->publish(message);

    }

private:
    rclcpp::Publisher<std_msgs::msg::Float32MultiArray>::SharedPtr publisher_;
};

struct mySlider {

        string name;
        double value;
        int min_value;
        int max_value;
};

class MyFrame : public wxFrame {
public:

    int resolution = 100;

    mySlider slider01 = {"c_ay",    0,    0,  10  };
    mySlider slider02 = {"c_T",     0,    0,  15  };
    mySlider slider03 = {"c_dpsl",  0,    0,  1   };
    mySlider slider04 = {"c_th",    0,    0,  4   };
    mySlider slider05 = {"c_vy",    0,    0,  20  };
    mySlider slider06 = {"c_dt",    100,    0,  200  };

    mySlider slider07 = {"w_ay",    0,      0,  1   };
    mySlider slider08 = {"w_w",     0,      0,  10  };
    mySlider slider09 = {"w_epsi",  50,     0,  100 };
    mySlider slider10 = {"w_dpsi",  100,    0,  200 };
    mySlider slider11 = {"w_ey",    50,     0,  300 };
    mySlider slider12 = {"w_dT",    0.1,    0,  1   };
    mySlider slider13 = {"Lambda",  5,      0,  25   };
    mySlider slider14 = {"w_T",     0.15,    0,  1   };

    MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
        : wxFrame(nullptr, wxID_ANY, title, pos, size), node_(std::make_shared<guiNode>()) {
        

        // Store the node
        node_ = std::make_shared<guiNode>();

        // Create a vertical box sizer
        wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

        // ============ SLIDER 01 =============
        // Create a label for the slider
        wxString labelText01 = wxString::Format("%s : %.2f [0 - %d]", slider01.name, slider01.value, slider01.max_value);
        m_label01 = new wxStaticText(this, wxID_ANY, labelText01);
        sizer->Add(m_label01, 0, wxALIGN_CENTER | wxALL, 5);
        // Create the slider
        m_slider01 = new wxSlider(this, wxID_ANY, slider01.value * resolution, slider01.min_value, slider01.max_value * resolution, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
        sizer->Add(m_slider01, 0, wxEXPAND | wxALL, 5);
        // Connect the event for the first slider
        m_slider01->Bind(wxEVT_SCROLL_THUMBTRACK, &MyFrame::OnSlider01Scroll, this);

        // ============ SLIDER 02 =============
        wxString labelText02 = wxString::Format("%s : %.2f [0 - %d]", slider02.name, slider02.value, slider02.max_value);
        m_label02 = new wxStaticText(this, wxID_ANY, labelText02);
        sizer->Add(m_label02, 0, wxALIGN_CENTER | wxALL, 5);
        m_slider02 = new wxSlider(this, wxID_ANY, slider02.value * resolution, slider02.min_value, slider02.max_value * resolution, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
        sizer->Add(m_slider02, 0, wxEXPAND | wxALL, 5);
        m_slider02->Bind(wxEVT_SCROLL_THUMBTRACK, &MyFrame::OnSlider02Scroll, this);

        // ============ SLIDER 03 =============
        wxString labelText03 = wxString::Format("%s : %.2f [0 - %d]", slider03.name, slider03.value, slider03.max_value);
        m_label03 = new wxStaticText(this, wxID_ANY, labelText03);
        sizer->Add(m_label03, 0, wxALIGN_CENTER | wxALL, 5);
        m_slider03 = new wxSlider(this, wxID_ANY, slider03.value * resolution, slider03.min_value, slider03.max_value * resolution, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
        sizer->Add(m_slider03, 0, wxEXPAND | wxALL, 5);
        m_slider03->Bind(wxEVT_SCROLL_THUMBTRACK, &MyFrame::OnSlider03Scroll, this);

        // ============ SLIDER 04 =============
        wxString labelText04 = wxString::Format("%s : %.2f [0 - %d]", slider04.name, slider04.value, slider04.max_value);
        m_label04 = new wxStaticText(this, wxID_ANY, labelText04);
        sizer->Add(m_label04, 0, wxALIGN_CENTER | wxALL, 5);
        m_slider04 = new wxSlider(this, wxID_ANY, slider04.value * resolution, slider04.min_value, slider04.max_value * resolution, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
        sizer->Add(m_slider04, 0, wxEXPAND | wxALL, 5);
        m_slider04->Bind(wxEVT_SCROLL_THUMBTRACK, &MyFrame::OnSlider04Scroll, this);

        // ============ SLIDER 05 =============
        wxString labelText05 = wxString::Format("%s : %.2f [0 - %d]", slider05.name, slider05.value, slider05.max_value);
        m_label05 = new wxStaticText(this, wxID_ANY, labelText05);
        sizer->Add(m_label05, 0, wxALIGN_CENTER | wxALL, 5);
        m_slider05 = new wxSlider(this, wxID_ANY, slider05.value * resolution, slider05.min_value, slider05.max_value * resolution, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
        sizer->Add(m_slider05, 0, wxEXPAND | wxALL, 5);
        m_slider05->Bind(wxEVT_SCROLL_THUMBTRACK, &MyFrame::OnSlider05Scroll, this);

        // ============ SLIDER 06 =============
        wxString labelText06 = wxString::Format("%s : %.2f [0 - %d]", slider06.name, slider06.value, slider06.max_value);
        m_label06 = new wxStaticText(this, wxID_ANY, labelText06);
        sizer->Add(m_label06, 0, wxALIGN_CENTER | wxALL, 5);
        m_slider06 = new wxSlider(this, wxID_ANY, slider06.value * resolution, slider06.min_value, slider06.max_value * resolution, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
        sizer->Add(m_slider06, 0, wxEXPAND | wxALL, 5);
        m_slider06->Bind(wxEVT_SCROLL_THUMBTRACK, &MyFrame::OnSlider06Scroll, this);

        // ============ SLIDER 07 =============
        wxString labelText07 = wxString::Format("%s : %.2f [0 - %d]", slider07.name, slider07.value, slider07.max_value);
        m_label07 = new wxStaticText(this, wxID_ANY, labelText07);
        sizer->Add(m_label07, 0, wxALIGN_CENTER | wxALL, 5);
        m_slider07 = new wxSlider(this, wxID_ANY, slider07.value * resolution, slider07.min_value, slider07.max_value * resolution, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
        sizer->Add(m_slider07, 0, wxEXPAND | wxALL, 5);
        m_slider07->Bind(wxEVT_SCROLL_THUMBTRACK, &MyFrame::OnSlider07Scroll, this);

        // ============ SLIDER 08 =============
        wxString labelText08 = wxString::Format("%s : %.2f [0 - %d]", slider08.name, slider08.value, slider08.max_value);
        m_label08 = new wxStaticText(this, wxID_ANY, labelText08);
        sizer->Add(m_label08, 0, wxALIGN_CENTER | wxALL, 5);
        m_slider08 = new wxSlider(this, wxID_ANY, slider08.value * resolution, slider08.min_value, slider08.max_value * resolution, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
        sizer->Add(m_slider08, 0, wxEXPAND | wxALL, 5);
        m_slider08->Bind(wxEVT_SCROLL_THUMBTRACK, &MyFrame::OnSlider08Scroll, this);

        // ============ SLIDER 09 =============
        wxString labelText09 = wxString::Format("%s : %.2f [0 - %d]", slider09.name, slider09.value, slider09.max_value);
        m_label09 = new wxStaticText(this, wxID_ANY, labelText09);
        sizer->Add(m_label09, 0, wxALIGN_CENTER | wxALL, 5);
        m_slider09 = new wxSlider(this, wxID_ANY, slider09.value * resolution, slider09.min_value, slider09.max_value * resolution, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
        sizer->Add(m_slider09, 0, wxEXPAND | wxALL, 5);
        m_slider09->Bind(wxEVT_SCROLL_THUMBTRACK, &MyFrame::OnSlider09Scroll, this);

        // ============ SLIDER 10 =============
        wxString labelText10 = wxString::Format("%s : %.2f [0 - %d]", slider10.name, slider10.value, slider10.max_value);
        m_label10 = new wxStaticText(this, wxID_ANY, labelText10);
        sizer->Add(m_label10, 0, wxALIGN_CENTER | wxALL, 5);
        m_slider10 = new wxSlider(this, wxID_ANY, slider10.value * resolution, slider10.min_value, slider10.max_value * resolution, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
        sizer->Add(m_slider10, 0, wxEXPAND | wxALL, 5);
        m_slider10->Bind(wxEVT_SCROLL_THUMBTRACK, &MyFrame::OnSlider10Scroll, this);

        // ============ SLIDER 11 =============
        wxString labelText11 = wxString::Format("%s : %.2f [0 - %d]", slider11.name, slider11.value, slider11.max_value);
        m_label11 = new wxStaticText(this, wxID_ANY, labelText11);
        sizer->Add(m_label11, 0, wxALIGN_CENTER | wxALL, 5);
        m_slider11 = new wxSlider(this, wxID_ANY, slider11.value * resolution, slider11.min_value, slider11.max_value * resolution, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
        sizer->Add(m_slider11, 0, wxEXPAND | wxALL, 5);
        m_slider11->Bind(wxEVT_SCROLL_THUMBTRACK, &MyFrame::OnSlider11Scroll, this);

        // ============ SLIDER 12 =============
        wxString labelText12 = wxString::Format("%s : %.2f [0 - %d]", slider12.name, slider12.value, slider12.max_value);
        m_label12 = new wxStaticText(this, wxID_ANY, labelText12);
        sizer->Add(m_label12, 0, wxALIGN_CENTER | wxALL, 5);
        m_slider12 = new wxSlider(this, wxID_ANY, slider12.value * resolution, slider12.min_value, slider12.max_value * resolution, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
        sizer->Add(m_slider12, 0, wxEXPAND | wxALL, 5);
        m_slider12->Bind(wxEVT_SCROLL_THUMBTRACK, &MyFrame::OnSlider12Scroll, this);

        // ============ SLIDER 13 =============
        wxString labelText13 = wxString::Format("%s : %.2f [0 - %d]", slider13.name, slider13.value, slider13.max_value);
        m_label13 = new wxStaticText(this, wxID_ANY, labelText13);
        sizer->Add(m_label13, 0, wxALIGN_CENTER | wxALL, 5);
        m_slider13 = new wxSlider(this, wxID_ANY, slider13.value * resolution, slider13.min_value, slider13.max_value * resolution, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
        sizer->Add(m_slider13, 0, wxEXPAND | wxALL, 5);
        m_slider13->Bind(wxEVT_SCROLL_THUMBTRACK, &MyFrame::OnSlider13Scroll, this);

        // ============ SLIDER 14 =============
        wxString labelText14 = wxString::Format("%s : %.2f [0 - %d]", slider14.name, slider14.value, slider14.max_value);
        m_label14 = new wxStaticText(this, wxID_ANY, labelText14);
        sizer->Add(m_label14, 0, wxALIGN_CENTER | wxALL, 5);
        m_slider14 = new wxSlider(this, wxID_ANY, slider14.value * resolution, slider14.min_value, slider14.max_value * resolution, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
        sizer->Add(m_slider14, 0, wxEXPAND | wxALL, 5);
        m_slider14->Bind(wxEVT_SCROLL_THUMBTRACK, &MyFrame::OnSlider14Scroll, this);

        SetSizerAndFit(sizer);
        Centre();

        // Create a timer to continuously log slider values
        m_timer = new wxTimer(this, ID_TIMER);
        m_timer->Start(1000); // Log values every 1 second
        Bind(wxEVT_TIMER, &MyFrame::OnTimer, this);
    }

private:
    enum {
        ID_TIMER = 1000
    };

    wxSlider* m_slider01;
    wxSlider* m_slider02;
    wxSlider* m_slider03;
    wxSlider* m_slider04;
    wxSlider* m_slider05;
    wxSlider* m_slider06;
    wxSlider* m_slider07;
    wxSlider* m_slider08;
    wxSlider* m_slider09;
    wxSlider* m_slider10;
    wxSlider* m_slider11;
    wxSlider* m_slider12;
    wxSlider* m_slider13;
    wxSlider* m_slider14;

    wxStaticText* m_label01;
    wxStaticText* m_label02;
    wxStaticText* m_label03;
    wxStaticText* m_label04;
    wxStaticText* m_label05;
    wxStaticText* m_label06;
    wxStaticText* m_label07;
    wxStaticText* m_label08;
    wxStaticText* m_label09;
    wxStaticText* m_label10;
    wxStaticText* m_label11;
    wxStaticText* m_label12;
    wxStaticText* m_label13;
    wxStaticText* m_label14;

    wxTimer* m_timer;
    float mySliderData[WEIGHT_NUM];
    std::shared_ptr<guiNode> node_;
    Weights weights;
    

    void OnSlider01Scroll(wxScrollEvent& event) {
        slider01.value = static_cast<double>(m_slider01->GetValue())/resolution;
        m_label01->SetLabel(wxString::Format("%s : %.2f [0 - %d]", slider01.name, slider01.value, slider01.max_value));
    }

    void OnSlider02Scroll(wxScrollEvent& event) {
        slider02.value = static_cast<double>(m_slider02->GetValue())/resolution;
        m_label02->SetLabel(wxString::Format("%s : %.2f [0 - %d]", slider02.name, slider02.value, slider02.max_value));
    }

    void OnSlider03Scroll(wxScrollEvent& event) {
        slider03.value = static_cast<double>(m_slider03->GetValue())/resolution;
        m_label03->SetLabel(wxString::Format("%s : %.2f [0 - %d]", slider03.name, slider03.value, slider03.max_value));
    }

    void OnSlider04Scroll(wxScrollEvent& event) {
        slider04.value = static_cast<double>(m_slider04->GetValue())/resolution;
        m_label04->SetLabel(wxString::Format("%s : %.2f [0 - %d]", slider04.name, slider04.value, slider04.max_value));
    }

    void OnSlider05Scroll(wxScrollEvent& event) {
        slider05.value = static_cast<double>(m_slider05->GetValue())/resolution;
        m_label05->SetLabel(wxString::Format("%s : %.2f [0 - %d]", slider05.name, slider05.value, slider05.max_value));
    }

    void OnSlider06Scroll(wxScrollEvent& event) {
        slider06.value = static_cast<double>(m_slider06->GetValue())/resolution;
        m_label06->SetLabel(wxString::Format("%s : %.2f [0 - %d]", slider06.name, slider06.value, slider06.max_value));
    }

    void OnSlider07Scroll(wxScrollEvent& event) {
        slider07.value = static_cast<double>(m_slider07->GetValue())/resolution;
        m_label07->SetLabel(wxString::Format("%s : %.2f [0 - %d]", slider07.name, slider07.value, slider07.max_value));
    }

    void OnSlider08Scroll(wxScrollEvent& event) {
        slider08.value = static_cast<double>(m_slider08->GetValue())/resolution;
        m_label08->SetLabel(wxString::Format("%s : %.2f [0 - %d]", slider08.name, slider08.value, slider08.max_value));
    }

    void OnSlider09Scroll(wxScrollEvent& event) {
        slider09.value = static_cast<double>(m_slider09->GetValue())/resolution;
        m_label09->SetLabel(wxString::Format("%s : %.2f [0 - %d]", slider09.name, slider09.value, slider09.max_value));
    }

    void OnSlider10Scroll(wxScrollEvent& event) {
        slider10.value = static_cast<double>(m_slider10->GetValue())/resolution;
        m_label10->SetLabel(wxString::Format("%s : %.2f [0 - %d]", slider10.name, slider10.value, slider10.max_value));
    }

    void OnSlider11Scroll(wxScrollEvent& event) {
        slider11.value = static_cast<double>(m_slider11->GetValue())/resolution;
        m_label11->SetLabel(wxString::Format("%s : %.2f [0 - %d]", slider11.name, slider11.value, slider11.max_value));
    }

    void OnSlider12Scroll(wxScrollEvent& event) {
        slider12.value = static_cast<double>(m_slider12->GetValue())/resolution;
        m_label12->SetLabel(wxString::Format("%s : %.2f [0 - %d]", slider12.name, slider12.value, slider12.max_value));
    }

    void OnSlider13Scroll(wxScrollEvent& event) {
        slider13.value = static_cast<double>(m_slider13->GetValue())/resolution;
        m_label13->SetLabel(wxString::Format("%s : %.2f [0 - %d]", slider13.name, slider13.value, slider13.max_value));
    }

    void OnSlider14Scroll(wxScrollEvent& event) {
        slider14.value = static_cast<double>(m_slider14->GetValue())/resolution;
        m_label14->SetLabel(wxString::Format("%s : %.2f [0 - %d]", slider14.name, slider14.value, slider14.max_value));
    }

    void OnTimer(wxTimerEvent& event) {
        
        mySliderData[0]  = slider01.value;
        mySliderData[1]  = slider02.value;
        mySliderData[2]  = slider03.value;
        mySliderData[3]  = slider04.value;
        mySliderData[4]  = slider05.value;
        mySliderData[5]  = slider06.value;
        mySliderData[6]  = slider07.value;
        mySliderData[7]  = slider08.value;
        mySliderData[8]  = slider09.value;
        mySliderData[9]  = slider10.value;
        mySliderData[10] = slider11.value;
        mySliderData[11] = slider12.value;
        mySliderData[12] = slider13.value;
        mySliderData[13] = slider14.value;

        
        
        node_->publishFloat(mySliderData,WEIGHT_NUM); // Publish the float value
        rclcpp::spin_some(node_); // Process any callbacks once

        // Clear the terminal
        system(clearCommand);

        cout << "\n =================\n ==== WEIGHTS ====\n =================\n" << endl;

        weights.c_ay   = mySliderData[0];
        weights.c_T    = mySliderData[1];
        weights.c_dpsi = mySliderData[2];
        weights.c_th   = mySliderData[3];
        weights.c_vy   = mySliderData[4];
        weights.c_dt   = mySliderData[5];
        weights.w_ay   = mySliderData[6];
        weights.w_w    = mySliderData[7];
        weights.w_epsi = mySliderData[8];
        weights.w_dpsi = mySliderData[9];
        weights.w_ey   = mySliderData[10];
        weights.w_dT   = mySliderData[11];
        weights.Lambda = mySliderData[12];
        weights.w_T    = mySliderData[13];

        std::cout << weights << std::endl;
    }
};

class MyApp : public wxApp {
public:
    virtual bool OnInit() {
        MyFrame* frame = new MyFrame("MPC weight ponderation", wxPoint(50, 50), wxSize(400, 200));
        frame->Show(true);
        return true;
    }
};

// Custom main function
int main(int argc, char **argv) {

    rclcpp::init(argc, argv); //Start ros node

    MyApp myApp;
    wxApp::SetInstance(&myApp);
    wxEntryStart(argc, argv);

    

    //execute application
    wxTheApp->OnInit();
    wxTheApp->OnRun();
    wxTheApp->OnExit();

    rclcpp::shutdown();
    wxEntryCleanup();

    return 0;
}