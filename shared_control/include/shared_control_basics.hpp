#ifndef SHARED_CONTROL_BASICS_HPP
#define SHARED_CONTROL_BASICS_HPP

// Library declaration
// -------------------
#include <memory> 
#include <chrono>
#include <iostream>
#include <array>  
#include <string>       
#include <cmath>       
#include <cstdlib>   

// ROS msg types
// -------------
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float32.hpp"
#include "std_msgs/msg/float32_multi_array.hpp"
#include "carla_msgs/msg/carla_ego_vehicle_control.hpp"

// "using" declarations
// --------------------
using namespace std::chrono_literals;
using namespace std;
using std::placeholders::_1;

// Global constants
// ----------------
#define N   30  // Number of intervals in the horizon MPC
#define PI  3.14159265358979323846
const char* clearCommand = "clear";


// -- Other data --
#define MAX_STEER_ANGLE 70.0  // Max angle of wheels
#define MAX_SW_ANGLE 540.0    // Max angle of steering wheel


// Data type declarations
// -----------------------
struct Simulator{
    struct{
        float sw_angle;                 // Steer angle received from simulator
        float sw_angular_velocity;      // Steer angular velocity received from simulator
        float throttle;                 // Throttle value received from simulator
        float brake;                    // Brake value received from simulator
        float sw_driver_torque;         // Torque applied by the driver received from simulator
    }status;
    
    struct{
        float sw_torque;
        float sw_friction;
        float sw_damp;
    }command;
};

struct CarlaControl {
    float steering;
    float throttle;
    float brake;
};

#define WEIGHT_NUM      14  // Number of sliders in the GUI
struct Weights {
    float c_ay;
    float c_T;
    float c_dpsi;
    float c_th;
    float c_vy;
    float c_dt;
    float w_ay;
    float w_w;
    float w_epsi;
    float w_dpsi;
    float w_ey;
    float w_dT;
    float Lambda;
    float w_T;
};

struct Vehicle {
    struct {
        float x;            // Vehicle x-position
        float y;            // Vehicle y-position
        float z;            // Vehicle z-position
    } location;

    struct {
        float roll;         // Vehicle roll
        float pitch;        // Vehicle pitch
        float yaw;          // Vehicle yaw
    } rotation;

    struct {
        float x;            // Vehicle x-velocity
        float y;            // Vehicle y-velocity
        float z;            // Vehicle z-velocity
    } velocity;

    struct {
        float x;            // Vehicle x-acceleration
        float y;            // Vehicle y-acceleration
        float z;            // Vehicle z-acceleration
    } acceleration;

    struct {
        float x;            // Vehicle x-angular velocity
        float y;            // Vehicle y-angular velocity
        float z;            // Vehicle z-angular velocity
    } angular_velocity;

    struct {
        float y;            // Vehicle lateral error
        float psi;          // Vehicle heading error
    }error;

    struct {
        float theta;            // Steer Wheel angle
        float w;                // Steer Wheel angular velocity
        float torque_control;   // Steer Wheel torque control value
    }sw;
};

struct Waypoints{
    float x[N+1];             // Waypoint x-positions
    float y[N+1];             // Waypoint y-positions
    float yaw[N+1];           // Waypoint yaw angles
};

#define REF_NUM         3   // Number of reference variables for MPC
struct References{
    float x[N+1];            // Reference x-positions
    float y[N+1];            // Reference y-positions
    float yaw[N+1];          // Reference yaw angles
};


#define ONLINE_NUM      3   // Number of online variables
struct OnlineData {
    float lambda[N+1];        // Authority values
    float ax[N+1];            // Acceleration values
    float curvature[N+1];     // Curvature values
};

#define PREDICT_NUM     4   // Number of predicted variables
struct Predictions {
    float x[N+1];             // Predicted x-positions
    float y[N+1];             // Predicted y-positions
    float yaw[N+1];           // Predicted yaw angles
    //float ey[N+1];            // Predicted lateral errors
    //float epsi[N+1];          // Predicted heading errors
    float T[N+1];             // Predicted torque control
};

struct Point {
    float x;                // Point x-position
    float y;                // Point y-position
};

std::ostream& operator<<(std::ostream& os, const Vehicle& vehicle) {
    os << "Location: (" << vehicle.location.x << ", " << vehicle.location.y << ", " << vehicle.location.z << ")\n";
    os << "Rotation: (" << vehicle.rotation.roll << ", " << vehicle.rotation.pitch << ", " << vehicle.rotation.yaw*180/PI << ")\n";
    os << "Velocity: (" << vehicle.velocity.x << ", " << vehicle.velocity.y << ", " << vehicle.velocity.z << ")\n";
    os << "Acceleration: (" << vehicle.acceleration.x << ", " << vehicle.acceleration.y << ", " << vehicle.acceleration.z << ")\n";
    os << "Angular Velocity: (" << vehicle.angular_velocity.x << ", " << vehicle.angular_velocity.y << ", " << vehicle.angular_velocity.z << ")\n";
    os << "Lateral Error: " << vehicle.error.y << "\n";
    os << "Heading Error: " << vehicle.error.psi << "\n";
    os << "Steer Wheel Angle: " << vehicle.sw.theta << "\n";
    os << "Steer Wheel Angular Velocity: " << vehicle.sw.w << "\n";
    os << "Steer Wheel Torque Control: " << vehicle.sw.torque_control << "\n";
    return os;
}

std::ostream& operator<<(std::ostream& os, const Weights& weights) {
    os << "c_ay: " << weights.c_ay << "\n";
    os << "c_T: " << weights.c_T << "\n";
    os << "c_dpsi: " << weights.c_dpsi << "\n";
    os << "c_th: " << weights.c_th << "\n";
    os << "c_vy: " << weights.c_vy << "\n";
    os << "c_dt: " << weights.c_dt << "\n";
    os << "w_ay: " << weights.w_ay << "\n";
    os << "w_w: " << weights.w_w << "\n";
    os << "w_epsi: " << weights.w_epsi << "\n";
    os << "w_dpsi: " << weights.w_dpsi << "\n";
    os << "w_ey: " << weights.w_ey << "\n";
    os << "w_dT: " << weights.w_dT << "\n";
    os << "Lambda: " << weights.Lambda << "\n";
    os << "w_T: " << weights.w_T << "\n";
    return os;
}

float max(float x, float y) {
    return (x > y) ? x : y;
}

int sign_function(float n){
    return (n<0)?(-1):(1);
}


#endif